The World of a Sorcerer 141

—that there is sense in this world, and that is the premise that his
philosophy of life explicitly rejects.

Though all things are meaningless, there is one standard by
which al] events are measured, by which all things are tested, death:
“Death is the only wise adviser that we have.”45 Nevertheless, adop-
ting the language of a Zen monk, don Juan announces: “Death is a

whorl. . . . Death is the face of the ally; death is a shiny cloud over
the horizon; death is the whisper of Mescalito {the Peyote god] in
your ears; .. , death is me talking; death is you and your writing

pad; death is nothing. Nothing! It is here yet it isn’t here at all.’
This kind of philosophical mishmash is supposed to be rare wisdom.
The old Indian is correct: “To be a sorcerer is a terrible burden.”*?
Especially philosophically.

Prisoners of Power

The apostle Paul, writing to the church at Rome, warned them
of the inescapability of service in the world. The question is never to
serve or not to serve; it is always whom to serve. “Know ye not,” Paul
asks, “that to whom ye yield yourselves servants to obey, his servants
ye are to whom ye obey; whether of sin unto death, or of obedience
unto righteousness?” (Romans 6:16). Don Juan’s discussion of power
demonstrates how clearly he understands this principle. On the one
hand, union with a demonic ally gives the sorcerer power: “From
then on you can summon your ally at will and make him do any-
thing you want.”"* These allies are supposedly neutral forces,
“neither good nor evil, but are put to use by the sorcerers for what-
ever purpose they see fit.”#? Yet on the other hand, he warns Casta-
neda against becoming a “slave to the devil’s weed” (jimsonweed), for
“it will never let you go.” The weed is the means of obtaining revela-
tions from the female spirit that is associated with it. “She will cut
you off from everything else. You will have to spend your life groom-
ing her as an ally. She is possessive, Once she dominates you, there
is only one way to go—her way.”

Don Juan’s references to the “protectors” of some friends and rel-

45. Ixtlan, p. 34.

46, Separate Reality, p. 195.
47, Hbid., p. 199.

48. Ibid., p. 40.

49. bid.

50, Teachings, p. 120.
