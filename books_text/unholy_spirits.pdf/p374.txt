368 UNHOLY SPIRITS

politically since at least Thomas Hobbes’ Leviathan (1651). The
French Enlightenment simply transferred Hobbes’ vision of the
king's sovereign central power to the democratic State’s sovereign
central power. The French Revolution was the result of this vision.
So was the Russian Revolution.

Since 1900, Darwinian evolution has also led straight into the arms
of the central planners. People want someone or some institution to
guarantee the existence of meaning, progress, and success. In earlier
centuries, they trusted the king and the church to provide such institu-
tional confirmation of God’s providence. This faith began to wane in
the latter years of the nineteenth century, What institution would
replace king and church? What sovereign force would replace God?
The free market was the answer of late-nineteenth-century Social
Darwinists: the eternal “law of competition.”” This faith never
became widespread. People have not trusted the hypothetically auton-
omous free market to give them what they want.

What finally replaced decentralized thinking was a new faith, a
faith in the ability of scientists to work with politicians and profes-
sionally trained (and screened) bureaucrats to create a scientific pro-
gram of central planning. The “laws of social evolution” and the
“laws of economics” could be applied to society as a whole in order to
produce social progress.”* Since 1900, virtually everything that the
Jeading humanist thinkers have officially promoted has led to cen-
tralization. Independent, market-produced technology has offered a
tremendous potential for decentralization, but not the politics of hu-
manism. The New Agers opcrate with the paradigm of evolution. It
is central to their world view. This humanism is not necessarily Dar-
winian. It is not strictly scientific, It is closer to the evolutionism of
alchemy than it is to the evolutionism of chemistry. Nevertheless, it
is an evolutionary process which is being heralded by a self
conscious elite, whether “Tibetan masters,” or “avatars,” or just plain
New Age members of one or another “inner circle.” By necessity, the
proposed evolutionary “leap of consciousness” begins with a minority
of “aware” or “enlightened” or “illuminated” ones, who in turn spread
the message and the various recommended techniques of enlighten-
ment. The circles of initiation grow wider, but they remain circles of

73. Richard Hofstadter, Social Darwinism in American Thought (rev. ed.; New York:
Braziller, 1959).

74. Gary North, The Dominion Covenant: Genesis (Lyler, ‘Yexas: institute for Cheis-
tian Economics, 1982), Appendix A and Appendix B.
