10
ESCAPE FROM CREATUREHOOD

Therefore if any man be in Christ, he is a new creature: old things are
passed away, behold, all things are become new.
HI Corinthians 5:17

We come now to the heart of this book. By now the reader may
be confused. What do all these things mean? Eyeless sight, auras
photographed by Kirlian photography, spontaneous human com-
bustion, psychic healing, knives in eyes, water witching, and other
improbable facets of human life that the textbooks ignore. Dozens of
books could be written and have been written on these topics, as well
as thousands of books on similar weird events. There is far too much
here for any one person to master. Yet we must not become like those
described by St. Paul: “Ever learning, and never able to come to the
knowledge of the truth” (II Tim. 3:7). There are fascinating aspects
about the general subject of the occult, but books about the occult
should not be written merely to satisfy the curious or the potential
occult practitioner. Men and women should know of the existence of
such phenomena in order to avoid them and give competent warn-
ings to those who have not avoided them. We do not have any need
for knowing everything about our spiritual opposition. We only need
to know enough to recognize the danger and avoid it. The issue is eth-
ics, not exhaustive knowledge. “And have no fellowship with the un-
fruitful works of darkness, but rather reprove them. For it is a shame
even to speak of those things which are done of them in secret. But all
things that are reproved are made manifest by the light: for whatso-
ever doth make manifest is light. Wherefore he saith, Awake thou
that sleepest, and arise from the dead, and Christ shall give thee
light. See then that ye walk circumspectly, not as fools, but as wise,
redeeming the time, because the days are evil” (Eph. 5:11-16).

There are many other subjects that might have been covered: ex-

329
