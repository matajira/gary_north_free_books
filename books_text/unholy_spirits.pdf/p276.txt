270 UNHOLY SPIRITS

iumistic forces.”

Roberts admitted publicly that when he was younger, an old In-
dian once healed him. Koch speculates that this may have been the
origin of his healing powers.” Transfers of such powers are reported
again and again by healing mediums. (Rev. Plume is one example.)

Conclusion

Western rationalism is as one-sided and narrow as any other
world-and-life view. Those who hold to its premises have exercised
great power over the last two centuries. Much of this power is well
deserved. By refining the techniques of science, Western thinkers
have made possible a flowering of human culture which is the envy
of the primitive, magical world. The whole underdeveloped world
wants a piece of our action, but they do not usually want to give up
the other aspects of their culture that guarantee continued poverty.
By turning their leaders into socialists, materialists, bureaucrats,
and rationalists by giving them scholarships to our better universi-
ties, we have cut off the leaders from their popular support. Democ-
racy cannot flourish in these nations for many reasons, not the least
of which is the undemocratic nature of all bureaucratic rule, and it is
bureaucracy, far more than democracy, which has been the West's
major organizational export — bureaucracy financed by government-
to-government foreign aid and heavy taxation inside the recipients’
nations. But democracy also cannot flourish where demonism is
present. The essence of demonism is an elitist manipulation of the
common tribesmen. The means of power are personal asceticism,
demon possession, trances, visions, dreams, and total immersion
into the occult. Ritual, not a study of the regularities of nature, is the
law of primitive cultures. The ideology of bureaucracy may be in
conformity with some of the aspects of demonism, but democracy is
in total opposition. No one but a Western intellectual could have be-
lieved that India was democratic, or that its traditions and institu-
tions might permit the limited democracy of the liberal West. Indira
Gandhi did not cut down Indian democracy at the roots; there never
were any roots, except in the dreams and visions of Western com-
mentators and in Indian newspaper editorials. And what is true of
India is true of the whole so-called Third World. Where demonism is a

18. Ibid., p. 55.
79, Bid., p. 54.
