This book is dedicated to

Bob Mumford

copyright, Gary North, 1988 [1994]

 

Library of Congress Cataloging-in-Publication Data
North, Gary

Unholy spirits: occultism and New Age humanism / Gary North.
. cm.

Includes bibliographical references and indexes.

ISBN 0-930462-53-X : $12.95

1, Occultism--Religious aspects~Christianity. 2. Apologetics.
38. Occultism--Controversial literature. 4. New age movement-
-Controversial literature. 5. Parapsychology--Controversial
literature. 6. Humanism--20th century--Controversial literature.
7. Dominion theology. I. Title.

BR115.03N67 1994 94-20710
239 .9--dc20 CIP

 

Institute for Christian Economics
P.O. Box 8000
‘Tyler, Texas 75711
