INTRODUCTION

Thate to revise my bouks. The older the book is, the more I hate
to revise it. I always feel compelled to read lots of books and articles
that have been published in the interim that deal with the topic of the
book in question, The longer the book has been out of print, the
stronger my compulsion to read and revise extensively. So I tend not
to re-release my older books. Marx’s Religion of Revolution (1968) has
been out of print for over a decade; An Introduction to Christian Eco-
nomics (1973, with revisions through 1979) has been out of print for
five years.

The original version of this book was written in 1974, It was pub-
lished in early 1976 under the title None Dare Call It Witchcraft. It has
been out of print since about 1979. It went through three printings,
but then the original publisher, Arlington House, went bankrupt.
While there has been a continuing trickle of inquiries about where to
buy the book, I just did not have time to revise it, I have written and
published about eight books since 1979, plus I completely reworked a
ninth book into a new format, plus I have had to build up my news-
letter business and the Institute for Christian Economics by launch-
ing about ten newsletters (and killing several as bad decisions). I
have just not had the time to revise it thoroughly.

T have decided to compromise. I have made a few revisions in
this book, but the text is very close to the original. I have added some
chapters but have removed the original appendices by Rushdoony,
Molnar, and Ketchen. ‘These essays are still in print in The journal of
Christian Reconstruction, Vol, I, No. 2 (Winter, 1974): “Symposium on
Satanism,” which I edited. This journal is published by the Chalce-
don Foundation, P.O. Box 158, Vallecito, California 95251.

Writing this Book

The original version had an odd history. In the summer of 1972, I
was lecturing to a group of students at a conference sponsored by the
Midwest office of the Intercollegiate Studies Institute (I.S.I.). I

1
