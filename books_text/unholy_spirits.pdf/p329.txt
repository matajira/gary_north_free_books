Invaders from... ? 323

past quarter of a century [1975], There is little new in the contents of
recent UFO reports: a 1975 report generally does not differ in basic
content from a 1955 or 1965 UFO report. UFOs in a real sense do
not constitute news.”® There is nothing new about them any more.

This is extremely important for understanding why scientists
have not and do not take flying saucers seriously. They have nothing
to gain from further studies, and everything to lose if this assault on
Kantian reason is based on noumenal events. The UFO creatures
do not contact scientists and show them how to improve theory,
science, or technology. They do not do what the space invader
Klaatu did in “The Day the Earth Stood Still”: go to the office of a
great scientist and correct his blackboard errors in mathematics.
That would gain their attention! If these invaders were to single out
one or two scientists in any field and give them better knowledge, so
that they could publish the new information and become famous
within their profession, word would get out. Even if the new knowl-
edge was impractical, if it was theoretically compelling to scholars in
the field, the invaders would have no trouble enlisting true believers
and followers in the scientific community. Non-tenured scholars
would search out the invaders and beg to be taught. Science would
develop, and eventually word would get out as to who was providing
the new information. Scientists, especially academically employed
scientists, will do whatever it takes to get published in scholarly jour-
nals, including meeting with little green men.

Scientists see that the contactees are not scientists generally, and
even when they are scientists, these men do not bring forth innovat-
ive new scientific information. So there is little likelihood that con-
tacts with the invaders will in any way bridge the gap between man’s
knowledge and theirs. We will not be able to transform today’s “not
yet known” into data that are usable in the scientific tool kit. In
short, conventional scientists smell trouble: trouble for their careers,
trouble for their sanity, and trouble with beings that have powers
that are not phenomenal in the Kantian sense but all too phenome-
nal (in the general English usage sense). Why get involved? Better to
define the problem away, ignore the problem away, or if pushed, rid-
icule the problem away. This is the safe course of action, too, for un-
like other scientific discoveries that were once defined away, ignored
away, and ridiculed away, only to become accepted later on, there is

8. Ibid. p. 3
