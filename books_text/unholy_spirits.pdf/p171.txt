Psychics 165

 

cluding social and historical laws. Especially after Darwin, the scien-
tific mind has become religiously anti-teleological. The future in no
way can influence the present, except as a vision which motivates
men to action. ‘The entire body of modern science rests on this pre-
supposition, Any break in this universal law — that the future cannot
be known except as an extension of present forces—threatens the
foundation of modern science, and therefore threatens atheistic secu-
Jar humanism.

It is understandable that the claim of anyone to be able to fore-
cast specific future events that are not the product of measurable so-
cial forces — “random” events — must be rejected by modern science.
Any aspect of the realm of “paranormal science” will be believed by
large numbers of conventional scientists before psychic forecastng
will be believed. This supernatural skill, above all others, will be rid-
iculed and denied. The Heisenberg principle of indeterminacy may
somehow offer modern science an epistemological escape hatch from
odd events and odder human abilities, but it offers no rational hope
for explaining the ability of some people to see the future.

Thus, the psychic is the last hurdle in the traditional humanist’s
race. If he trips on this one, he loses the race. When humanists begin
to believe in the ability of psychics to forecast specific future events,
the hole in the dike will have grown to dam-busting proportions.

In the mid-1960’s, some humanists began to believe in psychic
forecasts.

 

 

David Hoy

David Hoy is a psychic. Given his publicly demonstrated abili-
ties, he is é#e psychic — the most baffling mind-reader, prognosticator,
and diviner practicing his talents anywhere in the world today, He
performs without any mysterious atmosphere, esoteric wisdom,
Eastern philosophy, vibrations, or anything else. His special gift, if
one could call it a gift, is instant revelation. Not revelation about
God or the universe, but revelation concerning the more mundane
affairs of life, such as where someone has lost her false teeth. As
Newsweek reported: “It was phone-in time on radio station KCMO in
Kansas City. ‘Mr. Hoy, lisped a woman caller, ‘my false teeth are
missing. Can you figure out where chey are?” David Hoy, KCMO's
guest cxpert in extrasensory perception, needed only a few seconds
to decipher what he calls his ‘ESP flash’ ‘Your dog got them and put
them under the stove,’ he confidently informed her, ‘Would you go
