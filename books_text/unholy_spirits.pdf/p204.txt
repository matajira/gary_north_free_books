198 UNHOLY SPIRITS

In the spring of 1900, he developed a hoarse throat. It steadily
became a whisper. Out of desperation, he became a silent apprentice
to a photographer; he could no longer sell books and stationery sup-
plies. A travelling hypnotist visited town and heard about the young
man’s plight. He guaranteed to restore his voice for $200. When
hypnotized, his voice returned; as soon as he came out of the trance,
his whisper reappeared. After several attempts, the hypnotist gave
up. But another local amateur hypnotist and amateur osteopath, Al
Layne, remembered an incident from Cayce’s youth. He had been
hit on the head by a baseball. Dazed, the boy became feverish and
then delirious. Yet out of his delirium, he had told his parents to ap-
ply a poultice to his neck. They did, and he recovered quickly. He
had no memory of his diagnosis afterward, Layne put Cayce into a
trance and then asked him to diagnose his problem. Immediately,
the fateful words came forth: “Yes, we can see the body.”!! The voice
diagnosed the problem as insufficient circulation. Layne gave a sug-
gestion that the body cure itself. Cayce’s neck grew pink, then bright
red. Twenty minutes later, it became normal again. Layne told
Cayce to wake up, and when he did, his voice had returned,

This was the beginning, not only of Cayce’s diagnostic ministry,
but of a lifetime of trouble with his voice. His biographers seldom
refer to the fact that throughout the remainder of his life—45 years
— Cayce had recurring voice failures. He was completely dependent
upon his trance state and its circulation stimulation to return his
waking voice to normal.!? No one could give a physiological reason
for the loss of his voice. Those familiar with demon possession would
immediately recognize the cause: occult bondage. Cayce could not
abandon the physical “readings” once they had begun. He was trap-
ped.

Layne, the hypnotist, immediately saw possibilities for himself
and Cayce. He could put Cayce under, question him concerning the
ailments of others, get a prescription, and then mail the prescription
to the “patient.” Cayce was initially suspicious, both of his talent and
of Layne’s proposition. Layne asked him to diagnose his own illness,
and when he followed Cayce’s advice, he said that he felt much bet-
ter. Cayce gave in, but he refused to accept any payment for his ser-
vices. Layne, however, had no qualms about fees.

11. Tid., p. 12k.
12. Hugh Lynn Cayce, Venture Inward (New York: Harrow Books, [1964] 1972), p.
51.
