Escape from Creaturehood 345

One of the techniques used by Indian mystics to escape time's
bondage is an exploration by the mystic of his previous incarnations.
In doing so, one “burns up” his karmic residual. Buddha and his con-
temporary sages practiced this technique and recommended it. The
goal is to retrace time backwards to the point of time’s origin, “the
point where existence first ‘burst’ into the world and unleashed time.
Then one rejoins that paradoxical instant before which Time was
not, because nothing had been manifested.” Further, “to re-live one’s
past lives would also be to understand them and, to a certain degree,
‘burn up’ one’s ‘sins’; that is, the sum of the deeds done in the state of
ignorance and capitalised from one life to the next by the law of karma.
But there is something of even greater importance: one attains to the
beginning of Time and enters the Timeless—the eternal present
which preceded the temporal existence inaugurated by the ‘fall’ into
human existence. In other words, it is possible, starting from any
moment of temporal duration, to exhaust that duration by retracing
in course to the source and so to come out into the Timeless, into
eternity. But that is to transcend the human condition and to regain
the non-conditioned state, which preceded the fall into Time and the
wheel. of existences.”

Man’s goal, in this perspective, is to transcend all created limits.
The existence of such limits, manifested best by the existence of
time, is an intolerable burden. Men are to seek the pre-creation
state, which was non-conditioned. This can mean one of two states:
either total union with the metaphysical One or total and perfect spir-
itual autonomy, There have been yogis pursuing each of these goals:
the One (fusion with Oneness) or the Many (isolated autonomy).

The primary difference between the Indian view of time and the
view of other pagan cultures is that in Indian theory man plays no
part in the periodic re-creation of the world. The goal of ritual regen-
eration of the cosmos, such as we find in the seasonal fertility festi-
vals of most primitive cultures, is not pursued. Instead, the goal is
escape from the cosmic cycle.” But the means is the same in each
case: the attempted return to the Time before time.

The question of time is the question of mortality. Why do men
die? By reconstructing the creation events, men hope to explain how

35. Ibid. p. 12.

36. Eliade, Rites and Symbols of Initiation: The Mysteries of Birth and Rebirth (New
York: Harper Torchbooks, [1958] 1965), p. 107,

37. Eliade, Myth and Reality (New York: Harper Torchbooks, [1963] 1968), p. 62.
