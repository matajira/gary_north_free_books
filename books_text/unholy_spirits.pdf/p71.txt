The Biblical Framework of Interpretation 65

theorizing on the almost infinite extension of time in which men are
“imprisoned,” the theologians of karma think they have imprisoned
God and His judgment. The motive of all evolutionary thinking,
whether Darwinist or karmist, is the same: to escape the wrath of
God by pushing the creation and the judgment out of this world and
the next. It is also the motive of post-Kantian humanism, including
Hegelianism and Marxism,

Prohibitions on Occultism

Because men de not accept the validity of the preceding biblical
doctrines, they are susceptible to occult forces. The people of Israel
were protected from occultism by the revelation of God concerning
Himself and His relationship to the creation, as well as by His prom-
ise of ultimate restoration. But the doctrines alone were not sufficient
to preserve the people from the idolatry of the pagan cultures which
surrounded them, Explicit prohibitions were placed on citizens of
Israel against dabbling in the occult arts. These arts were well
known. The most comprehensive prohibition appears in Deuteron-
omy 18:10-12, but there are numerous other outright prohibitions
and warnings against the consequences of occult experimentation.

Witchcraft (sorcery) ~ Ex. 22:18
Necromancy-Spiritualism— Lev. 19:31, 20:6; Deut. 18:11
Astrology —Isa, 47:13
False prophecy
inaccurate — Deut. 18:20-22; cf. I John 4:1
idolatrous ~ Deut. 13:1-3
Divination ~ Deut, 18:10
arrows ~Ezk. 21:21
livers— Ezk. 21:21
images— Ezk. 21:21
Fire walking — Deut. 18:10
Omens ~ Jer. 10:2
Wizardry (secret knowledge?) — Deut. 18:1t
Charms (snakes) — Jer. 8:17
Enchantment (spells)— Isa. 47:9-12
Times (lucky days?)—Lev. 19:26

The term translated “witch” by the King James Version is more
accurately rendered “sorcerer.” The influence of Walt Disney's witch
