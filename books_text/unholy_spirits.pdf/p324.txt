318 UNHOLY SPIRITS

mated. Mrs. Fish’s identification resulted from a 1969 update of the
Gliese Catalogue of Nearby Stars, There was no possibility that anyone
on earth in 1964 could have known the exact location of these stars.”*

One question is never, ever raised by those who rely on hyp-
notism for background information: Is what the hypnotized person
is reporting based on his memory of an actual historical event, or is
another being passing along a story of his own making through the
mouth of a demonically possessed person? Could the incident have
been recreated mentally by outside sources—a kind of visionary
memory?75 And if it did take place as remembered, were these be-
ings what they claimed to be? In this case, however, there was this
verification: the Pease Air Force Base in New Hampshire had
detected the space vehicle on radar.76

As part of his investigation of the philosophy and ideas lying
behind the stories from the contactees, Vallee drove to Mendocino,
California to hear tapes that Timothy Leary had recorded in jail.
Leary, of course, was famous from the mid-1960’s onward for his
psychedelic philosophy of “turn on, tune in and drop out.” He was of
all the men of that six-year period the one who is most deserving of
the Pied Piper award for leading astray that generation. Vallee
listened to tapes Leary had recorded and summarizes Leary’s doc-
trines. “He stated that every living entity had a genetic purpose, and
that the problem before us now was that of the future of the human
race. He implied that Man was fast approaching the end of his rope,
that evolution was ready to make a new jump toward a higher form
of life, and that a superior intelligence had conceived the blueprint
for us on Earth. The central nervous system was its gift to us, a piece
of equipment to explore and use in order to establish communication
with our maker.””” By now you are familiar with this philosophy. It is
the philosophy above all other doctrines which links most of the
paranormal phenomena as well as the UFO phenomena. Men are
awaiting a leap of being to higher consciousness. This is the theme of both
occultism and New Age humanism.

Vallee also includes a chapter on the cattle mutilations that took
place in the late ’60s and early '70s and are still gomg on. A farmer

74. Clifford Wilson and John Weldon, Close Encounters, p. 96_

75. Wilson and Weldon do recognize the occult parallels with hypnotism (p. 124),
but they also have identified the occult nature of UFO phenomena.

76. Jacques Vallee, Passport to Mogonia, p, 90.

77. Messengers, p. 51.
