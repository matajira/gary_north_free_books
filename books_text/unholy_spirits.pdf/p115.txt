Paranormal Science Since 1960 109

exposed images superimposed on the negative. Perhaps most pecul-
jar of all are “ghost” images, which usually are small reproductions of
existing photographs of deceased subjects. Sometimes these images
are of famous persons, while other times they are photo reproduc-
tions of snapshots of dead relatives of the subject being photo-
graphed. These images can appear on films produced by a Polaroid
Land camera under test conditions. These weird images can also be
found on supposedly unexposed sheets of film that have never been
put into a camera. The psychic photographer can actually transfer
mental images to sealed, fresh film packs. One of the most proficient
of these medium-mentalists is John Myers.

John Myers is a wealthy industrialist who picked up his talent for
psychic photography after a visit with Emma Deane, the medium
whose work had been basic to Warrick’s Experiments in Psychics. He has
performed his feats of occult skill under numerous conditions, including
an appearance on the old PM Easi television show in 1961. Several of his
photographs are reprinted in Hans Holzer’s book, Psychic Photography
(1970). Packs of photographic paper are purchased by the experimenter
from any store. Myers concentrates his attention on the film for a few
minutes, announces that the test is finished, and the film is then devel-
oped. Symbols may appear, such as a cross or a tombstone; human
faces sometimes appear. Myers is unable to predict in advance just
what will appear, but he is aware when the psychic process is finished.

Another successful psychic photographer is Dr. Andrew Von Salza, a
West Coast physician. In March 1966, he visited the Holzers in New
York City, where he took a photograph of Mrs. Holzer, using a Polaroid
camera, model 103. Mrs, Holzer’s image appeared quite normally, but
next to her in the frame was a vague ghostly impression of a painting of
Russia's Catherine the Great, suspended in midair. When Holzer had a
reproduction of this picture made, it turned out poorly, Nevertheless,
he sent a copy to Von Salza. The latter immediately “felt led” to re-pho-
tograph the copy. This time he obtained a print with “Catherinc” quite
clear. He sent back two reproductions of this new picture to Holzer.

To check on this strange procedure, Holzer visited Von Salza in
May 1966. The physician repeated the process. He aimed his Polar-
oid camera at the copy Holzer had sent him, and out of his camera
popped another picture of Mrs. Holzer and “Catherine,” only this
time “Catherine” had extended her arm, as if she were offering a
crown to Mrs. Holzer. Unknown to Dr. Von Salza at the time, Mrs.
Holzer is a sixth-generation descendant of Catherine the Great. (The
“Catherine” in the photographs appears as a painting.)
