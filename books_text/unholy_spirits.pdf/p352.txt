346 UNHOLY SPIRITS

death came into the world. But the history of the creation is a sacred
history; the concern with these pre-time events is soteriological: men
want salvation from imperfection, a release from the bondage of
mortality. The concern with profane history is reduced accordingly.
Written records of the actions of actual men, apart from the tales of
heroes and demi-gods, are not crucially important. It was the com-
ing of Christianity, with its commitment to God’s actions in human
history, which brought the triumph of the book over the oral tradi-
tion, especially the secret oral tradition of the initiated. A people
without a written history is a people without a history today.* Only
by a lifetime of incomparable scholarly labor can a book like
Maenchen-Helfen’s study of the Huns be produced, and he never
did complete it in his lifetime. They left no written records; the
details concerning them must be derived from artistic fragments and
the records of those nations, predominantly Christian, that the Huns
invaded.

Astral Projection

One of the techniques used by adepts to escape the restraints of
time and space is astral projection. Castaneda said that he used such
techniques during his initiation. The book and movie, The Other, has
the young occultist-murderer develop his powers through this exer-
cise. Thelma Moss, the UCLA parapsychological researcher, de-
votes a chapter of her book, The Probability of the Impossible (1974), to
out-of-body experiences. But one of the most frightening cases took
place in June of 1975. A yoga instructor, Robert Antosczcyk, told his
associates that he was going into seclusion in his room to practice
astral projection, On June 3, the UPI story says, “His lifeless body
was found on the floor of the room, in a yoga position that is used for
deep meditation. He was flat on his back with his thumbs between
his index and middle fingers.” His mother claimed that he was in
perfect health and did not use drugs. Local yoga experts in Ann
Arbor, where the incident took place, asserted that such practices are
not recommended, As a revealing sidelight, the report said that he
taught his yoga classes at the local YMCA.

Astral projection and reincarnation are taught by the Edgar
Cayce movement — Cayce was an astral projectionist, or at least he
diagnosed people from great distances and described their surround-

38. Ibid., pp. 157-61.
