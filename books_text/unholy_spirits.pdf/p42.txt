36 UNHOLY SPIRITS

limit their questions to those that can be answered by the combina-
tion of sense data (experience) as interpreted by the universal, fixed
categories of human thought.

This Kantian humility is bogus. It is the humility of self-
proclaimed autonomous man. It is the humility of the man who
says: “Whe visible, scientific world is capable of being discovered and
molded by man’s mind and man’s activities as a rational species, and
nothing that man is incapable of understanding scientifically is rele-
vant in the external world.” But if this is true, ¢hen how can man’s mor-
ality be autonomous from scientific cause and effect, and yet also be connected to
the vealn of rational human action? In other words, how can man’s per-
sonality be preserved in a world of strict cause and effect? What hap-
pens to human freedom in such a world?

Rationalism vs. Irrationalism

The foundation of regularities of nature is not inherent in nature
itself, Kant argued. It is in the ¢ prior! categories of the human mind.
Nothing can be known without sense data, but without the categor-
ies of human thought to assemble the data into coherent wholes, ex-
perience tells us nothing. There is therefore no way of knowing any-
thing beyond our senses as categorized and ordered by the concepts of the
autonomous mind. “Thus the order and regularity in the appear-
ances, which we entitle nature, we ourselves introduce. We could
never find them in appearances, had not we ourselves, or the nature
of our mind, originally set them there.” ‘The human understand-
ing, he concluded, is “the faculty of rules.”

It is therefore man’s autonomous mind which creates that entity
which we know as nature. The “stuff out there,” that is, the so-called
“things in themselves,” must be forever unknown and unknowable to
us. We can only know nature through our own a priori categories of
reason, Man’s mind legislates the laws of nature! “Thus the understand-
ing is something more than a power of formulating rules through
comparison of appearances; it is itself the lawgiver of nature. Save
through it, nature, that is, synthetic unity of the manifold of appear-
ances according to rules, would not exist at all. . . .”87 Man is the ultimate
creator of his own reality. Man is autonomous. In this there is certainty of
knowledge. This is the so-called “humility” of Kantian philosophy.

 

35, Ibid, A125, p. 147
36. Ibid., A126, p. 147.
37, Ibid., A 126-27, p. 148.
