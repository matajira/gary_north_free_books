Demonic Healing 235

scissors and knives. He selected a penknife and made an incision, Both
Cruz and his father-in-law knew that it was impossible to cut into the liver
without massive hemorrhaging, and neither could explain why they permit-
ted this to be done, or why they stood by passively as Arigo cut into the
patient with an unsterilized knife and no anesthesia. Perhaps, they thought
later, it was because this was the last chance: all else had been given up.

They watched for the biood to spurt out, but only a thin tickle slid from
the sides of the wound. Then, Cruz claimed, an even stranger thing hap-
pened. Arigo inserted the scissors deep into the wound, removed his hand,
and the scissors seemed to move by themselves. Cruz turned to look at his
father-in-law, who nodded and exchanged glances. Later, they were to com-
pare notes and confirm, at least to themselves, what they had seen. In mo-
ments, Atigo removed the scissors, reached into the wound, and pulled out a
tumorous growth. With a showmanlike flourish, he slapped the tumor into
Cruz’s hand, Then he took the cotton and wiped it along the incision, When
he was finished, the edges of the wound adhered together without stitches and
Arigo momentarily placed a crucifix on it. Then he told Sonja to rise, which
she was able to do. She was weak and shaky, but felt no pain.!”

The biopsy of the tumor confirmed that the growth was cancer.
The liver regenerated itself—it is the one major organ that can do
this—and the woman lived. Neither Cruz nor his physician father-
in-law had any rational explanation for what they had seen. Obvi-
ously not; there is no rational explanation, i.e., an explanation in
terms of laws known to men or conceivably knowable to men. What
they had seen was a denial of what Weber described as Western ra-
tionalization. It could not happen, rationally speaking. Only it did
happen.

Prominent patients now joined the long lines of hopeful visitors.
They waited for hours in line; it was first come, first served. (I won-
der if “patient” refers to what people have to be in order get in to a
physician’s inner offices. No other profession has patients rather
than clients —only those in the healing profession.) President Kubit-
schek, the builder of that majestic political pyramid, Brasilia, sent
both his daughter and his pilot to visit Arigo, and both were cured.
The head of his security police also was treated, and if Arigo was a
fraud, this would have been a predictably harsh culmination of his
career. Investigating physicians, who were willing to put their repu-
tations on the line, publicly announced that the operations, while
unexplainable, were successful: few scars, tissues removed skillfully,

17, fbid., p. 82.
