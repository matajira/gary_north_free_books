192 UNHOLY SPIRITS

prevent a new fusion: messianic humanism and the occult powers of
the psychic? Is it so surprising to find that the Soviet Union should
be experimenting with psychics in order to find the “laws” that un-
dergird these most disturbing, yet most promising, impossible phe-
nomena? The messianic state seeks power, and psychics possess
power—the power of occult knowledge. The opposition of the mod-
ern humanistic culture to occult power has been methodological, not
moral. When the methodology shifts, as it has clearly begun to shift,
what moral restraint will be present to call a halt to mind readers,
mind controllers, and humanistic prophets of the coming one-world
order?
