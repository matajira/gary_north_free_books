Paychics 185

Undoubtedly, Mrs. Dixon has performed some important ser-
vices for people. She calls ambulances for people who have not yet
had a heart attack but who will in a few minutes. She tells people to
see a doctor quickly because they have a serious and previously un-
diagnosed disease. She keeps people from flying on airplanes that
are about to crash. She warns people about business deals that are
not going to work out. The book is filled with first-hand testimonials
of the beneficiaries of such prophecies. All in all, it makes psychic
powers look like a true gift from God, just as Croiset and Mrs. Dix-
on maintain. Why object to prophecies of this nature?

The biblical quote which introduces this chapter provides one
answer. It is not simply the accuracy of the prophecies concerning
the events which is crucial; it is the philosophy and theology that un-
dergirds the prophecies. Furthermore, the apparent benefits con-
firmed on both the prophet and the recipients of the advance infor-
mation (providing that the recipient does exactly what the prophet has
instructed) almost guarantees that some readers will imitate the
techniques of the prophet, and that others will somehow feel cheated
that no such gift has been bestowed on them. The market for materi-
als about or by Mrs. Dixon indicates just how curious the public is.
The sale, for example, of the Dell Purse Book titles that deat with the
development of prophetic skills indicates how widespread the imita-
tion factor is. The Book of Omens. Your Guide to Good Luck; Numerology,
Palmistry; Develop Your ESP: the titles, as they say, “tell all.”

Mrs. Dixon was first informed of her powers by a gypsy. It
sounds like a plot in some Hollywood B-grade film of the 1940's, but
she insists that it is the truth. When she was eight years old, her
mother took her to see a gypsy woman in northern California, close
to their home, and the woman said that the young girl had a unique
palm. (Eastern mystic types apparently have said the same thing, for
what it is worth.) The woman told them that when the girl grew up,
she would have special powers. Then she went into her covered
wagon home and produced a crystal ball, which she gave to young
Jeane Pinckert. (Her brother is Ernie Pinckert, who became an
All-American football player.) Almost immediately, she received
visual messages in it, including the fact that the old gypsy would
scald her hand in a cooking accident. Sure enough, it happened.
(Crystal balls in the hands of psychics seem to be good for a long life
of “I told you so’s,” not generally regarded as a sure way to win
friends and influence people, as Cassandra's biographer, a Greek by
