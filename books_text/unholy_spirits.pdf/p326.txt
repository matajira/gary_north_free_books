320 UNHOLY SPIRITS

rational. He points to a natural phenomenon, but one that is not yet
understood by conventional or even unconventional physical theory.

The most useful chapter in the book is the chapter from which I
have been quoting, “The Stratagem Theories.” He has a section
called, “The Six Social Gonsequences.” These are very, very impor-
tant. He lists the first consequence: the belief in UFO's widens the
gap between the public and scientific institutions. The second is that
contactee propaganda undermines the image of human beings as
masters of their own destiny. Third, increased attention given to
UFO activity promotes the concept of political unification of this
planet. Fourth, contactee organizations may become the basis of a
new high demand religion. Fifth, irrational motivations based on
faith are spreading hand-in-hand with beliefs in extraterrestrial inter-
vention. Sixth, contactee philosophies often include belief in higher
races and in totalitarian systems that would eliminate democracy.

We can see in this summary that Vallee, as a representative of
the older humanism, finds that the new religion of UFO's challenges
the basic world-and-life view of conventional democratic social
theory. He does not like the implications of this new philosophy, but
he recognizes that they exist. He is like a man who is defending a
sinking ship. Worse; his own books are like torpedoes that are wid-
ening the holes in his own ship.

The Breakdown of the “Treaty”

‘To return once again to Van Til’s analogy, there is a secret treaty
between the humanist’s two worlds, the phenomenal realm of science
and measurement on one side, and the noumenal realm of personal-
ity, mystery, the unmeasurable, the irrational, the ethical, the theo-
logical, and the “things in themselves” on the other side. (The ques-
tion to ask is: “The other side of what?”) As Van Til writes: “Irration-
alism has a secret treaty with rationalism by which the former cedes
to the latter so much of its territory as the latter can at any given time
find the forces to controi.”® That which is unknown must remain in
the shadows of external impotence. No invasion from the beyond —
the noumenal—can be tolerated by the scientific, logical mind.
Every effect has a cause. Every cause must be in principle knowable,
even though not yet known.

To repeat: Max Weber, the great German social scientist, dealt

84, Van Til, ‘The Defense of the Faith (Jnd ed.; Philadelphia: Presbyterian & Re-
formed, 1963), p. 525.
