304 UNHOLY SPIRITS

that are devoted to the topic, and by scientific panels. Again and
again, the evidence for them is scientifically significant. Not so
strong as the evidence for a scientifically repeatable experiment, but
strong in the sense that no explanation which is given by the profes-
sionals in terms of Newtonian science makes any sense.

In this sense, Unidentified Flying Objects are a manifestation of
occult “unknown”) power which can be taken somewhat seriously by
scientists, reporters, and rationalists generally. People who have no
particular theological beliefs, people who do not espouse the doc-
trines of New Age humanism, or ancient mysticism, nevertheless
can be persuaded that there are such phenomena as flying saucers.
‘They are seen as phenomena, not noumena. They can be explained
as technological wonders from beings who have advanced technolog-
ically beyond mankind.

The basis of this faith in the technological reality of flying saucers
is that there can be other beings outside of this world who are never-
theless limited by the natural laws and scientific laws of this world. It
is assumed, in other words, that creatures beyond earth's at-
mosphere somehow have gained access to certain kinds of knowledge
that enable them to visit the earth. This sort of knowledge, however,
is knowledge which in principle can be understood by scientific man.
They are better scientists than we are, but they are nat occult magi-
cians. They are better builders than we are today, but given enough
money and enough formulas and enough skills, General Motors in
principle will be able to produce interstellar or intergalactic or at
least interplanctary spaceships. So the scientists who are interested
in UFO's are willing to discuss the topic and study the topic, but
only under the standard Newtonian world view. They are willing to
discuss such phenomena only because they are believed to be phe-
nomena, that is measurable, scientific, and repeatable.

This may distinguish the average investigator of UFO's from the
average investigator of other kinds of paranormal phenomena. Then
again, it may not. It is my guess that the average man on the street
would be more willing to admit to a television audience that he be-
lieves in the existence of UFO's from outer space than he would be to
admit that he believes in haunted houses. I am not arguing that ser-
ious scientific investigators of the other sorts of unexplained phe-
nomena, or parapsychological and paranormal phenomena, are not
serious scientists. Some of them are. We know of investigators who
are clearly interested in such phenomena only because they believe
