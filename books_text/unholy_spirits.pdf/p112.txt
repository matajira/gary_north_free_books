106 UNHOLY SPIRITS

toward onc plant and no thoughts toward the other. One was told
mentally that it would live; the other was ignored. Within a month,
the ignored plant was dying. Vogel duplicated this feat with plants of
his own. By hooking up philodendrons to various machines, he
found that he could produce responses on the machine’s recording
devices—“in the plant” supposedly —by directing friendly thoughts
toward them.

Vogel had discovered early in his career that by “relaxing his
mind,” he could visualize activities in the behavior of liquid crystals
under a microscope that his professional colleagues could not see,
He refers to this phenomenon as his “higher sensory awareness.”?5
He had trained himself in this state of higher consciousness. Later he
became aware of his own unique position in his experiments with
plants. Again and again, manifestations of paranormal science are
found to be intimately linked to the experimenter—so strongly
linked, in fact, that the odd phenomena do not occur when he is not
present. As the authors state: “It became clearer to Vogel that a cer-
tain focused state of consciousness on his part seemed to become an
integral and balancing part of the circuitry required to monitor his
plants. A plant could be awakened from somnolence to sensitivity by
his giving up his normally conscious state and focusing a seemingly
extra-conscious part of his mind on the exact notion that the plant be
happy and feel loved, that it be blessed with healthy growth. In this
way, man and plant seemed to interact, and, as a unit, pick up sen-
sations from events, or third parties, which became recordable
through the plant... . Asked to describe the process in detail,
Vogel said that first he quiets the sensory responses of his body or-
gans, then he becomes aware of an energetic relationship between
the plant and himself. When a state of balance between the bioelec-
trical potential of both the plant and himself is achieved, the plant is
no longer sensitive to noise, temperature, the normal clectrical fields
surrounding it, or other planis. It responds only to Vogel, who has
effectively tuned himself to it— or perhaps simply hypnotizes it.”’6

Problem: How is anything —cven a plant —“simply hypnotized”?
Just because the phenomenon of hypnosis is so widespread, scie
tists and laymen alike tend to dismiss it as another of those daily mir-
acles associated with the mind of man. But to appeal to hypnosis as
an explanation for paranormal phenomena is to evade the question

 

75. Ibid., p. 35.
76. Ibid., pp. 37-38.
