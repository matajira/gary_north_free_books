Invaders from... ? 291

all this has taken place in less than three decades, since that first
great wave of publicity in June of 1947.

John Keel, a private reporter who began to collect information
on UFO's in the mid-1960’s, says that in 1966, he received from his
newspaper clipping service over 10,000 clippings and reports of
UFO sightings. This was in contrast to the Air Force's reported 1,060
sightings.? There is no doubt that UFO phenomena became wide-
spread after 1965.

In the first edition of this book, which was published almost ex-
actly at the time that Carter went public with his story of the UFO, I
included no chapter on Unidentified Flying Objects. There were
reasons for this. The literature on UFO's was very large and contin-
ues to grow. Furthermore, there are so many conflicting stories
about what UFO's are or could be, and so many different opinions
and interpretations regarding the reliability of the sightings, that I
decided that it would be best not to open that particular can of
worms. The topic deserved a whole book, perhaps, but a chapter
seemed to be far too short to contain the material necessary for a full
understanding of the topic. Had I been aware of John Keel’s intrigu-
ing books, especially UFO%-: Operation Trojan Horse (1970), I probably
would have written this chapter earlier. I agree entirely with his con-
clusion: “The UFOs do not seem to exist as tangible, manufactured
objects. They do not conform to the accepted natural laws of our envi-
ronment, .. . The UFO manifestations seem to be, by and large,
merely minor variations of the age-old demonological phenomenon.”

On the other hand, the thrust of this book is to demonstrate that
there are certain continuing themes in occult literature. We find
these same themes being repeated again and again in the literature
produced by those who say they have seen Unidentified Flying Ob-
jects, and more to the point, by those who claim that they have been
visited by beings from outer space. It then becomes mandatory that I
devote some attention to the topic.

Familiar Occult Themes

What are some of these themes? The first and most important is
the theme of the evolution of man. Man is perceived as a being who
is presently limited in time and space, but who is capable of achiev-
ing a leap of being and evolving into a much more powerful being in

8. Keel, Trojan Horse, p. 18
9. Ibid., p. 299.
