12 UNHOLY SPIRITS

Simultaneously, the rise of the New Christian Right since 1979
has astounded political commentators. A major shift in the thinking
of evangelicals is in progress. Younger charismatics and most of the
independent Christian day schools are headed toward biblical law
and away from the social and political policies of inaction that have
been common in traditional, pietistic, dispensational circles since
1925. They are picketing against abortion clinics (legalized in 1973
by the U.S. Supreme Court, but not by God’s supreme court), They
are adopting ethics religion and abandoning the older escapist relig-
ion, The key word in this shift of perspective is “dominion.” The sec-
ondary word is “resistance.” Resistance to what? Secular humanism
and its legal arm, the Federal government.

In contrast, Billy Graham, Wheaton College, Calvin College,
Christianity Today, neo-evangelicalism, and the evangelical and Re-
formed seminaries seem to be headed toward liberation theology (an
exception: Reformed Episcopal Seminary in Philadelphia), Old-line
dispensational seminaries are desperately trying to stay out of the
fight, and in doing so, they are becoming increasingly irrelevant.
This split is affecting numerous old-line conservative denominations
and para-church organizations. A Kuhn-like paradigm shift is
underway. The older foundations are broken; new ones are being
poured. New men are pouring them. It is a case of new wine in old
wineskins. The old wineskins are screaming “foul!” as they feel the
pressure from the bubbling liquid, but to no avail. They are about to
burst.

Older evangelical intellectuals are now faced with what they re-
gard as a dismaying choice: to line up with the practical prescriptions
of the theocratic conservatives in the Christian Reconstruction move-
ment, or to side with the socialistic liberation theologians, who are
presently visibly pacifists but who threaten to become revolutionaries
(as their spiritual forebears, the Anabaptists did, 1525-35). Either the
new Puritans or the new Anabaptists. Either David Chilton? or
Ronald Sider.*® Either pro-legalized life or pro-legalized abortion.

The flagship of middle-aged Christian scholarship’s fleet, the
Neutrality, has been hit by several torpedoes, and is taking on water

9. David Ghilton, Productive Christians in an Age of Guilt-Manipulators: A Biblical Re-
sponse to Ronald J. Side (3rd ed.; Tyler, Texas: Institute for Christian Economics,
1985).

10. Ronald J. Sider, Rick Christians in an Age of Hunger: A Biblical Study (2nd ed.;
Downers Grove, Illinois: Inter-Varsity Press, 1984).
