70 UNHOLY SPIRITS

claimed to be God (3:14; cf. Ex. 23:21). Christ specifically stated that
“No man hath seen God at any time; the only begotten Son, which is
in the bosom of the Father, he hath declared him’ (John 1:18). These
theophanies would seem to have been manifestations of the Second
Person of the Trinity — not Jesus Christ as the incarnate Son, but pre-
incarnate revelations. The importance that the writer of the Book of
Hebrews places on Melchisedec, who was both priest and king of
Salem (Gen. 14:18), and who was without “father, without mother,
without descent, having neither beginning of days, nor end of life;
but made like unto the Son of God; abideth a priest continually”
(Heb. 7:3), should indicate that Melchisedec was a theophany of
God, The centrality of the themes of priesthood and sacrifice in the
Book of Hebrews—Christ’s sacrifice as the sacrifice that satisfies
God's standards of justice —leaves no doubt as to the importance of
Melchisedec for Christian theology. He was no mere man, No ordin-
ary angel is ever compared to the Son of God, either.

The function of angels is multiple. They are messengers of God,
although it is unclear how this task operates in post-Apostolic times.
They are eventually to serve as agents of divine judgment (Matt.
13:41-42; Rev. 6-10). Despite the fact that, in terms of power, men are
“a little lower than the angels” (Ps. 8:5), men are nevertheless to serve
someday as judges of the angels (I Cor. 6:3). As creatures, they are fel-
lowservants of God with men (Rev. 19:10), and therefore they are not to
be worshipped (Col. 2:18). It should be clear that since the closing of the
biblical canon, angelic demonstrations of power have either been sub-
dued or else attributed by men to other factors, either natural law or the
direct intervention of God. Demons, frankly, make better copy.

Demons are fallen angels, both ethically and geographically.
Christ announced: “I beheld Satan as lightning fall from heaven”
(Luke 10:18), The twelfth chapter of Revelation, which is not so diffi-
cult a passage as some expositors would make it, affirms that a
“dragon” stood before a “woman” about to give birth “for to devour
her child as soon as it was born. And she brought forth a man child,
who was to rule ali nations with a rod of iron: and her child was
caught up unto God, and to his throne” (Rev. 12:4b-5). There should
be no doubt concerning the identity of a child who was caught up to
heaven and to God's throne.

‘And there was war in heaven: Michael and his angels fought against the
dragon; and the dragon fought and his angels, And prevailed not; neither
