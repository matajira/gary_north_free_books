246 UNHOLY SPIRITS

millan, into dropping his books. ‘These books did not get back into
print until 1965—in the midst of the occult revival. Unlike Arigo’s
knives in the cyc, the knives of the scientific guild, delivered very
often from behind, can maim and kill a man’s reputation, sometimes
affecting his ability to get an academic job, especially if he is not yet
tenured.

It was Arigo’s dream to establish a hospital. This had been Edgar
Cayce’s dream, too, This was why he had hope in the investigations
of these North American scientists. He did not understand that they
were basically pariahs in their profession for even daring to promote
his story back home, When the team returned in 1968 to make more
films, gather more statistics, and play the data game once again, it
was driven out by the press, which had received reports on the clan-
destine visit of the North Americans. Nothing fills a paranormal
scicntists’ heart with greater fear than the attention of journalists
prior to the full and final report, written in the discipline’s jargon,
addressed to one’s skeptical colleagues. In an academic world where
the mere writing of a textbook may tarnish a man’s reputation, as
Kuhn reveals, a summary by a journalist of a “major breakthrough”
is nothing short of suicidal.3* The team left when the reporters ap-
peared. Arigo was not going to get his hospital.

Why did he want a hospital? This is the anomaly, What would a
hospital have provided for him? He could treat 300 patients a day.
No one else in the world had his talents, so there was no possibility of
the division of labor, which is one of the primary functions of a hos-
pital’s medical staff. He could perform feats of surgery that no medi-
cal team could imitate. His powers were vastly beyond those ever
dreamed of by a medical technician. Indeed, the very nature of his
ministry was a denial of the need for medical technicians. What need
was there for a hospital? What need did a demon-possessed peasant
who cured men with a rusty knife in a filthy room have for a hospital
—the product of centuries of slowly improving medical techniques?
What need did he have for sanitary conditions, teams of researchers,
diagnostic equipment, anesthetics, or any other product of Western
technology and theoretical science?

This is the central secia/ implication of the conflict between West-
ern science and primitive healing. It is not that Western techniques,
being rational, arc therefore the only successful avenues of medical
treatment. That may be what the orthodox scientists think, but what

34. Kuhn, Scientific Revolutions, p. 21
