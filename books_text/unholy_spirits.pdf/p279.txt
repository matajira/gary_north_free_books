MAGIC, ENVY, AND FOREIGN AID

But it shall come to pass, if thou wilt not hearken unto the voice of the
LORD thy God, to observe to do all his commandments and statutes
which I command thee this day: that all these curses shall come upon thee,

and overtake thee.
Deuteronomy 28:15

One of the characteristic features of the Christian West is its
commitment to the concept of linear history. Westerners seldom give
the concept much thought. ‘They assume, almost automatically, that
no other idea of history could be held by anyone. But the concept of
linear history —a unique past, present, and future —is very much the
product of religious faith. It was the Hebrew concept of the creation
as an unrepeatable event that gave the West its belief in historical
time. By affirming the doctrine of creation, Christianity transformed
the West — indeed, created the West. It is quite proper, therefore, for
Charles N. Cochrane to end his book Christzanity and Classical Culture
(1940) with a chapter on Augustine. It was Augustine’s formulation
of linear history in The City of God that served to transform the think-
ing of Western scholars. The concept of cyclical history, held by clas-
sical culture and the ancieni pagan kingdoms, was overturned by
Augustine.! His words served as a kind of intellectual touchstone:
“The education of the human race, represented by the people of
God, has advanced, like that of an individual, through certain
epochs, or, as il were, ages, so that it might gradually rise from earthly
to heavenly things, and from the visible te the invisible.”?

The source of the idea of progress is Christianity. The pagan con-
cept of historical development, with its inevitable cycles of growth
and decay, did not leave oom for long-term progress. But Christian-

i. Augustine, The City of God, XAI:I7.
2, tid, X14.

273
