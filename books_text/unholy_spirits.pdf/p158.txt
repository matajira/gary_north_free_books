152 UNHOLY SPIRITS

world only in a manner of speaking. It cannot create or change any-
thing, and yet it makes the world because its function is to judge and
assess, and witness. I say that the tonal makes the world because it
witnesses and assesses it according to fona/ rules. In a very strange
manner the onal is a creator that doesn’t create a thing. In other
words, the tonal makes up the rules by which it apprehends the
world. So, in a manner of speaking, it creates the world.”9*

Kant never said it any better; in fact, he never said it as well.
There is no world-in-itself but only a description of the world that
men learn to visualize and take for granted. The only judgment
that matters to man is man’s own judgment. This sounds very reas-
suring, except for one minor point. What are all those shadows in
the night, and why do they seek to kill unwary men?

Timeless Dreaming

For the sorcerer, dreams are a source of power. For the
sorcerer’s apprentice, they are a means of internal discipline in the
initiation process. “Think of it as something entertaining. Imagine
all the inconceivable things you could accomplish. A man’s hunting
power has almost no limits in his dreaming.””” Don Genaro, the sor-
cerer who served as Castaneda’s “benefactor” (don Juan was only his
instructor), later told Castaneda that “the double begins in dream-
ing." This is the means by which the self manifests itself as the
nagual. As the discipline continues, this second personality can take
over, the Indians believe, thereby dreaming the self. It becomes im-
possible to know for sure at any given time whether one is perceiving
the world as the nagual or the tonal. Sometimes a man dreams his
double, and sometimes the double dreams the man.” “No one
knows how it happens,” reveals don Juan. Truer words were never
spoken. Nevertheless, the escape of the nagua/ as a second being is
made possible by these special dreams.

This is the doctrine of “magical time.” It is the nagual’s time:
beyond time, independent of time. When in the tonal’s time, men are
not to be irrational; when in the zagua/’s time, men are not to be ra-

94, Ihid., p. 125.

95. Ibid. p. 123,

96. Ixtlan, p. 91.

97. Ibid, p. 98.

98. Tales of Power, p. 67.
99. Ibid. pp. 81-82
100. Zxtlan, p. 143.
