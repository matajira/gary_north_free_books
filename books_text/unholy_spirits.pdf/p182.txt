176 UNHOLY SPIRITS

Croiset is perhaps the most famous psychic in Western Europe.
Television specials have been made about him. One was shown in
the United States in the mid-1960’s. (I remember watching it.) Most
Hollanders seem to have heard about him. He performs services for
detectives in solving crimes, especially crimes involving missing chil-
dren. Less well known is the fact that he performs psychic healings
for a fee.!® He is a gifted psychometrist, i-e., he works with objects that
belong or did belong to an individual about whom he is giving the
reading. (That other famous Dutch psychic, Peter Hurkos, is also a
psychometrist.) He says that he “sees” pictures associated with the
owner of the object— sometimes incredibly detailed pictures.

In April of 1963, a young boy, Wimpji Slee, disappeared. The
boy’s uncle contacted Croiset. Croiset told him that the boy had
drowned in a particular canal. Dragging the canal for the body
proved useless, however. Four days later, the body was found float-
ing in the canal at precisely the spot designated by Groiset.‘7

A similar case had taken place in May of 1960. Another young
boy, Toontje Thooner, had been missing for several days when a
local resident contacted Croiset. Prof. Tenhaeff happened to be with
Croiset at the time, and he switched on the tape recorder (which he
begs Croiset to usc in every case, but which Croiset uses only spo-
radically), recording the conversation on Croisct’s end of the phone.
Croiset’s biographer reports on the contents of that tape:

Is the playground where the child was last seen in a new suburb? [ ¥es.]
When you leave it at the left, is there some open ground? | ¥s.] When you
follow the border of this open ground, do you reach a canal? | Yes.) Is this
canal 200 to 500 metres from the playground? [Yes] Croiset paused, and
then inquired, “Are you a member of the child's family?”

“No, I'm a neighbour,” was the reply.

“Well,” sighed the sensitive, “I can then tell you that the outlook isn’t
good. In about three days, the child’s body will be found in the canal I men-
tioned, close to a bridge and near a bucket of zinc.”

Three days later I checked up. Unhappily enough, the police of Eind-
hoven had just found the child’s body next to one of the piers af the bridge
over the canal near a bucket of zinc. . . .18

16. Norma Lee Browning, The Psychic World of Peter Hurkos (Garden City, New
York: Doubleday, 1970), pp. 190-91.

17, Pollack, Croiset, p. 99

18. Tbid., p. 36
