172 UNHOLY SPIRITS

basic types of supernaturalist responses to the world: one type ac-
cepts things as they are (orthodoxy, in his terminology), and the
other seven are concerned with changing the world. His catalogue:
conversionist, revolutionist, introverstonist, manipulationist, thau-
maturgical, reformist, and reconstructionist."" (As reasonably
close-fitting examples of each, we could list: fundamentalist, Marx-
ist, Mennonite, science of mind, primitive magic, social gospel, and
Puritan.) Hoy is obviously a manipulationist.

Hoy’s approach is very much like the approach of “Rev. Ike,” the
former fundamentalist black preacher who by 1975 had become a
science of mind, “God-is-within-you” proponent. He worked the ma-
nipulationist psychology (or metapsychology) into the language of
the ghetto. He drove a $90,000 pink Rolls-Royce in 1975, and en-
couraged his followers to do the same. His philosophy is straightfor-
ward: “We are not interested in Pie in the Sky bye and bye. We want
our pie now, with ice cream on it, and a cherry on top.”!? Anyone
who has ever seen Rev. Ike on television will instantly recognize
Hoy’s technique, outlined in Lesson Four of Hoy’s Roffler products
training manual: the hair stylist (read: expensive barber) is supposed
to feel the prediction of increased sales . . . the touch of crisp new
dollar bills in his hands . . , the motion of the new car he will be
driving . . . his sense of personal greatness. Napoleon Hill’s Think
and Graw Rich is a similar handbook; so are the writings of the multi-
millionaire insurance entrepreneur, W. Clement Stone.

The difference between David Hoy and the other manipulation-
ists is that he frankly acknowledges that the name of this game is
mind over mind. He does not appeal to gods within us; he does not
use the jargon of the psychologist; he does not propose this system of
salesmanship as a program of self-transcendence. He simply says
that with sufficient concentration, the person who has developed
these powers can get other people to do what he wants them to do.
Hoy realizes the danger of sinister men who would make use of these
techniques, but he is convinced that these powers are in everyone
anyway, and sooner or later the word will get out.

Remember that bit of historical paradox that was repeated every-
where in 1964: Lincoln was elected in 1860; Kennedy was elected in

11. Bryan R. Wilson, Magic and the Millennium (London: Hineman, 1971), pp.
21-26.

12. William C. Martin, “Chis Man Says He's the Divine Sweetheart of the
Universe,” Esquire (June 1974).
