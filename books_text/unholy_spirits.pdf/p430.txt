424

Swedenborg, Emanuel, 396
syncretism, 30

Tarcher Books, 99, 123
Tate, Sharon, 7
technique, 339
technology, 296-98, 306, 325-26, 347
Teilhard de Chardin, 60, 337-38, 340
telekenesis, 88-89
telealogy, 165
telepathy, 90
teleportation, 143, 160
Tenhaeff, W. H. C., 174
Terrence, 331
Terte, Eleuterio, 250, 254-55
tests, 123-24
Tet Offensive, 5
theocentric, 58
theology, 31
theophany, 69
The Other, 98, 128, 177-78, 346
Thoburn, Robert, 393n
Thule, 289
Tibet, 271-72, 357
Tilton, Robert, 388, 392
time
abolition of, 153, 155, 276, 343-46
action, 386-87
alchemy, 337
before time, 345
burdens, 344
chance, 40
escape from, 276, 344, 345
“last times,” 386-87, 388-93
linear, 164, 273, 275, 283, 370
magical, 152-53
plenty, 352-53
Reformation, 26-27
Satan vs., 71
saving, 347
sorcery vs. 153
timelessness, 136-37, 148, 152-55, 276,
345
tingling, 97-98, 233, 256
tabiscope, 100
Toffler, Alvin, 366
tonal, 149-50, 151-52

UNHOLY SPIRITS

tongues, 381
tooth-pulling, 262
torture, 281
Tower of Babel, 145
train accidents, 117
trances, 193, 198-99, 206, 215, 223,
231, 250, 256, 317
transcendence.
divinization, 335
occult, 64, 85
politics, 363-64, 377
sorcery, 153
sec also self-transcendence
Transcendental Meditation, 350
transmutation, 340, 342
“treaty,” 31, 40-41, 41, 44, 45,
320-21, 322
Truman, Harry, 187-88
trust, 285
truth, 140, 224
Turin Shroud, 110

UCLA, 4, 125, 160, 257, 346
UFO's
ancient, 293
atomic age, 296
blackboard, 323
Carter, 288
close encounters, 316
conditioning, 315-16
Condon Committee, 303, 311-14
consciousness, 326
demonic, 291
designs, 299-300
dividing, 300
divinization, 326
dreams, 325
evolution, 305-6
fairy tales, 315
final judgment, 327-28
“flaps,” 293, 295-96, 298
healings, 325
hypnosis, 317
leaks, 299
magic, 314
malevolent, 325-26
measurable, 303
