8 UNHOLY SPIRITS

culture, It was peace and love everywhere, and drugs and fornica-
tion: “Woodstock Nation,” Mr. Yippie himself, Abbie Hoffman, later
dubbed it— Abbie, of Chicago Seven fame, of Steal This Book fame,
and much later, of drug pushing fame. On December 1, the story hit
the papers: a jailed follower of Charles Manson’s “dune buggy army”
had admitted that she and others in the Manson clan had committed
the Tate and LaBianca murders, The case was virtually solved, an-
nounced the Los Angeles Chief of Police, though he did not mention
Manson that day. More of the gory details came out at the Decem-
ber 5 grand jury investigation, and were leaked to the press in time
for the afternoon editions. The whole surrealistic story came out in
the months that followed. Charles Manson, who was the friend of
rock stars (including, as it later was revealed, several of the Beach
Boys, whe had actually recorded one of his songs), turned out to be a
vicious con artist who used drugs, mysticism, occultism, Beatles’
lyrics, Hermann Hesse’s pop classic in Eastern mysticism, Siddhar-
tha, sexual debauchery, and other mind-altering techniques to cre-
ate a dedicated little band of revolutionarics and murderers. The
counter-culture began to look fearful to millions of people. The “let’s
all get high and read tarot cards” mystique had begun to fade.
Things were going too far.

On December 6, the day after the grand jury hearings on Man-
son, the ill-fated, free-of-charge Rolling Stones rock concert took
place at the Altamont Speedway near San Francisco, with 300,000
in attendance. “Woodstock West,” it had been heralded. Someone
(no one remembered afterward just who) had invited the Hell’s
Angels motorcycle club to serve as bodyguards. Throughout the
day’s performances by popular rock groups — Santana, Jefferson Air-
plane, etc, —the Angels’ violence had intensified. Then, during the
Stone’s evening performance, a Hell’s Angel killed a black man who
had pulled a gun. It happened just after the Stones had begun to sing
“Sympathy for the Devil.” There had been a brief flurry of violence
just as they started to sing. Mick Jagger stopped singing and tried to
calm the crowd, remarking on-stage: “We always have something
very funny happen when we start that number... .”6 Funny
peculiar, not funny amusing. Moments later, death intervened. The

 

 

5, Bd Sanders, The Family: Ti
(New York: Dutton, 1971), ‘p. 106.

6. Philip Norman, Symphony for the Devil: The Rolling Stones Story (New York: Lin-
den Press/Simon & Schuster, 1984), p. 333.

Stary of Charles Manson’s Dune Bugey Attack Battalion
