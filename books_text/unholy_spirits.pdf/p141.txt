The World of a Sorcerer 135

parties, and the man who uses it must lead a hard, quiet life. The
gift of the smoke is a new vision of the world, or what the gnostics of
pre-medieval days would have called a new gnosis. This was the
quest of Renaissance mystics, alchemists, magicians, and illuminati.2!
“Everything is terrifying and confusing at the outset, but every new
puff makes things more precise. And suddenly the world opens up
anew! Unimaginable! When this happens the smoke has become
one’s ally and will resolve any question by allowing one to enter into
inconceivable worlds.”

On the one hand, ritual observance is crucial. The general prin-
ciple is as follows: “. . . we must follow certain steps, because it is in
the steps where man finds strength, Without them we are nothing.”
Again: “You must be infinitely careful. When one is dealing with
power, one has to be perfect, Mistakes are deadly here.” On the
other hand, human routines are to be shunned. An entire chapter of
Journey to Ixtlan is titled “Disrupting the Routines of Life.” The true
hunter is “free, fuid, unpredictable.” In dealing with the personal
sources of gnosis and power, rituals must be precise. In dealing with
the affairs of life, there must be no routines. As he says elsewhere,
there must not even be a personal history. There must be total atom-
ism of the personality: one man, alone, in a hostile universe. The
hunter must be unpredictable, for he is also the hunted. “All of us
behave like the prey we are alter. That, of course, also makes us prey
for something or someone else. Now, the concern of a hunter, who
is to stop being a prey himself.”

This is the universe of the magical manipulator. All the world is
like an enormous container of nitroglycerine. It must be handled
with ritually exact care when it is being manipulated, and at all other
times, the magician must be devious, fluid, totally unpredictable, in
order to escape the manipulations of others. This is the animists’
world, where living, malevolent beings strike out and trap the ritually
negligent. Perfection is a matter of precise ritual. The magician
must content himself with subduing only minute portions of his
world on a piecemeal basis; the world is something to be escaped

knows all this

 

21. Thomas Molnar, God and the Knowledge of Reality (New York: Basic Books,
1973).

22. Castaneda, Teachings, pp. 68-70.

23. Ibid., p. 158.

24. Ixtlan, p. 147

25. Ibid., p. 75

26. Ibid.
