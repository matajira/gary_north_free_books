52 UNHOLY SPIRITS

and Nova Scotia in Canada.
Explanations? Nothing fits. So the history textbooks remain
silent.

Spontaneous Human Combustion

They are also silent concerning another kind of mysterious fire.
If 1871 was a big year for prairie fires, 1938 was a spectacular year for
people fires. Here is one representative account. On September 20,
1938, a woman was dancing on a crowded dance floor in Clemsford,
England. Without warning, she burst into flames. Not her clothing
—her body. Her flesh emitted blue flames, indicating tremendous
heat, as she crumpled to the floor. Her escort and others tried to put
out the flames, but it was hopeless. Within a few minutes, there was
nothing left of her except a few ashes. There was no longer any trace
of a human being. Coroner Leslie Beccles announced: “In all my ex-
perience, I’ve never come across any case as mysterious as this.”

He would not have been able to make this statement had he been
coroner at England’s Norfolk Broads the previous July 30. On that
day, a woman, her husband, and her children were paddling in a
small boat when she suddenly caught fire, She was rapidly reduced
to a mound of ashes before the horrified eyes of her family, yet the
boat was undamaged and the other occupants were unaffected by the
heat. Eric Frank Russell, the British novelist, discovered newspaper
accounts of 19 victims of these fires in Britain in 1938.6

This, for want of an explanation, is simply classified as spontan-
eous human combustion, or SHC. It has existed in the medical liter-
ature of all European nations for over two hundred years. The earli-
est example appears in Acta Medica & Philosophica Hafniensia (1673).
Today, almost no medical experts are willing to admit its existence,
despite over one hundred cases recorded during the last century.
Lester Adelson, pathologist to the coroner of Cuyahoga County,
Ohio in the early 1950's, desperately hoped that some logical expla-
nation of SHC would be found, but he knew of no likely candidate.
He did define the problem in the March-April 1952 issue of North-
western University’s Journal af Criminal Law, Criminology and Police
Science: “Spontaneous human combustion is that phenomenon
wherein the body takes fire without an outside source of heat and is

6, Ibid., p. 190.
