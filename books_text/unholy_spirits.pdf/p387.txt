Is This the End of the World? 381

. Postmillennialism
. Amillennialism
. Historic premillennialism
. Dispensationalism
a. Pre-tribulational
b. Mid-tribulational
c. Post-tribulational

Bone

The prefixes “pre,” “post,” and “a” that are attached to “millen-
nial” refer to the temporal placement of Christ's physical return to
earth to begin the final judgment. Will this take place afer (“post”)
the millennium (the reign of peace on earth), before (“pre”) the millen-
nium, or after no earthly millennium (“a”)? Historically, the church
has had defenders of all three positions. The early church fathers
were mainly premillennial or postmillennial. The Roman Catholics
after Augustine (early fifth century) became mainly amillennial,
although there were occasional postmillennial revivals. Lutherans
have always been amillennial. Calvin’s eschatology was a mixture of
amillennialism and postmillennialism.? The early Puritans were
mainly postmillennial. Charles Haddon Spurgeon was postmillen-
nial,

Dispensationalism is a late development. The original formula-
tion, pretribulational, can be traced back no earlier than 1830 in
Scotland. The history of the origins of premillennial, pre-tribulational
dispensationalism is not discussed by its present-day adherents,
probably because of their understandable embarrassment.'° In 1830,
a 15-year-old girl began attending private prayer meetings at which
the phenomenon of “tongues” speaking (glossalalia) took place.
Members of Edward Irving’s church attended these meetings. Many
of them experienced tongues, but young Margaret Macdonald made
the crucial prophetic announcement, that Jesus would return to
carry His servants up into heaven (the “Rapture”) before He would
arrive physically to establish His earthly millennial reign of a thou-

9. Gary North, “The Economic Thought of Galvin and Luther,” The Journal of
Christian Reconstruction, IL (Summer 1975), pp. 103-6, For the case for Calvin as a
postmillennialist, see Greg L. Bahnsen, “The Prima Facie Acceptability of Postmil-
lennialism,” iid., IIL (Winter 1976- 77), pp. 69-76, For Galvin as an amillennialist,
see Heinrich Quistorp, Calvin's Dactrine of the Last Things (Richmond, Virginia: John
Knox Press, 1955) and T. F, Torrance, Kingdom and Church (1956), ch. 4.

10. Dave MacPherson, The Unbelievable Pre-Trib Origin (Kansas City, Missouri:
Heart of America Bible Society, 1973).
