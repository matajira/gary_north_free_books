Paranormal Science Since 1960 93

September 1975 issue. There is no doubt that the phenomenon ex-
ists. There is also no doubt that it has nothing to do with unassisted
human skills.

Reincarnation by Hypnosis

Dr. Vladimir Raikov, a Moscow psychologist, has developed a
truly innovative system of art training. He takes zero-talent students,
hypnotizes them, tells them in their trances that they are famous art-
ists of the past, and then wakes them. In case after impossible case,
they begin to display hitherto unknown artistic capacities. They do
not become master artists, but people who had been hard-pressed to
draw stick figures have become commercial (pardon the bourgeois
phrase) artists. As always, Raikov is searching for the “unknown laws”
that govern these artistic transformations that literally create talented
people out of average citizens. These techniques have also been used
to produce mathematical abilities in otherwise dull students.

If true, the following bit of historical revisionism is a shocker.
Raikov claims that Sergei Rachmaninoff had been hypnotized by a
Dr. Dahl during a period of depression, For years after the perfor-
mance in St, Petersburg of his First Symphony, which had received
catcalls, he had been unable to compose. Dahl put him under hyp-
nosis, trained him in autosuggestion, and let him loose on his music.
The Piano Concerto No. 2 in C Minor was the first result. Rach-
maninoff later claimed that whole passages of music would simply
come upon him. The phenomenon may be related to the dream-
induced musical creations of Wagner and Tchaikovsky.”

Hypnosis is hoped to become a tool for the unlocking of hidden.
talents. The problem is, however, the source of such talents. Where,
exactly, had they been hidden for so long? And why does hypnosis
bring them out? Hypnosis —a phenomenon which has led, time and
again, to other manifestations of paranormal powers—is not yet un-
derstood by any investigator, yet it is used increasingly by psycholo-
gists all over the world. It releases “untapped forces.” Indeed, and at
times untrapped forces.

Water Dowsing

The age-old practice of water dowsing (also metal dowsing,
missing-object dowsing, etc.) is being revived in the Soviet Union,

46. bid., pp. 156-57.
47. Thelma Moss, ‘The Probability of the Impossibie (Los Angeles: Tarcher; distrib-
uted by Hawthorne Books, New York, 1974), p. 228
