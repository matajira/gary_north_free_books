322 UNHOLY SPIRITS

willing to say that Western science must be rewritten. The question
is: Rewritten in terms of what kind of paradigm? In terms of what sorts of
regularities? In a universe of what sorts of limitations?

Hynek and Vallee are not to this point yet. Their book is well-
titled: The Edge of Reality. They are operating at the edge of the boun-
dary, the edge of the secret treaty between rationalism and irration-
alism, between the phenomenal and the noumenal. What they are
unwilling to admit is that they are operating at the boundary be-
tween the natural and the supernatural, or better put, between the
fixed patterns of the creation and demonic intervention.

The Edge of Reality is a delight to read. Most of it is a transcription
of a three-way discussion between Hynek, Vallee, and a psycholo-
gist, Hastings. At the end, they “pull out the stops” and allow their
imaginations to run wild, What can account for the phenomena?
What could they be? Outside aliens? Earth-bound aliens? Secret
weapons of very bright humans? Or genetic programming of some
kind? Why do they never seem to contact scientists? Why are the
stories of the contacts famitiar—basic patterns, leading nowhere?
What are the implications for mankind?

Hynek reports that at a lecture in the mid-1970's, he asked the
audience how many had seen a UFO or who has a close associate
who has? About 18 percent raised their hands. “How many of you re-
ported it?” One hand. He estimates that there are a hundred sight-
ings per day around the world.%

The three just cannot explain these phenomena. He ends the
book with a section on “interlocking universes,” meaning the edge of
reality. But that other universe sounds suspiciously like the occult:
mind over matter, out-of-body experiences, and so forth. But they
want to think of it as a world that men can eventually understand.
They want it to be phenomenal, not noumenal.

No News Is Safe News

A most important observation by the authors is this one: the
UFO stories stay the same. “The reported sightings that remain un-
explained after serious examination fall into a relatively small num-
ber of fairty definite patterns of appearance and behavior. These pat-
terns have been well delineated by UFO investigators and especially
by the present authors. The patterns have not changed during the

86. The Edge of Reality, p. 254.
