390 UNHOLY SPIRITS

leaders and their followers are rejecting this view, and at the same time they
are also rejecting the idea that Christians are really citizens of heaven, not
of this world, and that Christ is going to “rapture” His church out of this
world. The whole idea of the rise of Antichrist to rule the world during a
tribulation period, and the rapture of the church, whether pre-, mid-, or
post-trib is falling into disfavor. The views of many Christians concerning
the future of the world are beginning to have more and more in common
with the humanistic hope that mankind can really “find itself” and on the
basis of a common brotherhood begin to love one another and live up to our
potential of humanness and authentic personhood.?!

Why can't Christians have legitimate earthly hope regarding the
effects of their work on earth? Why should all the efforts of Chris-
tians come to nothing, culturally speaking? After all, wasn’t all
power given to Jesus Christ after His resurrection (Matt. 28:18)?
Furthermore, why can’t Christians be ethical citizens of heaven
while simultaneously working to establish a program of parallel
standards for earthly citizenship that are ethically comparable to
heaven’s standards? After all, we pray the Lord’s prayer, which in-
cludes the request: “thy kingdom come, thy will be done, on earth as
it is in heaven.” (Actually, there are many dispensational churches
that refuse to pray the Lord’s prayer, since it was supposedly part of
the “Jewish dispensation” of the ministry of Jesus, not part of the
“Church Age.”) Why must we draw an absolute ethical distinction
between heavenly citizenship and earthly citizenship? The pagan is
subject to this distinction, but why must Christians be subject to it?
Why must we adopt a theology which leads to a psychological “other-
worldliness” in terms of our work on earth, and pessimism concerning
our work’s earthly results— the theology I call pietism? Jt is this radical
distinction between heaven and earth in the theology of dispensationalism which
has played into the hands of the humanists. Fundamentalists have been con-
tent to let evil men run the world, since the Christian's citizenship
supposedly is exclusively in heaven. This is the theological basis of the
alliance between the escapist religion and the power religion.

A Paradigm Shift

He is quite correct on one point, however. Dispensational escha-
tology is increasingly falling into disfavor. This is why he is worried,
not just because a few charismatic preachers who are sloppy in their

21. Hunt and McMahon, pp. 215.
