The Crisis of Western Rationalism 7

The Purpose of This Book

What I argue in this book is a very simple thesis: ideas have conse-
quences. 1 argue that whenever the fundamental religious and philo-
sophical presuppositions that have predominated during one era in
history are abandoned, or at least become increasingly doubtful
among the spiritual, moral, intellectual, and political leaders of that
civilization, the result is predictable: a transformation of that civilization,
Social and political revolutions follow religious and philosophical
revolutions.

We have seen this take place in the history of Western Civiliza-
tion at least four times: 1) when classical civilization broke down and
was replaced by early Christianity; 2) when the Platonic early Mid-
die Ages were replaced by the Aristotelian later Middle Ages; 3}
when the Reformation replaced late medieval Roman Catholic civi-
lization~ a revolution which was preceded by and paralleled by the
revival of Roman paganism during the Renaissance (especially in
the Italian city states); 4) in the modern transformation, which com-
menced with Darwin, spread to modern physics in the 1920's, and
was culminated by the rise of the New Age movement after 1964.
These transformations were all-encompassing. No area of thought
and culture was immune.

This chapter may be confusing to some readers. It is a brief sur-
vey of some of the most important themes (and battles) in Western
philosophy. Why is this survey necessary in a popular book on
magic? Several reasons. First, before we discuss the reality (or un-
reality) of the world of the occult, we need to understand that men
have long differed over the question, “What is reality?” This raises
the question, “Can we define reality?” If so, “Then who is we?” Why
don’t men agree about their conclusions concerning the nature of
reality? In short: What can men know, and how can they know: it?

Second, we need to account for the revival of occultism in our
day. When occultism has revived in history after a period of being
either legally suppressed or at least under cultural restrictions, we
have seen major challenges to civilization, We are now facing an oc-
cult revival. Are we facing a collapse of our civilization? More to the
point, if we can understand how this occult revival has come about
—why the older rationalism began to retreat after 1964 — we can also
begin to answer questions about how we, as Christians, can begin to
reshape the civilization that will follow the disintegrating rational-
