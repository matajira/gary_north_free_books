The Crisis of Western Rationalism 23

by the media and the sycophant intellectuals who surrounded Ken-
nedy —and brought to an end the hero’s dynasty of rhetoric, cutting
down a man whose programs, like the technocratic liberalism he
promulgated, were mostly form without substance. Lyndon John-
son’s skillfully and ruthlessly imposed legislative substarice — the
final culmination of the old Progressive optimism — soon turned to
dust in the mouths of his followers. The Vietnam war, the race riots,
and the deficit-imduced price inflation broke the spirit of the age.
Johnson could not be re-clected in 1968, just four years after he was
elected President. No one can read the devastating vitriol of David
Halberstam’s popular history of the Vietnam war and the Eastern
Establishment which promoted it and lost it, The Best and the Brightest
(1972), and not recognize the end of not only a political era but of a
state of mind.

My thesis: John F. Kennedy was an unimpressive President in
terms of the legislation he was able to push through Congress during
his three years of power, but in a very real sense, it was his death
which symbolically closed an intellectual age: from Newton to Kennedy.
There was a brief transitional gap for ten weeks. Then the Beatles
landed in New York.

The Crisis of Secular Faith

On the campus, the new Bible for campus intellectuals after 1964
was Thomas Kuhn’s The Structure af Sctentific Revolutions (1962).
Kuhn, a physicist and historian of the history of science, introduced
a thesis which, though not really novel in the humanities, was a
blockbuster in the natural sciences. The history of science, he
argued, is the history of scientific breakthroughs. Not just new varia-
tions on an older theme, not simply a progression upward from fact
to fact, testing everything along the path of scientific progress, but
real revolutions. In these breakthroughs, the older men in an aca-
demic discipline or guild resist the new position. The new outlook
does nat conform to the accepted paradigm, that is, the alder, estab-
lished, accepied way of viewing the world. ‘The establishment insists
that different sorts of questions are supposed to be asked, using
different methods of inquiry, looking at different facts, and produc-
ing different conclusions. The young innovators ignore them, and in
a true scientific revolution, replace them.

The newcomers, who are very often skilled amateurs, or profes-
sionals working in self-imposed (or guild-imposed) isolation, or
