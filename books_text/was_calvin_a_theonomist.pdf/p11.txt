Was Calvin a Theonomist?

So it is concerning every man’s household.
[Covenant Enforced, p. 107.]

The same thing is true concerning cattle,
food, and all other things. For we see here [in
this text] that nothing is forgotten, And God
meant to make us to perceive His infinite good-
ness, in that He declares that He will deal with
our smallest affairs, which one of our own
equals would be loath to meddle with. If we
have a friend, we should be very loath, indeed,
and ashamed to use his help unless it were in
a matter of great importance. But we see here.
that God goes into our sheepfolds and into the
stalls of our cattle and oxen, and He goes into
our fields, and He cares for all other things as
well. Since we see Him abase himself thus far,
shouldn’t we be ravished to honor Him and to
magnify His bounty? [Covenant Enforced, p.
108.]

A Covenantal Promise

God promised the Israelites that they would be
blessed, so as to confirm His covenant with their
fathers. “But thou shalt remember the Lorp thy God:
for it is he that giveth thee power to get wealth, that
he may establish his covenant which he sware unto
thy fathers, as it is this day” (Deut. 8:18). Calvin
echoed this view: God’s blessings in history point to

His faithfulness in eternity:

Let us conclude, then, that when God says
that He shall bless us in the fruit of the earth,
and that He shall bless us in the fruit of our
cattle, it is a most certain argument that He
will not forget the principal thing. These things
are lowly and of little count, and many times
men despise them, and yet we see that God
takes care of them notwithstanding. Since this
is so, will He forget our souls, which He has
created after His own image, which also He has
so dearly redeemed with the sacred blood of his
Son? Surely not. First of all, therefore, let us
acknowledge God's favor toward us, in abasing
Himseif so far as to direct and govern every-
thing that belongs to our lives and sustenance.
And from there let us rise up higher, and un-

9
