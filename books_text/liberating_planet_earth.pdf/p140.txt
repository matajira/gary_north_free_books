132 Liberating Planet Earth

worldwide evangelism are saying that Satan will win in history's
struggles until that final day that ends history (Revelation 20).
The church fails in its mission to evangelize the world, disciple the
nations, and subdue the earth to the glory of God. This is the
heart and soul of the pessimist’s theory of history. The church fails.
He may talk victory ~ indeed, the language of pessimists is filled
with victorious-sounding phrases—but he really means historical
defeat.

Christians are humble before God, but confident before the
creation which they are called by God to subdue. After all, they
have Biblical law and the Holy Spirit, This confidence eventually
leads the Christians into historic defeat and disaster, say the
pessimists. But why should Christians lose? Why should obedi-
ence to God’s laws produce failure? Why should the gospel
message fail, when it produces good fruit?

Ethical rebels are arrogant before God, and claim that all
nature is ruled by the meaningless laws of probability — ultimate
chaos, including moral chaos. Those who predict the failure of the
church say of the humanists and Communists that by immersing
themselves in the philosophy of moral and revolutionary chaos—
the religion of revolution—covenant-breakers will somehow be
able to emerge totally victorious across the whole face of the earth,
a victory which is called to a halt only by the physical intervention
of Jesus Christ at the final judgment. A commitment to law-
lessness leads to external victory. This makes no sense theologi-
cally, let alone morally.

Evil’s Lever: Good

Covenant-breakers must do good externally in order to in-
crease their ability to do evil. They need to use the lever of God’s
Jaw in order to increase their influence. These rebels will not be
able to act consistently with their own religious and intellectual
presuppositions and still be able to exercise power. They want
power more than they want philosophical consistency. This is
especially true of Western covenant-breakers who live in the
shadow of Christian dominion theology. In short, they restrain the
