The Liberation of the Family 81

God also offers the covenantal society truly vast increases in
per family wealth, if the terms of the covenant are maintained.
The covenant community increases its contro} of capital, genera-
tion by generation, piling up ever-greater quantities of capital,
until the growth becomes exponential, meaning astronomical,
meaning impossible. Compound growth therefore points to the
fulfillment of the dominion covenant, the subduing of the earth. It
points to the end of cursed time.

(It might be appropriate at this point to clarify what I mean
when I speak about a covenant society amassing huge numbers of
monetary units called talents. If we are speaking of a whole soci-
ety, and not just a single family, then for all of them to amass
6,747 talents per family in 250 years, there would have to be mass
inflation, the printing of billions of “talent notes.” I am speaking
not of physical slips of paper called talents; I am speaking of goods
and services of value. The 100 talents per family, multiplied by all
the families in the society, would not be allowed to increase; prices
would fall in response to increased production of 3% per annum.
Eventually, if the whole society experiences 3% per annum eco-
nomic growth, given a fixed money supply, prices would begin to
approach zero. But prices in a cursed world will never reach zero;
there will always be economic scarcity [Genesis 3:17-19]. In fact,
scarcity is defined as a universe in which total demand is greater
than supply at zero price. So the assumption of permanent com-
pound economic growth is incorrect. Either the growth process
stops in the aggregate, or else time ends. That, of course, is pre-
cisely the point. Time will end.)

A man whose vision is geared to dominion, in time and on
earth, has to look to the years beyond his lifetime. He cannot hope
to build up his family’s capital base in his own lifetime sufficient to
achieve conquest. (Yes, a few men do achieve this, but not many;
we are talking about dominion by the Christian community, not
the dominion of a few families.) If he looks two or more centuries
into the future, it becomes a conceivable task.

If a man’s time perspective is limited to his own lifetime, then
he must either give up the idea of family dominion, or else he
