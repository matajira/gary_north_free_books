168 Laberating Planet Earth

Marxism vs., 5, 29
rivals of, 144
society &, 1
Chronos, 65
church
assembly, 24
authority, 98
covenant, 88-89, 98
discipline, 96
failure?, 132
family &, 94-95
head of, it
judgment, 97
loser?, 131
Marxism &, 90
representative, 94
schools, 98
self-policing, 93
state &, 88, 96
citizenship, 12
class conflict, 120
class struggle, 114
Coase, R. H., 124
communion, 95
Communist Manifesto, 19
Communism, 1, 47, 58, 123
collaboration with, 31
long run, 71
dominion &, 142-43
Communists, 129, 133
compassion, 153
compound growth, 79-82
confidence, 144
contentment, 153-54
contracts, 51
cooperation, 111-13, 125
corporation, 116
corruption, 6
courts, 96, 101, 103-4, 108
covenant, 26, 27, 28, 47, 48
authority, 54-55
blessings, 43, 75-76, 119, 129,
136, 139

bond, 54, 56
church, 89
civil law, 114
confirmations, 129
cursings, 78
dominion, 134-35
family, 73-74
freedom, 55
hierarchy, 54-55
inheritance, 56-57
judgment, 55-56
law, 55, 119
oath, 50, 51, 16
positive feedback, 129, 136, 137, 148
salvation, 3
Satan's, 57-60
State, 89-90, 102-3
structure, 157-58
vow, 50
whose?, 50
with death, 42
covetousness, 109, 110, 112, 113
creation, 5, 11, 22, 53, 119, 129
creativity, 26
creaturehood, 26
creed, 45
creeds, 49, 56
Cromwell, Oliver, 122
cross, 11, 63, 106, 135
Cuba, 68
culfs, 19
curse, 11
Czar, 32
Czechostovakia, 7

Darwin, Charles, 65
Darwinism, 156
death, 9

debt, 97
decapitalization, 126
decentralization, 155
deception, 6

defeat, 130
