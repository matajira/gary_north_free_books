12 Liberating Planet Earth

faces the hostility of his associates. He faces others on a regular
basis who are determined to confiscate what he has. The obvious
response is to conceal one’s success from others. But this also
means concealing one’s economic expectations.

Planning becomes clothed in secrecy. The planning agency of the
family limits its goals. Disputes between families increase, since
families cannot easily cooperate under such circumstances. The
future is a topic of discussion only in vague terms, except in the
privacy of family economic planning councils. The social division
of labor is thwarted, and the future-orientation of communities is
drastically reduced, since men refuse to discuss plans openly.

God forbids theft; covetousness is the inward destre that leads to
theft or fraud. It is the evil desire which overwhelms the law’s re-
straint on the sinner, the desire to have another man’s property,
whether or not the other man benefits from the transaction.

Voluntary exchange offers the other man an opportunity. He
may not have known of the opportunity. He may not have known
of a person’s willingness to part with some resource in order to ab-
tain what he, the owner, possesses, It is not immoral to offer
another person an opportunity, unless the opportunity is innately
immoral (such as offering to buy his wife’s favors). Covetousness is
the lawless desire to take the other man’s property, whether or not he finds the
transaction beneficial. When covetousness is common, men lose faith
in their neighbors, in the social and political structure which pro-
tects private property, and in the benefits offered by the division of
labor. Covetousness threatens the very fabric of society.

The tenth commandment was given to us so that we might en-
joy the fruits of social peace and social cooperation. This is equally true
of the earlier commandments. The law-order of the Bible is a
means of reducing conflict and extending the division of labor. Greater
efficiency becomes possible through the division of labor. What-
ever contributes to social peace thereby tends to increase per
capita productivity, and therefore per capita income. People have
an economic incentive to cooperate. The prohibition against cov-
etousness increases social cooperation by reducing its costs.

It is significant that the prohibition begins with the mind of
