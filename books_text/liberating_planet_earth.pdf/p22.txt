14 Liberating Planet Earth

quite accurate; it was no doubt a heritage of his Judaism, for
gehenna was the garbage dump outside of Jerusalem.) The stakes
in this “game” (which is not a game) are exceedingly high:

. . . but there are some who trouble you and want to pervert the
gospel of Christ. But even if we, or an angel from heaven, preach
any other gospel to you than what we have preached to you, let
him be accursed. As we have said before, so now I say again, if
anyone preaches any other gospel to you than what you have re-
ceived, let him be accursed (Galatians 1:7-9),

True Liberation Theology

There is a theology of liberation. It affirms that the God who
created all things and who judges all things has also sent His son
to die for the sins of mankind. Jesus announced at the beginning
of His public ministry:

The Spirit of the Lord is upon Me, because He has anointed
Me to preach the gospel to the poor. He has sent Me to heal the
brokenhearted, to preach deliverance to the captives and recovery
of sight to the blind, to set at liberty those who are oppressed, to
preach the acceptable year of the Lord (Luke 4:18-19),

Christ is the (berator. He is the same God who delivered His
people out of the bondage of Egypt and the bondage of Medo-
Persia. It is the God who raised up Joseph out of an Egyptian
prison to become second in command in Egypt, the God who raised
up Daniel out of the lions’ den to become (again) chief advisor to
the Medo-Persian empire. It is the same God who raised up Jesus
out of the ultimate prison: death. The Roman Empire became a
prison experience for the early church, but then came resurrec-
tion: Christians took over the Roman Empire.

There are people in prison and even whole nations in prison.
The Soviet Union is the largest prison in mankind’s history. No
rational person denies that there are prisons in life. Men are in
desperate need of liberation. But liberation comes through cove-
