The Liberation of the Church 89

the Bible. First, transcendence/immanence (presence). The minister
and elders represent God to the people and the people to God,
God is present with His people in the church worship meetings,
especially during the serving of the Lord’s Supper (communion).

Second, there is hierarchy. Elders rule over deacons, and both
rule through service over the members. The elders serve as an ap-
peals court in church disputes (1 Corinthians 3).

Third, there is ethics. The church preaches the gospel, declar-
ing God’s law for every area of life. The church is a counsellor, as
the Levites were, to other institutions.

Fourth, there is judgment. The church excommunicates~ cuts
off from communion —as a prelude to the final judgment of God
(1 Corinthians 5).

Fifth, there is continuity. The church is a continuing institution
over time, the place where parents and children gather. It pro-
vides continuity for families through the sacrament of baptism,

Naturally, we see a rival church in satanic cults, but more im-
portantly, in humanism’s self-proclaimed agency of salvation, the
messianic state.

The state seeks to serve as the voice of God, or what is the
same today, the voice of man in a world where no God is said to
exist. This is why tyrannies insist on elections, and require all citi-
zens to vote in these meaningless elections. “Vox populi, vox dei”:
the voice of the people is the voice of god—the god of humanism.

Second, the state establishes a bureaucratic hierarchy, This hi-
erarchy tells people how they must live. It is a top-down hierarchy.

Third, the state announces laws—an endless stream of laws.
In the United States, every day, the national government's bu-
reaucracy prints the Federal Register, which is over 200 pages long,
in three columns of small print, which announces the new rules
and regulations for the day. Over 54,000 pages appear each year.
Hardly anyone except specialized lawyers can even read these
laws. (Most of God’s laws for the civil government appear in Ex-
odus 20-23 and Deuteronomy).

Fourth, the state asserts itself to be judge over every aspect of
life, invading the church and the family with barely a thought of

 
