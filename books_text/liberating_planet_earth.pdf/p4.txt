Copyright ©1987 by Gary North
Second Printing, 1991

All rights reserved. Written permission must be secured from the
publisher to use or reproduce any part of this book, except for
brief quotations in critical reviews or articles.

Published by Dominion Press
Tyler, Texas 75701

Printed in the United States of America

Unless otherwise noted, all Scripture quotations are from the New
King James Version of the Bible, copyrighted 1984 by Thomas
Nelson, Inc., Nashville, Tennessee.

Library of Congress Catalog Card Number 87-071021
ISBN 0-930462-51-3
