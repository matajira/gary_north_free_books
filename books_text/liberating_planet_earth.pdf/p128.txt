120 Liberating Planet Earth

the poor and weak, forcing them into positions of permanent ser-
vitude. Historically, on the contrary, no soctal order has provided more
opportunities for upward soctal mobility than the free market.

The remarkable advance of numerous immigrant groups, but
especially of Eastern European Jews, in the United States from
1880 to 1950, is historically unprecedented.? Today, the policies of
the socialist welfare state are making lifetime dependents out of a
substantial minority of citizens. The modern welfare system is
deeply flawed, not simply because it uses coercion to take income
from the employed, but because it destroys the will of the recipi-
ents to escape from the welfare system.

The politics of welfare is also leading to class conflict. George
Gilder’s words in Wealth and Poverty are eloquent in this regard:

A program to lift by transfers and preferences the incomes of
less diligent groups is politically divisive—and very unlikely—
because it incurs the bitter resistance of the real working class. In
addition, such an effort breaks the psychological link between
effort and reward, which is crucial to long-run upward mobility.
Because effective work consists not in merely fulfilling the require-
ments of labor contracts, but in “putting out” with alertness and
emotional commitment, workers have to understand and feel
deeply that what they are given depends on what they give—that
they must supply work in order to demand goods. Parents and
schocls must inculcate this idea in their children both by instruc-
tion and example. Nothing is more deadly to achievement than the
belief that effort will not be rewarded, that the world is a bleak and
discriminatory place in which only the predatory and the specially
preferred can get ahead. Such a view in the home discourages the
work effort in school that shapes earnings capacity afterward. As
with so many aspects of human performance, work effort begins in
family experiences, and its sources can be best explored through
an examination of family structure. Indeed, after work the second
principle of upward mobility is the maintenance of monogamous
marriage and family.3

2, Thomas Sowell, Race and Economics (New York: David McKay Co., 1975),
Pi. IL.
3, George Gilder, Wealth and Poverty (New York: Basic Books, 1981), pp. 68-69.
