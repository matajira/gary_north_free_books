56 Liberating Planet Earth

During the creation week, God said “It is good” after each day.
He evaluated His own work, and He rendered judgment verbally. God
is the supreme King, but also the supreme Judge. When He
declares a man innocent, because of His grace to the person
through the gift of saving faith, God thereby imputes Christ's
righteousness to him. Without God's declaration of salvation,
meaning without the imputation of Christ’s righteousness to over-
come the imputation of Adam’s sin, there is no salvation.

When a covenant is “cut,” men are reminded of both the bless-
ings and the cursings attached to the covenant. There are oaths
and vows. There are covenant rituals, There are visible signs and
seals. We see this in the church (baptism, Lord’s Supper), the
family (marriage ceremony), and in civil government (oath-tak-
ing of civil officers).

5, Inheritance/Continuity (survival)

Finally, there is the legitimacy/inheritance aspect of the cove-
nant. There are covenantally specified standards that govern the
transfer of the blessings of God to the next generation. In other
words, the covenant extends over time and across generations.
The covenant is a bond which links past, present, and future. It has impli-
cations for men’s time perspectives. It makes covenantally faithful
people mindful of the earthly future after they die. It also makes
them respectful of the past. For example, they assume that the
terms of the covenant do not change in principle. At the same
time, they also know that they must be diligent in seeking to apply
the fixed ethical terms of the covenant to new historical situations.
They are respectful of great historic creeds, and they are also ad-
vocates of progress, creedal and otherwise. They believe in
change within the fixed ethical terms of the covenant.

Adam was disinherited by God because of his sin. The second
Adam, Jesus Christ, has re-established this forfeited inheritance,
and it is now passed to those adopted into God’s family through
the grace of God (John 1:12), “The wealth of the sinner is stored
up for the righteous” (Proverbs 13:22a).
