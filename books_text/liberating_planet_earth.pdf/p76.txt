68 Liberating Planet Earth

dispensible means is a new social structure. Is it not perfectly obvi-
ous that an existing social system has more efficacy for education
or miseducation than the exhortations of classroom or temple?
How far can you get with the idea that a person should not place
his or her heart in money and material things (the central idea of
the Sermon on the Mount) if the existing social system inculcates
just the contrary under pain of blows and death? Perhaps an insig-
nificant minority can heroically resist the peremptory mandates of
such a system. But Christianity cares about all human beings. It
cannot content itself with saving a tiny minority. The majority
cannot even assign a sense of realism to the Christian message of
brotherhood and solidarity with neighbor, when the social struc-
ture imposes on it, under pain of annihilation, the task of seeking
its proper interest and letting the chips fall where they may, with-
out preoccupying itself with other people. Structural change will
be a mere means for personal change ~but a means so obviously
necessary, that those who fail to give it first priority demonstrate by
that very fact that their vaunted desire to transform persons is just
empty rhetoric.?

Not to put too fine a point to it, but how does this prophet for
Communism think that the message and work of Jesus and the
disciples led to the transformation of the Roman Empire? What
means of comprehensive “structural alterations” of Mediterranean
society did that tiny band of disciples possess? All they had was
the truth, the empowering of the Holy Spirit, and a long-term vi-
sion of the kingdom of God.

What small groups of dedicated Christians need today,
Miranda might say in a moment of candor, is a cadre of trained
revolutionaries supported with weapons from the Soviet Union,
by way of Cuba. As it is, he devoted Chapter 3 of his three-chap-
ter book to the topic, “Politics and Violence in Jesus of Nazareth.”

This view of man leads to the creation of a massive central
planning system, run by an elite, and imposed by force in a top-

2. Miranda, Communism in the Bible (Maryknoll, New York: Orbis, [1981]
1982), p. 6.
