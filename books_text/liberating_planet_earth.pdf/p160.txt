152 Liberating Planet Earth

called while free is Christ’s slave. You were bought with a price; do
not become slaves of men. Brethren, let each one remain with God
in that calling in which he was called (1 Corinthians 7:20-24).

If we were converted to Christ as men under another man’s
jurisdiction, let us remain faithful slaves until the day that God
brings us an opportunity for freedom. The Hebrews in captivity
had to serve as slaves and unfree men for a time, but God eventu-
ally heard their groaning and their prayers. He delivered them
out of bondage to men and false gods so that they could obey Him bet-
ler as free men. We are called to freedom in order to obey God betier. So,
if we are not living today in the freedom that the gospel offers, let
us do our best today as unfree men to obey God as best we can. This will
train us to be better servants of God—and better free men—when
He delivers us.

We are not to choose slavery. Slavery to Marxism is the worst
kind of slavery. Paul warned us, “You were bought with a price;
do not become slaves of men.” God warned Isaac:

“Do not go down to Egypt; dwell in the land of which I shall tell
you. Sojourn in this land, and I will be with you and bless you; for
to you and your descendants J give all these lands, and I will per-
form the oath which I swore to Abraham your father. And I will
make your descendants multiply as the stars of heaven; I will give
to your descendants all these lands; and in your seed all the nations
of the earth shall be blessed” (Genesis 26:2-4),

Do not go down to Egypt, no matter how hard it seems to you
in the wilderness. In Egypt there is slavery, no matter how great
the promises seem, no matter how fine the land of Goshen looks.
Stay out of Egypt! In our day, the land of Marxism is the land of
Egypt.

What would Isaac receive? Not the promised land. God prom-
ised to bless his descendants, but He made no special promise to
Isaac. Isaac was to remain content, despite the fact that he would
not personally inherit the land in his lifetime. He had the promise
of God: his descendants would inherit the land. That was to be suffi-
cient for Isaac.
