The Covenant of Liberation 5t

us all, unless we accept God’s substitute, Jesus Christ, who called
down the wrath of God on Himself.

Other institutions are contractual or fellowship institutions,
not covenantal. Only three institutions possess the God-ordained
right to impose binding oaths before God: family, church, and
state. This is why self-maledictory oaths taken to secret societies
are unbiblical. They are Satan’s attempt to establish non-Biblical
covenants.

Without donding— meaning personal and judicial bonding—to
God by means of a lawful covenant, neither the individual nor a
society can sustain long-term dominion. This is why we need to
understand true covenants and false ones. When we understand
the covenants, we can better understand the nature of the ethical
war we are in.

Pastor Ray Sutton argues persuasively in his book on the cov-
enant, That You May Prosper (1987) that there is a five-part struc-
ture to both the Old Testament and New Testament covenant
model. Professor Meredith Kline discovered a five-point structure
in the Book of Deuteronomy, although it differs somewhat from
Sutton’s model, but he did. not pursue its implications. Sutton
does pursue them. Here is the basic outline of the covenant
throughout the Bible:

1. Transcendence/Immanence (presence)
2. Hierarchy/Authority (submission)

3. Law/Dominion (stipulations)

4, Judgment/Oath (sanctions)

5. Inheritance/Continuity (survival)

The language may seem a bit technical. We can put the words
differently and make things more practical:

. Who’s in charge here?

. To whom do I report?

. What are my orders?

. What do I get if I obey (disobey)?
. Does this outfit have a future?

RON

This may not seem to be a revolutionary insight, but it is. It is
not possible for me to reproduce here all of his arguments that
