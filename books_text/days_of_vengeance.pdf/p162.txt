3:10-11 PART TWO: THE SEVEN LETTERS

Philadelphia, their domination of the true covenant people will
not last long. Christ Himself will force them to come and bow
down at the Christians’ feet. In this statement is an ironic refer-
ence to Isaiah 60:14, where God gives this promise to the cove-
nant people, who had been persecuted by the heathen:

The sons of those who afflicted you will come bowing to you,

And all those who despised you will bow themselves at the
soles of your feet;

And they will call you the City of the Lorp,

The Zion of the Holy One of Israel.

Those who falsely claim to be Jews are really in the position
of the persecuting heathen; and they will be forced to acknowl-
edge the covenantal status of the Church as the inheritor of the
promises to Abraham and Moses, For the Church is the true
Israel, and in coming into the Church, these believers “have
come to Mount Zion and to the city of the living God” (Heb.
12:22). Apostate Israel has been pruned out of the tree of life of
the covenant people, while believers in Christ from all nations
have been grafted in (Rom. 11:7-24). The only hope for those
outside the covenant line, regardless of their ethnic or religious
heritage, is to recognize Christ as the only Savior and Lord, sub-
mitting themselves to Him. Unless and until the Jews become
grafted into the covenant line by God’s grace, they will remain
outside the people of God, and will perish with the heathen. The
Bible does hold out the promise that the descendants of Abra-
ham will return to the faith of Jesus Christ (Rom. 11:12, 15,
23-32). But until they do, Scripture classes them with the
heathen (with one major difference, however: the condemnation
of the apostate Jew is much more severe than that of the unen-
lightened pagan; see Rom. 2:1-29}.

10-11 Because the persecuted Christians of Philadelphia had
kept the word of perseverance, their Lord promises in return to
keep them from the hour of testing. Note well: Christ is not
promising to rapture them or to take them away, but to keep
them. In other words, He is promising to preserve them in trial,

9. See David Chilton, Paradise Restored: A Biblical Theology of Dominion
(Ft. Worth, TX: Dominion Press, 1985), pp. 1258.

128
