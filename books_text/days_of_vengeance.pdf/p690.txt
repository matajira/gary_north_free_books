APPENDIX C

and he and his host tremble at the thought (James 2:19). When
demonic men take seriously his lies about the nature of reality, they
become impotent, sliding off (or nearly off) God’s lap. It is when
satanists realize that Satan’s official philosophy of chaos and antinom-
ian lawlessness is a He that they become dangerous. (Marxists, once
again, are more dangerous to America than are the Ik.) They learn
more af the truth, but they pervert it and try to use it against God’s
people.

Thus, the biblical meaning of epistemological self-consciousness is
not that the satanist becomes consistent with Satan’s official philoso-
phy (chaos), but rather that Satan’s host becomes consistent with what
Satan really believes: that order, law, power are the product of God’s
hated order. They learn to use law and order to build an army of con-
quest. In short, ‘hey use common grace knowledge af the truth to
pervert the truth and to attack God’s people. They turn from a false
knowledge offered to them by Satan, and they adopt a perverted form
of truth to use in their rebellious plans. They mature, in other words.
Or, as C. S. Lewis has put into the mouth of his fictitious character,
the senior devil Screwtape, when materialists finally believe in Satan
but not in God, then the war is over.?* Not quite; when they believe in
God, know He is going to win, and nevertheless strike out in fury —
not blind fury, but fully self-conscious fury—at the works of God,
then the war is over.

Cooperation

How, then, can we cooperate with such men? Simply on the basis
of common grace. Common grace has not yet fully developed. But
this cooperation must be in the interests of God’s kingdom. Whether
or not a particular ad Aoc association is beneficial must be made in
terms of standards set forth in biblical law. Common grace is not com-
mon ground; there is no common ground uniting men except for the
image of God in every man.

Because external conformity to the terms of biblical law does pro-
duce visibly good results—contrary to Prof. Kline’s theory of God’s
mysterious will in history —unbelievers for a time are willing to adopt
these principles, since they seek the fruits of Christian culture. In
short, some ethical satanists respond to the knowledge of God’s law
written in their hearts. They have a large degree of knowledge about
God's creation, but they are not yet willing to attack that world. They
have knowledge through common grace, but they do not yet see what

26. C. 8. Lewis, The Screwtape Letters (New York: Macmillan, 1969), Let-
ter?

658
