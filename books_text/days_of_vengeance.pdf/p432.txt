16:2-3 PART FIVE: THE SEVEN CHALICES

(Ezek. 14:19; 20:8, 13, 21; 21:31). In the New Testament, it is
similarly used in contexts that parallel major themes in Revela-
tion: the spilling of wine (Matt. 9:17; Mark 2:22; Luke 5:37), the
shedding of Christ’s blood (Matt. 26:28; Mark 14:24; Luke
22:20), the shedding of the martyrs’ blood (Matt. 23:35; Luke
11:50; Acts 22:20; Rom. 3:15), and the outpouring of the Spirit
(Acts 2:17-18, 33; 10:45; Rom. 5:5; Tit. 3:6; cf. Joel 2:28-29;
Zech. 12:10). All these different associations are in the back-
ground of this outpouring of plagues into the Land that has
spilled the blood of Christ and His witnesses, the people who
have resisted and rejected the Spirit: The old wineskins of Israel
are about to split open.

2 As the first angel pours out his Chalice into the Land, it
becomes a loathsome and malignant sore upon the men who
had the mark of the Beast and who worshiped his image. The
sores are a fitting retribution for apostasy, “a hideous stamp
avenging the mark of the Beast’ —as if the mark had “broken
out in a deadly infection.”5 Just as God had poured out boils on
the ungodly, state-worshiping Egyptians who persecuted His
people (Ex. 9:8-11), so He is plaguing these worshipers of the
Beast in the Land of Israel— the Covenant people who have now
become Egyptian persecutors of the Church. This plague is spe-
cifically mentioned by Moses in his list of the curses of the Cove-
nant for idolatry and apostasy: “The Lorp will smite you with
the boils of Egypt and with tumors and with the scab and with
the itch, from which you cannot be healed. . . . The Lorn will
strike you on the knees and legs with sore boils, from which you
cannot be healed, from the sole of your foot to the crown of
your head” (Deut. 28:27, 35).

3 The second angel pours out his Chalice into the sea, and it
becomes blood, as in the first Egyptian plague (Ex. 7:17-21) and
the Second Trumpet (Rev. 8:8-9). This time, however, the blood
is not running in streams, but instead is like that of a dead man:

4, Ibid., p. 175.
5. J. P.M, Sweet, Revelation (Philadelphia: The Westminster Press, 1979),
p. 244.

398
