THE DAYS OF VENGEANCE

lawsuit of, 11, 13-20, 268, 466
nature of, 14
oath of, 176, See alse Oath
preamble of, 49
prologue of, 85
promise of, 176, 207
ratification of, 227
role of, 10-11
stipulations of, 141
succession arrangements of, 14
Covenant, Abrahamic, 300. See also
Covenant, New
Covenant, Ark of the, 87, 234, 261
Covenant, Davidie, 87. See also Jesus Christ,
kingdom of
Covenant, New, 6, 168-69, 173, 263, 389, 566,
578
“bith of,” 176-77, 473
Christ opens, 188
establishment of, 413
ethnic enlargement under, 227
fight of, 970-73
new creation and, 266-68, 320
Covenant of Grave, 556, See eisv Covenant,
Abrahamic; Covenant, New
Covenant of Redemption, 215
Covenant, Old, 292, 416, 566
curses of, 168-69
dead believers of, 515
end of, 6, 413
history of, and seven churches, 86-89
night of, 571-72
worship under, 152
Craftsmen, 464
CRC. See Christian Reformed Church
Creation, 266
covenant and, 266-67
doctrine of, 161-62
God’s rute over, 204
seven eyes and, 173
symbolism and, 32-33, 158-60
Creation, new, 110, 197, 283, 525, 548, 545. See
ais Heaven(s} and carth, new
birth of, 290
new covenant and, 266-68
untolding of, 547-48
Creatures, four living, 150, 174, 185
identity of, 155-60
Critical text, 45
Cross of Christ, 568n, 569. See also
Atonement; Death of Christ
Crown(s), 153, 187
Crown of life, 104
Crucifixion of Christ. See Atonement; Death
of Christ
Culture
regenerate, 526-28

self-consciousness and, 651-52
unregenerate, 526-28
Curse(s)
common, 629, 646
opvenant, 17-19
reduction of, 642
removal of, 569
special, 629, 646, 659
Cyrus the Persian, 407-8

Dallas Theological Seminary, 616
Dan, 212
Daniel, 43, 103, 263, 346, 371, 576
beasts of, 303, 308, 328-29
Darkness, 240-41
Dathan, 491
David, 87, 88, 169, 308, 525, 535, 536
key of, 90
kingdom of, 514, See atvo Jesus Christ,
kingdom of
offspring of, 578-79
Root of, 170-71, 378-79
Day of the Lord, 235, 267, 269, 413, 451. See
iso Judgment(s), day of
Dead
final judgment of, $32-34
rest of, 515-16
Dead Sea, 366
Death, 191-92, 533, 534
defilement of, 516
first, 633
keys of, 77-78, 191
spititual, at Sardis, 120
victory over, 366-70, See also Resurrection,
Christ's
Death of Christ, 50, 99, 364-65, 567-68. See
atso Atonement
collaboration in, 280, 282-83
Dragon and, 308
outside Jerusalem, 376-77
Death penalty, 142, 618-19
Death, second, See Second death
Deborah, 417
De-ereation, 196.97, 198
Dedication, Feast of, 217
Deification, 2780
Deism, 157
Demons, 244, 280, 316-17
king of, 247
Depravity, total. See also Fall of man; Sin
God's restraint of, 629-34
Deuteronomy
covenant structure of, 14-15, 379-80
preamble af, 49
sanctions of, 225-26
stipulations of, 141-42
succession arrangements in, 380-2

 

708
