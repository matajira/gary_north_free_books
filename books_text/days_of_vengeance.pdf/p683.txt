COMMON GRACE, ESCHATOLOGY, AND BIBLICAL LAW

gracious in order to pile the maximum possible pile of coals on their
heads. In contrast to Van Til’s amillennialist vision of the future, we
must say: When common grace is extended to its maximum limits pos-
sible in history, then the crack of doom has come—doom for the
rebels.

Epistemological Self-Consciousness and Cooperation

Van Til writes: “But when all the reprobate are epistemologically
self-conscious, the crack of doom has come. The fully self-conscious
reprobate will do all he can in every dimension to destroy the people of
God.” Yet Van Til has written in another place that the rebel against
God is like a little child who has to sit on his father’s lap in order to
slap his face. What, then, can be meant by the concept of increasing
epistemological self-consciousness?

As the wheat and tares grow to maturity, the amillennialist argues,
the tares become stronger and stronger culturally, while the wheat
becomes weaker and weaker. Consider what is being said. As Chris-
tians work out their own salvation with fear and trembling, improving
their creeds, improving their cooperation with each other on the basis
of agreement about the creeds, as they learn about the law of God as it
applies in their own era, as they become skilled in applying the law of
God that they have learned about, they become culturally impotent.
They become infertile, also, it would seem. They do not become fruit-
ful and multiply. Or if they do their best to follow this commandment,
they are left without the blessing of God—a blessing which He has
promised to those who follow the laws He has established. In short,
the increase of epistemological self-conscicusness on the part of Chris-
tians leads to cultural impotence.

I am faced with an unpleasant conclusion: the amillennialist ver-
sion of the common grace doctrine is inescapably antinomian. It
argues that God no longer respects His covenantal law-order, that
Deuteronomy’s teaching about covenantal law is invalid in New Testa-
ment times. The only way for the amillennialist to avoid the charge of
antinomianism is for him to abandon the concept of increasing episte-
mological self-consciousness. He must face the fact that to achieve
cultural impotence, Christians therefore must not increase in knowl-
edge and covenantal faithfulness. (Admittedly, the condition of twen-
teth-century Christianity does appear to enforce this attitude about
epistemological self-consciousness among Christians.)

Consider the other half of Van Til’s dictum. As the epistemological
self-consciousness of the unregenerate increases, and they adhere
more and more to their epistemological premises of the origins of mat-
ter out of chaos, and the ultimate return of all matter into pure ran-

651
