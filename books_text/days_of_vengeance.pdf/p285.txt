ALL HELL BREAKS LOOSE 9:17-19

demonic and pagan in character (on the binding of fallen angels,
cf. 2 Pet. 2:4; Jude 6). God is completely sovereign, and uses
both demons and the heathen to accomplish His holy purposes
(1 Kings 22:20-22; Job 1:12-21; of course, He then punishes the
heathen for their wicked motives and goals which led them to
fulfill His decree: cf. Isa. 10:5-14). The angels bound at the
Euphrates had been prepared for the hour and day and month
and year, their role in history utterly predestined and certain.

St. John hears the number of the horsemen: myriads of myr-
iads, We noted in the Introduction to this volume some of the
more fanciful interpretations of this expression (see pp. 11-13). If
we keep our imaginations harnessed to Scripture, however, we
will observe that it is taken from Psalm 68:17, which reads: “The
chariots of God are double myriads, thousands of thousands.”
Mounce correctly observes that “attempts to reduce this expres-
sion to arithmetic miss the point. A ‘double myriad of myriads’
is an indefinite number of incalculable immensity.”* The term
simply means many thousands, and indicates a vast host that is
to be thought of in connection with the Lord’s angelic army of
thousands upon thousands of chariots.

17-19 Avoiding the dazzling technolegical speculations ad-
vanced by some commentators, we will note simply that while
the number of the army is meant to remind us of God’s army,
the characteristics of the horses — the fire and the smoke and the
brimstone which proceeded out of their mouths —remind us of
the Dragon, the fire-breathing Leviathan (Job 41:18-21). “The
picture is meant to be inconceivable, horrifying, and even re-
yolting. For these creatures are not of the earth. Fire and sul-
phur belong to hell (19:20; 21:8), just as the smoke is characteris-
tic of the pit (9:2). Only monsters from beneath belch out such
things.”? Thus, to sum up the idea: An innumerable army is ad-
vancing upon Jerusalem from the Euphrates, the origin of
Israel's traditional enemies; it is a fierce, hostile, demonic force
sent by God in answer to His people’s prayers for vengeance. In
short, this army is the fulfillment of all the warnings in the law

8. Robert H. Mounce, The Baok of Revelation (Grand Rapids: William B.
Eerdmans Publishing Co., 1977), p. 201.

9. G. R, Beasley-Murray, The Book of Revelation (Grand Rapids: William
B. Eerdmans Publishing Co., [1974] 1981), pp. 165f.

251
