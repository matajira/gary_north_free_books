THE THRONE ABOVE THE SBA 4:4

of the Royal Priesthood, the Church.'5

That these elders are both priests and kings shows that the
Aaronic priesthood of the Old Covenant has been superseded
and transcended; the New Covenant priesthood, with Jesus
Christ as High Priest, is a Melchizedekal priesthood. Thus St.
John tells us that these priest-elders are wearing crowns, for the
crown of the high priest has been given to all. The two inde-
pendent testimonies from the second century that St. James in
Jerusalem and St. John at Ephesus wore the golden crown of the
high priest have generally been discounted by modern scholars;1¢
but these traditions may refiect the actual practice of the early
Church.

This brings us to another point that should be mentioned be-
fore we move on. We have already noted (see on 3:20) several
problems caused by the rationalistic tendencies of those groups
that grew out of the Reformation. Unfortunately, it became
common in those same groups to dispense with the elders’ robe
of office. Though the concern was for “spirituality,” the actual
effects were to platonize doctrine and worship, and to democra-
tize government and ministry — further steps on the long, dusty
road toward Reformed barrenness. As Richard Paquier reminds
us, “Color is a teacher through sight, and it creates moods. We
misunderstand human nature and the place of perception in our
inner life when we downgrade this psychological factor in the
worship of the Church.”!? God has created us this way, and the
continuing validity of official robes follows properly from the
patterns laid down in the Old Testament: The official character
of the elder is emphasized by the use of official robes, in the
same way that the judges in our culture still wear robes—a prac-
tice, incidentally, that grew out of the practice of the Church.

Paquier continues: “It is natural, therefore, that the man

18. A further argument for this interpretation will be developed in the dis-
cussion of 5:9, We will see that the song of the elders recorded there states
clearly that they are among the redeemed—a group that does not include
angels (Heb. 2:16). The elders, therefore, must be taken in the usual sense as
meaning the representatives of the Church.

16. See Dom Gregory Dix, The Shape of the Liturgy (New York: The Sea-
bury Press, [1945] 1982), p. 313; W. H. C. Frend, The Rise af Christianity
{Philadelphia: Fortress Press, 1984), p. 127.

17, Richard Paquier, Dynamics of Worship: Foundations and Uses of
Liturgy (Philadelphia: Fortress Press, 1967), p. 143.

153
