THE MILLENNIUM AND THE JUDGMENT 20:7-8

same time, there is virtually a complete neglect of what the Book
of Revelation actually says about the war with Gog and Magog;
apparently, the specific facts of Biblical revelation occasionally
get in the way of “prophetic truth.”35

Second, those who interpret the war of “Gog and Magog” as
an end-time war involving the Soviet Union usually pride them-
selves on being “literalists.” Yet we should take note of what a
strictly literal interpretation of Ezekiel 38-39 requires:

1, Gog’s reason for invading Israel is to plunder her silver
and gold, and to teke away her cattle (38:11-13); contrary to
much premillennialist exposition, nothing is said about expro-
priating Israel’s oil or extracting minerals from the Dead Sea.

2. All of Gog’s soldiers are on horseback (38:15); there are
no soldiers in trucks, jeeps, tanks, helicopters, or jets.

3. Alf of Gog’s soldiers are carrying swords, wooden shields,
and helmets (38:4-5); their other weapons are wooden bows and

 

Technology and Soviet Economic Development, 1917-67, three vols. (Stan-
ford: Hoover Institution Press, 1968-73); idem, National Suicide (New
Rochelle, NY: Arlington House, 1973); cf. Richard Pipes, Survival Is Not
Enough: Soviet Realities and America’s Future (New York: Simon and Schus-
ter, 1984). Those who are shocked that the possible future conquest of the
United States by the Soviets might not be included in Bible prophecy would do
well to consider the large number of important conflicts throughout the last
thousand years of Western history that have also been omitted —such as the
Norman Conquest, the Wars of the Roses, the Thirty Years' War, the English
Civil War, the American Revolution, the French Revolution, the Napoleonic
War, the Seminole War, the Revolutions of 1848, the Crimean War, the War be-
tween the States, the Sioux Indian War, the Boer War, the Spanish-American
War, the Mexican Revolution, the First World War, the Spanish Civil War, the
Italo-Ethiopian War, the Second World War, the Korean War, and the Vietnam
War, to name a few; many of which were viewed by contemporary apocalyp-
tists as notable fulfillments of Biblical prophecy.

35. The obvious example, of course, is Hal Lindsey, whose Late Great
Planet Earth (Grand Rapids: Zondervan Publishing House, 1970) spends
about thirty pages (pp. 59-71, 184-68) detailing how the Soviet Union will soon
fulfill the prophecy of “Gog and Magog” in the Battle of Armageddon, and
takes only two or three sentences to deal with Rey. 20:8 —not once even men-
tioning that the only reference to Gog and Magog in the entire Book of Reve-
lation is in that verse. Cf. idem, There’s a New World Coming: A Prophetic
Odyssey (Eugene, OR: Harvest House, 1973), pp. 222-25, 278. Another exam-
ple is the usually more circumspect Henry M. Morris, whose Revelation Rec-
ord: A Scientific ond Devotional Commentary on the Book of Revelation
(Wheaton: Tyndale House Publishers, 1983) discusses Gog and Magog under
Rev. 6:1 (pp. 108-110) and 16:12 (p. 310}, but strives mightily to dismiss the sig-
nificance of the reference in 20:8 (pp. 422f.).

321
