ei) PART THREE: THE SEVEN SEALS

So, from one standpoint, God’s people are definitely numbered;
none of the elect are missing, and the Church is perfectly sym-
metrical and whole. From another standpoint, the Church is in-
numerable, a great host that no one could count, Seen from one
perspective, the Church is the new, the frue, Israel of God: the
sons of Jacob gathered into all their tribes, full and complete.
From another, equally true perspective, the Church is the whole
world: @ great multitude redeemed from every nation and all
tribes and peoples and tongues.

In other words, the 144,000 are the Remnant of Israel; yet
the fulfillment of the promises to Israel takes place through the
salvation of the world, by bringing the Gentiles in to share the
blessings of Abraham (Gal. 3:8). The number of the Remnant is
filled by the multitudes of the saved from ail nations, just as the
New Jerusalem — whose dimensions are measured in twelves and
whose gates are inscribed with the names of the twelve tribes —is
filled with the glory and honor of the nations of the world
(21:12-27), Farrer says: “By the contrast between the numbered
tribes and the innumerable host, St. John gives expression to
two antithetical themes, both equa!ly traditional. God knows
the number of His elect; those who inherit the blessing of Abra-
ham are as numberless as the stars (Gen. 15:5). Yet St. John can-
not mean either that the number of Gentile saints is unknown to
God, or that the number of righteous Israelites can be counted
by men. What he tells us is, that his ear receives a number result-
ing from an angelic census; and that his eye is presented with a
multitude he cannot count, as was Abraham’s when called upon
to look at the stars. The vision of the white-robed host, purified
by martyrdom, must in any case reflect Daniel 11:35. The theme
is continued in Daniel 12:1-3, where the same persons are
described as ‘registered in the book’ and as ‘like the stars’; it is
easy to conclude ‘numbered, therefore, yet uncountable.’ "13

In St. John’s vision, therefore, the sealed Remnant of Israel
is the holy seed, the “first fruits” (14:4) of the new Church, des-
tined to expand into an innumerable multitude gathered in wor-
ship before the Throne in heaven. The nucleus of Israel becomes
the Church, redeemed from every nation in fulfillment of the

13. Tbid., p. 110.
214
