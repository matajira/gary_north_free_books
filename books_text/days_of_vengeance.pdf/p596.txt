21:24-27 PART FIVE: THE SEVEN CHALICES

Young camels of Midian and Ephah.

And all from Sheba will come,

Bearing gold and incense

And proclaiming the praise of the Lorp. .. .
Surely the islands look to me;

In the lead are the ships of Tarshish,
Bringing your sons from afar,

With their silver and gold,

To the honor of the Lorp your God,

The Holy One of Israel,

For He has endowed you with splendor. .. .
Your gates will always stand open,

They will never be shut, day or night,

So that men may bring you the wealth of the nations.
(Isa. 60:5-6, 9, 1)

St. John applies this prophecy to the New Jerusalem: The
nations shall walk by its Light, and the kings of the earth shall
bring their glory and honor into it. And in the daytime {for
there shall be no night there) its gates shall never be closed; and
they shall bring the glory and honor of the nations into it; and
nothing unclean and no one who practices abomination and
lying, shall ever come into it, but only those whose names are
written in the Lamb’s Book of Life. This is what Jesus com-
manded His Church to be: the City on the Hilt (Matt. 5:14-16),
the light of the world, shining before men so that they will glor-
ify God the Father. Obviously, the New Jerusalem cannot be
seen simply in terms of the eternal future, after the final judg-
ment. In St. John’s vision the nations still exist as nations; yet
the nations are all converted, flowing into the City and bringing
their treasures into it. Of course, “the other side to the fact that
the Gentiles bring in their honour and glory, is that they do not
bring in their abominations. . . . The access of the Gentiles here
is in strong contrast with their access in 11:2. The mere presence
of unregenerate heathen in the outer court spelled the ruin of
Old Jerusalem; ihe New admits them sanctified, to her undivided
precinct.”32

In another striking prophecy of the Gospel’s effect on the
world, Isaiah wrote:

30. Ibid.
562
