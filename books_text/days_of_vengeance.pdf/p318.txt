11:13-14 PART FOUR: THE SEVEN TRUMPETS

beholding them (cf. Acts 2:43; 5:5; 19:17; contrast John 7:13;
12:42; 19:38; 20:19), and with good reason: Through the resur-
rection of Christ, the Church and her Testimony became un-
stoppable, In union with Christ in His Ascension to glory (Eph.
2:6), they went up te heaven in the Cloud, and their enemies
beheld them.'5 The Witnesses did not survive the persecutions;
they died. But in Christ’s resurrection they rose to power and
dominion that existed not by might, nor by power, but by God’s
Spirit, the very breath of life from God. “We are not the lords of
history and do not contro! its outcome, but we have assurance
that there is a lord of history and he controls its outcome. We
need a theological! interpretation of disaster, one that recognizes
that God acts in such events as captivities, defeats, and crucifix-
ions. The Bible can be interpreted as a string of God’s triumphs
disguised as disasters.”'6

St. John draws an important parallel here that should not be
missed, for it is close to the heart of the passage’s meaning. The
ascension of the Witnesses is described in the same language as
that of St. John’s own ascension:

4:1 After these things I looked, and behold, a door standing
open in heaven, and the first Voice which I had heard, like a
trumpet speaking with me, saying: Come up here... .

11:11-12 And after the three and a half days . . . they heard a
loud Voice fram heaven saying to them: Come up here. .. .

The story of the Two Witnesses is therefore the story of the
witnessing Church, which has received the divine command to
Come up here and has ascended with Christ into the Cloud of
heaven, to the Throne (Eph. 1:20-22; 2:6; Heb. 12:22-24): She
now possesses an imperial grant to exercise rule over the ends of
the earth, discipling the nations to the obedience of faith (Matt.
28:18-20; Rom. 1:5).

13-14 One of the results of Christ’s ascension, as He fore-
told, would be the crack of doom for apostate Israel, the shak-

15, This bears some similarity to Elijah’s experience, with the major differ-
ence that it was his friend, and not his enemies, who saw his ascension (2 Kings
2:9-14),

16. Herbert Schlossberg, idols for Destruction: Christian Faith and Its
Canfrontation with American Society (Nashville: Thomas Nelson Publishers,
1983), p. 304.

284
