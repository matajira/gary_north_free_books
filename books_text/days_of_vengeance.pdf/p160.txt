3:7 PART TWO: THE SEVEN LETTERS

8 I know your deeds. Behold, I have put before you an open
door which no one can shut, because you have a little power,
and have kept My Word, and have not denied My name.

9 Behold, I will cause those of the synagogue of Satan, who
say that they are Jews, and are not, but lie—behold, I will
make them to come and bow down at your feet, and to
know that I have loved you.

10 Because you have kept the word of My perseverance, I also
will keep you from the hour of testing, that hour which is
about to come upon the whole world, to test those who
dwell upon the Land.

11 [am coming quickly; hold fast what you have, in order that
no one take your crown.

12 He who overcomes, I will make him a pillar in the Temple of
My God, and he will not go out from it anymore; and I will
write upon him the name of My God, and the name of the
City of My God, the new Jerusalem, which comes down out
of heaven from My God, and My new name.

13 He who has an ear, let him hear what the Spirit says to the
churches.

7 Like the church in Smyrna, the church in Philadelphia
had been especially persecuted by the apostate Jews. Christ
begins his message to the elders by declaring Himself as the One
who is holy, an established Biblical term for God (cf. Isa. 40:25),
and who is true, in contrast to the lying leaders of the Jews, who
had rejected the truth. Jesus Christ also has the key of David: He
opens and no one will shut, and He shuts and no one opens. This
is an allusion to Isaiah 22:15-25, in which God accuses a royal
steward of falsehood, of betraying his trust. God declares: “I will
depose you from your office, and J will pull you down from your
station” (v. 19; cf. Gen. 3:22-24). Moreover, God would replace
the false steward with a faithful one (cf. 1 Sam. 13:13-14):

And I will clothe him with your tunic,

And tie your sash securely about him.

1 will entrust him with your authority,

And he will become a father to the inhabitants of Jerusalem
and to the house of Judah.

Then I will set the key of the house of David on his shoulder:

When he opens no one will shut,

When he shuts no one will open. (Isa. 22:21-22)

126
