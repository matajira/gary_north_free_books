THE HOLY WAR 12:17

17 The Dragon had only “‘a short time” (v. 12) to destroy the
Church, and he failed again. Frustrated in his attempt to destroy
the Mother Church, he was enraged with the Woman, and went
off to make war with the rest of her seed, the Christians who were
unharmed by the Dragon’s war with the Woman. How is the
Church symbolized by both the Woman and her children? “These
distinctions are easily made and maintained. The Church, con-
sidered as an institution and an organic body, is distinguishable
from her children, as Isaiah 66:7-8 and Galatians 4:22-26 clearly
show. ... We accordingly observe that the Church is in one
point of view the totality of all her members of children; in
other ways, familiar to the Scripture, her individual members
are thought of as related to her as children to a mother.”43

Having been thwarted in his designs to destroy both the
Mother and her Seed, the Dragon turns in rage against the rest
of her seed, the (predominantly Gentile) Christian Church
throughout the Empire. Let us note well St. John’s description
of these brothers and sisters of the Lord Jesus Christ: They keep
the commandments of God and hold to the testimony of Jesus.
The definition of the Christian, from one perspective, is that he
is a member of the organized assembly of the people of God;
just as importantly, he is defined in terms of his ethical conform-
ity to the law of God.

And by this we know that we have come to know Him, if we
keep His commandments. The one who says, “I have come to
know Him,” and does not keep His commandments, is a liar,
and the truth is not in him, (1 John 2:3-4)

For this is the love of God, that we keep His commandments;
and His commandments are not burdensome. (1 John 5:3)

As St. John has already informed us, the saints overcome
the Dragon through the word of their testimony and their faith-
ful obedience, even unto death (v. 11). The following chapters
will detail several crucial stages in the continuing war between
the seed of the Serpent and the seed of the Woman. The passage

43. Ibid., p. 391. A related example is the Biblical use of the expressions
Zion and Daughter of Zion (cf. Ps. 9:11, 14; Cant. 3:11) and children of Zion
(cf. Ps. 149:2).

323
