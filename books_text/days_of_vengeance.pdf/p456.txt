PART FIVE; THE SEVEN CHALICES

‘We have seen that the Book of Revelation presents us with
two great cities, set in antithesis to each other: Babylon and
New Jerusalem. As we shall see in a later chapter, the New Jeru-
salem is Paradise Consummated, the community of the saints,
the City of God. The other city, which is continually contrasted
to the New Jerusalem, is the ofd Jerusalem, which has become
unfaithful to God. Another way to view this is to understand
that Jerusalem was intended from the beginning to be the true
fulfillment of Babylon, a word meaning “Gate of God.” The
place of God’s gracious revelation of Himself and of His cove-
nant should be a true Babylon, a true “Gate of Heaven” and
“House of God,” as Jacob understood when he saw God’s stair-
case to heaven, the true Tower of Babel, the true pyramid which
foretold of Jesus Christ (Gen. 28:10-22; cf. John 1:51). But Jeru-
salem did not walk worthy of the calling with which it had been
called. Like the original Babylon, Jerusalem turned its back on
the true God and sought autonomous glory and dominion; like
the original Babylon, it was apostate; and thus the “Gate of
God” became “Confusion” instead (Gen. 11:9).

How did the faithful City become a Harlot? It began with
the apostasy of the priesthood in Israel. The primary responsi-
bility of the priest (God’s representative), is to re-present the
Bridegroom to the Bride, and to guard her from danger. In-
stead, the priesthood led the people in apostasy from their Lord
(Matt. 26:14-15, 47, 57-68; 27:1-2, 20-25, 41-43, 62-66). Because
of the priesthood’s failure to bring the Bridegroom to Israel, the
Bride became a Harlot, in search of other husbands, The apos-
tasy of the priesthood is described in 13:11-17, under the figure of
the Beast from the Land. But the False Bride is not absolved of
responsibility. She is guilty as well, and St. John’s prophecy
rightly turns now to consider her judgment and destruction.!

The symbolic “Babylon” was destroyed when the seventh
angel poured out his Chalice, the drink-offering of annihilation
(16:17-21). As we have seen, this vision is part of the fourth
Seven of Revelation—the Seven Chalices containing the seven
plagues. The connection is provided in 17:1 (cf. 21:9), which tells

1, The failure of the priesthood, and the consequences of this for the Bride,
are recurring themes in Scripture. See James B. Jordan, Judges: God’s War
Against Humanism (Tyler, TX: Geneva Ministries, 1985).

422
