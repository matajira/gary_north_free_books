16:21 PART FIVE: THE SEVEN CHALICES

phrase, Stuart Russell writes: “It could not but be well known to
the Jews that the great hope and faith of the Christians was the
speedy coming of the Son. [t was about this very time, accord-
ing to Hegesippus, that St. James, the brother of our Lord, pub-
licly testified in the temple that ‘the 5on of Man was about to
come in the clouds of heaven,’ and then sealed his testimony
with his blood. It seems highly probable that the Jews, in their
defiant and desperate blasphemy, when they saw the white mass
hurtling through the air, raised the ribald cry, ‘The Son is com-
ing,’ in mockery of the Christian hope of the Parousia, to which
they might trace a ludicrous resemblance in the strange appear-
ance of the missile.”35

And the men blasphemed God—their consistent reaction
throughout the pouring out of the Chalices, revealing not only
their wickedness but their downright stupidity: When hundred-
pound stones are falling from heaven, it is surely the wrong time
to commit blasphemy! But God has abandoned these men to
their own self-destruction; their vicious, hateful rebellion con-
susmes them to such a degree that they can depart into eternity
with curses on their lips.

The Chalices containing the last of the plagues have been
poured out; but the end is not yet. The chapters that follow will
close in on the destruction of the great Harlot-City and her
allies, and conclude with the revelation of the glorious Bride of
Christ: the true Holy City, New Jerusalem. (Chapters 17-22 may
therefore be considered a continuation of the Seventh Chalice,
or an exposition of its meaning; in any case, the events are clearly
governed by the angels of the Chalices; see 17:1; 21:9.) “Thus the
whole book from beginning to end teaches the great truths—
Christ shall triumph! Christ’s enemies shall be overcome! They
who hate him shall be destroyed; they who love him shall be
blessed unspeakably. The doom alike of Jew and of Gentile is
already imminent. On Judea and Jerusalem, on Rome and her
Empire, on Nero and his adorers, the judgment shall fall. Sword
and fire, and famine and pestilence, and storm and earthquake,
and social agony and political terror are nothing but the woes
which are ushering in the Messianic reign. Old things are rapidly
passing away. The light upon the visage of the old dispensation

35. Russell, p. 482.
418
