INTRODUCTION

Auther and Date

Although the auther’s identity has been much debated, there
is really no reason to doubt that he was the same St. John who
wrote the Fourth Gospel, as the virtually unanimous testimony
of the early Church affirms. He identifies himself simply as
“John” (1:1, 4, 9; 21:2; 22:8), apparently assuming that he will be
recognized by his first-century audience on the basis of his name
alone; and he writes in an authoritative, “apostolic” style, not to
individuals merely, but to the Church. Taking into account the
Church’s highly organized government, which existed from its
inception, it is unlikely that any but a recognized apostle could
have written in this manner.’ In addition, there are numerous
points of resemblance between the Revelation and the Gospel of
John. Even a cursory glance reveals several expressions (e.g.
Lamb of God, Word, and witness) which are common only to
the Gospel of John and the Revelation; no other New Testament
writer uses these terms in the same way.? Austin Farrer? draws
attention to a number of stylistic similarities between the Gospel

1, Contrast this with the tone of St. Clement’s letter to the Corinthians. As
J. B. Lightfoot says in his edition of The Apostolic Fathers (Vol. I, p. 352):
“Authority indeed is claimed for the utterances of the letter in no faltering
tone, but it is the authority of the brotherhood declaring the mind of Christ by
the Spirit, not the authority of one man, whetber bishop or pope.” Cited in
John A. T. Robinson, Redating the New Testament (Philadelphia: The West-
minster Press, 1976), p. 328,

2. See William Hendriksen, More Than Conquerors: An Interpretation of
the Book af Revelation (Grand Rapids: Baker Book House, 1939), pp. 17ff.,
for a list of such similarities. For example, he cites John 7:37 and Rev. 22:17;
John 10:18 and Rev. 2:27; John 20:12 and Rev. 3:4; John 1:1 and Rev. 19:13;
John 1:29 and Rev. 5:6.

3. Austin Farrer, The Revelation of St. John the Divine (Oxford: At the
Clarendon Press, 1964), pp. 4lff.
