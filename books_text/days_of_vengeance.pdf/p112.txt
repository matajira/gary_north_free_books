1:19 FART ONE: THE SON OF MAN

Death and of Hades.** The Empire claimed to have all author-
ity, to possess the power over life and death, and over the grave;
Jesus declares instead that He—and not the State, nor the em-
peror, nor Satan, nor the ruler of the synagogue —has command
over all reality. He is the Lord of life and death, of all history,
and of eternity; and it is in terms of this complete dominion that
He commissions St. John to write this book which so clearly and
unequivocally sets forth the truth of His eternal and compre-
hensive government.

19 St, John’s commission was interrupted by his falling into
a dead faint; now that he has been “resurrected,” he is again
commanded: Write therefore3‘ the things you have seen, and
what they are, and what things are about to take place after
these things. Some interpreters read this as a threefold outline of
the whole book: St. John writes about what he has seen (the vi-
sion of Christ), then about the present (the churches, in chapters
2-3), and finally about the future (chapters 4-22), Such a divi-
sion is quite arbitrary, however; the Revelation (like all other
Biblical prophecies) weaves past, present, and future together
throughout the entire book.

A more likely meaning of this statement is that St. John is to
write what he has seen—the vision of Christ among the lamp-
stands holding the stars —and what they are, i.e., what they sig-
nify or correspond to. The word are (Greek eisiz) is most often
used in Revelation in this sense (1:20; 4:5; 5:6, 8; 7:13-14; 11:4;
14:4; 16:14; 17:9, 10, 12, 15). Thus verse 20 goes on to do just
that, explaining the symbolism of “the things you have seen”
(the stars and lampstands). St. John is then commissioned to
write the things that are about to happen, or (as he told us in

35. Adam originally held the Key of Death and Hades, for he was the Priest
of Eden, with the priestly responsibility of guarding the Gate of Paradise
(Gen. 2:15; see Meredith G. Kline, Kingdom Prologue (privately published syl-
labus, 1981), Vol. , pp. 1278. When he abdicated that responsibility, he him-
self was turned out into death, away from the Tree of Life, and the cherubim
took his place as guardians, holding the flaming sword (the key). By the Resur-
rection, Jesus Christ as the Second Adam returned to Paradise as Priest, the
guardian af Eden’s Gate, to cast rhe Serpent into Death and Hades (cf. Rev.
20:1-3),

36. The therefore shows the connection with St. John’s original commission
inv. H.

B
