PART ONE: THE SON OF MAN

Preamble. We have already noted the stress on divine revelation
in the opening sentence, and this is underscored in the following
verses. The churches are to “hear the words of the prophecy,
and keep the things that are written in it,” and the Lord pro-
nounces a special blessing upon those who obey (1:3); St. John
again speaks of himself as one who has berne witness to “the
Word of God and the Testimony of Jesus” (1:9); further, he tells
of the revelation that came to him in terms of the standard and
familiar patterns of covenantal revelation throughout Biblical
history: “I was in the Spirit on the Lord’s Day, and I heard be-
hind me a loud Voice like the sound of a trumpet, saying: Write
in a book what you see. . . .” (110-11; see below).

Redemption is also stressed in this passage: “Jesus Christ,
the faithful Witness, the Firstborn from the dead, and the Ruler
of the kings of the earth . . . who loves us and released us from
our sins by His blood . . . has made us to be a Kingdom, priests
to His God and Father; to Him be the glory and dominion for-
ever and ever. Amen” (1:5-6), Moreover, Christ is specifically
stated to be the Redeemer, the Son of Man, who “comes with
the clouds” in His glorious Ascension to the Father and coming
judgment upon Israel to receive worldwide dominion, glory,
and a Kingdom; who will be seen by “those who pierced him,”
and mourned over by “all the tribes of the Land” (1:7; cf. Dan.
7:13-14; Zech. 12:10-14; Matt. 24:30; John 19:37; Eph. 1:20-22).
St. John’s vision of Christ develops the idea of His redemptive
work: He is clothed as the High Priest (1:13), revealed as the in-
carnate Glory of God (1:14-15), the Creator and Sustainer of the
world, whose powerful Word goes forth to conquer the nations
(1:16); who died, and rose again from the dead, and who is alive
forevermore (1:17-18).

50
