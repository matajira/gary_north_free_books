6:7-8 PART THREE: THE SEVEN SEALS

will take a third of the Land (cf. 8:7-12), and the Chalice-
judgments will devastate it all.

Perhaps the most significant obstacle to a correct interpreta-
tion of this passage has been that commentators and preachers
have been afraid and unable to see that it is God who is bringing
forth these judgments upon the Land — that they are called forth
from the Throne, and that the messengers of judgment are the
very angels of God. Especially vicious and harmful is any inter-
pretation which seems to pit the Son of God against the court of
heaven, so that the curses recorded here are seen as somehow
beneath His character, But it is Jesus, the Lamb, who breaks the
seals of judgment, and it is Jesus, the King of kings, who rides
out in conquest, leading the angelic armies against the nations,
to destroy those who rebel against His universal rule.

It was crucial for the early Christians to understand this, for
these judgments were even then breaking loose upon their world.
In every age, Christians must face the world with confidence,
with the unshakable conviction that a// events in history are pre-
destined, originating from the Throne of God. When we see the
world convulsed with wars, famines, plagues and natural disas-
ters, we must say, with the Psalmist, “Come, behold the works
of the Lorp, who has wrought desolations in the earth” (Ps.
46:8). Ultimately, the Christian’s attitude toward God’s judg-
ments upon a wicked world is the same as that of the four living
creatures around the Throne, who joyfully call out to God’s
messengers of judgment: “Come!” We too, in our prayers, are
to plead with God to bring down His wrath on the ungodly, to
manifest His righteousness in the earth. Faced with these awe-
some revelations of judgment, what is our proper response? We
are told, in 22:17: The Spirit and the Bride say, “Come!”

The Martyrs Avenged (6:9-17)

9 And when He broke the Fifth Seal, I saw underneath the altar
the souls of those who had been slain because of the Word of
God, and because of the Testimony which they had maintained;

10 and they cried out with a loud voice, saying: How long, O
Lord, holy and true, dost Thou not judge and avenge our
blood on those who dwell on the Land?

11 And there was given to each of them a white robe; and they
wete told that they should rest for a litde while longer, until

192
