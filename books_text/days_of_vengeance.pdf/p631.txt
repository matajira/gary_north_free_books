THE LEVITICAL SYMBOLISM IN REVELATION

lamb, The clear bright gold of its streets means that God’s tabernacle
is built out of the pure in heart; this symbolism corresponds to that of
the white robes.

There was no sanctuary in it; that is to say, the Presence is not
localised. There is no alternation of light and dark upon it; no need to
calculate suns and moons; it lives in the perpetual light of the Pres-
ence. No seven-branched lamp needs to be kindled to burn through
the night; the Lamb is the lamp.

Through the lives of the elect souls in which God dwells the light
shall shine into the world. The community of the elect is wide open; its
gates are never shut. It has no national distinctions. The kings of the
earth bring their glory into it; a reference to the sacrifices offered by
Roman emperors and others at Jerusalem. The honour they gave to
that sanctuary shall come to this. Free to all shall be the waters and
fruits of the spiritual paradise.

No hereditary and monopolist priesthood shall have sole posses-
sion of this sanctuary and mediate between God and his people. All his
servants shall stand in his presence, and every one of them shall be like
the high priest, and have his name on their foreheads. Open universal
vision: open universal priesthood.

This epilogue builds up a picture of the Catholic church in which it
is contrasted at every point with the old Jewish temple, and shown to
be more glorious because every part of it is filled with the illumination
of the Presence which had been confined to the Holy of Hollies. St.
John deliberately avoids all the ornaments of temple worship— white
robes, golden girdles, harps, incense, altar; they are all gone. Note
also its square shape, its gates, and its living waters, which are all
taken from Ezekiel’s temple.

The Temple Sacrifice

We have gone through the later additions to St. John’s poem and
seen how illuminating it is to test them from the fiturgical point of
view; we now turn to the alder visions which are preserved within this
scaffolding.

Chapters J to 5 are new material which forms an introduction to
this older system; and no doubt older elements are to be found in
them. I have pointed out already how the High Priest is to be seen in
the vision of Christ in chapter 1, the sanctuary and its ormaments in
chapter 4, and the slain lamb in chapter 5.

Let me now outline the course of the daily burnt-offering at the
temple; it may be divided as follows:

597
