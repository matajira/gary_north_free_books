THE PRIMACY OF SYMBOLISM

For example, the symbolic number 666 (Rev. 13:18) clearly
refers to Nero Caesar; but if St. John had merely intended that
his readers should understand “Nero Caesar,” he would have
written “Nero Caesar,” not “666.”79 He used the number 666 be-
cause of an already established system of Biblical imagery that
allowed him to say a great many things about Nero simply by
using that number. As Philip Carrington says: “Many people ‘in-
terpret’ the Revelation . . . as if each detail of each vision hada
definable meaning which could be explained in so many words.
These commentators are rationalizers, deficient in the mystical
sense. Symbolism is a way of suggesting the truth about those
great spiritual realities which exclude exact definition or com-
plete systematization; that is why it is so much employed in wor-
ship. ... The symbol is much richer in meaning than any
meaning we can draw from it. The same is true of the parables
and symbolic teaching of Jesus. The same is true of the sacra-
ments and symbolic acts of the church, or even of society. Many
logical systems can be made up to explain the ‘meaning’ of shak-
ing hands or making the sign of the cross; but because of their
simplicity and universality these actions mean more than words
can explain,”#

Further, “the prophets in general use a great deal of hyper-
bole and picturesque exaggeration in the manner of Oriental
poetry. As the days of a tree shail be the days of my people Usa.
65:22). Yer destroyed I the Amorite whose height was like the
height of the cedars (Amos 2:9): statements which mean respec-
tively ‘very old’ and ‘very tall.’ It goes right back to primitive
poetry: The mountains skipped like rams... . The earth trem-
bled and shook (Ps. 114). Poets, even Western poets, will always
continue to use it. It includes the use of huge figures; a reign of
forty years means a good long reign, and a kingdom of a thou-
sand years means a good long kingdom. The poetry of Jesus has
it to a superlative degree; camels are swallowed or passed

79. The idea that he wrote it in “code” because he was afraid of being ar-
rested for treason is obviously false: The prophets were not timid men; and
anyway, the Book of Revelation is “treasonous” long before St. John gets
around to talking about Nero. Christians could be killed for saying simply
what Si. John says in Chapter I—that Jesus Christ is “the Ruler of the kings of
the earth.”

80. Philip Carrington, The Meaning of the Revelation, pp. B4f.

35
