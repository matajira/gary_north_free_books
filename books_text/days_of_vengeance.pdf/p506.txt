19:3-5 PART FIVE: THE SEVEN CHALICES

and their land (Deut. 32:23-43). Four times in this passage God
threatens that His vengeance will overtake the apostates: “Ven-
geance is mine, and retribution” (v, 35}; “I will render vengeance
on My adversaries, and I will repay those who hate Me” (v. 41);
“Rejoice, O nations, with His people; for He will avenge the
blood of His servants, and will render vengeance on His adver-
saries, and will atone for His land and His people” (v. 43).

3. In the second division of the song, the great multitude re-
peats the refrain: Hallelujah! The reason for praise is, again, a
godly rejoicing at the destruction of the Church’s enemy, for her
smoke rises up forever and ever. As we have noted (see on 14:11;
18:2, 9), this expression is based on the destruction of Sodom
and Gomorrah (Gen. 19:28), while the specific phraseology is
borrowed from Isaiah’s description of the punishment of Edom
(Isa. 34:10). It is used here to indicate the permanent nature of
Babylon’s fall.?

4 The third section of the liturgy finds the twenty-four
elders and the four living creatures—representing the Church
and all the earthly creation (see on 4:4-11)— taking up their dis-
tinctive part in the song. First, we are told, they fell down and
worshiped; again we notice the importance of posture, of physi-
cal attitude, in our religious activity. The modern Church’s
affliction of “spiritualistic” neoplatonism —not to mention sim-
ple laziness —has resulted in her all-too-casual approach to the
Most High. At the very least, our physical position in public,
official worship should be one that corresponds to the godly fear
and reverence which is appropriate in those who are admitted to
an audience with God who sits on the throne.

5 We are not told whose Voice pronounces the fourth sec-
tion of the liturgy from the Throne. It could be that of one of
the elders, leading the congregation from a position close to the
throne; but it is more likely to be that of Jesus Christ (cf. 16:17),
calling upon His brethren (Rom. 8:29; Heb. 2:11-12) to praise

2. The phrase thus cannot be pressed into service as a literal description of
the eternal state of the wicked in general. The actual flames that consumed
“Babylon” burned out long ago; but her punishment was eternal. She will
never be resurrected.

472
