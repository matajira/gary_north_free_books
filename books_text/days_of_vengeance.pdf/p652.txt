APPENDIX B

to the faith in a variety of cultural forms.

Unfortunately, for some, Messianic Judaism is seen as an alternative
to historic Christianity. This is due to the influence of pop-dispyism.
After all, if the Millennium is right around the corner, and Jewish cul-
ture will be imperialistically triumphant during the Millennium, then
even today Jewish practices anticipate that superiority. In fact, some
Messianic Jews apparently believe that they can claim unlimited finan-
cial support from Gentile Christians, because of this preeminence.!

Most af what 1] have written regarding Christian Zionism above
applies to this group of Messianic Jews. I should like, however, to call
attention to another facet of the matter. These Messianic Jews believe
wrongly that Gentile Christianity (the historic church) departed from
Biblical forms in the early days of the church. They see as their mis-
sion a restoration of these customs, which they believe they have pre-
served.

In fact, this is completely false. Anyone who has seen a presenta-
tion of “Christ in the Passover” is amazed at the number of non-
Biblical rites that are discussed and exhibited (the use of eggs, bread
broken in three pieces and hidden in cloth, etc.)., These customs arose
after the birth of the church, and do nat preserve Old Testament ritual
at all. Moreover, to try to place a Christian interpretation on the vari-
ous features of these rituals is most misguided and artificial. Clever as
such presentations are, they are grossly misleading.

As a matter of fact, the leading features of Temple and Synagogue
worship were brought straight into the church, as she spoiled the new
enemies of God: apostate Jewry. The period of this spoiling was a.p.
30 to a.p. 70, Once the church had completed her integration of the
spoils of the Old Covenant into her new, transfigured body, God de-
stroyed the remnants of the Old Covenant completely. Modern Jewish
Tituals and music owe far more to racial/cultural inheritance from the
peoples of Eastern Europe than they do to the Old Covenant.”

Thus, while there is nothing wrong with converted Jews maintain-
ing a cultural continuity with their past, there are no grounds for the
assumption that post-Christian Jewry has preserved the musical and

8. See Gary North, “Some Problems with ‘Messianic Judaism,’” in Biblical
Economics Today 7:3 (Apt./May, 1984).

9. Louis Bouyer has shown at considerable length that the eucharistic
prayer of the early church was a modification of the prayers of the Synagogue
and Temple. See Bouyer, Eucharist (Notre Dame: U. of Notre Dame Press,
1968}. Similarly, Eric Werner has shown that the plainchant of the Christian
church preserves the style of music known among the Jews of the Old Testa-
ment period. See Wemer, The Sacred Bridge (Columbia U. Press, 1959; the
paperback by Schocken only reproduces the first half of this important study).

620
