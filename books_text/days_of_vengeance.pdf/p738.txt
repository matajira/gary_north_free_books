THE DAYS OF VENGEANCE

Ascalon, 440
Ascension, 50, 69, 173, 576
definitive, 149
final, 149
progressive, 149
results of Christ's, 222-23, 284-85, 290, 316;
See aiso Jesus Christ, as Conqueror; Jesus
Christ, mediatorial reign of; Jesus Christ,
victory of
Asia Minor, 6-7
Asklepios, cult of, 105, 106
Assurance, 70
Assyria, 244
Athaliah, 308
Atonement, 34, 317. See also Death of Christ
Day of, 430, 446-47, 604, 607
limited, 377

Baalism, 157, 274, 285, 589
Babel, Tower of, 422
Babylon, 19, §9. Sae also Jerusalem
fall of, 362-64, 445-66, See aise Jerusalem,
destruction of
fall of, from Paradise, 239-40
the Great, 296, 416, 431
Harlot of, 114, 295, 363-64, 421-43
Teaction ta fall of, 452-39
word, 422
Babylonian Captivity, 300, 460
Balaam, 87, 90, 98, 101, 579
Balaamites, 98, 114
Balak, 87, 90, 107
Ban, 232
Baptism, 206, 384-85
resurrection and, 517-18
Baellium, 110
Beast, 20, 278, 325, 364, 408, 442, 529, See
aiso Roman Empire: Rome
appearance of, 327-29
beast imaged after, 336
Church vs., 279-80, 281, 438. See aiso
Woman
defeat of, 491-92
eighth, 436-37
explanation of, 433-35
Harlot atop, 428-29, 432
identity of, 438-36
from Land, 335-52
mark of, 30-31, 342, 355, 398
number of, 43, 344-52
rise of, 326-27, 434
scarlet, 428-29
from sea, 326-35, 342-43, 428-29
Beasts, 278-80
Daniel’s, 303, 306
two, 325-52, 491-92
unclean, 279

 

Bees, Killer, 129
Behemoth, 306
as beast from Lund, 342-43
defeat of, 491-92
Belial, 533
Bethlehem, 206
Bible, See Scripture
Bible Presbyterian Church, 664
Biblical theology, 37
Bilhah, 211
Birds of prey, 489-91
Blindness, 136
Blood, 390-92, 484
plagues of, 398-402
pouring of, 606-8
Boaz, 426
Book, 166-68, 181
bittersweet, 268
little, 261-62
Book of Life, 312, 334, 435, 533, 534, 569
controversy about, 122-25
Bottomless pit, 244. See also Abyss
Branch, Christ as, 170, 171
Bride, harlot, 381, 546, 552, See aso Harlot,
Great
jewels of, S60
Bride of Christ, 45, 89, 381-82, 463, 473-74,
545, 552, 554, $79, See also Church
a army of heaven, 484
British-Israelitism, 614
Buffalo Bill, 12
Bull, 188, 153
Ephmim as, 159
Bulls of Bashan, 279
Bumt offering, 232. See atsa Offering(s);
Sacrifice(s)

Caesar, Augustus, 7, 436
Caesarea, 440
‘Cassar, Julius, 7, 218, 436
Cacsars, 331-32, 436
deity assumed by, 7-9
‘Caesar, Tiberius, 330, 436
Caesar, Titus, 376, 439, 440, 455
‘Caesar-worship, 105, 112, See a’so Emperor
cult
Cain, 194, 307, 632
Caligula, 436
Call, effectual, 123
Calvinism, 122-23, 215
American, 624
Dutch, 624, 652-53
worship of, 137-38, 153-54
Canaan, 300
Canaanites, 236, 524, 631
Cannibalism, 402
Carmel, Mount, 4i1

706
