3:19-20 PART TWO: THE SEVEN LETTERS

duced “a very fine quality of world-famous black, glossy
wool”; '8 and its scientific community, renowned not only for its
prestigious medical school, but also for an eyesalve (called
“Phrygian Powder”) which had been well-known since the days
of Aristotle. Using these facts to illustrate the problems in the
church, Christ cites the general attitude of the Laodicean Chris-
tians: You say: I am rich, and have become wealthy, and have
need of nothing. In reality, despite the church’s wealth and
undoubted social standing, it was ineffectual, accomplishing
nothing for the kingdom of God. It is not a sin for a church (or
an individual) to be rich —in fact, God wants us to acquire wealth
(Deut. 8:18). What is sinful is the failure to use our resources for
the spread of the kingdom. When a relatively poor church such as
that at Smyrna (see Rev. 2:9) was having a rich effect upon its
community, there was no excuse for Laodicea’s impotence. Her
problem was not wealth, but disobedience: You do not know that
you are wretched and miserable and poor and blind and naked.

Yet, in grace, Christ makes an offer of mercy: I advise you to
buy from Me gold refined by fire, that you may become rich;
and white garments, that you may clothe yourself, and that the
shame of your nakedness may not be revealed; and eyesalve to
anoint your eyes, that you may see. The symbolism here should
be obvious. True faith and genuine works of obedience are
spoken of in Scripture in terms of jewelry, and especially gold
{1 Pet. 1:7; 1 Cor. 3:12-15); nakedness is symptomatic of disobe-
dience (Gen. 3:7), whereas being clothed in white robes is a sym-
bol of righteousness, with regard to both justification and sanc-
tification (Gen. 3:21; Matt, 22:11; Rev. 19:8); and blindness is a
symbol for man’s impotence and fallenness (Lev. 21:18; Deut.
29:4; Matt. 13:13-15; 16:3; 2 Cor. 4:3-4; 1 John 2:11) apart from
God’s restoration of him to true sight —the godly, mature ability
to judge righteous judgment (Luke 4:18; Acts 26:18; t Cor.
2214-15).

19-20 But Laodicea is not yet to be cast off by the Lord.
Harsh as His words are, He still professes His love for His Bride.
That, in fact, is the source of His anger: Because I love you, He

18. Charles F. Pfeiffer and Howard F. Vos, The Wycliffe Historical Geo-
Braphy of Bible Lands (Chicago: Moody Press, 1967), p. 377.

136
