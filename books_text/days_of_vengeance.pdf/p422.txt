15:5 PART FIVE: THE SEVEN CHALICES

Old Covenant fathers, as numerous passages abundantly attest.
For Thou alone art holy (Ex. 15:1]; 1 Sam. 2:2; Ps. 99:3, 5, 9;
Isa. 6:3; 57:5, 15; Hos. 11:9; cf. Matt. 19:17; 1 Tim. 6:16). God’s
“holiness” in Scripture often refers not so much to His ethical
qualities as to His unique majesty, His absolute transcendence
and “otherness.” Yet this very “unapproachableness” is here
stated to be the precise reason for His immanence, His nearness,
His accessibility to all peoples. The doctrine is declared positively:
For all the nations will come and worship before Thee, for Thy
righteous acts have been revealed (1 Chron. 16:28-31; Ps. 2:8;
22:27; 65:2; 66:4; 67:1-7; 86:8-9; 117:1; Isa. 26:9; 66:23; Jer.
16:19); the conversion of all nations is both the ultimate goal and
inevitable result of Gad’s judgments. The fall of Israel, St. John
is telling the Church, will bring about the salvation of the world
(and St. Paul extended the logic: Israel’s fall must therefore
eventually produce her own restoration to the covenant; Rom.
11:11-12, 15, 23-32).

The Sanctuary Is Opened (15:5-8)

5 After these things I looked, and the Temple of the Taber-
nacle of the Testimony in heaven was opened,

6 and the seven angels who had the seven plagues came out of
the Temple. They were clothed in linen, clean and bright,
and girded around their breasts with golden girdles.

7 And one of the four living creatures gave to the seven angels
seven golden bowls full of the wrath of God, who lives for-
ever and ever.

8 And the Temple was filled with smoke from the Glory of
God and from His power; and no one was able to enter the
Temple until the seven plagues of the seven angels were fin-
ished.

5 Now the scene changes, and we are shown the Temple of
the Tabernacle of the Testimony in heaven, the “¢rue Tabernacle”
(Heb. 8:2), the divine Pattern, of which the Tabernacle on earth
was a “copy and shadow” (Heb. 8:5; 9:1]-12, 23-24; 10:1; Ex.
25:9, 40; 26:30; Num. 8:4; Acts 7:44). St. John is very careful to
use correct technical expressions for his imagery here, based on
the Old Covenant order. The basic treaty document of the Cove-
nant was the Decalogue; this was often called the Testimony,
emphasizing its legal character as the record of the Covenant

388
