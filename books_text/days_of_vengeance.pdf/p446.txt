16:17 PART FIVE: THE SEVEN CHALICES

for the Messiah: After promising to “destroy all the nations that
come against Jerusalem” (Zech. 12:9}, God says:

And I will pour out on the house of David and on the inhab-
itants of Jerusalem the Spirit of grace and of supplication, so
that they will look on Me whom they have pierced; and they will
mourn for Him, as one mourns for an only son, and they will
weep bitterly over Him, like the bitter weeping over a first-born,
In that day there will be great mourning in Jerusalem, like the
mourning of Hadadrimmon in the plain of Megiddo. And the
Land will mourn, every family by itself... . (Zech. 12:10-11)

This is then followed by God’s declaration that He will re-
move from Israel the idols, the false prophets, and the evil spir-
its (Zech. 13), and that He will bring hostile armies to besiege
Jerusalem (Zech, 14).2#

“Megiddo” thus was for St. John a symbol of defeat and des-
olation, a “Waterloo” signifying the defeat of those who set
themselves against God, as Farrer explains: “In sum, Mt.
Megiddo stands in his mind for a place where lying prophecy
and its dupes go to meet their doom; where kings and their
armies are misted to their destruction; and where all the tribes of
the earth mourn, to see Him in power, whom in weakness they
had pierced.”

17 Finally, the seventh angel pours out his Chalice upon the
alr. The reason for this does not seem to be that the air is the do-
main of Satan, “the prince of the power of the air” (Eph. 2:2),
but rather that it is the element in which the lightning and thun-
der (v. 18) and hail (v. 21) are to be produced. Again a Voice
comes from the Temple of heaven, from the Throne, signifying
God’s control and approval. St. John told us in 15:1 that these
seven plagues were to be “the last, because in them the wrath of
God is finished”; with the Seventh Chalice, therefore, the Voice

28. Carrington (pp. 268-71) provides an extensive list of St. John’s allusions
to Zechariah, observing that “next to Ezekiel it has influenced St. John mast.
It is important to realize, therefore, that it speaks of the destruction of this
Jerusalem and a vengeance upon its inhabitants; it looks forward to the glory
of a New Jerusalem under the house of David, and the gentiles coming to wor-
ship there” (p. 271).

29. Farrer, p. 178.

412
