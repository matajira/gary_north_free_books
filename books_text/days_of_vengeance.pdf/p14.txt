THE DAYS OF VENGEANCE

mentators— especially Philip Carrington, Austin Farrer, J. Mas-
syngberde Ford, Meredith G. Kline, J. Stuart Russell, Moses
Stuart, Henry Barclay Swete, and Milton 8. Terry—and I hope I
have done justice to them in building on their foundation. Yet I
am painfully aware that the task of commenting on St. John’s
magnificent prophecy far exceeds my abilities. Where I have
failed adequately to set forth the message of the Revelation, [
beg the indulgence of my brothers and sisters in Christ, and
earnestly desire their comments and corrections. Letters may be
addressed to me at P.O. Box 2314, Placerville, CA 95667.

My beloved wife, Darlene, has always been my greatest
source of encouragement. Our children (Nathan David, Jacob
Israel, and Abigail Aviva} endured our collective “exile to Pat-
mos” with true Johannine grace (mixed, perhaps, with occa-
sional rumblings of Boanergean thunder as well!); and if their
bedtime stories were somehow filled with more than the usual
quota of cherubs, dragons, flying horses, and flaming swords,
they never complained.

Finally, I am grateful to my parents, the Rey. and Mrs.
Harold B. Chilton. 1 was immeasurably blessed to grow up in a
home where the Word of God is so highly honored, so faithfully
taught, so truly lived. The environment they structured was con-
stantly flooded with musical grandeur and richness, as the
atmosphere was charged with rousing theological discussion, all
in the context of caring for the needy, sheltering the homeless,
feeding the hungry, and bringing to all the precious message of
the Gospel. From the steaming jungles and rice paddies of the
Philippines to the shaded lawns of Southern California, they set
before me a remarkable and unforgettable example of what it
means to be bondservants of the Lord. Some of my earliest
memories are of seeing my parents’ faith tested beyond what
seemed the limits of human endurance; and when God had tried
them, they came forth as gold. Holding forth the Testimony of
Jesus, suffering the loss of all things in order to win Christ, they
are what St. John has exhorted us all to be: faithful witnesses.

This book is dedicated to them,

David Chilton
Tyler, Texas
May 8, 1986
Ascension Day
