KING OF KINGS i:l-15

And myriads upon myriads were standing before Him;
The court sat,
And the books were opened. (Dan. 7:9-10)

T kept looking in the night visions,

And behold, with the Clouds of heaven
One like a Son of Man was coming,

And He came up to the Ancient of Days
And was presented before Him.

And to Him was given dominion,

Glory and a Kingdom,

That all the peoples, nations, and men of every language
Might serve Him.

His dominion is an everlasting dominion
Which will not pass away;

And. His Kingdom is one

Which will not be destroyed. (Dan. 7:13-14)

I lifted my eyes and looked, and behold, there was a certain
man dressed in linen, whose waist was girded with a belt of pure
gold of Uphaz. His body also was like beryl, His face like light-
ning, His eyes were like flaming torches, His arms and feet like
the gleam of polished bronze, and the sound of His words like
the sound of a multitude. Now, I, Daniel, alone saw the vision,
while the men who were with me did not see the vision; neverthe-
less, a great dread fell on them, and they ran away to hide them-
selves. So I was left alone and saw this great vision; yet no
strength was left in me, for my natural color turned to a deathly
pallor, and [ retained no strength. But 1 heard the sound of His
words; and as soon as I heard the sound of His words, I fell into
a deep sleep on my face, with my face to the ground. Then be-
hold, a hand touched me and set me trembling on my hands and
knees. And He said to me, “O Daniel, man of high esteem, un-
derstand the words that I am about to tell you and stand upright,
for I have now been sent to you.” And when He had spoken this
word ta me, I stood up trembling. (Dan. 10:5-11)?9

These and other passages are combined to form the picture
of Christ in St. John’s introductory vision. The robe reaching to
His feet and the golden sash around His chest?° (cf. Ex. 28:4;

29. Cf. the discussion of this text in relation to Rev, 12:7-9 below.
30. According to Josephus, the priest wore the sash around his chest when he
was at rest and “not about any laborious service” (Antiquities of the Jews, iii.vii.2).

73
