THE KING ON MOUNT ZION 14:19-20

1 will break down its wall and it will become trampled ground.
And I will lay it waste;

It will not be pruned or hoed;

But briars and thorns will come up.

1 will also charge the clouds to rain no rain on it.

For the Vineyard of the Lorp of hosts is the House of Israel,
And the men of Judah His delightful plant.

Thus He looked for justice, but behold, bloodshed;

For righteousness, but behold, a cry of distress. (Isa. 5:1-7)

19-20 The Vineyard is judged: The angel threw his sickle to
the Land, and gathered the vine of the Land, and threw it into
the great wine press of the wrath of God to produce the sub-
stance that will be poured from the chalices in Chapter 16. The
repeated references to the Land (six times in verses 15-19), com-
bined with the imagery of the vine of the Land, emphasize that
this is a judgment on the Land of Israel. Reviewing the extensive
Biblical background of the vineyard idea, Carrington con-
cludes: “It does not seem possible to suppose that St. John could
have intended to apply these words to any other country than
Israel, or to any other city than Jerusalem. They echo the words
of St. John the Baptist, with which the whole Christian pro-
phetic movement began, Even now is the axe laid to the root of
the tree. What is contingent in the Baptist is absolute in Revela-
tion. Israel is rejected.”2

The imagery of this passage is based on Isaiah’s prophecy of
the destruction of Edom, where God is described as a man
crushing grapes in a wine press, He explains why His robe is
stained with “juice”:

1 have trodden the wine trough alone,

And from the peoples there was no man with Me.
Talso trod them in My anger,

And trampled them in My wrath;

And their juice is sprinkled on My garments,

And I stained all My raiment,

For the Day of Vengeance was in My heart,

And My year of redemption has come.

And I looked, and there was no one to help,

20. Carrington, p. 256, On Christ’s use of vineyard imagery in His parables,
see Chilton, Paradise Restored, pp. 76-82.

375
