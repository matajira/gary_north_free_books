APPENDIX C

domness, this chaos philosophy makes them confident. The Christian
is humble before God, but confident before the creation which he is to
subdue. This confidence leads the Christian into defeat and ultimate
disaster, say amillennialists, who believe in increasing epistemological
self-consciousness. On the other hand, the rebel is arrogant before
God and claims that all nature is ruled by the meaningless laws of
probability—ultimate chaos, By immersing themselves in the philoso-
phy of chaos, the unbelievers are able to emerge totally victorious
across the whole face of the earth, says the amillennialist, a victory
which is called to a halt only by the physical intervention of Jesus
Christ at the final judgment. A commitment to lawlessness, in the
amillennial version of common grace, leads to external victory. How
can these things be?

Amillennialism Has Things Backwards

Tt should be clear by now that the amillennialist version of the rela-
tionship between biblical law and the creation is completely back-
wards. No doubt Satan wishes it were a true version. He wants his fol-
lowers to believe it. But how can a consistent Christian believe it?
How can a Christian believe that adherence to biblical law produces
cultural impotence, while commitment to philosophical chaos —the re-
ligion of satanic revolution—leads to cultural victory? There is no
doubt in my mind that the amillennialists do not want to teach such a
doctrine, yet that is where their amillennial pessimism inevitably leads.
Dutch Calvinists preach the cultural mandate (dominion covenant),
but they simultaneously preach that it cannot be fulfilled. But biblical
taw is basic to the fulfillment of the cultural mandate. Therefore, the
amillennialist who preaches the obligation of trying to fulfil the cul-
tural mandate without biblical law thereby plunges himself either into
the camp of the chaos cults (mystics, revolutionaries) or into the camp
of the natural-law, common-ground philosophers. There are only four
possibilities: revealed law, natural law, chaos, or a mixture.

This leads me to my next point. Jt is somewhat speculative and
may not be completely accurate, It is an idea which ought to be pur-
sued, however, to see if it is accurate. 1 think that the reason why the
philosophy of Herman Dooyeweerd, the Dutch philosopher of law,
had some temporary impact in Dutch Calvinist intellectual circles in
the late 1960s and earty 1970s is that Dooyeweerd’s theory of sphere
sovereignty — sphere laws that are not to be filled in by means of re-
vealed, Old Testament law —is consistent with the amillennial (Dutch)
version of the cultural mandate. Dooyeweerd’s system and Dutch
amillennialism are essentially antinomian. This is why 1 wrote my 1967
essay, “Social Antinomianism,” in response to the Dooyeweerdian

652
