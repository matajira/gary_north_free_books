FOREWORD

Readers of the Book of Revelation are either mesmerized or
mystified by it. The mesmerized come up with such startling in-
terpretations that the mystified often conclude that sober-
minded Christians should leave the book well alone.

David Chilton’s commentary ought to be studied by both
types of reader. He shows that Revelation is a book, like every
other book of the New Testament, addressed primarily to the
first-century church and easily understood by them, because
they were thoroughly familiar with Old Testament imagery. He
shows that once we grasp these idioms, Revelation is not diffi-
cult for us to understand either.

Revelation remains, though, a challenging and relevant
book for us, not because it gives an outline of world history with
special reference to our era, but because it shows us that Christ
is in control of world history, and how we should live and pray
and worship. In vivid powerful imagery it teaches us what it
means to believe in God’s sovereignty and justice. May this valu-
able commentary prompt us to pray with John and the universal
church in heaven and on earth, ‘Even so come, Lord Jesus!’

Gordon Wenham
The College of St.
Paul and St. Mary
Cheltenham, England
