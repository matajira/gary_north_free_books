PUBLISHER'S PREFACE

be a few academic specialists who will respond competently to
this or that point in The Days af Vengeance, but their technical
essays will not be read widely, especially by the average pastor or
layman. There may also be one or two theologians who attempt
to respond comprehensively (though I doubt it), but their mud-
dled expositions will win few new followers. (I have in mind a
particular amillennial scholar who is known for his unique in-
sights into Biblical symbolism, but whose writings communicate
his ideas with the clarity of Zen Buddhist thought-teasers or
Alexander Haig’s press conferences.)

Mainly, they face the tactical problem of calling attention to
this book within their hermetically sealed followings. If their
followers ever sit down and read The Days of Vengeance, Chris-
tian Reconstructionism will pick off the best and the brightest of
them. Why? Because earthly hope is easier to sell than earthly
defeat, at least to people who are not happy to accept their con-
dition as historical losers. A lot of Christians today are tired of
losing. Even if it means starting to take responsibility — and that
is precisely what dominion theology means—a growing number
of bright, young Christians are ready to pay this price in order
to stop losing. Thus, any extended discussion of this book be-
comes a recruiting device for Christian Reconstructionism. Too
many bright, young readers will be tipped off to the existence of
dominion theology.

Our opponents know this, so I do not expect to see any sys-
tematic effort to refute Chilton on eschatology, any more than
we have seen a book-long effort to refute Greg Bahnsen’s
Theonomy in Christian Ethics 1977)" or R, J. Rushdoony’s Z#-
stitutes of Biblical Law (1973). The potential critics have had
plenty of time; they have not had plenty of definitive answers. I
believe the reason is that the Bible’s case for the continuing
standard of Biblical! law is too strong. Our opponents would
prefer that we remain silent and stop raising these difficult ethi-
cal questions. Our opponents are caught in a major dilemma. If
they continue to fail to respond, their silence becomes a public
admission of intellectual defeat. If they do respond, we have an

12, 2nd edition, 1984, Published by Presbyterian & Reformed, Phillipsburg,
New Jersey.
13. Nutley, New Jersey: Craig Press, 1973.

XXV
