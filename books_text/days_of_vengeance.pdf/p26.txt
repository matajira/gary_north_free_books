PUBLISHER’S PREFACE

opportunity to reply—and the replies are where the academic
debating points are always scored. When you fail to respond
effectively to the replies, you lose the debate. Our opponents
understand the rules of the academic game. They do not begin
the confrontation.

At the same time, they need our insights in order to make
sense of at least parts of the Bible. I have seen copies of Rush-
doony’s Institutes for sale in the Dallas Theological Seminary
Bookstore. They need his insights on Biblical law, yet they can-
not deal with the underlying theology of his book. They simply
dismiss him as somehow unimportant on such issues. They pre-
tend that he has not offered a monumental challenge to dispen-
sational ethics.'* They pretend that they can successfully use his
book as a kind of neutral reference work on the Old Testament
case laws, and also somehow avoid losing their most energetic
students to the Christian Reconstructionist movement. The ca-
reer of Pastor Ray Sutton (a graduate of Dallas Theological
Seminary) indicates that they have made a mistake.

In a popularly written essay for a non-Christian audience,
two fundamentalist authors insisted that while R. J. Rushdoony’s
insights on education and politics are used by fundamentalists,
they do not take his kingdom views seriously. When their Chris-
tian schools are brought to court by some arrogant state attor-
ney general, they call in Rushdoony to take the witness stand for
the defense, This has been going on since the mid-1970’s, They
need him. They know they need him. Yet his two fundamentalist
critics went on to say that hardly anyone in the Christian world
takes his views on the kingdom of God seriously. “Fortunately,
we can say with confidence thai he represents a very small group
with absolutely no chance of achieving their agenda.”'s

In terms of numbers, they were correct: The Christian
Reconstruction movement is small, In terms of young men who

14, The one book-length attempt of any dispensationalist scholar to refute
theonomists is an unpublished Dailas Theologica! Seminary doctoral disserta-
tion: Ramesh Paul Richard’s Hermeneutical Prolegomena to Premillennial
Social Ethics (1982). It has not been published even in a reworked form. It is
understandable why not: a terrible tille, Worse, the dissertation gave away too
much theological ground to the theonomists. This indicates the crisis facing
dispensationalism today.

15, Ed Dobson and Ed Hindson, “Apocalypse Now?” Policy Review (Oct.
1986), p. 20.

xxvi
