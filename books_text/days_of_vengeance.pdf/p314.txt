Ih? PART FOUR: THE SEVEN TRUMPETS

its various historical manifestations. The prophets often spoke
of pagan states as terrifying beasts that warred against the Cove-
nant people (Ps. 87:4; 89:10; Isa. 51:9; Dan. 7:3-8, 16-25). All
this will be gathered together in St. John’s description of Rome
and apostate Israel in Revelation 13. Yet we must remember that
these persecuting powers were but the immediate manifestations
of the agelong enemy of the Church—the Dragon, who is for-
mally introduced in 12:3,'' but who was well-known to any Bib-
lically literate person in St. John’s audience. The Christians al-
ready knew the ultimate identity of the Beast who arises from
the Abyss. It is Leviathan, the Dragon, the Serpent of old, who
comes out of his prison in the sea again and again to plague the
people of God. The Abyss, the dark, raging Deep, is where
Satan and his evil spirits are kept imprisoned except for periodic
Teleases in order to torment men when they commit apostasy.”
(Note that the legion of evil spirits in the Gadarene demoniac
pleaded to be kept out of the Abyss; with divine deception,
Jesus sent them into the herd of swine, and the swine rushed
headlong into the sea: Luke 8:31-33). The persecution of the
Covenant people is never a merely “political” contest, regardless
of how evil states attempt to color their wicked actions. It al-
ways originates in the pit of hell.

Throughout the history of redemption, the Beast made war
against the Church, particulariy against its prophetic witnesses.
The final example of this in the Old Covenant period is the war
of Herod against John the Forerunner, whom he overcame and
killed (Mark 6:14-29); and the culmination of this war against
the prophets was the murder of Christ, the final Prophet, of
whom all the other prophets were images, and whose testimony
they bore. Christ was crucified by the collaboration of Roman
and Jewish authorities, and this partnership in persecution con-
tinued throughout the history of the early Church (see Acts
17:5-8; 1 Thess. 2:14-17).23

ll. Closely related to che Biblical doctrine of the Beast is the Bible’s “dino-
saur theology”; for this, see my comments on 12:3.

12. See above on 9:)-6.

13. The Beast’s attempt to erase the testimony of God's witnesses eventually
led to its attack on the land of Israel, the birthplace of the Church; Titus sup-
posed that he could destroy Christianity by destroying the Temple in a.p. 70
(see on 17:14). The central religious motive behind the Roman war against the
Jews was its deeply rooted hatred for the Christian Church.

280
