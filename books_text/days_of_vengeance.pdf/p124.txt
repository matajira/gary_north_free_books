PART TWO; THE SEVEN LETTERS

3. Pergamum: Judgment on the Evil King and False
Prophet (2:12-17). This church is experiencing persecution and
temptation from the first-century counterparts of “Balak,” the
evil king of Moab, and the false prophet “Balaam.”

4. Thyatira: Judgment on the Royal Harlot (2:18-29). The
leader of the heretics, who entices God’s servants into idolatry
and fornication, is named after Jezebel, the adulterous queen of
ancient Israel.

The cycle now begins over again, so that these first four mes-
sages are “recapitulated” in the last three, but with attention to
different details. To understand this, we must start from the first
message again. St. John’s descriptions of Christ in the preamble
to each message are drawn from those in the vision of the Son of
Man in Chapter 1. But his order is chiastic (that is, he takes up
each point in reverse order). Thus:

The Vision of the Son of Man
A. His eyes were like a flame of fire, and His feet were like burnished
bronze (1:14-15).
B. Out of His mouth came a sharp two-edged sword (1:16),
C. Lam the First and the Last, and the Living One; and 1
was dead, and behold, 1 am alive forevermore, Amen; and
I have the keys of death and of Hades (1:17-18).
D. The mystery of the seven stars that you saw in My
right hand, and the seven golden lampstands (1:20).
The Letters to the Seven Churches
D. Ephesus The One who holds the seven stars in
His right hand, the One who walks among the seven
golden lamipstands (2:1).
C. Smyrna The First and the Last, who was dead, and has
come to life (2:8).
B. Pergamum The One who has the sharp two-edged sword
(2:12),
A. Thyatira The Son of God, who has eyes tike a flame of fire, and
His feer are like burnished bronze (2:18).
D. Sardis He who has the seven Spirits of God, and
the seven stars (3:1).
C. Philadelphia He who is holy, who is true, who has the
key of David, who opens and no one will shut, and who
shuts and no one will open (3:7).
C. Laedicea The Amen, the faithful and true Witness, the

90
