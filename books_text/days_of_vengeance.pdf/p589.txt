THE NEW JERUSALEM 21:12-14

stitute a city wall”; !9 this is made explicit in St. John’s account.
The twelve gates of the City are guarded by twelve angels (cf.
the cherubim who guarded Eden’s gate in Gen. 3:24), and are in-
scribed with the names... of the twelve tribes of the sons of
Israel, another feature in common with Ezekiel’s vision (Ezek,
48:31-34). Sweet comments: “The twelve portals of the Zodiac
in the city of the heavens are brought under the control of the
Bible: Israel is the nucleus of the divine society.”?¢

The City has three gates on the east and three gates on the
north and three gates on the south and three gates on the west.
We saw in the discussion of 7:5-8 that the twelve tribes of Israel
are listed by St. John (and before him, by Ezekiel) in such a way
as to “balance” the sons of Leah and Rachel. The order in which
the gates are listed (east, north, south, west) corresponds to this
tribal list—which we would naturally expect, since St. John
mentions the gates, with their unusual order, immediately after
mentioning the twelve tribes. In other words, he intends for us
to use the information in this verse in order to go back and solve
the riddle of 7:5-8 (see the charts on pp. 210-11).

There is another intriguing point about this verse: St. John
tells us that the gates are, literally, from the east, from the
north, from the south, and from the west — giving, as Sweet
suggests, “the picture of many coming from the four points of
the compass (Isa. 49:12; Luke 13:29).”2) As St. John later shows,
the nations will walk by the City’s light, the kings of the earth
will bring their wealth into her, and her gates will always be open
to them (v. 24-26).

St. John extends his imagery: The wall of the City had twelve
foundation stones, and on them were the names of the twelve
apostles of the Lamb. This, of course, is straight Pauline theol-
ogy: “So then, you are no longer strangers and aliens, but you
are fellow citizens with the saints, and are of Gad’s household,
having been built upon the foundation of the apostles and
prophets, Jesus Christ Himself being the Cornerstone, in whom
the whole building, being fitted together is growing into a holy
Temple in the Lord; in whom you also are being built together

19. Ford, p. 341.
20. Sweet, p. 304.
21, Ibid.

555
