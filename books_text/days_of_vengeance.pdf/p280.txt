9:7-12 PART FOUR: THE SEVEN TRUMPETS

Because of Israel’s rejection of the King of kings, the bless-
ings they had received would turn into curses. Jerusalem had
been “swept clean” by Christ’s ministry; now it would become “a
dwelling place of demons and a prison of every unclean spirit,
and a prison of every unclean and hateful bird” (18:2). The en-
tire generation became increasingly demon-possessed; their pro-
gressive national insanity is apparent as one reads through the
New Testament, and its horrifying final stages are depicted in
the pages of Josephus’ The Jewish War: the loss of all ability to
reason, the frenzied mobs attacking one another, the deluded
multitudes following after the most transparently false proph-
ets, the crazed and desperate chase after food, the mass mur-
ders, executions, and suicides, the fathers slaughtering their
own families and the mothers eating their own children. Satan
and the host of hell simply swarmed throughout the land of
Israel and consumed the apostates.

The vegetation of the earth is specifically exempted from the
destruction caused by the “locusts.” This is a curse on disobe-
dient men. Only the Christians are immune to the scorpion-like
sting of the demons (cf. Mk. 6:7; Lk. 10:17-19; Acts 26:18); the
unbaptized Israelites, who do not have the seal of God on their
foreheads (see on 7:3-8), are attacked and tormented by the
demonic powers. And the immediate purpose God has in un-
leashing this curse is not death, but merely torment, misery and
suffering, as the nation of Israel was put through a series of
demoniac convulsions. St. John repeats what he has told us in
6:16, that in those days men will seek death and will not find it;
and they will long to die and death shall flee from them. Jesus
had specifically prophesied this longing for death among the
final generation, the generation of Jews which crucified Him
(Lk. 23:27-30). As the Wisdom of God had said long before:
“He who sins against Me wrongs his own soul; all those who
hate Me love death” (Prov. 8:36).

7-12 The description of the demon-locusts bears many simi-
larities to the invading heathen armies mentioned in the proph-
ets (Jer. 51:27; Joel 1:6; 2:4-10; cf. Lev. 17:7 and 2 Chron. 11:15,
where the Hebrew word for demon is hairy one). This passage
may also refer, in part, to the Satanic gangs of murderous
Zealots that preyed on the citizens of Jerusalem. As Josephus
tells us, the people had more to fear from the Zealots than from

246
