IN THE PATH OF THE WHITE HORSE 6715-17

ment (Gen. 1:16); they are also clocks (Gen. 1:14), and their fall
shows that israel’s time has run out: The stars fell to the earth,
as a fig tree casts its unripe figs when shaken by a great wind
(Job 9:7; Eccl. 12:2; Isa. 13:10; 34:4; Ezek. 32:8; Dan. 8:10; Joel
2:10; 3:15); the great wind, of course, was brought by the Four
Horsemen, who in Zechariah’s original imagery were the Four
Winds (Zech. 6:5), and who will be reintroduced to St. John in
that form in 7:1; and the fig tree is Israel herself (Matt. 21:19;
24:32-34; Luke 21:29-32), Fifth, Israel now simply disappears:
The heaven vanished like a scroll when it is rolled up?! (Isa.
34:4; 51:6; Ps. 102:25-26; on the symbolism of Israel as
“heaven,” see Isa. 51:15-16; Jer. 4:23-31; cf. Heb 12:26-27). Sixth,
the Gentile powers are shaken as well; Every mountain and
island were moved out of their places (Job 9:5-6; 14:18-19;
28:9-11; Isa. 41:5, 15-16; Ezek, 38:20; Nah. 1:4-8; Zeph. 2:11).22
Goed’s “old creation,” Israel, is thus to be de-created, as the
Kingdom is transferred to the Church, the New Creation (cf. 2
Pet. 3:7-14). Because the rulers in God’s Vineyard have killed
His Son, they too will be killed (Matt. 21:33-45). The Vineyard
itself will be broken down, destroyed, and laid waste (Isa,
5:1-7). In God’s righteous destruction of Israel, He will shake
even heaven and earth (Matt. 24:29-30; Heb. 12:26-28) in order
to deliver His Kingdom over to His new nation, the Church.

15-17 Old Testament prophetic imagery is still in view as St.
John here describes the apostates under judgment. This is the
seventh phase of de-creation: the destruction of men. But this

21. Referring to the Biblical imagery (cf. Gen. 1:7) of a “solid” sky, Ford ex-
plains: “Heaven’s having been ‘wrenched apart like a scroll that is rolled up’
leads to an image not of a papyrus or leather roll but rather a scroll like the
two copper ones found in Qumran. The idea of noise is conveyed more dra-
matically if the reader is meant to picture a metal scroll suddenly snapping
shut.” J. Massyngberde Ford, Revelation: Introduction, Translation, and
Commentary (Garden City, NY: Doubleday and Co., 1975), p. 100.

22. In contrast to popular interpretations of the texts which speak of faith
moving mountains (Matt. 17:20; 21:21; Mark 11:23), it should be noted that
this expression occurs in passages which speak of the coming judgment upon,
and fail of, apostate Jerusalem. Jerusalem is often called “the mountain” in
Scripture (e.g. Dan. 9:16); rhus the saints at the altar (6:9-11) are pictured as
erying out, in faith, for this great mountain to fall down. Jerusalem's destruc-
tion is accordingly portrayed, in part, as a burning mountain being cast into
the sea (8:8; cf. Zech. 14:4).

197
