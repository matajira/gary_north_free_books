INTRODUCTION

all other “sources” are simply quoting from him. It is thus rather
disingenuous for commentators to claim, as Swete does, that
“Early Christian tradition is almost unanimous in assigning the
Apocalypse to the last years of Domitian.”)!2 Certainly, there
are other early writers whose statements indicate that St. John
wrote the Revelation much earlier, under Nero’s persecution.13

A good deal of the modern presumption in favor of a Domi-
tianic date is based on the belief that a great, sustained period of
persecution and slaughter of Christians was carried on under his
rule. This belief, as cherished as it is, does not seem to be based
on any hard evidence at all. While there is no doubt that Domi-
tian was a cruel and wicked tyrant (I come to bury a myth about
Caesar, not to praise him), until the fifth century there is no men-
tion in any historian of a supposedly widespread persecution of
Christians by his government. It is true that he did temporarily
banish some Christians; but these were eventually recalled. Rob-
inson remarks: “When this limited and selective purge, in which
no Christian was for certain put to death, is compared with the
massacre of Christians under Nero in what two early and entirely
independent witnesses speak of as ‘immense multitudes,”* it is
astonishing that commentators should have been led by irenaeus,
who himself does not even mention a persecution, to prefer a
Domitianic context for the book of Revelation.”!*

Our safest course, therefore, must be to study the Revelation
itself to see what internal evidence it presents regarding its date.
As we will see throughout the commentary, the Book of Revela-
tion is primarily a prophecy of the destruction of Jerusalem by
the Romans. This fact alone places St. John’s authorship some-
where before September of a.p. 70. Further, as we shall see, St.
John speaks of Nero Caesar as still on the throne—and Nero
died in June 68.

12. H. B. Swete, Commentary an Revelation (Grand Rapids: Kregel Publi-
cations, [1911] 1977), p. xcix.

13. See the detailed discussion in Moses Stuart, Commentary on the Apoca-
dypse (Andover: Allen, Morrill and Wardwell, 1845), Vol. I, pp. 263-84; see
also James M. MacDonald, The Life and Writings of St. John (London: Hod-
der and Stoughton, 1877), pp. 151-77.

14. Robinson has in mind the statements of the Christian pastor St. Clem-
ent (7 Clement 6) and the heathen historian Tacitus (Annals xv.44),

15, Robinson, p, 233; cf. pp. 236ff.

4
