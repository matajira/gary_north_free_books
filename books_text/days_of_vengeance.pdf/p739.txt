SUBJECT INDEX

Celibacy, 356
Cestius, 252-53, 258
Chaldeans, 103
Chalices, seven, 17, 169, 192, 379-580
fifth of, 405-7
fiest of, 398
fourth of, 402-4
Jast of, 412-19
negative sacramental character of, 38490
second of, 398-400
sinth of, 407-42
third of, 400-402
Chaos, 115-16
Chemubim, 150, 155-56, 158
Chiliasm, See Millenarianism
Chosen, The (film), 613
Christianity, orthodox, 584-85, 589, See also
Christians
Messianic Judaism and, 620-21
millenarianism and, 494-97
pluralism and, 496.97, 585
Christian Reformed Church (CRO), 624
Christians. See afso Church
attitude of, toward judgment, 192, 193. See
also Suffesing; Tribulacion(s)
as conquerors, $49
faithful, 104, 116, 118
judgment experienced by, 236-37
leadership by, 659-61
sufferings of, 118, 126, 127, 193
uibulation and first-century, 220-21
“Christ in the Passover,” 620
Church, 81-82, 147, 325, 585-86. See also Bride
of Christ
Beast against, 279-80, 281, 438. See alsa
Rome, church vs,
hheasts and, 279-80
Dragon assaults, 319-24. See aiso Womat
end of age of, $6-$7
at Ephesus, 93-99
Eucharist in, 475-78. See afso Eucharist
false, 244
gates of hell vs., 313-14
Glory-cloud in, 582, See also Glory-cloud
government of, 231. See also elders
Israel's enmity toward, 40, 103, 106
kingdom and, 20. See also Kingdom of God
at Laadicea, 132-39
as New Jerusalem. See Jerusalem, New
at Pergamum, 104-11
at Philadelphia, 125-32
preservation of, 201-2, 322
response of, to fall of Jerusalem, 458-59
Rome vs., 8-9. See also Beast, church vs.
royal priesthood of, 308-9, 510. See also
priests, kingdom of
at Sardis, 119-25

at Smyrna, 99-104
succession of, 17
survival of, 353
at Thyatira, 11-18
as crue Israel, 128, 214. See also Israel, new
as true Temple, 291-93, 392-93, 546-47. See
also Temple, church as
unity of, 82
universal, 213-16
worship of, 60-61, 162-64, 317-18, 332-33,
469-80, See aise Liturgy
Church age, errors about, 56-57, 615-616, Se
also Dispensacivnatisn
‘Churches, seven, 17, 41, 81
angels of, 81
covenantal form of letters to, 85-86
errors about, 5$-57
Jetters to, 85-144
Old Covenant history and, 86-89
Revelation’s structure in letters to, 89-95
Church Fathers, 37, 333, 503-4
Church history, 56, See alsa history
world history related to, 232
Church Year, 22
Circus Maximus, 224
Claudius, 241, 436
Cloud, glory. See Glory-cloud
Clouds, coming in, 64-67
Colon, 153
Coming of Christ, First. See First Coming of
‘Christ
Commandments of Christ. See Law of Gad;
Tea Commandments
Commerce, of Jerusalem, 454-56
Commodus, 656
Common grace, 498n, $27-28, 623-64
background of debate on, 624-25
common ground vs., 661-62
cooperation and, 658-60
final judgment and, 646-48
future grace and, 663
law of God and, 629-34, 647-48
‘Van Til's amillennial view of, 637-40, 651-32
Common ground vs. common grace, 661-62
Communion, Holy. See Eucharist
Confession, of Christ, 125
Constantinople, Council of, 58
‘Cooperation, 660
self-consciousness and, 651-52
with unregenerate, 6$8-59
Comerstone, Christ the, 555-56
Cosmic petsonalism, 204
Covenant, See aiso Covenant, New; Covenant,
Old
‘copies of, 167
cteation and, 266-67
curses of, 17-19, 89, 190, 191, 489
description of, 14

 

107
