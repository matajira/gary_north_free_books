APPENDIX C

the final wrath of God upon their heads, even as they gain external
blessings due to their increased conformity to the external require-
ments of biblical law. At the end of time, they revalt.

The unregenerate have two choices: Conform themselves to biblical
law, or at least to the work of the law written on their hearts, or, second,
abandon law and thereby abandon power. They can gain power only
on God’s terms: acknowledgement of and conformity to God’s law.
There is no other way. Any turning from the law brings impotence,
fragmentation, and despair. Furthermore, it leaves those with a com-
mitment to law in the driver’s seat. Increasing differentiation over
time, therefore, does not lead to the impotence of the Christians. it
leads to their victory culturally. They see the implications of the law
more clearly, So do their enemies. The unrighteous can gain access to
the blessings only by accepting God’s moral universe as it is.

The Hebrews were told to separate themselves from the people and
the gods of the land. Those gods were the gods of Satan, the gods of
chaos, dissolution, and cyclical history. The pagan world was faithful
to the doctrine of cycles: there can be no straight-line progress. But the
Hebrews were told differently. If they were faithful, God said, they
would not suffer the burdens of sickness, and no one and no animal
would suffer miscarriages (Ex. 23:24-26). Special grace leads to a com-
mitment to the law; the commitment to Gad’s law permits God to re-
duce the common curse element of natural law, leaving proportion-
ately more common grace —the reign of beneficent common law, The
curse of nature can be steadily reduced, but only if men conform
themselves to revealed law or to the works of the law in their hearts.
The blessing comes in the form of a more productive, less scarcity-
dominated nature. There can be positive feedback in the relation be-
tween law and blessing: the blessings will confirm God’s faithfulness to
His law, which in turn will lead to greater convenantal faithfulness
(Deut. 8:18). This is the answer to the paradox of Deuteronomy 8: it
need not become a cyclical spiral. Of course, special grace is required
to keep a people faithful in the long run, Without special grace, the
temptation to forget the source of wealth takes over, and the end result
is destruction. This is why, at the end of the millennial age, the unre-
generate try once again to assert their autonomy from God. They at-
tack the church of the faithful. They exercise power. And the crack of
doom sounds—for the unregenerate.

Differentiation and Progress

The process of differentiation is not constant over time. [t ebbs and
flows. Its general direction is toward epistemological self-consciousness.
But Christians are not always faithful, any more than the Hebrews

642,
