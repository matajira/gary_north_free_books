THE KING ON MOUNT ZION 14:17-18

few; therefore beseech the Lord of the harvest to send out work-
ers into His harvest” (Matt. 9:37-38). From His Cloud-Throne
the King answers the Church’s prayer: Throwing His sickle over
the earth, He sends out harvesters; the Land is reaped, and the
fruit is brought into His Kingdom. The image of the sickle is
comnected in Scripture with Pentecost, celebrated after the grain
had been harvested (Deut. 16:9), when the Spirit is poured out in
salvation and blessing (Acts 2).

17-18 St. John returns to the theme of judgment, for the
concomitant of the gathering of the Church is the excommuni-
cation of Israel. Genesis 21 records how the recognition of Isaac
as the child of promise required the casting out of the bond-
woman Hagar and her son, Ishmael; and St. Paul saw in this
story an allegory of the rejection of old Israel and the recogni-
tion of the Church as the “heir of the promise.” He spelled it out
to the churches of Galatia, which had been infiltrated by Judais-
tic teachings: “It is written that Abraham had two sons, one by
the bondwoman and one by the free woman. But the son by the
bondwoman was born according to the flesh, and the son by the
free woman through the promise. This is allegorically speaking:
for these women are two covenants, one proceeding from
Mount Sinai bearing children who are to be slaves; she is Hagar.
Now this Hagar is Mount Sinai in Arabia, and corresponds to
the present Jerusalem, for she is in slavery with her children. But
the Jerusalem above is free; she is our Mother. ... And you,
brethren, like Isaac, are children of promise. But as at that time
he who was born according to the flesh persecuted him who was
born according to the Spirit, so it is now also. But what does the
Scripture say? ‘Cast out the bondwoman and her son, for the
son of the bondwoman shall not be an heir with the son of the
free woman.’ So then, brethren, we are not children of a bond-
woman, but of the free woman” (Gal. 4:22-31). Old Jerusalem,
the capital city of apostate, persecuting Judaism, was cast out,
excommunicated from the Covenant, even as the Church was
being recognized as the legitimate heir of the promise. Chris-
tians, born of the Spirit, are the true children of the heavenly
Jerusalem.

A second angel, therefore, comes out of the Temple which is

373
