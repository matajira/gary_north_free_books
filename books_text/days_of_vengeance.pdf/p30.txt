THE DAYS OF VENGEANCE

book. Understand, Walvoord’s commentary appeared 96 years
after W. E. B.’s Jesus Is Coming, the book that launched dispen-
sationalism’s popular phase in the United States. It appeared
over half a century after the Scofield Reference Bible (1909). In
short, the exegesis that supposedly praves the case for dispensa-
tionalism came at the tail end of the dispensational movement’s
history, just about the time that R. J. Rushdoony had his initial
social and law-oriented books published. The dispensationalists
could point to only a handful of books with titles such as Lec-
tures on Revelacion or Nates on Revelation. In short, bits and
Pieces on Revelation, but nothing definitive—not after over a
century of premillennial dispensationalism. The bibliography in
Walvoord’s book lists a small number of explicitly dispensa-
tional commentaries on this book of the Bible, above all others,
that we would expect the dispensationalists to have mastered,
verse by verse.

Whatever we conclude about the history of dispensational-
ism, its wide popularity had very little to do with any systematic
exposition of the book that dispensationalists assert is the most
prophecy-filled book in the Bible. In fact, the average dispensa-
tionalist probably does not own, has not read, and has never
heard of a single dispensational commentary on the Book of
Revelation. It is doubtful that his pastor knows of one, either,
other than Walvoord’s which is about half the size of Chilton’s.

In contrast, the publication of Chilton’s two books on
eschatology, along with Rushdoony’s far fess exegetical book,
Thy Kingdom Come (1970), in the early phases of the Christian
Reconstruction movement places the foundational exegesis at
the beginning, where it belongs. We now have the basic exegeti-
cal work behind us. Chilton’s first two eschatology books are
seminal, not definitive. He and others will continue to build on
their foundation. If they do not continue to build, then the
movement is dead. Any movement that specializes in reprinting
“classics” and does not produce path-breaking new material is
dead. Our opponents will learn soon enough that this movement
is not dead. We have just barely begun to publish.

The point is, it is important to get the foundations laid early
if you intend to reconstruct civilization. This is what the dispen-
sationalists did not do, 1830-1966, perhaps because they never
intended to change civilization. They intended only to escape

XXX
