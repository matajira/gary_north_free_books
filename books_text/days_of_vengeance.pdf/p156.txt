3:4-6 PART TWO: THE SEVEN LETTERS

says: They will walk with Me in white; for they are worthy. He
who overcomes shall thus be clothed in white garments. The
saints are seen in white garments seven times in the Book of
Revelation (3:5, 18; 4:4; 6:11; 7:9, 13; 19:14), and it is obviously a
symbol in Scripture for cleanliness and righteousness, with its
ultimate origins in the sunlike brightness of the Glory-Cloud: In
Christ, the saints are re-created in the image of God, and are
clothed with the New Man, Jesus Christ (Gal. 3:27; Eph. 4:24;
Col. 3:10). Our being clothed in the white robes of righteous-
ness, therefore, takes place definitively at our baptism (Gal.
3:27), progressively as we work out our salvation in daily obedi-
ence to God’s commandments, “putting on” the Christian graces
and virtues (Col. 3:5-17), and finaffy at the Last Day (Col. 3:4;
Jude 24). As with all the promises to the overcomers in Revela-
tion, this too is simply a description of an aspect of salvation, in
which all of God’s elect have a share.

In this letter’s second promise regarding the overcomer,
Christ says: I will not erase his name from the Book of Life. This
statement has been the source of controversy for generations.
Can a true Christian fall away? Can you lose your salvation? At
least three erroneous answers have been offered:

d. Those who have been iruly saved by Christ's redemption
can fall away and be lost forever. This is the classical Arminian
Position, and it is absolutely and categorically denied by Scrip-
ture. The nature of the salvation provided by Christ is eternal,
and our justification in God’s sight is not based on our works
but on the perfect, finished righteousness and substitutionary
atonement of Jesus Christ. (See John 3:16; 5:24; 6:35-40;
10:27-30; Rom. 5:8-10; 8:28-39; Eph. 1:4-14; 1 Thess. 5:23-24;
1 John 2:19).

2. All those who have “accepted Christ” will be saved; no
matter what they do afterwards, they cannot be damned. This is
the classic “chicken Evangelical” position, and it too is opposed
by Scripture. Those who take this view are attempting to have it
both ways: They don’t want the predestinating God preached by
the Calvinist, but they don’t have the courage te affirm full Ar-
minianism, either. They want man to be sovereign in choosing
his salvation, without interference from God’s decree; yet they
want the door of salvation to slam shut as soon as man gets in-
side, so that he can’t get out. But the Bible teaches that God has

122
