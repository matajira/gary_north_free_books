18:9-10 PART FIVE: THE SEVEN CHALICES

Reactions to Babylon's Fall (18:9-20)

9 And the kings of the earth, who committed fornication and
lived sensuously with her, will weep and lament over her
when they see the smoke of her burning,

10 standing at a distance because of the fear of her torment,
saying: Woe, woe, the great City, Babylon, the strong City!
For in one hour your judgment has come.

11 And the merchants of the earth weep and mourn over her,
because no one buys their cargoes any mare;

12 cargoes of gold, silver, precious stones, and pearls; of fine
linen, purple, silk, and scarlet; and every kind of citron
wood, every article of ivory, and every article made from
very costly wood, bronze, iron, and marble;

13 and cinnamon, incense, perfume, and frankincense; and
wine, olive oil, fine flour, and wheat; and sheep and cattle,
of horses, of chariots, and of bodies; and souls of men.

14 And the fruit of your soul’s desire has gone from you, and
all things that were luxurious and splendid have passed away
from you and men will no longer find them.

15 The merchants of these things, who became rich from her,
will stand at a distance because of the fear of her torment,
weeping and mourning,

16 saying: Woe, woe, the Great City, she who was clothed in
fine linen and purple and scarlet, and adomed with gold and
precious stones and pearls;

17 for in one hour such great wealth has been laid waste! And
every shipmaster and everyone who sails anywhere, and
every sailor, and as many as make their living by the sea,
stood at a distance,

18 and were crying out as they saw the smoke of her burning,
saying: Who is like the Great City?

19 And they threw dust on their heads and were crying out,
weeping and mourning, saying: Woe, woe, the Great City, in
which all who had ships at sea became rich by her costliness,
for in one hour she has been laid waste!

20 Rejoice over her, O heaven, and you saints and apostles and
prophets, because God has judged your judgment against
her!

9-10 Three classes of people lament for the destruction of
Jerusalem. The first group comprises the kings of the earth, the
nations of the empire who aided and abetted the faithless Cove-

452
