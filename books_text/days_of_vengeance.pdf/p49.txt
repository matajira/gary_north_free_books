THE COVENANT LAWSUIT

Deuteronomy

. Preamble (1:1-5)

. Historical Prologue (1:6-4:49)

. Ethical Stipulations (5:1-26:19)

. Sanctions (27:1-30:20)

. Succession Arrangements (31:1-34;12)

Awe

If a vassal kingdom violated the terms of the covenant, the
Jord would send messengers to the vassal, warning the offenders
of coming judgment, in which the curse-sanctions of the cove-
nant would be enforced. This turns out to be the function of the
Biblical prophets, as I mentioned above: They were prosecuting
attorneys, bringing God’s message of Covenant Lawsuit to the
offending nations of Israel and Judah, And the structure of the
lawsuit was always patterned after the original structure of the
covenant. In other words, just as the Biblical covenants them-
selves follow the standard five-part treaty structure, the Biblical
prophecies follow the treaty form as well.34 For example, the
prophecy of Hosea is ordered according to the following out-
line:

Hosea

. Preamble (1)

. Historical Prologue (2-3)

. Ethical Stipulations (4-7)

. Sanctions (8-9)

. Succession Arrangements (10-14)

wen

Like many other Biblical prophecies, the Book of Revelation
is a prophecy of Covenant wrath against apostate Israel, which
irrevocably turned away from the Covenant in her rejection of
Christ. And, like many other Biblical prophecies, the Book of
Revelation is written in the form of the Covenant Lawsuit, with
five parts, conforming to the treaty structure of the Covenant.
This thesis will be demonstrated in the commentary; by way of
introduction, however, it will be helpful to glance at some of the
major points that lead to this conclusion. (Also, I have provided

34. Incidentally, the point is not that Scripture is madeled after pagan
treaties; rather, as Sutton argues, the pagan treaty-forms were ultimately
dezived from God’s Covenant.

15
