LEVIATHAN AND BEHEMOTH 13:18

as a whole, rather than with Nero alone, does this not change
the “number of the Beast” when another Caesar is on the
throne? Moreover, is this not merely an example of “newspaper
exegesis” — using first-century newspapers?2? The answer is that
Nero’s name is nof the primary reference of 666; rather, the
number of the Beast is based on several strands of Biblical data
which point ultimately to the Roman Empire. The name Nero
Caesar by no means exhausts the significance of the riddle. The
Bible itself gives us enough information to allow us to identify
Rome as the Beast, the fulfillment of 666.

We begin with the simple number 6, which is associated with
both Beast and Man from the beginning, since they were both
created on the sixth day of the week (Gen. 1:24-31). Six days out
of seven are given to man and beast for labor (Ex. 20:8-11); the
Hebrew slave was in bondage for six years before his release in
the seventh year (Ex. 21:2); six cities of refuge were appointed
for the accidental slaying of a man (Num. 35:9-15), Six is thus
the number of Man, i.e. a human number. Lenski explains:
“John writes the number not in words but in Greek letters: x’ =
600, £ = 60, <'=6, thus 666. This is the number 6, plus its multi-
ple by 10, namely 60, again plus its multiple by 10 x 10 (intensi-
fied completeness), namely 600—thus 666, three times falling
short of the divine 7. In other words, not 777, but competing
with 777, seeking to obliterate 777, but doing so abortively, its
failure being as complete as was its expansion by puffing itself
up from 6 to 666.”4 Six is thus the number Man was born with,
the number of his creation; the repetition of the number reveals
Man in opposition to God, trying to increase his number, at-
tempting to transcend his creaturehood. But, try as he might, he
can be nothing more than a six, or a series of sixes.

And this is exactly what we see in Scripture, as apostate man
attempts to deify himself. Goliath, the ancient enemy of God’s

23. There is, of course, some justification for a first-century “newspaper ex-
egesis,” for the Book of Revelation itsclf teads us to expect a first-century ful-
fillment of its pre phecies. We should look — carefully — for historical events in
the first century which correspond to the apocalyptic visions. This docs not ne-
cessarily lend itself to undue speculation, for it is simply taking John’s own
statements about his book scriously. He said it would be fulfilled “shortly.”

24, R. C,H. Lenski, The Interpretation of St. John's Revelation (Minnea-
polis: Augsburg Publishing House, 1943, 1963), pp. 4lIf,

345
