Part Four

COVENANT SANCTIONS:
THE SEVEN TRUMPETS
(Revelation 8-14)

Introduction

The fourth section of the standard treaty document dealt
with the sanctions (curses and blessings) of the covenant (cf.
Deut. 27:1-30:20).! In Deuteronomy, these sanctions are set
forth in the context of a ratification ceremony, in which the Cove-
nant between God and the people is renewed. Moses instructed
the people to divide into two groups, six tribes on Mount Geri-
zim (the symbol of blessing) and six at an altar built on Mount
Ebal (the symbol of cursing). The congregation was to take a
solemn oath, repeating Amen as the Levites repeated the curses
of the Covenant, calling down those curses upon themselves if
they should ever forsake the law (Deut. 27:1-26). Moses made it
clear that this Covenant oath involved not only the people who
swore to it, with their wives, children, and servants, but also
with the generations to come (Deut. 29:10-15).

Deuteronomy 28 is practically the paradigmatic blessing/
curse section of the entire Bible. The blessings for obedience are
listed in verses 1-14, and the curses for disobedience are enumer-
ated (in more detail) in verses 15-68. The Jewish War by
Josephus reads almost like a commentary on this passage, for
the Great Tribulation culminating in the Fall of Jerusalem in
A.D. 70 and the subsequent scattering of the Jews throughout
the earth was the definitive fulfillment of its curses. When the

1. See Meredith G. Kline, Treaty of the Great King: The Covenant Structure
of Deuteronomy (Grand Rapids: William B. Eerdmans Publishing Co., 1963),
pp. 121-34; cf. Ray R. Sutton, That You May Prosper: Dominion By Covenant
(Tyler, TX: Institute for Christian Economics, 1987).

225
