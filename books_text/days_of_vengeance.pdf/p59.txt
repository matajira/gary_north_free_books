THE NATURE OF REVELATION! APOCALYPTIC?

body by the Roman poet Persius or the pagan philosopher Sen-
eca in the first century, and they are only elaborating a thesis
from Greek philosophical authors going back to the seventh
century B.c. Briefly, the puritan theory is that worship is a purely
mental activity, to be exercised by a strictly psychological ‘atten-
tion’ to a subjective emotional or spiritual experience. . . . Over
against this puritan theory of worship stands another— the ‘cere-
menious’ conception of worship, whose foundation principle is
that worship as such is not a purely intellectual and affective ex-
ercise, but one in which the whole man—body as well as soul,
his aesthetic and volitional as well as his intellectual powers —
must take full part. It regards worship as an ‘act’ just as much as
an ‘experience.’ "6 It is this “ceremonious” view of worship that
is taught by the Bible, from Genesis to Revelation. Since all the
action of Revelation is seen from the viewpoint of a worship ser-
vice, this commentary will assume that the prophecy’s liturgical
structure is basic to its proper interpretation.

The Nature of Revelation: Apocalyptic?

The Book of Revelation is often treated as an example of the
“apocalyptic” genre of writings which flourished among the
Jews between 200 B.c. and a.p. 100, There is no basis for this
opinion whatsoever, and it is unfortunate that the word apeca-
Iyptic is used at all to describe this literature. (The writers of
“apocalyptic” themselves never used the term in this sense;
Tather, scholars have stolen the term from St. John, who called
his book “The Apocalypse of Jesus Christ.”} There are, in fact,
many major differences between the “apocalyptic” writings and
the Book of Revelation.

The “apocalyptists” expressed themselves in unexplained and
unintelligible symbols, and generally had no intention of mak-
ing themselves really understood. Their writings abound in pes-
simism: no real progress is possible, nox will there be any victory
for God and His people in history. We cannot even see God act-
ing in history. All we know is that the world is getting worse and
worse. The best we can do is hope for the End— soon.‘ Ferrell

56, Dom Gregory Dix, The Skupe of the Liturgy (New York: The Seabury
Press, [1945] 1983), p. 312.

57, See Leon Morris, Apocalyptic (Grand Rapids: William B. Eerdmans
Publishing Co., 1972).

25
