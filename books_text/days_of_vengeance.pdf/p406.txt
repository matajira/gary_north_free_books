14:14-16 PART FOUR: THE SEVEN TRUMPETS

the result of Christ’s original Ascension in the Clouds to the
Father—the definitive Parousia.'* The Son of Man reigns now
as the Second Adam, the King of kings. St. John does not show
Christ coming in the Cloud, but in fact already seated on the
Cloud, installed on His heavenly throne. Earlier (v. 6), he
showed us the Israelite officials sitting over the Land; over
against them sits the Lord Christ, enthroned on the Glory-
Cloud (cf. Ps. 2:2-6).

The King has not only a crown on His head, but also a sharp
sickle in His hand. And another angel came out of the Temple,
erying out with a loud voice to Him who sat on the Cloud: Put
in your sickle and reap, because the hour to reap has come, be-
cause the harvest of the Land is ripe. The first angel in this triad
repeats what the first angel of the other triad had said (v. 7): The
hour has come! This time, however, the emphasis falls not on
judgment but on blessing, the gathering in of the elect. This,
too, is connected with the work of the Son of Man in His Parou-
sia, when He sends out His “angels,” His apostolic messengers,
to gather in the elect (Matt. 24:30-31). The word for gather is,
literally, to synagogue; His meaning is that Israel, which refused
to be synagogued under Christ (Matt. 23:37-38), will be replaced
by the Church as the new Synagogue. The first churches were
simply Christian “synagogues” (James 2:2), and looked forward
to the soon-approaching Day when apostate Israel would be
thoroughly disinherited, and the Church revealed as the true
Synagogue, “gathered together” in the final, New Covenant
form (2 Thess. 2:1). Jesus described the Kingdom of Ged as a
great harvest (Mark 4;26-29), and told His disciples: “Behold, I
say to you, lift up your eyes, and look on the fields, that they are
white for harvest. Already he who reaps is receiving wages [cf.
Rev. 14:13], and is gathering fruit [cf. Rev. 14:4] for life eternal;
that he who sows and he who reaps may rejoice together” (John
4:35-36).

Accordingly, the first angel (representing his earthly counter-
parts) calls on the Son of Man to put in His sickle (mentioned
seven times in this passage) and reap, praying in obedience to
Christ’s command: “The harvest is plentiful, but the workers are

19, See David Chilton, Paradise Restored: A Biblical Theology of Domin-
ion (Ft. Worth, TX: Dominion Press, 1985), pp. 68ff., 102f.

372
