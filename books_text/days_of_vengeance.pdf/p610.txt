22:10 PART FIVE: THE SEVEN CHALICES

superior. Even so, in the New Covenant age that is no longer ap-
propriate. Angelic superiority over man was intended only to be
temporary, an expedient after Adam forfeited his responsibility
as guardian of the sanctuary (Gen. 2:15; 3:24). Now that Christ
has ascended to the Throne, His people are saints, with access to
the sanctuary as God’s counselors and confidants; indeed, says
St. Paul, the saints are destined to rule not only the world but
angels as well (1 Cor. 6:1-3). The angel, though exalted and pow-
erful, is no more than a fellow servant of the apostle and his
brethren the prophets—the other members of the Christian
Church, all those who keep the words of this book. The believer
is a member of the heavenly council, and is able to worship God
face to face (cf. v. 4). Again, this shows that the blessings enum-
erated in these closing chapters are not reserved for the consum-
mation alone, but have already been granted to God’s people;
otherwise, the angel would have accepted St. John’s act of rever-
ence. We have direct access to God’s Throne.

That this incident had to be repeated almost word-for-word
demonstrates both the centrality of this concern to the apostle,
and how hard it is for us to learn it. It may well be said that the
most important teaching of the Book of Revelation is that Jesus
Christ has ascended to the Throne; and the second most impor-
tant lesson is that we have ascended to heaven with Him.

10 And he said to me: Do not seal up the words of the
prophecy of this book, for the time is near. Again the angel em-
phasizes the imminence of the prophecy’s fulfillment. For this
reason St. John is forbidden to seal up the words of the book.
We have already had occasion (on 10:4) to contrast this with the
command to Daniel to “conceal the words and seal up the book
until the time of the end” (Dan. 12:4). Because his prophecy
spoke of the distant future, Daniel was ordered to seal it up;
because St. John’s prophecy refers to the imminent future, he is
ordered to set it loose. “Indeed, these are the very days for
which Daniel wrote, and St. John has been inspired to ‘unseal’
him.”?

9. Austin Farrer, The Xevelation of St. John the Divine (Oxford: At the
Clarendon Press, 1964), p. 225.

576
