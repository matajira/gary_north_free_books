APPENDIX C

had held off destruction (19:21-22),

The same was true of Noah. Until the ark was completed, the
world was safe from the great flood. The people seemed to be prosper-
ing. Methuselah lived a long life, but after him, the lifespan of man-
kind steadily declined. Aaron died at age 123 (Num. 33:39). Moses
died at age 120 (Deut. 31:2), But this longevity was not normal, even in
their day. In a psalm of Moses, he said that “The days of our years are
threescore years and ten; and if by reason of strength they be four-
score years, yet is their strength labour and sorrow; for it is soon cut
off, and we fly away” (Ps. 90:10). The common curse of God could be
seen even in the blessing of extra years, but long life, which is a bless-
ing (Ex. 20:12), was being removed by God from mankind in general.

The Book of Isaiah tells us of a future restoration of long life. This
blessing shall be given to all men, saints and sinners. It is therefore a
sign of extended common grace. It is a gift to mankind in general.
Isaiah 65:20 tells us; “There shall be no more thence an infant of days,
nor an old man that hath not filled his days: for the child shall die an
hundred years old; but the sinner being an hundred years old shall be
accursed.” The gift of long life shall come, though the common curse
of long life shall extend to the sinner, whose long life is simply extra
time for him to fill up his days of iniquity. Nevertheless, the infants
will not die, which is a fulfillment of God’s promise to Israel, namely,
the absence of miscarriages (Ex. 23:26). If there is any passage in
Scripture that absolutely refutes the amillennial position, it is this one.
This is not a prophecy of the New Heavens and New Earth in their
post-judgment form, but it is a prophecy of the pre-judgment mani-
festation of the preliminary stages of the New Heavens and New
Earth —an earnest (down payment) of our expectations. There are still
sinners in the world, and they receive long life. But to them it is an ul-
timate curse, meaning a special curse. It is a special curse because this
exceptionally long life is a common blessing—the reduction of the
common curse. Again, we need the concept of common grace to give
significance to both special grace and common curse. Common grace
(reduced common curse) brings special curses to the rebels.

There will be peace on earth extended to men of good will (Luke
2:14), But this means that there will also be peace on earth extended to
evil men. Peace is given to the just as a reward for their covenantal
faithfulness. It is given to the unregenerate in order to heap coals of
fire on their heads, and also in order to lure rebels living in the very
last days into a final rebellion against God.

Final Judgment and Common Grace

An understanding of common grace is essential for an understand-
ing of the final act of human history before the judgment of God. To

646
