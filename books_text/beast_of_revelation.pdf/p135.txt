The Political Evidence 103

resented by the seven heads spoke of the peculiar geography of
Rome. Rome was familiar to all of the ancient world as the “the
city on Seven Hills.” This geographical clue in the vision, coupled
with the expectation of soon occurrence and the relevance of the
letter to its original audience, necessarily limit us to the broad
historical era of the ancient Roman Empire!

But there is a more particular aspect to the vision, an aspect
which confines the outer reaches of the time-flame to the early
date era. I speak of the folitical reference to the line of the kings.
John writes of the seven heads that they are not only “seven
mountains” but: “They are seven kings; five have fallen, one is, the
other has not yet come; and when he comes, he must remain a
little while.” Here is described for us a sequence of seven kings of
Rome. This statement, as we will see, closely fixes the time of
composition of Revelation. All we need to do is to determine the
identity of these seven kings, and which ones are alive as John
writes. I lay before the careful student the following evidences,
supportive of the early date position.

First, it may be dogmatically asserted that the sixth king is
alive and ruling even as John writes. John clearly states that the
first five “have fallen.” In this verse is used a verb in the past
tense:"The five that “have Mien” are fast rulers. For whatever
reasons, these five have already fallen from power as “kings.” He
continues by noting that the sixth king “is.” Here John uses the
present tense.’ The sixth king is presently in control of the king-
dom. The seventh “has not yet come;‘and when he comes, he
must remain a little while” (Rev. 17:10). The coming to power of
the seventh is not yet a present reality.

This obviously speaks of the past deaths of the first five kings

1, See Chapter 2,
2, The form of the Greek verb fipto (“fall”) ia the second aoriat active indicative.
8, The Greek is the present indicative form of the verb cini (“to he").

4, Here John uses the “prophetic second aorist active of erchomat” according to
A, T, Robertson, Word Pictures in the Naw Testament, 6 vols. (Nashville Broadman,
1983) 6452.
