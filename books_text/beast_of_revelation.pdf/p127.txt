The Thematic Evidence 95

seems to be indicated by the fact that the verse is a blending of
Daniel 7:13 and Zechariah 12:10.83 The Zechariah 12:10 passage
indisputably refers to the land of Israel: “And I will pour out on
the house of David and on the inhabitants of Jerusalem, the Spirit
of grace and of supplication, so that they will look on Me whom
they have pierced; and they will mourn for Him, as one mourns
for an only son, and they will weep bitterly over Him, like the
bitter weeping over a first-born. In that day there will be great
mourning in Jerusalem, like the mourning of Hadadrimmon in the
plain of Megiddo. And the land will mourn, every family by itself.”

Gospel Confirmation

That these phrases in Revelation 1:7 speak of the Promised Land
of the first century is evident in Jesus’ teaching. There we find a
recurring emphasis upon the culpability of the generation of Jews
then living — those who actually crucified the Messiah, the Lord of
Glory. In Matthew 23 He calls down a seven-fold woe upon the
scribes and Pharisees, those who “sit in the chair of Moses” (Matt.
23:2). In this woeful passage He distinctly and clearly warns
(Matt. 23:32-38): “Upon you {will} fall the guilt of all the righteous
blood shed on earth [or: “on the land”). . . . Truly I say to you, all
these things shall come upon this generation. O Jerusalem, Jerusalem,
who kills the prophets and stones those who are sent to her! How
often I wanted to gather your children together, the way a hen
gathers her chicks under her wings, and you were unwilling.
Behold, your house is being left to you desolate!”

Christ then goes on to describe the desolation of Israel’s
“house” (temple) in Matthew 24. In Matthew 241-2 He clearly
and distinctly makes reference to the destruction of the temple.
And in the following context He expands on this as involving the

33, An early note should be recalled at this point. Daniel 7:13 has original
reference to the ascension of Christ to glory. Here John is not interpreting Daniel 7:18,
but merging it with Zechariah 12:10 in application to the theme of his prophecy the
judgment-coming upon Iarael that resulted from the asccnsion-coming to the Father
to take up His rule,
