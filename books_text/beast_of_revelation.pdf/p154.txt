122 The Beast of Revelation

symbol would be confusing in its blatant anachronism. The temple
is required to be standing for the symbolical action of the vision
to have any meaning.

A Great City?

It might be ihought that the phrase “the great city” (Rev.
11:8) indicates Rome, which actually authorized and performed.
the crucifixion of Christ. Such a designation of Jerusalem may
seem much too grandiose. But it will not, however, excite wonder
among those who are aware of either the covenantal-redemptive
significance of Jerusalem, or its historical fame. Historically, even
Roman historian: spoke of its magnificence: Tacitus called it “a
famous city.”!? He noted that Jerusalem housed a temple which
“was famous beyond all other works of men.”3 Pliny the Elder
said of Jerusalem that it was “by far the most famous city of the
ancient Orient.” * Appian, a Roman lawyer and writer (ea. A.D.
160) called it “the great city Jerusalem.”= Truly, then, Jerusalem
was one of the most famous cities of the civilized world at that
time.

More important, however, is the covenantal significance of Jerusa-
lem. The obvious role of Jerusalem in the history of the covenant
should merit it such greatness, The intense Jewish love of Jerusa-
Jem pictured it as of great stature among the famous cities of the
nations,

The Fifth Bock of the Sibylline Oracles is a Jewish oracle written
from Egypt in the A.D. 90s. In this oracle Jerusalem is spoken of
thus: “He seized the divinely built Temple and burned the citizens
and peoples who went into it, men whom I rightly praised. For
on hig appearano: the whole creation was shaken and kings per-

12, Histories 52,

13, Fragments of the Histories 1,

14 Natural History 5:14:70.

15, The Syrian Wars O.

16. David Ben-Gurion, The Jaus in Their Lund, trans, Mordechai Nurock and
‘Misha Louvish (Garden City, NY: Doubleday, 1966), p. 152.
