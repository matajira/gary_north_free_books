The Ecclesiastical Evidence 131

dence that the era in which John wrote was one in which Chris-
tianity was still largely affected by and strongly attached to the
Jewish community. Let us survey a few aspects of Revelation that
indicate this,

Revelation 2 and 3
In Revelation 2:9 and 3:9 we discover some interesting evi-
dence in this direction:
I know your tribulation and your poverty (but you are rich), and

the blasphemy by those who say they are Jews and are not, but are
a synagogue of Satan (Rev. 2:9).

Behold, I will cause those of the synagogue of Satan, who say that
they are Jews, and are not, but lie — behold, I will make them to
come and bow down at your feet, and to know that I have loved
you (Rey. 3:9).

In these two passages John indicates that at least two of the seven
churches (Smyrna and Philadelphia) are plagued by “those who
say they are Jews.”

That those who plagued them were racial Javs and undoubt-
edly of the Jewish faith may be fairly assumed. This is so because
the Jews wore a distinctive cultic mark: circumcision. In one of his
debates with a Jew, early church father Justin Martyr (A.D.
100-165) mentions the distinctiveness of the Jew in this regard:
“For the circumcision according to the flesh, which is from Abra-
ham, was given for a sign; that you may be separated from other
nations, and from us;... For you are not recognised among the
rest of men by any other mark than your fleshly circumcision.”8
Pagan Roman historian Tacitus (A.D. 56-117) wrote of the Jews:
“They adopted circumcision to distinguish themselves from other
peoples by this difference.”

Now the question naturally arises: Who would array them-

8. Dialogue with Tiypho the Jao 16.
9, Histories 5:5,
