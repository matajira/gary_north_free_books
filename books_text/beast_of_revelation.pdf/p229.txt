GENERAL INDEX

Abaddon, 38.

“Abomination of desolation,” 96.

“About to,” (Se-e atic: Revelation -
expectation), 22,28,

Abraham, 91, 131, 186,

Abyss, 134,173.

Actor, 18,66.

‘Adam, 11,

Aggripina, 15.

Ahenobarbus, 14,

Ahenobarbus, Lucius (See: Nero).

Alexandria, 38, MO, 141, 149, 156,

Alphabet(s), 29-80,

Altar(s), 61,58,69,62,64, 66, 111, 120,
121, 128,

Animal(s) (see also: Beasts), xv, 17, 40,
&.

Angele), xxv, 21,22,98, 184,
interpretation by, 12, 105-108,

Antichrist, xii, xix, xx, xxii, xxiv, 11,45,
161, 158, 154, 155, 164,172n.

Apocalypse (Sex: Revelation).

Apollo, 63,64,66,

Apollonius, 42, 158n.

Apologetic(s), 160.

Apostle(s), xv, mudi, xxxvi, 49, 77, 86,
100, 109, 125, 185, 136, 141, 148, 144,
146, 150, 154, 156, 157, 162, 164.

Appian, 122,

Arabia, 42,

Aramaic, 38-39, 194,

Archaeology, 15,31,94,58,60,118, 186.

Arena, 40,41,46.

Arethas, 146-147, 148, 18,

Armageddon, 4,88,95, 184,

Artist, 19,63.

Ascension of Isaiah, 42n.

Asia, 60,61,62, 156, 179.

Asia Minor, 13, 37, 48, 54, 109, 168,
175, 176,
Jewish population in, $8,

Assassinate, 82.

Astrology, 15.

“At hand” (See also: Revelation -
expectation), 10, 25,

Athens, 63,

Augustine, 159, 172.

Augustus (emperor), 61,62,64,104,106,
107, 108, 162.
name used a8 oath, 60.
Nero as, 63,64,66.
worship of, 69-61, 62,64, 169,

Banishment (5: John the Apostle -
banishment),

Barnabas, 116, 135,

Beast of Revelation, (See also: Nero), xi,
xvii-xix, 2c, xxiv, 4, 5,11, 12, 46,
48, 75, 81, 106, 155, 168, 173, 186,
188.

appearance, 5,

authority, 40, 57.

character, 10, 14,33,40-46, 185,
death, 5,68-69,73-75,77,

dual nature, 11,

generic referent, 11, 12-14, 19,67,

197
