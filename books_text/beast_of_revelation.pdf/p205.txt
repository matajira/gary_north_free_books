Objections to the Early Date 173

Interestingly, the myth was not simply a “wives’ tale” of little
significance. It had a measurable impact even on political affairs.
Pretenders to the imperial throne, claiming to be Nero, are re-
corded to have attempted to use the myth in quests for power. 18

Clearly the existence, spread, and influence of the Nero Redivi-
ous myth cannot be disputed. It is one of the most fascinating and
best-known legends in all of political history. But the questions
with which we must deal are: Does the myth appear in Revelation?
And if so, does this necessitate a late date for the composition of
Revelation?

Despite the confidence with which some late-date advocates
employ the Nero Redivivus myth, two intriguing facts arise in regard
to its use by Biblical scholars,

First, not all late-date proponents allow the argument as help-
ful to the question of the dating of Revelation. We will cite just one
example. Donald B. Guthrie, a most able late-date adherent,
carefully considers the merits of the Nero Redivicus argument, but
discourages its endorsement in the debate: “If then an allusion to
the Nero myth is still maintained as underlying the language of
Revelation xiii and xvii, it must be regarded as extremely inconclu-
sive for a Domitianic date. The most that can be said is that it
may possibly point to this.” !9

Second, a number of early alai% advocates believe the myth
appears in Revelation, but still maintain the Neronic dating posi-
tion! John A. T. Robinson is a case in point: “As virtually all agree,
there must be a reference to Nero sedivivus in the beast that ‘once
was alive and is alive no longer but has yet to ascend out of the
abyss before going to perdition.”“x

It is most interesting to find proponents of oth dating positions

18, Tacitus, Histories 1:78; 2-8; Suetonius, Nero 67.

19, Donald B, Guthrie, Naw Testament Introduction, 3rd ed. (Downers Grove, IL.
Inter-Varsity Presa, 1970), pp. 953-054.

20. John A. T, Robinson, Redating the New Testament (Philadelphia Weatminater
Press, 1976), p. 246. Moses Stuart and J. Stuart Russell are orthodox, early-date
scholars who have allowed that the myth appears in Revelation,
