92 The Beast of Revelation

illustrate the mattzr.

Ignatius (A.D. 50-115) quite frequently drives home the point
ofJewish culpability regarding Christ’s death when he refers to the
Jews as “Christ-k ling Jews, ”"* “those murderers of the Lord,”!5
and “the Jews fighting against Christ.”' Justin Martyr (A.D.
100-165) plays the same theme of Jewish liability when he writes:
“Jesus Christ stretched forth His hands, being crucified by the
Jews,”!” “all these things happened to Christ at the hands of the
Jews,”!® and “the Jews deliberated about the Christ Himself, to
crucify and put Him to death.” Irenaeus (A.D. 180-202) concurs
when he says of the Jews:. “[God] sent in Jesus, whom they
crucified and God raised up,”2° and “to the Jews, indeed, they
proclaimed that the Jesus who was crucified by them was the Son
of God.”?! Other church fathers who return to this theme include:
Melito of Sardis (d. A.D. 190), Tertullian” (A.D. 160-220), Hip-
polytus’3 (A.D, 1; 0-236), Cyprian” (A.D. 200-258), Lactantius®
(A.D, 240-820), tc name but a few.5

©The Tribes of the Earth”
Second, this view is reinforced in the Revelation 1:7 passage

14, Epistle to the Magvesians 11,

16, Epistle to the Fraltians 11,

16, Epistle to the Smyrracans 2,

1, First Apology 35.

18, Ihid, 38.

19. Dialogue with Tryho 72.

20, Against Heresies 312:2,

21, Ibid, 3:12:13,

22, Apology 23 and 26; On Idolatry 7; An Answer to the Jews 9 and 13; Against Marcion
3:6; 3:25; 515,

23, Treatise on Christ and Antichrist 30 and 57; Expository Treatise Against the Jaws, 2,
and 7; and Against Noeh 1s 18.

24, Treatises 9:7; 10:5; Introduction to Treatise 12; 12:2:14; 12:2:20,

25. Divine Institutes 4:18; Epitome of the Divine Institutes 46; On the Marmer in Which the
Persecutors Died 2.

26, Additional references can be found in my Befre Jerusalem Fall: Dating the Book
of Revelation (Tyler, TX Institute for Christian Economics, 1989).
