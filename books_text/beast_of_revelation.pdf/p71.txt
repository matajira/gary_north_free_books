The Number of te Beast 37

specific alternative, the case against the Nero theory would have
been more seriously challenged. Interestingly, Irenaeus suggests
the hopelessness of determining the proper understanding “It is
therefore more certain, and less hazardous, to await the fulfillment
of the prophecy, than to be making surmises, and casting about
for any names that may present themselves, inasmuch as many
names can be found possessing the number mentioned; and the
same question will, after all, remain unsolved.” Irenaeus admits his
own ignorance on the matter. How can that prove the Nero theory
wrong? None of the later church fathers does more than guess at
the solution. Did the riddle have xo answer?

The Problem of the Hebrew Spelling

Some have argued that since John writes to Gentile churches
in Asia Minor, the mechanical maneuver necessary to derive the
name from its Hebrew spelling would be too difficult for the
audience. Though reasonable at first glance, this objection also
fails to undermine the Nero theory.

First, although John wrote in Greek, Revelation has long been
recognized as one of the more “Jewish” books of the New Testa-
ment. All technical commentaries on Revelation recognize this.
For instance, in his commentary R. H. Charles includes a major
section entitled “A Short Grammar of the Apocalypse.” Section
10 of this “Grammar” is entitled “The Hebraic Style of the Apoca-
lypse.”* There Charles well notes of John’s unusual syntax “The
reason clearly is that, while he writes in Greek, he thinks in Hebrew.”
As J. P. M. Sweet puts it: “The probability is that the writer,
thinking in Hebrew or Aramaic, consciously or unconsciously
carried over Semitic idioms into his Greek, and that his ‘howlers’
are deliberate attempts to reproduce the grammar of classical
Hebrew at certain points.” Some scholars have even suggested

23, Against Heresies 5:30:3.

9A, Charles, Raelation 1 scxvii, cxlii,

25. Ibid, p. cxliii.

26, J. P, M, Sweet, Revlation (Philadelphia Westminster Press, 1979), p. 16,
