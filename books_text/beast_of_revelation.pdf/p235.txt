General Index

33, 35, 36, 38, 40, 41, 47, 48, 68,
69, 70, 76, 76, 81, 86, 86, 93, 95,
99, 100, 101, 103, 104, 107, 109,
118, 114, 116, 118, 121, 128, 181,
182, 184, 187, 189, 140, 148, 146,
149, 152, 158, 154, 157, 162, 163,
164, 166, 174, 175, 176, 179, 182,
184, 185, 186.

banished (exiled), 47,52,81, 104, 144,
146, 146, 147, 148, 156-157, 158,
159, 161, 162, 165.

chasing a heretic on horseback, 161-
162.

Domitian and, 144,147, 156,

Greek language and, 37-39.

labor in mines, 168,

martyrdom by Jews, 117, 142-148,

persecution with Peter and Paul, 147,
148, 165.

Nero and, 47, 53, 81, 104, 144-146,
146, 147, 148,

Papias and, 149-149,

Polycarp and, 142.

two persecution of, 147, 148, 187.

vigorous activity, 157-158, 161-162,
163.

Josephus, 40,61,62, 71,72, 73,74,96,
107, 109, 128,

Judaism (See also: Christianity - Jewish
background; Jews), x, 49,85,86, 116,
117, 121, 129, 190, 182, 133, 185, 136,
187, 181.

Judas, 45.

‘Judea (See aso: Palestine; Land, the), 27,
74,94, 116, 117.

judgment, xvi, xxviii, xuvi, 25,26,85,
99,95,96,87, 100, 114, 120, 168, 181,
194,

Julio-Claudian line of emperors, 19,45,
64,71,76.

Julius Caesar (emperor), 14,19,104,106,
107,108.

203

“Father of His Country,” 59.

worship of, 57-58, 168,
Jupiter, 57,58,59,60,61.
Juvenal, 42,168,

Kingdom, ~odi, 11, 12,26,70,74,

divine, xvi, xxix, xovii, xxxviii,
26,27,47,81,86,96, 121.

King(s), 11, 65, 1088, 108, 109-110, 112,
123, 146, 156-157.
Julian emperors aa, 109-110.
seven kings, 12,69, 75, 76-77.
seventh, 104, 108, 186,
sixth, 76, 108-105, 108, 186,
eighth, 76, 77.

Lactantius, 43-44,52,92, 160, 172.

Lectinos, 36.

Land, the (See alto: Iarael; Judea;
Palestine), 26, 40n, 98-95, 97, 98, 99,
100, 117, 121, 122, 128,

Laodicea, 176-177, 179.

“Last days”, xi, xiti-xvii.

Late-date (Ses: Date of Revelation).

Latin, 35-86, 148, 151, 170.

Legend (See Nero — Legend),

Lepida, 15.

Liberal, 88, 118, 149, 167, 168, 172n,
188,

Literal (ism) (See also: Revelation -
interpretation)

Lucan, Marcus Annacus, 42,

Luke, 115, 116, 128,

LXX (See: Septuagint).

Man/mankind (See also: Human race),
4458,60, 127.

Man of Sin, xi, 44

Manuscript(s) (See also: Texts), 34, 141,
142, 148, 155.

Mareus Aurelius (emperor), 42.

Marquia de Sade, 45.
