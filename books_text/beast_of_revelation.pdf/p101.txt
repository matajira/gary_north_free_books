The Worship of the Beast 67

the Beast, particularly as it is incarnate in Nero Caesar. There is
abundant testimony to emperor worship at various stages of devel-
opment well before Nero. And Nero himself actually demanded.
such worship in a way unsurpassed by any previous emperor,
except, perhaps, for Caligula.
