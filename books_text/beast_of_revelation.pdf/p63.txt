3
THE NUMBER OF THE BEAST

Here is wisdom, Let him who has understanding calculate the number
of the beast, for the number is that of a man; and his number is six
hundred and sixty-six (Rev. 18:18).

Following upon an understanding of the necessity for a Beast
who is relevant to first century Christians, I come now to material
which very particularly points to Nero Caesar. This piece of
evidence is drawn from what is probably one of the best known
features of Revelation.! We speak, of course, of the number 666,
which is recorded in Revelation 13:18 quoted above. Who among
us has not feared $6.66 coming up on his cash register receipt?
Or worse yet, 666 appearing in his Social Security number!

But how is this number helpful to our inquiry? Let us begin
with some background research,

The Function of Ancient Alphabets
The usefulness of this number lies in the fact that in ancient
days alphabets served a two-fold purpose. Letters functioned, of
course, as phonetic symbols. As such, they functioned just as our
modern alphabet does. But in ancient times letters also served as
numerals, in that the Arabic numbering system was a later devel-

1, Mounce introduces his discussion of Revelation 1318 thus: “No verse in
Revelation has received more attention than this one with its ic reference to the
number of the beast” (Robert Mounce, The Book of Raelation [Grand Rapids: Eerd-
mans, 1977], p. 263),

29
