Conclusion 185

spelling of Nero’s name, however, we are getting somewhere.® And
when the character of the Beast is matched to Nero’s infamous
conduct, we become more confident still.6 Nero was clearly a
beastly character possessed with a horrendously sinful will to evil
and great power to unleash his base desires.

In addition we noted the remarkable correspondence between
the war of the Beast with the persecution of Christians by Nero.’
This correspondence involved even the detail of time equations:
42, months, according to Revelation; November, A.D. 64, to June,
AD. 68, according to history. Filling out the evil character of the
Nero-Beast was Nero's encouragement of emperor worship, which
John alluded to in Revelation 13.3 And then to top it all off, one
of the most unusual features of the Beast — his death and “resur-
rection” — finds remarkable fulfillment in the events of the A.D.
60s after the death of Nero. Rome was buckling to its knees,
fainting to its death, with the demise of its sixth head, Nero, in the
Civil Wars of A.D. 68-69. But the empire — the Beast generically
considered — was revived under Vespasian, to the “wonder” of the
world?

The Beast is clearly the Roman Empire, particularly expressed
in its most evil head, Nero Caesar. This Beast has lived and died,
according to the infallible prophecy of Scripture. But, of course,
all of this evidence for the identity of the Beast depends on the
date of Revelation’s composition. For if it were written almost 30
years after his death, the whole theory would frail. So, I presented
the case for the early date of Revelation in the pre-A.D. 70 era.

Summary of Evidence for Revelation’s Date
The evidences for Revelation’s early-dating, during the reign
of Nero Caesar, are multiple, varied, clear, and compelling. In

5. See Chapter 3,
6. See Chapter 4,
7. See Chapter 5.
8, See Chapter 6,
9, See Chapter 7.
