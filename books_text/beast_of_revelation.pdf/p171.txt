The Historical Evidence (1) 139

we-volume commentary on Revelation, R. H. Charles introduces
the evidence from tradition as follows: “This evidence almost
unanimously assigns [Revelation] to the last years of Domitian.”4

These grandiose statements simply do not fit the facts, as I
will show. Some of the evidences to be used below are directly
helpful to the debate, in that they speak rather clearly to the
matter. Other pieces of the evidence are provided as merely sugges-
tive possibilities, in that they are not as forthright. We will consider
them chronologically.

The Shepherd of Hermas

A work little known among laymen today was once very
important in early Christianity. That work is known as The Skep-
herd, or The Shepherd of Hermas. This work contains three parts:
Vision, Mandates, and Similitudes, It is indirectly suggestive of an
early date for Revelation. Cautious employment of The Shepherd is
demanded in light of both the nature of its usefulness (as indirect,
circumstantial evidence) and the difficulty of its dating.

The Usefidness of the Shepherd

The Shepherd of Hermas may be strong evidence for discerning
the date of Revelation, if it was written in the first century. Many
competent scholars detect evidence of Hermas’s knowledge of
Revelation. Among older scholars we could name Moses Stuart,
B. F. Westcott, H. B. Swete, and R. H. Charles. In the classic
series entitled The Ante-Nicene Fathers, it is stated boldly that Reve-
lation “is quoted in Hermas freely.” In more recent times noted
critics have concurred in this assessment we mention but a few.
Patristics scholar Edgar J. Goodspeed states confidently that Her-
mas is “clearly acquainted with the Revelation of John.”* John

4.R. H. Charles, A Critical and Exegetical Commentary on the Revelation of St. John, 2
vols. (Edinburgh: T. & T. Clark, 1920), 1 :xci.

6, A. Cleveland Coxe, in Alexander Roberts and James Donaldson, eds., The
Ante-Nicene Fothers, 10 vols. (Grand Rapids: Rerdmans, n.d. [rep. 1976]), 5:600.

6, Edgar J. Goodspeed, The Apostolic Fathers (New York Harper, 1960), p. 97.
