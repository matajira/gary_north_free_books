164 The Beast of Revelation

paign of hostility to God. He was the second to promote persecu-
tion against us, though his father, Vespasian, had planned no evil
against us.

At this time, the story goes, the Apostle and Evangelist John was
still alive, and was condemned to live in the island of Patmos for
his witness to th: divine word. At any rate Irenaeus, writing about
the number of the name ascribed to the anti-Christ in the so-called
Apocalypse ofJchn, states this about John in s0 many words in the
fifth book against Heresies.*9

As we analyze the weight of this evidence, we must bear in
mind that Eusebius clearly declares his dependence upon Irenaeus in this
matter. Whatever difficulties there may be with Irenaeus (see
previous discussion), such must necessarily apply to Eusebius.
Furthermore, there are some perplexing difficulties in Eusebius’s
writings, even apart from his founding his view on Irenacus. Let
us briefly survey these problems.

Inconsistent Usage a Irenaeus

In the first place, despite Eusebius’s express dependence upon
Trenaeus in this area, Eusebius disagrees with Irenacus on an
extremely import ant and intimately related question. Eusebius
denies what Irenz.cus clearly affirms: that John the Apostle wrote
Revelation.’This poses a problem. In another place in his book,
Eusebius establishes the Apostle John’s longevity based. on Ire-
naeus’s confident statement that John lived through Domitian’s
persecution.*! But he disagrees with Irenaeus’s teaching that John
wrote Revelation, even though both ideas are found in the same
place in Irenaeus. If Eusebius believed the one report, why not the
other? The two issues — (1) that the Apostle John wrote Revela-
tion (2) during Domitian’s reign — are bound up together in Ire-
naeus. To doubt one would seem necessarily to entail the doubting
of the other.

39, Eusebius, Ecclesiastical History 3:17-18,
40, Ecclesiastical Histry 3:29:1,2,5,6.
41, Ibid, 3:18:1-3; 5:8:5.
