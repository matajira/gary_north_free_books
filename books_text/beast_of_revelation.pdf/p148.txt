116 The Beast of Revelation

structures. These events had not already occurred, but lay in the
future for both Jesus (whose words Luke records) and John (in
Revelation). The context of Luke demands a literal Jerusalem
{Luke 21:20) and temple (Luke 21:5, 6) besieged by literal armies
(Luke 21:20) in literal Judea (Luke 21:21). As a matter of indisput-
able historical record this occurred in the events leading up to
AD. 70, not after.

The Natural Interpretation

The most natural interpretation of Revelation 11, then, sug-
gests that the reference is to the literal temple, for only in literal
Jerusalem did God have His temple. In light of these factors
certain questions arise.

Even recognizing that the part of the temple to be preserved
has a spiritual referent (see discussion below), how could John be
commanded to measure symbolically that which did not exist with
the idea of presenting (in some sense) a part and destroying the
rest? If Revelation were written in the A.D. 90s, why would there
be no reference to the temple’s already having been destroyed,
particularly in such a work as this, that treats of divine judgment
upon Judaism? E arly, post-apostolic Christianity made much of
the fact of the destruction of the temple as evidence of God’s
rejection of the Jew. Let us survey a few early Christian references
to the destruction of the temple.

The Epistle of . Barnabas is dated between A.D. 75 and 100. In
Barnabas 16: \ff. w 2 read: “Moreover I will tell you likewise con-
cerning the templ:, how these wretched men being led astray set
their hope on the building, and not on their God that made them,
as being a house of God... . So it cometh to pass; for because
they went to war it was pulled down by their enemies.” It is
indisputably clear that Barnabas makes much of the fact of Jerusa-
lem’s fall as an ap dlogetic for Christianity.

Ignatius wrote around A.D. 107. And although clear and.
explicitly detailed reference is not made to Jerusalem’s fall in
Ignatius’s letters, there is what seems to be an allusion to the
