16
OBJECTIONS TO THE EARLY DATE

He who has an ear, let him hear what the Spirit says to the church-a
(Rev. 2:16).

Despite the wealth of evidence from within Revelation as to its
early date, since the early 1900s late-date advocacy has persisted
among the majority of both liberal and conservative scholars. In
the nineteenth century the evidence cited in defense of a late date
for Revelation was derived almost exclusively from church tradi-
tion. Milton S. Terry, author of a much used text on the principles
of biblical interpretation, wrote in 1898: “ [N]o critic of any note
has ever claimed that the later date is required by any internal
evidence.”! This is no longer true today.

Though depending mostly on evidence from tradition, current
late-date literature does attempt to build a case from Revelation’s
self-witness. In order to better secure the early date argument in
terms of the self-witness evidence, I will address the major con-
trary arguments put forward by late-date advocates.

The modem case for the late date of Revelation tends to
concentrate its focus upon four basic arguments. These have been
ably summarized by noted evangelical scholar and late-date advo-
cate Leon Morris. We choose to investigate Morris’s approach for
two basic reasons. He has rightfully earned an international repu-

1, Milton Terry, Biblical Hermeneutits(Grand Rapids: Zondervan, undated re-
print), p. 240,

167
