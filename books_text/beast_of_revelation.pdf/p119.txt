The Importance of the Date of Revelation 8&7

for a Domitianic date for Revelation (A.D. 95-96), there is a
growing movement away from such a position to the more conser-
vative Neronic dating which predominated in the late 1800s. I
trust that this section of the present study will be used in some
small way to interest Christians in the important debate. I hope
that the evidence rehearsed below will draw many to the early
date view of the writing of Revelation.
