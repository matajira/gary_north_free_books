How io Read Prophecy 17

often makes use of symbotic language. But we must realize that
the same is true of the prophets: they, also, spoke in poetry, in
figures and symbols, drawing on a rich heritage of Biblical images
which, as we shall see, actually began in the original Paradise —
the Garden of Eden.

Indeed, that is where prophecy began. And it is worth noting
that the very first promise of the coming Redeemer was stated in
highly symbolic terms. God said to the Serpent:

I will put enmity

Between you and the woman

And between your seed and her seed;

He shall crush your head,

And you shall strike His heel. (Gen. 3:15)

The real question to start with, therefore, is not some
artificial symbolic-vs.-literal debate, but a much more basic
issue: Shall our interpretation be Biblical or speculative? In
other words, when I attempt to understand or explain some-
thing in the Bible, should 1 go to the Bible itself for the answers,
or should I come up with something “creative” on my own? To
put the question in this way is much more accurate, and will
yield more fruitful results.

Let me use an extreme example to make my point clear, The
Book of Revelation describes a woman clothed with the sun,
standing on the moon, and laboring in childbirth while a dragon
hovers nearby to devour her child. A radically speculative inter-
preter might turn first to news of the latest genetic experiments,
to determine whether a woman’s size and chemical composition
might be altered sufficiently for her to be able to wear the sun;
he might also check to see if the Loch Ness Monster has surfaced
recently. A Biblical interpreter, on the other hand, would begin
to ask questions: Where in the Bible does this imagery come
from? Where does the Bible speak of a woman in labor, and
what is its significance in those contexts? Where does the Bible
speak of a Dragon? Where does the Bible speak of someone try-
ing to murder an infant? If we are going to understand the
message of the Bible, we must acquire the habit of asking ques-
tions like this.

Of course, each approach has its drawbacks. The main
drawback of the Biblical method is that it usually requires more
