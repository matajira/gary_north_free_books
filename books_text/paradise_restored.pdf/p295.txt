Josephus on the Fall of Jerusalem 285

sacrifice (for it is not lawful for them to feast singly by themselves),
and many of us are twenty in a company, found the number of sacri-
fices was two hundred and fifty-six thousand five hundred; which,
upon the allowance cf no more than ten that feast together, amounts to
two millions seven hundred thousand and two hundred persons that
were pure and holy; for as to those that have the leprasy, or the gonor-
rhoea, or women that have their monthly courses, or such as are other-
wise polluted, it is not lawful for them to be partakers of this sacrifice;
nor indeed for any foreigners neither, who come hither to worship.

4. Now this vast multitude is indeed collected out of remote
places, but the entire nation was now shut up by fate as in a prison,
and the Roman army encompassed the city when it was crowded with
inhabitants. Accordingly the multitude of those that therein perished
exceeded all the destructions that either men or God ever brought
upon the world; for, to speak only of what was publicly known, the
Romans slew some of them, some they carried captives, and others
they made search for underground, and when they found where they
were, they broke up the ground and slew all they met with.

There were also found slain there above two thousand persons,
partly by their own hands, and partly by one another, but chiefly
destroyed by the famine; but then, the ill savor of the dead bodies was
most offensive to those that lighted upon them, insomuch that some
were obliged to get away immediately, while others were so greedy of
gain, that they would go in among the dead bodies that lay in heaps,
and tread upon them; for a great deal of treasure was found in these
caverns, and the hope of gain made every way of getting it to be
esteemed lawful. Many also of those that had been put in prison by the
tyrants were now brought out; for they did not leave off their bar-
barous cruelty at the very last; yet did God avenge himself upon them
both, in a manner agreeable to justice.

As for John, he wanted food, together with his brethren, in these
caverns, and begged that the Romans would now give him their right
hand for his security, which had often proudly rejected before; but for
Simon, he struggled hard with the distress he was in, till he was forced
to surrender himself, as we shall relate hereafter; so he was reserved for
the triumph, and to be then slain: as was John condemned to perpetual
imprisonment: and now the Romans set fire to the extreme parts of the
city, and burnt them down, and entirely demolished its walls.

‘Caesar's Birthday Party
(viisiii:1)

1. While Titus was at Cesarea, he solemnized the birth-day of his

brother [Domitian] after a splendid manner, and inflicted a great deal

 
