40 Paradise Restored

The wolf will dwell with the lamb,

And the leopard will lie down with the kid,

And the calf and the young lion and the fatling together;
And a little boy will lead them.

Also the cow and the bear will graze;

Their young will lie down together;

And the lion will eat straw like the ox.

And the nursing child will play by the hole of the cobra,
And the weaned child will put his hand on the viper’s den.
They will not hurt or destroy in all My Holy Mountain,
For the earth will be full of the knowledge of the Lorp
As the waters cover the sea. (Isa. 11:6-9; cf. Isa. 65:25)

On the other hand, God warned, the Curse would reappear
if the people turned away from God’s law: “I will let loose
among you the beasts of the field, which shall bereave you of
your children and destroy your cattle and reduce your number
so that your roads lie deserted” (Lev, 26:22; cf. Num. 21:6;
Deut, 28:26; 2 Kings 2:24; 17:25; Ezek. 5:17; 14:15; 32:4; Rev.
6:8). When a culture departs from God, He surrenders its peo-
ple to the dominion of wild animals, in order to prevent them
from having ungodly dominion over the earth. But in a godly
culture this threat against life and property will progressively
disappear; and, ultimately, when the knowledge of God shall
cover the earth, the animals will be tamed, and harnessed again
to the service of God’s Kingdom.

Finally, in this connection we must consider the dinosaurs,
for there is a whole theology built around them in the Bible.
While the Bible does speak of land dinosaurs (cf. behemoth in
Job 40:15-24, which some mistake for a hippopotamus, but
which is actually closer to a brontosaurus), our focus here will
be on dragons and sea serpents (cf. Job 7:12; 41:1-34—the crea-
ture mentioned in the latter reference, a huge, fire-breathing
dragon called Leviathan, is supposed by some to be a
crocodile!}, Essentially, as part of God’s good creation (Gen.
3:21: sea monsters), there is nothing “evil” about these creatures
(Gen, 1:31; Ps. 148:7); but, because of man’s rebellion, they are
used in Scripture to symbolize rebellious man at the height of his
power and glory.

Three kinds of dragons are spoken of in Scripture: Tannin
(Dragon; Ps. 91:13), Leviathan (Ps. 104:26), and Rakab (Job
