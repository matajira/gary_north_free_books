The Garden of the Lord 45

ernacles,” or booths, made entirely from “the foliage of beauti-
ful trees, palm branches and boughs of leafy trees and willows
of the brook” (Lev, 23:40). Israel usually dwelled in walled
cities, as a protection against their enemies; yet, at the very time
of prosperity (the end of harvest)—when attack would seem
most likely—God ordered them to leave the security of their
homes and journey to Jerusalem, to live in unprotected booths
made of branches, palm fronds, and fruit! God promised, how-
ever, that He would keep the heathen from attacking during the
festivals (Ex, 34:23-24), and Israel had to trust in His strength.

The feast was, obviously, a reminder of life in Eden, when
walled cities were unnecessary; and it looked forward to the day
when the world would be turned into Eden, and the nations
would beat their swords into plowshares (Mic, 4:3). For this rea-
son they were also commanded to sacrifice 70 bullocks during
the feast (Num. 29:12-38). Why? Because the number of the
original nations of the earth was 70 (they are listed in Gen. 10),
and the feast celebrated the ingathering of all nations into God’s
Kingdom; thus atonement was made for all.

It is important to remember that the Jews did not keep this
feast—in fact, they forgot it was even in the Bible — until their
return from captivity under Ezra and Nehemiah (Neh. 8:13-18).
During this period of renewal and restoration, God enlightened
the minds of the prophets to understand the significance of this
feast as an acied-out prophecy of the conversion of all nations
to the true faith. On the last day of the feast (Hag. 2:1), God
spoke through Haggai: “ ‘I will shake all the nations; and they
will come with the wealth of all nations; and I will fill this House
{the Temple] with glory. . . . The silver is Mine, and the gold is
Mine,’ declares the Lorn of hosts” (Hag. 2:7-8). About this
same time, Zechariah prophesied about the meaning of the feast
in terms of the conversion of all nations and the sanctification of
every area of life (Zech. 14:16-21). And hundreds of years later,
during the celebration of the same feast, Christ Himself
declared its meaning: the outpouring of the Spirit upon the re-
stored believer, so that the Church becomes a means of restora-
tion to the entire world (In. 7:37-39; cf. Ezek. 47:1-12).

Israel was to be the means of bringing the blessings of the
Garden of Eden to the world: Scripture goes out of its way to
