146 Paradise Restored

and then we are resurrected —firstfruits, then harvest. (Note
well: no other stages are mentioned.)

Second, we are told when the Resurrection takes place: “at
His coming.” Since we already knew that the Resurrection coin-
cides with the Last Judgment, we now know that Christ’s Sec-
ond Coming will be on the Last Day, at the Judgment.

Third, the text also informs us that these events occur at “the
End.” The end of what? Much needless debate has focused on this
phrase. Paul goes on to tell us that the End comes “when He shall
have delivered the kingdom to the God and Father, when He shall
have abolished all rule and all authority and power.” The End here
is, simply, é#e End —the end of time, of history, and of the world.
This follows, of course, from the fact that this is the fast Day;
moreover, this is the end of Christ’s conquest of the earth, when
He shall have established His total rule over all things, destroying
all His enemies. It is the end of the “Millennium,” the consumma-
tion of the Kingdom —the precise moment when the Book of Rey-
elation, in complete harmony with | Corinthians, places the Resur-
rection and the Last Judgment (Rev. 20:11-15).

Fourth, Christ’s present reign, which began at His Resurrec-
tion and ascension, continues “until He has put all His enemies
under His feet.” This statement comes from Psalm 110-1, where
God the Father says to the Son: “Sit Thou at My right hand, un-
til I make Thine enemies a footstool for Thy feet.” We know
that at Christ’s ascension He did sit at the Father’s right hand
(Mark 16:19; Luke 22:69; Acts 7:55-56; Rom. 8:34; Eph.
1:20-22; Col. 3:1; Heb. 1:3; 8:1; 10:12; 12:2; 1 Pet. 3:22). Accord-
ing to Scripture, therefore, Jesus Christ is now ruling from His
heavenly throne, while all His enemies are being made into a
footstool for His feet. The implications of these texts are in-
escapable: Christ has ascended to the throne, and He will not
return until the last enemy hes been defeated, at the Resurrec-
tion on the Last Day. “For He must reign, until He has put all
His enemies under His feet.”

‘We must remember that the Bible speaks of salvation in
terms of the definitive-progressive-final pattern which we noted
before, Definitively, all things were placed under Christ’s feet at
His ascension to His heavenly throne; in principle, He rules the
world now as the Second Adam. Progressively, He is now
engaged in conquering the nations by the gospel, extending His
