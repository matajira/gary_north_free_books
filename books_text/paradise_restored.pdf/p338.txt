328 Paradise Restored

the primary Kingdom institutions),15 and Ray Sutton’s develop-
ment of Meredith G. Kline’s five-point covenant structure.

 

more faithfully it serves the triune God, the more clearly it becomes an em-
bassy of the Kingdom,”. . .“The office of elder is first of all a family office. . . .”
What the Bible teaches is something very different: the offices of father and
church elder are completely separate institutionally, although the church elder
must first rule his family in an orderly manner.

This overemphasis on the family and underemphasis on the church readily
leads to familism, and then to clanism and even a kind of supposed Hebrew
tribalism, all of which are features of one or another of the groups associated
with the “identity movement,” or “Western destiny” movement, or “British
Israel” movement, which also frequently emphasize the Old Testament dictary
laws.

14. The author I cited in the previous footnote also has written that the
Great Commission (Matt. 28:18-20) places teaching before baptizing, and that
this means that people owe their tithes, not to the local church, but to schools
and to educational foundations: “The Great Commission is a commission to
teach and to baptize: it has reference to education as well as to worship, to the
establishment of schools as well as churches. Teaching is cited before baptiz-
ing. It is teaching which alone can create a godly civil government and a faith-
ful church.”

This is highly misleading exegesis. First, the author has misread the verses
because he used the King James Version without cross-checking with a modern
translation or the Greek, which literally reads: “Going therefore disciple ye all
the nations, baptizing them in the name of the Father and of the Son and of
the Holy Spirit, teaching them to observe all things whatever I gave command
to you... ." The Interlinear Greek-English New Testament, by Alfred Mar-
shall (London: Bagster, 1958), p. 136. In the Great commission, the word
“baptizing” precedes the word “teaching”!

Second, his conclusion would have been unwarranted even if teaching had
preceded baptizing in the verses. Can teaching, as he writes, a/one “create a
godly civil government and a faithful church”? Of course not! The primacy of
worship has always been the fundamental doctrine of the church. We worship
God even when we do not rationally understand every aspect of worship. The
author has fallen into the trap that Christian philosopher Cornelius Van Til
warned against: the primacy of the intellect. The author continues: “The pri-
macy of teaching before church worship and national discipleship are asserted
by Scripture. The great missionary requirement of the days ahead is Christian
schools and institutions. . . . It must become the central area of activity for all
Christians, and for their tithes, in the days ahead.” Teaching is central to mis-
sions? Schools are primary to missions? Isn't worship primary? Isn’t the
church, the Bride of Christ, central?

For a refutation of these views, see James Jordan, “Introduction,” to Jor-
dan (ed.), The Reconstruction of the Church, Vol. 4 of Christianity and Civili-
zation (1986), published by Geneva Ministries, Tyler.

15. James Jordan, The Sociology of the Church (Tyler, Texas: Geneva Min-
istries, 1986).

 
