88 Paradise Restored

And as He was going out of the Temple, one of His disciples said
to Him, “Teacher, behold what wonderful stones and what
wonderful buildings!” And Jesus said to him, “Do you see these
great buildings? Not one stone shall be left upon another which
will not be torn down” (Mark 13:1-2).

And while some were talking about the Temple, that it was
adorned with beautiful stones and votive gifts, He said, “As for
these things which you are looking at, the days will come in
which there will not be left one stone upon another which will
not be torn down” (Luke 21:5-6).

The only possible interpretation of Jesus’ words which He
Himself allows, therefore, is that He was speaking of the
destruction of the Temple which ‘hen stood in Jerusalem, the
very buildings which the disciples beheld at that moment in
history. The Temple of which Jesus spoke was destroyed in the
fall of Jerusalem to the Roman armies in a.p. 70. This is the
only possible interpretation of Jesus’ prophecy in this chapter.
The Great Tribulation ended with the destruction of the Temple
in a.p. 70. Even in the (unlikely) event that another temple
should be built sometime in the future, Jesus’ words in Matthew
24, Mark 13, and Luke 21 have nothing to say about it. He was
talking solely about the Temple of that generation. There is no
Scriptural basis for asserting that any other temple is meant.
Jesus confirmed His disciples’ fears: Jerusatem’s beautiful Tem-
ple would be destroyed within that generation; her house would
be left desolate.

The disciples understood the significance of this, They knew
that Christ’s coming in judgment to destroy the Temple would
mean the utter dissolution of Israel as the covenant nation. It
would be the sign that God had divorced Israel, removing Him-
self from her midst, taking the kingdom from her and giving it to
another nation (Matt. 21:43). It would signal the end of the age,
and the coming of an entirely new era in world history —Jesus
Christ’s New World Order. From the beginning of creation until
A.D. 70, the world was organized around one central Sanctuary,
one single House of God. Now, in the New Covenant order, sanc-
tuaries are established wherever true worship exists, where the
sacraments are observed and Christ’s special Presence is
manifested. Earlier in His ministry Jesus had said: “An hour is
