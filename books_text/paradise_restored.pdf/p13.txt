1
THE HOPE

This is a book about hope. For too long, Christians have been
characterized by despair, defeat, and retreat. For too long, Chris-
tians have heeded the false doctrine which teaches that we are
doomed to failure, that Christians cannot win-the notion that,
until Jesus returns, Christians will steadily lose ground to the
enemy. The future of the Church, we were told, is to be a steady
slide into apostasy. Some of our leaders sadly informed us that we
are living in a “Laodicean age” of the Church (a reference to the
“lukewarm” church of Laodicea, spoken of in Rev, 3:14-22), Any
new outbreak of war, any rise in crime statistics, any new evidence
of the breakdown of the family, was often oddly viewed as prog-
ress, a step forward toward the expected goal of the total coflapse
of civilization, a sign that Jesus might come to rescue us at any mo-
ment. Social action projects were looked on with skepticism: it was
often assumed that anyone who actually tried to improve the world
must not really believe the Bible, because the Bible taught that
such efforts were bound to be futile; as one famous preacher put it,
“You don’t polish brass on a sinking ship.” That slogan was based
on two assumptions: first, that the world is nothing more than a
“sinking ship”; second, that any organized program of Christian
reconstruction would be nothing more than “polishing brass.”
Evangelism was an invitation to join the losing side.

This was rooted in two problems. One was a false view af Spir-
ituality. The unbiblical idea of “spirituality” is that the truly “spirit-
ual” man is the person who is sort of “non-physical,” who doesn’t
get involved in “earthly” things, who doesn’t work very much ar
think very hard, and who spends most of his time meditating
about how he’d rather be in heaven. As long as he’s on earth,
though, he has one main duty in life: Get stepped on for Jesus.

3
