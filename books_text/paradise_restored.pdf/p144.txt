134 Paradise Restored

“the Day of the Lord” was coming. What Amos sought to cor-
rect was Israel’s erroneous expectation of that Day’s outcome
for themselves.

The interesting point (to begin with) is this. Here we find
Amos simply adopting an already understood, full-blown,
highly developed theological concept. The expression itself did
not (apparently) originate from direct revelation, yet the proph-
ets took it up unquestioningly as part of their vocabulary. This
indicates that the term must be based on some Biblical concept
which was so well-known in Israel that the undisputed expres-
sion Day of the Lord almost spontaneously arose to describe it.
How can we account for this? Our answer to this question will
bring us to some surprising conclusions in several areas. More-
over, it will provide us with firm Biblical data about the Second
Coming of Christ —the fiza/ Judgment Day.

Judgment Day in Eden

The Biblical imagery for the Day of the Lord, the Day of
Judgment, begins (as we should naturally assume) in Genesis.
Right at the beginning of the creation account we are told that
God created light and named it Day (Gen. 1:2-5). We must
recognize just what happened at that moment. As we saw in
Chapter 7, God was hovering over the creation, robed in the
glorious light of the Cloud, shining as the original Light (cf.
John 1:4-5). This means that when He created light, it was as a
mirror-image, a sort of “clone,” of Himself. From the start,
therefore, we are taught to associate Day and Light with God.
This basic association is developed and carried through the rest
of the creation week, as the first of two concepts which are im-
portant for our understanding of the Biblical idea of the Day:
Day is in the image of God. The light of day is a reminder of
God’s bright, unapproachable Light (1 Tim. 6:16). For this rea-
son the sun and the dawning of the day are used in the Bible as
symbols of God and His coming (Ps. 84:11; Isa. 30:26; 60:1;
Mal. 4:2; Luke 1:78-79; Eph. 5:14; 2 Pet, 1:19; Rev, 1:16).

The second concept is that Day is the time of Goa’s judicial
assessment of His creatures, when all things are judged by Him.
Here Moses records seven acts of seeing (assessment) and decia-
ration: “God saw that it was good” (Gen. 1:4, 10, 12, 18, 21, 25),
climaxing with the seventh declaration: “And God sew all that
