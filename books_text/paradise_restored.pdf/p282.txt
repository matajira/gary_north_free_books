272 Paradise Restored

pent, and amend what hath been done amiss, even at the last extremity.
Thou hast an instance before thee in Jechoniah, the king of the Jews,
if thou hast a mind to save the city, who, when the king of Babylon
made war against him, did, of his own accord, go out of this city be-
fore it was taken, and did undergo a voluntary captivity with his fam-
ily, that the sanctuary might not be delivered up to the enemy, and that
he might not see the house of God set on fire; on which account he is
celebrated among all the Jews, in their sacred memorials, and his
memory is become immortal, and will be conveyed fresh down io our
posterity through all ages. This, John, is an excellent example in such
atime of danger; and [ dare venture to promise that the Romans shalt
still forgive thee.

“And take notice, that 1, who make this exhortation to thee, am one
of thine own nation; I who am a Jew do make this promise to thee.
And it will become thee to consider who I am that give thee this coun-
sel, and whence I am derived; for while I am alive I shall never be in
such slavery as to forego my own kindred, or forget the laws of our
forefathers. Thou hast indignation at me again, and makest a clamor
at me, and reproachest me; indeed, I cannot deny that I am worthy of
worse treatment than all this amounts to, because, in opposition to
fate, I make this kind invitation to thee, and endeavor to force
deliverance upon those whom God hath condemned.

“And who is there that does not know what the writings of the an-
cient prophets contain in them, —and particularly that oracle which is
just now going to be fulfilled upon this miserable city—for they
foretold that this city should be then taken when somebody shall begin
the slaughter of his own countrymen! and are not both the city and the
entire temple now full of the dead bodies of your countrymen? It is
God therefore, it is God himself who is bringing on this fire, to purge
that city and temple by means of the Romans, and is going to pluck up
this city, which is full of your pollutions.”

A Mother Becomes a Cannibal®'
Wisiii:3-4)

3. Naw of those that perished by famine in the city, the number
was prodigious, and the miseries they underwent were unspeakable;
for if so much as the shadow of any kind of food did anywhere ap-
pear, a war Was commenced presently; and the dearest friends fell a
fighting one with another about it, snatching from each other the most
miserable supports of life. Nor would men believe that those who were
dying had no food; but the robbers would search them when they were
expiring, lest any one should have concealed food in their bosoms,
and counterfeited dying: nay, these robbers gaped for want, and ran

 

 
