258 Paradise Restored

stilt alive, by their relations; nor was there any care taken of burial for
those that were dead; the occasion of both which was this, that every
one despaired of himself; for those that were not among the seditious
had no great desires of any thing, as expecting for certain that they
should very soon be destroyed; but, for the seditious themselves, they
fought against each other, while they trod upon the dead bodies as
they lay heaped one upon another, and taking up a mad rage from
those dead bodies that were under their feet, became the fiercer
thereupon. They, moreover, were still inventing somewhat or other
that was pernicious against themselves; and when they had resolved
upon anything, they executed it without mercy, and omitted no
method of torment or of barbarity. . . .

“The SON Is Coming!?**
(v:vi:3)

3. ... The engines, that all the legions had ready prepared for
them, were admirably contrived; but still more extraordinary ones
belonged to the tenth legion: those that threw darts and those that
threw stones, were more forcible and larger than the rest, by which
they not only repelled the excursions of the Jews, but drove those
away that were upon the walls also. Now, the stones that were cast
were of the weight of a talent,?5 and were carried two furlongs** and
farther. The blow they gave was no way to be sustained, not only by
those that steod first in the way, but by those that were beyond them
for a great space. As for the Jews, they at first watched the coming of
the stone, for it was of a white color, and could therefore not only be
perceived by the great noise it made, but could be seen also before it
came by its brightness; accordingly the watchmen that sat upon the
towers gave them notice when the engine was let go, and the stone
came from it, and cried out aloud in their own country language,
“THE SON COMETH:”?’ so those that were in its way stood off, and
threw themselves down upon the ground; by which means, and by
their thus guarding themselves, the stone fell down and did them no
harm. But the Romans contrived how to prevent that by blacking the
stone, who then could aim at them with success, when the stone was
not discerned beforehand, as it had been till then; and so they
destroyed many of them at one blow, . . .

Josephus Rebukes the Jews?*
(vsix:4)
4. While Josephus was making this exhortation to the Jews, many
of them jested upon him from the wall, and many reproached him;
nay, some threw their darts at him; but when he could not himself per-
