Josephus on the Fall of Jerusalem 243

~—and after him he did the same to his mother, who willingly received
it; and after them he did the like to his wife and children, every one
almost offering themselves to his sword, as desirous to prevent being
slain by their enemies; so when he had gonc over all his family, he
stood upon their bodies to be seen by all, and stretching out his right
hand, that his action might be observed by all, he sheathed his entire
sword into his own bowels. This young man was to be pitied, on ac-
count of the strength of his body and the courage of his soul; but since
he had assured foreigners of his fidelity [against his own countrymen]
he suffered deservedly.

5, Besides this murder at Scythopolis, the other cities rose up
against the Jews that were among them: those of Askelon slew two
thousand five hundred, and those of Ptolemais two thousand, and put
not a few into bonds; those of Tyre also put a great number to death,
but kept a greater number in prison; moreover, those of Hippos and
those of Gadara did the like, while they put to death the boldest of the
Jews, but kept those of whom they were most afraid in custody; as did
the rest of the cities of Syria, according as they every one either hated
them or were afraid of them; only the Antiochians, the Sidonians, and
Apamians, spared those that dwelt with them, and they would not en-
dure either to kill any of the Jews, or to put them in bonds... .

50,008 Jews Staughtered in Alexandria (4.p. 66)
Gisxviti:8)

&. Now when he perceived that those who were for innovations
would not be pacified till some great calamity should overtake them,
he sent out upon them those two Roman legions that were in the city,
and together with them, five thousand other soldiers, who, by chance,
were come together out of Libya, to the ruin of the Jews. They were
also permitted not only to kill them, but to plunder them of what they
had, and set fire to their houses. These soldiers rushed violently into
that part of the city which was called Delta, where the Jewish people
lived together, and did as they were bidden, though not without blood-
shed on their own side also; for the Jews got together, and set those
that were the best armed among them in the fore-front and made
resistance for a great while; but when once they gave back they were
destroyed unmercifully; and this their destruction was complete, some
being caught in the open field, and others forced into their houses,
which houses were first plundered of what was in them, and then set
on fire by the Romans; wherein no mercy was shown to the infants,
and no regard had to the aged; but they went on in the slaughter of
persons of every age, till all the place was overflowed with blood, and
fifty thousand of them lay dead upon heaps: nor had the remainder
