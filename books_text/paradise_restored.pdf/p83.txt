The Coming of the Kingdom B

him, one who js unable to resist the victorious onslaught of
Christ's army. The gates of his city are doomed to collapse be-
fore the relentless attacks of the Church (Matt. 16:18).

The Growth of the Kingdom

At this point some will object: “If Jesus is King now, why
aren’t all the nations converted? Why is there so much ungodli-
ess? Why isn’t everything perfect?” In the first place, there’s no
if about it. Jesus és the King, and His Kingdom das arrived. The
Bible says so, In the second place, things will never be “perfect”
before the Last Judgment, and even the millennium described
by certain popular writers is far from perfect {in fact, theirs is
far worse; for they teach that the nations will never truly be con-
verted, but will only feign conversion while waiting for their
chance to rebel),

Third, although the Kingdom was established definitively in
the finished work of Christ, it is established progressively
throughout history (until it is established finafiy on the Last
Day). On the one hand, the Bible teaches that Jesus Christ is
now ruling the nations with a rod of iron; He is now seated in
power above all other rulers in heaven and earth, possessing all
authority. On the other hand, the Bible also teaches that the
Kingdom develops progressively, growing stronger and more
powerful as time goes on. The same letter to the Ephesians that
tells us of Christ’s absolute rule over creation (1:20-22), assuring
us that we are reigning with Him (2:6), a/so commands us to put
on armor for battle against the devil (6:10-17). There is no con-
tradiction here—just two aspects of the same reality. And the
fact that Jesus is now ruling as King of kings is precisely the rea-
son why we can have confidence of victory in our conflict with
evil. We can experience progressive triumph now, because Jesus
Christ definitively triumphed over Satan in His life, death,
resurrection, and ascension.

Jesus told two parables which illustrate the Kingdom’s
growth. Matthew tells us:

He presented another parable to them, saying, “The
Kingdom of heaven is like a mustard seed, which a man took
and sowed in his field; and this is smaller than all other seeds;
but when it is full grown, it is larger than the garden plants, and
