320 Paradise Restored

(Dominion Press, 1987), which is filled with footnotes, He delib-
erately wrote that book for scholars as well as laymen; Paradise
Restored was written for people, not theologians.

The Recent Spread of Dominion Theology

This book has already had a remarkable effect on several na-
tional television ministries. I can think of two TY preachers who
have switched their eschatologies as a direct result of having
read this book. One of them once mentioned publicly that the
book changed his thinking, but he remains fearful of promoting
it. Jts uncompromising postmillennialism has scared TV minis-
ters, who are probably afraid of scaring away the bulk of their
financial supporters, who remain premillennialists.

Pat Robertson was so concerned that his evangelist peers
might think that he had switched to Chilton’s version of post-
millennialism that he wrote a personal letter to many of them
(including one to me) in the summer of 1986 that stated that he
had not adopted Chilton’s theology. He mentioned Paradise
Restored specifically, Then he outlined his own views, in which,
as a premillennialist, he somehow completely neglected to men-
tion the Great Tribulation. That a doctrine so crucial to premil-
lennial dispensationalism as the Great Tribulation could disap-
pear from his theology indicates the effect that Chilton (or
someone) has had on his thinking.

Nevertheless, the spread of dominion theology is not simply
the result of the writings of the Christian Reconstructionists. If
it were, it would only be some backwater operation. The Holy
Spirit moves widely when He changes a civilization, so that no
single group can claim exclusive credit for God’s work. The
change in Pat Robertson’s thinking (and the thinking of many
premillennialists) had begun several years before Paradise Re-
stored appeared. Rev. Jimmy Swaggart begins a highly critical
article against “kingdom now” theology, including Pat Robert-
son’s version, with a lengthy excerpt from a speech given by Rev.
Robertson on Robert Tilton’s Satellite Network Seminar on
December 9-12, 1984. This was several months before I banded
Rev. Robertson a copy of Paradise Restored, and about a
month before the first edition of the book was published. He
had already made the switch away from traditional dispensa-
tionalism. Here is what Robertson said, as excerpted by Rev.
