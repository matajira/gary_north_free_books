The Kingdom of Priests 199

A Thousand Years

Like the other numbers in Revelation, the “1,000” is sym-
bolic, a large, rounded-off number, Where seven connotes a
fullness of quality in Biblical imagery, the number éert contains
the idea of a fullness of quantity; in other words, it stands for
manyness. A thousand multiplies and intensifies this (10 x 10x 10),
and it is used in Scripture much the way we, with a more infla-
tionary mentality, use the term sniflion: “I’ve told you a million
times!” (Perhaps “literalists” never talk that way, but I’m sure
the rest of us do on occasion.) There is a difference, however.
When the Bible speaks of 1,000, it is not really for the purpose
of exaggeration, the way we do, but simply to express great vast-
ness. Thus, God claims to own “the cattle on a thousand hills”
(Ps. 50:10). Does Hill No. 1,001 belong to someone else? Of
course not. God owns aif the cattle on aé/ the hills. But He says
“a thousand” to indicate that there are many hills, and much
cattle. (For some similar uses of 7,000, see Deut. 1:11; 7:9; Ps.
68:17; 84:10; 90:4.) In the same way — particularly with regard to
ahighly symbolic book — we should see that the “1,000 years” of
Revelation 20 represent a vast, undefined period of time. It has
already lasted almost 2,000 years, and will probably go on for
many more. “Exactly how many years?” someone asked me.
“Pll be happy to tell you,” I cheerfully replied, “as soon as you
tell me exactly how many hills are in Psalm 50.”

According to some, Christ’s Kingdom will begin only when
He returns in the Second Coming; then, they say, Jesus Christ
will actually take up residence in Jerusalem, where there will be
a restored, active Temple, with real sacrifices—sometimes I
wonder if these dear people ever read the New Testament! None
of these ideas are contained in this text (or any other, for that
matter), As we have repeatedly seen, Jesus.Christ is reigning
now (Acts 2:29-36; Rev. 1:5), and He will remain in heaven until
the Last Judgment (Acts 3:2).

The thrones in Revelation 20:4 stand for the reign of the
saints, the faithful overcomers who are victorious over the
Dragon and the Beast (Rev. 12:9-11). Our rule is going on now,
on this earth (Matt. 19:28; Luke 18:28-30; 22:29-30; Eph. 2:6),
and the extent of our rule coincides with the progress of the
gospel. As it increases, so does the dominion of Christians. The
