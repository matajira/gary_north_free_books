Josephus on the Fail of Jerusalem 265

those that went out into the valleys to gather food. Some of these were
indeed fighting men, who were not contented with what they got by
rapine; but the greater part of them were poor people, who were deter-
red from deserting by the concern they were under for their own rela-
tions; for they could not hope to escape away, together with their
wives and children, without the knowledge of the seditious; nor could
they think of leaving these relations to be slain by the robbers on their
account: nay, the severity of the famine made them bold in thus going
out; so nothing remained but that, when they were concealed from the
robbers, they should be taken by the enemy; and when they were going
to be taken, they were forced to defend themselves, for fear of being
punished; as, after they had fought, they thought it toa late to make
any supplications for mercy; so they were first whipped, and then
tormented with all sorts of tortures before they died, and were then
crucified before the wall of the city.

This miserable procedure made Titus greatly to pity them, while
they caught every day five hundred Jews; nay, some days they caught
more; yet did it not appear to be safe for him to let those that were
taken by force go their way; and to set a guard over so many, he saw
would be to make such as guarded them useless to him. The main rea-
son why he did not forbid that cruelty was this, that he hoped the Jews
might perhaps yield at that sight, out of fear lest they might them-
selves afterwards be liable to the same cruel treatment. So the soldiers
out of the wrath and hatred they bore the Jews, nailed those they
caught, one after one way, and another after another, to the crosses,
by way of jest; when their muititude was so great, that room was
wanting for the crosses, and crosses wanting for the bodies.

2. But so far were the seditious from repenting at this sad sight,
that, on the contrary, they made the rest of the multitude believe
otherwise, for they brought the relations of those that had deserted
upon the wail, with such of the populace as were very eager to g0 over
upon the security offered them, and showed them what miseries those
underwent who fled to the Romans; and told them that those who
were caught were supplicants to them, and not such as were taken pris-
oners. This sight kept many of those within the city who were so eager
to desert, cill the truth was known; yet did some of them run away im-
mediately as unto certain punishment, esteeming death from their
enemies to be a quiet departure, if compared with that by famine. . .

“With Their Eyes Fixed Upon the Temple”’
(vixii:3-4)
3. So all hope of escaping was now cut off from the Jews, together
with their liberty of going out of the city. Then did the famine widen
