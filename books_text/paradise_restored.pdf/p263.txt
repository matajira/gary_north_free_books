Josephus on the Fall of Jerusalem 253

taken and the sanctuary burnt, by right of war, when a sedition should
invade the Jews, and their own hand should pollute the temple of
God. Now, while these zealots did not [quite] disbelieve these predic-
tions, they made themselves the instruments of their accomplishment.

Simon’s “Perpetual Bloodskedding’’
Givzix:7-8)

7. ... Thence did Simon make his progress over all Idumea, and
did not only ravage the cities and villages, but laid waste the whole
country; for, besides those that were completely armed, he had forty
thousand men that followed him, insomuch that he had not provisions
enough to suffice such a multitude. Now, besides this want of provi-
sions that he was in, he was of a barbarous disposition, and bore great
anger at this nation, by which means it came to pass that Idumea was
greatly depopulated; and as one may see all the woods behind despoiled.
of their leaves by locusts, after they have been there, so was there
nothing left behind Simon’s army but a desert. Some places they burnt
down, some they utterly demolished, and whatsoever grew in the
country, they either trod it down or fed upon it, and by their marches
they made the ground that was cultivated, harder and more untrac-
table than that which was barren. In short, there was no sign remain-
ing of those places that had been laid waste, that ever they had had a
being.

8. This success of Simon excited the zealots afresh; and though
they were afraid to fight him openly in a fair battle, yet did they lay
ambushes in the passes, and seized upon his wife, with a considerable
number of her attendants; whereupon they came back to the city re-
joicing, as if they had taken Simon himself captive, and were in pres-
ent expectation that he would lay down his arms, and make supplica-
tion to them for his wife; but instead of indulging any merciful
affection, he grew very angry at them for seizing his beloved wife; so
he came to the wall of Jerusalem, and, like wild beasts when they are
wounded, and cannot overtake those that wounded them, he vented
his spleen upon all persons that he met with.

Accordingly, he caught all those that were come out of the city-
gates, either to gather herbs or sticks, who were unarmed and in years;
he then tormented them and destroyed them, out of the immense rage
he was in, and was almost ready to taste the very flesh of their dead
bodies. He also cut off the hands of a great many, and sent them into
the city to astonish his enemies, and in order to make the people fall
into a sedition, and desert those that had been the authors of his wife’s
seizure. He also enjoined them 1o tell the people that Simon swore by
the God of the universe, who sees all things, that unless they will re-

 

 
