When did prophet and vision cease from Israel?
Was it not when Christ came, the Holy One of holies?
It is, in fact, a sign and notable proof of the coming
of the Word that Jerusalem no longer stands, neither
is prophet raised up nor vision revealed among them.
And it is natural that it should be so, for when He
that was signified had come, what need was there any
longer of any to signify Him? And when the truth had
come, what further need was there of the shadow? On
His account only they prophesied continually, until
such time as Essential Righteousness had come, Who
was made the ransom for the sins of all. For the same
reason Jerusalem stood until the same time, in order
that there men might premediate the types before the
Truth was known. So, of course, once the Holy One
of holies had come, both vision and prophecy were
sealed. And the kingdom of Jerusalem ceased at the
same time, because kings were to be anointed among
them only until the Holy of holies had been anointed.
Moses also prophesies that the kingdom of the Jews
shall stand until His time, saying, “A ruler shall not
fail from Judah nor a prince from his loins, until the
things laid up for him shall come and the Expectation
of the nations Himself” [Gen, 49:10]. And that is why
the Saviour Himself was always proclaiming “The law
and the prophets prophesied until John” [Matt.
11:13]. So if there is still king or prophet or vision
among the Jews, they do well to deny that Christ is
come; but if there is neither king nor vision, and since
that time all prophecy has been sealed and city and
temple taken, how can they be so irreligious, how can
they so flaunt the facts, as to deny Christ Who has
brought it all about?

St. Athanasius, On the Incarnation [40]
