Josephus on the Fail of Jerusalem 267

had themselves.

However, when the seditious still showed no inclination of
yielding, Titus, out of his commiseration of the people that remained,
and out of his earnest desire of rescuing what was still left out of these
miseries, began to raise his banks again, although materials for them
were hard to be come at; for all the trees that were about the city had
been already cut down for the making of the former banks. Yet did the
soldiers bring with them other materials from the distance of ninety
furlongs, and thereby raised banks in four parts, much greater than
the former, though this was done only at the tower of Antonia... .

The Murder of the Chief Priest
(vexili:1)

1. Accordingly Simon would not suffer Matthias, by whose means
he got possession of the city, to go off without torment. This Matthias
was the son of Boethus, and was one of the high-priests, one that had
been very faithful to the people, and in great esteem with them: he,
when the multitude were distressed by the zealots among whom John
was numbered, persuaded the people to admit this Simon to come in
to assist them, while he had made no terms with him, nor expected
anything that was evil from him. But when Simon was come in, and
had gotten the city under his power, he esteemed him that had advised
them to admit him as his enemy equally with the rest, as looking upon
that advice as a piece of his simplicity only: so he had him then
brought before him, and condemned to die for being of the side of the
Romans, without giving him leave to make his defence. He con-
demned also his three sons to die with him: for as to the fourth, he
prevented him, by running away to Titus before. And when he begged
for this, that he might be slain before his sons, and that as a favor, on
account that he had procured the gates of the city to be opened to him,
he gave order that he should be slain the last of them all: so he was not
slain till he had seen his sons slain before his eyes, and that by being
produced over-against the Romans; for such a charge had Simon
given to Ananus, the son of Bamadus, who was the most barbarous of
all his guards. He also jested upon him, and told him that he might
now see whether those to whom he intended to go aver, would send
him any succors or not: but still he forbade their dead bodies should
be buried.

After the slaughter of these, a certain priest, Ananias, the son of
Masambulus, a person of eminency, as also Aristeus, the scribe of the
sanhedrin, and born at Emmaus, and with them fifteen men of figure
among the people, were slain. They also kept Josephus’s father in
prison, and made public proclamation, that no citizen whosoever
