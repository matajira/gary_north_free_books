Who, then, is this Christ and how great is He,
Who by His Name and presence overshadows and
confounds all things on every side, Who alone is
strong against ail and has filled the whole world with
His teaching? Let the Greeks tell us, who mock at
Him without stint or shame. If He is a man, how is it
that one man has proved stronger than all those
whom they themselves regard as gods, and by His
own power has shown them to be nothing? If they call.
Him a magician, how is it that by a magician all magic
is destroyed, instead of being rendered strong? Had
He conquered certain magicians or proved Himself
supericr to one of them only, they might reasonably
think that He excelled the rest only by His greater
skill. But the fact is that His cross has vanquished all
magic entirely and has conquered the very name of it.

St. Athanasius, On the Incarnation [48]
