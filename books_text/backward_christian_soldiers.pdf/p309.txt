ABOUT THE AUTHOR

Gary North received his Ph.D. from the Univer-
sity of California, Riverside. He served on the Sen-
ior Staff of the Foundation for Economic Education,
in Irvington-on-Hudson, New York. He presently
edits several newsletters, including the biweekly
financial report, Remnant Review, He is president of
the Institute for Christian Economics. Dr. North’s
essays and reviews have appeared in three dozen
magazines and journals, including The Wall Street
‘Journal, Commercial @ Financial Chronicle, National
Review, Modern Age, The Freeman, American Opinion,
Numismatic News, COINage, Coin Mart, The Ruff
Times, Whole Earth Catalog, Whole Earth Epilog, Journal
of Political Economy, The American Spectator, and others,
He is a popular speaker at investment conferences.
