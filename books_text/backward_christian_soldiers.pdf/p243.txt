optimistic coRPses 290

to their willingness and ability to build for the
future. They have borrowed an ethic more properly
described as Christian, a future-oriented philosophy,
and they have not faced the reality of cosmic mean-
inglessness too consistently. As they grow more con-
sistent, their implicit relativism will destroy them.
But at least the evolutionist knows he must die. He
can defer the implications of meaninglessness until a
million or a billion years into the future.

How, then, does he try to escape from death?
Many ways. One way is to adopt the attitude which
says, “I’ve got five good years left.”

FIVE GOOD YEARS This is a very common atti-
tude. Men in positions of authority in one-man
outfits, especially non-profit outfits, hold this posi-
tion quite frequently. As they grow older, they refuse
to consider the future of their little organizations,
They say to themselves, “I’ve got five good years left,
maybe even ten. No need to start thinking about a
successor. No need to worry about a plan for the
future. I'll think about all that when I’m at the end of
my work, But right now, I've got five good years
left.”

They have another tendency. They equate their
little organizations with themselves. They are the
organization. Then they equate the organization
with civilization’s best and most enduring features.
But when they die, the organization dies, so civiliza-
tion will probably die, too. Therefore, in terms of
their time perspective, they think they are as immor-
tal as civilization. They take no thought of twenty
