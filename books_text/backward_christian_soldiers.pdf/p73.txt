7

CAPTURING THE ROBES

 

“The very same people who the fundamentalists
regard as followers of Satan have set up the
accreditation system, and the fundamentalist
leaders have rushed to submit themselves to
them in order to get their certificates of
academic acceptability.”

 

Robes are a symbol of authority in the West. The
man who wears a robe as part of his profession has
been invested with a degree of formal authority that
other men do not possess. In the West, four groups
wear robes: judges, university professors, ordained
ministers, and church choirs. High school graduates
and college graduates wear their robes once, and
rent them. University professors are entitled to wear
robes at special formal university affairs, and some
(though probably very few) buy them. Only judges
and ministers in the pulpit normally wear robes.
Choirs also wear them, as agents of the church.
