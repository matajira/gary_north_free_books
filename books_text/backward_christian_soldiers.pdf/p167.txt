CRISIS MANAGEMENT AND FUNCTIONAL ILLITERACY 153

too. The results are pretty much the same, either way.

Today we find businessmen who cannot find the
time to read books, They read the Wall Street Journal’s
headlines. They may read a business magazine and
the sports pages of a newspaper. They read business
reports from their employers, or from a customer.
Perhaps they read a financial newsletter or two, But
on the whole, they’re stuck. If they don't read, they
can only grow or advance along proscribed lines.
Not surprisingly, they tend to advance along the
lines suggested by their reading. They advance in those
areas where they still continue to read.

The busy man always places a high premium on
his time. Why not? This is his one non-renewable
resource. Once an hour is gone, it’s gone forever. So
the busy man husbands his time. (Some wives
believe that the verb, “to husband,” when connected
to the noun, “time,” means “to take away from wives
and give to the National Football League.”) He
allocates it carefully. He doesn’t have any time to
waste. Time is money, Wasted time is forfeited
money. At some point, he is more willing to waste
money than time. He pays retail when, with some
extra shopping time, he might have paid only
wholesale. He isn’t being irrational, either. He
selects the resource which is less valuable to him,
and he is more careless with the less valuable
resource.

PASTORAL SCHEDULES What about pastors?
They seldom have extra money, At the same time,
they seldom have extra time. The pastor, unlike
