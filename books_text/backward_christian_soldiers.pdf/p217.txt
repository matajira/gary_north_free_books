CHURCH NEWSLETTERS 203

but then you have trouble with zip codes (which you
need in sequence if you mail bulk rate). The zip
codes must be sorted, then arranged numerically.

I recommend a mailing at least once a month, to
remind readers that you are still around. A church
newsletter aimed at topics of general concern is im-
portant. You should not be sending out an “in-house”
letter to those outside the church family. You must
demonstrate your relevance.

You also need a mailing list to announce special
meetings, a conference, a movie, or other occasional
programs. If you have the person’s phone number on
your data file, it helps. But you do not want it on the
actual mailing label that goes in the mail.

Not many churches will ever develop a mailing list
the size of Calvary Temple’s in East Point, Georgia.
The Temple Tones goes out to over 10,000 people. But
there is always the possibility that your church will
develop a list of 2,500, if you work at it all the time.
Any time you go over 500, you have to start thinking
about computerizing.

CONCLUSION The newsletter is a new tool of com-
munication. Why shouldn’t your church get one
started?

The Geneva Divinity School makes available a
monthly newsletter to churches. Space for your
church’s masthead is provided. It deals with general
topics of concern. In Tyler, Texas it’s called the Tpler
Christian Observer. Your church or group can buy
camera-ready copy inexpensively this way. Write to:
Geneva Divinity School, 708 Hamvasy, Tyler, TX
75701.
