HUMANISM'S ACCOMPLICES 89

tape of his talk. “I’m still confused, somewhat. . . . I
have not been able yet to get it sorted out what it
means to be an economist and also to be a
Christian.” Fair enough; then he should have stop-
ped right there. But he didn’t. He felt constrained to
lay the foundations of defeat. “The world is not
going to get better, It is under the domain of Satan,
It was never under the domain of the Christian. It’s
under the domain of Satan. It’s coming to an end,
maybe quicker than we perceive. I wish to God it
were today, So out of this chaos—and I think it’s
great; I think our current situation is great— because
out of this chaos, there’s only one hope, and that’s
Jesus Christ. And I think every Christian is called to
evangelize. . . . I'm preaching to you.”

He did not have in mind evangelization through
the construction of a biblical view of economics. He
meant “tract-passing.” He said that the Bible teaches
that we cannot please our fellow man, that we can
expect persecution (and he expects to be persecuted),
and that hope is secured in this world through suffer-
ing. He said he is certain about the following facts.
He is called, first, to exercise the Great Commission
to witness to people about Christ. Second, he has a
spiritual gift to be exercised in the body of Christ.
Third, he is called to exercise spiritual leadership in
his family. “Those three things I know for sure.
What other things I have to do in this economy [dis-
pensation?] I’m not sure of.” In short, he must
devote himself to personal evangelism, the institutional
church, and his family. The rest of life he is not sure
about.
