23

THE COMPUTER

 

“It Is time to take advantage of the power that
inexpensive computer technologies have
delivered to middle-class people and thelr
organizations.”

 

You have heard a Jot about the computer revolu-
tion. Adam Osborne’s little paperback, Running
Wild, describes how it took place, 1975-80. You may
be thinking about what it can do for your church. If
you have not thought about it, now is the time to
begin.

The processing of data is imperative. We need
greater speed, reduced cost, and simple-to-use
equipment. At last, we are getting it. The best com-
puter deal for the money in 1984 is the Kaypro I, for
about $1,700, which includes $1,000 worth of “soft-
ware” (programs). For $2,800, they will include a
10-megabyte hard disk drive, which will store a pile
of information in its large memory—many thous-
