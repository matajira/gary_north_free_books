152 BACKWARD, CHRISTIAN SOLDIERS?

time is the fundamental Christian occupation. If a
man is able to produce a hundred dollars’ worth of
goods or services per hour, then every hour used for
other purposes costs him, and the community, a
hundred dollars. If a man can write an article in a
day and earn $1,000, and he instead repairs a leaky
faucet, which would have cost him $50 to get
repaired by a plumber, then he has lost $950 ($1,000
minus $50, excluding tax considerations). This
assumes, of course, that he can crank out $1,000
worth of articles every day of the week, and that the
lost income cannot be retrieved by writing the same
article the next day, a day which would have been
otherwise non-productive.

When anyone develops his talents to the point that
he has become a productive member of the commu-
nity, he finds that his “free” time becomes more
precious, precisely because it really isn’t free. In fact,
of anything he owns, his time is no longer free. A
child may have relatively free, meaning inexpensive,
time on his hands. He hardly knows what to do with
all his spare time, But spare time, or spare anything,
is an asset that disappears once the possessor finds
ways to put it to profitable, income-producing uses.
‘The day a man finds ways to make money from his
spare time is the day he no longer has time to spare.
Every minute devoted to watching television is a
minute’s worth of income forfeited.

BUSY ILLITERATES An illiterate is a person who
cannot read. What should we call a person who can
read but refuses to? I would call him an illiterate,
