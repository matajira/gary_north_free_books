182 BACKWARD, CHRISTIAN SOLDIERS?

springing up everywhere. They do not report on
what they are doing or where they are. These schools
are like hornets. There are too many of them to fight
effectively. one by one.

The tactical problem facing Christians is this:
How can we gain the benefits of a centralized, well-
paid organization, yet avoid the concomitant bur-
eaucratization? How can we mobilize the army, yet
keep all the troops in the field, constantly sniping at
the enemy? How can we train local men to carry the
battle to government officials, yet make certain that
the local people are ready and able to fight successful
battles? We do not want legal precedents going
against us because the local headmasters and their
lawyers were not well prepared. We already face a
situation where the civil governments are attacking
schools continually in order to get adverse legal
precedents.

Obviously, few churches and Christian schools
can afford to hire local lawyers at $100 per hour.
Besides, lawyers face the problem of specialization.
They have to educate themselves in a new field.
‘There are cases to read, arguments to master, in
state after state. There is no doubt that since the late
1970’s, there has been a coordinated effort on the
part of Federal, state, and local education officials to
limit the Christian schools. The state attorneys are
no longer being surprised by new arguments of the
defense lawyers, as they were in the early 1970's,
Precedents are going against Christian schools to-
day, Prosecuting attorneys know who the better-
known defense witnesses are and what they will say.
