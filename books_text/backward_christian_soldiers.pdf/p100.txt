86 BACKWARD, CHRISTIAN SOLDIERS?,

Christian labor unions (which have no distinctly
Christian economic approach to the analysis of wage
rates), and Christian political parties (which are a
political impossibility in twentieth-century America
—another program of built-in failures). There is
nothing wrong with these activities, but they are
“holding actions” the products of a “holding action”
eschatology. They are not programs of victory, but
programs of cultural isolation testimonies to a fallen
world which cannot respond to the presentation of a
Christian witness.

American fundamentalism, to the extent that it
has an approach to apologetics (philosophical
defense of the faith), is mired deep in the old Roman
Catholic-Princeton methodology of intellectual syn-
cretism (mixture). Furthermore, American funda-
mentalists generally have adopted a premillennial,
dispensational eschatology. Like amillennialism,
premillennialism denies the possibility of cultural
victory on earth prior to the physical, bodily return
of Christ to set up a universal earthly kingdom.
Again, it does little good, in the eyes of the consistent
fundamentalist, to construct Christian cultural alter-
natives, except as a witness~a witness inevitably
doomed to cultural failure.

Pessimism erodes the incentives to create detailed
alternatives to collapsing secularism. Men can make
a witness far more readily to Christ’s offer of salva-
tion of souls than they can to Christ’s offered healing
of human institutions. “Witnessing” for the fun-
damentalist means calling attention to the rot of
secularism and then offering a personal life preserver
