222 BACKWARD, CHRISTIAN SOLDIERS?

target specific segments of their audiences who are
ready for specific programs: first by education, se-
cond by organizational mobilization. In short, we
need feet, hands, and eyes, and each subgroup within
various large television audiences needs specific
guidance and training in order to become proficient.

There is no doubt that CBN would be better able
to begin this program of specialized training than
any of its competitors—as of early 1984. It has the
broadest audience. Scattered within any given
prime-time audience, there are more people who
might be interested in getting involved in a particu-
lar action program. CBN is on the air 24 hours a
day. It can therefore devote specific time slots to
identifying and developing segments of the overall
viewing audience, but without alienating the viewers
as a whole. Also, Pat Robertson, not being a pastor,
is less of a threat to the egos and programs of the na-
tion’s pastors.

CBN University offers an institutional base for
Jaunching an educational program. Robertson’s
Freedom Council offers an institutional base for the
creation of political education and training. If each
of these two organizations can recruit the services of
outside specialists in the particular areas, then the
expertise of the “fulcrum developers” can be put to
use. I have in mind such non-profit organizations as
the Free Congress Foundation, the Rutherford In-
stitute, the American Vision, the Foundation for
American Christian Education, the Foundation for
Christian Self-Government, the Institute for Chris-
tian Economics, the various creation research organ-
