288 BACKWARD, CHRISTIAN SOLDIERS?

Raff Times, 170
Rushdoony, R. J...
73, 128, 147
Russell, Bertrand, 227

xi, 6,

sacerdotalism,

sales, 34

salt ofearth, J

sanctification, J7
personal, x
progressive,
social, x

Satan, 48, 5759, 89, l4,
LT, 145

satellites, 148
education & 219
reconstruction &, 227

16, 20

x

resistance, 279-27
SAT’s, 160
Schaeffer, Francis, 147
schizophrenia, 26, 29
scholarship, 133
scholarships, 94, 97
science, 30
Scopes Trial, 144
Scotland, 167
Scott, Gene, 149, 226
Scott, Otto, 230
secular, 16
Secularism, 68
secularism,

Christian colleges & 85

weakness, 5, 7
self-government, 39, 130
Seminaries, 6, 60-65, 176

seminary, 169
Seneca, 36
Shochox, 202

“Shotgun approach,” 200-1
Sider, Ronald, 65
Sileven, Everett, 278

sin, 42, 109, 119

Smith, Adam, 142
Smith, Bailey, 26

social action, 1

social change, 246-47 26f
society, 246

soldier, 69

soldiers, 109

Solzhenitsyn, Aleksandr, me
South, 52

South Korea, 100

sovereignty of God, 2% 31.

Soviet Union, 14
Sowell, Thomas,
spaceship, 243
specialization,
sprinters, 243

d7t

St. Andrews University, 169:

stalemate,
standards,

126, 175
State, 73, 77 78, 80, 177

102, 107
67, 70, 123,

195-96, 224.

State (financing), 254, 256f.

status quo, 80, &2
Stent, Gunther, 6
strategy, 49, 115, 134
surrender, 103
survivors, 237
syncretism, §5, 275-76

 

 
