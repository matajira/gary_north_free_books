208 BACKWARD, CHRISTIAN SOLDIERS?

comers could listen to tapes in a few afternoons, or
driving to and from work, or while cleaning the kit-
chen? Shouldn’t the fundamentals be put on tape, ina
simple form, with the detailed instruction coming
later on, once the listeners have a grasp of the basics?

When pastors treat their own time as a free good,
their parishioners will do the same.. Pastors of large
churches have assistant pastors to take care of the
personal problems of most members in most in-
stances. But to make good use of his time, the pastor
of a small church has to find substitutes for a team of
assistant pastors. His time should not be devoted to
any project for which a substitute~a less expensive
substitute —is economically possible. Screening should
be done more mechanically, by means of tape teaching.

QUALITY PRODUCTION The audio cassette tape,
being cheap, is used by many people. Yet few people
really ever listen to the tapes produced by cheap
hand-held machines, with their pitiful built-in con-
denser microphones. The reproduction is atrocious.
These are the sound equivalent of the 1898 Brownie
camera. You simply would not want to spend time lis-
tening to them. You could not hear these lectures in a
car, given the noise of the engine and traffic. A cheap
cassette tape recorder is a toy for actual recording.
Something worth doing at all is worth doing well.
Never use a cheap mimeograph machine and a man-
ual typewriter, when you can use an IBM Selectric TII
and an offset press. Never use an-IBM Selectric IIL
when you can use a microcomputer and a good word
processing program. Use the best tool you can afford
