56 BACKWARD, CHRISTIAN SOLDIERS?

crackpots, too, for I also warned you. But more im-
portant, I warned you because I took seriously the
word of God, whereas the secular crackpots have ig-
nored the explicit teachings of the Bible as thor-
oughly as the humanists in Washington have ignored
them. That’s what long-term social critics have to
do: criticize. But to be effective in the long run, the
critics must be crilicizing in terms of a framework. They
must be criticizing in terms of a reliable standard.
That is what most other critics are not doing today.
So stock up on diesel. You may think you have no
need of such information. But keep it around for a
few-years. It may surprise-you in the future.

THE RUDE AWAKENING The rude awakening is
coming. It always does. Men cannot go to sleep at
the wheel indefinitely. There will be an accident. Or
more accurately, there will be a nasty result. You
cannot expect a civilization to sleep at the wheel for-
ever, with the engine running at top speed, and not
crash. Such crashes are hardly accidents.

When the awakening comes, there will be a frantic
search for solutions—immediate, bread-and-butter
solutions—and then scapegoats, and finally answers
to existing problems. Woe to the man or movement
that is discerned to be a scapegoat. What happened
to the business community in the 1930’s can happen
to any group that is riding high in the days immedi-
ately preceding the crisis. What happened in Britain
to the government of Neville Chamberlain in Sep-
tember of 1939 is only tao typical.

Christians are not riding high today. It is not our
