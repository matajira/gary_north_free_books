3

ESCHATOLOGIES OF SHIPWRECK

 

“The State needs pastors who preach a theol-
ogy of defeat. It keeps the laymen quiet, in
an era In which Christian laymen are the most
significant potential threat to the unwarranted
expansion of state power.”

 

The great chapter in the New Testament which
deals with the division of labor within the church is I
Corinthians 12. The basic teaching is found in verse
12: “For as the body is one, and hath many mem-
bers, and all the members of that one body, being
many, are one body: so also is Christ.” The church,
the body of Christ, is to perform as a disciplined, in-
tegrated body performs. It is not to fight against it-
self, trip itself, or be marked by jealousy, one mem-
ber against another.

The twentieth century has brought with it a
deplorable application of these words. Instead of
viewing the body of Christ as a symbolic body with
