8

HUMANISM’S CHAPLAINS

 

“, .. talk conceming a Christian world-and-life
view is incredibly cheap; the test is this: What
are the sources and standards for constructing
a biblical alternative?”

 

The most important question of human knowl-
edge is this one: “By what standard?” Is there some
sort of universal reason which provides all mankind
throughout all ages with a sufficient basis for making
jadgments? Or is the very idea of intellectual neu-
trality a snare and a delusion?

Historically, Christians and secularists have taken
both sides. In their attempts to devise a universally
valid intellectual defense of the faith, Christian
apologists have appealed to “natural law” or “the law
of non-contradiction,” or some other common
ground methodology. They have hoped that logic
might bring rebellious men face to face with the
claims of Christ. As Cornelius Van Til has demon-
