eed, NOT 1948 53

When this happens to an electrical wire, it leads to
power failures and fires. The same thing is true of
cultures.

The evangelical world can no longer persist in the
illusion that the American Republic is still opera-
tional as it was in the days of our Founding Fathers.
It has gone the way of all flesh. The faithful are at
ease in Zion. God has sent them fatness, and put
leanness into their souls (Ps. 106:15).

The pastors of America are stuck. If they wish to
model their ministries along the lines of the. Old
‘Testament prophets, all the fat, comfortable sleepers
will stir, rub their ears, and grow angry. “Why are
you preaching all this doom and gloom stuff? We get
enough of that on T.V. We get that at the office all
day long. We don’t come to church to get more of the
same. We come for relief.” They ireat the church as if tt
were a giant glass of Alka Selizer. Relief is just a swallow
away.

Want to shrink a minisiry to the point of invisibility?
Just start preaching specific sermons on specific sins
that are dear to the hearts of the faithful, Just start
teaching a view of Christian responsibility that goes
beyond heart, hearth, and sanctuary. Recently, a
relatively “concerned” evangelical church in Texas
had the opportunity of bringing members out to hear
some information on abortion and the possibility of
setting up an abortion hot-line, to try to keep women
from killing their children. Almost no members of
this good-sized church showed up. “This sort of topic
isn't spiritually uplifting,” one member informed the
pastar.
