226 BACKWARD, CHRISTIAN SOLDIERS?

of higher education, political training, and legal
defense.

As the number of Christian broadcasting alter-
natives expands, we will be able to create a new edu-
cational system. Communications are crucial. Now
that Gene Scott has pioneered the low-cost reception
dish idea, others can get involved. An investment of
$777.77 allows a church to receive one station; one
station is only the beginning. When Christian
broadcasters know that they can reach an audience
of thousands of churches, each armed with a satellite
reception dish and several videocassette recorders,
they will be able to restructure modern Christian
education, including political education. The mo-
nopoly of the humanist media will be definitively
broken. The sooner your church buys a reception
dish, the sooner this monopoly will be destroyed.

Since late 1982, several part-time “networks” have
sprung up. Robert Tilton’s is the largest, the Word
of Faith Church’s network. It is received by over
1,100 local churches. He plans to launch a 22-hour
per day full-scale network by late 1984. This will be
imitated. A revolution in Christian mobilization is
imminent. Buy a dish.
