62 Covenant Sequence in Leviticus and Deuteronomy

Appendix D of my forthcoming book Touch Not, Taste Not: The
Mosaic Dietary Laws in New Covenant Perspective,® the fourth com-
mandment should be seen to begin with 21b. And, chapter 13 has
to do with false mediators, not with wearing God’s name emptily.

The third commandment has to do with the character of God’s
people, those who take up (wear) His Name. They are not to do
so in vanity, in the’ sphere of death and impotence. As I have
briefly argued in Chapter 2 of this monograph, the third com-
mandment is the general concern of Leviticus 17-22. In Leviticus,
the food laws are put in the section concerning the second com-
mandment, and within that section they signify fidelity to the first
commandment (no covenantal idolatry). Leviticus 11-16 deal with
cleansing and mediation. Here in Deuteronomy, however, cleans-
ing is not in view. Rather, the character of the person is in view,
which is in the third commandment’s zone of concern,

There are only two stipulations in this section, and each has
the same form, a form not found in the laws preceding or follow-
ing them. Unfortunately, the versification obscures this structure.
An outline of Deuteronomy 14:1-21a will illuminate the matter:

14:1-2a—Wholeness in Self as God's Image:
Who you are: Sons of God.
Stipulation: Do not deface God's image, for the sake of the dead,
Glosure: You are a holy people to God.

14:2b-21a--Wholeness in Relations:
Who you are: Separated from the peoples.
Stipulation: Do not eat detestable food, or anything dead.
Closure: You are a holy peopie to Ged.

Fourth Commandment: Deuteronomy 14:21b-16:17

The commandment not to boil a kid in its mother’s milk initi-
ates the sermon on the fourth commandment. Symbolic laws also
initiate the sections on the seventh, ninth, and tenth command-
ments. In the present case, the boundary closures are clear froma

&. Tyler, TX: Institute for Christian Economics, 1989.
