The Strizture of the Book of Deuteronomy 61

breaking of any of the rest involves a breaking of the covenant,
and thus of the first commandment. ( Just so the last command-
ment, focussing on the heart’s motivations, is broken when any of.
the rest are.) The first commandment concerns covenantal idolatry,
while the second concerns liturgical tdolatry.

A. The Covenant Made at Sinai:
1. The God of Israel, 6:1-19
. Initial Deliverance from Egypt, 6:20-25
Commandment, 7:1-I1
. Sanctions, 7:12-16
. Continuity, 7:17-26

VeRO

To have continuity, they must resist graven images. But, at
Sinai, they made a golden calf, and broke the covenant. Thus, we
go through the covenant sequence again, showing that God re-
newed the covenant with them.

B. The Covenant Renewed after Being Broken:

1. The God of Israel and His sovereign choice, 8:1-9:6
2. Breakdown and Renewal, 9:7~10:1

3, Commandment, 10:12-11:12

4. Sanction, 11:13-17

5, Continuity, 11:18-32

Second Commandment: Deuteronomy 12-17

Worship and mediation are the themes here. Chapter 12 con-
cerns the place of mediation, and chapter 13 the treatment of anti-
mediators, those who would draw the people into relations with
other gods.

Third Commandment: Deuteronomy 14:1- 21a

The boundaries of the third commandment have been a puz-
zle to exegetes who have wrestled with the question.” Kaiser and
Kaufman include 13:1-14:27. For reasons I set forth below and in

7. Kaiser, p. 132; Kaufman, p. 125.
