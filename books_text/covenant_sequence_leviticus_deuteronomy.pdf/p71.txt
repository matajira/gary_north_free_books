The Structure of the Book of Deuteronomy 67

omy 25:5 and end with 25:16. I believe, however, that the com-
mands to exterminate Amalek and to rejoice with God are rele-
vant to the question of covetousness.

25:4—the levir is entitled to benefit from his nephew's land
* while he rears him (cf. 1 Cor. 9:9; 1 Tim. 5:18).'?

25:5-40 ~ the delinquent Ievir, who hopes to get his brother's
land by refusing to do his duty. He loses his shoe, his protection
against the cursed soil, and thus stands to lose his own land! Be-
cause he won't give seed, he receives spit (a symbol of seed).

25:11-12 — the female assailant. This is the reverse of the case in
Exodus 21:22. An attack on procreation, and thus on the house.

25:13-16—the tools of the thief are forbidden to the Israelite.
In literary terms, two unequal stones in one bag relates directly
to the preceding case. Perhaps under “natural law” one might
reason that since God gave men two unequal stones, we might
also employ them in commerce. Such an inference is rejected
here.

25:17-19—cure for covetousness: war on God's enemies.

Chapter 26—cure for covetousness: pay tithes and rejoice
with God.

IV. Impitementation

The fourth aspect of the covenant is implementation, Wit-
nesses are established, who will report on the faithfulness or faith-
lessness of the people. The sanctions, positive and negative, are
set out, so that the people will know what to expect. Deuteronomy
27-30 are concerned with this.

I. Witness stones, 27:1-10

Il. Ebal and Gerizim, 27:-26. The land is here brought under
covenantal sanction.

12, Kaiser and Kaufman put this with the ninth commandment. I agree with
Carmichael that this law has to do with the levirate. See Calum M. Carmichael,
* Treading’ in the Book of Ruth,” Zeitschrift fir die Alttestameniliche Wissenschaft 92
(4980):248-266. My own understanding of Aow this relates to the levirate differs
from Carmichael’s.
