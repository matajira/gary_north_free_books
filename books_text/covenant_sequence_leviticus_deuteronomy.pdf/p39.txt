The Structure of the Book of Leviticus 35

Notice that Leviticus 17:7-18:28 are the only four laws of the book
that are said to apply to God-fearers as well as Israelites. Their
grouping here creates a clear link between chapters 17 and 18. The
early church in Acts was concerned about the relationship between
Israelites and God-fearing uncircumcised gentiles, who now were
being put into the one body of Christ on a equal footing. (The dis-
tinction is made explicit in- Acts 13:16, 26.) This was the concern
of the Jerusalem Council of Acts 15, which decreed on the basis of
Leviticus 17 and 18 that God-fearers were only obligated to the
Moaaic law in these four areas: “abstain from things sacrificed ta
idols and from blood and from things strangled and from fornica-
tion” (Acts 15:29).

The book of Genesis explicitly links the prohibition on drink-
ing blood with the prohibition on bloodshed (Gen. 9:4-5). Thus,
the affirmation that only the true God has life in Himself (first
commandment) is linked with the prohibition on murder (sixth
commandment). Leviticus 17 clearly belongs with the first of the
five aspects of the covenant.

Chapter 18 discusses sexual sin and idolatry (v. 21) together,
correlating with the second and seventh commandments and the
second aspect of the covenant. Chapter 20 goes over much of the
same ground as chapter 18, but this time adds the punishments for
sin. Both human judgments (ninth commandment) and Divine
judgments (fourth commandment) are discussed, the fourth
aspect of the covenant. We are on safe ground, then, in assuming
that chapter 19 has to do with the third aspect, and we have
already seen the predominance of the Name of God in this chap-
ter (third commandment). Disobedience to the law is called tres-
pass, a form of theft (eighth commandment), and Leviticus 19 is a
rehearsal of the whole of the law. A detailed look at the structure
of Leviticus 19 is found in Chapter 3 of this monograph.

Finally, chapters 21-22 concern the priests and the sacrifices.
The priests are Israel's spiritual parents (fifth commandment),
and the sacrifices are Israel’s gifts. The people must offer God
good sacrifices, and not try to cheat. They must not covet but give
freely (tenth commandment). Thus, these chapters deal with the
