22 Covenant Sequence in Leviticus and Deuteronomy

pointment of successors, artistic enhancements of the content of
the covenant (e.g., memorial pillars, songs) designed to help fix it
in the heart, and anything else having to do with carrying the cov-
enant to succeeding generations. Agreeably, the fifth command-
ment enjoins obedience to parents as the most important condition
for long life in the land. Similarly, inberitance in the land is im-
possible if each man covets his neighbor's goods, and refrains from
tithing (tenth commandment). I believe that continuity and suc-
cession can be seen to be the general concern of Leviticus 24-27.

Leviticus 1-7

The sacrifices that occupy the first seven chapters are them-
selves arranged by this pattern. The first section, chapters 1-3,
concerns the relationship between God and man directly: The
Burnt Offering affirmed God’s transcendence, the Cereal or Tribute
Offering affirmed the Israelite’s fealty to God, and the Peace
Offering affirmed God's fellowship with man. The Purification
Offering had to do not with cleansing the individual sinner, but
with cleansing God’s house, society at large, which was symboli-
cally defiled by the presence of sinners.? The house of God was the
place of mediation, so appropriately the Purification Offering is dis-
cussed next. The Compensation Offering had two purposes: to
deal with theft (point three) and with perjury (point four). Finally,
the last point of the covenant/re-creation sequence has to do with
succession, the appointment of servants to continue the work
begun by the master. Appropriately, Leviticus 6:38-7:36 has to do
with the priests.

Leviticus 1-7

Burnt Offering, ch. t, transcendence
Cereal Offering, ch. 2, fealty
Peace Offering, ch, 3, fellowship

2. Gordon Wenham, The Book of Leviticus (Grand Rapids: Eerdmans, 1979),
pp. 848. The true house or habitat for God is “in the midst” of His people, and in
their hearts. Thus, the Temple and Tabernacle symbolized the people. In terms
of this, when the people sinned or became onclean, an equivalent defilement “ap-
peared” on the altars at the center of God's house. The Purification Sacrifice
removed this defilement and cleansed the house.
