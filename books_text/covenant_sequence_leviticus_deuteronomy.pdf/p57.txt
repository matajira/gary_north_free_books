An Outline and Analysis of Leviticus 19 53

stipulations of Leviticus 27. Similarly, the potential offspring of
the seduction of a slave-girl belongs to God, and must be redeemed
by a Compensation Offering in the form of a sacrifice. In this case
a monetaty equivalent will not suffice.?

Thus, if you mate two kinds of animals, the offspring belengs
to God. If you mix seeds in your field, the harvest belongs to God.
Mixed clothing is the prerogative only of the priest, those who are
holy to God (Ex. 28:6—dyed thread is woolen). If you wear it,
you become holy and thus liable to the priestly rules; but since you
are not a priest, this can only bring judgment upon you. You
would have to de-sanctify yourself by buying yourself back under
the rules of Leviticus 27.

Verse 20 says, “And a man: if he sleeps with a woman—emis-
sion of seed. . . .” The express statement regarding seed again as-
sociates this law with the previous one concerning mixed seed in
the field. The slave-girl is “designated,” which is equivalent to
being betrothed. Seduction of a betrothed woman was a capital
offense (Dt. 22:23-27). Since the woman is a slave, she probably
would not feel free to cry out, so she is innocent, but the man is
guilty (Dt. 22:25-27). In this case, however, since the woman is a
slave, the penalty is not death. Damages must be paid, a dowry-
compensation probably equal to or double what the girl was being
married for.*

The seducer must also bring a Compensation Offering to de-
sanctify the potential offspring. Because sin was involved, a
money payment will not do, but blood sacrifice is required.°

The second subsection gives the law for fruit trees, The fruit of
the first three years is to be regarded as uncircumcised. Since it is

3. On the Compensation Offering, see James B. Jordan, “The Death Penalty
in the Mosaic Law: Five Exploratory Essays" (available from Biblical Horizons,
P.O. Box 1320/1, Tyler, TX-75713), chap. 2.

4. Wenham argues persuasively for the translation of bikkoreth as damages.
Gordon Wenham, The Book of Leviticus (Grand Rapids: Eerdmans, 1978), pp.
270-71,

5, Jacob Milgrom discusses the relationship between the Compensation Offer-
ing and Leviticus 27 in Milgrom, Cult and Conscience: The “Asham’ and the Priestly
Doctrine of Repentance, Studies in Judaism in Late Antiquity 18 (Leiden: E. J. Brill,
1976), pp. 448. Milgrom discusses Leviticus 19:20-22 on pp, 129. of the same
book. My own approach differs somewhat from his.
