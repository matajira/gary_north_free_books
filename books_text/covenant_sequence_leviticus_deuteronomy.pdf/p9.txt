Covenant as a Literary Structuring Device 5

is based on John Frame’s work.§ It is also possible and desirable to
see the sequence as having five aspects:?

. God’s transcendence (1, 2).

. New order and hierarchy (3-5).

. Stipulations (6, 7).

. Sanctions (blessings and curses) (8, 9),
. Succession arrangements (10-12).

vow Ne

If we look at how God institutes His new Kingdom progres-
sively in history, we shall basically be concerned with four steps or
stages:

1. God’s Announcement of His intention, including His judg-
ment of the old world.

2. The Exodus of God’s people to a new world.

3. The Establishment of God's people in the new world.

4. The History (and decline) of the new world, by means of
the application of sanctions.

(5.) God’s Judgment of the world, which is simultaneously His
Announcement of a new world, and thus step 1 of a new sequence.

Each of these patterns can be found in the Bible. Given the
prominence of the number seven, and the hebdomadal sabbatical

6. Footnote j above.

7. The most expansive treatment of the five-fold approach is found in Sutton’s
That You May Prosper; cf, Sutton, Who Owns the Family?: God or the State? (Fort
Worth: Dominion Press, 1986); Second Chance: Biblical Blueprints for Divorce and
Remarriage (Dominion, 1987). See also Sutton’s newsletter, Covenant Renewal, pub-
lished by the Institute for Christian Economics, P.O, Box 8000, Tyler, TX 75711,

Gary North has worked with this model in several books, including The Sinat
Strategy: Economics and the Ten Commandments (Tyler, TX: Institute for Christian
Economics, 1986); Liberating Planet Earth: An Introduction to Biblical Blueprints (Fort
Worth: Dominion Press, 1987); Inherit the Earth: Biblical Blueprints for Economics
(Dominion, 1987); Healer of the Nations: Biblical Blueprints for International Relations
(Dominion, 1987).

Other significant literature emplaying one or another version of this model
includes David Chilton, The Days of Vengeance: An Exposition of the Book of Revelation
(Fort Worth: Dominion Press, 1987); Gary DeMar, Ruler of the Nations: Biblical
Blueprints for Government (Dominion, 1987); George Grant, The Changing of the
Guard: Biblical Blueprints for Political Action (Dominion, 1987).
