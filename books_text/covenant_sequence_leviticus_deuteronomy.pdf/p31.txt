The Structure of the Book of Leviticus 27

Spirit. And so forth. Understanding these things, however, we
can read Leviticus 8 as a re-creation passage.

First, Aaron and his sons are washed (Lev. 8:6). The dirt of
the Old Creation is removed from them, and they are prepared
for entrance into the New. Next, Aaron is invested with his
garments of glory and beauty (vv. 7-9). This corresponds to No. 1
above, the formation of Adam.

The next event is Moses’ anointing the Tabernacle with oil
(wy. 10-il). The Tabernacle had already been built, but now it is
anointed with the oil of the Spirit, and made fully “alive” as God’s
new earthly Garden-sanctuary. This corresponds to No. 3 above.

Then comes the anointing of Aaron (v. 12), as the Spirit “breathes”
life into him (No. 2 above), After this, Aaron’s sons are brought
near and clothed (v, 13), these being the helpers fitted to assist him
(No. 5 above).* Following this, animals are brought to Aaron (vv,
14ff.), though this time not to be named but to be sacrificed (No. 6
above). Both naming and sacrificing are acts of dominion.

Then Aaron is told to guard the Tabernacle, positioning him-
self at its doorway (forecourt), for seven days (No. 4 above). He is
to do this on pain of death (No. 7 above): “At the doorway of the
tent of meeting, moreover, you shall remain day and night for
seven days, and do guard duty for the Lord, that you may not die,
for so I have been commanded,” said Moses (v. 35).°

The other details of Leviticus 8 need not concern us at this point,
because what is of interest here is only the parallel to Genesis 2.°

4, Maritally, Adam’s helper is Eve. In labor, his helpers are his sons. In terms
of the theology of the book of Numbers, the helpers fitted for the priests are the
Levites, In terms of the theology of the books of Samuel, the helper fitted for the
warrtor-king is his commander in chief (1 Sam. 26—the question here is whether
David or Abner has better assisted Saul in fighting off a “Satanic attack”),

5. “Do guard duty” is the literal meaning of the command here. For a full,
though very technical, discussion, see Jacob Milgrom, Studies in Levitical Terminol-
agy (Berkeley: University of California Press, 1970), pp. 5.

6. For the most part, the remaining details of Leviticus 8 concern purification
offerings designed to cleanse the altar of defilements caused by Aaron and his
sons, so that they can have access to it, and also the offering of a Peace Sacrifice
that is analogous to Passover. The Passover lamb is a species af Peace Offering,
and like at the first Passover, blood of the “Ordination Peace Offering” is placed on
Aaron’s ear, thumb, and toe as a sign that he is “under the blood” and so that
‘God's plague will pass him by when he draws near.
