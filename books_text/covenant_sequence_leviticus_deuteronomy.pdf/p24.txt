20 Covenant Sequence in Leviticus and Deuteronomy

Lev. 23:22, 43
Lev, 25:55
Num, 10:10
Num, 15:41

Tam the Lor who sanctifies you.
Lev. 20:8
Lev, 22:32

For I am the Lorn your God:
Lev. 11:44
Lev. 20:7
Lev, 24:22
Lev, 25:17
Lev. 26:1

Lam the Lorn your God, who has separated you:
Lev. 20:24

Cut off
Gen. 17:14
Ex. 12:15, 19
Ex. 30:33, 38
14
7:20, A, 25, 27
Lev. 17:4, 9, 10, 14
Lev. 18:29
Lev. 19:8
Lev. 20:3, 5, 6, 17, 18
Lev. 22:3
Lev. 23:29
Num. 9:13
Num. 15:30, 31
Num. 19:13, 20

 
 

The predominance of the phrases “I am the Lorn” and “I am
the Lorn your God” in Leviticus 19, a total of fifteen out of

1. This phrase lacks the absolute character of the other statements of God's
name. If we subtract its incidences, the contrast between Leviticus 17-22 and the
rest of the book is even more pronounced.
