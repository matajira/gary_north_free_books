12 Covenant Sequence in Leviticus and Deuteronomy

Thus, the first five commandments generally follow the con-
cerns of the covenant in sequence. The same is true of the last five
commandments.

The sixth commandment forbids manslaughter. The taking of
human life is a prerogative reserved to God and His appointed
steward-authorities alone. For man to murder is to seize at God’s
transcendence and to abuse the integrity of other men. It is per-
haps significant that the prohibition of murder is the only one of
the ten commandments expressly found in Genesis, as part of the
Noahic covenant (Gen. 9:5-6).

The seventh commandment forbids adultery. This is because mar-
ital order and relationship is the fundamental form of order in the
Bible. The order in the Garden was for the husband to teach, feed,
and guard his wife. This order was reversed in sin, but is restored
as Christ teaches, feeds, and protects His bride. Thus, the overall
concept in prohibiting adultery has to do with respect for God and
care for the poor. God's deliverance of His bride from Egyptian
“rape” is the theme of Exodus. (Ex. 1:16, 22. Compare the previous
exoduses of Abraham from Egypt and Philistia, and of Isaac from
Philistia: In each case, the bride was under attack; Gen. 12, 20, 26.)

The relationship between the seventh and second command-
ments is significant and interesting. The Bible very often states
that the relationship between God and His people is one of mar-
riage, but this is never ritualized in any kind of sexua} fashion. All
the pagan religions had ritual sex to show that they were married
to their gods. These religions also bowed down to idols, showing
that their gods were within their reach. The second command-
ment requires us to affirm the absolute physical transceridence of
God and that our relation with Him is wholly in the Spirit and
covenantal. The same thing is affirmed in the absence of sex from
biblical liturgy. The requirement of physical monogamy in the
seventh commandment correlates to the requirement of Spiritual
fidelity in the second,

The eighth commandment forbids theft. Respect for the property
of others clearly connects largely with the third zone of the five-
fold covenant structure, because the third area is that of the dis-
