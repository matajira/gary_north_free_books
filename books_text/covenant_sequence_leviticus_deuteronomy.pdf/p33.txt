The Structure of the Book of Leviticus 29

8:30 Garments consecrated —robes of office
8:31-36 Remain in doorway (place of birth) seven days
8:35 Do guard duty

B. Chapter 9: Aaron enters his labors

9:1-7 Moses’ commands
9:8-14 Sacrifices for Aaron
A. Purification Offering
B. Burnt Offering
9:15-21 Sacrifices for the people ~ the bride brought to Adam
A. Purification Offering
B. Burnt Offering
CG, Peace Offering
9:22-23 Aaron blesses the people
9:24 Fire from God

Chapter 9 shadows the covenant making sequence:
9:1-7 Moses’ commands initiate action.
9:8-14 Aaron’s sacrifices are his transition into service.
9:15-21 Aaron’s performance of sacrifices for the people
is his duty, his obedience to the law governing
him as priest.
9:22-23 Aaron's blessing of the people has to do with
sanctions.
9:24 God’s fire initiates the altar fire, which will be
kept burning continually into the future.

C. Chapter 10; Cain and the sons of Aaron

Background: Exodus 4:14-17; 7:1; 32:1-6: Aaron’s Appoint-
ment and Fall
Leviticus 8: “Clothing of skins” (blood)
restoration
Leviticus 10; Rebellion of sons (Cain)
10:1-2 Strange fire
10:3-5 Judgment and removal
10:6-7 No- mourning in God's presence
10:8-11 God speaks to Aaron
No drinking on the job
Distinguish holy and profane, clean and unclean
Teach Israel the law
10:12-15 Despite sin, they still have access to holy food
10: 16-20 Applying the law in new situations
