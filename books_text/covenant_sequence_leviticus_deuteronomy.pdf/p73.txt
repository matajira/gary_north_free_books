The Structure of the Book of Deuteronomy 69

II. The Testament of Moses, 32:48-34:12. This section also seems
to follow the.covenant sequence:

A.

B.

c.

God speaks to Moses, 32:48

Moses is to move from the people to the mountain, a tran-
sition, 32:49-52

Moses describes the covenant grant given to Israel,

33:1-29

. God implements the sanction of death upon Moses,

3421-8

, Joshua takes Moses’ place, 34:9-12
