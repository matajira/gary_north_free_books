Covenant as a Literary Structuring Device 13

tributed grant. We have to respect what God has granted to others.
Also, disobedience to any part of God’s law is regarded as a trespass
or more literally a “debt,” as we see in the Lord’s Prayer. Thus,
any lawbreaking is a form of theft, creating indebtedness, which
must be covered by a Trespass or Compensation Sacrifice. Theft
has to do with boundaries, which is why it is equivalent to tres-
pass. Leviticus is the book of boundaries, of who is allowed to go
where, and of how to become cleansed once you have trespassed.

The ninth commandment forbids false witness bearing. As we
mentioned above, witness bearing has to do with the application
of sanctions, and thus is associated with the fourth zone of the
covenant, This is highlighted in Numbers. Ten of the spies
brought back false witness of the land. Balaam was commissioned
by Balak to curse Israel with false witness.

The tenth commandment forbids coveting. The man who is
covetous will tend to act to disinherit his neighbor, and prevent his
succession from continuance. In a wide sense, this is a major
theme of Deuteronomy. Moses stresses over and over that God
had given Israel a good land, plenty good enough for all of them.
They would be the envy of other nations, but they were not to
covet other nations, but be content with what God had given
them. At the national level, covetousness leads to war, and Deu-
teronomy greatly restricts war. (Judges 18 records an instance of
this, when the Danites rejected what God had given them,
coveted another land, and conquered it.) Moses stresses that if
they come to covet the things that the gods of the other nations
have given their people, they will forsake the Lord and worship
those gods, hoping to get the same “blessings.” If they do this, the
Lord will cast them out of the land, Thus, covetousness is strongly
associated with the idea of succession and inheritance, and with
the concerns of Deuteronomy.

Conclusion

We can pull together what our “inductive” study has provided
and summarize the five points as follows:
