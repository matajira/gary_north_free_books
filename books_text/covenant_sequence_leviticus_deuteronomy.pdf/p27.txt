The Structure of the Book of Leviticus 23

Purification Offering, 4:1-5:13
Mediation

Compensation for thefts, 5:15-19
Stealing glory from God

Compensation for perjury, 6:1-7
Compounding. ordinary theft with
false oaths before God

Priestly administrators, 6:8-7:36
Honored by sacrificial portions

Covenantal sequencing can be found in the individual sections
here. For instance, using Frame’s three-fold covenant outline (see
pp. 4-5 above) we find the sacrifices in Leviticus 1-3 (the food offer-
ings) are arranged as follows:

Control Whole Burnt Offering Submission to God
Authority Cereal Offering Fealty to God
Presence Peace Offering Fellowship with God

Numerous groups of three are found within these chapters.
There are three kinds of animals for Whole Burnt Offerings (cat-
de, sheep or goats, and birds), three kinds of cooked grain Cereal
Offerings (oven, griddle, and pan, 2:4-10), and three kinds of ani-
mals for Peace Offerings (cattle, sheep, and goats). We shall find
that there are also three occasions for the Peace Offering (thanks-
giving, vows, and freewill; 7:12-16),

We should take note of the hierarchical arrangement of the
Purification Offerings in the second section:

High Priest, bull, 4:3-12

Congregation as a whole, bull, 4:13-21

Civil leader, male goat, 4:22-26

Common people, female goat or lamb, 4:27-31, 32-35
(Explanation of the sin requiring this sacrifice, 5:1-6)
Poor people, two doves or pigeons, 5:7-10

Very poor people, tenth of an ephah of fine flour, 5:11-13

In the Compensation Offerings, found in sections three and
four, we again find a triad:
