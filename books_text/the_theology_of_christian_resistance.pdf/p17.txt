EDITOR’S INTRODUCTION xvil

The world-and-life view of classical civilization was in the
process of disintegration.’ This was what eventually brought
down Rome, not the external military pressures of the bar-
barians. The church faced a tottering giant. By the fourth
century, the will to resist Christianity had gone out of the
Empire’s leaders. The military was divided, and army fac-
tions kept raising up new candidates for Emperor, who then
battled against their rivals. The Roman Empire became
Christian when classical culture could no longer sustain it.

The insightful political theorist, C. Northcote Parkinson,
has thrown light on the question of successful resistance.
Parkinson is most famous for Parkinson's Law, a book-length
discussion of modern bureaucracy. The law states: “Work ex-
pands so as to fill the time allotted for its completion.” But he
is a serious thinker. He summarized an important lesson from
successful revolutions:

In studying the history of revolt we find that it seldom occurs,
and still more rarely succeeds, under a regime that is sure of itself.
Our first inental picture may be of brave men ploiting against a
crushing tyranny, against a ruthless and cruel despot, against an ex-
clusive group of the heartless and the proud. We soon realize, how-
ever, that men are not as brave as that. They do not rebel against
strength but against weakness and indecision, Revolutions take
place when the regime is wavering and, above all, divided.1*

We must learn from history. We will be swallowed up if
we attempt a frontal attack today, for we have few “troops in
reserve.” Why do I say that we have few troops in reserve?
Aren’t there millions of Christians in the United States? Prob-
lem: the millions who profess Christianity in the United
States are little more than occasional church attendees. They
will not get involved in anything more controversial than an
Easter sunrise service. They will, if anything, try to destroy
the work and reputation of a committed resister, for the com-
mitment of the “enthusiast” testifies against the complacency of the pew-
sitiers, They resent it. The resister tells them that this present
world order is evil, and therefore it may soon come under the
judgment of God. He tells them that they have a moral

13. Charles Norris Cochrane, Christianity and Classical Culture (New York:
Oxford University Press, [1944] 1957).

14, C. Northcote Parkinson, Let Luggage: A Caustic History of British
Socialism from Marx ta Wilson (Boston: Houghton Mifflin, 1967), p. 27.
