RAHAB’S JUSTIFIABLE LIE 69

prayer of lust is condemned in another (Mark 11:24; James
4:3). Romans 7:1-3 seems to teach that if your husband is
alive but you are married to another man, you are without
qualification an adulteress. But Jesus’ statement in Matthew
19:9, where fornication is enlisted as just ground for divorce
and remarriage, is emphatically exceptional. The same ap-
plies to truthtelling, which is qualified by Scriptural precept
and Scriptural example. On the one hand, there are
generalizations that exalt truthtelling (Romans 12:17); on the
other hand, there is the inspired qualification (James 2:23; I
Samuel 16:1-5). So we must register the logical point that
what is right “in general” may not be universally and without
qualification. This is the logical cliche of the exception prov-
ing the rule instead of negating it.

The Significance of the Ninth Commandment

The words, “Thou shalt not bear false witness against thy
neighbor” mean exactly what the words indicate: It is morally
unlawful to speak falsely against your neighbor. The force of
this commandment is to protect human relationships as well
as to foster love. Note: The cormmandment does not say that
“thou shalt never tell a lie.”

Words are often more visceral than cerebral, and the word
“lying” is not an exception; so a point on terminology: It
needs to be underscored that there does not have to be a one-
to-one correspondence between the English word and the
Biblical word. Why? Because the languages are different. So
we can be flexible on terminology as long as the point is clear.
Connotatively, “lying” is an opprobrious terrn; denotatively,
“lying” may not always be. For example, one might say, “she
lied, but did not bear false witness.” The Bible, and not the
Queen's English, must be our focus. That Rahab spoke falsely
is incontrovertible; but that Rahab spoke falsely against her
neighbor is what is denied. One cannot intelligently discuss
the Ninth Commandment without contrasting the Western
(Greek) and Hebrew conceptions of truth. As Dr. Hendrick
Krabbendam has said:

The former pertains to the exposure of that which is hidden,
The latter denotes dependability; constructive, rather than
destructive ability. The Bible opposes truth as exposure (such as
gossiping); but promotes truth as a constructive entity,
