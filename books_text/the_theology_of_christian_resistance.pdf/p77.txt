WHAT THE WAR IS REALLY ABOUT 37

headed toward a time when the government would license
clergymen and churches; when the churches would be
tolerated and permitted to function, as in ancient Rome, if the
preachers bend their knees and hail Caesar, Such “churches”
would be instruments of government policy. In Communist-
occupied Rumania, for instance, there are “churches.” In
them, worshippers pray to Ceausescu, not to God. Who is
Ceausescu? Didn't you know, Pilgrim? Nicolae Ceausescu is
the Communist dictator of Rumania, Rumania is where the
real preachers are being tortured in jail. It now becomes as
realistic as it is incredible to speculate about the same thing
happening here.

In Louisville, Nebraska, near Omaha, the Reverend
Everett Sileven recently was in jail. Reverend Sileven is the
Pastor of Faith Baptist Church, which runs the Faith Chris-
tian School, and the Nebraska government doesn’t like it. In
the fall of 1981, a judge ordered the Reverend Sileven to close
the school down. Nebraska wants to certify the teachers.
Pastor Sileven tells your obedient servant that his teachers are
certified —by God. Judge Raymond Case ordered the sheriff
to padlock the church building where the classes were taking
place. Remember, this happened not in Rumania, but in
Nebraska. When your reporter talked with him, the Pastor
was planning to lead a miniature Exodus of the children
across the Missouri River every day to the relatively free state
of Iowa, were they would go to school. Whether any of the
pursuing Nebraska forces were drowned, we don’t know.
Later, the Pastor reopened Faith Christian, the government
threw him into jail, and threatened to throw his wife into jail!
Even the Mob wouldn't touch a man’s wife. This is reminis-
cent of the Nazis and Communists.

And it is just another example. There are others. Do you
still think your reporter's fears are far fetched?

Why is this happening? At the beginning of this treatise,
we spoke of a religious war. One good proof of that war is the
exclusivity of the government schools. Along with Mao Tse-
tung, the liberals who run them say, “Let a hundred flowers
bloom, Let a thousand schools of thought contend,” but, in
practice, they won't Jet the children who want Lo do so pray. If
the advocates of Do Your Own Thing were not motivated by
another religion, they wouldn’t care. Their absurd attempt to
exclude the Lord is an expression of the hostility one religion
