136 CHRISTIANITY AND CIVILIZATION

and temporal government, considered such a connection a
“calamity and a curse.” But at the same tire, the knowledge
of God revealed in Scripture was so commonly diffused, and
Washington's dictur that religion is a pillar of society was so
commonly believed, that it was prabably not anticipated by
Christians of the Revolutionary era that the Constitution
might be made to operate on the human rights principle with-
out any recognition of God.

Yet again it is notoriously the case that few who take a
wrong turn in the path of life anticipate the misery that they
will come to further along. The direction taken by the
Virginia declaration has no other way to go but toward, at the
very least, holding God to have no place in temporal govern-
ment. The analysis agrees in toto with that of Groen van
Prinsterer, a nineteenth century Dutch thinker now being
republished. He finds the cause of revolution to be unbelief:
“¥ should therefore like to let you see that as a matter of simple
logic atheism in religion and radicalism in politics are not only
the exaggeration, misuse or distortion, but that they are in
fact the consistent and faithful application of a principle which
sets aside the God of Revelation in favour of the supremacy of
reason. [ should like you to see, in addition, that because this
principle contradicts the very essence and immutable order of
things, it is possible to predict, even without the light of
history, the drift of events and the metamorphosis of the prin-
ciple as it has continued to reassert itself.” I would amend this
so as to say simply, “the principle which sets aside the God of
Revelation” for any vain notion or imagined substitute.

The Virginians need not have anticipated this logical
development and probably never thought of themselves as
leaving the “Rock” of belief.

In fact, it will be shown later on that insofar as human
rights as set forth in the Virginia declaration are made
specific, they are in fact a kind of back-handed statement of
benefits of the common law which Christians enjoy. It was
easy to confuse logical origins. The common law punishes any
dishonest violation of each man’s person or his goods and so it
is easy to understand a condition in which each may be said to
enjoy the “right to life, liberty and property.”

Nevertheless, colonial Virginians did know they were do-
ing something different. They were introducing a change, and
a radical one. The whole Christian world understood the
