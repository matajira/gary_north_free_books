140 CHRISTIANITY AND CIVILIZATION

Court is not unrelated. However stated, it makes some men
the manipulators of other men, and that validated by no more
than who can get away with the most. It is the imaginary law’
of the jungle, giving rise to the abominable dogma of “sur-
vival of the fittest” and the warrant for every political
upheaval that may be engineered. Law and government are
play-things for man, and what “human rights” always boils
down to in practical analysis is the defense of any and all
political upheavals simply because they are political.

Freedom of speech and freedom of press are, in fact, ap-
plied seriously only to giving government protection to in-
stigators of riot and rebellion, as well as those who would
undermine human order by more subtle attacks on morals
and customs. When Mr. Carter advises the Kremlin he wants
them to respect human rights, he cannot possibly mean the
right to own property, since this is expressly repudiated by the
very doctrine that makes Communism what it is. Since no one
has yet been able to explain what “liberty” is without
reference to the Creator God, it follows that Mr. Carter has ©
no concrete notions about that either. But what he means is
clear as a bell: he means he wants Russians to be allowed to
criticize their government to any degree without going to jail
for what they say or do. Further, he wants those who are in
jail for subversion or political offenses to be set loose. How far
he wants to go with this right to revolution no one can teil, but
this is without doubt what he is advocating.

Lawful and Right

The dream that mankind might become its own law-
maker, of course, is not new. Adam’s sin was precisely that he
undertook to live by such a system (the fruit of the tree of
knowledge of good and evil, “knowledge” meaning also con-
trol or determination). We can only dimly imagine the extent
of the catastrophe that followed immediately upon the whole
earth; but we do know that the end of Adam’s scheme was
death. And we also know that God overrode Adam, instituted
restrictive decrees to curb his drive back toward the nothing
from which he came, and held out the promise of redemption
through the “seed of the woman.” In short, God, through law
and justice, prevailed and God was not thwarted in his pur-
pose for human destiny,
