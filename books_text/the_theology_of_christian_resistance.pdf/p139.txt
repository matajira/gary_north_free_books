CHRISTIAN RESISTANCE TO TYRANNY 99

sleep by communist tactics of “peaceful coexistence”!§ and
“detente,”!6 pursuing trade and aid to our sworn and proven
foes, together with disarmament talks, even while our would-
be destroyers have been undertaking the greatest peacetime
arms buildup in the history of the world. Third, we, together
with our western “allies,” have supplied our active and
dedicated enemies with technological wherewithal to account
for the vast bulk of Soviet economic and industrial growth and
modernization since 1917, and particularly since 1933,
thereby also contributing to the buildup and modernization of
the world’s greatest strategic and conventional military force,
by supplying everything from the world’s largest and most
productive truck (and tank) plant to computers for missile
guidance systems. Anthony Sutton, our foremost expert on
the subject, is not inaccurate when he terms such policies
National Suicide.

 

many strategic metals which we surrendered to a pro-communist regime
under the Carter Administration) and our continued adversary relationship
io South Africa running a close second for stupidity and ungodliness. The
Carter Administration's sellout of our ally Somoza, and of Nicaragua, is ofa
piece with past policy; for, having given away our Panama Canal, one of the
four main strategic waterways in the world, to a pro-Castro Marxist, it is
only logical that we should give up Nicaragua, historically the alternate
canal route, to a group of Marxist terrorists, the Sandinistas.

15. Hyans, pp. 305-307, reminds us that communist bosses from Lenin to
Brezhnev have affirmed and reaffirmed an intention to use “peaceful co-
existence” as a means of gaining ultimate victory over America and the
West. Lenin stated the basic doctrine with stark clarity: “The existence of
the Soviet Republic side by side with the imperialist states for a long time is
unthinkable. In the end one or the other will conquer, and until that time
comes, a series of most terrible collisions between the Soviet republics and
the bourgeois states is inevitable.”

16. Brezhnev said in 1966: “It goes without saying that there can be no
peaceful coexistence where matters concern the internal process of the class
and national liberation struggle in the capitalist countries or in the colonies.
Peaceful coexistence is not applicable to the relations between oppressors
and oppressed, hetween colonialists and the victims of colonial oppression.”
In 1972, he said of “detente” that it “in no way signifies a possibility of
weakening the ideological struggle... . On the contrary, we should be
prepared for an intensification of this struggle.” As Evans notes, the
“liberals” have fallen into this communist trap consistently.

17. Anthony Sutton’s National Suicide: Military Aid to the Soviet Union (New
Rochelle, N.Y.: Arlington House, 1973) should be required reading for
every Christian, and for every American. The work is but a distillation and
a sequel to Sutton’s earlier authoritative and scholarly studies which docu-
