RAHAB’S JUSTIFIABLE LIE 73

sheer arbitrariness and feeling. Secondly, the situations where
deceit is envisaged may be normal or abnormal, whereas we
have limited them to the abnormal (that is, war against crime
especially when life and possessions are at issue). The words
of John Warwick Montgomery are a telling indictment of the
school of situation ethics:

This brings us necessarily to a corollary of the ancient logical
conundrum, treated at length by Bertrand Russell and others: ‘If
a Cretan tells you that all Cretans are liars, can you believe him?’
Our restatement goes: ‘If a situation ethicist halding to the prop-
osition that the end justifies the means in love, and tells you that
he is not lying, can you believe him?’

The Bible states that love and the commandments of Christ
are mutually affinitive. What is the law of the Lord but an
elucidation of the demands of love? We must echo Mont-
gomery again: in listening to the school of situation ethicists
there is absolutely no way of determining, short of sodium
pentothal, when they are telling the truth. Any degree of
prevarication may be allowed on the basis of homage to love.

Keeping in mind that a Christian is always under a vow to
refrain from speaking evil against his neighbor, we must make
at least a limpid attempt to apply these principles. Lovers of
fictitious canines can rest easier as well as wily vacationers
who set their light to go on and off automatically, The Chris-
tian has no obligation to speak truthfully to those who have
forfeited the right to hear the truth (Bonnie and Clyde
citizen).

Because a Christian is always at war with crime he has the
moral responsibility to lic, if necessary, to protect his
neighbor. Since it is a responsibility raandated by God, it
should not be defined as a “lesser of two evils” either. To make
this contention is not only to deny that a lawful escape is
always open (I Corinthians 10:13), but promote the practical
effect of de-Christianizing the Christian by enslaving him in
the ald unconverted state where he is faced once again with
“not possible not to sin” alternatives (Romans 3:10-12; Mat-
thew 7:17-18). So, in what contexts may the justifiable lie be
rightly used? Some suggestions: A Christian may fake a heart
attack if he is about to be robbed; he may speak falsehoods to

3. John Warwick Montgomery, “Situation Morality: The Ethics of
Immaturity,” The Outlook (February, 1972), p. 22.
