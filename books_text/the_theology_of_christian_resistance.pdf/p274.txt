234 CHRISTIANITY AND CIVILIZATION

identified by its title.

These radical versions of the original American politics,
with their emphasis on equality and liberty in the abstract
(and their presumption of a great break with our European
past) follow from an excessive concentration upon the en-
thusiasms of a few articulate philosophers of the closet, upon
off-hand remarks and peripheral figures, thus ignoring the
fact that things assumed in a given era or community are not
often defended in print. There is no credit to be had from
stating the obvious—unless the obvious is in real danger of
being misunderstood, or is under serious attack. To this date
we may observe responsible public figures (like a former am-
bassador who appeared not long ago on Bill Buckley’s series)
whe speak of the American paradigm as justifying a “continu-
ing revolution” of constitutional tinkering, unfounded expec-
tations, and sentimental meliorism: an impious levering away
at a viable social and political order in the name of certain
propositions which appear nowhere in our Constitution.
These gentlemen are greatly confused concerning the truth
about our early history. And their confusion threatens what
remains intact from an originally wholesome political in-
heritance.

In achieving a proper perspective on the American
Revolution, of what it signified and what it did not mean, it is
first necessary that we recover some of the eighteenth-century
understanding of the word “tyranny.” Opposition to tyranny
was a mainspring of the American Revolution. There were, of
course, all sorts of justifications for the collective American
decision to seek our independence of the authority of Grown
and Parliament. Some of these were entirely secular; and
some were both secular and religious, Both varieties, how-
ever, contained a theoretical objection to tyranny, with an at-
tendant definition of that crime. Simplistic millenarianism of
the variety that sometimes appeared in the Puritan camp dur-
ing the English Revolution of the 1640s was so rare as to be an
insignificant feature of the American explanation/apologia

3. Henry Steele Commanger, The Empire of Reason: How Europe Imagined
and America Realized the Enlightenment (Garden City, N.Y.: Anchor Press/
Doubleday, 1977).
