DEFENSIVE WAR IN A JUST GAUSE SINLESS 223

divided against itself; our dispute is with administration. This
is cause of great sorrow, that such a heavy judgment has
befallen the kingdom; and yet we are not without some in-
stances in scripture of people refusing obedience to kings,
when they became arbitrary and oppressive. When
Rehoboam threatened Israel with nothing but tyranny, they
did not long hesitate till they gave the king this answer, “What
portion have we in David? Neither have we inheritance in the son af
Jesse: To your tents, O Israel! Now see to thine own house David.” I
Kings xi: 16.

And this certainly has been the faith of Great Britain, as
might be made to appear by many instances, one of which I
shall at present just mention. When King James II departed
from the constitution, and became arbitrary, by dispensing
with acts of Parliament by proclamation, issuing guo warrantos
against charters, and endeavouring to introduce popery, the
people esteemed it no sin to invite William, the Prince of
Orange, to invade England, and obliged James to abdicate a
kingdom he had forfeited his right to govern.

But there are some texts of scripture urged on this occa-
sion, which were to be considered. The following are thought
to enjoin non-resistance in the strongest terms, viz. Let every
soul be subject to the higher powers: For there is no power but of Gon;
the powers that be, are ordained of Gon. Whosoever therefore resisteth
the power, resisteth the ordinance of Gov: and they that resist, shall
receive to themselves condemnation, Rom. xti:1, 2, etc. The apostle
is no friend to anarchy, for he well knew the corruption and
depravity of man would oppress the innocent, if there were no
legal restraints. But, in order rightly to understand these
words, it is of great importance to determine first, what is
meant by the higher powers: For this is what every soul is to
be subject unto. We shall find na exposition liable to less ex-
ception, than to understand by the higher powers the just, the
good, the wholesome and constitutional laws ofa land, merely
respecting civil government. The very design of these higher
powers is to secure the property and promote the happiness of
the whole community. These higher powers therefore appear
as binding on princes as people. And as Gop has ordained
these powers for the good of the whole, whosoever resisteth
the same, resisteth the ordinance of Gop, and receiveth finally
condemnation, and that justly; for anarchy and tyranny are
essentially the same, and equally to be dreaded, as cach resists

  
  
