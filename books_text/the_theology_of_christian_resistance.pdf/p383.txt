REVIEWS 343

rary Christian community in this regard does not justify the
wholesale compromise of Biblical strategy advocated by those who
insist that all the “haves” must become “have nots” if they are to be
truly Christian,

A final issue to be discussed is a supposed tension in the mind of
some evangelicals between training and conversion, “But today’s
evangelicals seen to have lost their faith in the power of conversion
and insiead look to Christian education to insure the salvation of
their young” (p. 49). “Do modern Baptists think it is possible to ac-
complish by education what their forebears believed came only from
conversion?” (p. 50). This is another false issue, The issue, if it can
be called such, is not education versus conversion. The question is
simply the freedom to present the gospel verbally as opposed to the
non-verbal witness Van Alstine endorses (p. 118). Rather than an
education versus conversion mentality we should sec it as a debate
aver environment, where one side is appealing for an environment
which encourages and substantiates the claims of Christ, while the
other side is calling for the continued support of an environment
which encourages growth in a secular mindset. Evangelicals forfeit
their right to the term evangelical if they fail ta understand the doc-
trine of regeneration and “look to Christian education to insure the
salvation of their young.” Where such thinking exists it is wrong,
but my suspicion, supported by specific experiences in the Christian
school movement, is that this attack is a “straw man” attack.

Hats which are hung on Van Alstine’s work are the hats of the
undiscerning. This book notwithstanding, Christian schools are the
true corollary to evangelical profession, just as schools in general are
the true corollary to any profession. May we be spared the kind of
“salt and light” our children are supposed to be in public schools.
May we instead be busy giving consistent training to our covenant
children with a vision for their subsequent mature witness-bearing to
an apostate culture.

Clarence B. Carson, The World in the Grip of an Idea (Westport,
Conn,; Arlington House, 1979), 562 pp., $14.95. Reviewed by
Tommy W. Rogers,

The book is a magnificent treatise on the idea which holds much
of the world its sway. It is most commonly designated as socialism or
by the more inclusive name of collectivism, Fabianism, democratic
socialism, and gradualism are other carriers. The more virulent
wing of the movement is communism. At a deeper level, the general
