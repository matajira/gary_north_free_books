CONFIRMATION, CONFRONTATION, AND GAVES 51

the church withdraws its support for the existing civil govern-
ment in favor of a new civil government. It anoints new
leaders. It supports the “lesser magistrates” in their challenge
to the central government which has broken the covenant
with God and the people.

The question then arises: Which churches constitute the
legitimate anointing agent? All tax-exempt religious organiza-
tions? All denominations? Bible-believing churches? And how
is the resisting civil magistrate or resisting individual Chris-
tian to know whether a new civil government has been
anointed by the lawful church? The Israelites did not know
that Samuel had anointed David while Saul was alive, or that
in principle (though probably not in time), Eljah had
anointed Hazael to replace the line of Ahab, And because
they did not know, they were not called to revolt. How will
Christians know if they are being called to resist by a legiti-
mate anointing agent?

Perfect confidence is like all perfection: beyond attainment
this side of the final judgment. But Christians who believe in
the authority and infallibility of the Bible, and recognize their
theological peers, will have more confidence in the lawfulness
and wisdom of resistance at a given point in history when they
sce a majority of church officers of Bible-believing congrega-
tions step out and proclaim the right of resistance to unlawful
State power. Then they must wait to see if those same church
elders will quietly begin to organize a resistance effort, or at
least co-operate with those who do.

This is beginning to happen today. The success of Francis
Schaeffer’s Christian Manifesto points to preliminary stirrings of
concern on the part of Christian leaders and laymen. But the
resistance movement is in its infancy. A premature call to
violence or direct confrontation with the existing religious-
political establishment would inevitably be counter-
productive. There are better ways, more bureaucratic ways,
to achieve a measure of success— ways that will also provide
the necessary traisiing for the politically unskilled Christians
of today. (The next issue of Christianity and Civilization
will explore the tactics of Christian resistance.)

 

Re-entering the Garden
Ahab desired to buy the vineyard owned by Naboth. It
