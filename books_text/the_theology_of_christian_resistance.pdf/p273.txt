AND GOD DEFEND THE RIGHT
The American Revolution and the
Limits of Christian Obedience

M. E. Bradford

N all of the noise, glamour, and patriotic enthusiasm of the

1976 beginnings of our bicentennial celebrations, most
Americans failed to recognize the extent to which there is
always a partisan political dimension to the way in which we
understand the American Revolution. For as a nation so very
much defined by its history (as opposed to blood, or religion,
or language and culture, or “great men”) our Revolution is
both prescript and precedent. If we are to concede that our
country owes its existence to an icleological explosion like the
French Revolution of 1789 or the Russian Revolution of
1918, then it could rightly be used, both here and abroad, as a
sanction for the same sort of a priori, all-or-nothing politics in
1982. Robert R. Palmer’s The Age of Democratic Revolution: A
Political History of Europe and America, 1760-1800 argues in
favor of such a confiation.t And with a slightly different, less
egalitarian, and more libertarian emphasis, Bernard Bailyn in
his The Ideological Origins of the American Revolution reasons to
the same effect and discovers in the sayings of the Fathers of
the Republic a full commitment to the doctrine of the “true
Whigs,” the teachings of Trenchard and Gordon and the
Commonwealthmen.? Even worse is Henry Steele Com-
manger’s The Empire of Reason: How Europe Imagined and Amer-
ica Realized the Enlighienment—the argument of which is well

1. Robert R. Palmer, The Age of Democratic Revolution: A Politicat History of
Europe and America, 1760-1800, 2 vols. (Princeton: Princeton University
Press, 1959, 1964).

2, Bernard Bailyn, The Ideological Origins of the American Revolution (Cam-
bridge, Mass.: Harvard University Press, 1967). John Trenchard and
Thomas Gordon brought out Cao's Letters in 1775, Professor Caroline Rob-
bins in her The Eighteenth Contury Commonwealitman (Cambridge, Mast.:
Harvard University Press, 1959) has identified the company to which they
belong,

233
