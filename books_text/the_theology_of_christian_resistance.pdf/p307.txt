THE DEBATE OVER FEDERAL SOVEREIGNTY 267

mediate. Jefferson wrote a protest for consideration by the
Virginia legislature. Madison wrote another, and Madison’s
version was approved. Jefferson then anonymously wrote
another draft, and his friend John Breckenridge submitted it
to the Kentucky legislature, which passed it. Predictably, the
Federalist Party rejected the case for states’ rights—limited
power by the central government— and asserted the case for
Federal sovereignty. New Hampshire and “libertarian”
Rhode Island argued that the Federal judiciary has the final
say in matters of Federal power. The Federalists, of course,
had controlled the appointments to Federal judgeships over
the previous decade.

Kentucky replied in February of 1799. Madison wrote a
lengthy, scholarly defense of the Virginia position in 1800.
The defeat of the Federalists in the Presidential election of
1800 sealed the doom of the Party at the national level, but
their power was extended for a generation by Supreme Court
Justice John Marshall, who created the doctrine of judicial
supremacy by declaring unconstitutional and void a section of
the Judiciary Act of 1799, and more famously, in Marbury v.
Madison (1803). Marshall was thus the intellectual father of
the doctrine of the final sovereignty of the Supreme Court.

These documents are reprinted as examples of early
American constitutional philosophy: a balance between the
one and the many, central power and local power. The battle
against the expansion of centralized political power has been
going on from the earliest days of the Constitution. The
Virginia and Kentucky Resolutions appear in volume IV of.
Jonathan Elliot’s Debates on the Federal Constitution
(Philadelphia: J. B. Lippincott, [1836] 1907), pp. 528-32,
540-45,

The Sedition Act (July 14, 1798)

Sec. 1, Be it enacted... . That if any persons shall unlaw-
fully combine or conspire together, with intent to oppose any
measure or measures of the government of the United States,
which are or shall be directed by proper authority, or to im-
pede the operation of any law of the United States, or to in-

4. Alfred H. Kelly and Winfred A. Harbison, The American Constitution:
dts Origins and Development (rev. ed.; New York: Norton, 1955), pp. 2264.
