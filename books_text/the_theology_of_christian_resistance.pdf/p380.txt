340 CHRISTIANITY AND CIVILIZATION

as merely “there to be won for Christ” without the corresponding
truth that non-Christians are by the nature of the case proud and
militant possessors of an antithetical faith (Rom. 1:18-23). A
Biblical view of cultural antithesis demands much more isolation for
training purposes than Van Alstine is comfortable with.

A second major thesis surfaces in the form of a tension. More ac-
curately, Van Alstine argues simultaneously for two contradictory
positions. He appeals on the one hand for the potentially strong
influence Christians can have in public schools (pp. 19, 103ff.), while
at the same time he endorses those teachers who purposely subor-
dinate their influence in the interests either of the Golden Rule or of
their commitment to pluralism in society and thus in education. It
may not immediately be apparent that these positions are contradic-
tory, but problems with this position come clearly into focus when
Van Alstine’s appeal is given its face value. He is telling Christians
to mute their own trumpets. He is advocating self-imposed limits to
witness-bearing. I am not aware of Scriptural support for muted
trumpets. Is it not more Biblical to question the validity of service in
an institution which says “you can speak for God but only AFTER
you have endorsed the boundaries set by Caesar”? Perhaps I should
Jet Van-Alstine put this in his own words: “Therefore it is our Chris-
Han duty to make the public schools dss (italics his) Christian” (p.
53). Evangelicals need not be opposed to sending missionaries into
situations where there are governmentally imposed limits to their
service. But to argue for the perpetuation of a structure which im-
poses limits on Christian witness is to argue for the continuation of
conditions which should be identified as deplorable and in need of
change!

It thus goes without saying that a third major thesis is
wholehearted allegiance to a supposed educational neutrality.
Believers who deny that education can be neutral have about run
out af creative ways to make the point. It must be said again, how-
ever, that education, as long as it involves people and ideas, is in-
evitably religious. To suppose that the issue under discussion is an
issue of “salt” versus “monasticism” is to show a total and inex-
cusable ignorance of the real issue. The real issue is consistent
Christian training versus alien religious training.

Additional Issues

Tn addition to the three major lines of thought, Van Alstine’s work
includes discussion of numerous educational issues. Let me offer
