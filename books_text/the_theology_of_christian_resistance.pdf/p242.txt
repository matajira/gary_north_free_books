202 CHRISTIANITY AND CIVILIZATION

It is therefore quite clear that although Calvin distinguished
the two realms, he did not separate them into upper and lower
realms with some form of a chain of being inherent in his sys-
tem. In Calvin’s view, these two realms join and interpenetrate
one another rather than standing in opposition to one another,
Even though it is impossible to reduce the political and spiritual
worlds to one another, it is equally impossible to separate them,
For Calvin, any real alienation between the two would have
spelled disaster. Although part of the reason for his insistence
may be vestiges of a spirit-matter dualism, Calvin's self
conscious reason for such a distinction must be found elsewhere.

Looking back at the quotations above, and taking into
consideration the fact that in the 1536 edition of the Institutes,
Calvin, in one section, covered Christian freedom, ecclesiasti-
cal power, and civil government, one can see that Calvin uses
the term “spiritual kingdom” as synonymous with the
“kingdom of Christ.” It is likely, then, that the distinction be-
tween spiritual and political kingdom has something to do
with the distinctive character of the kingdom of Christ.

In Calvin's development of the three offices of Christ, he is
careful to maintain that Christ exercises His offices pro
nobis—for us. Specifically, the prophetic office is not universal,
but primarily for the Church, His body, so that the power of
the Spirit may be exercised through the Church’s preaching of
the gospel.?3 The same is true for the priestly office of Christ.
This office has the primary purpose of making atonement for
the sins of the world and to acquire eternal life for men.’
Christ is high priest and sacrifice at the same time and fulfills
His function specifically pro nobis—for us who are the re-
deemed sinners united to His body. Therefore, when we come
to the kingly office of Christ, this pattern is not suddenly
altered. For Calvin, Christ’s kingship is just as much
soteriologically qualified. He distinguishes between the
kingdom of the Father and the dominion of Christ,7? and
makes clear that the kingdom of Christ is only stable and
abiding because it is not of this world and not carnal.’® The
foundation of the Kingship of Christ is that He has conquered
death and redeemed His people. God has installed him as

73, Instatutes, UL, xv, 2.

74, Commentary on Acts 26:22.
75. Institutes, IE, xv, 5.

76. Institutes, TI, xv, 3, 5.
