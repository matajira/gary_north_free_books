306 CHRISTIANITY AND CIVILIZATION

(10) Interposition by a State and a County:

The border State of Missouri was divided in sympathy.
She, like her sister border State of Kentucky, sought to re-
main neutral in the impending war. Both states offered
guarantees of peace and order within their respective ter-
ritories if left free to control their own affairs as sovereign
political States, But since neither state agreed to supply troops
for invading the South, they came under suspicion and were
subsequently invaded by Union troops.

In the spring of 1861, Union troops suddenly appeared
and surrounded a peaceful encampment of Missouri State
Guard at Gamp Jackson near St. Louis. Captain Nathaniel
Lyon was in command of the Union troops in the absence of
General W. S. Harney. Sadly, after the Missouri State Guard
handed over their arms, the Union troops opened fire, killing
some of the State militia and also killing 10 and wounding 20
women and children who were bystanders. Upon his return to
the Union command, General Harney negotiated a treaty
with General Sterling Price of the Missouri State Guard (who
was known up to that time as a pro-Union man). The treaty,
signed on May 21, 1861, guaranteed the territorial integrity
of Missouri as a neutral State. In part, the treaty read:

General Price, having by commission full authority over the mili-
tia of the State of Missouri, undertakes with the sanction of the
Governor of the State, already declared, to direct the whole
power of the State officers to maintaining order within the State
arnong the people thereof. General Harney declares that, this ob-
ject being assured, he can have no occasion, as he has no wish, to
make military movements that might otherwise create excite-
ment and jealousy, which he most earnestly desires to avoid.*!

Nevertheless, the military invasion of Missouri did not
stop, so General Price led the poorly equipped Missouri State
Guard in a series of desperate (and successful) efforts to eject
the invaders from the state. But though the Guard, through
superior leadership and dedication of the poorly equipped
men, won every battle, the sheer number of 70,000 invading
Union troops forced General Price to lead his men, who never
numbered more than 3,500, out of the state southward, where
they joined the Confederate troops.

This unsuccessful military interposition by State officers

11. Dbid, p. 417.
