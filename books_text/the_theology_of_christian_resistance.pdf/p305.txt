THE DUTCH-AMERICGAN GUERRILLAS 265

to be learned about the history of this period. Even such a
perceptive historian as Shy, for example, repeats the myth
about John Adams’ statement that it was a minority Revolu-
tion. What is apparent from some of the very careful local
studies such as Leiby’s, drawn from a variety of obscure, and
long-forgotten records, is that perhaps the American Revolu-
tion has more than “a few lessons for our own time.”
