REVIEWS 341

comments on five of these other issues.

First, to his credit he faces the problem of what the “unifying vis-
ion” of public education should be, since he is quick to point out that
the focus of public education cannot be “Christ, God or God’s law”
(p. 82). He answers: “Public education in America should not (italics
his) try to have a single unifying vision of greatness. Rather, schools
and educators should acknowledge that such an ideal is necessary for
each individual, and then offer help and encouragement to children
and families trying to articulate it for themselves” (p. 83). Such an
asscrtion is fine . . . but only ifone is an existentialist rather than a
Christian. It appears to have been overlooked that the products of
such an educational approach will be, in general, confused, yet ai
the same time will see one thing very clearly. The clearly-seen con-
cept is a view of God in which He is perhaps important for me and
my family, but He obviously is not important enough to make
demands on those who have a prior commitment to an “alternate
belief structure.” The subsequent step in this digression of thought is
that whoever God is, He must not he anywhere near as important as
my parents may have said He is. This is because belief in Him is not
so vital that we are obligated to disturb those who persist in their
unbelief. What is really tragic is that Van Alstine leaves us with the
distinct impression that we are to help unbelievers “articulate” their
rebellious views “for themselves.”

Second, the issue of the “messianic character’ of American
education is raised. Van Alstine attempts to disclaim a messianic at-
titude, stating “some idealistic promoters of public schools have be-
lieved that education is potentially the patent medicine cure-all for
society’s ills. History has taught us that this is not so” (p. 90). Yet
from these surne lips we hear an appeal for “socializing [after eleven
years in the study and practice of education I am still not sure what
that term means—L.O.] young people for life in a democracy” (p.
39). He speaks favorably when discussing the proponents of the
earlier Cormmmon School Movement who “saw public schools as a
means of control, as a way of civilizing, socializing and assimilating
the children of these fringe people into the American way of life” (p.
39). My impression is that while Van Alstine desires to have a sen-
sitivity to the negative aspects of liberal and utopian thought, he is
nonetheless caught in the grasp of a blind vision of the “potential for
good” supposedly existing in our “no-commitment” statist schools.
In reality, an undiscerning focus on “potential for good” is a mere
restatement of the utopian views of the former era.

A third issue faced by Van Alstine is the lack of quality in public
