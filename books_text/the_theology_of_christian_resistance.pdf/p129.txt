PACIFISM AND THE OLD TESTAMENT 89

There is no problem with this, obviously, except that Lind
overstates his case on the first page of his text (numbered p.
23): “This conviction was so emphatic that Israel’s fighting,
while at times a sequel to the act of Yahweh, was regarded as
ineffective; faith meant that Israel should rely upon Yahweh’s
miracle for her defense, rather than upon soldiers and
weapons.” This implies an opposition between God's activities
and those of men, which does not square with the Biblical
presentation of the relationship between the two.® God's
actions are the foundation of human action, not a negation of
it. The notion that God’s actions and those of men cannot be
dependently parallel assumes that God and man exist on the
game continuum. Thus, to the extent that God predestinates,
man is a mere puppet, and to the extent that man acts, God
does not act. On the contrary, however; the doctrine of crea-
tion teaches us that God predestinates all things, and acts in
all things, yet the actions of men are genuine free moral
choices, and indeed only have meaning against the back-
ground of God's sovercignty.

Now, in the case of holy war, this simply means that the
victory is always ultimately ascribed to God, whether any
human actions take place or not. As pointed out above, David
was to follow God’s army into battle, fighting on the ground
while God fought in the treetops (2 Sam. 5:23f.). The actions
are parallel, yet man is wholly dependent on God for the
outcome.

A second prablem enters in here, which Lind does not
discuss at all. That is, that there is a distinction between what
we might call “holy war” and “just war,” the former being
salvific and absolute in character (liquidating the Canaanites,
destroying the Egyptians), the latter being defensive and
limited in character (as the laws of Deuteronomy 20:10-20).

6. An example of theological messiness is on p. 38, “If one truly believes
in the promise of grace that God will give the land, then one has no need to
take the way of works by fighting for it. The logic of Paul’s doctrine of
grace—calling for response of faith rather than works~is here
prefigured. .. .” On the contrary, grace calls for both faith and faithful
works; it only negates faithless works of the law, done in pride. Moreover, if
God was giving the land in the sense Lind means, why did He tell Israel co
fight also? Jesus said, giving us insight into this, “the gospel of the kingdom
of God is preached, and every onc is forcing his way into it” (Luke 16:16).
Christ’s saving actions on our behalf do not negate our need to strive.
