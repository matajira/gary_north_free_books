58 GHRISTIANITY AND CIVILIZATION

unwillingness to sve the incomparable confrontation coming,
Boyajian’s comment concerning the Armenians in 1890 applies
even more forcefully to the period immediately preceding the
genocide of 1915-16: “One wonders in fact why the Armenians
exhibited so few signs of disaffection in the light of the harass-
ment they suffered and from which they were never free.”®

The Turks began to round up the leaders of Armenia in
April of 1915. This cut off any verbal protests. Hundreds of
thousands of them were deported from three regions, and left
fo starve in concentration camps.? No one knows for certain
just how many Armenians died, but it was at least 800,000,
and may have been as many as two million.!© An American
eye-witness, cited in Johannes Lepsius’ book, Deutchland und
Armenien (1919), and reprinted by Boyajian (pp. 118-20),
reported on what he had seen. Here was Armenia's price of
perpetual patience:

 

It is impossible to render an image of the horrible impressions |
received'on my journey through the dispersed camps along the
Euphrates river, I travelled on the right-hand bank of the stream,
To speak of ‘camps’ is actually not possible.

The major portion of these miserable people brutally driven
from home and land, separated from their families, robbed of every-
thing they owned and stripped of all they carried underway, have
been herded like cattle under the open skies without the least protec-
tion against heat and cold, almost without clothing, and were fed
very irregularly, and always insufficiently. Exposed to every change
in weather, the glowing sun in the desert, the wind and rain in
spring and fall, and the bitter cold in winter, weakened through ex-
treme want and their strength sapped by endless marches,
deplorable treatment, cruel torture and the constant fear for their
lives, those that had some shreds of their strength left dug holes at
the banks of the river to crawl into them.

 

Kazan’s 1963 account of a Greek youth in the late 1890's who sees what the
Turks do to his Armenian friend’s church and community, and who decides to
flee to Amer no matter what the costs. He refuses to live under Turkish
rule any longer, knowing that he can never be a free man. His family and
fiends cannot understand his motivation. He leaves behind the opportunity
to marry into a successful family. The film ends with the youth in New York,
working as a shoeshine buy, happy to be right where he is. I have never seen
the film on television, although I have watched for it for almost two decades. It
is perhaps the finest Hollywood-produced account of what it meant for irn-
migrants of ihe last century to escape to the United States.

8. idem.

9. For an account of the horrors, frorh one who went through them from
1895 on, see Abraham H. Hartunign, Neither to Laugh nor to Weep (Boston:
Beacon, 1968),

10. Boyajian, Armmia, p. 1,

 

 

 
    
