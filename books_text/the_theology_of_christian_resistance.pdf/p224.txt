184 CHRISTIANITY AND CIVILIZATION

that the gospel must be applied to the everyday concerns of
political life. On the other hand, they rejected the medieval
idea of ecclesiastical and political union in order to fracture
the Holy Roman Empire. Their situation was a difficult one.
Not only did they have to contend with the Anabaptists, but
they also had to fight the might and power of the Roman
Catholic Church and her political allies. The Reformers,
therefore, had to establish their churches against both ecclesi-
astical and political opposition.

The dilemma that this situation created is obvious. On the
one hand, God requires his people to give supreme allegiance
to Him even to the point of hating father and mother (Luke
14:26), while on the other hand, a Christian must also take
seriously Paul’s admonition to be “in subjection to the govern-
ing authorities” (Rom. 13:1f.).

Each of the different “arms” of the Reformation answered
this dilemma in different ways. The Lutheran Reformation
ultimately solved the problem via a compromise that is
summed up in the formula cuius regio, eius religio, which means
“he who reigns, his religion.” In other words, the local prince
decided what the religion of that locale would be. As
Christoph Jungen notes, “One of the consequences of this
compromise-solution was that after the Peace of Augsburg
(1555), the Lutheran Reformation stagnated and advanced
only insignificantly beyond those areas in which it had already
been established by that day.””

The Zwinglian Reformation was not very different,
although in many respects more militant. The various loci of
the Swiss Reformation usually began with popular resistance
to the corrupt Roman Catholic Church, but it almost in-
variably ended by imposing the religion of the Reformation
upon the cities and cantons by the civil magistracy. Where the
magistracy was not converted, the Reformation made little
inroads.

The Calvinistic arm of the Reformation was quite differ-
ent. “Not only did Calvinism succeed in gaining a large fol-
lowing in France,” writes Jungen, “where opposition and
persecution were most severe, but it also succeeded in break-

7. Christoph Jungen, “Calvin and the Origin of Political Resistance
Theory in the Calvinist Tradition” (Unpublished Th.M. Thesis,
Westminster Theological Seminary, 1980), p. 3.
