110 GHRISTIANITY AND CIVILIZATION

and political thought and practice that these Biblical prin-
ciples and God-given duties have been obscured or denied. It
is a powerful testimony to the strength and endurance of
Christian influence in American legal and political thought
that what Europe and England experienced in the sixteenth
century we did not experience (as a powerful intellectual and
social force) until the late nineteenth century.

Christian Natural Law and Its Problems

Christian theories of Natural Law have frequently been
found in close association with Christian Biblical Law
thought. After ail; Christian theories of Natural Law presup-
pose God as the source of law and the fundamental locus of
authority for law, since they presuppose that God has created
into His universe universal meaning and ethical-legal prin-
ciples revealed to man in God’s created and sustained natural
world, and that God has created man in His image, as a ra-
tional creature who can therefore rationally know these God-
created ethical and legal principles—especially when he
reasons on the basis of the prompitings of his conscience.”
Christian thinkers such as Calvin and the American Puritans,
among others, have maintained that since there is no con-
tradiction between God’s law revealed in His natural creation
and God’s law revealed in the Bible, one can obtain the
clearest understanding of “Natural Law” by reading God’s
law in Scripture. Thus, Calvin preached extensively on Deu-
teronomy, maintaining the abiding validity of God’s revealed
laws, in detail, for today** and stil] believed in Natural Law;
and the Puritans, while theonomically attempting to apply
God's case law to their civil government,* also early affirmed
belief in Natural Law, a belief which lay at the foundation of
the War for Independence.*¢

The characteristics of law and the role of civil government

44, Calvin's sermons on Deuteronomy, not reprinted in English since
1583, are available today, in newsletter form, from the Geneva Divinity
School (708 Hamvassy, Tyler, Texas 75701); title: Calvin Speaks.

45. See The Jounal of Christian Reconstruction; Symposium on Puritanism and
Law V:2 (Winter, 1978-1979).

46. See Baldwin, op. cit., and The Journal of Christian Reconstruction: Sym-
posium on the American Revolution III: 1 (Summer, 1976), especially pp. 19-51,
59-62, 69-73, 103-112.

 
