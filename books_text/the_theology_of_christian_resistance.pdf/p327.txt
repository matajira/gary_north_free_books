ON RECONSTRUCTION 287

Here we see a clear biblical pattern for a federal structure of
government rather than a centralized one. It is obvious when
we consider that the Israelites, even at this time, numbered
from 1.5 to 2.5 million, that Moses could not have personally
selected the rulers of thousands, fifties, and tens. Rather, he
simply confirmed and formalized the aleady-existing hierarchy of
the local leadership of tribes and clans that the people them-
selves had set up. In short, Moses’ action simply endorsed for-
mally the two governmental concepts of /ocal self-rule and
Jederalization, Here we also see the concept of higher law — that
the civil authority is simply a distributor of God's law rather
than a source of law in and of itself, for the local rulers were to
be men “such as fear God.”

Our colonial forefathers were astute students of the Bible,
for they used God's holy word as their guide and rule in
establishing every aspect of society; especially civil govern-
ment, for they had suffered under the perversion of biblical
teaching called “the divine right of kings.”

The concept of local self-government is solidly New Testa-
ment also, for Paul advised Titus to ordain elders in every city
(Titus 1:5), and he advised Timothy that the elders who rule
well should be counted worthy of double honor {I Tim. 5:17).

The biblical flow-of-power concept that our colonial forefa-
thers set up, in both their churches and their civil govern-
ments, is this: power flows directly from God to the individual, who
in turn voluntarily compacts with other God-responsible in-
dividuals at the local level in establishing both church and
civil polity. In the church authority is vested in a local body
(the elders), who are ordained of God but elected by the people.
Thus, self-government arises from following biblical precepts.

The concept of federalism is applied in a practical way
when local churches voluntarily band together in mutual
Christ-centered fellowship to establish higher courts: Ist, local
churches establish presbyteries, then presbyteries group into
synods, and synods combine to form general assemblies.
While the higher church courts rule on matters referred to
them, according to the Reformed understanding of biblical
federalism, the flow-of-power is not from God to the higher
courts, to the local church elders, and then to the people.
(This would be a form of centralism.) But, rather, the power
at higher court levels is delegated from the local body of elders.
Thus, the biblical flow of governmental power is from God to
