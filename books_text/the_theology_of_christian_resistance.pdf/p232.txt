192 CHRISTIANITY AND GIVILIZATION

greatest gift that God has given us in this transitory life.2#

Calvin held out a very high view of the magistrate and his
offices to his readers and hearers, and this served as a warning
and reminder to those who wished to give little respect to the
magistrates; but he did not support any of the absolutistic no-
tions common in his day. “For Calvin the fact that monarchs
and other magistrates hold office by a sovereign act of God
does not so much imply that their rule is absolute and in-
violable, but rather it is an expression of the ultimate author-
ity of God over them that implies heightened duties and
responsibilities to him.”29

The Magistrate

For Calvin, each man had a distinct calling before Gad
and accompanying duties, whether he realized them or not.
Some were endowed by the Spirit of God for the difficult task
of being magistrates. Others were subjects whose primary
duty it was to obey the laws and the magistrates who enforced
them. For Calvin, the scriptual injunction concerning sub-
mission to the will of one’s parents and the responsibility of
their calling as parents had similar applications to politics. He
writes, “In the fifth commandment are comprised by
synechdoche all superiors in authority... . The name of God
is, figuratively indeed, ... applied to magistrates, upon
whom, as the ministers of his authority, he has inscribed a
mark for his glory... . His owm dignity is claimed for the
judges, in order that the people may reverence them, because
they are God’s representatives, as His lieutenants, and vicars.

“Tt is a signal exaltation of magistrates, that God should
not only count them in the place of parents, but present them
to us dignified by His own name.”#0

If it were not enough that God appointed magistrates as
His representatives, and invested them with His dignity, in
Calvin's view, He actually makes them the external medium
of salvation in the broad sense of the term. Indeed, as he
wrote in the Institutes, their function is not less than that of
“bread, water, sun, and air.”3! They are one of the ordinary

28. Commentary on Romans 13:1f
29, Jungen, p. 21. Cf. Commentary on Psalm 110:1.
30. Commentary on Exodus 22:28.

31. Instituies, TV, xx, 3.
