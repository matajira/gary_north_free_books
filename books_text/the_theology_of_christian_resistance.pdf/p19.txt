EDITOR’S INTRODUCTION xix

The visible leadership of today’s Protestant resistance
movements has not yet acquired the political skills, or the com-
mon sense produced by years of experience, to identify either
the high-priority programs or the most likely means of achiev-
ing success in the political struggles. Some of them are aware of
their lack of experience, and they have brought in men like
Paul Weyrich, a non-Protestant, to give them “nuts and bolts”
advice concerning the technical details of winning elections or
getting legislation stopped (though seldom passed) in Con-
gress.!6 These Protestant leaders also lack a comprehensive
theology of dominion, which should undergird any program of
Christian resistance.!? At best, today’s tentative calls for civil
disobedience by Christians should be regarded as the begin-
ning of a new political awareness on the part of a handful of
Christian leaders and their tiny bands of unskilled followers.'®

Lots of people taik tough when the price of commitment is low,
Talk is cheap. Action is more costly. But most expensive of all
is long-term planning. Those who commit their futures to
movements led by short-term thinkers who have vast projects
and half-vast ideas are asking for trouble. The biblical pattern
of victory is clear: “But the word of the Lord was unto them
precept upon precept, precept upon precept; line upon line,
line upon line; here a little, and there a little; that they might
go, and fall backward, and be broken, and snared, and taken”
(Isa. 28:13). Step by step, we must build alternatives to a col-
Japsing social] order, starting with our families and local
churches, and expanding outward. Like the church elders and
deacons who are first required to rule their households (families
and businesses) well before they rule the church (I Tim. 3), so
are those who would launch a program of comprehensive
Christian resistance. Victory takes practice. First things first.

16. Mr. Weyrich is connected with two organizations, the Gomsnittee for
the Survival of a Free Congress, and the Free Congress Foundation. He
operates seminars in the fundamentals of getting elected to political office.
Address: CSFC or FCF, 721 Second St., S.E., Washington, D.C. 20002.

17. Gary North, “Comprehensive Redemption: A Theology for Social
Action,” The Journal of Christian Reconstruction, VIII (Summer, 1981).

18. It is indicative of just how ideologically weak the humanist liberals
are today that they see in the “New Christian Right” a major, immediate
threat to their control of American thought and culture. See, for example,
the book, Holy Terror (Garden City, New York: Doubleday, 1982). A similar
book is Gad’s Bullies,
