32 GHRISTIANITY AND CIVILIZATION

issued at an earlier date than the tax years under investiga-
tion, the L.R.S. should be able to determine without difficulty
whether the charter had been revoked for any reason or was
still in force during the pertinent period.”

Here, Dumbauld as much as says that a church must in-
corporate to be genuine. Is this true? No, it is false; it is ab-
surd! A church may incorporate if it likes; the Church of
Christian Liberty is incorporated. But there is no law that says
it must. For the present purposes, the best proof of this comes
from the Internal Revenue Service! Remember the July, 1978
letters with which I.R.S. began this absurdity? In those mail-
ings, the Internal Revenue Service told the Church this: “If
you are incorporated, please furnish a copy of your Articles of
Incorporation and any amendments.” So, IR.S. knows
perfectly well that the Church may not be incorporated. But,
Dumbauld expects us to believe that he doesn’t.

On this page, too, there is a footnote. It says in part: “It
must be remembered that tax exemption is a privileged status
and that the taxpayer claiming it has the burden of demon-
strating entitlement thereto. . . .” Bul, as we have seen, this
does not apply to churches, incorporated or not, A church does
not have to apply for exemption. Its tax-exemption is
automatic. It is not a privilege, but a right. The Internal
Revenue Service says so itself, in Section 508 of the Code and
elsewhere. Section 508 is just a short distance from 501(c)(3).
Dumbauld expects us to believe that he is aware of one but
not the other. Again he is telling us that the government will
certify the church.

A word should be said about why a church is automatically
tax-exempt. Why does a church have this unique status in our
country? The answer lies in a word the legal system uses every
day: jurisdiction. If entity A can tax entity B, entity A has
jurisdiction over it, is superior to it. Where the government
can tax the church, there is a government-ruled church.
Where there is a government-ruled church, there is a
government-approved church, as in Communist-occupied
Russia; “an establishment of religion,” in the words of the
U.S. Constitution,

On the next page, Dumbauld drops a blockbuster:
“Typical activities of an organization operated for religious
purposes would include (a) corporate worship services, in-
cluding due administration of sacraments and observance of
