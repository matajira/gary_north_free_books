REVIEWS 347

spreading of socialist ideas even though it was ostensibly and
allegedly designed to contain communism. The idea which has the
world in its grip has been spread “both by international communisra
and from counury to country as socialism, liberalism, the welfare
state, social dernocracy, or whatever.” Whereas communism is
spread by the creation, penetration, and infiltration of organiza-
tions, and by the spread of terror, culminating in the establishment
of a state of terror, gradualism spreads by continually promising
more and more benefits and by ever extending the sway of govern-
ment,

Gradualists believe that so long as more and more decisions are
being collectively made, progress is being made. Pressure for collec-
tivization, frequently advanced in terms of welfare, government
planning, and disiributionist schemes marked as alleged antidotes to
communism and/or as substantive requirements of demacracy, has
been spread within an intellectual atmosphere nurtured by Western
intelligensia and propagated as intellectual fashion, American
foreign aid has been a primary strategic device for advancing social-
ism throughout the world. Carson observes in his chapter “Coex-
istence, Detemte, and Convergence” that the “world conflicts of so-
cialism are but a reflex on a grand scale of the determination imbed-
ded in the idea to crush all independence.”

Part VIII, “The Individual,” has chapters titled “Victim of the
Idea,” “The Subjugation of the Individual,” “The Restoration of the
Individual,” and “Establishing Individual Responsibility.” Urgent
primary tasks for our time set forth by Garson are (1) holding of
government functionaries to Constitutionally intended limitations
on their authority; and (2) a removal of the constraints by which in-
dividuals qua individuals are controlled.

Dr, Carson, in my judgement, has provided one of the most im-
portant works of its nature of this era. It is historic, factual, descrip-
tive, explanative. Jt is high viscosity material, but interpretive
strains are finely delineated. It is very important material for per-

 

to ensure the loans by which he seeks to build socialism and thereby benefit
the poor. In actuality, capitalists have built the Soviet Union as a formidable
industrial entity. Monopoly capital may be as opposed to individual free-
dom and free enterprise as the most avowed revolutionary. Those who ag-
gregate large sums of capital are not necessarily advocates of free enterprise.
They may seek to utilize government in the interest of monopoly capital.
Socialism, rather than a system in which distributive “justice” is obtained, is
asystem whereby the elite are more readily enabled to make servants of the
populace through the machinery of government.
