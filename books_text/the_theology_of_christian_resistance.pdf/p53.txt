CHRISTIAN RESISTANCE 13

Mises, Ludwig von. Omnipotent Government: The Rise of the State
and Total War (1944). New Rochelle, NY: Arlington
House, 1969.

North, Gary. Unconditional Surrender: God’s Program for Victory.
Tyler, TX: Geneva Divinity School Press, 1981.

Rushdvony, Rousas John. The Institutes of Biblical Law.
Nutley, NJ: Craig Press, 1973.

. This Independent Republic. Fairfax, VA: Thoburn
Press, 1978.

Rutherford, Samuel. Lex Rex; or, the Law and the Prince (1644).
Available from Sprinkle Publications, Box 1094, Harrison-
burg, VA 22801.

Schaeffer, Francis. How Should We Then Live? Old Tappan,
NJ: Revell, 1976.

a ___, A Christian Manifesto, Westchester, IL: Cross-
way Books, 1981.

. Joshua and the Flow of Biblical History. Downers
Grove, DL: InterVarsity Press, 1975.

Whipple, Leon. The Story of Civil Liberty in the United. Siates.

Westport, CT; Greenwood Press, 1927,

 
