THE DEBATE OVER FEDERAL SOVEREIGNTY 273

preamble would admit a construction which would erect the
will of Congress into a power paramount in all cases, and
therefore limited in none. On the contrary, it is evident that
the objects for which the Constitution was formed were deemed
attainable only by a particular enumeration and specification
of each power granted to the federal government; reserving all
others to the people, or to the states. And yet it is in vain we
search for any specified power embracing the right of legisla-
tion against the freedom of the press.

Had the states been despoiled of their sovereignty by the
generality of the preamble, and had the federal government
been endowed with whatever they should judge to be in-
strumental towards the union, justice, tranquility, common
defence, general welfare, and the preservation of liberty,
nothing could have been more frivolous than an enumeration
of powers.

All the preceding arguments, arising from a deficiency of
constitutional power in Congress, apply to the Alien Act; and
this act is liable to other objections peculiar to itself. Ifa suspi-
cion that aliens are dangerous, constitutes the justification of
that power exercised over them by Congress, then a similar
suspicion will justify the exercise of a similar power over
natives; because there is nothing in the Constitution
distinguishing between the power of a state to permit the
residence of natives and aliens. It is, therefore, .a right origi-
nally possessed, and never surrendered, by the respective
states, and which is rendered dear and valuable to Virginia,
because it is assailed through the bosom of the Constitution,
and because her peculiar situation renders the easy admission
of artisans and laborers an interest of vast importance.

But this bill contains other features, still more alarming
and dangerous. It dispenses with the trial by jury; it violates
the judicial system; it confounds legislative, executive, and
judicial powers; it punishes without trial; and it bestows upon
the President despotic power over a numerous class of men.
Are such measures consistent with our constitutional prin-
ciples? And will an accumulation of power so extensive in the
hands of the executive, over aliens, secure to natives the bless-
ings of republican liberty?

If measures can mould governments, and if an uncontrolled
power of construction is surrendered to those who administer
them, their progress may be easily foreseen, and their end
