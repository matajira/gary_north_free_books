JOHN GALVIN’S THEOLOGY OF RESISTANCE 181

to avenge the Admiral’s attempted assassination, and that it
was necessary to strike before being struck.

Gondi was given the assignment of relating the aston-
ishing news to the King that it was his mother, along with his
brothers and the Guises,? who had planned the attempt on the
Admiral’s life. While Gondi was explaining to the increasingly
angered King, Catherine, along with Anjou and the rest of
her group, entered the court room. She began by lying to
Charles, telling him that the Admiral had sent dispatches to
Germany and Switzerland in an effort to raise arms against
the King, and then pressed the point that civil war was immi-
nent. With the finesse of the good Machiavellian that she was,
Catherine persuaded the King: “You must know that all the
Catholics, tired of seemingly endless troubles and dreading
new calamities, are resolved to be done with them: if the King
does not take their advice—that is, strike first by killing the
chief Huguenot leaders tonight—they will be determined to
elect a captain-general and organize a league under his pro-
tection. Thus you will be left alone, exposed to the worst
dangers, with neither power nor authority; you will see
France separated into two great parties, over which you will
have no command and from which you will obtain no obe-
dience!”?

The implied threat made by Catherine was, of course,
that Anjou would be made “captain-general” and take the
place of his brother. This thrust at the sorest point of this weak
king, his envy of his brother, was enough to cause Charles IX
to agree to the offensive murders. He stormed around the
room swearing in fury and anger. If his noble friends and farn-
ily felt it advisable for the Admiral to be killed, then he too
wanted it. But he also wanted the death of all the Huguenots
in France so that none would remain to reproach him later.
He gave the orders for the plan of his mother to go into effect
immediately. He then left the room, leaving his mother and
her advisors to give such specific orders as were necessary for
the execution of such a diabolical enterprise.

During the next 24 hours, the streets of Paris ran with the

2. The noble house of Guise controlled the Lorraine in northeastern
France. They were fiercely loyal to the Roman Church, and were probably
agents of Philip II, King of Spain, the most powerful monarch in the
Vatican stable at the time.

3. Quoted in Gray, p. 136.
