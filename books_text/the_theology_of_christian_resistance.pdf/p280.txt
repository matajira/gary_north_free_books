240 CHRISTIANITY AND CIVILIZATION

Henry Drayton of South Carolina, contended that the British
government in 1774-1775 was like all other legitimate, non-
tyrannical governments, in that once the pattern of “protec-
tion and subjection,” linking prince and freeman, collapsed,
then no true liberty could exist until it was replaced.1? The
popular magistrates, whose offices had the same constitu-
tional sanction as the Crown, where the keys to what could be
attempted. No abrogation of the Charters, no return to Anar-
chy, no starting up from “scratch”; on these grounds neither
internal revolution, nor selective resistance to positive law,
nor the making of unreasonable, non-negotiable demands
could be elevated into a positive principle — one to be invoked
at the least provocation.

Thus I would suggest an analogy between the moral free-
dom of the Christian who has, in the formula of St. Paul, ac-
cepted the Christian rule for life and the liberty for which the
Fathers of this Republic were willing to risk their “lives, -
[their] Fortunes, and [their] sacred honor.” Both predicate the
reality of freedom within an accepted bond—within a political
structure, sheltering a social structure. Belonging in this
equation paradoxically makes men free. The doctrines of nat-
ural rights, we must remember, are heathen in their implicit
primitivism. A “state of nature” is a desperate, barbarous
condition. The American Revolution was not the venture of
men bemused with an optimism concerning human nature.!3
It was not the handiwork of sentimental democrats, nor the
Constitution after it. But to live under tyranny is not to enjoy
a social and political estate—a civil condition for family,
private property, legitimate voluntary association, and social
classes which are able to maintain themselves free of unces-
sant political invasion or domination: a world where citizens
yote on some questions, and the law within its sphere is the
same for all who come before it.'* To that felicity colonial

12. My commentary on Drayton appears on pp. 111-133 of A Batier Guide
Than Reason. paraphrase Drayton throughout this essay. The specific work
quoted here is “Charges to the Grand Jury, of general session held at
Charlestown 1776 and 1777, commending the constitution as established by
Congress March 26, 1776; the rise of American Empire and other
topics...” It is reprinted in Vol. II of John Drayton’s Memoirs of the
American Revolution (Charleston: A, E. Miller, 1821).

13. Vox Populi, Vox Dei was not their rootto.

14. I quote here from Robert Nisbit, Ths Social Philosophers: Community and
