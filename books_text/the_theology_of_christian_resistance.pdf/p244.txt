204 CHRISTIANITY AND CIVILIZATION

The “earthly,” “temporal,” or “political” kingdom, then,
includes all that which does not directly pertain to this rule of
Christ, but has reference to all the things that “have their sig-
nificance and relationship with regard to the present life and
are, ina sense, confined within its bounds.”#+ Calvin does not
deny that the Kingdom of Christ is in this world, but “strictly
speaking, although it dwells within us, it is a stranger to the
world, since its state is completely different.”®5 The kingdom
of Christ is firm and stable and will never be overthrown or
shaken, but the same cannot be said of earthly kingdoms. The
reason that this is so, according to Calvin, is precisely due to
the fact that the Kingdom of Heaven is not earthly.26

But just exactly what does all of this have to do with civil
obedience or disobedience? From Calvin’s viewpoint, a great
deal: “Through this distinction it comes about that we are not
to misapply to the political order the gospel teaching on
spiritual freedom, as if Christians were less subject, as con-
cerns outward government, to human laws.”87

In another place, commenting upon Matthew 22:21, he is
even more specific: “A clear distinction is set out here between
spiritual and civil government, that we should know ourselves
to be under no external constraint from holding a clear cons-
cience in the sight of God. The error Christ wanted to refute is
the idea that a people cannot belong to God unless it is free of
the yoke of human rule. . . . God’s law is not violated or his
worship offended if the Jews in external government obey the
Romans... .

“Keep the distinction firm: the Lord wishes to be sole
lawgiver for the government of souls, with no rule of worship
to be sought from any other source than his word, and our
adherence to the only pure service there enjoined; yet the
power of the sword, the laws of the land and decisions of the
courts, in no way prevent the perfect service of God from
flourishing in our midst.”88

In other words, Christian freedom does not mean that
civil government has become unnecessary, or that it gives one
an excuse to rebel against the civil government. On the one

84. Institutes, IT, ii, 13.

85. Commentary on John 18:36.

86. Commentary on John 18:36, and Acts 5:34.
87, Institutes, Il, xix, 15; 1V, xx, 1.

88. Commentary on Matthew 22:21,
