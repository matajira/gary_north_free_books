THE LESSER MAGISTRATES 173

need not labour to prove that an impious king is a mark of the
Lord’s anger, since I presume no one will deny it, and that
this is no less true of a king than of a robber who phinders
your goods, an adulterer who defiles your bed, and an
assassin who aims at your life, since all such calamities are
classed by Scripture among the curses of God. But let us insist
at greater length in proving what does not so easily fall in with
the view of men, that even an individual of the worst
character, one most unworthy of all honour, if invested with
public authority, receives that illustrious divine power which
the Lord has by his word devolved on the ministers of his
justice and judgment, and that, accordingly, in so far as
public obedience is concerned, he is to be held in the same
honour and reverence as the best of kings.

And, first, I would have the reader carefully to attend to
that Divine Providence which, not without cause, is so often
set before us in Scripture, and that special act of distributing
kingdoms, and setting up as kings whomscever he pleases. In
Daniel it is said, “He changeth the times and the seasons: he
removeth kings, and setteth up kings” (Dan. ii 21, 37). Again,
“That the living may know that the Most High ruleth in the
kingdom of men, and giveth it to whomsoever he will” (Dan.
iv 17, 25). Similar sentiments occur throughout Scripture, but
they abound particularly in the prophetical books. What kind
of king Nebuchadnezzar, he who stormed Jerusalem, was, is
well known. He was an active invader and devastator of other
countries. Yet the Lord declares in Ezekiel that he had given
him the land of Egypt as his hire for the devastation which he
had committed. Daniel also said to him, “Thou, O king, arta
king of kings: for the God of heaven hath given thee a
kingdom, power, and strength, and glory. And wherescever
the children of men dwell, the beasts of the field and the fowls
of the heaven hath he given into thine hand, and hath made
thee ruler over them all” (Dan. ii 37, 38). Again, he says to his
son Belshazzar, “The most high God gave Nebuchadnezzar
thy father a kingdom, and majesty, and glory, and honour:
and for the majesty that he gave him, all people, nations, and
languages, trembled and feared before him” (Dan. v 18, 19).
When we hear that the king was appointed by God, let us, at
the same time, call to mind those heavenly edicts as to
honouring and fearing the king, and we shall have no doubt
that we are to view the most iniquitous tyrant as occupying
