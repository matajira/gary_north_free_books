Economic Oppression by Means of the State 443

have equally good information about the value of the fruits of
production. The person who wants to lease the land probably
has somewhat poorer information about the physical details of
the property On the other hand, the land owner may have
fallen into debt. Perhaps he is not a good manager of his mon-
ey. He may be a poor farmer. He may have poor information
about the value of the stream of net income from the land. So
the text does not specify one of the two parties as the more
likely oppressor.

To identify the oppressor here, we nced to identify the
person who uses the State, or his knowledge about the most
likely future actions of the State, in order to gain a compctilive
advantage over the other person in a voluntary transaction. It
is rare for biblical law to specify pricing as judicable cconomic
oppression except in life-and-death situations - what I call
“priestly pricing.” Biblically defined cconomic oppression.
through price-setting is usually based on a person's efficient use
of illegitimate power by the State. The oppressor and the civil
magistrates act in collusion to oppress someone or some group.

A Question of Knowledge

The law of the jubilee was clear: in year 50, Israel’s agricul-
tural land was to revert to the original owners or their heirs.
‘This leads me to ask: On what basis could anyone not know
what to pay for or charge for leasing the land? All land was not
equally valuable. To the extent that one piece of land was more
productive, net, than another, to that extent the lease price
would have been higher than less productive land. For exam-
ple, a farm with a well-developed orchard would have brought
a higher price than a farm whose income was dependent on
farming that required higher inputs of labor and capital. The
net income from the fruit of the orchard probably would have
exceeded the net income from grain farming. So, the existence
of variously priced annual leasehold rents was not necessarily
evidence of economic oppression by anyone.
