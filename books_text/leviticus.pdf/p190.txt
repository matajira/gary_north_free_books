7

GUARDIAN OF THE CIVIL OATH

And the Lorp spake unio Moses, saying, If a soul sin, and commit a
trespass against the Lor, and lie unto his neighbour in that which was
delivered him to keep, or in fellowship, or in a thing tahen away by
violence, or hath deceived his neighbour; Or have found that which was
lost, and lieth concerning it, and sweareth falsely; in any of all these that
aman doeth, sinning therein: Then it shall be, because he hath sinned,
and is guilty, that he shall restore that which he took violently away, or
the thing which he hath deceitfully gotten, or that which was delivered
him to keep, or the lost thing which he found, Or all that about which he
hath sworn falsely; he shall even restore it in the principal, and shall add
the fifth part more thereto, and give it unto him tv whom it appertaineth,
in the day of his trespass offering. And he shall bring his trespass offer-
img unto the Lorp, @ ram without blemish cut of the flock, with thy
estimation, for a trespass offering, unto the priest: And the priest shalt
make an atonement for him before the Lorn: and it shall be forgiven him
for any thing of all that he hath done in trespassing therein (Lev. 6:1-7).

The theocentric meaning of this passage is that theft is a
transgression against God. God is here identified as the primary
victim of crime: “Ifa soul sin, and commit a trespass against the
Lorp. . . .” This principle is fundamental to biblical law. It is
therefore not sufficient for a thief to make restitution to his
earthly victim; he must also make restitution to God.
