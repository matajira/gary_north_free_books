138 LEVITIGUS: AN ECONOMIC COMMENTARY

either its discovery or his conviction, as we shall sce. The 20
percent penalty payment constituted a double tithe.°

Why impose a 20 percent penalty, the equivalent of a double
tithe? What did the tithe have to do with restitution to the
victim? James Jordan suggests that it was because guardianship
is associated with Levitical office, and so is the tithe. Numbers
18 established the Levites as the guardians of sacred space and
sacred things. “And thy brethren also of the tribe of Levi, the
tribe of thy father, bring thou with thee, that they may be
joined unto thee, and minister unto thee; but thou and thy sons
with thee shall minister before the tabernacle of witness. And
they shall keep thy charge, and the charge of all the tabernacle:
only they shall not come nigh the vessels of the sanctuary and
the altar, that neither they, nor ye also, die” (Num. 18:2-3).
They were required to keep the common Israelites away from
the sacred spaces of the tabernacle. This entitled them to a tithe
as their lawful inheritance (Num. 18:21-24), Conclusion: the
tithe and the Levitical protection of sacred space were linked
judicially.

Death was the civil penalty for invading the temple’s sacred
space, which was protected by the Levites (Num. 18:7), just as
an invasion of the priests’ sacred space by the Levites would
bring God’s death sentence against both priest and Levite
(Num. 18:3). The penalty for other invasions of sacred areas
was the 20 percent penalty: a double tithe. A vow to a priest
was redeemed by paying a 20 percent penalty (Lev. 27:19).
Refusal to pay this redemption price resulted in the permanent
loss of the property, even rural land (Lev, 27:20-21).!° Unin-
tentional boundary violations of sacred things also required a
double tithe penalty: “And if a man eat of the holy thing unwit-
tingly, then he shall put the fifth part thereof unto it, and shall

9. Andrew A, Bonar, A Commentary on Leviticus (London: Banner of Truth Trust,
[1846] 1966), p. 109.
10. See Chapter 37, below.
