730 LEVITICUS: AN ECONOMIC GOMMENTARY

holiness of [sracl, 425
jubilec, 388, 421, 432
law &, 22
Levites &, 476-77
population growth, 417,
421
sin &, 569
subsidized, 70
Uriah, 472, 505
usury (sce interest)
Uzziah, 172

value, 436-38, 439n, 440

vanity, 240

vassal, 81

vegetarians, 342

vengeance, 264-65, 268, 270-
72, 276, 643

vexing, 308, 310-11

Vickers, Douglas, 652

victim
ceiling on criminal’s price,

503

deaf, 233
double restitution, 500, 503
gleaners, 197
God, 134, 144, 196, 233
jubilee, 466n
kinsman-redcemer, 239
representative, 145, 137
sanctions, 238, 239
spokesmen for, 232-33

victim’s rights, 140, 144-45,
500

Vietnam, 373n

violence, 270-72

Voltaire, 159, 191

voluntarism, 230

vomiting

covenantal language,
182, 183, 338

land, 182-83, 193, 297
military, 186, 188
New Covenant, 193
post-exilic, 188

voting, 99n

vouchers (legal fees), 140

vow
annulment, 587-88
devotion, 583
holiness, 581
irreversible, 584, 585,

586

lessee, 612-14
money, 580-81
Nazarite, 156
priestly adoption, 642
rural land to priests, 607-9
singular, 585
to priest, 138, 607-9
violation & restitution, 130
votive offering, 75
also see oath, 130

wages

advance payment, 225-
28

criminals not entitled, 500-1

daily, 225-26, 228

delayed, 223-30

Israelite bondservant, 497-
99, 530, 533-34, 539-40

kinsman-redeemer, 534-35,
536, 537

Levites as regulators, 540

rapid payment, 223
