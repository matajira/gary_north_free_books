Covenantal Fruit 301

God’s covenant; their deliverance from bondage in Egypt was
not sufficient in their eyes. They were basically announcing:
“No immediate payoff in real estate; so, no mark of covenantal
subordination in our sons.” ‘They wanted an immediate payoff,
just as Adam had desired in the. garden; they were unwilling to
trust God. with respect to the inheritance of the land by their
children. So, God kept that uncircumcised younger generation
in the wilderness until the exodus generation died, except for
Joshua and Caleb.

There may also have been a judicial reason for their refusal.
The nation had rebelled against Joshua and Caleb, and then
against God when they attacked the Amalekites and Canaanites
against God’s specific command (Num. 14:39-44). ‘The 10 cow-
ardly spies had been killed by God through a plague (Num.
14:37). ‘he nation had become unholy: separated from the
definitively holy Promised and for one generation. The fathers
may have concluded that they had lost their status as household
priests. So, they refused to circumcise their sons, or have the
Levites circumcise them. Whether this was at God’s command
is not revealed in the text. But these people were cowards, and
they had seen what happened to the 10 cowardly spies. They
may have decided that discretion was the better part of valor
with respect to circumcising their sons.

After entering the land, the sons who had been born in the
wilderness were immediately circumcised. At that point, they
celebrated the Passover with the existing fruit of the land (Josh.
5:11). Immediately, the miraculous manna ceased. The people
lived off the fruit of the land from that time on (Josh. 5:12).
‘They had moved from miraculous food to miraculous warfare
(Jericho). After the conquest of the land, they moved to non-
miraculous planting.®

2. There would still be onc remaining miracle: the triple harvest just before the
seventh sabbatical year (Lev. 25:21).
