452 LEVITIGUS: AN ECONOMIC COMMENTARY

food would come predictably in terms of their corporate coven-
antal conformity to His law: the greater their obedience, the
more predictable their food.

The Miraculous Triple Crop

In order to remind them of their continuing need to obey
Him, as the sovereign provider of food, God did not totally
remove His miracles [rom the land. ‘Iwice per century, God
promised to provide them with bread in a miraculous way: the
triple crop of the sixth year in the seventh cycle of the sabbati-
cal week of years. This would be the equivalent of manna.

The Self-Discipline of Thrift

In a normal cycle of seven years, the Israclitcs had to save
cnough grain over six years to get through the seventh (sabbati-
cal) year and half way through the eighth year, until the
eighth-year crop could be harvested.’ But this was not the case
in jubilee year periods. In the sixth year would come a triple
crop. That crop would feed them in the second half of year six,
all of year seven (sabbatical), all of year eight (jubilee), and half
way through year nine.

This means that in the six years prior to a jubilee year, farm-
ers did not have to store up crops in order to carry themselves
through the sabbatical year, the jubilec year, and half way
through the ninth year until the crop came in. This triple crop
was Old Covenant Israel’s equivalent of the manna of the wil-
derness: a miraculous gift from God. It was the bread of life.

In escaping the production restraints of a normal sabbatical
cycle, they acknowledged their dependence on the grace of
God. The thrift that was agriculturally necessary during normal
sabbatical periods was not required during the jubilee’s week of

3, Gary North, Teals of Dominion: The Case Laws of Rxodus (Tyler, Texas: Institute
for Christian Economics, 1990), pp. 816-17.
