Mutual Self-Interest: Priests and Gleaners 365

gleaners in the case of tithe-collecting. The more gleaners in
the fields, the more likelihood that two or more of them would
have told the truth to the church’s officers about the size of the
crop.

Reducing the Costs of Monitoring Cheaters

If a landowner did not allow any gleaners to glean, one or
more of them could lawfully complain to the Levites. his
would alert the Levites to the possibility of an infraction: if the
landowner was willing to cheat God by cheating the gleancrs,
he was perhaps equally willing to cheat God by cheating the
Levites of their tithe. ‘The presence of gleaners meant the pres-
ence of monitoring agents whose self-interest coincided with the
priesthood’s self-interest.

These agents were not paid by the priesthood. This points to
the priesthood as the authorized agency for enforcing the
gleaning laws. Why not the local civil magistrate? Because the
Levites reccived a greater percentage of the crop than a God-
honoring civil magistrate would. The Levites lawfully reccived
a full 10 percent of the increase in the crop; only a corrupt
king would demand this much (1 Sam. 8:15, 17). The Levites
had to give only 10 percent to the priests, retaining 90 percent
for themselves (Num, 18:26-28). There was no similar kingly
guarantee for the percentage retained personally by local mag-
istrates. Thus, local Levites had a far greater economic incentive
under Mosaic law to monitor the output of the fields than the
local civil magistrates did. The Levites had the greater econom-
ic incentive under biblical law to seek out zero-price agents to
monitor the output of the farms. ‘This incentive structure indi-
cates that the church was where God lodged the judicial au-
thority governing the gleaning law. The church could do very
well — collect the full tithe owed to it - by doing good: defend-
ing the gleaners.

The recapitulation of the gleaning law in the section of Le-
viticus dealing with two fixed-payment grain offerings — the
