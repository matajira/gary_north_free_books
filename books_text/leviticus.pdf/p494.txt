438 LEVITIGUS: AN ECONOMIG COMMENTARY

ing business; ct me assure you, most producers do not have
sufficient funds to change the minds of many consumers." Ail
the shoe manufacturcr can do is lower the price of his invento-
ry, even if he does not regain his costs of production. Alter all,
some income is better than no income. Some money is better
than a pile of unsold shoes that must be stored somewhere.

Consumers can buy many things with their inventory of
unspecialized money; produccrs cannot buy many things with
their inventory of specialized goods. This is why consumers are
economically sovereign over producers, even though consumers
and producers are equally sovereign legally. ‘The hierarchy of
control under capitalism is economic. Consumers “hold the
hammer”: money (the most marketable commodity) plus the
legal authority to buy or not to buy from any producer

Market theory rests on the insight that the consumer is
economically sovereign, even though the owncr of a tool of pro-
duction is legally sovercign. The owner lawfully can do whatever
he pleases with his property, so long as he does not physically
injure someone else, but he cannot thwart the consumer at zero cost.
If he thwarts the demand of the highest-bidding consumer by
not sclling the capital good’s final output to him, he thereby
forfeits the extra amount of moncy which that consumer would
have paid him. ‘lhe owner’s inventory cost is not just the cost of
storage and insurance, but also the forfeited income.”

In the expectation that a particular piece of capital equip-
ment will produce something of value to future consumers —
something they will pay for - producers today impute value to
capital equipment. ‘They do the same with land, labor, and raw
materials. They do this as present economic agents of future consumers.
‘The sovereign consumers in a supplier’s plans are nol present

11. The classic: example is Ford Motor Company's introduction of the Edsel
automobile, 1958-60. Ford could not sell enough cars to make a profit.

12. The cost of production is not an aspect of economic cost. What is spent is
spent; sunk costs. Once spent, the producer’s past costs are irrelevant to the crucial
question: What can I get for my stock of goods?

 
