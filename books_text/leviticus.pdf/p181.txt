Sacred, Profane, and Common 125

Christians can better understand the biblical distinctions
between “sacred vs, common” and “sacred vs. profane” by
considering the difference between a communion meal held
during a worship service in church and a family meal eaten at
home by a Christian family. Both meals are equally religious.
Both meals are legitimately introduced by prayer. But only one
meal is sacramental: the church’s communion meal. What must
be understood from the beginning of our discussion is this: the
family meal is not profane. It is common, but it is not profane.
Also, it is religious despite its legal status as common.

It is incorrect to contrast an inherently sacred place with an
inherently profane place. A sacred place has been made sacred
by the judicial declaration of God or by a priest acting in God's
name. It has been sanctified: set apart judicially. It is neither
naturally nor metaphysically sacred. Similarly, there can be no
naturally or metaphysically profane place in the way that there
can be a naturally common place. A profane place is a violated
sacred place. It has been the victim of an illegal trespass. The
Hebrew word translated most frequently as “profane” (khawlawtl)
is usually translated as “slain.” It is sometimes translated as
“wounded” (I Sam. 17:52). This Hebrew word means pierced. It
conveys the sense of someone’s having violated a boundary.
The word is not used in the sensc of a common place that just
siis there being common. A common place cannot become
profane, for it possesses no sacred boundary to trespass; only a
sacred place can become profane.

  

The Sacred as Priestly

What is “the sacred,” biblically spcaking? It is not mercly the
religious sensibility in man, a need analogous to the need for
food or sex, as modern academic usage would have it. Rather,
it has to do with the church’s sacraments. In its narrowest

8. Nui. 19:16; 19:18; 23:24; and dozens of other verses.
