366 LEVITICUS: AN ECONOMIC COMMENTARY

first{ruits — also points to the priesthood as the agency of en-
forcement for the gleaning law. The priests are identified in
this passage as being guaranteed a fixed payment at the feasts,
irrespective of the size of any farm’s crop. As judges, their
economic self-interest was in no way threatened by the gleaners.
The Levites and priests could enforce the gleaning law without
worrying that their very diligence would automatically reduce
uhcir income. The costs of the gleaning program would be
borne by the landowners.

What we have here is a system of mutual self-interest be-
tween the priestly tribe and the gleaners. The Levites and
priests gained allies among the gleaners during the season of
the tithe — zero-price (to the priestly tribe) monitors in the
fields - while being exposed to no economic threat from their
allies during the scasons of the feasts. Simultancously, the
gleaners gained allies — a priesthood with the power to cxcom-
municate uncooperative landowners — during the scason of the
tithe, while being exposed to no cconomic threat from their
allies during the seasons of the feasts.

This mutually beneficial arrangement worked well in normal
years. It broke down, however, during sabbatical years (Lev.
25:4-5, 20). In sabbatical years, the pricsthood had a short-term
financial interest in seeing a normal harvest rather than idle
(resting) land. Priests and landowners did not acknowledge the
long-term agricultural productivity benefits of resting the land
one year in seven. ‘heir shortened time perspective persuaded
them not to honor the sabbatical year of rest for the land.”
‘This secms to me to be the most likely reason why the sabbati-
cal year of rest for the land was not enforced in Israel for al-
most five centuries (II Chron. 36:21). The Levites defected. But
it was the pricsthood, not the State, that was authorized by God
to enforce the gleaning law.”

21. See below, p. 404,
22. Gary North, Boundaries and Dominion: The Economics of Leviticus (computer
