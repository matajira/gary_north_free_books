262 LEVITIGUS: AN ECONOMIC COMMENTARY

Conclusion

‘The Bible specifics the locus of primary judicial sovercignty:
the local court. This court has the benefit of better knowledge
of the facts and circumstances of any alleged crime. It has a
tradition of judicial decisions (precedents) that is familiar to
jurors. It is made up of people who speak God's law — jurisdic-
tion — with a familiar local “accent.” This enables local residents
to forecast more accurately what is expected of them. ‘This
reduces forecasting costs.

The jury is the culmination of a long tradition of Christian
history. The jury makes possible a greater division of judicial
labor. A jury is less likely to be arbitrary than a lone judge.
Men can obtain justice less expensively because of the greater
efficiency of a jury’s collective judgment. The authority of the
jury at the local level provides a counter to the decisions of
professional bureaucrats.

By lodging in local courts the final authority to declare an
accused person “not guilty,” God’s law provides a check to the
centralization of political power. A distant civil government
cannot impose its will on local residents without a considerable
expenditure of time and moncy, possibly risking the public’s
rejection of the central government's legitimacy, the crucial
resource of any government.
