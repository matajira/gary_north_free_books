144 LEVITICUS: AN ECONOMIG COMMENTARY

Civil Identification of a True Church

This law requires that the local civil government identify the
local ecclesiastical guardians of the oath. It.nust identify those
congregations that are confessionally orthodox and thercfore
eligible to receive the trespass offcring. This authority to identi-
fy confessionally orthodox churches implies that members of
associations not so identificd as orthodox cannot legally be
granted the legal status of citizens. In short, the State is a con-
fessional, oath-bound, covenantal institution. It is required to
establish what constitutes a valid civil oath, but only after con-
sultation with churches. Churches are confessional, covenantal
institutions, separate from the State. They may lawfully impose
added confessional requirements beyond the civil oath for their
members and officers, but if they do not confess the Trinity,
they are not to be recognized as guardians of the civil oath.

Conclusion

We sce in this law the application of the Bible’s fundamental
principle of civil justice: victim’s rights. The twin issues in this
case involve the defense of a pair of judicial boundaries: private
property and the civil oath. The ecclesiastical issue is this: What
is the meaning of the trespass offermg? I argue that the tres-
pass offering is tied judicially to the defense of the civil oath
against the criminal who falsely declares his innocence. ‘hat is,
there is more to a legitimate defense of the civil oath than the imposition
of civil sanctions.

The primary victim of the theft is God, against whose majes-
ty the thefi is committed. ‘Ihe secondary victim is the earthly
victim. He then becomes the primary agent of God in this legal
dispute between God and the criminal. God brings a lawsuit
against the criminal in His heavenly court; He authorizes the
victim to bring a lawsuit in a civil court. ‘This is the biblical
principle of victim’s rights.

An important goal of the criminal justice system is to gain a
confession from the criminal before a trial is held or a verdict
