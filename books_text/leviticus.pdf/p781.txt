Christian, 83
dispensationalism’s, xxxii
Kline’s presuppositions &,
551-52
liberation theology, 240
predictable sanctions &,
457-58, 461, 551-52, 579
Trinity &, 218
also see pluralism.
socialism, 213, 241-43, 246-47,
553
society, 98-99, 103
Socrates, XXV-xxvi
Sodom, 544, 631
soil, 396
soul, 81
sovereignty
civil, 249
consumer, 412, 436-39, 532-
33
earthly, 257
economic vs. legal, 438
God (see God: sovereignty)
jury, 260
locus of, 249
modern, 546
people, 94, 95, 96-97,
98-99
State, 52-53, 248
treason &, 374
witness, 336n
Sowell, Thomas, 112n
specialization, 255 (also see
division of labor)
spies, 300-1
spoils, 415, 430
stake in society, 467-68
stars, 566

State

agent of, 270, 336n

autonomous man, 58

bankruptcy of, 106

below God, 143

boundaries on, 53, 62, 271,
367

central ?, 97, 273

charity, 171, 173, 207-8,
360, 485

church &, 93, 97, 104, 111-
12, 142-44, 165

church protects, 143

common grace, 141-42

courts, 189-40

defense of property, 486

divinization, xliv, 58, 95,
142

division of powers, 272-76

execution, 268

fiat money, 266n

foreign aid, 559

graduated tithe, 248

guardian of its oath?, 142-
43

guilt &, 142

gun control, 336n

healing, 175, 208, 267n,
271

heart &, 263

invader, 486

judges God?, 527

leprosy, 171-72

Moloch State, 59

monopoly, 270-72, 361

neutrality, 380

next-to-last judgment, 267

oath, 142-44
