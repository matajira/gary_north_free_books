Boundaries of the Jubilee Land Laws 4i)

No work was allowed on that day. The day of atonement was a day
of vest — the ultimate day of rest in ancient Israel, symbolizing cove-
nant-keeping man’s rest from the curse of sin. It was a day set apart
for cach person’s examination of his legal state before God and
the self-affliction of his soul (Lev. 16:30-34; 23:27-31).

Once each half century, the day of affliction was to become
the day of liberation. The meaning of the Hebrew verb for
“alllict” is submission or humility. The day of national liberation
and family inheritance took place on the day of formal subordi-
nation to God. The imagery is obvious. Only through submission
to God can man experience liberation. Autonomy is not liberation.
It is the antithesis of liberation. This is why modern human-
ism’s free market economic theory, which is both agnostic and
individualistic, is not the source of the free society that its de-
fenders proclaim. If we begin our economic analysis with the
presupposition of the autonomous individual in an autonomous
cosmos, we begin with a hypothesis that cannot lead to liberty
and maintain it.

A Question of Subordination

The year of jubilee began with the blowing of a trumpet, a
trumpet announcing the day of atonement. The ram’s horn,
yobale (Josh. 6:4-5), is the origin of the English transliteration,
jubilee (Lev. 25:10-13). ‘The covenantal basis of dominion is
formal, oath-bound subordination to God. The jubilee year
began with the sound of a trumpet: the audible symbol of the
final judgment (I Cor. 15:52). The day of atonement was to
remind the Israelite nation of its unique corporate subordina-
tion to God. This ritual subordination was to serve as the foun-
dation, both judicially and psychologically, of each Israelite’s
tasks of leadership. Humble before God, they were to be ag-
gressive toward the world. This is the meaning of the New
Testament statement, “Blessed are the meek; for they shall
inherit the earth” (Matt. 5:5). They are meek before God, not
