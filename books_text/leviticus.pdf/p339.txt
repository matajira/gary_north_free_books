The Preservation of the Seed 283

sion. Tribal boundaries were part of an overall structure of
covenantal unity. This included politics: localism.

Family membership and rural land ownership in Isracl were
tied together by the laws of inheritance. A rural Israclite — and
most Israclites were rural’ — was the heir of a specific plot of
ground because of his family membership. There was no rural
landed inheritance apart from family membership. Unlike the
laws of ancient Greece, Mosaic law allowed a daughter to inher-
it the family’s land if there was no son. But there was a condi-
tion: she had to marry within the tribal unit (Num. 36:8). The
landed inheritance could not lawfully move {rom one tribe to
another (Num. 36:9).? A man’s primary inheritance in Isracl
was his legal status (freemanship). Land was tied to name. The
land of Israel was God's; His name was on it. The family’s land
was tied to the family’s name.° Jacob had promised Judah that
his blood line would rule until the promised heir (Shiloh)
should come (Gen. 49:10). Thus, the integrity of each of the
seed lines in Israel — family by family, tribe by tribe — was main-
tained by the Mosaic law until this promise was fulfilled. The
mandatory separation among the tribes was symbolized by the
prohibition against mixing sceds. The prohibition applicd to
the mixing of seeds in one field. The field did not represent the
whole world under the Mosaic Covenant; the field represented the
Promised Land. The husbandman or farmer had to create boun-
darics between his specialized brecds and between his crops.

So closely were sced and land connected in the Mosaic law
that the foreign cunuch, having no possibility of seed, was not

7. This is not to say that God intended them to remain rural. On the contrary,
the covenantal blessing of God in the form of population growth was to move most
Israelites into the cities as time went on. Sec below, Chapter 25, section on “The
Demographics of the Jubilee Inheritance Law,” pp. 416-22.

  

8. The exception was when rural land that had been pledged to a priest went to
him in the jubilee year if the pledge was violated (Lev. 27:20-21). See Chapter 37,
below: “The Redemption Price System.”

9. North, Boundaries and Dominion, ch. £7, subsection on “Family Land and
Family Name”
