224 LEVITICUS: AN ECONOMIG COMMENTARY

A Position of Weakness

The wage earner in verse 13 is in a position of comparative
weakness. He is assumed by God to be in a weaker economic
position than the individual who is paying his wages. This
employer-cmployce relationship reflects God’s supremacy as the
sovereign employer and man’s subordination as a dependent
employee.

If the wage earner is not paid immediately, then he is being
asked by the employer to extend credit to the employer. ‘he
employer gains a benefit — the value of the labor services per-
formed — without having to pay for this benefit at the end of
the work day. The Bible allows this extension of such credit
during daylight hours, but not overnight.” This law teaches
that the weaker party should not be forced as part of his terms
of employment to extend credit to the stronger party. God ac-
knowledges that there are differences in bargaining power and
bargaining skills, and He intervenes here to protect the weaker
party. This is one of the rare cases in Scripture where God docs
prohibit a voluntary cconomic contract.

What if the worker says that he is willing to wait for his pay
if he is given an extra payment at the end of the period to
compensate him for the time value of his money (i.c., interest)?
This would be an unusual transaction. The extra moncy carned
from two weeks of interest would be minimal in comparison to
the amount of the wage. In any case, to abide by the terms of
this law, such a voluntary agreement would have to be a legal
transaction publicly separate from wage earning as such. There would
have to be a public record of its conditions. It would constitute
an investment by the worker. But the worker would have to
pay his tithe and taxes on this money before he could legally
lend it to the employer.

2. By implication, the night laborer is under the same protection: he must be
paid before the sun rises. The idea is that he must he paid by the ond of the work
day.
