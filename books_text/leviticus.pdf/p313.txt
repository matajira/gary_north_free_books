Local Justice us. Centralized Government 257

local court. The fundamental agency of corporate judgment is the local
court, whether civil or ecclesiastical. This is an extremely important
principle for any system of law designed to resist the centraliza-
tion of power.

The civil judge in the Mosaic Covenant declared the sen-
tence: negative sanctions. Capital sanctions were carried out by
the people, beginning with the witnesses (Deut. 17:6). Gase by
case, the civil court was to declare judgment. As the cases grew
more difficult, they would work their way up the appeals court
system. The most difficult civil cases ended up in Jerusalem in
the king’s courtroom. The king was the Supreme Court of the
Israelite civil order. This is why he was commanded to read the
law daily (Deut. 17:18-19). Yet even the king could not lawfully
declare absolutely final earthly judgment, imposing final earthly
sanctions, for there is no final, institutionalized, earthly court of
appeal in a biblical civil order. Only one person can lawfully
declare the final judicial word of the Lord: Jesus Christ. There-
fore, the people as a whole could lawfully intervene to restrain
the king, as they did when Saul attempted to carry out his
judgment against his son Jonathan (I Sam. 14:45). The people
placed a judicial boundary around the king, and they were
willing to place a physical boundary around him. He relented.
On what basis could they overturn the king’s sentence? Only as
authorized jurors who refused to convict Jonathan because the
king’s verbal legislation on the battlefield had been foolish and
therefore unconstitutional. Their declaration of “not guilty” was
final, and Saul accepted it.

Nevertheless, the king did lawfully serve as the highest civil
judge in Israel. This was the great authority of kingship: exer-
cising the power of speaking in God’s name as the single indi-
vidual who could declare God’s final earthly judgment, unless
the people lawfully revolted under the direction of the lower
magistrates.’* David's rebellious son Absalom began his revolt

13. Sce John Calvin, Institutes of the Christian Religion, [V!XX:31.
