The Sabbatical Year 397

inheritance for future generations. God’s sharecroppers in one
gencration were not allowed to undermine the future value of
the land by overproduction in the present. God, as the land’s
ultimate owner, was thereby able to maintain a greater percent-
age of the land’s original capitalized value.

The Israelites did not always enforce the provisions of the
sabbath land law prior to the exile. In other words, they did
not enforce the terms of the original lease. God allowed this
infraction to continue for almost five centuries. Then He col-
lected payment from a later generation. “And them that had
escaped from the sword carried he away to Babylon; where
they were servants to him and his sons until the reign of the
kingdom of Persia: ‘Io fulfil the word of the Lorp by the mouth
of Jeremiah, until the land had enjoyed her sabbaths: for as
long as she lay desolate she kept sabbath, to fulfil threescore
and ten years” (II Chron. 36:20-21).* Two generations of sharc-
croppers then learned a judicial lesson in Babylon: God has a
long memory for the details of His law. Those who violate it
will eventually pay restitution to Him by paying restitution to
their victims. In this case, they paid to the land, which rested.

A Year of Gleaning

There appears a problem with the translation in the King
James Version, Actually, there is no problem, but there is a
problem for interpreters who do not take the text literally.
Verse 5 says: “That which groweth of its own accord of thy
harvest thou shalt not reap, neither gather the grapes of thy

4. By the time of Jeremiah, the Israelites had been in the Promised Land for
almost eight centuries. Of this period, 490 ycars (70 x 7) had been spent without a
sabbatical year, Jeremiah did not say when this period of law-breaking began. I
presume that it began 490 years before the Babylonian captivity, ie., sometime late
in Saul’s kingship. I am using James Jordan's chronology: “The Babylonian Gormec-
tion,” Biblical Chronology, V1 (Nov. 1990), p. 1: 3426 Anno Mundi = 586 B.C, The
accession of Saul was 2909 AM. Jordan, “Chronologies and Kings (IT),” iid., TT
(Aug. 1991), p. 2. Computation: 586 + 490 = 1076 B.C., ie., 3426 AM - 490 AM =
2936 AM. David came to the throne in 2949 AM, i.e., 1063 B.C.
