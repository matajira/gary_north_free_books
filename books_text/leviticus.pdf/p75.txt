Introduction 19

Sam. 8:15, 17) would have been in the range of one-third to
one-half of an agricultural family’s income. ‘This is comparable
to the middle-class member's tax burden in the twentieth centu-
ry ~ a very high-tax era. In the modern world, most of this
money goes to various levels of the State. In ancient Israel,
most of it went to the priestly tribe and the poor. ‘Theirs was a
far better system, but it was expensive. T know of no society in
the ancient world with anything like these external costs on the
average farmer-citizen.

An Israelite could have chosen to live in a city located closer
to Jerusalem, but this would have led to higher real estate
prices in those cities. What a man saved in travel costs he paid
for in housing costs. The costs of sacrifice had to be borne,
There should be no question about it: Old Govenant Israel was
an expensive place to live, especially for Israclites.

The Farming Subsidy to Resident Aliens

This brings us to a controversial but inescapable conclusion:
non-Israelites, who did not have to pay these temple-based
costs, had a tremendous economic advantage as farmers m
Israel. Except for one year in seven (Deut. 31:10-12), they were
not required to attend the feasts. They could invest their time
and money into farming while the Israelites were on the march.
‘They were allowed to lease agricultural property from Israelites
for up to 49 years (Lev. 25:47-52). This means that there was
an indirect economic subsidy in ancient Isracl for forcigners
and covenant-breakers to occupy the agricultural arcas, with
the Israelites occupying the citics. Covenant-breakers would
have paid rent for rural land (o the Israclites who moved to the
cities.

The larger the Israelite families became, the smaller and less
economically efficient each generation’s share of the original
family plot. If the jubilee laws were enforced, this must have led
to the creation of professionally managed farms along the lines
of modern corporate farming. It is likely that non-Israelites
