10

THE PROMISED LAND AS
A COVENANTAL AGENT

Defile ot ye yourselves in any of these things: for in ail these the
nations are defiled which I cast out before-you: And the land is defiled:
therefore I do visit the iniquity thereof upon it, and the land iiself vom-
iteth out her inhabitants. Ye shall therefore keep my statutes and my
judgments, and shall not commit any of these abominations; neither any
of your own nation, nor any stranger that sojourneth among you: (For
all these abominations have the men of the land done, which were before
you, and the land is defiled;) That the land sue not you out also, when
ye defile it, as it spued out the nations that were before you. For whosoev-
er shall commit any of these abominations, even the souls that commit
them shall be cut off from among their people (Lev. 18:24-29).

‘The theocentric meaning of this passage is that God is the
Lord of history. He brings judgments in terms of His covenan-
tal law. History is theocentric. It is therefore to be understood
in terms of the covenant.

God, the supreme authority of the covenant (point one),
possesses the power to impose sanctions directly (point four),
but He usually chooses to use agents in this task (point two). In
this passage, He uses an agent to scparatc covenant-breakers
from the socicty of covenant-keepers (point three).
