356 LEVITIC

 

AN ECONOMIC COMMENTARY

the Promised Land — that was mandated at Pentecost: the cov-
enantal necessity of ethical continuity with righteousness.

Pentecost was understood by the rabbis as the anniversary of
the giving of the ‘len Gommandments.' It was a third-month
feast in the sanctuary calendar. There were two overlapping
systems used in Israel for measuring the year: sanctuary and
ordinary.? The religious year began in the spring: the first
month, Nisan (Esth. 3:7}, when Passover was celebrated (Ex.
12). The civil year began in the fall: on the first day the seventh
month of the religious calendar (called Tishri in the Talmud).*
‘This month began with a day of sabbath rest (Lev. 23:24-25).
Ten days later, the day of atonement took place (Lev. 23:27-
28).* In terms of the sanctuary calendar, the law was given to
Moses in the third month (Ex. 19:1) on the third day of the
week (Ex. 19:16). Iabernacles was a seventh-month feast (Lev.
23:24). It completed the annual cycles of three feasts.*

The Festival of Pentecost

Pentecost was closcly associated with the harvest.' It was a
grain-related feast. The festival required the following: “Ye
shall bring out of your habitations two wave loaves of two tenth
deals: they shall be of fine flour; they shall be baken with leav-

1. Alfred Edersheita, The Temple: Its Ministry and Services As They Wore in the Time
of Jesus Christ (Grand Rapids, Michigan: Eerdmans, 11874| 1983), p. 261.

2. James Jordan, ‘Jubilee, Part 2,” Biblical Chronology, V (March 1993), p. 1.

3. Rosh. Hash., 1:3; cited in “Month,” Cyclopaedia of Biblical, Theological, and
Ecclesiastical Literature, edited by John M’Clintock and James Strong, 12 vols. (New
York: Harper & Bros., 1876), IV, p. 547.

4, Jordan notes that the official first year of the reign of a king of Judah ran
from the first day of the seventh month of the religious year to the last day of the
sixth month of the next religious year.

 

5. This structure parallels the week of purifications for the person who had come
in contact with a dead body: tbird-day purification and seventh-day release (Num.
19:11-12), James Jordan, The Law of the Covenant: An Exposition of Exodus 21-23 (Tyler,
institute for Christian Economics, 1984), p. 57.

6. Gordon J. Wenham, The Book of Leviticus (Grand Rapids, Michigan: Ferdmans,
1979), p. 804.

 

Texas:
