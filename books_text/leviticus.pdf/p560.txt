504 LEVITICUS: AN ECONOMIC COMMENTARY

a slave in another man’s house, under another’s temporary
authority. Judicially, he had become a child.

Citizenship in any holy commonwealth is the legal authority
to declare or bring negative civil sanctions in God's name. The
pre-eminent manifestation of this authority in Israel was service
in the military: God’s holy army. The army had the task of
defending the boundaries of the land, i.e., keeping it holy,
secure from forcign invaders. The army had to keep the land
from being prefaned by invaders: boundary violators. To be a
member of the army required the payment of redemption
blood money at the time of the numbering of the nation imme-
diately prior to a holy war (Ex. 30:12-13).? Circumcised Israel-
ite males became eligible to serve at age 20 (Ex. 30:14).

The Israelite slave had to treated as “as an hired servant,”
the text says. He had to be paid a wage by his Israelite master.
Iie therefore had money to pay the redemption blood money
to the priests. Did this give him the right to serve in the army?
No; he was judicially a child even though he was over age 20.
Only with his owner’s permission could he serve in the army.
He was not a free man; he was not a citizen.

Gentile Slaves

Was a gentile slave who paid his redemption blood money
and also fought for Israc! in a holy battle subsequently released
from bondage? Abram had fighting men (Gen. 14:14), but they
did not receive automatic freedom. However, this was before
the Abrahamic covenant was established (Gen. 15). It may be
that in Mosaic Israel, the willingness ofa slave to risk his life in
holy battle gained him his freedom, though not landed inheri-
tance.® Ile became a citizen in a walled city. If nothing else,

5. North, Tools of Dominion, ch. 82: “Blood Money, Not Head Tax.”

6. ILis one of the most interesting facts about the American Civil War (1861-65)
that in its final months, Southern leaders and generals began to discuss the possibility
of granting freedom to any Negro slave who was willing to enlist in the Confederate
