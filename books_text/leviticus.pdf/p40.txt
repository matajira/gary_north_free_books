PREFACE

And further, by these, my son, be admonished: of making many books
there is no end; and much study is a weariness of the flesh (Eccl. 12:12).

When I began writing my economic commentary on the
Bible in the spring of 1973,! I did not imagine that it would
take me over two decades just to begin Numbers. When I esca-
lated my time commitment to the project in the fall of 1977 to
10 hours per week, 50 weeks per year, I also did not imagine
that it would take this long. I did not imagine that I would
write such lengthy appendixes as Dominion and Common Grace, Is
the World Running Down? and Political Polytheism. But most re-
markable of all, I did not imagine that what now appears to be
a 30-year task to complete the Pentateuch will, if completed,
turn out to be the world’s longest footnote to another man’s
thesis: Ray Sutton’s 1985 discovery of the Bible's five-point
covenant structure.? Not four, not six, not seven: five. I also
want to make it unambiguously clear that I am talking only
about a covenant model. I am not saying, nor have I ever im-
plied, that this five-point model is the only model or structure
in Scripture. The seven-day weck model, the three-fold Trini-
tarian model, and other numeric and non-numeric models are

1. The first installment. wax published in the Chalcedon Report in May, 1978.

2, Ray R. Sutton; That You May Prosper: Dominion By Covenant (2nd cd.; Tyler,
Texas: Institute for Christian Economics, 1992).
