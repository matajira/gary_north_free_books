684 LEVITICUS: AN ECONOMIG COMMENTARY

credit &, 225-27
deferred (OT), 55
division of labor economy,
234
forgiveness, 55-56, 400, 402
freeman status threatened,
226
hierarchy, 56
kinsman-redcemer, 534
legal status &, 226
prisons, 55, 57
redemption &, 534
reduction, 234
sabbatical year, 400, 402
second-class, 495
servitude to aliens, 533-34
threat, 226
to God, 9, 11, 51, 53-57,
226
trap, 403
years of the finits, 435
also see bankruptcy
decalogue (see Ten Command-
ments)
decentralization, 113, 387, 482
(also see localism; tribalism)
deception, 135-37, 145
dedication, 582, 595
default, 227, 228-29, 487, 489-
90, 530
deflation, 488-489
democracy, 95, 260, 268
demons, 41
Denton, Jeremiah, 373n
dependence, 81, 211, 214-16,
228, 224
de-programming, 99n
design, 218-19

Deuteronomy, xlviii-xlix

devotion
boundary, 587
dedication vs., 595
legal status, 582-83
sanctification vs., 583-84

diaspora, 23

Diderot, 159

diet, xiv, 341-48

diminishing returns, 562

discontinuity, 79, 115-16, 279-
80, 458, 653 (also see conti-
nuity)

discase, 173 (also see leprosy;
plague)

disinheritance
broken vow to priest, 607-8
circumcision leads to, 340
covenant-breakers, 331
Eli’s sons, 591
excommunication, 340, 389-

90
exile, 407
Israel, 290-91, 323-24, 390-
91, 407, 584n, 611, 647

kinsman-redeemcr, 389
leprosy, 170-71
loss of citizenship, 389
Moloch worship, 328-29
rebellious sons, 608-9
reparation offering, liv
sanctions, 581
self-, 340
State, 62

dispensationalism, xiv-xv, xix,
XxIxX-Xxxviii, 650

division of labor
debt is inevitable, 234
