Introduction ll

trespass or more literally a “debt,” as we see it in the Lord’s Prayer.
‘Thus, any lawbreaking is a form of theft, creating indebtedness, which
must be covered by a Trespass or Compensation Sacrifice. Theft has to
do with boundaries, which is why it is equivalent to trespass. Leviticus
is the book of boundaries, of who is allowed to go where, and of how
to become cleansed once you have trespassed.?

The Book of Leviticus is book three of the Pentateuch. It is
the book of property. The eighth commandment, “Thou shalt
not steal,” is the third law of the so-called second table of the
law, i.e., the third law in the second group of five covenantally
structured laws. The third commandment, “Thou shalt not take
the name of the Lorp thy God in vain,” establishes a boundary
around God’s name. God’s name is His property, and He in
effect licenses the use of His name only for specific uses. The
parallels should be obvious. Point three of the biblical covenant
model establishes boundaries. Leviticus is the book of property
because it is the book of boundaries.

A Holy Walk Before the Lord

We now come to a topic that is never discussed by the com-
mentators. I have never secn any commentator devote as much
as one page to it, yet it is more important for understanding
the unique nature of the economic life of ancient Israel than
any other topic. T am not trying to exaggerate; I really mean
this. Here is the question that demands an answer: How did
they have time to earn a living? ‘(he mandatory sacrifices ate
up time as well as crops. Whatever answers to this question that
Israel came up with were fundamental to the life of the nation
for almost 14 centuries, yet we honestly do not know how Israel
answered it. As far as I know, nobody has discussed in detail
the economics of the festival journeys. The rabbis who compiled
the Mishna and Talmud in the four centuries after the fall of

13. Hid, pp. 12-13.
