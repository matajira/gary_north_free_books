Inheritance Through Separation 341

ye shall not make your souls abominable by beast, or by fowl, or
by any manner of living thing that creepeth on the ground,
which I have separated from you as unclean” (v. 25). ‘These
animals had not been prohibited before God led them out of
Egypt. The distinction between clean and unclean had been
present in Noah’s day (Gen. 7:2, 8), but no prohibition against
eating unclean beasts was announced at that time. In this sense,
the clean-unclean distinction was prophetic for Noah. The
distinction was established so that Noah would take scven times
as many pairs of clean beasts into the ark (Gen. 7:2). The dis-
tinction had significance for the future of Israel — the increased
likelihood of the survival of clean beasts — but not for Noah’s
day. Otherwise, the detailed food jaws of Leviticus would have
been given to Noah. But they weren’t. “Every moving thing
that liveth shail be meat for you; even as the green herb have
I given you all things” (Gen. 9:3).° Abraham was under no
dietary restrictions; God’s promise of the land did not involve
dietary separation. Joseph was under no dictary restrictions in
Egypt. Clearly, the dictary laws were not cross-boundary laws.

A society’s diet separates it from other societies almost as
completely as its language does. It is very difficult for over-
weight people to lose weight permanently because of people's
almost unbreakable eating habits. To change a society's eating
habits takes generations, even assuming extensive contact with
foreigners (which Israel did experience because of her open
borders). Immigrants, or the children of immigrants, slowly
adopt the foods of their host nation. The Mosaic dictary laws
forced a major cultural break with the home nation for all those
who became circumcised resident aliens in Isracl.

6. Given his insistence of the authority of the Mosaic food laws in the New
Testament, Rushdoony should have commented on Genesis 9:3 in The Institutes of
Biblical Law (Nutley, New Jersey: Craig Press, 1973}. ‘here is only onc reference to
this verse, in the middle of a block quotation from another author (p. 36). The verse
is not even cited in the Scripture Texts index in volume 2, Law and Society (Vallecito,
California: Ross House, 1982).
