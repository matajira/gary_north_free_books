334 LEVITIGUS: AN EGONOMIG COMMENTARY

Immigrants were welcome in Israel, but the price of immigra-
tion was the forfeiture of the right to proselytize among the
Israclitcs. The houschold gods of immigrants could not lawfully
leave their households. They could not become public gods in
Israel. That is, they could not lawfully take on the status of
national gods. There was to be no public polythcism in Israel,
political or otherwise.

The God Who Imposes Boundaries

Rushdoony’s discussion of laws of separation is correct: “God
identifies Ilimself as the God who separates His people from
other peoples: this is a basic part of salvation, The religious and
moral separation of the believer is thus a basic aspect of Biblical
jaw.”” Separation can be achieved in several ways, however.
First, the believer can jom other believers in a religious gheuto.
This ghetto can be geographical, as in the case of certain Amish
and Mennonite sects. It can be cultural, as in the case of much
of modern fundamentalism and immigrant religious groups. It
is always psychological, what Rushdoony has called the perma-
nent remnant psychology.’ Second, the believer can seek the
physical removal of unbclievers from the community, either
through execution or expulsion. This was God’s required meth-
od with respect to the Canaanites. The problem here is honor-
ing the biblical judicial concept of “the stranger within the
gates": preserving liberty of conscience without opening the
social order to a new law-order, which means a new god. The
radical Anabaptists in Miinster in 1533-35 and the Puritans in
New England, 1630-65,? madc the mistake of cxiling residents

7. Rushdoony, Institutes, p. 204.

8. R.J, Rushdoony, Yan Til (Philadelphia: Presbyterian and Reformed, 1960), p.
13.

9. A 1662 letter from Charles II to Massachusetts established liberty of con-
science. It was not read in the Massachusetts General Court until 1665, In that year,
the Gencral Court repealed all laws that limited the vole to Congregational church
members. The Court determined that citizens in the colony henceforth did have to
