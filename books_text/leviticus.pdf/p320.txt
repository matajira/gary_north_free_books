264 LEVITIGUS: AN ECONOMIC COMMENTARY

except insofar as love is defined judicially: treating the neighbor
legally, i.e., love as the fulfilling of God’s law (Rom. 13:10). But
even in this case, there would have to be an infraction of a
specific civil law or an act against another person's rights —
lawful immunities (protected boundaries) — in order to enforce
this law of compulsory love. Hence, this law, too, is inherently
unenforceable by the State.

Nevertheless, this verse begins with a prohibition against
individual acts of vengeance. This is clearly an aspect of civil
law; the relevant Mosaic case law is the requirement that any
man who injures another man in a fight must pay restitution to
him (Ex. 21:18-19): no private vengeance. But why is this
verse’s negative injunction attached to two other injunctions
that are clearly individual moral injunctions — aspects of self-
government rather than civil government? By prohibiting per-
sonal grudges and requiring personal love, this verse makes it
clear that the concern of the civil portion of this civil law is the
elimination of privately imposed vengeance. The civil prohibi-
tion against taking vengeance applies only to individual actions.
This prohibition does not apply to the State. Civil law applies
negative sanctions to individuals whe commit specified prohibit-
ed acts; hence, it applies to individual acts of vengeance. Ven-
geance is legitimate when imposed by the State.

The parallel verse in Deuteronomy is used by Paul in his
epistle to the Romans to introduce his discussion of the civil
magistrate. “Dearly beloved, avenge not yourselves, but rather
give place unto wrath: for it is written, Vengeance is mine; I
will repay, saith the Lord” (Rom. 12:19; cf. Deut. 32:35a).
Paul’s message is not that there should be no vengeance in
history. On the contrary, he immediately launches into a discus-
sion of the civil magistrate’s lawful administration of vengeance:
*... for he beareth not the sword in vain: for he is the minister
of God, a revenger to execute wrath upon him that doeth evil”
(Rom. 13:4b), It is a mistake to see Paul’s prohibition of ven-
geance in these verses as applying to the institution of the State,
