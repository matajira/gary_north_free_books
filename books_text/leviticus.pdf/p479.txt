Boundaries of the Jubilee Land Laws 423

were to be owners of rural land in Israel — not the immigrant
stranger, and surely not the Canaanite.

Only the Israelites were strangers and sojourners with God.
Therefore, for as long as God dwelled uniquely in the land,
only His covenant people were allowed to remain agricultural
owners. They would police the land’s boundarics, keeping
strangers out except on God’s terms: inside walled cities, inside
Israelite households as slaves, as leaseholders, and as free agri-
cultural laborers. Far from being sojourners in the sense of
“wanderers in the land,” Israelites were to become the only
permanent owners of rural land. They would be strangers and
wanderers outside the Promised Land, but permanent owners
inside. The Promised Land was to serve as “home base” in a
worldwide program of trade and evangelism. ‘lo be a perfect
stranger to the covenant-breaking world outside the geograph-
ical boundaries of Israel, one had to be: 1) a covenanted mem-
ber of an Israclite family that had participated in the conquest,
or 2) an adopted member of a walled city’s tribe. This was the
meaning of “strangers and sojourners with me”: strangers to
the world but perpctual land owners inside rural Israel. Then
as now, the concept of stranger was an inescapable concept. A
person was either a stranger with God or a stranger from God.
The physical mark of circumcision and law[ul inheritance inside
Israel identified a man as being a stranger with God.

So, God set apart the Promised Land as His holy dwelling
place. He sanctified it. He placed boundaries around it. Thus,
the fundamental covenantal principle of the jubilce law was
holiness: the separation of covenantally unequal people from each
other.

The Principle of Inequality

God established His people as owners of the land through an
historically and judicially unique program of genocide. The
covenantal principle of the jubilee is simple: those who.wor-
shipped false gods within the geographical boundaries of Israel
