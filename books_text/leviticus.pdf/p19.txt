Foreword xix

A Question of Trust

Because I really do expect some parts of Leviticus to be
applied to modern life some day, I could not adopt the stan-
dard commentator’s implicit assurance to his readers: “Trust
me.” The stakes are too high. A serious reader of a serious
subject should not be asked to take the author’s word for any-
thing. This rule applics to Bible commentaries. The author
should be expecied to spell out in detail both his reasoning and
his evidence; conclusions alone are not sufficient. Only if a
commentator expects nothing in a biblical text to be applicable
in the real world should he expect his readers to trust him.

The problem is not simply that the reader has been asked
previously to trust the commentators. He has also been told to
distrust the Mosaic law. First, dispensational commentators have
argued that the Mosaic law is in a kind of suspended animation
until Jesus returns in person to establish His earthly millennial
kingdom. This exclusion includes even the Ten Command-
ments."! Second, higher critics of the Bible for over two centu-
ries have argued that the Pentateuch is unreliable judicially
because Moses did not really write it; instead, lots of anony-
mous authors wrote it. Third, Protestant theologians for almost
five centuries have denied that the Old Covenant provides
moral and judicial standards for personal and corporate sanc-
tification. Fourth, Roman Catholic and Eastern Orthodox theo-
logians for a millennium and a half have substituted the legal
categories of Greek philosophy, either Platonic (belore the
cleventh century) or Aristotelian (after the eleventh century in
the West), for Old Testament law. Thus, Christians have been
told for almost two millennia: “Don’t trust the Mosaic law!” So,
most Christians do not trust it. Most Christian leaders not only
do not trust it; they hate it. They are outraged by it. ‘The Mosa-
ic law is an insult to their sense of justice. ‘They are relieved to

LL. S. Lewis Johnson, “The Paralysis of Legalism,” Bibliotheca Sacra, Vol. 116
(Aprilffunc 1963).
