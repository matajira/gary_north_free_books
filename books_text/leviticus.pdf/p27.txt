Foreword XXVii

revolt and their liberators to action.” This is a true statement
regarding the history of slavery. How can a Bible-believing
Christian explain what seems to be the Mosaic law’s moral blind
spot on the question of slavery (Ley. 25:44-46)? Yet he knows
there is something wrong with this statement:

And the same Bible that on the basis of an archaic social code of
ancient Israel and a tortured reading of Paul is used to condemn all
homosexuals and homosexual behavior includes metaphors of redemp-
tion, renewal, inclusion and love — principles that invite homosexuals to
accept their freedom and responsibility in Christ and demands that
their fellow Christians accept them as well.

What can the typical evangelical say in response? Le, too,
believes that Leviticus promoted “an archaic social code.” It also
established laws that seem to have been annulled, such as the
laws of separating seeds in the same field or avoiding clothing
made of both wool and linen. Is the social code of Leviticus
inextricably ticd to such laws of separation? If so, how can this
social code be honored today? If not, how can we separate the
still-valid social code from the annulled laws? Gomes puts it
well: “The questions are, By what principle of interpretation do
we proceed, and by what means do we reconcile ‘what it meant
then’ to ‘what it mcans now?” Here he is on target. These are
the two absolutely fundamental questions of biblical interpreta-
tion (hermeneutics) that I have sought to answer in this com-
mentary and in my previous commentaries. These are the two
questions that deliberately have been left unanswered by Protes-
tant commentators on the Old ‘[estament ever since the Refor-
mation. It is time to begin answering both of them.

Let me remind my Bible-affirming readers that these two
questions are not inicllectual curiosities proposed by academic
theologians. If Christians cannot find answers to both of them,
then they had better learn to live with (and perhaps dic with)
