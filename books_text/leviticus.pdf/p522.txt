466 LEVITICUS: AN ECONOMIC COMMENTARY

Who would have chosen to live in a walled city in the cra of
the conquest? Any Israelite family would have had the right to
participate in the distribution of rural land. This would have
been that family’s permanent inheritance. The urban residents
would then have been made up of the following: 1) land-own-
ing Israelites who became absentee landlords; 2) permanent
resident aliens who had been adopted into the tribe of a city; 3)
permanent resident aliens who had not been adopted by an
Israelite family or tribe; 4) traders who would reside there
relatively briefly; 5) Levites who were not residents of a Leviti-
cal city; 6) soldiers or other officials from the central govern-
ment; 7) Israelites who had been excommunicated (i.c., circum-
cised strangers: nokree}; 8) convicted Israelite criminals who had
been sold into servitude to someone in a walled city.*

The Terms of Sale

The text does not speak of a deferred payment, i.c., a mort-
gage beyond onc ycar. The right of redemption was one year.
There is no indication that this means anything except one year
from the time that the transfer of ownership took place. Owner-
ship transfers with responsibility over the property. Ownership
is a judicial concept: the identification of the legally responsible
agent. The owner has the right to disown the property.

8. The Bible docs not say whether convicted criminals were part of the jubilee
land law’s primary benefit: a judicial return to the family’s land; ic, liberation from
bondage. This would have meant freedom for all criminals in the jubilee year. This,
in turn, would have created a subsidy to crime as the jubilee year approached: a
conviction would not have led to a high price for his sale into bondage, since the
time of potential servitude was steadily shrinking. The victims would have been
short-changed. Because God defends the victim, it seems safe to conclude that there
were two exceptions to the jubilee law of liberation: the apostate who had forfeited
his inheritance and the criminal who was still under the requirement to pay off his
victims or the person who bought him, with the purchase price going to the vietins.
‘This conclusion follows from two general principles of biblical law: 1) God does not
subsidize evil; 2) victim's rights. IF this is correct, then the criminal who was released
from bondage would have had to wait until the next jubilee year to reclaim his land.

 
