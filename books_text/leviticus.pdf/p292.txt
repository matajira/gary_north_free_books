14

IMPARTIAL JUSTICE VS.
SOCIALIST ECONOMICS

% shall do no unrighteousness in judgment; thou shalt not respect the
person of the poor, nor honour the person of the mighty: but in righteous-
ness shalt thou judge thy neighbour (Lev. 19:15).

The theocentric meaning of this law is that the State is to
imitate God by doing what God does: judge all people without
respect to their persons, i.e., their class, status, or power.

This law is one of the two most important laws in the Bible
that deal with civil government. The other verse is Exodus
12:49, which insists that civil judgment in the land of the cove-
nant must apply to all men equally, whether strangers or Isracl-
ites: “One law shall be to him that is homeborn, and unto the
stranger that sojourneth among you.” Fxodus 12:49 confirms
the judicially binding nature of the civil law of God: biblical
civil laws are to be applied equally to ail people residing within
the geographical boundaries of a biblically covenanted society.
The same civil laws are to be applied to everyone residing in
the land, regardless of race, color, creed, or national origin.’

1. Gary North, Moses and Pharaoh: Dominion Religion vs. Power Religion Tyler,
“fexas: Institute for Christian Economics, 1985), ch. 14: “The Rule of Law”
