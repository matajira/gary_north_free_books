Wine as a Boundary Marker 155

outside the tabernacle. God dwelt with Israel inside the taber-
nacle. His presence was judicial: throne-related (the mercy seat:
Ex, 25:17-22). Any declaration of His law from within His own
house had the force of supreme law. The law declared here was
not mere advice. It could not be appealed. This was Israel's
highest ecclesiastical court of appeal. The priest was acting as a
boundary guard on holy ground. This was the boundary. It was not
simply that his office was holy; his environment was holy. Jesus
did not apply a whip to the backsides of the moneychangers
outside the temple, but only inside. 11 was here that God was
most offended. The temple was a house of prayer, the place
where men brought their cases before God and sought God’s
authoritative pronouncements.

The Third Book of the Pentateuch

This interpretation is consistent with the structure and role
of the Book of Leviticus: boundarics. The priest within the
tabernacle was a student of the law as a beundary guard for the
people in their role as God’s dominion agents. As God's dwell-
ing place, the tabernacle was the place of God's judgment. The
tabernacle was therefore sanctified - sct apart judicially by God.
When in the geographical-jadgmental presence of God in the
Mosaic Covenant era, the priest had to avoid anything that
would make him lightheaded, meaning artificially lighthearted.
The priest was also the one who offered sacrifices as a bound-
ary guard whose efforts placated the wrath of God. Offering
sacrifices was the crucial official activity within the tabernacle. If
the priest was not alert to the ritual requirements of the sacri-
fices, he risked bringing under judgment both himself and
those represented by him.

There was a secondary consideration. If the priesthood as a
whole failed to declare and observe God’s law correctly, this
would undermine all lawful judgment: self-judgment, family
judgment, civil judgment, and ecclesiastical judgment. This
would in turn undermine the dominion activities of the family,

 

 

 
