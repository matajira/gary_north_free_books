Introduction to Part 5 387

four decades before the conquest began. The return of rural
land to the heirs of these original families every 50 years was
not statist wealth redistribution; rather, it was the judicial de-
fense of original title: a defense of private property.

The Meaning of the Jubilee

The Mosaic law guaranteed that the Israelites would multi-
ply if they obeyed God’s law: longer life spans (Ex. 20:12) and
zero miscarriages (Ex. 23:26). But a multiplying population
leads to ever-shrinking land holdings. As time passed and the
population grew, each family plot in Israel would shrink to the
point of near-invisibility. Given the fact that the average family
allotment at the time of the conquest was under cleven acres, a
population that doubled every quarter century (3 percent
growth per annum) could not remain an agricultural society for
very long. Every 24 years, the average family’s share of the
farm would shrink by half. The average allotment would have
been down to a little over an acre within a century with a popu-
lation growth rate of 3 percent a year.

The jubilce law had nothing to do with the equalization of
property except in the peculiar sense of eventually producing
plot sizes so tiny that the value of any given family’s landed
inheritance was so small that it really did not make any differ-
ence. In today’s world, an inheritance worth two dollars is twice
as large as an inheritance worth one dollar, but in terms of
what either inheritance will buy, the percentage difference
between them really docs not matier.

Then what was the jubilee law all about? First, it was a law
that decentralized politics: every heir of a family of the con-
quest could identify his citizenship in a particular tribe because
every fumily had an inheritance in the land. Ownership stayed
inside the tribes. Second, it restricted the accumulation of rural
land holdings by the Levites, who could never buy up the land.
This geographically dispersed urban tribe would remain dis-
persed. Third, it kept the State from extending its land hold-
