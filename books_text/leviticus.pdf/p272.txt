216 LEVITICUS: AN EGONOMIC COMMENTARY

We are used to the idea that an item that costs five times more
than another item is probably of higher quality. We believe that
the product will not break readily, and therefore our time
won't have to be spent taking the thing in to be repaired.
Brand names enable us to make better predictions about the
performance of both services and goods.

This is an aspect of the third point of the biblical covenant.
God protects His name from profanation. In a similar way, we
protect our own names from misuse. We have a property right
lo our names: other people are not allowed to’use our names to
promote their ends without our agreement. This is why the
existence of brand names and the legal right to property estab-
lished in a brand name are so important in a socicty, in order
to reduce the uncertainty of the future. People can make deci-
sions based on price and name with respect to the quality of the
good or the service.

Contracts

Contracts are a crucial part of this system of economic inter-
dependence. God’s goal is greater cooperation among men and
a reduction of coercion in economic affairs: peaceful exchange.
Peace on earth is a biblical goal. Contracts enable people to
specify their own performance more precisely. At least when all
parties understand the terms of the contract, contracts reduce
the cost of cooperation, and hence increase the quantity of
cooperation demanded.

There are always inherent limitations on contracts. One
limitation is the difficulty of specifying the conditions of perfor-
mance. This is why, as societies become more complex, con-
tracts tend to grow longer and in ever-smaller print. Lawyers
are the ones today who speak to cach other about the nature of
the conditions; the actual participants in the contract are rarely
able to understand. This has created a new pricsthood of law-
yers. They speak to each other, and their supposed employers
