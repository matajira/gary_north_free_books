672 LEVITICUS: AN ECONOMIG COMMENTARY

Amish, 2, 276, 334
Ammon, 325
Ammonites, 470
amputated warriors, 170
Anabaptists, 275-76, 334
anarchism, 272-74 (also see
warlordism)
angels, 560, 565-67
animal sacrifice
blemish-free, 50-52, 53-54,
60
civil court &, 140
corporate, 357
death of sacrificer, 49
not divisible, 115-16, 129
preligured Christ, 141
animal skins, 47
animals, 108
animism, 54n
antinomianism, 2, 5, 275
applied theology, xii
Arabs, 405
Aristotle, xxvi, 95, 481
Ark of the Covenant, 43, 462,
623
army
adulthood, 504, 585
aliens in, 468 (also see
Uriah)
anointing and re-entry,
170
citizenship, 389, 391, 450n,
468, 470-72, 495, 497,
504, 518, 525
defends land’s boundaries,
504
leprosy, 170
post-exilic, 470

royal priesthood, 463
Uriah the Hittite, 472, 505
walking, 14
Articles of Confederation, 273n.
ascension, 131-32, 154
assault, 231, 235, 373, 379
assembly, 91-92
assent, 87, 90, 94
Assyria, 184, 185, 188, 469
atheists, 142
Athens, 259
atomism, 100-1
atonement
corporate, 101
day of, 409-12
jubilee &, 429
legal act, 50
leprosy &, 169
Pentecost, 356
restitution &, 137-41
substitution, 53-54
audience, xv
Augustine, 526
autarky, 221
authority
assent, 87, 90, 94
bottom-up, 95, 99, 221
Canaan’s land, 182
congregation, 101-2
costs of stewardship, 204
decentralized, 113
delegated, 94, 113, 219,
221, 648
democratic, 95, 100-1
ecclesiastical, 100-1, 589-90
{also see excommunica-
tion)

family, 99
