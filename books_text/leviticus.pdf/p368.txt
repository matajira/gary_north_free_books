312 LEVITICUS: AN EGONOMIG COMMENTARY

both prohibit evil public actions; both are justified in terms of
the Israelites’ experience in Egypt.

The State as Savior

The State is authorized by God to impose only negative
sanctions. ‘The modern welfare State ignores this restriction. It
altempts to benefit some groups at the expense of others. It has
become the Savior State, a plundering State. It seeks to act
messianically: a healer in history. It secks to make men good
rather than to restrict its activities to preventing designated evil
acts.

Corporate Sanctions

The State imposes negative physical sanctions as God’s dele-
gated agent in history. If Israelite magistrates failed in this task
with respect to individual Jaw-breakers, God would raise up
other agents of His justice Lo impose negative sanctions on the
whole society. For example, when Judah refused to honor the
sabbatical year of rest for the land, God raised up Babylon -
strangers — to carry His people into captivity, so that the land
would receive its long-awaited lawful rest. God’s law had speci-
fied this as the appropriate negative sanction (Lev. 26:32-35;
fulfilled in II Chron. 36:17, 21).

The biblical justification for the State’s imposition of negative
sanctions against individual law-breakers is God’s threat to
imposc negative corporate sanctions against the entire socicty if
His Bible-revealed civil law is not enforced by civil magistrates.
This is the distinctive principle of biblical civil government. 1
keep returning to this theme because it is central to biblical
political economy. God's negative historical sanctions wil! be
applied. The question is: By God or by the civil magistrates?

6. Gary North, Boundaries and Dominion: The Economics of Leviticus (computer
edition; Tyler, Texas: Institute for Christian Economics, 1994), ch. 19, section on
“Negative Sanctions: The State as Intermediary”
