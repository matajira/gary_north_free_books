XXXIV LEVITIGUS: AN ECONOMIC COMMENTARY

that fact, it seems proper to appeal to it as indicating God’s
attitude toward any kind of harm to the unborn, including
abortion. Since nothing in the NT suggests that God’s attitude
toward the unborn has changed, the OT passage is relevant for
determining God's attitude toward the unborn and for de-
manding protection of them.”*

They know not what they say. They began this section of
their book by rejecting Bahnsen’s statement of the theonomic
position, namely, “that unless Scripture shows change with
respect to OT law, NT era believers should assume it is still in
force.”*= Bahnsen does indeed teach this; this is his theonomic
hermeneutic: the presumption of judicial continuity, Yet they
defend their appeal to a Mosaic case law on this same basis:
“Since nothing in the NT suggests that God’s attitude toward
the unborn has changed, the OT passage is relevant for deter-
mining God’s attitude toward the unborn and for demanding
protection of them.” That is to say, they adopt Baknsen’s hermeneu-
tic as the only one that can deliver them, in the name of the Bible, into
the camp of the pro-life movement.

In July, 1970, over two years before the U.S. Supreme Court
handed down the Roe v. Wade decision, which legalized abor-
tions on demand, Rushdoony challenged Christians to return to
Exodus 21:22-25 as the judicial basis of their opposition to
abortion. Anything less, he warned, has led in the past to com-
promises with paganism on this question. He wrote:

Among the earliest battle-lines between the early Christians and the
Roman Empire was the matter of abortion. Greck and Roman laws had
at times forbidden abortion, even as they had also permitted it. The
matter was regarded by these pagan cultures as a question of state
policy: if the state wanted bixths, abortion was a crime against the state;
if the state had no desire for the birth of certain children, abortion was
either permissible or even required. Because the slate represented

34, Feinbergs, Ethics, p. 39.
35. Bid. p. 34,
