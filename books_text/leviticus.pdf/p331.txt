‘The State’s Monopoly of Vengeance 275

law. Modern Christianity is politically polytheistic.2! Rush-
doony is correct: “To hold, as the churches do, Roman Catho-
lic, Greek Orthodox, Lutheran, Calvinist, and all others virtual-
ly, that the law was good for Isracl, but that Christians and the
church are under grace without law, or under some higher,
newer law, is implicit polytheism.”” This antinomian outlook
turns over judicial authority to polytheistic humanist kingdoms
as surely as the pacifism of the Mennonite sccts causes them to
turn over the law-making power, police power, and military
authorily to others. Thus, modern Christians hait as biblically
valid the truncated court systems of modern nationalism. They
reject the ideal of Christendom on two accounts: its commit-
ment to universal Christian legal standards and its denial of
humanistic nationalism as anything more than a temporary
stopgap measure analogous to the scattering at the Tower of
Babel. ‘hey do not regard Babel’s scattering as God’s curse on
covenant-breakers’ confession of autonomy: to make themselves
a name. Rather, they see judicial Babel as inherent in the hu-
man condition, even if all men were to covenant with God.
Nevertheless, the creation of such a supreme judicial civil
court must nol precede the creation of a supreme ccclesiastical
court. The church is the model for the State, not the State for
the church. The church continues into cternity; the State does
not (Rev. 21; 22). No agency will then be needed to impose civil
sanctions: no sin! Conclusion: to begin to create a supreme civil
world court before creating the covenantal foundation of a free
world socicty - Christendom — is to altempt the creation of a
secular one-world order. It represents a return to the Tower.
The inherently international ideal of Christendom is denied
by right-wing judicial Anabaptists, but they cannot escape the
theoretical problem of social order. Traditionally, they have

21, Gary North, Political Palytheism: The Myth of Pluralism (Tyler, Texas: Institute
for Christian Economics, 1989).

22. Rushdoony, Institutes of Biblical Law, p. 18
