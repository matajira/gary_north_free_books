122 LEVITIGUS: AN EGONOMIG COMMENTARY

seen as common when not actually profane. This theological
confusion has led to the retreat of Christians from leadership in
the arts, industry, and most other fields.®

Under the Mosaic Covenant, an inadverient violation of
God's commands was settled by paying the victim whatever he
had lost as a result of the transgression. The ethical transgres-
sion covered by this law must have been a transgression of one
of God’s verbal boundaries; no human victim is identified here.
God did not impose a 20 percent payment in addition to the
sacrifice of a ram for the violation of a commandment (Lev.
5:17-18). But when someone violated a sacred space or sacred
object, he violated God’s word (the law) as well as the actual
thing or space (Lev. 5:15-16). The transgression was a double
boundary violation: word and place. The penaliy was therefore
greater.

Sacred Boundaries

There is so much confusion over the relationship between
the sacred and the common that interpreters have tended to
misrepresent the relationship. They have confused the common with
the profane. This false interpretation has. undermined Christian
social and cthical theory whenever it has appeared. It makes
the common appear as if il were a realm “naturally” opposed to
grace and ultimately beyond grace - legitimately so in history.
This places a boundary around grace. The interpreters have not
understood that every created thing begins as common and
remains common unless judicially sanctified: actively sct apart
by God or His law. Nothing begins as profane; it must become
profane, just as something becomes sanctificd. This may seem
like a minor point, but it is not, as we shall sce.

The sacred here refers to the sacramental, i.c., having to do
with the twin covenantal signs of ecclesiastical subordination: in

6. itid., ch. 6, section on “Full-Time Christian Service.”
