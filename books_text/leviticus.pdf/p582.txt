526 LEVITICUS: AN ECONOMIC GOMMENTARY

ROK OR ok ak

Servitude exists because sin exists and because God's judg-
ments in history and eternity also exist. This was Augustine’s
argument a millennium and a half ago, an argument that was
old when he offered it: slavery is one of God’s penal sanctions
against sin.” Richard Baxter warned slave owners in 1673: “If
their sin have enslaved them to you, yet Nature made them
your equals.”"*

Covenant theology teaches that slavery is an inescapable
concept. Slavery’s positive model is the indentured servant who
buys his way out of poverty, or who is released in the sabbatical
year or jubilee year. He learns the skills and worldview of do-
minion. IIe becomes self-governed under God, a free man.
Slavery becomes a means of liberation when coupled with bibli-
cal ethics. The fundamental issue, as always, is ethical rather
than economic. His ability to buy his way out is indicative of a
change in his ethical behavior.

Slavery’s negative model is God’s judgment of covenant-
breakers throughout eternity. He consigns them first to hell
and then, at the resurrection, to the lake of fire (Rev. 20:14-15).
God places people on the whipping block, and then He flogs
them forever. Of course, what they actually experience for
eternity is far more horrifying than the comparatively minor
inconvenience of an eternal whip. 1 am only speaking figura-
tively of whips; the reality of eternal torment is far, far worse
than mere lashes. Thus, the legal right of some people to en-
slave others under the limits imposed by God’s revealed law is
based on the ultimate legal right of God to impose eternal
torment on covenant-breakers. Biblical servitude is a warning to
sinners as well as a means of liberation.

12, Augustine, City of God, Book 19, Chap. 15. Cf. R, W. Carlyle and A. J.
Carlyle, A [History of Mediaeval Political Theory in the West, 6 vols. (2nd ed.; London:
Blackwood, [1927] 1962), I, p. 115.

18. Richard Baxter, A Christian Directory (London: Robert White for Nevil Sim-
mons, 1678), Part IT, Christian Occonomicks, p.'74. The first edition appeared in 1673.
