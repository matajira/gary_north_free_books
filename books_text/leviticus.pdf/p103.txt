Introduction to Part 1 47

Fifth, there was the trespass or guilt offering. The pricst kept
the skin of the animal (Lev. 7:9). Animal skins were also God’s
gift to Adam and Eve just before they were cast out of the
garden (Gen. 3:21). These skins were the coverings that would
preserve them: a testimony to God’s grace to them by providing
a future.

Atonement for Sin, Not Food for God

Milgrom points out that all the food sacrifices were to be
performed where laymen could view them: on the outer altar
in the open courtyard.’° This courtyard was open to all Israel-
ites.'! These sacrifices were public acts. Speaking of the altar
of incensc, which was inside the tent or tabernacle, God said:
“Ye shall offer no strange incense thereon, nor burnt sacrifice,
nor meat offering; neither shall ye pour drink offering thereon”
(Ex. 30:9). The sacrifices were for the benefit of the nation.
They were not for “the care and feeding of God” — a funda-
mental error of Mesopotamian religion generally.”

The sacrifices atoned for men’s sins. This also meant cleans-
ing. “For on that day shall the priest make an atonement for
you, to cleanse you, that ye may be clean from all your sins
before the Lorp” (Lev. 16:30). By appcasing God through
sacrifice, the nation was cnabled to escape God’s wrath in histo-
ry. But the fundamental sacrifice is always ethical: avoiding sin
after payment to God has been made. That is to say, the es-
sence of acceptable sacrifice is ethical holiness, just as the judi-
cial foundation of holiness is sacrifice.

The Book of Leviticus is the book of holiness, the book of
sacrifices, and the book of property. That is to say, Leviticus is

10. Jacob Milgrom, Leviticus {-16, vol. 3 of The Ancher Bible (New York: Double-
day, 1991), p. 39.

11. fbid., p. 148, He provides a suggested sketch of the outer court, which was
separate from, but contiguous to, the tent and the inner court: p. 135.

12. fdem,
