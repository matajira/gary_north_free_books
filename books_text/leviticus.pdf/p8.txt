Part 4: Covenantal Acts

 

  

Introduction to Part 4.2... eee 353
22. Mutual Self-Interest: Priests and Gleaners oo. 355
23. Blasphemy and Civil Rights ............0.0.. 368
Part 5: Inheritance

Introduction to Part 5 6... ee eee 385
24. The Sabbatical Year .... 06... 0 cee eee 393
25. Boundaries of the Jubilee Land Laws .......... 408
26, Economic Oppression by Means of the State ..... 434
27. Food Miracles and Covenantal Predictability ..... 448
28. The Right of Redemption ......... 0000000005 462
29, Poverty and Usury ...... 60.00. 0 eee eee 480
30. Promise, Adoption, and Liberty ............... 492
31. Slaves and Freemen ......... 0.000 c ee eeeaee 515
32. Mandatory Redemption Upon Payment ......... 529
33, Nature As a Sanctioning Agent .............6. 541
34, Limits to Growth oo... 00 cee eee eee 556
35. God's Escalating Wrath ........... 000.0000 571
86. The Priesthood: Barriers to Entry ............. 580
37. The Redemption-Price System .........-..-005 594
38. Tithing: The Benefit of the Doubt ............. 619
Gonelusion 6.6... eee eee 625
Scripture Index 26... eee eee 656
Index... et ete 669

About the Author . 6... 6... eee eee eee 733
