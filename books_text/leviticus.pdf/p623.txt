Limits to Growth 567

ed. Thus, Satan benefits from a growing population only inso-
far as he can keep them under his covenant and entrap them
in hell. The threat of their rejection of his covenant grows ever-
greater over time: more humans to join God’s forces, and more
likelihood that God will send His promised days of blessing.”
This is why the zero population growth movement, like the
abortion movement, can be accurately described as satanic.

Israel’s Limits

The question for Israel was this: When these limits to popu-
lation growth were reached inside the nation’s geographical
boundaries, how did God expect the Israelites to overcome
these population limits? There were either geographical limits
or population limits. Walking to the feasts placed geographical
limits on Israel, but without limits on. Israel’s population, Is-
rael’s geographical limits would be breached. Gonclusion: God
mandated another exodus beyond the borders of Israel when
He established population expansion as His covenantal stan-
dard. he Israelites were expected to move outside of the geo-
graphical boundaries of Israel. This was the meaning of Christ’s
metaphor of new wine in old wineskins (Matt. 9:17): the fer-
menting new wine would burst its inflexible container. His
people were always intended to inherit the earth, not just the
land of Israel. “For evildoers shall be cut off: but those that wait
upon the Lorn, they shall inherit the earth” (Ps. 37:9). “But the
meek shall inherit the earth; and shall delight themselves in the
abundance of peace” (Ps. 37:11). “For such as be blessed of him
shall inherit the earth; and they that be cursed of him shall be
cut off” (Ps. 37:22),

Inheritance in Israel implied growth for obedient covenant-
keepers: growth in the number of heirs and growth in the
value of their individual mheritances. But geographical limits —

15. Kenneth L. Gentry, Jr., He Shall Have Dominion: A Postmillennial Eschatology
Gyler, Texas: Institute for Christian Economics, 1992).
