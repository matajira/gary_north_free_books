258 LEVITICUS; AN ECONOMIC COMMENTARY

by serving as a lower judge in the gates (II Sam. 15:2-6), But
his was a messianic impulse: “Absalom said moreover, Oh that
1 were made judge in the land, that every man which hath any
suit or cause might come unto me, and I would do him justice!”
(II Sam. 15:4), Ile promised justice to all.

To restrain this messianic impulse, the king was not allowed
by God to multiply horses (offensive weapons), wives (alliances),
or precious metals (Deut. 17:16-17). le was required to study
biblical law daily (Deut. 17:18-19). He had to be placed under
judicial and institutional] restraints in order to restrict the devel-
opment of a messianic impulse based on concentrated civil
authority. Legitimate authority was nol to become illegitimate power.
It is this move from multiple authoritics to a single authority —
from legitimate, decentralized social authority to centralized
State power — that is the essence of the move from freedom to
totalitarianism. Biblical law places boundaries around cen-
tralized political authority in order to prevent this development.

What, then, was the basis of a judge’s authority? We can
answer this best by asking: “Biblically, who declared the law in
ancient Israel?” The priests did. Yet this office was not limited
to ecclesiastical affairs. Israel was a kingdom of priests. “And ye
shall be unto me a kingdom of priests, and an holy nation.
These are the words which thou shalt speak unto the children
of Isracl” (Ex. 19:6). This was an ollice held by all adult circum-
cised males (age 20+)" and all adult women under the au-
thority of a circumcised male."

Civil Priests

There were both civil and ecclesiastical priests. ‘he elders in
the gates in ancient Israel were empowered by God to make the

14, Robert A. Nisbet, The Quest for Community (New York: Oxford University
Press, 1953), ch. 5.

15, Exodus 80:14.

16. The best example is Deborah (Jud. 4).
