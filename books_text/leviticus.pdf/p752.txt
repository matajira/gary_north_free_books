696 LEVITICUS: AN ECONOMIC COMMENTARY

inequality
equality before the law &,
241-42
jubilee, 433
justice & 245
legitimate, 239
productivity, 245
sanctions &, 244-47
two kinds, 241-43
infinity, 561
inflation, 488-489
information
economic coordination &,
213-15
judges’, 233
local courts, 251, 255-56,
not free, 230
oppression & 441-45,
Satan’s, 565-66
inheritance
Abraham, 340, 647
adoption, 390 (also see
adoption)
annulment, 638
atonement, 412
baptism, 290
Caleb, 303
changes, 280-81
Christ, 84, 292, 521-22
(also see Shiloh)
citizenship, 288, 519, 522
collateral, 494-95
conquest, 396, 413
daughters, 17, 283
Deuteronomy, xlviii
dietary laws, 345
dilution of, 507, 509, 511
eschatological, 568

eunuch, 283-84

excommunication &, 389,
522

execution &, 328

exile, 407

family, 283-84

fire, 324

genocide, 414-15, 463

gift of God, 340, 358, 396

gleaning, 205, 359-60

God’s people, 567

Holy Spirit &, 291

Israel's, 291-92

Israel’s geography, 568

jubilee, 506

Judicial status, 283

kinsman-redeemer, 389

Jand, 358-59 (also see jubi-
lee)

Moloch worship, 328

new tribe, 58]

obedience, 340, 347, 567-68

plots shrink (see plots)

predictable, 553

priesthood, 280, 359

priests, 162, 359, 643

promise, 290

reparation offering, liv

rest, 415

revocation, 647

sacrifice &, 120

sanctuary, 494-95

Seed, 281, 283, 638

separation &, 282-83,
337-49

shrinking, 19-20, 568

spoils of war, 412-15

tribal, 17
