370 LEVITICUS: AN ECONOMIC COMMENTARY

God’s name. This was an active assault, which was not tolerated
in Israel. It was a capital crime.

Penalty

God’s required civil sanction against blasphemy is clear from
His answer to Moses’ inquiry: “Bring forth him that hath
cursed without [outside] the camp; and Ict all that heard him
lay their hands upon his head, and let all the congregation
stone him” (Lev. 10:14) This verbal assault had been an act of
rebellion, of treason. It had been a public challenge to the
legitimacy of the social order. ‘The legitimacy of the covenanted
social order had to be maintained by the State. The proper civil
sanction, God insisted, was public stoning: the corporate exccu-
tion of the blasphemer.

No society in the West enforces this statute in modern times,
or even thinks about it. No society in the West requircs witness-
es to participate in the public stoning of those who have com-
mitted a capital crime. This biblically mandated sanction has
never been taken seriously by church or State in New Covenant
times. Indeed, even to suggest that stoning is a legitimate form
of execution, let alone required by God, is to risk bringing
down the negative sanction of public ridicule on the head of
the person who suggests this, and ridicule by ‘\rinitarian theo-
logians at that.? The modern world, including the modern
Christian world, takes offense against the very idea of negative
sanctions imposed by God, whether Mosaic, historical, or eter-
nal.* Nevertheless, the fundamental issue of blasphemy is the

3. In their altempt to establish a dispensational case against capital punishment
for any crime except murder, H. Wayne House and Thomas D. Ice refer to my five-
point defense of public stoning as Gad’s specified means of execution. They seem to
believe (perhaps correctly) Unat this will seal their case in modern fundamentalist
circles against the theonomists’ defense of the Mosaic penal sanctions. House and Ice,
Dominion Theology: Blessing or Curse? (Portland, Oregon: Multnornah Press, 1988), p.
73.

4. At a meeting in May, 1989, a majority of a group of 385 neo-evangelical
theologians of the National Association of Fvangclicals voted against the doctrine of

 
