NOTE TO THE READER

You have in your hands a drastically stripped-down version
ofa far longer book: Boundaries and Dominion: The Economics of
Leviticus. As of mid-1994, the manuscript was over 2,300 dou-
ble-spaced pages. The typesetting costs would have been either
$11,500 or three days of work for me.’ To have printed 2,000
stitched hardback copies would have cost about $20,000. Add to
this a minimum of 150 hours of my indexing time, a price I
preferred not to pay. The price tag for the book would have
been at least $50. What reader is going to invest $50 plus, say,
80 hours to read an economic commentary on the Book of
Leviticus? What book stores would carry such a book? Few.

The day after I completed the manuscript, after having
invested some four years (2,000+ hours) of work, I decided not
to publish it in the traditional format, at least not before I
published this shorter commentary. I have therefore painfully
removed well over half of the text of the 38 chapters of Bound-
aries and Dominion. I have not reprinted its 11 appendixes.
Readers who are really serious about Leviticus can and should
consult Boundaries and Dominion.

But how? Easily! Today, as a result of the ever-multiplying
wonders of computer technology, Boundaries and Dominion is
available on a pair of 3.5-inch plastic disks. Eventually, it will be

1. 1 typeset all the ICE books with a remarkable semi-automatic macro that
Ruben Alvarado wrote for my WordPerfect 5.1 program.
