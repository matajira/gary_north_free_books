The Preservation of the Seed 287

same type. So, this law commanded what the economy would
have required anyway: separation. J would have applied only to
owners who had begun programs of experimental breeding to produce
@ separate breed.

The seed of each breed had to be separated. To obey this
law as it applicd to “non-mongrels,” an Israelite would have
had to construct a holding area or pen for each specialized
breed. This means that a specific seed or line was associated
with a specific place at any point in time. Owners could lawfully
move animals to new locations, but there was always to be a
geographical boundary associated with cach breed (seed). This
boundary established a connection between land and seed. This
connection was mandatory for both man and beast.

Crops

[he same principle of separation ~ prohibition of genetic
experimentation — applied also to crops. ‘The law stipulated,
“thou shalt not sow thy field with mingled seed.” This means
that a specific field had to be devoted to a specific crop at any
given point in the growing season. Like the pens for animals,
the seeds of the crop had to reside in a particular place. Seed
and Jand had to be linked.

What about genetic experimentation? The same prohibition
applied. There could be no lawful, systematic mixing of seeds.
An Israelite was not to apply his ingenuity to the creation of
new species of plants. Hybrid animals and seeds were illegal to
develop. They could be purchased from abroad, but since most
hybrids are either sterile (e.g., mules) or else they produce
weak offspring, there was little economic incentive to import
hybrids except as a one-generation consumer good. Such im-
ports were legal: with no “inheritance” possible, there was no symbolic
threat from hybrids. A hybrid was not prohibited because of its
status as a hybrid. It was illegal to produce them deliberately
because of the prohibition against mixing seeds, which was
