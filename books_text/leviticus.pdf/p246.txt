190 LEVILIGUS: AN ECONOMIG COMMENTARY

through (Gen. 13:15-17). Vhus, it is fruitless to search the his-
torical records of earthquakes in covenant-keeping nations and
covenant-breaking nations in the expectation that a predictable
pattern will be discovered.®

If I am incorrect about this, then the land still mediates
between God and man. We do have such a case in the Old
Testament: Cain’s curse. “When thou tillest the ground, it shall
not henceforth yield unto thee her strength; a fugitive and a
vagabond shalt thou be in the carth” (Gen. 4:12), This was a
sanction against Cain, whose brother’s blood had penctrated
the land and testified against Cain (Gen, 4:10). The promised
sanction was not active but rather negative: the absence of
positive sanctions. The curse in Genesis 4 was agricultural: the
land would no longer yield its fruit to Cain. So, Cain built a city
(Gen. 4:17), He had been a tiller of the soil; he became a resi-
dent of a city. He was not threatened with an carthquake; he
was threatened with personal famine. He avoided personal
famine by building a city and becoming a trader or other non-
agricultural producer He escaped the curse of the ground by
(presumably) switching occupations — he would no longer be a
shepherd or a cattle drover — and by changing his residency:
rural to urban.

Cain’s curse did not speak of earthquakes: the active stone-
casting that the land later brought against Israel at the close of
the Old Covenant order in A.D. 70. While I believe that God
will reveal to covenant-keeping societics techniques that mini-
mize the effects of earthquakes, I do not believe that He will
predictably alter their number and intensity in relation to the
degrce of the societies’ obedience to His law.

9. One carthquake that struck a sin center was the 1994 southern California
earthquake, which centered in the Canoga Park-Chatsworth area. ‘Uhis is the center
for pornographic movie production in the U.S. Models wh appear in such movies
temporarily became less enthusiastic about their work, according to one agent for
these performers. “It’s put the fear of God into them.” Christianity Teday (March 7,
1994), p. 57.
