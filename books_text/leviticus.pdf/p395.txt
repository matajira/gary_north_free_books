Inheritance Through Separation 339

The Canaanite nations are spoken of here in the singular, as a
single culture: “ye shall not walk in the manners of the nation.”
According to the definition in Strong’s Concordance, the Hebrew
word translated here as “nation,” commonly transliterated as
goy (more accurately, go’ee), is apparently derived from the same
root as the Hebrew word for massing: “a foreign nation; hence
a Gentile; also (fig.) a troop of animals, or a flight of locusts:
Gentile, heathen, nation, people.” It is the most commonly used
Hebrew word for “nation” in the Old Testament. The Ganaan-
ites had served as God’s stewards over the land for generations;
their rebellion had now come to fruition.®

Sustaining Grace

The Promised Land was already a land flowing with milk
and honey when the Israclites arrived. This matcrial wealth
had been set aside by God in Abraham’s day as His gift to
Abraham’s heirs. The land contained raw materials of great
value: original capital (Deut. 8:7-9). Furthermore, it contained
secondary capital: marketable wealth which was the product of
other men’s thrift and vision over scveral generations (Deut.
6:10-11). This combined capital value — land plus labor — could
be maintained intact long-term orily by obeying God (Deut.
6:12-15).

The capitalized value of the land was part of God's promise
to Abraham. It was therefore not earned by the Israelites. It
was an unmerited gift: the biblical definition of grace.* But once
delivered into the hands of Abraham’s heirs, possession of the
land could be maintained only by national covenantal faithful-
ness, as manifested by the Israelites’ outward obedicnce to

3, Gary North, Boundaries and Dominion: The Economics of Leviticus (computer
edition; Tyler, Texas: Institute for Christian Economics, 1994), ch. 21, subsection on
“Removing the Evil Stewards.”

4. Gary North, Dominion and Common Grace: The Biblical Basis of Progress (Tyler,
‘Texas: Jnstitute for Christian Economics, 1987), p. 4. By unmerited, L mean unmerit-
ed by the recipient. It was mcriled by Josus Christ.
