The Priesthood: Barriers to Entry 585

was no option for an Israclite father to buy back his daughter
from her pricstly husband by returning the bride price to his
son-in-law.® Similarly, there was no way for a man to buy back
himself, his wife, or his children from formally devoted service
to God. In short, there was no redemption price for this kind
of vow. This is why the vow was pawlaw: “singular.”

‘There is no indication that a man could place his adult male
children into mandated priestly service. An adult son was not
eligible for compulsory adoption. He was a lawful heir to the
land and the legal status of his tribe and family. He could not
be disinherited at his father’s prerogative. The crucial legal
issue for identifying adulthood for men was military number-
ing. An adult male was eligible to be numbered at age 20 to
fight in a holy war: “This they shall give, every one that passeth
among them that are numbered, half a shekel after the shekel
of the sanctuary: (a shekel is twenty gerahs:) an half shekel shall
be the offering of the Lorp” (Ex. 30:13). At age 20, a man came
under the threat of God’s negative sanctions: going into battle
without first having paid blood money to the temple.” Once he
became judicially eligible for numbering as a member of his
tribe, he became judicially responsible for his own vows. He
became, as we say, “his own man.” He became a member of
God's holy army, A father could no longer act in the son’s
name.

A daughter could not legally be numbered for service in
God’s army. Thus, an unmarricd daughter could be delivered
into a priestly [amily, as we see in the case of Jephthah’s daugh-
ter (Jud. 11:34-39).° Jephthah’s vow to sacrifice the first thing
to come out of his house could not legally be applied literally to

for Christian Economics, 1990}, pp. 218-19.

6. The dowry remained with the wile in any case; it was her protection, her
inheritance from her father.

7. Iid., ch, 32: “flood Money, Not Head ‘lax.”

8. Laccept the standard interpretation of this story: she was not literally executed
by her father

 
