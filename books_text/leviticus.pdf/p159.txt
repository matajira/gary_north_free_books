Corporate Responsibility 103

faithfulness of the politicians. (If God’s blessings on society
hinged primarily on the covenantal faithfulness of politicians,
all would have been lost by Nimrod’s day.)

The Centrality of the Church”

Christians are required by God to affirm the social centrality
of the church. This presupposition must govern Christian social
theory. The New Covenant church is the fulfillment of the
promise of God to establish a kingdom of priests. “But ye are a
chosen generation, a royal priesthood, an holy nation, a pecu-
liar people; that ye should shew forth the praises of him who
hath called you out of darkness into his marvellous light” (1 Pet.
2:9). In this sense, God regards the church as a nation. Jesus
prophesied to the leaders of Israel: “Therefore say I unto you,
The kingdom of God shall be taken from you, and given to a
nation bringing forth the fruits thereof” (Matt. 21:43). Like the
priests of Israel, the ordained priests of the new temple must
protect the assembled saints by not committing unintentional
sins. Similarly, the assembled saints must not commit uninten-
tional sins, in order to protect the society around them.

TI conclude: what is central to biblical social order is the preserua-
tion of Bible-based judicial sanctions inside the church. The church is
more important than the State. A socicty’s creeds are more
important than its civil constitution. The sacraments are
more important than the franchise. The tithe is more important
than taxcs. This is why combined taxes should not equal the
tithe (I Sam. 8:15, 17). Until the twentieth century, with its
messianic humanistic State and ils cndlcss, power-centralizing
wars,* taxes in the West were bclow 10 percent of net capital

43. Peter J. Leithart, The Kingdom and the Power: Rediscouering the Centrality of the
Chuvch (Phillipsburg, New Jersey: P&R Publishing, 1993).

44. R. J. Rushdoony, Foundations of Social Order: Studies in the Creeds and Councils
of the Early Church (Fairfax, Virginia: ‘Thoburn Press, {1968} 1978).

45. Robert Higgs, Crisis and Leviathan: Critical Episodés in the Growth of American
Government (New York: Oxford University Press, 1991}; Robert Nisbet, The Present
