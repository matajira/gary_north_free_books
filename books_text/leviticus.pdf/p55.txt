Preface lv

The Pentateuch is itself revelatory of the five-point structure
of God's covenant. My economic commentary on the Penta-
leuch is therefore a commentary on a covenant. I call it the
dominion covenant, for it is the God-given, God-required as-
signment to mankind to exercise dominion and subdue the
earth that delines mankind's task as the only creature who
images God the Creator (Gen. 1:26-28).
