Food Miracles and Covenantal Predictability 449

“Wherefore ye shall do my statutcs, and keep my judgments,
and do them; and ye shall dwell in the land in safety. And the
land shall yield her fruit, and ye shall eat your fill, and dwell
therein in safcty.” This is a repeated theme in the Bible. “But
they shall sit every man under his vine and under his fig tree;
and none shall make them afraid: for the mouth of the Lorp of
hosts hath spoken it” (Mic. 4:4).

Universal Benefits: Peace and Food

If this dual promise of peace and food were found only in
Leviticus 25, it could be discussed as an aspect of the jubilee
laws and therefore no longer in force. But the list of God’s
positive sanctions in Leviticus 26:3-15 indicates that this pair of
positive sanctions was not uniquely tied to the jubilee. ‘The
promise of peace and food is more general than the jubilee law,
since it refers to “my statutes” and “my judgments.” God refers
Israel back to His revealed Jaw-order. It is their covenantal
faithfulness to the stipulations of this law-order which alone
serves the basis of their external prosperity. Without obedience,
they can have no legitimate confidence in their earthly future
in the land. This law has a broad application. It undergirds the
observation by David: “I have becn young, and now am old; yet
have I not seen the righteous forsaken, nor his seed begging
bread” (Ps. 37:25). The link between obedicnce to God’s stat-
utes and eating is reflected in David's observation: righteous-
ness and the absence of begging.

Why is this passage found in the jubilec statutes? Because of
the unique place of both the land and the harvest in the jubilee
laws, Preceding this section are laws that deal with the transfer
of a family’s land to the heirs: the return — legally, though not
necessarily physically - of each man to his father’s land (v. 13).
This was a testament of liberation. Because of this law, there could
be no permanent legal enslavement of Israelites inside the
