132 LEVITICUS: AX ECONOMIC COMMENTARY

Covenantal postmillennialism alone can confidently discuss the
doctrine of Christ's ascension, for postmillennialism does not
seek to confine the effects of Christ’s ascension to the realms of
the internal and the trans-historical.” ‘Vhat is to say, postmil-
lennialism does not assert the existence of supposedly inevitable
boundaries around the effects of grace in history. On the con-
trary, it asserts that ali such boundaries will be progressively
overcome in history, until on judgment day the very gates
(boundaries) of hell will noi be able to stand against the church
(Matt. 16:18).?%

It is now the task of Christians to work out progressively in
history the implications of what these delinitive transformations
have already accomplished judicially. Whatever God has declared
judicially, He requires to be manifested progressively. This dominion
assignment to His people involves extensive personal responsi-
bility, which is why dominion theology is resisted so adamantly
by pietists. But the church has been given a written Bible, the
Moly Spirit, and the division of labor (I Cor. 12) to enable
Christians to extend God’s dominion covenant. This historical
task is huge, but our tools are more than adequate.

Sadly, most Christians in my generation prefer intellectual
slumber and life in a cultural ghetto, living on “hand-me-
downs” from the world of humanism. They, too, have adopted
the false dualisms of humanism: sacred vs. profane, religious vs.
secular, nature vs. grace. They, too, have adopted the view that

22. This is why amillennialism drifts so easily into Barthianism: the history of
mankind for the amillennialist has no visible connection with the ascension of Jesus
Christ, Progressive sanctification in this view is limited to the personal and ecclesiasti-
cal; it is never cultural or civic. Lhe ascension of Christ has no transforming implica-
tions for society in amillennial theology. The ascension was both historical and
publicly visible; its implications supposedly are not. The Barthian is simply more
consistent than the amillennialist: tie denies the historicity of both Jesus’ ascension
and His subsequent grace to society. Christ’s ascension, like His grace, is relegated to
the trans-historical. North, Millennialism and Social TReary, pp. 111-13.

28, Kenneth L. Gentry, Jn, He Shall Have Dominion: A Postmillennial Eschatology
(Tyler, Texas: Institute for Christian Economics, 1992), chaps. 12, 13.

 

 
