Biblical Quarantine 175

threatens other individuals physically. If this threatening behav-
ior is breathing upon others, then the State must see to it that
the individuals who are a threat to others are not put into close
contact with those who might be injured as a resuit.

If the Statc in the Mosaic Covenant was not told by God to
support those who fell victim to diseases that mandated quaran-
tine, then there is no biblical case for the State as an agency of
tax-financed healing today. If the victim of leprosy in the Mosa-
ic Covenant was forced out of his home by the State, and made
to wander outside the city, and still the State was not responsi-
ble for his financial support, then the case for modern social-
ized medicme cannot be based on any biblical text.’° It must
be based on the argument from silence. It must be based on the
conclusion that there has been a fundamental change in the
function of civil government in the New Testament: from pro-
tector (Old Covenant) to healer, We have yet to see the exegeti-
cal case for such a change. While the presuppositions of the
twentieth century’s political order favor such a view of the State
—as did the presuppositions of the ancicnt pagan world — hu-
manist presuppositions are not a valid substitute for biblical
exegesis.

10. When I raised this argument in my debate with Ron Sider in the spring of
1981 at Gordon-Conwell Seminary, his rhetorical response was dever. He cried,
“Unciean, unclean!” He then admitted that he had never heard anything like this
before. But he made no attempt to answer my argument exegetically.
