Corporate Responsibility 105

Conclusion

The purification offerings linked ordained rulers to God’s
covenant people. The representatives of the people in both
church and State were bound to the people through the details
of God's law. There are no unacknowledged private sins on the
part of ordained rulers that do not threaten the safety of the
holy commonwealth. The corporate implications of private sins
were the reason why rulers had to offer public sacrifice for
their unintentional private transgressions of God’s law.

The institutional church in the Mosaic social order was basic
to the survival of that order. The church was also crucial for
the successful defense of liberty. The Statc possesses concentrat-
ed power; without the church’s unique power of the gospel, the
sacraments, and the threat of excommunication [rom the Lord's
Supper, neither the family nor the institutional church can
successfully resist the concentrated power of the modern State.
Men’s only reasonable hope in such a sanctions-free ccclesiasti-
cal world is in the collapse of the existing civil order because of
its own incompetence — again, a kind of self-inflicted (autono-
mous) judgment: the bureaucratic suicide of the cxisting
State.® But the problem still remains for reconstruction dur-
ing the post-collapse era: By what standard? Whose sanctions will
be enforced, God’s or scli-proclaimed autonomous man’s?

The political theorists of the Enlightenment’s right wing,
most notably John Locke, lodged ultimate sovereignty in the

 

tomb in order to keep the disciples from stealing His body and claiming that He had
risen from the dead (Mait. 27:62-66). Meanwhile, the disciples had scattered. The
covenant-breakers understood the specifics of Jcsus’ prophecy; the disciples did not.
This has been a continuing curse on the church from the beginning.

49. In Eastern (now Central) Europe in the final quarter of 1989, the collapse of
Communist rule was in part an act of cither treachery against Communism on. the
part of the ruler or else a highly risky deception of the West — Gorbachey, for
whatever reasons, refused to send in the tanks — and in part the prayerful work of
the national churches. In this revolt, the churches were recognized as the friends of
the people, not the allies of the rulers and the targets of the revolution’s rulers, as
had been the case in the French and Russian revolutions.
