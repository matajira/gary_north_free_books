Impartial Justice us. Socialist Economies 247

administration expenses at Icast half of the Federal govern-
ment's total expenditures on welfare programs.”

All of this is rejected by those Christian socialists and Keynes-
ians, who reject — necessarily — the idea of judicially binding
biblical blueprints in civil government and economics.”

Conclusion

Leviticus 19:15 establishes a fundamental principle of justice:
the impartial application of God’s {egal standards to all men,
irrespective of their wealth or status. It proclaims the judicial
principle of equality before the law. This biblical principle of
civil justice is the antithesis of all socialism, The socialist pro-
claims the need for the equality of economic results, not equali-
ty before the law. There is no way to achieve the former with-
out abandoning the latter, and vice versa. Logically, the socialist
has to deny the legitimacy of Leviticus 19:15; logically, the
defender of Leviticus 19:15 has to deny socialism. People are
not always logical, however. What we lind is that defenders of
Christian socialism either ignore the existence of Leviticus 19:15
or else reinterpret it to mean the opposite of what it says. They
interpret it, as Sider interprets it, to mean that the judge must
uphold the poor man in his cause. Upholding the poor man in
his cause is as great a sin as upholding the mighty in his cause.

The response of Christian socialists and welfare statists has
been to deny that the Bible offers biblical blueprints for eco-
nomics. Any appeal by a Christian economist to the Mosaic law
is rejected as illegitimate. ‘This has to be their response, since
the legal order of the Mosaic Covenant, if obeyed, would inevi-
tably produce a free market social order. Without Mosaic law,
however, it is not possible to say what kind of social and eco-

22. James L. Payne, The Culture of Spending: Why Congress Lives Beyond Our Means
(San Francisco: ICS Press, 1991), p. 51.

23. North, Boundaries and Dominion, ch. 14, section on “Lhe Rejection of Biblical
Blueprints.”
