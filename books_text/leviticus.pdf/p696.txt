640 LEVITICUS: AN ECONOMIC COMMENTARY

God’s original grant of leaseholds at the time of the conquest.
There is no agency of enforcement today. There has been no
national grant of land.”

The jubilee law (Lev. 25:8-13). This law applied only to na-
tional Israel. It was a law uniquely associated with Israel’s con-
quest of Canaan. It was in part a land law and in part a seed
law: inheritance and citizenship. It was more judicial — citizen-
ship — than economic. The annulment of the jubilee law was
announced by Jesus at the beginning of his ministry (Luke
4:17-19). This prophecy was fulfilled at the final jubilee year of
national Israel.°° This probably took place in the year that
Paul’s ministry to the gentiles began, two years after the cruci-
fixion.”

The jubilee law prohibiting oppression centered around the
possibility that the priesis and magistrates might not enforce
the jubilee law (Lev. 25:14-17). Thus, those who trusted the
courts when leasing land would be oppressed by those who
knew the courts were corrupt.”

The jubilee year was to be preceded by a miraculous year
bringing a triple crop (Lev. 25:18-22). This designates the
jubilee year law as a land law with a blessing analogous to the
manna. The manna had ceased when the nation crossed the
Jordan River and entered Canaan.**

The prohibition against the permanent sale of rural land (Lev.
25:23-24). This was a land law. It was an aspect of the conquest
of Canaan: the original land grant. This law did not apply in
walled cities that were not Levitical cities.”

The law promising rain, crops, peace in the land, and no wild beasts
in response to corporate faithfulness (Lev. 26:3-6). This was a land

 

20, Chapter 24.
30. Chapter 25.
31. James Jordan, ‘Jubille (3),” Biblical Chronology, V (April 1993), [p. 2).
32. Chapter 26.
38. Chapter 27.
34. Chapter 28.
