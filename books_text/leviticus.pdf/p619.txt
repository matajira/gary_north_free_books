Limits to Growth 563

in terms of the Bible’s covenantal limits — moral, judicial, and
eschatological — rather than in terms of Newtonian environmen-
tal limits: mathematical, physical, and biological."

The growth of population points either to the limits of
growth or the limits of time. Because the Bible affirms that the
limits to covenant-keeping man’s population growth are coven-
antal rather than biological, the Bible affirms that there will be
a final judgment. The Bible's promise of growth in one seg-
ment of the human population — covenant-keepers — is a testi-
mony to the end of history. Men arc expected to obey God’s
law; if they do, God promises to extend to them the positive
sanction of growth. ‘Thereforc, time will run out. But, the Bible
also tells us, time will run out before mankind presses against
unyielding environmental limits. The primary limit to growth
in history is covenantal. he environmental limits 1o growth are
merely theoretical - not hypothetical, but determinate physical
limits that are indeterminate in man’s knowledge.

11. There are some journalists and social thinkers who prefer to substitutc
quantum mechanics for Newtonian mechanics as a model for social theory, They
want to escape the Newtonian world’s determinate limits to growth by means of an
appeal to the indeterminacy of the quantum world: physical indeterminacy, not
merely conceptual. The two most prominent American authors who take this ap-
proach are George Gilder and Warren Brookes. At the time of his death in Decem-
ber of 1991, Brookes was working on a book developing this idea. He and I had
spent hours on the phone discussing this issue. He had presented an early version of
his thesis in The Kconomy in Mind (New York: Universc Books, 1982), ch. 1. He was
a Christian Scientist and feaned toward accepting non-physical explanations of man’s
condition. Gilder outlines his thesis in Misracosm (New York: Simon & Schuster,
1989). Eloquent as Gilder is regarding the exponential increase in the power of
computers, he cannot apply his thesis to population growth. Bodies cannot escape
into the realm of the quantumin order (o evade the limits to growth. Gilder invokes
Moore's Law, which says that the number of transistors on commercial microchips
doubles every 18 months. This law has held wruc since the late 1950's. ‘The law seems
to overcome certain physical limits. But Moore’s Law does not overcome the limits on
biological growth. Moore's Law was discovered by Gordon Moore, the co-founder of
Intel, the largest American mictochip producer, On Moore's Law and its commercial
implications, sce Robert X. Cringely (pseudonym), Accidental Empires (New York:
Addison-Wesley, 1992), pp. 41, |44, 306-7.
