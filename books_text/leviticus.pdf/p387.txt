Inheritance by Fire 331

False household worship was not generally a crime for resident
aliens.

Specifically, false worship was a crime if they participated in
a ritual offering of a child to Molech. Even if the child survived
the ordeal, the parent or parents were to be executed. The
crime was not murder or attempted murder; it was the profa-
nation of God’s boundary: the altar of sacrifice. The attempted
sacrifice of a child on such an altar was a capital crime. It was
this crime that God specified through Jeremiah as the crime of
Israel and Judah, leading to their captivity in Babylon. This was
the abomination that God would not tolcrate when His cove-
nant people did tolerate it (Jer. 32:35).

5. Is This Law Still in Force?

What principle of interpretation would lead us to conclude
that this law is not still in force? The Bible-affirming expositor
who claims that there is a total judicial discontinuity between
the two covenants with respect to this law needs to identify the
biblical basis of this alleged discontinuity.

‘The covenantal principle of inheritance teaches that the
heirs of covenant-keepers will inherit the earth progressively
over time. “His soul shall dwell at ease; and his seed shall in-
herit the earth” (Ps. 25:13). This is clearly one aspect of the
seed laws, which were all fulfilled in Christ. Covenant-breakers are
progressively disinherited. “For evildoers shall be cut off: but those
that wait upon the Lor», they shall inherit the earth” (Ps. 37:9).
The practice of Molech initiation reverses this principle of
inheritance: infanticide, either physical or covenantal. It is
therefore an abomination before God.

To the extent that the initiatory practice relies on demonic
intervention to protect the child, this ritual will kill off more
and more children as the demonic realm becomes weaker.
When demons can protect no child from the fire, the partici-
pants will disinherit themselves. Presumably, this will reduce
the number of participants over time. Also, the death of a child
