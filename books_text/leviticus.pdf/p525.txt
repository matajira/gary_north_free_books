The Right of Redemption 469

resided within the jurisdiction of a particular tribe at the time
of the reclaiming of the land by that tribe would become part of
a new land allocation (Ezek. 47:21-23). They could not be disin-
herited. But if that was true, then they could not be enslaved.

There is no indication that the jubilec’s heathen slave law
was annulled after the exile. Jesus announced His ministry in
terms of jubilee liberation (Luke 4:18-21). This assertion rested
on the continuing authority of the jubilee slave law. That aspect
of the jubilee was related to family ownership and citizenship,
not the original distribution of the land under Joshua. But a
new land allocation would free participating heathen families
from any threat of inter-generational bondage. Those who
resided in the land at the time of the return could not lawlully
be enslaved.

This was the source of the lawful continuing presence of
Samaritans in the land. These foreigners had been brought into
the Northern Kingdom by the Assyrians to replace the captive
Israelites. The returning Israelites were not authorized to kill
or exile these people. ‘here would never again be a lawful
program of genocide to establish original tile in Israel. Rather,
the resident alicn at the return would receive an inheritable
grant of rural land. The worship of Canaanite gods and reli-
gion never reappeared. The gods of Canaan had been gods of
the land, meaning gods of the city-state. ‘Whose gods were no
longer relevant in a nation under the authority of Medo-Persia,
then Lellenism, and finally Rome. In contrast, Persian dualism,
Hellenism, and ‘lalmudism were not bound by geography.
These became the main threats to biblical orthodoxy.

The returning Israelites took centuries to reconquer the
land. The reconquest was never completed, nor was Mosaic civil
authorily ever re-established. The tribes did not re-establish
their original borders, nor were they ever again totally free
from foreign civil rule. But the Jews did come close to re-estab-
lishing their pre-exilic political power and national boundaries
in the decades prior to Rome’s invasion, which led non-Jewish
