706 LEVITICUS: AN ECONOMIC COMMENTARY

charity, 171, 173

cities, 171-72

citizenship, 171

complete, 168

costs of, 171, 173, 174-75

covenantal death, 171

disinheritance, 170-71

judicial, 165-66, 172-73

legal status, 167

meaning of, 164

non communicable, 165

not contagious, 171

offerings (4), 169

Passover, 170

priest &, 150

priesthood, 165

reparation offering, 169-70

State &, 171-72

temple purity, 174

lessee, 395, 440-42, 444, 446,
612-14
Levites

Adamic, 139

adoption, 588-89, 591

advantages of, 477, 479

boundaries, 42-43, 138

centralization, 252-53

cities, 430, 475-78

covenant &, 359

cross-boundary tribe, 252-
53

decentralized, 387, 432,
477

defection, 366, 403-5, 406-7,
477-78

dominant, 477

feast, 15, 358

freemen, 587

gleaning &, 359-60

God’s agent, 252-53

golden calf, 251

guardians, 42-43, 138, 590-
91

holiness, 7

inheritance, 598

jubilee declaration, 391

jubilee enforcers, 477

judicial counselors, 588

land, 387

law’s subsidy to, 477, 479

meaning (Septuagint), 6-7

restricted, 387

sabbatical year, 400, 406

sanctuary, 138

separation, 290

subsidies to, 477

tithes, 138, 358, 365

urban, 387, 476-77, 479

wage regulation, 540

wealth, 477-78

wine &, 152

Leviticus

archaic law?, xxvii-xviil

book of boundaries, 11, 48,
278

book of kingdom, 8-10

book of life, 7

book of property, 10-11

boundaries, 1, 5-6

central message, 43

civil sanctions, 2

diet book?, xiv

economic laws, xxxvii-
xxxviii, 5-6

economics of, 43

five sections, 44-45
