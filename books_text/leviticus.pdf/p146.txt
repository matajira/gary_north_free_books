90 LEVITICUS: AN ECONOMIC COMMENTARY

jeopardy all those who were under his authority. It was not just
that he sinned on his own behalf; he sinned repyesentatively. In
contrast, this high degree of corporate responsibility for unin-
tentional sins did not rest upon the civil ruler, as we shail see.*
How could the priest’s unintentional sin bring the people
under visible judgment? Because of the structure of the biblical
covenant. Responsibility is covenantal, which means that it is
imposed hierarchically. Human accountability is simultancously
upward and downward. God is at the top of the hicrarchy;
nature is at the bottom. In between, God gives men and women
varying degrees of accountability, depending on their ordained
offices, their economic positions, and their social roles.
Because of the existence of God’s covenant sanctions in
history, the doctrine of covenantal hierarchy leads us to con-
clude that responsibility is both upward and downward. Those
who are under the legal authority of a covenantal officer arc
under the historical sanctions of God, both positive and nega-
tive, which God applies to them through this ordained agent
and also sometimes because of him. Authority is always hierar-
chical. It is therefore necessarily representative.” No one can legiti-
mately claim judicial innocence based merely on his claim of
autonomy. Participation in any covenantal institution is inevi-
tably a form of assent to representative authority, though always
limited by God’s law in the degree of required obedience.®
This assent is made in history; the sanctions are applicd in
history. This includes God’s sanctions.’ This was true in

4. Tam not speaking here of intentional sins of a civil ruler, such ay in the case
of David, who intentionally numbered the people in peacetime, against the advice of
Joab (I Sam. 24).

5. Ray R. Sutton, ‘That You May Prosper: Dominion By Covenant (Qnd ed; ‘Iyler,
Texas: Institute for Christian Economics, 1992), ch. 2.

6. Gary North, When Justice Is Aborted: Biblical Standards for Non-Violent Resistance
(FL. Worth, Texas: Dominion Press, 1989); cf. Christianity and Civilization, Nos. 2 and
3 (1983).

7. Gary North, Boundaries and Dominion: The Economics of Leviticus (computer
edition; ‘Iyler, ‘Texas: Institute for Christian Economics, 1994), ch. 4, subsection ow
