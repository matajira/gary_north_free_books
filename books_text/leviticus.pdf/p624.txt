568 LEVLTLCUS: AN ECONOMIC COMMENTARY

family Jand, tribal land, national land - were judicially fixed by
the terms of the conquest. A growing number of heirs necessi-
tated a declining per. capita landed inheritance within the
Promised Land. This pointed to the eschatological nature of
God’s covenantal laws of inheritance: a transcending of Israel’s
geographical boundaries. The promised inheritance of cove-
nant-keepers pointed to the breaking of the boundaries of the
Promised Land. The limits to growth of confessional Israel would
not be the boundaries of geographical Israel. The original con-
quest of Canaan would cease to be a limiting factor in the ex-
tension of God’s covenantal boundaries.

Enemies of Growth

The covenant-breaker prefers not to think about final judg-
ment. To cscape the very thought of such an event, modern
man has invented’a theory of the heat death of the universe.
The universe is supposedly being pushed by the second law of
thermodynamics toward absolute zero and total randomness.
Any thought that man’s population growth will extend to a
limit in history - ending history — is rejected out of hand. ‘The
limits to growth are scen as environmental rather than tempo-
ral. ‘The Bible teaches otherwise. It teaches that God’s blessings
can continue until the end of time, and one of these blessing’s
is mankind’s population growth. This points to the Second
Coming, not the heat death of the universe in billions of
years.'® Mankind’s limits are covenantal, not temporal.”

The advocates of zero-population growth (ZPG) are intellec-
tual heirs of Rev. T. Robert Malthus, whose anonymous 1798
Essay on Population argued that man’s growth is geometrical,
while man’s food supply grows merely arithmetically - a pre-
posterous claim theoretically and rcfuted empirically by two
centuries of rapidly growing food supplies. He dropped this

16. North, Is the World Running Down?
17. North, Boundaries and Dominion, ch. 34, soction on “Entropy”
