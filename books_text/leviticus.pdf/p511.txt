Food Miracles and Covenantal Predictability 455

requirements God’s revealed law? Was this annulled by Jesus in
His fulfillment of the jubilee year? No. In Leviticus 26, which
appears after the close of the jubilee laws, we read: “And I wiil
give peace in the land, and ye shall lie down, and none shall
make you alraid: and I will rid evil beasts out of the land, nei-
ther shall the sword go through your land” (Lev, 26:6). This
recapitulation of the promise of Leviticus 25:18-19 indicates
that this aspect of the jubilee law was broader than an aspect of
the jubilee law. But was it a cross-boundary law? Did it apply
outside the Promised Land? The recapitulation in Leviticus 26
is paralleled in Deuteronomy 28, and is mentioncd as a testimo-
ny to enemy nations, which would fear them (Lev. 26:7-10).

Why should these enemies be afraid of Israel if they did not
interpret the visible predictable sanctions in Israel as proof of
God’s unique presence with Isracl? Did this fear apply only to
the risks of invading Isracl? Were the nations not also to fear a
counter-invasion by Isracl?® Deuteronomy 20:10-20 lisis the
laws of siege. These laws did not apply to Israel's invasion of
Canaan, for they established legitimate terms of surrender,
which were not options during the conquest. Therefore, these
military laws had to apply to warfare outside the land. They
were cross-boundary laws. Since Israel was to be feared by
foreign nations, the corporate covenantal sanctions visible to
foreigners inside the land had to be presumed by them to
apply outside the land, too (Deut. 4:4-8).

Without the miracle of manna or the miracle of the triple
crop, New Covenant Christians are thrown back on their faith
in God’s revealed word. ‘he compelling evidence of God is
supposed to be God’s word. This always was the case, but the
miracles were added to overcome the Israclites’ weakness of
faith. Old Covenant believers in the wilderness had daily edible
reminders of God’s presence. In the Promised Land, these

6. Israel was not to initiate foreign wars. The Mosaic festival laws made empire
impossible, There was no permanent payoff in launching foreign wars.
