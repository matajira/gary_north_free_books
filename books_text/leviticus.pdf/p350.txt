294 LEVITICUS: AN ECONOMIC COMMENTARY

ly separation within Israel is therefore not in question here.
What kind of separation was involved? Did this law refer to the
legal boundary separating circumcised and uncircumcised men
dwelling in Israel? Did it refer to the separation between cir-
cumcised and uncircumcised nations? Or was there some other
separation involved? I believe that it referred to a unique form
of covenantal separation, one which is represented by no other
law in Scripture: a separation whose origins were in Isracl’s
past. This separation was the 40-year period of wandering in
the wilderness in which the Israelites of the exodus generation
refused to circumcise their sons.

This law applied to orchards. God marked off the fruit of
newly planted trees for His own purposes. He set this fruit
outside of covenant-keeping man’s lawful access. That is, He
placed a “no trespassing” boundary around the fruit of newly
planted trecs for three years alter they began to bear fruit.
Then he announced that the fruit of the fourth year was holy:
set aside for him. This was analogous to what He had done in
the garden with the tree of the knowledge of good and evil:
setting it aside for a period, keeping men away from it. The
question is: Why?

Temporarily Forbidden Fruit

Two facts need to be noted. First, this prohibition applicd to
the first four years of fruit borne by a tree that was planted in
the Promised Land after the land had come under the control
of the Israelites. As we shall see, the prohibition did not apply
to fruit from trees that had been planted by the Canaanites just
prior to the invasion of Canaan by Israel. It was not “trees as
such” whose fruit came under this ban; it was trees that had
been planted after the conquest.

The seeds or cuttings that would serve as the parents of
Isracl’s first crop would have come from the existing trees of
Canaan. ‘The new trees’ fruit was to be set aside for three sea-
sons and offered to God in the fourth. This indicates that there
