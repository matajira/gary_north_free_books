176 THE GREATNESS OF THE GREAT COMMISSION

Bible as, 68, 73n
Dilessing/promise, ix, 16-20, 74, 84, 90
conditional, 17
curse, 16, 20, 74, 84, 86, 90
definition, 16-20
divine, xiii, 2-3, 10, 14-28, 79, 151
Dominion Covenant, xi, xii, xili
ethics, 18-19, 23, 64, 67
hierarchy, xii, 18, 19, 41-63, 79, 115
human, 15-16
key promise, 20-21
Messenger of (see Christ ~ covenant
&)
Mosaic, 24
New, xi, 15, 16, 21, 22, 47, 48
oath, 16-20, 35, 79, 80, 84-85, 90
Old, 47
relation, 88n
sanctions, xii, 20, 84, 85, 90
sign, 88
structure of, 18-20, 64, 79, 90
succession, 18, 19, 23, 62, 91-108
terms/stipulations of, 16, 18, 64-78, 84,
114, 120
transcendence/sovereignty, 18-19, 22,
27-40
treaties, contracts, &, 16
unity of, 16
Creation/created
covenant &, 3, 10-14
creation week, 8-11, 13
goodness of, 7
man set over, xii, xiii, 58
orderly, 55-56
New Creation (see New Creation)
Creed, x, 114, 141
Culture
Christianity &, 5, 75-79, 105, 114,
118, 140
definition, 9, 9n
early man &, 9-10
ethnas, 51-52
models, 151-153
modern, 3, 58, 60, 72, 121

promotion of, 125, 128

renewal, 82, 83, 91, 99, 137, 153n

salvation &, 56, 62, 80, 96, 98

see also Civilization; Custom
Cukural Mandate (see Mandate)
Custom(s), 50, 51 (see also Culture)

David, King of Israel, 16, 34, 36
DeMar, Gary, 121n, 128n
Disciple(s) (n)
Christ’s, 13, 22, 29, 80, 95
Christians as, 65, 67, 82, 83, 90
fearful, 92-94
future Jewish, 144
receive Great Commission, 36, 38-39,
45n, 46
Disciple (v)
authority, 22, 41, 46, 71-72, 80,
duty to, 1, 37, 49, 64-78, 99, 113
national aspect, 53, 62, 97, 100-102
teaching, 30, 50, 67, 73, 74, 99, 105,
147
Disobey/disobedience, 17, 84
Dispensationalism (see Eschatology)
Doctrine
foundational, 74
importance of, 113-114
lack of, 158-159, 163
Dominion
Christ &, 44-45
Christianity &, x, 73
commanded, 14
denied, 105
family &, 119
inner drive, 9-10, 153
resisted, 103n, 154
Satan &, 82
see also Covenant — Dominion;
Subdue

Economics, 75, 76, 120, 123, 124
Education
Christian, 76, 114, 115, 119, 124-195
secular, xiii, 76, 124
