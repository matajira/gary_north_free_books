136 THE GREATNESS OF THE GREAT COMMISSION

this book. In that the Great Commission is so affected by one’s
eschatological system, it might be helpful to provide a bricf
summary of several of the leading features of the four major
evangelical eschatological systems.* It should be understood
that any particular adherent to one of the following views may
disagree with some aspect as I have presented it. There are
always differences of nuance among adherents to any particular
system. Nevertheless, the presentation attempts to portray accu-
rately the general, leading features of the systems. The systems
will be presented in alphabetical order.

Amillennialism

Definition: That view of prophecy that expects no wide-rang-
ing, long-lasting earthly manifestation of kingdom power until
Christ returns, other than in the salvation of the elect. Amillen-
nialist Kuiper writes: “'The thousand years’ of Revelation 20
represent in symbolic language a long and complete period;
namely, the period of history from Christ's ascension into heav-
en until his second coming. Throughout that age Christ reigns
and the saints in glory reign with him (vs. 4). Satan is bound in
the sense of not being permitted to lead the pagan nations
against Christendom (vss. 2-3) . .. . During that period also
takes place under the rule of Christ what may be termed the
parallel development of the kingdom of light and that of dark-

ness... . Toward the end of ‘the thousand years’ Satan will be
loosed for a little while. Those will be dark days for the church
of God . . . . Christ will return in ineffable glory and, having

raised the dead, will sit in judgment on all men (Rev,
20:12,13).”*

Descriptive Features: 1. The Church Age is the kingdom era
prophesied by the Old Testament prophets.’ Israel and the
Church are merged into one body in Christ to form the Israel

%. For more detailed information, see: Robert G. Clouse, ed, The Meaning of the
Millennium: Four Views (Downer’s Grove, IL: Inter-Varsity Press, 1977).
4. R.B. Kuiper, God-Centered Evangelism (Grand Rapids: Baker, 1961), pp. 208-209.
5. Amillennialist Anthony Hoekema secs the fulfillment of the kingdom prophecies
in the New Heavens and New Earth, rather than in the Church. Anthony Hoekema, The
Bible and the Future (Grand Rapids: Wm, B. Eerdmans, 1979). See my footnote 2, p. 147.
