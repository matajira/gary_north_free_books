56 THE GREATNESS OF THE GREAT COMMISSION

regarding world salvation refer, then, to the world as the orderly
system of men and things. That is, the world that God created and
loves is His creation as He intended it: a world in subjection to
man who in turn is to be in subjection to God (Psa. 8).

A point frequently overlooked in the passages cited above is
that those verses clearly speak of the world-system focus of His
sovereign redemption. Thus, in each of the passages passing
under our scrutiny, we have reference to the aim of full and
free salvation for the Aosmos, the world as a system. That is,
Christ’s redemptive labors are designed to redeem the created
order of men and things. Hence, the Great Commission
command to disciple “all nations” involves not only all men as
men (anthropos), but all men in their cultural connections (ethos)
(Matt. 28:19), for Christ “is Lord of all” (Acts 10:36). All of
society is to be subdued to the gospel of the sovereign Christ.

Consequently, as A. T. Robertson marvelled regarding the
Great Commission: “It is the sublimest of all spectacles to see
the Risen Christ without money or army or state charging this
band of five hundred men and women with world conquest and
bring them to believe it possible and to undertake it with
serious passion and power.”® Yet that is precisely what Christ
did. As Chamblin put it, when speaking of the giving of such
authority to Christ: “God the Father . . . now wills that Jesus’
existent authority (7:29; 8:9) be exercised universally,"*

Cultural Redemption

Salvation is designed for the “world as a system” (kostmos)
involving men in their cultural relations (¢hnos). Obviously,
then, it must follow that its effects should be pressed in every
aspect of life and culture, not just in the inner-personal
realm.® In fact, Christ’s Commission claims just that in two
very important phrases.

 

63. A.T, Robertson, Word Pictures in the New Testament (Nashville: Broadman, 1980),
1:244-245.

64, Chamblin, Matthew, p. 760.

65. Gary North, “Comprehensive Redemption: A Theology for Social Action” (1981),
in North, Is the World Running Down? Crisis in the Ghristian Worldview (Tyler, Texas:
Institute for Christian Economics, 1988), Appendix C.
