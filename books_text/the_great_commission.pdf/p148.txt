132 ‘THE GREATNESS OF THE GREAT COMMISSION

Conclusion

The Great Commission has important, direct bearings on the
three foundational societal institutions, the family, the Church,
and the State, The full-orbed character of the Great Commis-
sion demonstrates both its greatness and its practicality to life.
The eyes of the Lord are in every place beholding the evil and
the good (Prov. 15:3), not just in the heart, but in all areas of
life.

If Christians are to preserve the very greatness of the Great
Commission, they need to see its applicability to all of life. To
do so will require a radical re-orientation in our thinking, a
biblical re-orientation. We need to reclaim the Pauline spirit:

Therefore I testify to you this day that I am innocent of the blood
of all men. For I have not shunned to declare to you the whole
counsel of God. Therefore take heed to yourselves and to all the
flock, among which the Holy Spirit has made you overseers, to
shepherd the church of God which He purchased with His own
blood (Acts 20:26-28).
