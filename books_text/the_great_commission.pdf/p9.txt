PUBLISHER’S FOREWORD
Gary North

So God created man in his own image, in the image of God created he
him; male and female created he them. And God blessed them, and God said
unto them, Be fruitful, and multiply, and replenish the earth, and subdue
it: and have dominion over the fish of the sea, and over the fowl of the air,
and over every living thing that moveth upon the earth (Gen. 1:27-28).

And Jesus came and spake unto them, saying, All power is given unto
me in heaven and in earth. Go ye therefore, and teach all nations, baptiz-
ing them in the name of the Father, and of the Son, and of the Holy Ghost:
Teaching them to observe all things whatsoever I have commanded you:
and, lo, Tam with you alway, even unto the end of the world. Amen (Matt.
28:18-20).

There is a tremendous need today for evangelism. By this, I
do not mean simply the limited personal evangelism of tract-
passing. In any case, the tract is no longer with us. The newslet-
ter and the satellite TV interview show have replaced tracts.
What is needed today is a comprehensive program of worldwide
evangelism that brings the message of salvation to every indi-
vidual on earth, in every walk of life.

Having brought people into the kingdom of God through
conversion, God then asks them to begin to make a difference
in their world. He does not mean that they should spend day
and night passing out tracts or the equivalent thercof; He
means that they should reform their lives, their families, and
their daily walk before Him and men. Evangelism means teach-
ing people to obey God’s law, through the empowering of God’s
