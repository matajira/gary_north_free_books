122 THE GREATNESS OF THE GREAT COMMISSION

2. Involved child rearing and discipline. Our children expressly
should be taught how to live the Christian life by the diligent
application of biblical principles of child rearing’® and disci-
pline.” This is “discipling” in the home, as per the Great
Commission (Matt. 28:19). This discipline should also include
consistent church attendance, to worship Him who has “all
authority in heaven and on earth” (Matt. 28:18).

The training of covenant children should not be left to oth-
ers by parents too busy for their children. The regular “envi-
ronmental”™ influence of God’s Word and Christ's gospel in
daily family living is vital." Children should be taught the le-
gitimacy and practice of living under authority in society by wit-
nessing it in the home through the headship of a loving, in-
volved, and godly father.'* Too often this duty has devolved
almost wholly upon the mother, though she obviously has an
important role, as well. Ultimately, biblical child discipline will
work back to practical training in living under Christ’s authority
(Matt. 28:18).

Children also should be taught how to set goals for the long
term (Matt. 28:20), rather than being allowed to drift about
with the winds.

3. Teaching the value of labor. Apprenticeship of children in
both family living and personal and corporate labor with a goal
to self-sufficiency is important.

Ours is an intolerably irresponsible age. Christians must
swim against the secular tide by instilling responsibility and
Giligence in their children.” The Christian is aware of the di-
vinely ordained institution of labor from man’s primordial
beginning (Gen. 1:26-28; 2:15). The Commission is replete with
verbal action. In addition to commands to disciple, baptize, and

12. Deut. 11:20-21; Josh. 24:15; Psalm 78:4-7; Prov. 4:1-4; 29:6; Eph. 6:4.

13. Prov. 19:18; 29:15; 23:15; 23:14; 29:15; 29:17; 18:24,

14. Regarding the ultimate personal environment in which we dwell, see: Paa. 189:7-
12; Jer. 23:24; Acts 17:28. For a brief discussion of our “divine environment,” see:
Kenneth L. Gentry, Jr., The Necessity of Christian Schooling (Mauldin, SC: GoodBirth, 1985).

15. Gen, 18:19; Psa, 1:1-6; Deut. 6:5-25.

16. 1 Cor, 11:1f% Eph. 5:22-6:4; Col. 3:20. See the insightful discussion in Gary
North, An Introduction to Christian Economics (Nutley, NJ: Craig, 1978), ch. 21.

47. Prov. 12:24; 21:25; Rom. 12:11; 1 Tim. 5:8; 2 Thess. 3:10; Heb. 6:12,
