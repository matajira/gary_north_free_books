THE NEBRASKA SCHOOL WAR 183

legal. After all, the children are in school. Along the
border children on both sides frequently attend
school on the other side, since driving distances are
great. within each statc, These private schools can
sneak through a gap in the law. Wyoming doesn’t
regulate Christian schools, but the Nebraska truancy
officers and county attorneys figure that’s a matter
for Wyoming to deal with. And Wyoming authori-
ties, not being idiots, aren’t about to stick their
hands in a hornets’ nest until they see the outcome of
the pending cases in Nebraska.

THE ISSUES What is happening in Nebraska is a
war. It's a war between two systems of thought, one
based on a long-familiar and now increasingly
suspect philosophy, and the other based on an ac-
curate but politically unpopular philosophy. It’s a
question af sovereignty, Who is responsible for the
education of children: the State or the parents? It’s as
simple as that, although other issues have been raised
by participants on both sides.

The position that the State is responsible for the
education of children is ancient. It gocs-back at least
to the blueprint sketched by Plato of his recommended
communist commonwealth. He argued that the con-
trollers (“guardians”) need a monopoly over instruc-
tion: “Then shall we so easily let the children bear
just any tales fashioned by just anyone and take into
their souls opinions for the most part opposite to
those we'll suppose they must have when they are
grown up? . . . First, as it seems, we must supervise
the makers of tales; and if they make a fine tale, it
