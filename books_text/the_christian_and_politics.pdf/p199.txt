‘THE NEBRASKA SCHOOL WAR 187

become convinced, as so many naive Americans are,
that the State is responsible for the education of chil-
dren. The Bible teaches that parents are responsible,
not the State, It is because modern men believe that
the State has taken on the functions of the family
that people can believe that the State must supervise
education. This religion of the “State as pseudo-
parent” is widespread. The Siate has taught it in its
very own established church, the public school system.

Conservatives are not immune from this error. I
received a letter from a subscriber to Remnant Review,
who protested against my insertion of a promotional
flyer telling the story of Pastor Sileven’s 1982 crisis.
Amazing—one of Sileven’s bureaucratic opponents
was a subscriber! Here, believe it or not, is one
political conservative’s view—a high official of the
public schools in a Nebraska county:

The State of Nebraska requires public and
private school teachers to be certified which re-
quires a B.S. or B.A. degree in education. The
Catholic and Lutheran private school systems
adhere to this policy and they state that
although religion is an added part of the curric-
ulum, qualified teachers still are needed to teach
the three R’s.

‘To someone living in Florida or Oregon and
reading the newspapers about Everett Sileven’s
church, one would think that anti-religious
idiots existed in the State of Nebraska. The
public pulse columns of the Omaha World
Herald were full of letters to the editor from all
