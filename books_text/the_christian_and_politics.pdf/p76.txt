64 THE CHRISTIAN AND POLITICS

at present. I remember as a child that we were taught
respect for the law. We were told if we ever went to jail
we would have to live on “bread and water,” ‘The elec-
tric chair was the punishment for murder.

An excellent little book, Essays on the Death Penalty,
edited by T. Robert Ingram, appeared several years
ago. I especially recommend the essay by C. S. Lewis
entitled “The Humanitarian Theory of Punishment.”

RESTITUTION The principle that undergirds the
Biblical approach to crime and punishment is the
concept of restitution. Under restitution the criminal
repays his victim for the crime.

Restitution is basic to God’s order for the
universe. Man sinned against God. God’s justice re-
quired restitution. Sin deserves death. Only Christ
could make restitution to God for sinful man. There
can be no forgiveness without restitution. A just God
forgives sinful man because Christ made restitution.

Justice on earth is to follow the same pattern. If a
man commits a capital crime, he pays with his life.
Lesser crimes require that a payment be madc to the
victim. Under the humanistic approach we are told
to feel sorry for the criminal because he is the victim
of his environment. Under the Biblical system the
focus is on the victim who has been wronged.

The current penal system refers to the debt a
criminal has to society. The criminal’s debt is to the
victim under Biblical law. When the criminal pays a
debt to society, he stills feels guilty because he is
guilty. He has not made restitution to his victim.
Restitution must be. made before he can be forgiven.
