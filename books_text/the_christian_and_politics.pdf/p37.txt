“IT'S A MATTER OF SURVIVAL” 25.

employces.

Now the Federal government plans to require
churches to pay social security taxcs. They will not
be required to pay on the minister (who will con-
tinue to pay that tax on his own unless he has been
exempted from it), but the churches will have to pay
the employer share for other employees of the
church. For the first time the government will be
directly taxing the churches.

Churches are being attacked by zoning laws.
When I moved to Fairfax County, Virginia, 24 years
ago there were no laws relating to the location of
churches. For centuries churches had purchased
land and built houses of worship. Suddenly this all
changed. An ordinance was passed requiring that
churches obtain a special use permit in order to
build. They even went so far as to ban prayer
meetings and Bible studies in a private home with-
out the permission of the civil authorities. An aged
priest, Father Gedra, was fined for conducting wor-
ship in his home. A lady who had conducted Bible
studies in her home for years was forced to go to the
Board of Zoning Appeals for a permit.

She got her permit, but that is not the point. The
Board could just as well have turned her down.
They, not she, were deciding whether she could have
a Bible study im her home. When her case came be-
fore the Board, the room was packed with Chris-
tians. When the ordinance was passed, there were
about a half dozen of us there to speak against it.
Had all those Christians turned out at the hearing on
the ordinance I doubt that it would have been passed.
