MY EXPERIENCE iN POLITICS «131

Committee (known as the P&E Committee) for con-
sideration. Much to the surprise of the pro-ERA
people (and even to myself) the bill was voted out of
committee.

When it got to the floor, the pro-ERA legislators
loaded it up with about eight frivolous amendmenis.
They specified that it would have to be voted on in
separate voting booths for men and women. They
wanted to appropriate a large sum of money for the
election (It wasn’t going te cost anything since an
election was being held anyway.), etc.

‘Then the pro-ERA people voted to send my bill
back to the P&E Committee to die forever. This was
the very committee they had accused of bottling up
legislation. The last thing they wanted was to have
the people of Virginia vote on ERA. They knew they
would lose. They said they feared that Jerry Falwell
and others would get all kinds of right-wingers elected
in the process of voting on the ERA.

After this we shut down a lot of the ERA propa-
ganda. They didn’t forget the part I had played. I
was the only member of the Virginia Gencral
Assembly from Northern Virginia who was against
the ERA. There are about 25 legislators from Nor-
thern Virginia.

The year I was elected the libbers had gone after
Delegate James Thompson from Northern Virginia
because he was anti-ERA. They defcated him, but
while they were at it we knocked out two pro-ERA
delegates. I took the place of onc of them. When I
was up for re-election they went after me with a
vengeance, I was defeated. ERA was not the only
