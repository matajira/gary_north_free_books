146 THE CHRISTIAN AND POLITICS

recruiting workers, raising money, canvassing the
voters, and all the other things that need to be done.

SELECTING CANDIDATES Candidates for political
office come from all walks of life. The most frequent
background is that of an attorney. Sixty percent of
the Virginia General Assembly were attorneys when
I was there.

Persons can be recruited from various back-
grounds. Often candidates will put themselves for-
ward, The politicians themselves may license others
and require them to have all kinds of credentials, but
the politician doesn’t have to possess any himself. He
has to meet some age and residency requirements,
file for office, perhaps pay a fee, but that’s about it.
The voters pass on his qualifications.

Most successful candidates séem to be outgoing
types, but believe me there are all kinds.

“Don’t bet on a horse who doesn’t want to run.”
That was the advice given to me by a retired colonel
in Alexandria who was active in politics. I don’t
recommend you run unless you really want to.

CAMPAIGN ORGANIZATION The key person is the
campaign manager. You will need a, treasurer. The
kind of office you are running for will determine the
extent of the campaign organization.

If you are running for Congress, get a pro to run
the campaign. A housewife or retired person who
has time may be a good manager for a state or local
office.

Tt is the duty of the candidate to recruit workers.
