waxes 95

Who am I to argue with the Supreme Court, espe-
cially in those rare instances when they are right? An
insurance company has a contractual obligation to
pay. The Feds don’t.

Social Security is called a contribution, but it is
not a contribution in the usual sense of the word.
‘Tribute, yes, but contribute, no. It is a payroll tax.

Social Security is not a progressive tax. Nor is it
a proportional tax. It is a regressive tax. Those who
have the lowest income pay a higher percentage of
their total earnings than those who carn more. This
is unjust to those who earn less,

The tax rate is the same for everyone. The differ-
ence is that income over about $37,800 isn’t taxed
(yet!). The base rate has been rising rapidly in re-
cent years as well as the top amount on which the tax
is paid. It is scheduled to go higher, much higher.

Still there is not enough money. The system is
going broke, The reason is clear. The Social Security
system is actuarily unsound. This means there is not
enough money scheduled to come in to meet the
commitments. Social Security.is a type of Ponzi
scheme, It is like the chain letter racket. It needs an
increasing number of people paying in so that
money promised can be paid out. That is why newly
hired Federal workers and employees of tax-exempt
organizations are being brought under the system.

Congress will patch up the system from time to
time to try to keep it going. Don’t depend on Social
Security for your financial future. The Social Security
idea started in Germany. In that country in 1923 a
month's social security check would not buy an egg.
