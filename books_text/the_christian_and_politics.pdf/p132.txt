120 THE CHRISTIAN AND POLITICS

among the Christians. If they didn’t support me, then
who would? | talked’ to Christian leaders. I visited the
churches, All over the area Christians held receptions
for me ‘in their homes. One of my most ardent sup-
porters was Alyse O'Neill, a Jew. She brought m the
largest voic in any of the precincts.

I spoke at these receptions about the issues. I got
Christians interested. Many volunteered to help in
the campaign. We distributed campaign literature
all over the district. Young people helped us do this.
They had a contest to see who could get the most
bumper stickers out.

Direct mail was an important part of our cam-
paign. My daughter-in-law wrote a Thoburn for Con-
gress Cookbook. It was widely distributed. We set up a
campaign headquarters. We made lots of phone calls.
We organized the precincts. I appeared at candidate
forums throughout the district. We got newspaper and
television coverage. I got endorsements from promi-
nent politicians and people in the cormmunity.

We were able to raise more money than our op-
ponents. A successful campaign is usually expen-
sive. Most of our funds were raised by direct mail. I
loaned my campaign money also.

When the votes were counted, more persons had
voted in the primary .than had ever voted in a
Virginia Republican Primary before.

I carried the two most populous areas of the
district. I lost in an outlying county even though T
received more votes there than had even been cast in
the previous primary. I did not have. time to get
organized there. I believe if I -had two or three more
