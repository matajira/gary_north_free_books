30 THE CHRISTIAN AND POLITICS

been a source of hope for the future. The operation.
of Christian schools has made the churches and
parents conscious of the issucs. Many Christians will
take a lot from secular humanists. They will pay
high taxes, endure regulations, and put up with all
kinds of things. But when it comes to what is near
and dear to them on this earth, their own children,
plenty of Christians arc ready to fight.

Another important point is that with the estab-
lishment of Christian schools, Christians are being
forced to think about the material world, They are
rethinking economics, history, geography, reading,
math, and all the other subjects in light of the Scrip-
tures. Christians are beginning to realize that educa-
tion in a Christian school is different. It is not a mat-
ter simply of opening the day with a prayer or even
just adding a Bible course to the curriculum. It is a
matter of developing cach subject area from a Chris-
tian perspective. And that means looking at civil
government from a Biblical perspective, In line with
this the Christian community is producing new text-
books. When I began a Christian school 22 years ago
there was hardly anything available in the way of
textbooks written from a Biblical point of view. Now
we have a healthy competitive market with numer-
ous materials to choose from. The situation should
markedly improve in the years ahead.

If the Christian schools are to thrive and flourish,
it is necessary that Christians become active politi-
cally. If you want to control the education of your
child, you must become active.
