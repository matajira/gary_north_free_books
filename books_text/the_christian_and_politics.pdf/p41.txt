“17S A MATTER OF SURVIVAL” 29

placed upon them.

A few years ago the Christian schools were
threatened by Revenue Procedures proposed by the
Internal Revenue Service. The bottom line was that
the Christian schools were going to be considered
guilty of racial discrimination unless they could
prove themselves innocent. The IRS would have
forced quotas of all kinds on the schools along with
arbitrary enforcement.of vague regulations. Provi-
dentially the proposals were defeated, but not with-
out a fight. Again it was the threat of removing tax-
exemption which constituted the stick in the govern-
ment’s hand.

When the Christian schools have to start paying
social security taxes, it will mean a payroll increase
of over 14% the first year and more increases on top
of that. Since parents are already taxed heavily to
support the government schools, ali this means an
added burden for the Christian schools. It is a mat-
ter of survival. That is why the Christian needs to
get involved in politics.

Teacher certification, accreditation, curriculum
control, and licensing of schools has been a major
avenue of attack upon the Christian schools. In
Virginia the state code does not even define what a
school is, but other states haven’t been so fortunate.
The Christian schools and parents have had to do
battle in Ohio, North Carolina, Kentucky, and
numerous other states to maintain the freedom to
operate without governmental control.

While the Christian schools have been the focus
of the attack from the humanists, they have also
