THE ROLE OF CIVIL GOVERNMENT 51

While the church is a ministry of grace, the
state is to be a ministry. of justice. As a ministry of
justice it carries out a different function under God.
Paul says in Romans 13 that the civil ruler has the
power of the sword. He is a minister of God to ex-
ecute God’s wrath upon evildoers. The sword is an
instrument of death. It is associated with power.

The church works by moral persuasion, The ulti-
mate power of the church is to excommunicate the
unrepentant, thus pronouncing Ged’s sentence of
death upon them. The church has the keys to the
kingdom — the key of doctrine and the key of disci-
pline. The church opens the kingdom to men by
declaring that salvation is by the blood of Christ.
The church closes the door by declaring that unbe-
lievers are lost without Christ. Thus the power of the
church is very real.

The power of the civil government involves phy-
sical force. The sword was not a toy. Capital punish-
ment is the right and duty of civil government. Civil
rulers are to maintain justice. When civil rulers
pervert justice, God brings judgment. Isaiah called
rulers to repentance in his day with these words:
“How is the faithful city become an harlot! it was full
of judgment; righteousness lodged in it; but now
murderers.” (Isaiah 1:21)

The state is to be a ministry of justice. But what
is justice? The state is to punish evildoers. How do
we determine who is an evildoer? These questions
are important because liberals love to talk about
“justice.” Especially are they fond of the term “social
Justice.” It has become one of their buzzwords. They
