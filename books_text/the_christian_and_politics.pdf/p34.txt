22 THE CHRISTIAN AND POLITICS

favorite indoor sport— going to court? It's no fun
when you lose this game and Christians have been
losing, at least until recently.

We know the Romans fed the Christians to the
lions. We’re familiar with Stephen, Paul, Foxe’s Book
of Mariprs, and Bunyan. We recall the accounts of
those who were burned at the stake for the faith. And
we know persecution takes place in other countries.
The slaying of missionaries in South America by the
savage Auca Indians was graphically portrayed ‘in
Life magazine and elsewhere. We ali know the Soviet
Union persecutes Christians.

What we need to realize is that there is plenty of
persecution going on right now in the good old
U.S.A. and if we don’t move to protect ourselves it
will get worse. We like to think we have religious
freedom because we have the First Amendment to
the Constitution. You might be surprised to learn
that freedom of religion is guaranteed by ‘the Consti-
tution of the USSR also. You can believe anything
you want to in the Soviet Union. Just don’t try to act
in terms of your belief.

The United States Constitution says that Con-
gress shall make no law respecting an establishment
of religion nor prohibiting the free exercise thereof.
Increasingly this is being interpreted to mean what
religious freedom does in Russia. Our founding
fathers realized that one must be able to exercise his
religion in order to have freedom of religion.
Remember that liberals not only like to play with
nice words. They like to subvert nice documents.
They didn’t come out for abolishing the Bible. They
