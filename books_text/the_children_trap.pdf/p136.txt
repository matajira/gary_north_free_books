116 The Children Trap

Under political pressure from a vicious secret society, the Ku
Klux Klan, the State of Oregon had made private high schools il-
legal. It was a classic piece of anti-Roman Catholic legislation; in
that era, there were virtually no private Christian schools in the
United States, except those run by Protestant immigrant groups
and Roman Catholics. The Supreme Court said Oregon did not
possess the authority to enforce such a law. It has been said,
though, that eternal vigilance is the price of liberty. Another true
saying is that “When the legislature is in session, our liberties are
in danger.”

We should appreciate the freedom that yet remains in the
United States. We should not take it for granted. Have you heard
of any Christian schools in the Soviet Union? I had a visitor from
Sweden in 1985. She runs a Christian school with about 24 stu-
dents. Her school is one of only nine Christian schools in the en-
tire country.

We have the freedom to send our children to a Christian
school. I have set forth several of the ways the state tries to controt
these schools. There is one other important method of control that
I want to discuss with you. I will call this “pocketbook control.”
My mother (who never touched alcoholic beverages in any shape
or form) used to say, “Talk is cheap, but it takes money to buy
whiskey.” As applied to education, her comment means that one
has the freedom to send children to a Christian school, but it costs
money.

There is no question in my mind that financing Christian
schools would not be all that difficult if we Christians could keep all of
our own money that now goes to the public schools. We are talking about
a lot of money. The public school system is far more expensive
than most of us realize.

There is no such thing as a free lunch. There is also no such
thing as a free education. Someone has to pay for it sooner or
later. (Usually it is sooner.) The public schools are not free. They
are very expensive. The education of your children and my chil-
dren has a cost attached to it. The only question is who will pay
that cost.
