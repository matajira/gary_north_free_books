130 The Children Trap

I mentioned earlier that the availability of a building can be an
advantage of a church operated school. This may also be a disad-
vantage. Usually the Sunday School rooms of a church are quite
small. The Christian school classes will have to be small also.
Such small classes, while considered an advantage, will increase
costs a great deal. A larger class can be taught just as efficiently.
The increased income makes it possible to lower tuition costs, to
acquire better equipment, and to pay teachers adequately. The
church facilities may also lack in other respects also.

One pastor, Morris Sheats in Dallas, has a very productive
comment on the structuring of schools which is especially applica-
ble to churches which don’t have proper facilities.

He suggests that the parents who want a school form a private
corporation and build facilities which the church would then rent
from the school at sufficient enough rates to cover the basic cost of
the facilities. Then they are responsible for the oversight of the
school, and the pastor does not have to take time from normal
pastoral duties. The responsibility is placed squarely where it
should be: on the parents.

Of course, in choosing a non-church-related school, great care
must be taken to insure moral and academic integrity. You know
your local church. You may not know your local Christian school;
so, check them out carefully.

In choosing a school, I would consider the following:

1. The doctrinal position of the school.

As a Christian, I want my children in a school that believes
the Bible to be the infallible word of God. The school should sub-
scribe to the fundamental doctrines of the faith. I won’t take the
space to list all of them, but they would include belief in the sover-
eignty of God, the Trinity, the virgin birth and resurrection of
Christ, the atoning death of Christ on the cross, salvation by
grace through faith alone, creation, and the validity of God’s law.

2. The educational philosophy of the school.
Just because a school calls itself “Christian” does not make it
so. A school may appear to be sound doctrinally and still be
