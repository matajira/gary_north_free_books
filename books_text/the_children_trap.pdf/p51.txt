God-Centered Education 31

law of God is faithfully taught in the school. If God says some-
thing is right, then it is right. If God says something is evil, then it
is evil. The Christian school delights in the law of the Lord.

Lie Number Four: The Child Is Inkerently Good

This humanist lie is a denial of original sin. The Bible teaches
that the sin of Adam is imputed (declared by God) to his descend-
ants. Only Jesus Christ was not descended from Adam by ordin-
ary generation and is without sin. Man's basic problem is his sin.
God-centered education is based on the fact that the child has a
sinful nature. Sin is dealt with in the child in terms of Biblical dis-
cipline. Basic to this is instructing the child to repent of sin and to
trust in Jesus Christ as: his Saviour.

The humanist denies original sin. For the humanist man’s
problem is his environment. Thus, the humanist wants to modify
or change man’s environment. When Adam was caught in sin, he
blamed the woman. When God questioned the woman, she blamed
the serpent. They were blaming their environment — the environ-
ment that God had given to them. Ultimately, they were blaming
-God. It was all His fault.

The humanist looks upon the child as an animal to be condi-
tioned through the manipulation of his environment. Thus, hu-
manist education becomes a conditioning process. Teachers are even
called “change agents.” Modification of behavior to suit humanist
goals is important in humanist schools.

Because the humanist believes the environment is the prob-
lem, he denies individual and personal responsibility. The focus is
on legislation to change man’s environment. Thus, the use of the
coercive power of the state is central in humanism.

God-centered education teaches personal responsibility for
one’s actions. God's grace is central in the Christian school. It is
never mentioned in the government schools unless done in secret.

Lie Number Five: Man Saves Himself

Humanists deny the existence of God. Of course they do not
believe in the Trinity. If there is to be any salvation, it must come
