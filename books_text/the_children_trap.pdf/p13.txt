xiii The Children Trap

6, Well, then, each student should be allowed to offer a silent
prayer before he begins school. After all, this is a Christian nation!

7. Christian children need to attend. They need ta be exposed
early to the real world; they shouldn’t be kept in religious hot-
houses.

Let me try an analogy on you. We are adults here. Substitute
the word “whorehouses” for “public schools.” Now, let’s hear these
same familiar arguments in favor of making the public schools de-
cent for Christians:

1. We need to regulate the local whorehouses.

2. We need to inspect them for disease.

3. We need to make them safe from violence.

4. We need to require a prayer when they open for business.
(Megal?)

5, Well, we need to have them allow voluntary prayer before
they open for business. (Illegal?)

6. Well, then, each customer should be allowed to offer a silent
prayer before he conducts his business. After all, this is a Christian
nation!

7. Christian children need to be exposed early to the real
world; they shouldn’t be kept in religious hothouses. (“Cathouses,
not hothouses!”)

Preposterous, aren’t they? But if the myth of neutrality is
false, then equally preposterous are all the calls to “recapture our
public schools.” They were never “ours” in the first place. They in-
herently violate Biblical principles: the family has the moral and
financial responsibility for educating its children, not the state.

But is a public school the same as a whorehouse? It’s close
enough! God calls worshipping false gods “whoring after false
gods.” False worship is whoredom, and whoredom is the worship
of false gods. The two are equated in the Bible. The Book of
Hosea is built around this theme.

What is a school system that teaches mankind’s evolution out
of meaningless slime, if not whoredom? What is a school system
that makes it illegal to teach God’s law as the only valid moral
standard for mankind, if not whoredom? What is a school system
