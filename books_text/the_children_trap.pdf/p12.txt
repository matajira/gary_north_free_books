xii The Children Trap

Reforming the Public Schools

All is not lost, most Christians tell us. “We can and must re-
form the public schools!” (Have we seen anything but academic
and moral decline since 1840?) “We can and must make them safe
again.” (Teachers get mugged; over 70% of the students have ex-
perimented with drugs before graduation; Planned Parenthood is
allowed on many campuses, teaching abortion as a legitimate al-
ternative.) “We can force them to teach creationism.” (Does any
school district in the United States teach it?) “The public school
creationist view will not mention a Creator, of course; that would
be illegal, a violation of the myth of neutrality.” (Creationism
without a Creator?) “But we can teach a zero-God view of créa-
tionism.” And then Christians are asked to contribute hundreds of
thousands of dollars—money that could be building Christian
schools—in attorney fees to fight these “Creation science” cases, not
one of which has been won, and not one of which is likely to be won.

Or what about this one? “We'll get prayer back into the
schools!” The prayer can’t mention Jesus, of course. That would
violate the myth of neutrality, and we all believe in the separation
of church (whose God is decidedly unneutral) and state. But the
prayer can at least mention the word “God.” Only it can’t. Not
one school district in the nation legally can permit such prayer.
Only silent prayer is allowed. So much for freedom of religion.
What the U.S. Congress has every morning— prayer by a taxpayer-
financed chaplain —children are not entitled to.

The Arguments for “Staying in the System”

You have heard lots of arguments for Christians sending their
children to the public schools, and for running these schools on a
Biblical, moral basis. Let me list a few:

1, We need to regulate the public schools.

2, We need to inspect them for health.

3. We need to make them safe from violence.

4, We need to require a prayer when they begin. (Illegal?)

5. Well, we need to have them allow voluntary prayer before
they begin. (Illegal?)
