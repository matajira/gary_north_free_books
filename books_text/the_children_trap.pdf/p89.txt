Education Should be Voluntary 69

mandatory, the money to operate the school is mandatory. In a
later chapter, I will discuss the financing of the public school sys-
tem. For now, let us focus our attention on the compulsory atten-
dance laws.

Compulsory attendance laws have been on the books since the
beginning in the state of Massachusetts. The Puritans passed a
compulsory attendance law in 1647:

“It is therefore ordered, that every township in this jurisdiction,
after the Lord hath increased them to the number of fifty house-
holds, shall then forthwith appoint one within their town to teach
all such children as shall resort to him to read and write, whose
wages shall be paid either by the parents or masters of such chil-
dren, or by the inhabitants in general. . . .”

It was this long tradition of compulsory Christian education,
which was in part financed by the taxing authority of the civil gov-
ernment, that two centuries later served Horace Mann so well in
his call to establish taxpayer-financed non-Christian schools in
Massachusetts.

Taxpayer-financed education in the southern United States
came in full force after the South lost the Civil War in 1865. Com-
pulsory education laws were dropped briefly in the late 1950's and
early 1960's, when massive resistance to racial integration was in
style. The state of Virginia did not have compulsory attendance in
these years, and possibly some other states dropped compulsory
attendance laws for a period of time. To my knowledge, all states
now have such laws. The wording of the laws varies from state to
state.

Enforcing What Most People Already Do

It should be understood that it is difficult in a nation that hasa
democratic or republican form of government to get voters to sup-
port civil measures that they do not support privately. An illiterate
nation is unlikely to vote for compulsory school laws. Only where
there is already a high degree of literacy in a society will voters ap-
prove universal, taxpayer-financed education.
