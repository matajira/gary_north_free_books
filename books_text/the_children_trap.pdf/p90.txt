70 The Children Trap

What the vast majority of people have already done for their
own children — taught them to read — they then vote to make com-
pulsory on the tiny minority of citizens or residents who have
refused to imitate the majority voluntarily. Thus, compulsory at-
tendance laws can do very little to improve a nation’s educational
standards, As we have seen for two generations in the United
States, the nation’s literacy rate has actually dropped.

Legalized Kidnapping and Desertion

The discussion of school attendance laws never centers around
whether there ought to be such laws, but rather what should be
the age limits and what exceptions, if any, ought to be allowed.
The government, in effect, claims the children at all ages. Just as
government claims all our income and exempts some from taxa-
tion, so political leaders claim our children from birth. The debate
in the legislatures is only over the age at which children must at-
tend school, the hours of attendance, and the number of days per
year. The legislature of Virginia has even decreed that school
must not begin until after Labor Day. The purpose of this law is to
“Keep Virginia Green” by luring the local tourists to spend for one
more holiday weekend. Literacy is one thing, but tourism is really
important politically,

The trend in recent years has been to require children to at-
tend school at an ever-younger age. There are several reasons for
this. The humanists want to get'the children away from their par-
ents sooner. They consider some of the kids “hopeless reactionar-
ies” by the time they are six years old. The children have been “in-
doctrinated” so much by their parents that the school has difficulty
re-educating them.

Another reason for earlier school entrance is that more parents
are going to work, so they want taxpayer-financed babysitting as
soon as possible.

Perhaps the most important reason is that if early taxpayer-
financed education isn’t available, the parents will start their chil-
dren in private schools. They may decide to keep them in the pri-
vate schools after they reach the compulsory school age.
