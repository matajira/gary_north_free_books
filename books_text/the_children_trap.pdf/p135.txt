We Must Count the Hidden Costs 15

abuse traceable to the humanist education of the public schools is
beyond calculation. The cost in increased crime and other social
problems (so well documented a century ago in Zachary Montgom-
ery’s Poison Drops in the Federal Senate) is also difficult to calculate.

Suffice it to say that just as there is no such thing as a free
lunch, there ts no such thing as free public schools. They are much more
expensive than we realize. Studies show that they cost two to three
times as much as private schools, and that is being conservative
about it,

By taking so much of our incomes for the government schools,
the state is making it more difficult to afford Christian schools.
The Christian faces double taxation. He pays for the public
schools. Then he pays again for the education of his own children
in a school of his choice. The Roman Catholics have been doing
this for a long, long time. They generally have large families and
many came to this country as poor immigrants. But they have
made the sacrifices to send their children to their own parochial
schools. It isn’t easy, but it can be done.

In a later chapter, I want to offer some very practical advice on
how a Christian can afford to put his children in a Christian
school. Right now, I just want you to realize that public schools
aren't cheap and they aren’t free.

Paying the Price of Liberty

Elsewhere, I have referred to Pastor Everett Sileven’s battle
against the State of Nebraska to keep open his church's school. He
won that fight. Nebraska has had a long history of interfering in
private education. The state once passed a law making it illegal
for any school, public or private, to teach a modern foreign lan-
guage to any student who had not completed the eighth grade. In
Meyer wu Nebraska (1923), the Supreme Court struck down this
foolish and oppressive law. (I like the title of a book by Orville
Zabel, God and Caesar in Nebraska.)

Thanks to our Constitution and a Supreme Court decision
rendered in 1925 (Pierce vs. Soctely of Sisters}, the right to send our
children to a non-public school has been firmly established.
