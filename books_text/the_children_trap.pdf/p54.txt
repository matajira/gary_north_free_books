34 The Children Trap

Summary

The public schools are a battle zone, figuratively and some-
times literally. The war is between two rival views of God. The
source of the law of any system is its god. In humanism, that god
is man.

The Bible teaches that God is God, and He has revealed Him-
self in the Bible. We begin with God and His Word, and we end
with God and His Word. In humanism, we begin with man and
his word, and end with man and his word.

The big lie of the public schools is that the God of the Bible is
irrelevant. The textbooks never mention Him. Everyone assumes
that children do not need to know anything about God, God’s law,
and God’s Word in order to become educated people. This is
Satan's own lie.

The Christian school must offer a better view of education. It
must place God at the center of everything. God’s Word is the
standard, not man’s word. Children must not be taught that man
can know anything he wants without reference to God. Man can
know nothing truly without reference to the God of the Bible.

In Summary:

1. The Bible teaches that parents must instruct their children
in God’s commandments.
2. Education must be God-centered.
3, Parents are in charge of education.
4, Government schools are not God-centered.
5. The god of the public schools is man.
6. Children are not allowed to pray publicly in these schools.
7. God is excluded on campus.
8. Christians need to read the “classroom menu.”
9. The government schools are based on lies.
10. Lie one: God is irrelevant.
11. Lie two: mankind evolved.
12. Lie three: man is his own god.
13. Lie four: the child is inherently good.
14. Lie five: man saves himself.
15. Lie six: there is no hope for the future.
16. Humanism is not the answer.
