Parents in the Driver’s Seat 39

mation, but this skill diminishes over time. (Anyway, I think so. I
read about it somewhere. I forget just where.) Think of how they
can sing advertising jingles that they hear on television —the same
way older folks can remember radio jingles for products long
gone, even if they can’t memorize the books of the Bible very easily.

Dorothy said her daddy has been teaching her the books of the
Bible on the way to school, Little children can learn far more than
we realize, and they seem to be able to do it with minimum effort.
The key is parental involvement.

But understand: a child does not intuitively learn the names
and order of the books of the Bible. Parents have to help. Memor-
ization of important material is a discipline. It is part of a struc-
tured program of teaching. The child does not learn this way on
his own. He does not learn it on a field trip or in some sort of im-
provised “social action” project for four-year-olds, His parents
work with him, and discipline him to learn the material.

Home Schooling

One of the most significant movements in education today is
the home school. It is almost entirely confined-to the Christian
community. It is part of two trends, the general trend toward de-
centralization and the trend of Christians toward setting up alter-
natives to humanist institutions. Both trends are creating trouble
for humanist educators.

John Naisbitt, in his book Megatrends, points out that decen-
tralization is taking place in many areas of our lives. In education,
he cites home schooling as an example. No doubt there is a reac-
tion against centralizing education. We have seen the movement
away from locally controlled small schools to the large consol-
idated school districts. A cabinet level Department of Education
has been established at the Federal level, The home school can be
seen as a wholesome movement in the other direction.

A far more important reason for the home school movement is
the desire on the part of parents to have a direct hand in the edu-
cation of their offspring. There are tens of thousands of children
being taught at home in America today. We don’t know how many
