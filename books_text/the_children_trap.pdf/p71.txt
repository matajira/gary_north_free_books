He Who Pays the Teachers Will Control Education 51

Why should a Christian school be concerned about “material
prosperity”? After all, the school is supposed to be doing the work
of the Lord. A school is not a commercial enterprise. It is not con-
cerned with the bottom line. Who cares whether it makes a profit
or not?

This is the problem with a lot of our thinking. We tend to
equate poverty with spirituality. We suppose that to be poor is to
be godly. In Deuteronomy 28:1-14, God promises His people that
if they obey His commandments, all kinds of blessings will come
to them. Their fields, cattle, sheep, etc. will be blessed by God.
These are material blessings. God gave them a land flowing with
milk and honey. He increased Abraham’s flocks and made him rich
with silver and gold. He prospered Job because Job was obedient.

Jesus says that God’s people are to inherit the earth. (Gary
North’s book on economic principles in the Biblical Blueprints
Series is titled: Inherit the Earth.) God tells us in the Old Testament
that when riches increase we are not to set our hearts upon them.
Jesus warns of the danger of worshipping mammon instead of God.

We should not conclude that because worshipping material
things is wrong, the material universe itself is sinful. Man is a sin-
ner, and the curse is on the ground because of man’s sin. God cre-
ated the material universe for man to cultivate’ and enjoy. Man is
to have dominion over the earth.

What does this have to do with pocketbook control over a
Christian school? I think we need to answer that question by look-
ing further at the reasons for educating our children,

Why Educate Our Children?

‘We teach our children the way of salvation. We teach them to
love God and to serve Him. We want the school to assist us in
helping our children to know God. God has called us to have do-
minion over the earth. Adam was to dress the garden and to keep
it. Work is a basic institution. Man's labor is under the curse
because of Adam’s sin. Man must work to support his family. A
tithe of the increase of his wealth is to be given to God. Some of
his wealth is to be given to the poor.
