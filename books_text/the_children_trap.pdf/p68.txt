48 The Children Trap

Organization

If you want to find out who is calling the institutional tune,
find out who is paying the piper. In education, it isn’t always clear
just who is paying the piper. The less clear it is, the more power
the hired pipers will possess.

As in the court of Nebuchadnezzar, education is delivered by
tutors, The principle of sovereignty is always hierarchical. God
delegates authority to men, and they in turn must delegate
authority to those under them. Tutors possess authority over chil-
dren only to the extent that they have been granted lawful author-
ity by those who are financing the educational system. But if this
control is not actively exercised by those who pay, then the tutors
will be tempted to believe that they are sovereign, just as sinful
man concludes that his own strength has made him wealthy (Deu-
teronomy 8:18). The servants will then begin to dictate terms to
the sovereigns.

This is the age-old story of bureaucracy. In ancient Babylon,
the king could say, “Off with their heads!” In the modern world,
sovereigns can say, “Off with their funds!” If they are in any way
prohibited from saying this, then the bureaucrats will steadily be-
come the sovereigns in fact, if not in law.

He who pays the piper had better call the tunes, or else the
pipers will start calling them, and then demand higher wages for
fewer tunes.

Structuring the Christian School

Christian schools can be organized in several ways. The
school can be under the authority of a church. Elders, deacons,
bishops, or the congregation itself would have control in a church
school. The church might have a schoo! board to look after the poli-
cies and operation of the school. Parents would exercise control in
various ways. They might be officers in the church, might have a
vote in congregational meetings, and might serve on the board.

Another type of organization is a Christian school society
made up of interested parents. The parents select a board which
