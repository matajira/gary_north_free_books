What Can the Church Do? 145

what will happen should be kept in the proper context. The “forth-
telling” is always in a judicial setting. It announces judgment.

When Jonah went to Nineveh, for example, he told them what
would happen if they did not repent. Here is what he said,

“Yet forty days and Nineveh will be overthrown” (Jonah 3:4),
What did he mean? Jonah told them that God was going to judge
them for their sins if they did not repent. This is what we lack in
our culture.

Soft-Core Salvation

A few weeks ago, I saw one of the nation’s leading evangelists
on a television talk show. The interviewer was definitely not a
Christian. He came right out and asked the evangelist if he
thought that AIDS is a judgment sent by God. The evangelist
said, “No, God wouldn’t do such a thing.”

Odd, isn’t it? This evangelist believes that God will send mil-
lions and perhaps even billions of people to hell for all eternity, to
be forced to tolerate screaming intolerable agony and loneliness
forever, without hope forever. But God wouldn’t send a few thou-
sand homosexual perverts the plague of AIDS as a warning to
them and others of judgment to come.

Compared to hell, AIDS is nickel and dime sort of judgment.
But the evangelist is afraid to admit in public that God judges sin-
ners in history as a down payment on future eternal judgment, or
to warn them about the coming eternal judgment, God’s judg-
ments are somehow outside of history. It’s pie in the sky by and
by, and it’s boil in the oil in the soil.

But for the present, judgment is all very distant. Why, AIDS
is no more a judgment of God than, say, herpes.

With respect to his doctrine of history, our evangelist is a lib-
eral, and a wimpy liberal at that. Liberals don’t want God’s judg-
ment in history, either. Such judgment points to hell, and this is
the offending doctrine for liberals, Also for atheists.

History belongs to man.

In our, society, there is usually no connection between what
happens in the world and God’s law. This vital cause/effect rela-
