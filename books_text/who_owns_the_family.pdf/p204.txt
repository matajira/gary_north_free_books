178 Who Owns the Family?

local, 160

spirituality, 157
Gothard, Bill, xii
Graduated Income Tax, 54
Great Saciety, xix
Griswold v. Connecticut, 74
Guggenheim, Meyer, 112
Gun Control, 164

Healing, 149-150
HBO, 118
Hell, x, 145
Henry, Matthew, 110
Herpes, 72
HEW, xvii
Hierarchy
in Deuteronomy, 7
in the family, 9
Hippocratic Oath, 63
Home-Schooling, 85
Homosexuality, 79, 145
Resolution against, 160
Household
Baptism, 109
God's, 103
family, 103
Humanism
Definition, 19°
Tactics, 78
Humanist Manifesto, 89
1 & HH, 92
I, 70-77
False Continuity, 96
False Ethic’s, 94
False Hierarchy, 93
False Sanctions, 95
Human Rights, 17
Hunt, H.L., 113
Hypocrisy, 135ff

Illegitimate Births, xix
Image of God, 34
Imprecatory Psalms, 1524

Incest, 79
Incorrigibility, 41
Industrial Revolution, 108
Inerrancy, 132
Inheritance
conditional, 59
Living Trust, 59-60
problems, 55
significance of, 51
Tangible/Intangible, 57-59
Institutional Monopolies, ix
Internal Revenue Code, 100
Islam, 112

Jackson, Stanley, 114
Jordan, James, 70, 71

Kidnapping, xxi
Kinsey, 75

Legal Ammunity, 21
Levirate Marriage, 48
Lesbianism, 79

Mafia, 116
Manipulative, 31
Mann, Horace, 85
Marriage

Covenant, 4, 8f

licenses, 5

no contract, 3

Puritan, 4-5

Purpose, 80
Marxism

Logic of, 69

predestinarian, 112

theory of family, xi
Maynard v. Hill, 3, 120
Mead, Sidney, xii
Military Academies, 87
Monogamy, 147
Moral Environment, 90, 135f
Morality

legislation of, 156
