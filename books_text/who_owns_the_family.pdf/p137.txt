Who Owns the Future Generation? Mii

Anyone for a Covenantal Dynasty?

Scripture makes it clear that there are three ingredients neces-
sary to build a Christian dynasty. Of Abraham, the Bible says,
“For I have known (determined) him, in order that he may com-
mand his children and his household after him, that they keep the
way of the Lord, to do righteousness and justice, that the Lord may
bring to Abraham what I am doing” (Genesis 18:19). (emphasis
added)

Since all true believers are Abraham's “seed” (Galatians 3:29),
three relevant points stand out for us today: destiny, discipline,
and dominion. God had a will and plan for Abraham that gave
him destiny. He disciplined his children. And, he took dominion
through them. Anyone who wants to extend his faith from one
generation to another must inculcate these elements into the
future generation.

The curious thing, however, is that non-Christians are often
more successful at extending their religion and morals over the
generations. Some of these families have been so successful they
have become dynasties.

In May of 1979, the Wall Street Journal ran a series of articles
titled, “Founding Families” (May 7, 1979). It was an interesting
series on the great American dynasties like the Cabots, Astors,
and Rockefellers, In many respects, the-articles in this series are a
case study of both the oddities and distinctives of these great fami-
lies. Yet, I found that this series, combined with further study of
dynastic families, revealed definite patterns. To the degree that a
family was able to cultivate these features, it was successful in be-
coming a dynasty.

Destiny

First, the belief in destiny. The leading successful dynasties
have been people of destiny —at least they perceived themselves
that way. To believe in destiny means that one conceives of some-
one, or something, having determined his life for some special pur-
pose, Whether one believes he is destined by fate, God, or chance,
