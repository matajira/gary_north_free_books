52 Who Owns the Family?

Whether you realize it or not, this all goes back to a very im-
portant principle of inheritance. And, what we find true-on the
larger scale, is valid on the smaller, the family.

The principle of inheritance, as a matter of fact, finds its
origin in the family. Think what it would be like if families could
build up an inheritance and send their children off into the future
with the foundation of the past. It would mean that they would
have a better start. Like the third man in the relay race, he has a
better chance of helping his team to victory if the second man
passes the baton to him ahead of the other team’s second man.

Somehow, we all sort of intuit that a better start is more likely
to mean a better jinish. That is, all of us except the Federal Gov-
ernment. The Federal Government has decided that “in order to
keep the race fair,” if one runner gets too far ahead of the competi-
tion, he will be required by law to slow down before he passes the
baton.

What do I mean?

For almost 80 years a new philosophy about inheritance has
entered our society. It all began around the turn of the century.

Income Tax

“The Congress shall have power to lay and collect taxes on in-
come from whatever source derived, without apportionment
among the several States, and without regard to any census or
enumeration” (The 16th Amendment to the Constitution of the
United States),

The 16th amendment altered completely this country’s view of
inheritance and the family. It is not like the other court cases I've
presented in this book, but it is definite /egat action that affected
family life in America.

How?

There’s no better way to establish my point than to allow a
statement made by President William Howard Taft, made in
June, 1909, in a signed message to the Senate and House of Rep-
resentatives. His statement recommended the imposition of a
Corporation Excise Tax, but his comments speak to proposed in-
