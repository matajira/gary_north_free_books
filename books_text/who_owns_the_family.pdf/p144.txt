118 Who Owns the Family?

eral Government would find it very difficult to block the sale and
installation of reception dishes: “freedom of speech, freedom of
religion.”

There was another secret of his success. When local cable sta~
tions were beginning to start up, owners knew there would be
howls of righteous protest against soft-core pornography and vio-
lence. R-rated movies were scheduled for broadcast by HBO and
its competitors, and these would be the financial backbone of ca-
ble television’s late-night programming. The cable operators
needed a way to deflect criticism. Pat Robertson became every
operator’s excuse.

“Look,” he could say to local regulators, “we offer a wide variety
of programming, We have something for everyone. We have
Christian programming, too.” Guess whose Christian program-
ming? The man who got there first. Pat Robertson, being a man of
destiny and discipline, had seen the opportunity. Even though
HBO was going to show unacceptable movies — movies that Rob-
ertson would not have allowed ta be shown if he had possessed the
power to stop them —he still offered his ftee programming to cable
stations.

The result: every station that picked up HBO also broadcast
the 700 Club to subscribers as a free bonus. Not only did that
become the stepping stone to the broad popularity of the station,
but many have been converted as a result. This is the kind of dis-
cipline that, coupled with destiny and dominion, builds dynasties.

We should understand, however, that the advent of sin into
the world corrupted man’s will to dominate. The cross of Christ,
however, renewed not only the will but the ability to dominate.
Regrettably, the modern Christian family has forgotten or hidden
from what the unbeliever is self conscious about.. The great
dynastic families, without a shadow of a doubt, attempt to domi-
nate. They see nothing wrong with dominion, with wanting do-
minion, with having dominion, with studying dominion prin-
ciples, and consequently, they have the dominion.

For that matter, the basic patterns we have observed in mostly
pagan family empires—destiny, discipline, and dominion—have
