BIBLIOGRAPHY

Adams, J. E, Christian Living in the Home. Phillipsburg, New Jer-
sey: Presbyterian and Reformed, 1972.

Adams, Blair, and Stein, Joel. Who Owns the Child. Grand Junc-
tion, Colorado: Truth Forum, 1983,

Bromiley, Geoffrey, Ged and Marriage. Grand Rapids: Eerdmans,
1980.

Johnston, O. R. Who Needs the Family?. Downers Grove, Ilinois:
InterVarsity Press, 1979.

Kilpatrick, William Kirk. Psychological Seduction. Nashville:
‘Thomas Nelson, 1983.

Morgan, Edmund, 8. The Puritan Family. New York: Harper &
Row, 1944.

Murray, Charles. Losing Ground: American Social Policy 1950-1980.
New York: Basic Books, 1984.

Plymouth Rock Foundation. Biblical Principles. Plymouth, Massa-
chussetts: Plymouth Rock Foundation, 1984.

Robison, James. Attack on the Family. Wheaton, Illinois: Tyndale
House, 1980.

Whitehead, John, W. Parents’ Rights. Westchester, Illinois: Cross-
way Books, 1985.

171
