Conclusion 121

The principle in this chapter is that God is the authority.
Neither State nor family have final authority over the family. God
does, and God entrusts the family with a “delegated authority.”

Principle 3; the principle of law. The chapter is called “By What
Standard?” I wanted to emphasize that the State does not deter-
mine the standard for the family. I used the famous, Nebraska ex
rel. Douglas x Faith Baptist Church. The issue was: “by what stand-
ard” will children of the Church be raised and educated? Faith
Baptist Church argued that their standard was the Bible.

Since God has delegated His standard to the family, as well as
to every sphere of society, I used the Ten Commandments as a
guide, relating each commandment to show the ramifications of
Biblical law.

Principle 4: the principle of sanctions. I referred to the amazing
“Snyder Case” where parents tried to take their “incorrigible”
daughter to the State for disciplinary help. Instead of helping
them, the State bent the law to take their daughter away.

The Bible gives parents the authority to discipline. Sure, they
can give an “incorrigible” teenager over for serious discipline, But
the State has no right to take children from parents, unless the
parents are genuinely “abusing” the child. So that Biblical disci-
pline is understood, I presented Biblical methods of punishment.

Principle 5: the principle of inheritance. “Family Inheritance”
was the title of this chapter. Here, I presented the damaging
legislation of the 16th Amendment, the income tax law. Why was
it so damaging? It was made a tax on success, the first “graduated
tax” in our nation’s history. This cut into the “inheritance” of
families,

Why is inheritance important? It is a powerful tool for
building up families over generations. Of course, inheritance is in-
tangible (character traits, etc.) and angible. If parents can pass ona
spiritual and fiscal legacy, they are arming the next generation
with an inheritance that can overcome the world, the flesh, and
the Devil. In the last half of this chapter, using the inheritance
passages of the Bible, I summarized some guidelines for parents
to keep in mind.
