A Covenant, Noi a Contract 9

*This is now bone of my bones
And flesh of my flesh;

She shall be called Woman,
Because she was taken out of Man.”

Therefore a man shall leave his father and mother and be joined to
his wife, and they shall become one flesh. And they were both naked, the
man and his wife, and were not ashamed (Genesis 2:18-25).

1, Transcendence (Genesis 2:18). The marriage covenant
begins, “Then God said.” Notice the similarity between this com-
ment and the beginning of Deuteronomy. God’s Word establishes
the covenant in both cases.

God created the family, making it a “Divine” institution. I
don’t mean “Divine” in the sense that it becomes God. Rather, it
has a Divine origin. In the traditional church wedding ceremony,
the minister quotes Jesus and says, “What God has joined
together, let not man separate” (Mark 10:9). God forms a mar-
riage union, just as He created the first one.

This is the family’s greatest defense against an encroaching
State. God gave the parents a Divine trusteeship. When the State
attacks the family, it makes war with a sacred covenant; it steps
onto a battlefield with God.

2. Hierarchy (Genesis 2:19-22). Biblical hierarchy has to do
with authority. What is the authority of the family? It has been
given an assignment of subduing the earth to the glory of God.
We sometimes call this the “cultural mandate.” Not just the male,
but male and female are called to have authority and dominion
over the earth. God said, “Let Us make man in Our image, ac-
cording to Our likeness; let them (male and female) have domin-
ion over the fish of the sea, over the birds of the air, and over the
cattle, over all the earth and over every creeping thing that creeps
on the earth” (Genesis 1:26).

God shows Adam this hierarchy by giving him the job of nam-
ing the animals. Adam learns two things. One, he has authority
over all the’earth, Two, he cannot fulfill the mandate by himself,
He needs a partner.
