EDITOR’S INTRODUCTION
by Gary North

And do not fear those who kill the body but cannot kill the soul.
But rather fear Him who is able to destroy both soul and body in
hell (Matthew 10:28),

God has established three institutional monopolies: family,
church, and state. Each of these is a God-ordained government.
Each of these is a covenant. There are only three institutional cov-
enant governments. A covenant is always marked by an oath,
either explicit (church, family) or sometimes implicit (state citi-
zenship) and sometimes explicit (state law court). Each of these
three governments is to protect the other, and each deserves pro-
tection from the other.

The oath is central to any covenant. It involves calling down the
wrath of God on the oath-takers, in time and in eternity, for any viola-
tion of the terms of the oath. When the people’s fear of God’s
judgement when violating the terms of the oath declines, the
power of the covenant institution also declines.

In the twentieth century, the premier institution in the minds
of most people is the state, or civil government, We live in the era
of the power religion. The power religion teaches that might
makes right, and therefore might is right. The logical goal, obvi-
ously, is to seek power. If anyone chooses not to seek power, then
he becomes tempted by the other great religion of the day, the
escape religion. That person seeks to get away from those who use
power.

There is a third religion: Biblical dominion religion. It seeks
God’s comprehensive kingdom, a visible as well as invisible

ix
