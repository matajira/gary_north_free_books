144 Who Owns the Family?

unworthy manner, eats and drinks judgment io himseif, not discerning
the Lord’s body. For this reason many are weak and sick among you,
and many sleep. For if we would judge ourselves, we would not be
judged. But when we are judged, we are chastened by the Lord,
that we may not be condemned with the world (I Corinthians
18:27-32).

Three, discipline opens and shuts the door to the kingdom, It
can open the door in that it retrieves a wayward brother. James
says, “Brethren, if anyone among you wanders from the truth,
and someone turns him back, let him know that he who turns a
sinner from the error of his way will save a soul from death and
cover a multitude of sins” (James 5:19-20).

Discipline can also shut the door in an attempt to restore, Paul
says:

It is actually reported that there is sexual immorality among
you, and such sexual immorality as is not even named among the
Gentiles—that a man has his father’s wife! . . . deliver such a one
to Satan for the destruction of the flesh, that his spirit may be saved
in the day of the Lord Jesus” (I Corinthians 5:1-5).

So, the Church, and the Church alone, is given the power of
the “keys.” With preaching, the sacraments, and discipline, it
destroys hell,

The program for returning ownership of the family back to the
rightful trustees, therefore, turns on these three “keys.” If the
Church is going to affect the State, it’s going to have to use these
“keys.” There's simply no other Biblical way. In the following, I
would like to outline several things in each area that would change
the Church, and consequently change society. That is, if we are to
believe that it is the institutional Church that destroys hell, then
the change will have to come from the “keys” given to it, Here is
what the Church can do.

True Prophetic Preaching

Too often, prophecy is viewed only as “telling something that
will happen in the future.” Certainly this is one aspect. But, telling
