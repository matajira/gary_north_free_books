Who Owns the Future Generation? 119

been generally recognized by them. They may not call these prin-
ciples by the same names, but the point is that pagan dynasties
are self-conscious. The average Christian family is not even aware
that God wants his family to grow and have great influence for
generations. Not just one generation. Generational influence is
powerful. The Christian family, therefore, fails by default.

Summary

T have tried to answer one basic question: Who owns the
future generation?

1. L answered this question by beginning with the case involv-
ing the father of a girl who was already in the covenant. A minis-
ter wanted to re-baptize her. When this situation went to court, it
was ruled that the girl was in the covenant already. This indicates
@ certain rationale.

2. Icall it the generational principle. Children of believers are
to be placed in the covenant and trained in the ways of the cove-
nant. God owns the future through the principle of generational ex-
pansion of the faith. That's why the State tries to curb this kind of
growth, any way it can. For this reason, we need to become self-
conscious about building for the future through our children.

3. A covenantal dynasty. I pointed out three Biblical precepts
involved in generational growth: destiny, discipline, and domin-
ion. This world belongs to God’s people. It will be here until all
things are under the dominion of Christ. The way to arrive at vic~
tory is through the raising up of a “holy seed,” from generation to
generation.

This concludes our “ten principles.” Now its time to apply this
information and see what the family, Church, and State can do to
put ownership back into the proper hands. Before we do that,
however, I want to summarize in the next chapter what we have
covered so far.

Can you name all ten principles? Can you remember some of
the court cases? Can you recall the five-fold covenantal model?
Let’s turn to the following chapter, and review before we move on
to the “applications” section.
