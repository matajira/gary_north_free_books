Who Owns Sexual Privacy?

Homosexuality and Lesbianism:

Therefore God also gave them up to uncleanness, in the lusts of
their hearts, to dishonor their bodies among themselves, who ex-
changed the truth of God for the lie, and worshipped and served
the creature rather than the Creator, who is blessed forever. Amen.
For this reason God gave them up to vile passions. For even their
women exchanged the natural use for what is against nature. Like-
wise also the men, leaving the natural use of the woman, burned in
their lust for one another, men with men committing what is
shameful, and receiving in themselves the penalty of their error
which was due (Romans 1:24-27).

Adultery:

If a man is found lying with a married woman, then both of
them shall die, the man who lay with the woman, and the woman
(Deuteronomy 22:22).

Pre-marital sex with engaged and not-engaged virgins:

If there is a girl who is a virgin engaged to a man, and another
man finds her in the city and lies with her, then you shall bring
them both out to the gate of that city and you shall stone them to
death; the girl, because she did not cry out in the city, and the
man, because he has violated another man’s wife. Thus you shall
purge the evil from among you. But ifin the field the man finds the
girl who is engaged, and the man forces her and lies with her, then
only the man who lies with her shall die. But you shall do nothing
to the girl; there is no sin in the girl worthy of death, for just asa
man rises against his neighbor and murders him, so is this case.

If a man finds a gir] who is a virgin, who is not engaged, and
seizes her and lies with her and they are discovered, then the man
who lay with her shall give to the girl's father fifty shekels of silver,
and she shall become his wife because he has violated her; he can-
not divorce her ail his days (Deuteronomy 22:23-29).

Incest:

None of you shall approach anyone who is near of kin to him,
to uncover his nakedness; I am the Lord (Leviticus 18:6).

79
