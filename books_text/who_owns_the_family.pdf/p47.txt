By Whose Authority? 21

outside of this legitimate family authority are still called illegiti-
mate.) Negatively, the family bears the “rod.” The Bible says that
the “parent” can take the rod to his child. Proverbs, addressed to
“fathers,” says, “Foolishness is bound up in the heart of a child, but
the rod of correction will drive it far from him” (Proverbs 22:15).

Positively, the Church has a monopoly of administering the
sacraments. Negatively, only the Church is given the “keys of the
kingdom.” Jesus says, “I will give you (Peter representing the
Church) the keys of the kingdom of heaven, and whatever you
bind on earth will be bound in heaven, and whatever you loose on
earth will be loosed in heaven” (Matthew 16:19). “Keys” lock and
unlock doors, So the Church has the “delegated” authority to ad-
mit and dismiss (excommunicate) from the Church.

Positively, the State has a monopoly of providing protection
unto death, Negatively, only the State can execute, or wield the
“sword.” When it comes to “civil” disobedience, the State is given
this authority. Paul says, “For he (Civil Magistrate) is God’s min-
ister to you for good. But if you do evil, be afraid; for he does not
bear the sword in vain; for he is God’s minister, an avenger to exe-
cute wrath on him who practices evil” (Romans 13:4).

A seeming exception to this jurisdiction of the sword is the so-
called “right of self-defense.” It isn’t an exception. Again, we
should not speak of human rights; we should speak of legal im-
munities. There is a person's legal immunity against being executed
for killing someone who has threatened his or someone else’s life,
but this “right” of self-defense—this legal immunity from the
State’s execution —is delegated to him by God through the State, in
whose name he is acting. He is not acting as a father or as a Church
officer; he is acting as an agent of the State. The State establishes
limits on such actions, even in the household (Exodus 22:2-3).

This distinction is important to emphasize in our era, which is
noted by a revival of radical familism or “clan-ism,” in which
violence-prone men defend their supposed right to execute others
as an attribute of the family or some hypothetical tribal Church. I
refer here especially to a minority of radical tax protestors whe
proclaim violence against the State in the name of theological doc-
