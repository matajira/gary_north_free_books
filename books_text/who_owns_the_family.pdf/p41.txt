2
BY WHOSE AUTHORITY?

Who or what has authority.over the family?

The State? The Church? The family itself?

The whole question of authority hag been a big issue in the
area of education. Unfortunately, controversies in the public
school systems have been determining parental authority.

In 1985 a huge dispute in the State of New Jersey was ruled on
by the Supreme Court. It seems one of the teachers went too far in
searching a student, The parents complained and eventually the
matter ended up in court, New Jersey x. T.L.O. The decision of the
case restricted public school officials.

Sounds good! But wait till you hear how the public schools
were limited: not by the prior authority of parents, but by the
more important authority of the civil government in general.
Here is part of what the court said through Judge White:

“Teachers and school administrators, it is said, act én loco paren-
tis in their dealing with students: Their authority is that of the
parent, not the State. . . . Such reasoning is in tension with con-
temporary reality and the teachings of the court. . . . If school
authorities are state actors for purposes of the constitutional guar-
antees of freedom of expression and due process, it is difficult to
understand why they should be deemed to be exercising parental
rather than public authority when conducting searches of their
students.

“More generally, the Court has recognized that ‘the concept of
parental obligation’ as a source of school authority is not entirely
‘consonant with compulsory education laws.’. . . Today’s public

15
