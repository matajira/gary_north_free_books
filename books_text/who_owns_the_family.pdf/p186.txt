160 Who Owns the Family?

into laws, At the local level, you can use your own precinct in-
volvement to submit key resolutions. ve already suggested that
the application of the capital offense laws would go a long way.
But we're a “long way” from that happening. So, what should we
push for in the mean time? In the following, I am suggesting 10
resolutions that are already being applied at the local precinct
level in Texas, and winning.

Why the local and not the federal level? ‘The present direction
of our politics is shifting back to “State control.” This is good, and
it means that the way to affect your local situation best is through
local expressions of political parties. Forget about the federal level for
the time being. You can’t win there anyway. But you can win at the
local level. So, here are 10 suggested resolutions that will move
our society closer to one that puts family trusteeship back into the
hands of parents.

Resolution On Opposition To Minority Status For Homosexuals

WHEREAS, the practice of homosexuality is an abomination
before God and is indicative of a society’s moral decadence, and
leads to the spread of severe diseases, such as AIDS; and

WHEREAS, the legalization of the practice of homosexuality
would confer public acceptability to this activity and would lead in-
exorably to the breakdown of the traditional family unit and subse-
quently to the destruction of our nation; and

WHEREAS, state officials are refusing to defend anti-sodomy
laws which are being challenged by the practitioners of homosex-
ual conduct in the federal courts; now, therefore

BE IT RESOLVED, that the party in precinct #_____ calls
upon the Governor and other state officials to defend the present
anti-sodomy statutes against the challenge which it is experiencing
in the federal courts, and further calls upon civil magistrates at all
levels to denounce this activity and to maintain and strictly enforce
laws prohibiting homosexual conduct; and

BE IT FURTHER RESOLVED, that homosexuality should
not be taught or modeled as an alternate lifestyle in our public
schools, nor should marriages between homosexuals, nor should
the adoption of children by homosexuals be allowed in our State or
Nation; and
