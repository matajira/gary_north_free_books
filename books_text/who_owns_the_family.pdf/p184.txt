158 Who Owns the Family?

I think it’s time we start throwing our political weight around.
But we've got to know what to say to the State. We need to make
sure that we lay out a Biblical agenda. We have to work within the
political arena with this agenda.

Nothing else will work when it comes te the State. If Chris-
tians want the State to change, they'll have to become folitical stew-
ards, I don’t mean they have to stop being spiritual. Heaven for-
bid. No, they will have to apply their spirituality to the political
arena. For you to become a political steward, working within the
political arena, you will have to take the following minimum
steps.

1. Register to vote. You would be surprised how many Chris-
tians are not even registered to vote. The liberals go, around in
cars and round up people to register. You need to round up every-
one in your Church and make sure he is registered. Tell your
pastor to announce it from the pulpit. Have seminars on voting
procedures. Learn when the voter registration deadlines fall.

2. Pick a party to work in. Try to choose a party that best rep-
resents your Christian beliefs. As of the moment, the Republican
Party is the best choice because it is willing to incorporate pro-life
resolutions in its party platform. If you happen to choose another
patty, make sure you think through a strategy for working your
way to the top of the influential ladder. By the way, if you decide
to become a “mole” inside a liberal party, realize that it will proba-
bly take much longer and a lot more patience. I don’t recommend
this approach unless you’re extremely experienced and knowl-
edgeable.

The long-term goal is to make every political party a self-
consciously Christian party, just as it’s the goal to make every in-
stitution self-consciously Christian. The goal is to subdue the
whole earth. No loopholes, no escape hatches, no “king’s x” from
the King of kings. But it’s easier to subdue your back yard before
you subdue the Sahara desert. Practice in your back yard.

3. Attend precinct meetings. Our political system is designed
to operate from the bottom up. Call the local party headquarters to
find out what precinct you're in. You can probably find the party’s
