What Can the State Do? 163

Resolution On The Sovereignty Of The Family

BE IT RESOLVED, that the party in Precinct #____ rec-
ognizes that the monogamous family is a God-ordained institution
and is one of the foundational units of society. The family is pri-
marily responsible for the welfare, education, and property of its
members. (The family is defined as those related by blood, mar-
riage or adoption.) All attempts to weaken or destroy the family,
including no-fault divorce, desertion, pornography, homosexual-
ity, adultery, and numerous forms of governmental interference
and control over the family must be opposed.

This resolution is a definition of the family that protects its
Biblical prerogatives. [f you can get this one through, you'll neu-
tralize a lot of legislation against the family.

Resolution On Pro-Life

WHEREAS, God is the author of life and that human life orig-
inates at conception; now therefore,

BE IT RESOLVED, by the party in Precinct #_______ that
abortion must be opposed as the shedding of innocent blood which
will surely bring God’s judgement upon our nation; that a Human
Life Amendment to the U.S. Constitution must be adopted by the
U.S. Congress to protect innocent human life from the point of
conception until the time of natural death; and that we call upon
our State Legislature to pass appropriate legislation to this end.

Let’s face it, even if the Congress passes a pro-life law, it will
have to be adopted by the individual states. The battle is going to
boil down to your local community, Regardless of what the Feds
do, the local citizenry will have to re-criminalize abortion.

Resolution On Non-Staie Schools

WHEREAS, education is the primary responsibility of
parents, and that parents, not the state, are the stewards of the
children, now therefore,

BE IT RESOLVED, that the party in Precinct #____ op-
poses all attempts by the state or local government to interfere with
parental rights in education; and that we further support main-
