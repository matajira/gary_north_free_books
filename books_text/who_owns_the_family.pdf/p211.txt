What Are Biblical Blueprints? 185

given dominion on the earth to subdue it for God’s glory. “So God
created man in His own image; in the image of God He created
him; male and female He created them. Then God blessed them,
and God said to them, ‘Be fruitful and multiply; fill the earth and
subdue it; have dominion over the fish of the sea, over the birds of
the air, and over every living thing that moves on the earth’”
(Genesis 1:27-28).

Christians about a century ago decided that God never gave
them the responsibility to do any building (except for churches).
That was just what the humanists had been waiting for. They im-
mediately stepped in, took over the job of contractor (“Someone
has to do it!”), and then announced that they would also be in
charge of drawing up the blueprints. We can see the results of a
similar assertion in Genesis, chapter 11: the tower of Babel. Do
you remember God’s response to that particular humanistic pub-
lic works project?

Never Be Embarrassed By the Bible

This sounds simple enough. Why should Christians be embar-
rassed by the Bible? But they are embarrassed . . . millions of
them. The humanists have probably done more to slow down the
spread of the gospel by convincing Christians to be embarrassed
by the Bible than by any other strategy they have adopted.

Test your own thinking. Answer this question: “Is God mostly
a God of love or mostly a God of wrath?” Think about it before
you answer.

It’s a trick question. The Biblical answer is: “God is equally a
God of love and a God of wrath.” But Christians these days will
generally answer almost automatically, “God is mostly a God of
love, not wrath.”

Now in their hearts, they know this answer can’t be true. God
sent His Son to the cross to die. His own Son! That’s how much
God hates sin. That’s wrath with a capital “W.”

But why did He do it? Because He loves His Son, and those
who follow His Son. So, you just can’t talk about the wrath of God
without talking about the love of God, and vice versa. The cross is
