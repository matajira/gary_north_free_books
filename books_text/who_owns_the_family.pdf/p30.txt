4 Who Owns the Family?

I know it’s hard to believe that the Supreme Court once said
such a thing, but it’s true. There is real legal precedent that mar-
riage is not just a contract.

Well, if it's not a contract, then what is it? The language of the
Supreme Court is that it is an institution. So what? Since marriage
is an “institution,” it is a sacred covenant.

A Sacred Covenant

A few years ago, I was with some friends. After dinner, I hap-
pened to notice a faded-out, old document on the table. I couldn’t
make out the words, but in big bold letters at the top of the docu-
ment it said, “Marriage Covenant between. . . .”

I couldn't believe my eyes. I asked my friend what this was.
He told me it was a marriage covenant, filed at the local court-
house, between his great, great, great grandparents.

An immediate question popped into my head: “When did peo-
ple stop drawing up marriage covenants?”

The answer to that question tumed out to be a fascinating
study, one that I can’t go into at this point. But it is quite clear that
marriages in this country, 100, 200, and even 300 years ago, were
viewed as sacred covenants. Below, you'll find a covenant dating
from 1664/1665. (Two years are listed because in the old days— the
17th century —the shift to the modern calendar hadn't taken place.
The new year came in March. So what they called 1664, we call
1665.) Even though it is called a “contract,” notice that the word-
ing of the document itself calls it a covenant.

Marriage Contract
January 20, 1664/65

Plymouth, Massachusetts

This writing witnesses, between Thomas Clarke, late of
Plymouth, yeoman, and Alice Nicholls of Boston, widow, that,
whereas there is an intent of marriage between the said parties, the
Lord succeeding their intentions in convenient time; the agree-
ment, therefore, is that the housing and land now in the possession
