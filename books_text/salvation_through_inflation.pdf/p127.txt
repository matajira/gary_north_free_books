Who Represents the Consumers? 95

another, exercises sovereignty. He wrote that “the existing
system does not distribute the control of intermediate produc-
tion to individuals at all; and, secondly, gives them no say what-
ever as to the quantity, quality or variety of ultimate prod-
ucts.”* Under capitalism, consumers have no say whatever!

No say whatever? But I do have a say. It’s called, “Pll buy it.”
It’s also called, “No, I won't buy it.” You possess the same
power. This is the heart of capitalism: consumer sovereignty.

Capitalists are the economic agents of future consumers.
Those capitalists who guess wrong or plan wrong about what
we consumers are willing and able to buy will suffer losses. This
is the hammer that we consumers hold over producers. Because
of the free market’s system of profit and loss, consumers control
producers. Economist Ludwig von Mises has described the
relationship between producers and consumers: “In the capital-
ist system of society's economic organization the entrepreneurs
determine the course of production. In the performance of this
function they are unconditionally and totally subject to the
sovereignty of the buying public, the consumers.” In other
words, capitalist owners are legally sovereign over the produc-
tion process, but they are not economically sovereign. Consum-
ers are economically sovereign over the process.

Legally, producers represent themselves if they own the tools
of production, but economically, they represent consumers.
Understanding the difference between legal representation and
economic representation is crucial for understanding capitalism.
The link between the two kinds of representation is capitalism's
system of profit and loss. By buying from producers who in the
consumers’ opinion serve them best, consumers reward certain

38, Conirol and Distribution of Production (2nd ed.; London: Stanley Nott, 1934),
p. 10.

34. Ludwig von Mises, “Profit and Loss" (1951), in Mises, Planning for Freedom
(4th ed.; South Holland, Illinois: Libertarian Press, 1980), p. 108. The publisher is
now located in Grave City, Pennsylvania.
