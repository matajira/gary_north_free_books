292 SALVATION THROUGH INFLATION

credit masters
above voters, 191
bureaucrats, 77-78
centralization, 186
fear of, 216
formula, 228
free market, 190
hammer, 186
hierarchy, 218
inventory, 186, 234-35, 258-

59

powers of, 60
representation, 193
sovereign, 203-4
valuation, 200

crisis, xx

cults, 242

curse, 44, 45

cycle, xvi

Darwin, Charles, 78
Darwinism, 20, 211
decapitalization, 199-203
decentralization, 185, 186
deflation, 12-13
demand, 113, 195-97
democracy, 90, 153-54
demons, 143-44
depositors, 204, 206
depreciation, 122-28, 151
depression, 112
distribution, 106-18
dividend, 66-67, 169, 172
division of labor, 112-13, 145,
147
dole, 73-75, 78, 213-14 (see
also National Dividend)
Douglas, C. H.

A&B Theorem, 243-57
Aberhart &, 57
anti-banking, 204
anti-biblical law, 83
anti-Christianity, 101
anti-democracy, 190-91, 193
anti-Semitic, 220-23
anti-taxes, 214-15
anti-work, 144, 211-12
bankers = owners, 90
biography, 34-35
blueprint, 185-86

capital goods, 256
capitalism is bad, 165
capitalism's failure, 11]
capitalism’s flaw, 242, 252
capitalism’s history, 166-69
conspiracy theory, 221
consumer credit, 234
consumer sovereignty, 206
consumers’ democracy, 153
contradictory, 251-52
credit policy, 162

cultist, 242

Darwinist, 211
decentralization, 185-86
democracy, 90

diagram, 256-57

dividend defined, 177
dividends & income, 172
dividends & wages, 172
dole, 213-14

elitist, 190-91, 206, 228
entrepreneurship, 119
errors of, 268-71

ethics, 210

exploitation, 182-83
exploitation theory, 180, 247
