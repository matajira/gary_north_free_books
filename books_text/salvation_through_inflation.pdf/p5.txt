This book is dedicated to
Don Bell

whose journalistic integrity is
matched only by his dogged
persistance — a model for all
of us professional scribblers.
