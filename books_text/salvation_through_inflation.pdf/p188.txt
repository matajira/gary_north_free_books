156 SALVATION THROUGH INFLATION

I Want Int

Consider a large auditorium. It has 5,000 seats. On a partic-
ular evening, a promoter has scheduled the appearance of the
nation’s most popular entertainment group. There will be one
performance only in this city. A ticket entitles the bearer to
occupy a particular seat during the performance. The ticket is
a convenient device for allocating seats in advance. Instead of
hiring people to take money from would-be members of the
audience, one by one, in front of each seat, the manager of the
auditorium sells tickets in advance. The tickets function as
warehouse receipts, except that in this case, people want to sit
in the “warehouse.”

What is a ticket worth? As with every other resource, a ticket
is worth whatever some buyer will pay for it. Prior to the per-
formance, it may be worth a person’s income for a week. Im-
mediately after the performance, a ticket is worth whatever a
collector of memorabilia is willing to pay for it.

The problem of allocating seats is not solved merely by prin-
ting up tickets. It is solved by selling them. Again, remember
my economic policy parrot: “High bid wins!” The seller of seats —
actually, he rents the seats — offers tickets for sale that legally
entitle the holders to specific seats during the performance.
The auction for seats now goes to work. If the tickets are priced
too low, there will be people waiting outside to buy a ticket
when the performance begins. If the promoter has priced the
seats too high, the auditorium will have empty seats.”

22. The economic function of ticket “scalpers” is to transfer some of the risk
from the promoter to the scalper. When he sells a ticket to a scalper, the salesman at
the gate becomes a wholesaler, Those buyers who are willing to stand in line and buy
blocks of tickets in advance can then enter the market and sell these tickets to late-
comers or others who are willing to bid for them. If demand is heavy, scalpers make
money. [fit is lower than expected, scalpers lose money, The promoter is more likely
to sell out all the seats when scalpers buy blocks of tickets. Also, the scalpers get all
the bad publicity as price gougers if the promoter has priced the tickets too low.
