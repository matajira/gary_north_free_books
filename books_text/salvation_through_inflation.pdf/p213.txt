Dividends Under Capitalism 181

Worse, the trusts and banks then suppress output. They get
rich only when others get poor. This, too, is Marx's argument:
the supposed impoverization of the proletariat, the declining
condition of the working class. Marx wrote that “the general
tendency of capitalistic production is not to raise, but to sink
the average standard of wages, or to push the value of labour
more or less to its minimum limit.”* Douglas wrote: “Similarly,
the Trusts and Banks, obliged, as a condition of existence un-
der the system, to reabsorb the majority of the credit distribut-
ed as wages, through the agency of prices, restricted the supply
of ultimate commodities, not only by their own forms of sabo-
tage, but by directing production more and more to capital
goods and goods for export.” The banks sabotage the system
that feeds them. If we believe this, we must believe that capital-
ism is indeed a paradoxical system. Or could it be that Major
Douglas could not keep his arguments straight?

Underconsumption or Underproduction?

I feel compelled to ask: Is Douglas’ theory a theory of un-
dercomsumption or underproduction? In this place, he wrote
that capitalism restricts “the supply of ultimate commodities,”
by which he seems to have meant consumer goods: underproduc-
tion. Elsewhere, he argued repeatedly that Finance Capital
reduces the availability of funds for consumers to buy the exist-
ing consumer goods: underconsumption. Well, which is it? Capi-
talism is apparently the worst of all possible production systems.
It reduces consumption by not making consumer goods avail-
able, but also by not creating enough “tickets” for consumers to
buy goods that it does produce. Capitalists invest in capital
goods in order to increase production of future consumer
goods, yet the system somehow suppresses the sales of these

25. Karl Marx, Value, Price and Profit (1865), in Marx and Engels, Collected Works,
Vol. 20 (New York: International Publishers, 1985), p. 148.

“26. Credil-Power and Democracy, p. 45.
