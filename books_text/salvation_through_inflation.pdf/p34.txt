2 SALVATION THROUGH INFLATION

founder, it has retained the original name. This has produced
considerable confusion over the years, both inside and outside
the Social Credit Party. Conservative voters have turned to this
party in Canada over the last several decades, unaware that the
founder of the Social Credit movement was not a conservative,
nor were many of his early disciples, such as socialist G. D. H.
Cole, fascist Ezra Pound, and communist Hewlett Johnson.

This is evidence of the long-term problem: what is regarded
as a conservative critique of the economy is all too often inher-
ently socialistic in its policy prescriptions. Conservatives retain
their allegiance to the name and the rhetoric of the original
critique, despite the fact that a consistent application of the
proposed solution would produce results foreign to everything
conservatives claim they stand for. A study of the Social Credit
Party in Canada would, I believe, verify my thesis. It is a con-
servative movement that officially relies on what was originally
a left-wing critique of capitalism.

Hostility to the Free Market

It is my contention that there is today an underlying hostility
to certain aspects of free market capitalism, a hostility shared by
conservatives, radicals, and modern liberals. All three have
come to the same conclusion, namely, that without intervention
from the national government in the field of monetary policy,
capitalism will collapse.

In the early nineteenth century, capitalism was regarded as
evil by conservatives and radicals because it supposedly exploits
labor. In the twentieth century, this complaint shifted. Capital-
ism has been seen as insufficiently exploitative — not of men but
of resources. Without the State’s ability to create fiat money at
will, the critics assure us, the free market will be strangled by
underconsumption. In other words, the free market needs the
State to provide the central institution in any highly developed
division-of-labor economy: money.

After 1965, this “slow-growth” criticism of capitalism was
