154 SALVATION THROUGH INFLATION

My point is that a democracy of consumers can be attained by only
one way: where the “vote” is not political. The vote is economic
~ the private, personal decision to buy or sell, lend or borrow,
deposit or withdraw.

A False Definition of Money: Tickets

The number-one analytical error in Major Douglas’ criticism
of capitalism was his definition of money. Because he did not
understand what money is - the most marketable commodity —
his solution was the creation of fiat money by the State.

He viewed money as a system of tickets for goods and servic-
es. “The money system can accurately be described as a ticket
system. . . .”'* In Social Gredit, he described the operations of
a railway ticket system. A railway ticket, he said, is a “limited
form of money.”

A Legal Claim or the Most Marketable Commodity?

This is a complete misunderstanding of money. Money is a
not a claim on goods and services in the way that a ticket is a
claim on a particular good or service. Money is the most mar-
ketable commodity. In contrast, a ticket is a legal claim on a
specific quantity of goods or services: “one seat per ticket” or
“one car wash per ticket.” The person who holds that ticket is
legally entitled to whatever is promised on the ticket. A person
holding a ticket faces no counter-bid from someone holding
another ticket. He cannot lose his seat in the auditorium just
because someone else has five tickets while he has only one.
There is no system of competitive bidding, with the highest
number of tickets offered entitling that bidder to one seat. On
the contrary, a bidding process is what allocates the tickets: a
bidding process involving money.

18, Warning Democracy, p. 80.
19. Social Credit, p. 62.
