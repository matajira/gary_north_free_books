Xxxii SALVATION THROUGH INFLATION

this book. You can and should post-judge it. You can and
should draw conclusions. But before you do, you should finish
reading all of the book. Heed my warning: just because Social.
Credit sounds quite bizarre ~ anti-Establishment — this is not a
sufficient reason to assume that Social Credit is correct. Just
because its leaders challenge Establishment enemies who really
do need challenging is no proof that their challenge is on tar-
get. Just because a few of its defenders say that Social Credit’s
economic theory is Christian® is no reason to assume that it
really is Christian. Just because the world economy may look
shaky is not a sufficient reason to believe that Social Credit’s
proposed reforms will produce spectacular economic growth,
which is what Major Douglas promised.

I€ I am successful with this book, you will understand its
arguments as you read it, and you will understand its thesis
when you finish it. You may not agree with it, but you will
understand it. Let me warn you one more time: if you do not
understand an author when he deliberately writes a book aimed
at the average reader, you can be fairly confident that the prob-
lem is with the author, not you. Either the author is incapable
of expressing himself clearly or else his theory is wrong, and he
is using a lot of jargon or technobabble to cover up the fact that
his theory does not hold water. Always be suspicious of articles
or books addressed to laymen if these articles and books are
filled with jargon, and especially if they are attempted refuta-
tions of a clear, understandable criticism of a particular position
or idea. Whenever you read that sort of article or book, think
to yourself, “You may be right, mate, but don’t expect me to
climb on board your bandwagon until I understand exactly
what you're talking about.”

It is time to take a closer look at the bandwagon.

8. For example, Eric D. Butler, Social Credit and Christian Philosophy (Melbourne:
New Times Limited, 1956); Charles Pinwill, The North-South Dialogue on Christian
Principles of Money and Banking (Cranbrook, W.A., Australia: Veritas, 1990).
