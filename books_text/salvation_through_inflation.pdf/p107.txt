Social Credit’s Blueprint 75

Douglas said. “It is both pragmatically and ethically undeniable

that the ownership of these intangible factors vests in the mem-

bers of the living community, without distinction, as tenants-for-

life."™ Social Credit would put all but the rich on the dole.
Why is Social Credit regarded as conservative?

The Premises of Socialism

Social Credit is socialistic in its basic philosophy. It begins
with a false intellectual premise, namely, “the community”
(society) owns all the capital within the State’s borders. This
means that society is the same as the State: the familiar assumption
of all radicals and socialists. Social Credit proposes the estab-
lishment of a national system of compulsory State credit. When
Douglas says “community,” he means individuals who are mem-
bers of a national political order. They exercise economic con-
trol through State coercion, not the free market. “If the com-
munity can use the plant it is clearly entitled to it... "8

Douglas referred to the “real owners” in society: all members
of the political order. They exercise their ownership through
the threat of legal violence: the creation of the State’s monopo-
listic credit masters. Douglas insisted that “the power to draw on
the collective potential capacity to do work, is clearly subject to the
contro! of its real owners through the agency of credit.” This
is the essence of all socialism: collective ownership.

Douglas sometimes retained the language of free market
individualism. “It is a fact inherent in the nature of the case
that ownership must vest in an individual, and any attempt to
get away from this law of nature results as a practical conse-
quence in the appointment of an administrator whose power
increases as the number of his appointers increases.” This is

84. Tid, p. 190.

85, Economic Democracy, p. 114.
96. Ibid., p. 115.

87. Warning Democracy, p. 8.
