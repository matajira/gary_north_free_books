Social Credit’s Blueprint 67

into the company and the income pulled out of the company.
The investor forfeits the use of everything the money would
buy when he turns the money over to the company (initial
offering) or to another investor who is selling his share of own-
ership (secondary market), What the initial investor gives up
constitutes the capital of the firm. There is no increase in the
money supply; there is only a transfer of money from one
entity (the investor) to another (the firm). The same is true of
dividends: the money goes the other way. Paying corporate
dividends creates neither monetary inflation nor price inflation.

Not so in Douglas’ system. It is a reform based on the cre-
ation of new money and credit. Social Credit is not a direct tax
scheme, he insisted. (I argue that because it is inherently infla-
tionary, Social Credit can easily become an inflation tax scheme,
paid for by those who hold cash or instruments of wealth that
promise to pay money: bonds, cash-value life insurance, pen-
sion funds, mortgages, etc. The beneficiaries are those who
spend the newly created money fast, go into debt, and pay off
later with depreciated money.)

Under Social Credit’s national dividend scheme, no citizen is
required first to turn money over to the government, as a pri-
vate investor would turn money over to the seller of ownership
shares. On the contrary, the government sends out pieces of
paper called money. These payments are not a dividend on
money previously invested, i.e., money forfeited by the investor.
These payments are made with fiat money: an additional injec-
tion of purchasing media into the economy. No one has
stopped spending in order to enable another person to spend.
Everyone continues to spend. This dividend is monetary inflation,
and a similar scheme in France, 1789-94, produced vast price
inflation.

How fair is Douglas’ proposed system? Consider the fate of
anyone with high income. This person is, in Douglas’ words,
“the exception mentioned in the paragraph that follows.”
