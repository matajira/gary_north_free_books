140 SALVATION THROUGH INFLATION

5. The economy does not need injections of fiat money or
bank-created money.

6. There is no inherent need in capitalism of bank-created
credit money to offset the money received by sellers of raw
materials, land, and lenders.

7. There is no single central problem in capitalism for which
there is a single solution (“magic pill”).
