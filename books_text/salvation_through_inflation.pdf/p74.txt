42 SALVATION THROUGH INFLATION

ety? He answered: above all, allow the voluntary exchange of
goods and services without coercive interference from civil
magistrates. He concluded that it was the economics of mercan-
tilism ~ State control over the terms of trade — that hampered
economic growth. The solution is to reduce the authority of
politicians and bureaucrats to set the terms of trade: tariffs,
quotas, prices, and so forth.

The Critics of Capitalism

This conclusion has not satisfied many generations of critics
of free market capitalism. They begin as Smith did, by asking
the same question: What are the policies of civil government
that lead to greater wealth in society? They conclude something
entirely different: additional government controls over the
economy. They blame poverty on capitalism’s policy of placing
ownership in the hands of individuals and then leaving them
free to buy and sell from each other. Some critics of capitalism
call for a return to a version of mercantilism: controls over the
terms of trade. National socialism (Nazism) and Fascism are
examples of this type of thinking: the “corporate state” which
proclaims the “partnership between business and government.”
Other critics call for full socialism: the State’s ownership of the
means of production. Still others call for a reform in the mone-
tary system. This was the heart of Major Douglas’ recommend-
ed reform.

This is not to say that anyone and everyone who calls for a
reform of the monetary system is necessarily a hostile critic of
free market capitalism. Some are, some aren't. Marx surely was.
In the Communist Manifesto (1848), Marx and Engels recom-
mended “Centralisation of credit in the hands of the State, by
means of a national bank with State capital and an exclusive
monopoly.”' This was point five in their ten-point program to

1. Karl Marx and Frederick Engels, Collected Works (New York: International
Publishers, 1976), VI, p. 505.
