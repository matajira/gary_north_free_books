Who Represents the Consumers? 103

Major Douglas, like virtually all of capitalism’s critics, did not
understand the doctrine of consumers’ sovereignty. He honestly
believed that producers dictate terms of sale to consumers. He
wrote:

Let me repeat — the only true, sane origin of production is
the real need or desire of the individual consumer. If we are to
continue to have co-operative production, then that productive
system must be subject to one condition only - that it delivers
the goods where they are demanded. If any men, or body of
men, by reason of their fortuitous position in that system, at-
tempt to dictate the terms on which they will deliver the goods
(not, be it noted, the terms on which they will work), then that is
a tyranny, and the world has never tolerated a tyranny for very
long.*

There are indeed tyrants in the capitalist economy: the
consumers. They are merciless. They keep asking producers
this question: “What have you done for me lately?” And this
question: “What can I expect from you tomorrow?” But Major
Douglas saw the producers as the tyrants. He therefore failed
utterly to understand capitalism’s system of economic represen-
tation: the owners of capital must serve the consumers or suffer
the consequences, namely losses or forfeited profits. It is the
producer’s opportunity to make a profit that serves the con-
sumers as their hammer: share owners are pressured to elect
managers who serve consumers well — with the least waste of
scarce resources.

Because Major Douglas misidentified the bankers as the true
masters of the economy, he misidentified them as the hidden
thieves in the system. Central bankers - those who use the State
to gain a monopoly over the control of money and credit —
were indeed thieves, and remain thieves. But this is not because
they are bankers - agents of private depositors who lend mon-

46. Control and Distribution of Production, p. 13.
