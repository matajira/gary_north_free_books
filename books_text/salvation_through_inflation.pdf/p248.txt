216 SALVATION THROUGH INFLATION

Douglas vs. Negative Economic Sanctions

Major Douglas offered a theory of economic productivity
unique in modern history. He argued that the fear of poverty
is the cause of poverty. That is to say, he argued that people
who are not fearful are more efficient. I suppose we might ask:
All of the time? In every case? He thought so: all of the time, in
every case. (Notice his phrase, “tend automatically.”)

T have no hesitation whatever in saying that the most important
work, the hardest work, and the most work per man in the
world is done by men who have no fear whatever of poverty and
no human likelihood of ever being poor. Conversely, these sec-
tions of society which are constantly faced with the fear of pover-
ty tend automatically to become incapable of anything but the
lowest grade of work, and ultimately do even this work less
efficiently than better-paid and socially better-placed individuals.
Whatever function it may have fulfilled in the past, it is my
personal opinion that fear of any kind is the most destructive
and generally undesirable motive which can be imported into
any human action, and that no greater service can be made to
mankind that its elimination.”

He wanted to eliminate the fear of economic failure. But if
there is no fear of economic failure on the part of producers,
how can consumers impose their will on producers? Only politi-
cally. The credit masters will impose fear and trembling on
producers, not consumers. The credit masters will impose this
fear politically. All registered companies — the only ones with
access to the government's below-cost credit - will be guaran-
teed an average rate of profit for five years. “Undertakings
unable to show a profit after five years’ operation to be struck
off the register.”"® That is, these firms will be cast out into the
outer darkness of competition without government subsidies.

17. Warning Democracy, p. 29.
18. Social Credit, p. 209.
