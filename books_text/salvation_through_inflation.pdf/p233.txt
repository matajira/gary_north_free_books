Social Credit Means State Monopoly Credit 201

would not collect money from rents. It would not participate in
any profits. There would be only an estimation of the national
capital’s total money value. A true dividend could exist only.
when there were actual profits and rents collected, but since a
Social Credit government would not own the nation’s assets, but
would merely control the money (credit) supply, it would not
participate in any profits or rents. The State would merely print
up checks and mail them to all citizens (except the rich).

Fiat Money

The Social Credit system is one among many fiat money
schemes.” It adds extra features, such as the national invento-
ry, the National Dividend (money sent to citizens), and the
‘Just Price” (money issued to producers), but it is ultimately a
fiat money scheme. Every such scheme promises to keep the in-
crease in the money supply tied statistically to the increase in
production.” The well-acknowledged practical problem with
all such schemes is the same: how to keep the increase in money
from producing rising prices. There has to be a judicial or eco-
nomic limit in the system that keeps the central bank, govern-
ment, or other money managers from creating too much mon-
ey. Without a gold standard or other commodity standard,
where individuals can exchange money for a fixed supply of
the commodity, what institutional restraint exists?

There is another practical problem, but it is rarely acknowl-
edged, even by economists: how to keep an injection of fiat
money from causing distortions in relative prices. That is, how to
keep new money from causing price distortions at those points
in the economy where this new money arrives first, such as in
the accounts of the businesses that get government credit. It is

29. See Appendix C, below.

30. Gary North, “Gertrude Coogan and the Myth of Social Credit,” in Gary
North, dn Introduction to Christian Economies (Nutley, New Jersey: Craig Press, 1978),
ch: LL.
