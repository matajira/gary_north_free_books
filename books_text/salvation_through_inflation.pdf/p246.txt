214 SALVATION THROUGH INFLATION

and women carrying sandwich-boards and collecting-boxes
through the financial quarters in and around Wall Street, bear-
ing on them the legend, “The Salvation Army is Father Knicker-
bocker’s best friend.” It is perhaps hardly necessary to explain
that Father Knickerbocker is generally taken to represent the
respectability of solid, or perhaps preferably, liquid capital. That
is to say, it may be taken as a scientific statement of fact that one
of the most dangerous opponents of a better, cleaner world, is
the sentimental spirit which is entirely concerned with the beau-
ties of a prospective Heaven, whether that Heaven is theological
or moral, The head of the institution to which I have just re-
ferred, has recently elaborated the preceding statement by an
intemperate attack on the “dole,” basing his objection to it on the
“demoralisation” of the recipient and not, of course, on the
financial jugglery which accompanies it ~ an attitude entirely
similar to that of the Puritan in his abolition of bear-baiting; not
because it was cruel to the bear, but because it gave pleasure to
the populace. The practical outcome of this Puritanism is always
negative. In short, there is a type of sentiment which, under
existing conditions, is able to attain great respectability, but
which can, with very little difficulty, be identified with the for-
malism against which the Great Reformer of nineteen hundred
years ago launched his most bitter invective; and wherever that
is found, the prospect of effective assistance is not encour-
aging.*

Yet it was Jesus who warned: “And fear not them which kill
the body, but are not able to kill the soul: but rather fear him
which is able to destroy both soul and body in hell” (Matt.
10:28). This is formalism with a vengeance: God’s vengeance.

The Decline of Economic Sanctions

Douglas attacked modern taxation. He regarded the modern
State as a robber. This, despite the fact that the technical heart
of his proposal for reform was the permanent transfer of con-

14, Iid., pp. 200-1.
