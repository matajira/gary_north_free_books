218 SALVATION THROUGH INFLATION

once you and I will no longer have the legal right to deposit
our money in a bank - then will come the millennium!

If we are to emerge from this period into the millennium which
is easily possible, although by no means certain, the re-organisa-
tion necessary must be based on a philosophy which, whatever
other elements it may contain, will certainly not enthrone the
productive and industrial systems in the preponderantly impor-
tant position which they have occupied for the past 100 years.”

Who Brings Sanctions Against the Credit Masters?

In a free market, the saver has the legal right to determine
where his savings will go. Not so under Social Credit. The free
market gives the saver the right to withdraw his savings from
the bank, or sell his shares of ownership, or sell his real estate.
Not so under Social Credit.

Douglas understood that there must be a hierarchy in soci-
ety: rulers and ruled. He also understood that for rulers to
control the ruled, there must be appropriate sanctions. What he
failed to understand is that under capitalism, the consumers are
the rulers, while the producers are the ruled. He did not un-
derstand the dual sanctions of profit and loss. Most socialists
regard the producers as the masters. Douglas differed from
them only in regarding the bankers as the masters.

There must be hierarchies in every society. There must also
be sanctions. But, he insisted, economic sanctions — rewards and
punishments — are fading from the modern economy, except
for the final sanction: the smash-up. With respect to the coming
economic breakdown — his version of final judgment — Douglas
said, in effect, the quicker the better. “Whatever may be the
case in other matters, compromise in arithmetic seems singular-
ly out of place, and it is much better that the present defective
system should be allowed to discredit its upholders, and so

20, Ibid., pp. 84-85.
