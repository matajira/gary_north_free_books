Dividends Under Capitalism 171

alized companies expanded rapidly in the nineteenth century.
These people got jobs in factories. The machines that investors
provided to workers made life better for them; they increased
labor productivity and therefore labor income.’ Why would
the opportunity to pool capital in a limited liability corporation
reduce the number of wage-earners? Always before, an increase
in the amount of capital had increased the productivity of labor
and made possible an expanding labor market. How was this
connection destroyed by the coming of limited liability compa-
nies and local banks? Douglas never said.

The return on invested capital has fluctuated over the years
between six percent and twenty percent. For example, the yield
to industrial capital in Germany, 1850 to 1910, stayed in the
narrow range of 6.5% and 7%." The reason for these fluctua-
tions is often statistical; until the twentieth century, statistics on
invested capital have been unavailable for most countries. This
return is normally an interest payment. Rare is a company that
earns above ten percent on capital invested, and even more
rare is a firm that continues to do so year after year. Also,
companies rarely pay out in dividends all of the profits earned.
It is not uncommon for half of the profits to be retained for
capital investment. How could this tiny percentage ~ a percent-
age that did not increase under limited liability — have affected
the wage system? Employee compensation in the United States
has remained in the narrow range of 65% to 73% of national
income since 1930." This is not fundamentally different from
other industrial nations. Dividends play a very small role in the
overall demand for consumer goods, but they play a crucial
role in persuading investors to forfeit present consumption.

11. R. M. Hartwell, “The Standard of Living Controversy: A Summary” in
Hartwell (ed.}, The Industrial Revolution (New York: Barnes & Noble, 1970), ch. 8.
12. Solomos Sclomou, Phases of Economic Growth, 1850-1973 (Cambridge Univer-
sity Press, 1990), p. 109.
_ 13. Herbert Stein and Murray Foss, An Hlustrated Guide to the American Economy
(Washington, D.C.: AEI Press, 1992), p. 47.
