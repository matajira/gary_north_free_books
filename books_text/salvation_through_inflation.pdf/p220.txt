188 SALVATION THROUGH INFLATION

cians and unelected bureaucrats thwarts consumer sovereignty.
Consumer sovereignty rests on a single legal principle: the
lawful authority of the consumer over the allocation of his money and
other assets. Yet the technical heart of Social Credit’s proposed
reform is the forcible transfer of the authority over capital
allocation from consumers to the State. A. R. Orage set forth
the justification for this massive transfer of authority in his
section of Credit-Power and Democracy, and Douglas allowed this
to be published in the name of Social Credit: the State is the
lawful custodian of the nation's wealth, for it is the true repre-
sentative of the community. He wrote:

Economically regarded, a nation is an association of people
engaged in the production of Real Credit, and in this sense the
State, as the custodian of the Real Credit of the community, may
be said to represent the interests of Producer and Consumer
equally, since both are equally necessary to the creation of Real
Credit. Since, however, Producers and Consumers between them
make up the whole community, we may conclude that Real
Credit is social or communal in origin; that it belongs neither to
the producer nor to the consumer, but to their common element,
the community, of which they each form a part.

Let the reader be reminded: the idea that the community (soci-
ety) owns the wealth of the nation, and the State alone repre-
sents all members equally, is the heart, mind, and soul of every
socialistic theory of economic reform. (See Chapter 5, above.)

Bureaucracy

By law, the State is always a bureaucracy. Its employees are
salaried. They do not own the State; they work for the State.
They cannot legally share as owners in the economic successes
of the State. That is, it is difficult for a bureaucrat to benefit

5. A. R. Orage, “Commentary” Credit-Power and Democracy (2nd ed.; London:
Gecil Palmer, 1920), pp. 157-58.
