A Bibliography of Free Market Monetary Theory 287

Mises, Ludwig. Human Action: A Treatise on Economics (1966
edition). Washington, D.C.; Henry Regnery Company, chapters
XVI-XX, pages 327-586.

Mises, Ludwig. On the Manipulation of Money and Credit (1978).
Dobbs Ferry, New York: Free Market Books, 296 pages.

Mises, Ludwig. The Theory of Money and Credit (1953 edition).
Irvington, New York: Foundation for Economic Education, 493

pages.

Capitalism as a Moral Order

The Morality of Capitalism (1992). Irvington, New York: Founda-
tion for Economic Education, 150 pages.

Griffiths, Brian. Morality and the Market Place (1982). London:
Hodder & Stoughton, 160 pages.

Griffiths, Brian. The Creation of Wealth (1984). London: Hodder
& Stoughton, 160 pages.
