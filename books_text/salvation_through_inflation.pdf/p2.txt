Other Books by Gary North

Marx’s Religion of Revotution (1968, 1989)

An, Introduction to Christian Economics (1973)
Puritan Economic Experiments (1974, 1988)
Unconditional Surrender (1981, 1988)

Successful Investing in an Age of Envy (1981)

The Dominion Covenant: Genests (1982, 1987)
Government by Emergency (1983)

Backward, Christian Soldiers? (1984)

75 Bible Questions Your Instructors Pray You Won't Ask (1984)
Coined Freedom (1984)

Moses and Pharaoh (1985)

The Sinai Strategy (1986)

Conspiracy: A Biblical View (1986)

Honest Money (1986)

Fighting Chance (1986), with Arthur Robinson
Unholy Spirits (1986)

Deminion and Common Grace (1987)

Inherit the Earth (1987)

Liberating Planet Earth (1987)

Healer of the Nations (1987)

The Pirate Economy (1987)

Is the World Running Down? (1988)

When Justice Is Aborted (1989)

Political Polytheism (1989)

The Hoax of Higher Criticism (1990)

The Judeo-Christian Tradition (1990)

Tools of Dominion: The Case Laws of Exodus (1990)
Victim’s Rights (1990)

Westminster's Confession (1991)

Christian Reconstruction (1991), with Gary DeMar
The Coase Theorem (1992)

Politically Incorrect (1993)

Rapture Fever: Why Dispensationalism Is Paralyzed (1993)
