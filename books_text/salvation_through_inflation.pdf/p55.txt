Testing the Prophets 23

This, I contend, has been the approach of those Christians
who have come to other Christians in the name of Social Cred-
it.

Is Social Credit in Some Way Christian?

When Major Douglas proposed the economic reform known
as Social Credit, this question did not concern him. As he forth-
rightly announced in a 1937 speech, “I am not here as a pro-
tagonist of Christianity, I am looking at this from a very differ-
ent point of view. . . .”* But in the 1930's, he attracted an im-
portant follower in Canada, William Aberhart, a radio evange-
list and politician who ran a successful campaign to elect a
Social Credit government in the province of Alberta. From that
day until now, Christians have adopted Social Credit as an ideal
supposedly consistent with Christianity. Major Douglas did not
embrace Mr. Aberhart’s theology; Mr. Aberhart embraced the
rhetoric (though not the policies) of Major Douglas’ economic
system. Major Douglas recognized the hierarchical nature of his
proposed reform: Social Credit must be on top. Any of his
disciples who wanted to promote some other reform had to be
willing to place it at the foot of Social Credit’s altar.

This book is addressed primarily to Christians who have
read something about Social Credit, or who may even have
publicly embraced some of its ideas. 1 have not written to con-
vert the leaders of the Social Credit movement to a systemati-
cally and self-consciously biblical economic point of view. People
who have invested many years of their lives and their reputa-
tions in a defense of any doctrine are unlikely to abandon this
doctrine late in life. To do so would be an open admission that
they have wasted the best years of their lives in piling up what
the Bible calls wood, hay, and stubble. People are generally

Pluralism (Tyler, Texas: Institute for Christian Economics, 1989).
8 G.H. Dougtas, The Policy of a Philosophy (Liverpool: K.R.P Publications, 1937),
p. 5.
