i, Transcendence/Presence

6
“NO TRESPASSING!”

You shall not remove your neighbor’s landmark, which the
men of old have set, in your inheritance which you will inherit in
the land that the Lord your God is giving you to possess (Deuter-
onomy 19:14).

In the second half of this section of the book, I intend to pro-
vide examples of how men are legitimate rulers under God, and
why the same five-point covenantal pattern reappears in the
affairs of men.

The first principle of a Biblical covenant is transcendence.
God is the Creator. How does this apply to man in his relation to
the creation? Man is made in God’s image. Therefore, man is a
ruler over creation, too.

In the Old Testament, the guardians of God’s holy sanctuary
were the priests. This is why the Old Testament occasionally
Tefers to the religious leaders as gods. “God stands in the congre-
gation of the mighty; He judges among the gods. How long will
you judge unjustly, And show partiality to the wicked?” (Psalm
82:1-2). Men are rulers, or judges, over the creation. “I said, You
are gods, And all of you are children of the Most High. But you
shall die like men, And fall like one of the princes” (Psalm 82:6-7).
God's judgment was to fall on the religious leaders just as it was
about to fall on princes. They all judged unrighteously.

Thus, men are to exercise their rulership over the creation,
which is similar to the absolute rulership which God exercises
over His creation. This is what the first principle of the covenant,
the Creator-creature distinction between a transcendent God and

74
