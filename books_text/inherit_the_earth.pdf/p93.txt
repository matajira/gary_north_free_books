"No Trespassing!” 85

3. He delegates to men a limited legal power to exclude others
in every area of life.

4. Redeemed men are to take dominion from Satan’s followers
in every area of life.

5. Redeemed men are therefore to exclude rebellious men
from ownership in every area of life.

6. The means of lawful economic exclusion is productivity
within a competitive market, not political force.

7. This power of exclusion operates in every area of life: fam-
ily, church, State, business, education, etc.

8. Exclusion is basic to dominion: it is the training ground for
personal responsibility.

9. Ownership (the right to exclude) of property is not to be
violated by the State, just as the right to exclude others in marriage
is not to be violated.

10. The State is not to become the single owner; therefore, the
State cannot legitimately abolish private property.

11. Socialism is theft: the illegitimate exclusion by the State of
lawful owners.

12. Socialism is therefore anti-dominion and pro-power.

13. Socialism is historically and theoretically anti-family.
