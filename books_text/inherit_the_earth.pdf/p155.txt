12
CHURCH RESPONSIBILITIES

I know your works [Church of Laodicea], that you are neither
cold nor hot. I could wish you were cold or hot. So then, because
you are lukewarm, and neither cold nor hot, I will spew you out of
My mouth. Because you say, ‘I am rich, have become wealthy, and
have need of nothing’ and do not know that you are wretched,
miserable, poor, blind, and naked . . . (Revelation 3:15-17).

Churches today, like every other institution, are rich beyond
the wildest dreams of men a century ago. The vast outpouring of
productivity which the free market has produced since the late
1700’s has transformed all of us. By historical standards, we are
fantastically wealthy.

Yet the church is miserable, poor, and blind today, just as the
Ghurch of Laodicea was in John’s day. It is neither hot nor cold. It
has lost its impact in society. In the liberal camp, the churches
can’t compete against revolutionary groups, or liberal politics, or
just evening television. In evangelical circles, the churches can’t
compete against the large television ministries, called the “elec-
tronic church,” or just evening television.

Conservative fundamentalist churches are growing, and a mi-
nority of these churches have tentatively begun to experiment
with social action projects (mostly the abortion issue), Christian
schools, and politics. So far, they have not begun to have much
political impact, and certainly not much economic impact, in the
community at large, especially outside the South and rural
Midwest.

The church is not to become the primary agency of welfare.

147
