The Legacy of Knowledge 131

they have. The free market allows each of us to buy and sell our
talents, including our intellectual talents, to anyone who wants to
deal with us. The market also gives us a method of sorting out
consumer-satisfying information from overpriced information.
This is the system we call profit and loss,

If we fail to understand why the free market is our greatest
source of new information, under God, and if we fail to teach our
children to respect the Bible’s laws of ownership, then we will dis-
inherit our children. They will curse us for wasting their inheri-
tance from God.

We need to understand these principles concerning the gather-
ing and inheriting of economically valuable knowledge:

1. Each individual, on his own, probably does not have
enough personal knowledge to keep himself alive.
2. We all depend on the division of intellectual labor to sustain
us.
3. Each person has something to sell, and each person has
needs to fill.
4, We communicate to each other impersonally, through the
free market.
5. Prices are the means of registering the information needed
by consumers and producers.
6. The plans of competing individuals are integrated and ad-
justed by market participants by means of profit and loss.
7, The free market allows consumers to be represented on
profit-and-loss sheets,
8. Profit and loss serve as incentives for providing more and
better solutions.
9. Profit and loss serve as devices for filtering out “wheat” from
“chaff.” The “wheat” is consumer-satisfying information.
10. People have competing goals and competing plans for the
future,
11, The system of incentives in an economy should encourage
plan revisions that satisfy consumers.
12. The system of incentives should represent consumers as
closely as possible.
13. Socialist economies represent the interests of bureaucrats,
