Profit and Loss 113

Christ, and also diversified, meaning that it’s made up of numer-
ous individuals who are very different from one another and who
possess very different talents.

‘We now return again to an earlier theological theme, namely,
the concept of unity and diversity. The Trinity is both unified and
yet one God. Therefore, the church reflects this same diversity
and unity. It can maintain itself as a unity because it has one God
who has given one revelation to man, and members of the church
are conveniently linked to this God, and are responsible to Him.

(It’s interesting that the New Age movement's promoters also
talk a lot about unity and diversity. But they have no all-powerful
God in heaven who directs human history, nor do they have a reli-
able, open, publicly revealed Word of God to consult. They are
imitating God’s program, yet twisting it. This is what Satan has
done from the beginning.)

The covenant involves hierarchy or authority (principle two),
and it also involves moral standards (principle three). A cove-
nantal structure is unified because it has one personal living head,
and it’s diversified because it has many individuals who are re-
sponsible to that single head. It has a program of action, because
it has the revealed Word of God: the Bible, It has a final judgment
in terms of its performance as a collective, for God is the Judge of
collectives. We see this in the third chapter of Revelation, where
John writes to a number of churches that have not performed ac-
cording to God’s standards, and he warns them that God will deal
with them as individual congregational units, not simply as indi-
vidual members.

The Division of Labor in the Economy

Probably the most famous textbook example of the division of
labor in the economy is found in the first chapter of Adam Smith’s
Wealth of Nations (1776). He describes a pin-making factory. Each
man has specialized machines to work with, and each machine
does just one simple operation. Together, ten men back then could
make 48,000 pins a day; if one man had been required to do the
whole job, he probably couldn’t have produced one pin a day. The
