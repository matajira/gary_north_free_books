102 Inherit the Earth

his goods or services at a price which he thinks is beneficial to both
parties.

This doesn’t mean that the State should allow immoral behav-
ior to go on without any punishment. What this does mean is that
if a particular occupation is lawful before God, the State shouldn't
legally interfere with the agreements made by men in their volun-
tary dealings with one another. It means simply that I can come to
you and say, “I'll make you a better deal than any of my competi-
tors will make.” I should have the legal right to bid. My competitors
should have that same right. This is the meaning of competition.

Buyers and Sellers

Years ago, I was in the back yard of a friend of mine who is a
very successful businessman, Robert Tod. He is a graduate of the
Harvard Business School, and he was (and remains) a senior
partner in an extremely successful multi-million dollar business
that buys up other businesses. He was talking with his four-year-
old son, and he was trying to explain to his son why fathers have
to go work, “Why do I have to go to work every day, Robbie?”
Robbie, at age four, had a very good answer: “In order to buy
money.” His father replied, “No, Robbie, not to buy money —to
earn money.”

At that point I intervened. “No, Robbie is right. You go to
work to buy money.” The four-year-old didn’t have a Harvard
degree, but he had a better explanation of why his father went to
work than his father did. His father thought about it for a mo-
ment, and then admitted that Robbie was correct.

We say that we earn money. That’s a figure of speech. We buy
money. When we sell our labor services, we buy money.

On the other hand, the buyer of labor services is selling
money. Clearly, there is a buyer and a seller in every transaction.
In any voluntary exchange, each of us is at the same time a buyer
and a seller. Because of conventional speech, we don’t think of it
this way, but that really is the nature of the transaction.

We always forget that every buyer is a seller, and every seller is a buyer.
We always say that the seller (of goods) is the person who controls
