78 Inherit the Earth

the administration of this property. If they misuse their property
(for example, if they use it as a weapon) and violate other people's
use of their property, then they are held legally accountable by the
civil government and by church government if they are church
members,

Not only are they legally accountable, but they are also eco-
nomically accountable. As I have argued earlier, they are held
economically accountable by consumers. If they refuse (or are un-
able) to use their property in an efficient (low-waste) manner to
meet market demand, they suffer losses. Ownership is a social
function,

The first time that civil authorities allow thieves to have their
way in the community because the State refuses te punish them,
or refuses to require convicted criminals to repay their victim (Ex-
odus 22), the State has begun to weaken the defense of property. If
citizens encourage their political representatives to vote away the
property of others, they themselves have become partners in the
crime. Socialism and other kinds of political wealth redistribution
are forms of theft. Why? Because the State is violating the right of
present property owners to legally exclude other people from the
use of their property or the fruits of their labor and property. The
State begins to exclude rightful owners from their own property.
Exclusion is inescapable. The question is: Who will exclude
whom, and on what basis? Will power rule, or will God’s law?
Will God’s law determine who should be excluded, or man’s law?

Redeemed men are to increase their authority and dominion.
They are to progressively exclude Satan’s followers from positions
of authority, in every area of life. How is this to be done? Not by
the exercise of power, but by the exercise of following God's law.
Redeemed men are to compete, They are to get rich through pro-
ductivity. They are to give money away, in a grand exercise of
charity, They are to run for political office, especially at the local
level, where the Bible says that primary civil responsibility is to be
located. They are to bear more and more responsibility in every
area of life. Power flows to those who bear responsibility.

In short, the exclusion of the unrighteous from positions of public power
