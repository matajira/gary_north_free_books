34 Inherit the Earth

Authority: Top-Down or Bottom-Up?

The basis of dominion is cooperation under God. Every insti-
tution needs to have a specified chain of command or chain of re-
sponsibility, so that men understand and accept: (1) their God-
imposed limits, (2) their God-given assignments, (3) their per-
sonal responsibility for the resources that God provides them
with, meaning (4) the God-established standards for judging per-
formance which the organization enforces over its members.

Without these guidelines (“blueprints”), men cannot make
wise economic decisions. They cannot decide what to do and
when to do it. A society without hierarchies is impossible—a
myth. Any society that did not have many groups that have inter-
nal ranks of authority would rest on a foundation of chaos. Soci-
eties cannot survive without hierarchies. Hierarchy is an inescapable
concept. It's never a question of “hierarchy vs. no hierarchy”; it’s
always a question of which hierarchy or hierarchies,

Nevertheless, Biblical Christianity never asserts that any sin-
gle authority is absolutely supreme, except the Creator God. This
means that there must be God-ordained and God-limited institu-
tions to settle disputes. It also means that there should be no central
planning agency over the entire economy,

The Biblical concept of social order is not a top-down pyramid
of political power, but rather a bottom-up system of appeals
courts. Initiative should normally begin at the lower level of the
social system. (An exception, obviously, is during wartime. Yet
even here, most of the details for carrying out any mission must
be dealt with at the platoon level.)

Vladimir Lenin, the Communist revolutionary who captured
Russia in 1917, was a master of tyrannical, pyramid-like organiza-
tion. He once contrasted his “revolutionary political democracy”
(the Bolshevik Party) with the less centralized socialists, the Social
Democrats: “The latter want to proceed from the bottom upwards.
. .. The former proceed from the top... . My idea... is ‘bu-
reaucratic’ in the sense that the Party is built from the top down-
wards. .. .” He transformed Russia into the Soviet Union in
terms of this blueprint. It is demonic to the core,

 
