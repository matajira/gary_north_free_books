156 Inherit the Earth

thinking to themselves right now: “Well, I agreed with North up
until now. But he’s gone too far! He’s challenging the perfectly
proper right of the State to regulate the sale of [my product or ser-
vice]. This whole book is therefore nonsense.” Note: if you're
thinking this to yourself, the State has already captured your life,
your mind, and your future. You are in moral and intellectual
bondage, for you presently exist in terms of the State’s temporary
and conditional grant of an economic monopoly to your occupa-
tional special-interest group. You will be asked to pay for this spe-
cial financial aid, one way or another. There are no free lunches in
life. The State isn’t here to help you free of charge.)

Each group tries to impoverish its competition, Inescapably,
all consumers are harmed at the same time. What the seeker of
benefits wants is to keep consumers from working out a deal with
his competitors at a price that he is unwilling or unable to match.
He regards such deals as unfair competition. You can imagine
what buggy whip manufacturers would have done to Henry Ford,
if the State had been bigger back then.

I can remember a political campaign in the 1960’s in Califor-
nia to prohibit pay-per-show television. The existing “free” televi-
sion stations succeeded in getting a proposition on the ballot to
outlaw pay T.V. (Maybe it was the other way around; I forget.
Maybe the pay T.V. promoters were somehow required to get a
bill passed in order to be allowed to offer their services for sale.)
The “free” T.V. stations waged a propaganda war in the news-
papers against this “unfair” new source of entertainment. I re-
member especially a full-page newspaper advertisement which
showed a child in front of a T.V. He was asking his father why his
father wouldn’t pay to allow him to watch a show.

This was a blatant appeal to the fear of financial loss on the
part of parents, meaning lower-middle-class and poor parents.
Freedom of choice was ignored. Benefits to adults were ignored.
Today, as we have learned, parents are the people who watch
subscription T.V., since most programming is aimed at adults. I
think the “free” T.V. industry knew that back in the 1960's, too.
But the voters approved the initiative; pay T.V. was outlawed in
