104 Inherit the Earth

price is too high. I may walk across the street and buy a similar
but cheaper good, or the same good at a lower price. So the seller
of goods is in competition with other sellers of goods.

Buyers: At an auction, I’m a buyer of goods with my money.
P’m in direct competition with other potential buyers of goods with
their money. The auctioneer decides who gets the goods by seeing
who will bid the highest price. Simple: high bid wins! We see
clearly in the case of an auction that the auctioneer is not in com-
petition with the buyer. He’s only trying to get greater competi-
tion among the buyers. He uses all the skills he has in order to in-
crease the competition among the buyers. Buyers compete against
buyers.

What if the auctioneer starts the bidding at a price above what
any of the bidders are willing to bid? He isn’t going to make a sale.
He will be forced to lower the starting price, or put the item aside
and hope that at some other auction, he may be able to sell it ata
higher price.

This concept of competition is different from what you nor-
mally read about in a newspaper. The modern critic of capitalist
society doesn’t really understand how the market works. He
doesn’t understand what the basic principle of the market place is:
namely, the responsible work of each man before God in order to
meet his stewardship responsibility. He doesn’t understand the con-
cept of responsible ownership. He doesn’t understand competition.

“Make Me a Better Offer!”

What is competition? Competition is my offering to make a
better deal for the consumer than the consumer is able to get from
some other seller of goods and services. Or if I’m the consumer,
it’s my legal right to offer a particular seller more money, or better
terms or something that the seller wants compared to what any
other buyer is willing to offer him. That's all competition is. Its
the legal right to say, “Buy it from me,” or the legal right to say,
“Sell it to me.” It’s the legal right of an individual to offer a better
deal to some other individual.

Ina sense, what I’m asking the other person to do is to substi-
