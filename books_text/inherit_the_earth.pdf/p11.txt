INTRODUCTION

“The world economy is headed for a crash!”

“The economy has never been better!”

Well, which is it? We hear economists arguing both ways.
How can we make up our minds? How should we prepare for the
future? Is it a dark economic future or a bright one? The debate
continues. Here are some familiar examples:

Western banks have loaned hundreds of billions of dollars to
Third World nations that now are close to bankruptcy. Not only
will they never repay their loans, but they are also borrowing heay-
ily just to make their interest payments on time. If they default, the
banks go bankrupt (“bankrupt” « bank + rupture). But they have
to default; they don’t have the money to repay.

But . . . Western technology is creating a totally new world,
where computers will be cheap, and will perform tasks that are
now barely dreamed of. We will work inside our homes, set our
own schedules, and be able to buy computer-customized products
at “off the rack” mass-production prices,

We are drowning in a sea of pollution. The great lakes are
dying. Acid rain is rotting away our farms and businesses. Air pol-
lution has become a way of life. Poisonous chemicals are every-
where; criminal syndicates are dumping them down our sewers
and in our neighborhoods. Plastics never decompose. Nuclear
energy leaves deadly wastes behind, and no one knows how to seal
them up until they're harmless 100,000 years from now. We spend
billions on health care, but we are dying of diseases that slowly
destroy the body.
