Copyright® 1987 by Gary North

All rights reserved. Written permission must be secured from
the publisher to use or reproduce any part of this book,
except for brief quotations in critical reviews or articles.

Published by Dominion Press
7112 Burns Street, Fort Worth, Texas 76118

Prinied in the United States of America

Unless otherwise noted, all Scripture quotations are from the
New King James Version of the Bible, copyrighted 1984 by
Thomas Nelson, Inc., Nashville, Tennessee

Library of Congress Card Number 86-073075
ISBN 0-930462-56-4
