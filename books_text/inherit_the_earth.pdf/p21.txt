God Owns the World 13

absolutely owned by any single individual or any single human
institution, This conclusion is implied by the very statement
which begins this chapter: that Ged alone is the absolute owner of
all the creation. He, and He alone, possesses absolute rights of
ownership. All other ownership claims are subordinate.

When we speak of human ownership, we have to speak of
God-given ownership. God is the absolute and ultimate ruler over
all the creation, and therefore He is the absolute owner of the cre-
ation. We are told, however, that God has delegated to man the
responsibility of caring for the creation (Genesis 1:28). Man is
therefore a steward under the overall supervision of God. This
means that man is responsible to God for all that he is, and he is
responsible to God for the proper administration of everything
that has been entrusted to him.

Private Ownership

There is no question concerning the Bible’s affirmation of pri-
vate property. This includes the New Testament. Jesus offered the
following parable as a description of the kingdom of God. A land
owner sends his servant out one morning to hire workers. Several
are hired in the morning. The servant returns to the marketplace
several times during the day. Each time, men agree to work in the
fields, At the end of the day, the owner pays each of them the same
wage. Those who worked all day complain. Why shouldn’t they
have received more money than those who came late?

What was Jesus trying to teach? That God saves some men
early in their lives, some men in mid-life, and some men just be-
fore they die. Why should the early beneficiaries complain? They
had sought employment, and they had received it. Had they not
understood the terms? The land owner chides them: “Is it not law-
ful for me to do what I wish with my own things? Or is your eye
evil because I am good?” (Matthew 20:15). Jesus compares the
sovereignty of God in granting men salvation with the sovereignty
of the owner over his goods.

The early church in Jerusalem practiced voluniary common
ownership of goods. They had been warned by Jesus that Jeru-
