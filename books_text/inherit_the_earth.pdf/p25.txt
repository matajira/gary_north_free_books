God Owns the World 7

because he knows them, but because he believes they are willing
to bid the highest for the property in the competitive auction we
call the free market.

The number-one economic question is this: Who will offer the
highest bid? Will it be the consumers of food, or will it be the consum-
ers of living space? The owner of the land is rewarded to act as a
steward (property manager) for those people in the community
who are willing to pay the most for the land. [fhe refuses te follow
this rule, he cannot gain the maximum economic return from that
property. The important point is that the future profit he expects
to make from the sale of the land is his economic signal from future
consumers that they want him to sell it to the highest bidding buyer.

Assume that the farmer (the “agent” of future food buyers) is
unwilling to bid as much for the property as the “agent” of the
future apartment house dwellers. The man who presently owns
the property nevertheless allows the farmer to lease or buy the
property at the lower price. He makes money from the sale of the
land to the farmer, but to do this, he must give up the money that
the apartment house builder would have paid for the land. He
suffers a financial loss: he loses the money he could have made by
selling to the apartment house builder, minus the money he actu-
ally made by selling to the farmer.

There is nothing very amazing about this analysis of how the
free market works. It is a giant auction. The average person
understands this process, even if he has never thought much
about it. All this analysis says is: “You can’t get something for
nothing.” We live in a world of scarcity. Scarcity means that if
every item were sold at zero price, there would be more demand
than supply. So we put prices on such items in order to limit de-
mand. We decide who gets what by a system of bidding, just like
an auction. To get one thing, a person must give up something
else, And most of the time, we make our decisions based on the
rule: high bid wins. We do the best we can with what we've got. But
what is “the best”? We search out “the best” by asking ourselves:
“What is the best (highest) price I can get for this item or service?”
