Theft “1

The Religion of Socialism

The socialist assumes that (1) all property should belong to so-
ciety, meaning the State; (2) there should be no such thing as pri-
vate property; and (3) men will work for “society in general” with
the same kind of intensity and dedication that they will work for
their own families or for themselves. These assumptions are dis-
proved every time they're attempted. That, of course, does not
change the mind of the socialist.

The reason why it doesn’t change his mind is that she socialist is
@ deeply religious person. He has a very specific view of God, man,
and law. He has a very definite opinion of the nature of man. And
what he says is that society, through the inauguration of socialistic
means of production and socialistic ownership, can transform the
very nature of man.

This is basic to the Marxist system, and it’s implied in all other
socialist systems. It assumes one of two things: (1) man is very
different from what the Bible says he is and what we know our-
selves to be, or (2) man can be remade by the State to fit the
socialist model, the socialist view of the truly dedicated and com-
pletely altruistic (selfless) man.

There is another thing to consider. If an individual believes
that his property is easily confiscated by the State, his goal will
then be to take power over his neighbors by means of politics or
bureaucracy. Seeking control over a neighbor’s property by means
of politics will eventually become much more important to many
men than actually producing anything. Thus, not only will eco-
nomic productivity drop, but people's resources, time, energy, and
careful attention will be used to master a means of political theft
rather than to actually produce goods and services for consumers.

Does this sound like the twentieth century? It should.

Theft versus Dominion
As theft increases, which is in defiance of the law of God, the
ability of society to exercise greater and greater dominion is there-
by reduced. People get scared. They start hiding what they own.
They start spending money on burglar alarms and locks. Busi-
nessmen stop producing as many consumer goods and start pro-
