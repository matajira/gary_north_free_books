Church Responsibilities 149

A Program of Reconstruction

The first step that churches need to take is to require that each
voting member, or head of household, pay 10% of his after-tax in-
come to the church. “No representation without taxation.” This
will force men to take seriously the responsibilities of full member-
ship. It will also force them to come to grips with the importance
of the church as an agency of welfare.

Second, churches probably should take at least 10% of their in-
come from people’s tithes and offerings and set this money aside
for welfare activities. They should care for the poor, or work with
local churches that have ministries to the poor. This is how
churches can pay tithes,

In the Old Testament, there was a special poor tithe at the end
of every three years. The poor, the foreign residents, and the
Levitical priests were to be invited to a national feast of celebra-
tion (Deuteronomy 14:22-29). The money was used to finance
every family in the land in a special celebration before God. Some
commentators believe that every third year, the entire tithe was
set aside for the poor. Others think it was one-third of the tithe
every year. The closest celebrations to this that we have in the
United States are the annual turkey dinners at Thanksgiving and
Christmas, when poor people (“bums”) are given a free meal at
the local rescue mission. The steadily employed members of the
community never show up. It is not a true communal celebration.

Question: Why don’t churches do this? Why do parachurch or
non-church ministries do it?

Third, churches should see to it that the wife of every head of
household has sufficient low-cost “term” life insurance written on
her husband’s life to protect her and the children. She should own
the policy, paying for it from her own personal, exclusive check-
book. This establishes her as the owner of the policy.

Why should she own it? First, it does not become part of his
estate, so there is no inheritance tax involved. Second, what if he
quits the church, divorces her, and remarries? If he owns the
policy, he will probably name the new wife as the beneficiary,
