48 Inherit the Earth

have restored to them whatever was stolen from them, plus a pen-
alty payment (Exodus 22). This way, the State does not grow
larger at the expense of the criminal, nor does the State grow
larger at the expense of the victim.

When the State becomes a thief by enforcing a heavy and
graduated tax system, or by other coercive laws that interfere with
men’s voluntary exchanges, then everyone under its jurisdiction
becomes a potential victim. When the State becomes a represen-
tative thief of a majority of men, nothing is safe again. When the
State becomes the agent of corruption, the major institution for
the repression of evil doing is thereby corrupted.

If men have larceny in their hearts, and they use the ballot box
to make theft legal, then the entire society becomes larcenous, and
it will eventually lose its ability to grow and progress. God will not
be mocked. Judgment will come upon that society.

The following Biblical economic principles are essential if we
are to exercise effective, God-honoring dominion:

1, God is the absolute owner of property.

2. Adam's rebellion was displayed as an act of theft. It began
with the desire for something that was not his,

3. Tyranny always involves theft (Pharaoh).

4, The best cooperation is voluntary cooperation.

5. Self-interested people cooperate voluntarily.

6. Beggars don’t exercise dominion.

7. Appeals to charity are not to become the primary basis of
gaining other people’s cooperation.

8. Theft by ballot box is not to become the basis of gaining
other people’s cooperation,

9, Socialism and Communism are religions of humanism, for
they are based on the belief in political man (rather than God) as
the supreme ruler,

10, Men’s view of time affects their view of life.

11. Present-oriented people suffer from poverty, both of the
spirit and the pocketbook.

12, Present-oriented people are lower-class people.

13. Future-oriented people are upper-class people.

14. Christianity is a future-oriented religion,
