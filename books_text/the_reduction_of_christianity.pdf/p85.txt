Crying Wolf? 45

Agers were and are “in sync” with the present-oriented, humanistic
“Me Generation,” despite all their rhetoric about cosmic evolution.

The New Age Movement should not be taken lightly, but
neither should we cringe in its presence. This book is.designed to
put present events, both good and evil, into biblical and historical
perspective, We believe that the New Age Movement is human-
ism becoming more and more consistent with its foredoomed
attempts to rebel against God. As with all those who oppose the
Lord and His law, “they will not make further progress” (2 Tim.
3:9).

Why Such Visible Progress?

Weeds advance when little effort is expended to remove them
from a carefully prepared, once-vibrant garden. Ant-Christian sys-
tems progress because the church does very little to challenge them. More
often than not, we find the church retreating from battle instead of
leading the charge “against the schemes of the devil” (Eph. 6:11).
As we will show, this program of.cultural retreat has not been the
position of the church down through the centuries. The advance
of civilization came with the advance of Christianity.

God has always called Christians to set the agenda, to be a
light in a world where there is darkness. Those outside of Christ
are to see our “good works” so they can glorify God who is in
heaven (Matt. 5:16), The redeemed in Christ are to act as sign-
posts to point the lost to Christ. In Jesus’ day, miracles were used.
Today, God. calls on His new creations to perform the task
through the fruit of gospel works. It is our contention that this vi-
sion has been lost in'a day when the church is preoccupied with
signs it believes point to the end of the world. Today, there is a
new agenda. The church has taken a defensive posture, fighting
battles when the war is just about over. If God has given us time,
then we should get busy with the work at hand. Idleness is apt to
give the devil an “opportunity” (Eph. 4:27).

In this chapter we will explore the impact of the notion that we
are the last generation before Jesus returns, Is the so-called
prophetic clock of Daniel ticking once again? Are our present
