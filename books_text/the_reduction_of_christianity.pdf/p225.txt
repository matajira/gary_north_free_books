Dave Hunt's Heavenly Kingdom 185

Lalonde says that North believes that “preaching and tract-
passing” is-a “worn-out” belief, This is not at all what North
writes. Hunt, Wilkerson, Lalonde, and others do not believe that

the gospel is comprehensive, embracing the whole counsel of.
God. This is the view that is “worn out.”

Heavenly Citizenship

Hunt believes that only heaven is the kingdom. The Christian
is a citizen of heaven, not of an earthly kingdom.* This is not en-
tirely true. There is no indication in Scripture that we can’t be cit-
izens of both heaven and an earthly nation. The apostle Paul saw
no contradiction in claiming his Roman citizenship (Acts
16:37-39; 22:22-29) and maintaining that he was also a citizen of
heaven (Phil. 3:20), The apostle did not cry out: “Persecute me all
you want. P'm a citizen of heaven!” Instead, he called on the privi-
leges granted to him as a Roman citizen. In fact, he appealed, not
to heaven, but to “Caesar” (Acts 25:11). Of course, he was. using
the appeal to Caesar as a means to advance the gospel. The point
is that Paul did not believe that his heavenly citizenship cancelled
his rights as a citizen of Rome. Paul was prepared to use. his
earthly citizenship to advance the gospel. of the heavenly kingdom.

Moreover, the church is spoken of as 4 citizenship: “So then
you are no longer strangers and aliens, but you are fellow-citizens
with the saints, and are of God’s household” (Eph. 2:19). We
might say that membership in the church and heavenly citizen-
ship are two aspects of the same thing. The Christian’s heavenly
citizenship places him in an ecclesiastical body where a law order
should operate (Matt. 16:13-19; 18:15-20; 1 Cor. 6:1-11). To be
joined with Christ’s body is to be a citizen. of heaven. The point
here, though, is that heavenly citizenship.doesn’t cancel out our
earthly responsibilities in the church.

The Christian’s heavenly citizenship makes him an alien,
stranger, and exile on earth (Heb. 11:13; 1 Peter 2:11). But the

34. Beyond Seduction, p. 252.
