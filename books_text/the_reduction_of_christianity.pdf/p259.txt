The Kingdom is Now! 219

God alone can make us willing and righteous subjects of His king-
dom: As Jesus told Nicodemus, we cannot enter the kingdom un-
less we have been born from above ( John 3:5). But there are other
requirements for those who would persevere in the kingdom. Both
John and Jesus required repentance of those who would enter the
kingdom. The kingdom should be our highest priority and our
greatest joy (Matt. 6:33). The King requires total surrender
(Luke 9:60-62; 18:29). Jesus said that our status in His kingdom
depends on our attention to the details of His law (Matt. 5:19). In
fact, in some passages, Jesus goes further and says that righteous-
ness is a condition of entrance into the kingdom (Matt. 5:20;
7:21).8 A major part of the righteousness that the King requires is
humility (Matt. 5:3, 10; Luke 6:20). We must humble ourselves as
little children to be fit for the kingdom (Matt. 18:1-4; 19:14; Mark
10:13-16). This humility is shown in our willingness to forego our
own. rights. and to serve others.

The whole world benefits in many ways. from the rule of
Christ. But the rule of Christ also means condemnation for those
who despise His offer of blessing and salvation. Ridderbos notes
that the kingdom “means judgment because God maintains his
royal will in opposition to all who resist his will.” Thus, Christ’s
universal rule over all things is manifested either in blessing or
cursing. The Psalmist warned that the enthroned King would rule
the nations with a rod of iron and shatter the disobedient like pot-
tery (Psalm 2:9). The punishment of the wicked is more severe
than under the Mosaic system, “for if the word spoken through
angels proved unalterable, and every transgression and disobedi-
ence received a just recompense, how shall we escape if we neglect
so great a salvation?” (Heb. 2:2-3). Throughout the book of Acts,
the apostles warned people to repent because Christ had been
raised and exalted to judge all nations (Acts 2:32-36; 10:40-42;
17:31). Thus, the age of the kingdom is an age of crisis. When the

18. We do not mean that there is any merit in our righteousness.. Rather,
rightesusness, or obedience to God's Word, is a necessary mark of the Christian
who has been redeemed apart from the law.

19, Ridderbos, The Coming of the Kingdom, p. 20.
