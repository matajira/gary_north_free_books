312 The Reduction of Christianity

have a spiritual dimension? The question is: What spirit is trans-
forming civilization?

Civilization, therefore, is the reflection of a chosen spirit,

- whether Christ or Satan. The Christian, therefore, is to be in the
world, but not of the world (John 17:14-16). Civilization is not to
squeeze him into the world’s mold (Rom. 12:2). The spirituality of
the Christian is to make a difference in the world,

The Christian is to keep himself “unstained by the world”
(James 1:27). He is warned not to get entangled in the “defilements
of the world” (2 Peter 2:20). Nowhere are Christians told to abandon
the world because of its unspiritual character (Matt. 28:18-20;
John 3:16), to hand the world over to the spirits of darkness.

The “world” is corrupt because people are corrupt. Where cor-
rupt people control certain aspects of the world, we can expect de-
filement. But the world does not have to remain in decay. When
individuals are redeemed, the effects of their redemption should.
spread to the society in which they live and conduct their affairs.
In this case, the effects of regeneration are manifested outwardly.

The world of pagan thinking and practice is to be replaced by
Christian thinking and practice. It is a perversion of the gospel to
maintain that the world, as the domain where evil exists, is inher-
ently corrupt. We should remember that Jesus came to this world
to give His life for the world’s redemption (John 3:16). Jesus’ re-
demptive work is comprehensive enough to affect all aspects of.
life, not just individuals in the world.

By denying the spirituality of God’s created order, we neglect
its importance and give it by default to those who deny Christ.
Worldliness is to be avoided, not the world. The Bible warns us

against worldliness wherever it is found [James 1:27], certainly in
the church, and he is emphasizing here precisely the importance
of Christian involvement in social issues. Regrettably, we tend to
read the Scriptures as though their rejection of a “worldly” life-
style entails a recommendation of an “otherworldly” one.

This approach has led many Christians to abandon the “secu-
lar” realm to the trends and forces of secularism. Indeed, because.
of their two-realm theory, to a large degree, Christians have
