Crying Wolf? 55

not told to wage war against bootleggers, liquor stores, gamblers,

murderers, prostitutes, racketeers, prejudiced persons or institu-

tions or any other existing evil as such. Our ministry is not refor-
mation, but transformation. The gospel does not clean up the
outside but rather regenerates the inside.

While we are told to “render unto Caesar the things that are
Caesar's,” in the true interpretation we have very few ties on this
earth. We pay our taxes, cast our votes as a responsibility of citi-
zenship, obey the laws of the land, and other things demanded of
us by the society in which we live, But at the same time, we are
cognizant that our only purpose on this earth is to know Christ
and to make him known. Believing the Bible as I do, I would find
it impossible to stop preaching the pure saving gospel of Jesus
Christ, and begin doing anything else—including fighting Com-
munism, or participating in civil-rights reforms.

Fifteen years later, Dr. Falwell repudiated his earlier remarks
calling them “false prophecy.” In Listen, Americal Rev, Falwell out-
lines his new agenda; “I am seeking to rally together the people of
this country who still believe in decency, the home, the family,
morality, the free enterprise system, and all the great ideals that
are the cornerstone of this nation. Against the growing tide of per-
missiveness and moral decay that is crushing our society, we must
make a sacred commitment to God Almighty to turn this nation
around immediately.”26

Many have noticed the shift. Dave Hunt, David Wilkerson,
Jimmy Swaggart, and others have noticed. As the earlier quotation
from The Humanist shows, the humanists are also aware of it, and
they are not happy with the turn of events. Paul G. Kirk, Jr., chair-
man of the Democratic National Committee, labeled. conservative,
Bible-believing Christians who are involved in politics as “an ex-
tremist faction.” He is most concerned about the presidential can-
didacy of Pat Robertson. Kirk makes the following points:

25. Quoted by James A. Speer, New Christian Politics (Macon, GA: Mercer
University Press, 1984), pp. 19-20.

26. Jerry Falwell, Listen, Americal (New York: Doubleday, 1980), p. 244.
Falwell has slowly drifted back to his pre-1965 views, although he has not stopped
training “champions for Christ” at his future-oriented Liberty Baptist University.
