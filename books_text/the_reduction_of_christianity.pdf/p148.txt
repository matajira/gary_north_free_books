108 The Reduction of Christianity

Those who propose a sinking ship scenario project no hope for
an earthly future prior to the millennium. There is no possible
chance to change things for the better. People like Coppola paint a
picture of glamor for those without hope. It’s no wonder that we
are losing our future to those who offer at least the temporal vision
of hope.

Phoney as a Three Dollar Bill

The average American and most Christians have grown up
with this “smorgasbord mentality,” so they no longer can tell the
real from the counterfeit. The writer to the Hebrew Christians
describes this mind-set. He stops in mid-thought, wanting to ex-
plain the priesthood of Jesus and how it is similar to the priest-
hood of Melchizedek. He recognizes that their spiritual discern-
ment makes what he wants to write “hard to explain” (Heb. 5:11).

What had happened to these converts? They had become “dull
of hearing” (Heb. : 5:11). By this time in their Christian walk they
should have matured, advancing from “milk” to meat (i Cor.
8:1-2; 1 Peter 2:2). Instead of progressing from the basics and be-
coming “teachers” (Heb. 5:12), they are in need of someone once
again to teach them “the elementary principles of the oracles of
God” (v. 12). As a result, their senses were not trained to discern
good [the real] and evil [the counterfeit] (v. 14). When something
like the New Age Movement comes along, we have no reason to
think that Christians and the typical American religionist will be
able to tell the difference between the real and the counterfeit, un-
less they have progressed to “solid food.”

What is a counterfeit? A counterfeit is an illicit copy of an
original designed to be passed off as the real thing. We're most
familiar with the counterfeiting of United States currency. The
important thing to remember about counterfeiting is that there is
a genuine article that is being copied. If there is no genuine arti-
cle, then there can be no counterfeit. If someone handed you a
three dollar bill, you would know immediately that it wasn't real.
You might, however, be hard pressed to spot a counterfeit ten dol-
Jar bill.
