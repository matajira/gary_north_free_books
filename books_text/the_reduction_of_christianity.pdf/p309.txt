The Zenith and Decline of Optimism 269

shows, “the strength of thé movement in the centers of national
life waned precipitously.”

In the face of all these tumultuous developments, the hope for
cultural victory declined, Late 19th-century and early 20th-
century evangelicals continued to speak of victory, but increas-
ingly the victory was personal and individual, not cultural.

Keeping the Flame Alive

Eschatological optimism never died out completely. In fact,
we can trace a clear line from the late 19th century postmillennial-
ists to the present day. B. B, Warfield, who taught at Princeton
until his death in 1921, was a postmillennialist, The founder of
‘Westminster, Theological Seminary, J. Gresham Machen studied
at Princeton under Warfield. Westminster was founded in 1929,
and Machen taught there until his death in 1937, John Murray,
professor of systematic theology at Westminster from 1930-1966,
was, at least late in his life, something of a postmillennialist. Out-
side of the immediate Westminster community, there were also a
few postmillennial writers. Roderick Campbell's Israel and the New
Covenant was published in 1954, and the introduction by Westmin-
ster Seminary professor ©. T. Allis made clear his own postmil-
lennial convictions. Loraine Boettner studied at Princeton in the
late 1920s, and his postmillennial book The Reformed Doctrine of Pre-
destination was published in 1932, while his more extended post-
millennial study The Millennium was first published in 1957, West-
minster and Princeton graduate, J. Marcellus Kik, a member of
the editorial staff for Christianity Today, delivered his postmillennial
lectures on Matthew 24 and Revelation 20 at Westminster Semin-
ary in. 1961. Kik dedicated one of his books to Roderick Campbell.
Thus, the postmillennialism of Princeton ‘Theology was main-

43, Ibid., p. 185,

44. See Frank, Less Than Conquerors, pp. 125ff.

45. (Nutley, NJ: The Presbyterian and Reformed, {1932] 1987), pp. 130-45.
‘The most recent edition of this book, in 1987, was its twenty-fifth printing. More
than 90,000 copies have been published, including translations into several
foreign languages. This is not an obscure book.
