Crying Wolf? 6

Benito.Mussolini,#@ Adolf Hitler, Henry Kissinger, and the
Papacy* have been mistakenly identified as the “Antichrist.” In
Scripture, the word “Antichrist” is often plural, and it refers to
anyone who denies that Christ came in the flesh to save His peo-
ple (see 1 John 2:18, 22). Taken out of its historical context, almost
anyone can be the Antichrist. Hal Lindsey is correct: “However,
we must not indulge in speculation about whether any of the cur-
rent figures is the Antichrist.”

Predictions of the near end of the world have been a promi-
nent feature of recent evangelical thought. Looking back,.we can
.say with confidence that they were wrong. Of course, this does not
mean that current predictions are automatically wrong because
they have been wrong in the past. It does mean, however, that we
should be careful when it comes to analyzing the Bible in terms of
contemporary events, in what one writer has described as “news-
paper exegesis.”*5 Historian Mark Noll again writes: “The verdict
of history seems clear. Great spiritual. gain comes from living
under the expectation of Christ’s return. But wisdom and re-
straint are-also in order. At the very least, it would be well for
those in our age who predict details and dates for the End to re-

42. “Many will recall widespread preaching during the World War II era that
Mussolini or Hitler was the Antichrist, Since the. slogan VV IL DUCE was
widely used by Mussolini, and because the Roman numeral value of the slogan/
title is 666,-many were sure of positive identification.” David A. Lewis, “The
Antichrist: Number, number, who's got the number?” (no publishing in-
formation).

In a popular tract that was circulated during World War II, Mussolini was
supposed to be the Antichrist: “Someone has to be the Anti-christ. Why not
Mussolini? In his life, death, and his.exhumation he has fulfilled 49 prophesies.
Why not consider him?” From the pamphlet Mussolini... The Antichrist by
McBirnie.

43. Samuel J. Cassels, Christ and Antichrist or Jesus of Nazareth Proved to be the
Messiah and the Papacy Proved to be the Antichrist (Philadelphia, PA: Presbyterian
Board of Publication, 1846); Ralph Woodrow, Great Prophecies of the Bible (River-
side, CA: Ralph Woodrow Evangelistic Association, 1971), pp. 148-200,

44, Hal Lindsey, The Late Great Planet Earth (Grand Rapids, MI: Zondervan,
{1970] 1973), p. 113.

45. Greg L, Bahnsen, “The Prima Facie Acceptability of Postmillennialism,”
pp. 53-55.
