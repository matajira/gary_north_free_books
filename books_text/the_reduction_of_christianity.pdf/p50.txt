10 The Reduction of Christianity

gospel” that in reality was no gospel. Paul then proceeds, in his
letter to the Galatians, to outline once again the basics of the gos-
pel message reminding them that “if righteousness comes through
the Law, then Christ died needlessly” (2:21). Justification by grace
through faith was a test of one’s orthodoxy. You could not claim
the name of Christ and deny justification by the grace of God. A
denial of it meant the repudiation of the faith. Not even “an angel
from heaven” has any authority to preach and thus alter the gospel
message (1:8).

Paul’s disciples at Galatia were not alone in their confusion of
what the Christian message was ali about. All those who claim
Christ should be aware of false doctrine. The Apostle John warns
the church with these words:

Beloved, do not believe every spirit, but test the spirits to see
whether they are from God; because many false prophets have
gone out into the world. By this you know the Spirit of God:
every spirit that confesses that Jesus Christ has come in the flesh
is from God; and every spirit that does not confess Jesus is not
from God; and this is the spirit of the-antichrist, of which you
have heard that it is coming, and now it is already in the world
(1 John 4:1-3).

So then, a creedless Christianity will not do. In fact, a creed-
less Christianity is a contradiction, an impossibility. There must
be a constant appraisal of what the Bible teaches about itself and
about what it means to be a Christian. We are to “test” everything
by the standard of truth. Confessions and creeds are expressions
of unity, demonstrations of a common faith that help the church
gather around truth and fight against error. What a person pro-
fesses to believe about Jesus Christ separates him from all com-
peting faiths. Without a creed there is no difference between belief
and unbelief, saved and lost, truth and error, and salvation and
damnation. A creedless church is no church at all since it has
nothing to distinguish it from the rest of what the world believes.
Church historian Philip Schaff writes that the Christian church
has never been without a creed, for it has never been without con-
