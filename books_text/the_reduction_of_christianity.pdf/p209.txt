The Timing of the Kingdom . 169

Final

The New Testament also. teaches that we look for a future
manifestation of the kingdom (Matt. 25; 1 Cor. 15:23-24; Rev. 21;
otc.). In this sense, we agree with Hunt that the kingdom refers to
heaven and the fullness of the new heavens.and new earth. And
we agree that our true and permanent home is in the heavenly
mansion that Jesus is preparing for us, and that our life here is
from.one perspective a pilgrimage to that blessed land of rest. We
look forward to heaven with joy and expection, knowing that we
shall be forever with our Savior and King in His perfect King-
dom. The hope of heaven helps us endure the trials of the present
life. We look forward to the day when all believers from all lands
will gather to worship the Lamb that was slain from the beginning
of the world, and when we will live in perfect peace and love, free
from the last remnants of sin. Any Christian who does not eagerly
await his heavenly reward is grievously confused. Any Christian
whose sole hope is an earthly reward has not understood Christianity.

But this does not relieve us of responsibility on earth. On the
last day, we will be judged according to our service on earth
(Matthew 25). Thus, we cannot sit on our laurels and wait for
Jesus to come. We must be seeking and, by His grace, extending
Christ's kingdom throughout our lives. Moreover, we do not look
for a new kingdom, The heaverily kingdom is not something that
God will establish for the first time at the end of history, It’s simply
the full and final and glorious manifestation of the kingdom that
was first established 2000 years ago. Since the coming of Christ,
therefore, we can say that the kingdom is both already present in _

 

battle, The kingdom follows the pattern of its King, who was exalted after endur-
ing the Cross.

‘This isa good place to add that there are. some differences among “reconstruc-
tionists.” Many would agree with the view presented here, that the kingdom has
already been established, arid that it is growing over many centuries until the end.
of the world, Others, however, took forward to.a “golden age” in which the king-
dom will advance even more spectacularly than it has in the past. Despite these
differences, however, there is one important common denominator: Christ and
His people will be victorious on earth,
