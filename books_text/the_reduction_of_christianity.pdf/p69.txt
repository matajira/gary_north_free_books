Let's Define Our Terms 29

The church of today has reduced Christianity to regeneration
(being born again) alone. For many Christians there is nothing
more, Few ask the question: “Regeneration for what?” When the
question is asked, the answer that usually comes back is: “Regen-
eration for heaven and only heaven.” Reconstructionists believe
that dominion begiris with regeneration and should encompass all
of life, Christians should keep in mind that dominion cannot be
denied. Rushdoony again writes:

Dominion does not disappear when a man renounces it; it is
simply transferred to another person, perhaps to his wife, chil-
dren, employer, or the state. Where the individual surrenders his
due dominion, where the family abdicates it, and the worker and
employer reduce it, there another party, usually the state, con-
centrates dominion, Where organized society surrenders power,
the mob gains it proportionate to the surrender.

This fact poses the problem, which for an Orwell, who saw
the issue clearly, is impossible to answer. Fallen man’s exercise of
dominion is demonic; it is power for the'sake of power, and its
goal is “a boot stamping on a human face— forever.” Its alterna-
tive is the dominion of anarchy, the bloody and tumultuous reign
of the momentarily strong.

Dominion is a fact. For Christians, it is a lost legacy that must
be regained as we move into the 2ist century. If the clocks of the
prophetic speculators are running fast, then it is imperative that
we begin now to recapture the biblical doctrine of dominion under
the lordship of Jesus Christ. Dominion cannot be avoided.

Kingdom Theology

Kingdom theology grows out of the dominion concept. In fact,
the terms are often used interchangeably. The phrase kingdom
theology is widely used in certain charismatic circles. It has not
been used by those who advocate a dominion theology, although
there are many points of agreement. Basically, kingdom theology
deals with the timing and nature of the kingdom. Is the kingdom

25. Ibid., pp, 440-49.
