Crying Wolf? 57

Platonic® world views. While the Bible addresses only “spiritual*
issues such as prayer and Bible reading,” we are told that it has
little if anything to say about “secular” matters such as economics
and politics, unless we're dealing with the tithe and church gov-
ernment. Sin and the power of the devil make it nearly impossible
for Christians to effect any real and permanent societal changes,
we are assured. The church’s only recourse is to retreat to the
“spiritual” dimension. R. J. Rushdoony has called this the “The
Heresy of the Faithful’;

Many people excuse the extensive apostasy in the Church by
pointing to original sin. Man is so great a sinner, we are told, that
we should not be surprised at the extensive sway of unbelief in
the very hearts of the faithful, let alone the world. We are reminded
that the heart of man “is deceitful above all things, and desper-
ately wicked: who can know it?” (Jer. 17:9). This is true, but the
Scripture is not a Manichaean document. It does not assert that
Satan and sin have a power equal to or greater than God and His
grace. On the-contrary, “God is greater than our hearts” (I John

 

spiritual and material, and seeks salvation by denying his material nature.
‘Manis principal ideas are found in many Christian groups, who teach that the
only. significant part of man is his soul. As a result, political and social concerns
are not considered to be significant for the Christian.

28. Neoplatonism was a modification of Plato’s philosophy that was first
systematized by Plotinus in the 4th century a.p. Like Manichaeanism, Neo-
Platonism often involves a low view of the material world. For the Neo-Platonist,
the world of sense objects— the world that can be seen and felt~is a dim re-
flection of the true world of ideas. The world of sense is therefore less real and
less important than the realm. of ideas.. Neo-Platonic thought has deeply influ-
enced the church. See R, J. Rushdoony, The Flight From Humanity (Tyler, TX:
Thoburn Press, 1973).

29. Prayer and Bible reading are foundational to any real reformation. Prayer
and Bible reading are not ends in themselves but are means for the greater work.
of the kingdom, When thirty-six men were killed in the battle with the men of Ai,
Joshua and the elders prayed to the Lord, But that was not the end of things: “So
the Loxp said to Joshua, ‘Rise up! Why is it that you have fallen on your face?
Israel has. sinned, and they have also transgressed My covenant which I com-
manded them’ (Joshua 7). For a full discussion of the “privatization” of prayer
and Bible reading see R. J. Rushdoony, “Sanctification and History,” in Law and
Society (Vallecito, CA: Ross House Books, 1982), pp. 227-30.
