New Age Humanism: A Kingdom Counterfeit 107

reference to Western civilization has been around a long time. If it
had been taken seriously every time someone used it, we would be
in worse shape than we are now. The Rev. John Newton, the once
infamous slave. trader who wrote: the classic Christian hymn
“Amazing Grace,” used the sinking ship metaphor in the 19th cen-
tury in addressing a minister who believed that the Bible applied
in some measure to politics.

Allow me to say, that it excites both my wonder and concern
that a minister, possessed of the great and important views ex-
pressed in your two sermons, should think it worth his while to
appear in the line of a political writer, or expect to amend our
constitution or situation, by proposals of a political reform.
When I look around upon the present state of the nation, such an
attempt appears to me no less vain and unseasonable, than it
would be to paint a cabin while the ship is sinking, or a parlour
when the house is already on fire.”

Newton’s words are curious in light of his kind words for
William Pitt, of whom he said, “I cannot but think that the provi-
dence of God raised up Mr. Pitt for the good of these kingdoms,
and that no man could do what he has done, unless a blessing
from on high had been upon his counsels and measures."8* Where
would the abolition of slavery have gone without the work of
Wilberforce? Keep in mind that it was Christians who worked to
put an end to the evil trade by which Newton once gained his liv-
ing. There was no civil war in England. It was done with peaceful
tmheans, unlike America’s experience. There was the genuine belief
that when the gospel and God’s law are applied to all aspects of
life, society changes.

 

kind of social evil, and to expect the Christian conquest of the social order, are in-
deed futile. It must be noted, however, that it was such premillennial opinions
that united with Unirarianism in the early 1800s to replace Christian schools with
state schools, so that the church could retreat to a minimal program, revivalism.”
R. J. Rushdoony, God's Plan for Victory: The Meaning of Post Millennialism (Fairfax,
‘VA: Thoburn Press, 1977), p. 9-10.

32. ‘Newton, The Works of the Rev. John Newton, 4 vols. (London: Nathan
Whiting, 1824), vol. 4, pp. 579-80.

33. Ibid., p. 582.
