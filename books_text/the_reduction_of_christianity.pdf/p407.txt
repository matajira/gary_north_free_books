A Christian Reconstruction Library 367

Gary North, The Dominion Covenant: Genesis. 2nd ed.; Tyler,
Texas: Institute for Christian Economics, (1982) 1987. This is the
first volume of North’s multi-volume economic commentary on
the Bible. It explains every verse in Genesis that relates to eco-
nomics. He begins with Genesis 1 and shows how the concept of
man’s exercise of dominion over the earth is inescapable; man is a
dominion agent under God, either as a covenant-keeper or as a
covenant-breaker. The book contains several lengthy appendixes,
including North’s critique of Darwinism in modern thought,
“From Cosmic Purposeless to Humanistic Sovereignty,” which
North regards as his most important single essay.

Ray R. Sutton, That You May Prosper: Dominion By Covenant.
Tyler, Texas: Institute for Christian Economics, 1987. This book
was the first to present the five-point biblical covenant model:
transcendence/presence, hierarchy, law, judgment, and inherit-
ance, It then applies this model to the three covenantal institu-
tions: family, church, and state. It includes detailed appendixes
showing how this five-point model serves as the model for the Ten
Commandments, Psalms, Matthew, Romans, Revelation, and
Hebrews 8:

3. Biblical Law

Greg L. Bahnsen, By This Standard: The Authority of God’s Law
Today. Tyler, Texas: Institute for Christian Economics, 1985. In
this easily read paperback book, Bahnsen presents the case for the
continuing validity of Old Testament law in New Testament
times. Chapters include God’s word as our norm, the entire Bible
as today’s standard, the covenant’s uniform standard of right and
wrong, the categories of God’s law, the political implications of the
comprehensive gospel, law and politics in Old Testament Israel,
and law and politics in the nations around ancient Israel. This
book presents the apologetic case for Rushdoony’s position in The
Institutes of Biblical Law, but in a more easily digested form than in
Baknsen’s much larger work, Theonomy in Christian Ethics (Phillips-
burg, New Jersey: Presbyterian & Reformed, [1977] 1984).
