xxii The Reduction of Christianity

people. It is not “confidence in man” that is the basis of postmillen-
nial optimism; it is confidence in the covenantal faithfulness of God in re-
warding covenant-keepers (Deut. 28:1-14) and punishing covenant-
breakers (Deut. 28:15-68).!5 Listen to the words of Professor
Thomas Sproull over a century ago regarding the coming period
of millennial blessings:

In order to accomplish this, the presence of the humanity of
Christ is not necessary. The destruction of the kingdom of Satan
cannot be done by a nature, but by a person. It is the work not of
humanity, but of divinity. That kingdom extends over the whole
world, and requires for its overthrow an omnipresent power. It
received its death-blow when our Lord by his resurrection was
“declared to be the Son of God.”— Rem 1:14. In his ascension “he
spoiled principalities and powers, and made a show of them
openly.” — Cel. 2:15, His manifestation in the flesh was necessary,
that he might make atonement for sin; but by his incarnation he
received no increase in strength, for vanquishing his enemies. It
is indeed the God-man that gains the victory; not by human, but
by divine power?”

How much plainer could he be? The basis of millennial bless-
ings in history is the power of God in history, not the power of
man in history. Yet our opponents for over a century have boldly
and unconscionably distorted the postmillennialists’ explanation
of the millennium. These leaders have not been ignorant men;
they have been able to read. They have simply and deliberately
preferred to mislead their followers, It is not an intellectual defect
on their part; it is a moral defect.

Dave Hunt has gone one step beyond. He not only rejects
postmillennial optimism, he even implies that to hold such a view
of the future is to give aid to the New Age Movement.

16. Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, TX:
Institute for Christian Economics, 1987), chapter 4.

17, Rev. Thomas Sproull, Prelections on Theology (Pittsburgh, PA: Myers,
Shinkle, & Co., 1882), p. 411.
