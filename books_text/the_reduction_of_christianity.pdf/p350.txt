310 ‘The Reduction of Christianity

All of society must be transformed. We have not arrived when
we can say that we now have a Christian President, a Christian
Supreme Court, a majority of Christian Congressmen, and other
Christian politicians. In fact, we will not have a Christian nation
if we do not have Christian Christians, Christian families, and
Christian churches. Humanism continues to march forward
because our nation is basically humanistic.

Spiritual Kingdomism

Building a Christian civilization is looked upon with suspicion
by those who consider the kingdom of God to be purely spiritual
in nature. For them, the kingdom of God is personal and only has
a spiritual dimension. The passage in Luke. 17:21 restricts the
kingdom to the heart: “The kingdom of God is within you.”
There is no external manifestation of the kingdom, and therefore
there can be no Christian civilization. The church is the domain
of Christian activity. The world is the devil's kingdom.

The kingdom is certainly spiritual, but confusion arises over
the term “spiritual.” To be “spiritual” means to be governed by the
Holy Spirit. For many, spirituality means to be preoccupied with
non-physical reality. Therefore, in this view to be spiritual means
nat to be involved with the material things of this world. Biblically
this is not the case. The devil and his demons are spiritual (non-
physical) and evil: “And I saw coming out of the mouth of the
dragon and out of the mouth of the beast and out of the mouth of
the false prophet, three unclean spirits like frogs; for they are spirits
of demons, performing signs, which go out to the kings of the whole
world, to gather them together for the war of the great day of God
Almighty” (Rev. 16:13-14). There are “deceitful spirits” (1 Tim.

19. The preposition entos in Luke 17:21 can be translated two ways: the “king-
dom of God is among you" or the “kingdom of God is within you.” If the kingdom is
among us it is certainly in us. If che kingdom is within us it has an external effect
as well. If the kingdom of God is within the believer, it ought to energize him to
action in building a civilization that will bring honor and glory to God.
