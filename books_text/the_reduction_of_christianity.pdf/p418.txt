378 The Reduction of Christianity

12, Journals
The Journal of Christian Reconstruction. Published by the Chalce-
don Foundation, P.O. Box 158, Vallecito, California. This schol-
arly journal has been published since 1974. Gary North was its
editor until 1981.

Christianity and Civilization. Published by Geneva Ministries,
P.O. Box 131300, Tyler, TX 75713. Only four volumes of this
journal appeared, 1982-85: two edited by Gary North and two
edited by James Jordan.

13. Newsletters

Newsletters covering many topics are available from the fol-
lowing organizations. As time goes on, many organizations con-
tinue to adopt elements of the Christian Reconstruction position,
and the first thing they do is start a newsletter. These four organi-
zations have been around the longest.

American Vision
P.O. Box 720515
Atlanta, GA 30328

Chalcedon Foundation
P.O. Box 158
Vallecito, CA 95251

Institute for Christian Economics
P.O. Box 8000
Tyler, TX 75711

Counsel of Chalcedon
3032 Hacienda Ct.
Marietta, GA 30066
