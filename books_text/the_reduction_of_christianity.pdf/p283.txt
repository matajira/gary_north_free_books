From the Church Fathers to the Reformation: 243
In commenting on Isaiah 9:1-7, Henry says that Christ’s kingdom

shall be an increasing government. It shall be multiplied; the
bounds of his kingdom shall be more and more enlarged, and
many shall be added to it daily. The lustre of it shall increase, and
it shall shine more and more brightly in the world. The monar-
chies of the earth were each less illustrious than the other, so that
what began in gold ended in iron and clay, and every monarchy
dwindled by degrees; but the kingdom of Christ is a growing
kingdom, and will come to perfection at last.

Thus, though De Jong is right that this optimistic view of the
future was less widespread after 1660, it certainly did not die out
entirely in England. And, it was renewed during the revivals of
the early 18th century.

Conclusion

From the earliest centuries to the 18th century, the doctrine
that the kingdom of Ged would triumph on earth has been taught
by many Christians. While this emphasis varies from writer to
writer and from century to century, a strain of this teaching has
always existed within the Western church. It was very strong in
Reformed churches during the 16th and 17th centuries. In the next
chapter, we will continue this historical survey by examining the
history of American Christianity.

44, Ibid., p. 60.
45, See Murray, The Puritan Hope, chapter 6.
