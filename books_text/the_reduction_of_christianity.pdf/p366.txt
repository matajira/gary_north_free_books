326 The Reduction of Christianity

Winston Churchill would have defined “Christian civilization.”
But he did see something that made him connect Christianity with
the preservation and advance of civilization. England had a long
history of Christian influence that resulted in the advance of civili-
zation around the world, America’s earliest founders did not
break from their English heritage. In fact, they sought to establish
old England in New England.

New England was founded consciously, and in no fit of
absence of mind. Patriots seeking the glory of England first called
the attention of their countrymen to these shores. Commercial
enterprise made the first attempts at settlement. Puritanism over-
laid these feeble beginnings by a proud self-governing common-
wealth, dedicated to the glory of God and the happiness of a pe-
culiar people. These three main streams in the life of old Eng-
land, the patriotic, the commercial, and the religious, mingled
their waters on every slope.%

The colonial colleges of Harvard (1636), William and Mary
(1693), and Yale (1701) were founded upon the university system
in England. Oxford and Cambridge were their models. There
was a disproportionate number of university men who came to
New England in relation to the population. This does not include
those who received a comprehensive and sound classical educa-
tion in the English grammar schools. Of course, the university
graduates had a cultural impact far greater than their numbers.
They were not concentrated in a single geographic area but were
“scattered all over the country.”*3 These were mainly clergymen
who did not serve in the political ruling class. But their influence
was great because they were nearly the exclusive source of infor-
mation for the colonists.* William Bradford, John Cotton, John

52. Samuel Eliot Morison, Builders of the Bay Colony (Boston, MA: Northeast-
ern University Press, [1930] 1981), p. 3.

53, Samuel Eliot Morison, The Intellectual Life of Colonial New England (2nd ed.;
Ithaca, NY: Cornell University Press, [1956] 1965), p. 18.

54. Harry S. Stout, The New England Soul (New York: Oxford University
Press, 1986). See “When God Had No Competition,” Newsweek (October 20,
1986), p. 23.
