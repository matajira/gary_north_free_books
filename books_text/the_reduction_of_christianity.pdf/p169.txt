cl. ." Here a Conspiracy, There a Conspiracy 129

appeal and then fill it with new content. The unsuspecting will be
drawn into the new world view without warning. But in fact, hu-
manism. is much more sinister than the word would suggest.
Francis Schaeffer writes:

The term Aumanism means Man beginning from himself, with
no knowledge except what he himself can discover and no stand-
ard outside of himself. In this view Man is the measure of all
things, as the Enlightenment expressed it. In other words, man-
kind can only look to itself for solutions to its problems and never
looks to God either for salvation or for moral direction, Human-
ism.can‘be seen, then, as the ultimate attempt to pull one’s self up
‘by one’s own bootstraps.

The New Agers are equally adept at choosing the right words.
What if the present New Age Movement used phrases that really
expressed what they believe? They would call their movement
“The anti-God, Man is god, we are god Movement.” Or perhaps,
“Pm all god, you're all god.” This would immediately turn off mil-
lions of people who are normally quite naive when it comes to
spiritual things.

A head-on, frontal attack is unwise if your goal is to capture
the mood and mind of the unsuspecting. “New Age” seems so
optimistic-and upbeat. Who doesn’t want to be part of a new age,
the “Age of Aquarius” as it was described in the 1960s? The aging
traditional New Deal liberal, Max Lerner, in the Foreword to
Marilyn Ferguson's The Aquarian Conspiracy, captures the author's
hopeful prognosis for the future: “She describes with excitement
the world of those who have strained to see past the blinders on
the human. spirit and have thrown them off, and she matches her
own mood to their sense of optimism. ‘I bring you good news’ is
her message.” Such an appeal is attractive to people with little or
no theological training. The Christian knows that the gospel is the

 

10. Francis A. Schaeffer, “The Secular Humanist World View Versus the
Chistian World View and Biblical Perepectives on Military Preparedness,” in
Who is For Peace? (Nashville, TN: Neison, 1983), p. 13.

11, (Los Angeles, California: J. P. Tarcher, Inc., 1980), p. 12.
