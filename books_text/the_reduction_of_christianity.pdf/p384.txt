344 The Reduction of Christianity

Hunt is not opposed to the application of the Bible, but lie
tends to apply it within certain limited areas of life. We believe
that it would be more consistent with his own strong and com-
mendable emphasis on obedience. for Hunt to insist that obedi-
ence extends to every area of life. And, we believe that his incon-
sistency on this point accounts for much of his opposition to
“dominion” Christianity,

Conclusion

Christianity has triumphed over idolatry before, and it can do
so-again. Christianity has brought peace to warring tribes, trans-
formed barbarians into champions of justice and mercy, brigands
into servants of the poor, and rapists into defenders of women.
But a triumphant Christianity must be a complete Christianity.
We.cannot take every thought captive without adequate ammuni-
tion. We cannot fight giants and dragons with-a pocket-knife; we
must wield a double-edged sword. We cannot satisfy the world’s
hunger with a diet of milk; men and women must have bread and
wine and meat. If we are to fill the earth with the knowledge of the
Lord, we must have a full message. If we are to transform the
whole world through the gospel of Christ, we must preach the
whole gospel. If we are to reduce the world to the lordship of
Jesus, we must be done with the reduction of Christianity.
