no The Reduction of Christianity

Jesus’ view of reality was false, the counterfeit, while their view
was true, the original. In order for the Pharisees to keep up the
charade, they needed to get rid of the Original. Their counterfeit
would no longer be considered a counterfeit because there would
be no original around with which to compare it.

The Counterfeit Kingdom

Jesus came to install His kingdom through His marvelous
grace. The kingdom was God’s good news that sinners would be
saved. The political savagery of Rome’s kingdom and its promise
of peace and salvation would die as God’s kingdom flourished in
the light of His unfathomable grace. John the Baptist was its fore-
runner: “Repent, for the kingdom of God is at hand” (Matt. 3:2).
God’s grace made repentance a reality. Without grace repentance
would mean nothing. So entrance into the kingdom is God's do-
ing: “Truly, truly, I say to you, uriless one is born of water and the
Spirit, he cannot enter into the kingdom of God” (John 3:5).

But the King demands obedience. First, the sinner must re~
pent, bow before God in humble submission to Him, in effect, to
surrender unconditionally to God’s demands.* Second, the new
man or woman in Christ must live in terms of the King’s de-
mands. His life must reflect righteousness: “For the kingdom of God
is not eating and drinking, but righteousness and peace and joy in.
the Holy Spirit” (Rom. 14:17; cf. Matt. 6:33). For Jesus, the king-
dom was established by fulfilling “all righteousness” (Matt. 3:15).
This meant that He had to submit Himself to the demands of His
Father. This is why His Father could say at Jesus’ baptism: “This
is My beloved Son, in whom I am well pleased” (Matt. 3:17).

Satan offers a similar program. Entrance into his kingdom
comes through unconditional. surrender to Ais “ethical system”:
“The devil took Him to a very high mountain, and showed Him
all the kingdoms of the world, and their glory; and he said to

 

34. Gary North, Unconditional Surrender: God's Program for Victory (2nd. ed.;
‘Tyler, TX: Institute for Christian Economics, 1988).
