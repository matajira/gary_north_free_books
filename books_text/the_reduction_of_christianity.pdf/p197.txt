The Timing of the Kingdom 157

nium, Hunt, as far as we can tell, disagrees.

It is important to stress this point. If our interpretation of
Hunt's position is correct, he has abandoned the traditional dis-
pensational system at this point. He has denied that the kingdom
of God will ever be manifested on earth, even in the millennium...
Hunt admits that during the millennium, the “whole earth will
resemble the Garden of Eden before the fall.”” But the Garden
was where man first sinned. Similarly, the millennium will end in
disaster:

Converging from all over the world to war against Christ and
the. saints at Jerusalem, these rebels will finally have to be ban-
ished from God’s presence forever (Revelation 20:7-10). The
millennial reign of Christ upon earth, rather than being the king-
dom of God, will in fact be the final proof of the incorrigible
nature of the human heart.®

If this is the case, then all talk of the kingdom of God on earth
is a delusion—a delusion of the Antichrist. This. anti-historical
bias was always implicit in dispensationalism, but Hunt has made
it explicit. There is no hope for Christians in history, not even
during the millennium. Christians will never exercise dominion,
not even during Christ's personal reign from Jerusalem. The rea-
son, Hunt says, is that it is impossible for God to set up an earthly
kingdom. Apparently, Satan is too powerful.

In fact, dominion—taking dominion and setting up the king-
dom of Christ—is an impossibilify, even for God. The millennial
reign of Christ, far from being the kingdom, is actually the fmal
proof of the incorrigible nature of the human heart, because
Christ Himself can’t do what these people say they are going to
do... .*

We would like to believe that Hunt did not think through the
implications of this statement very carefully. As it stands, Hunt is

17. Beyond Seduction, p. 250.

18.. Idem. :

19, Dominion and the Cross, Tape #2 of Dominion: The Word And New World Order,
distributed by the Omnega-Letter, Ontario, Canada, 1987.
