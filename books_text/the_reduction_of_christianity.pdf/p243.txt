Myths of Militancy 203

‘These ideas were in the air from the beginning of the Puritan
Revolution and doubtless contributed to the revolutionary fer-
vor. It is not clear whether thesé men were pre- or postmillennial,
but their tendency toward revolution was obviously fed by a sense
that some dramatic eschatological event was just around the cor-
ner. This frantic sense of imminence, combined with the Puritan
emphasis on reform, was, we believe, a major flaw in the Puritan
outlook at that time, and the Puritans might have avoided some
mistakes if they had not had such a truncated historical perspective.

This short-term view of the future was a motivating force for
the “People’s Crusades” of the eleventh and twelfth centuries. The
people who participated in the Crusade to the Holy Land

saw themselves as actors in the prodigious consummation to-
wards which all things had been working since the beginning of
time. On all sides they beheld the “signs” which were to mark the
beginning of the Last Days, and heard how “the Last Trump pro-
claimed the coming of the righteous Judge.”*

They believed that the biblical prophecies of the end of the world
were just beginning to be fulfilled and that

Antichrist is already born~at any moment Antichrist may
set up his throne in the Temple at Jerusalem: even amongst the
higher clergy there were some who spoke like this. And little as
the phantasies had to do with the calculations of Pope Urban,
they were attributed to him by chroniclers struggling to describe
the atmosphere in which the First Crusade was launched. It is the
will of God~—Urban is made to announce .at Clermont—that
through the labours of the crusaders Christianity shall flourish
again at Jerusalem in these last times, so that when Antichrist
begins his reign here—as shortly he must—he will find enough
Christians to fight.

Similar sentiments were expressed by the Anabaptists who
seized Munster in 1534-1535: °The rest of the earth, it was an-
nounced, was doomed to be destroyed before Easter; but Munster

14. Norman Cohn, The Pursuit of the Millennium (rev. ed.; New York: Oxford
University Press, [1957] 1970), p. 71.
15. Bid, p. 75.
