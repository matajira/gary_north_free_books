Building a Christian Civilization 30t

For man, in the deepest reaches of his being, is religious; he is
determined by his relationship to ‘God. Religion, to paraphrase
the poet's expressive phrase, is not of life a thing apart, it is man’s
whole existence. [John A. Hutchison in Faith, Reason and Exist-
ence], indeed, comes to the same conchision when he says, “For
religion is not one aspect or department of life beside the others,
as modern secular thought likes to believe; it consists rather in
the orientation of all human life to the absolute.”?

In the Soviet Union, for example, a Marxist-Leninist ideology
defines the society, both in philosophy and policy.? The prevailing
ideology directs the nation. In Iran, an extreme form of Islamic
tyranny dominates the nation.

Some societies are in transition. China has broken with many
of its Maoist policies and is now experimenting with Western eco-
nomic practices, still, however, under the strict oversight and con-

 

logical aspects of the church's history to the neglect of how beliefs were translated
into daily life.

“This behavioral approach has already been applied to the study of American
religion, with some interesting results. Dr. Martin E. Marty, church historian at
the University of Chicago, demonstrated in his A Nation of Behavers that it makes
mote sense to classify religious people in contemporary America by their relig:
ious behavior than by the more traditional denominational or even theological
labels” Timothy P. Weber, Living in the Shadow of the Second Coming: American Pre-
millennialism, 1875-1982 (2nd ed.; Grand Rapids, MI: Academie Books/Zonder-
van, 1983), pp. 7-8.

2. Henry R. Van Til, The Calninistic Concept of Culture (Grand Rapids, MI:
Baker, [1959] 1972), p. 37.

3. It's very important to understand that ideology defines culture and thus
gives rise to civilizations. Without understanding the underlying ideology of a
civilization, words-can, and often do, mean different things to different people.
The Soviet Union wants “peace” as well as “democracy.” Can the once-Christian
West work with the non-Christian East since their goals are the same? “[T here is
the problem of logomachy, or the communist device of deceiving their opponents
through the subtle use of words which deliberately lead the non-communist to
understand the words used by communists in a different way to that in which
commiunists themselves understand them. Classic examples of this are the much
used words ‘peace’ and ‘democracy.’ For by ‘peace, the communists mean ‘world
conquest by communism, preferable without (communists) bloodshed,’ and by
‘democracy’ they mean ‘the dictatorship of the Gommnunist Party’ (which they
again misleadingly call ‘the dictatorship of the Proletariat’).” Francis Nigel Lee,
Communist Eschatology: A Christian Philosophical Analysis of the Post-Capitalistic Views
of Marx, Engels, and Lenin (Nutley, NJ: Craig Press, 1974), p. 16.
