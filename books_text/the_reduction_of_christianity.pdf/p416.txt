376 The Reduction of Christianity

sitionalist approach to the philosophical defense of Christianity.
Van Til’s work is the philosophical foundation of Christian
Reconstructionism, although Van Til was not himself a Christian
Reconstructionist. No one has presented the case against the
myth of neutrality more forcefully than Van Til. He shows that all
philosophies are presuppositional. There are really only two sys-
tems: those that presuppose that the God of the Bible created
everything and that His word is therefore the standard of truth,
and those that presuppose that man is ultimately autonomous and
that his word is the standard of truth. ‘Van Til taught philosophy
to the late Francis Schaeffer in the mid-1930s.

Cornelius Van Til, The Defense of the Faith. 2nd ed.; Phillips-
burg, New Jersey: Presbyterian & Reformed, 1963. This is Van
Til’'s most famous book on apologetics (the philosophical defense
of Christianity). The difficulty with reading Van Til is that he ap-
proached every topic by refuting what is wrong in his opponent's
system. This makes for hard reading. But his basic theme is
always present: without presupposing the Creator God of the
Bible, man’s thinking is always incomplete and inconsistent.
Autonomous man, he said, is like a child that must sit on his
father’s lap in order to slap his face.

i. Conspiracies

Douglas R. Grothuis, Unmasking the New Age. Downers Grove,
Illinois: InterVarsity Press, 1986. A calm, scholarly look at the
New Age movement, with chapters on its philosophical roots
(pantheism, monism), the counterculture, holistic health, the
human potential movement in psychology, and New Age spiri-
tuality. It shows how close New Age ideas are to modern human-
ism, This book demonstrates that it is possible for Christians to
examine critically a rival religious movement without becoming
hysterical and. without falling for Satan’s lie that he and his
cohorts will inevitably win in history, making Christians historical
losers.
