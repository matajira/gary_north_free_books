230 Armageddon Now!

62. Christian Century, November 17, 1937, p. 1412, as cited in Fishman,
American Protestantism and a Jewish State, p. 48.

63. Evangel, February 1, 1930, p. 9.
64, Agnes Scott Kent, “Palestine Is for the Jew,” The King’s Business, XXII

(November, 1931), 494. Louis S. Bauman, “Present-Day Fulfillment of
Prophecy,” The King's Business, XXII (July, 1932), 313.

65. Evangel, February 4, 1933, p. 5.

66. Frederick Childe, “Christ's Answer to the Challenge of Communism
and Fascism,” Evangel, October 31, 1931, p. 1. Evangel, December 2, 1939,
p.7.

67. Evangel, December 5, 1931, p. 4, citing S. J. Williams, Jewish Mission-
ary Magazine.

68. Bauman, “Present-Day Fulfiliment of Prophecy,” p. 314.

69, Evangel, July 4, 1936, p. 4, citing Keith L, Brooks, Prophecy.

70. Evangel, November 30, 1937, p. 7.

71. Arthur W. Payne, “Recent Progress in Palestine,” The King's Business,
XXI (March, 1930), 129.

72, W. F, Smalley, “Another View of the Palestine Situation,” The King’s
Business, XXI (June, 1930), 290.

73, Tid. p. 291,

74. Toid.

75. Ibid., p. 292.

76. Prophecy, Ill (November, 1931), 23.

41. Prophecy, 1V (November, 1932), 22. Evangel, June 23, 1934, p. 5, citing
Jewish Missionary Intelligence in Alliance Weekly.

78. William H. Nagel, “Palestine—What of Its Progress? How Bible Prom-
ises Are Being Fulfilled,” Evangel, October 31, 1936, pp. 8-9,

79. Evangel, August 7, 1937, p. 7, citing Jewish Chronicle.
80. Evangel, August 28, 1937, p. 7.

81. Childe, “Christ’s Answer,” p. 1. Bauman, “Present-Day Fulfillment of
Prophecy,” p. 313.

82. George T. B. Davis, Rebuilding Palestine According to Prophecy, p. 112.
83. Evangel, March 18, 1936, p. 6.

84, Oswald J. Smith, Prophecy—What Lies Ahead?, p. 85.

85. Evangel, March 26, 1938, p. 5, citing The Hebrew Christian (italics mine).
86. Evangel, August 12, 1939, p. 9

87. Our Hope, XLVI (1939-40), 179.

88. Christian Century, May 31, 1939, pp. 695-96, as cited in Fishman, Amer-
ican Protestantism and a Jewish State, p. 51.

CHAPTER 6
1. Prophecy, 1V (September, 1932), 22,

2, Evangel, November 12, 1932, p. 14, citing The Life of Faith.
3. As cited in Evangel, January 30, 1937, p. 7.
