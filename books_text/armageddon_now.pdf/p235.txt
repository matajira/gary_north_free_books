Jerusalem! What Now? 203

depicted as a demonic power that is about to pounce on the poor
Jews in the Middle East or is about to come to blows with the
Antichrist leading the European Common Market as the revived
Roman Empire.

Martin R. DeHaan, director of the “Radio Bible Class,” wrote
in 1960 in The King’s Business on “Russian Communism and
God’s Timetable.” He used the time-honored equation of “Magog
equals Russia,” saying that this identification had been the con-
sensus of Bible expositors since Russia’s earliest times. He claimed
that only a few years earlier the fulfillment of Ezekiel’s prophecy
had seemed remote, but “we are today living in the very day
of the fulfillment of these prophecies.” For further emphasis, he
proclaimed:

God has clearly foretold that Russia will meet her doom,
only in Palestine, when she meets up with the Western Al-
liance of nations under the leadership of a Super-man upon
the mountains of Israel....We are, and dare to be, dog-
matic about this fact, for we have the clear teaching of the
Word of God in this matter, Russia’s program of aggression,
bit by bit, is running true to form according to prophecy... .
Oh, that men would turn to this Book for their information
and guidance in dealing with the present Russian crisis.

Unfortunately, DeHaan offered his reader no suggestion of what
should be done, even though he was party to the revealed wisdom
of the ages. He concluded by simply announcing, “The stage is
all set, Israel is in the land, Russia is on the march, the United
Nations described in Ezekiel 38 are seeking to organize, waiting
only for one man, the Super-man of the last days to unite them
against the King of the North.”¢! DeHaan’s dogmatism, however,
was in disagreement with a speaker at the West Coast Prophetic
Congress at Los Angeles in 1961 who taught that the Russians
would not be destroyed by an opposing army, but by the direct,
supernatural intervention of God Himself.®?

Evangelist Billy Graham in his 1965 book dramatically en-
titled World Aflame wisely avoided any specific identification of
nations, but on the very first page observed the world around
him and lamented, “We seem to be plunging madly toward Arma-
geddon.”* Such warnings of doom in this national best-seller by
one so highly esteemed in America served to reinforce the general
