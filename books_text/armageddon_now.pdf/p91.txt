[P=

 

 

 

 

The Twenties Roar Faintly—
Bear, Beast, and Young Lions

Current international events are assuredly finger posts to
Armageddon,

Christabel Pankhurst
—The Lord Cometh, 1923

Here you have read that Russia is going to war with Pales-
tine. That is coming. ... There is where we are to-day. There-
fore, we may expect very shortly that this conflict will take
place,

 

—The Pentecostal Evangel, 1928

If the writer should hazard a prediction he would be
inclined to say that the outcome of the present situation will
finally confirm the Jewish claim and will eventually speed
up developments for setting the stage for the final act of

the end of the age,
—The King's Business, 1929

59
