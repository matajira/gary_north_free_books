Foreword xti

Too long we have allowed servants of the antichrist spirit to be
the activists. It is time to sound a clarion call to the true be-
lievers to become active in the affairs of this very time in
which we live” (p. 139). He tells how pastor Chuck Smith’s
tape Prophecy 1982 and the film Ezekiel File had been cir-
culated among Israeli officials and had influenced their military
decisions. His organizations take out advertisements in the New
York Times to cultivate support for Israel. After the invasion
of Lebanon when he had information which he felt the State
Department was withholding from President Reagan, he sent a
letter and also sent a copy by way of Secretary of Interior
James Watt, a premillenarian, in hopes of getting the informa-
tion directly relayed to Reagan. Lewis lauded the activities of
the International Christian Embassy. After thirteen countries
closed their embassies in Jerusalem in protest of Israel’s declaring
the occupied city as its capital in 1980, a group of Christians
had rented one of the vacated embassies and had established a
“Christian Embassy” to aid and comfort the Jewish people.

The Temple

One sub-theme in the 1977 edition of Armageddon Now!
was the continual resurfacing of rumors about the rebuilding
of the Jewish Temple. This has developed into a melodrama of
epic proportions. John Feinberg has explained the reason for
the premillenarians’ anxious concern, pointing out that “in
order for Daniel’s prophecies and our Lord’s prophecies to be
fulfilled about Temple worship, the Temple must be rebuilt in
Jerusalem” (Fundamentalist Journal, 9/82). The media, includ-
ing the popular television news commentary 60 Minutes, has
popularized interest in the restoration of temple worship. It was
the subject of an article in The New Republic (6/18/84) by
Barbara and Michael Ledeen, “The Temple Mount Plot.” The
authors said, “All we know about the Temple Mount suggests
that it will grow in interest and become a source of conflict.”
Some yeshivas, Jewish schools for training young priests, are
teaching the rituals of temple worship and are being funded by
American premillenarians. The Ledeens believed that money for
groups attempting to blow up the Islamic shrines on the temple
mount was provided by American Christians. Some $50,000 in
legal fees were paid by wealthy Texans to defend four Jewish
