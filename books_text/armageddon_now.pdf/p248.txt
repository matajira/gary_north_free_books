216 Armageddon Now!

but any set of beliefs may be expected to demonstrate in practice
an internal consistency within that body of ideas.

Literalism. If the premillenarians espouse an interpretation of
Scripture that expects a literal, precise fulfillment of prophecy,
then one may expect precise, accurate definitions of the fulfill-
ment of those prophecies, or lacking such precision, one may
expect an admission of indefiniteness which would at least have
the advantage of avoiding false identifications and gross error.
The premillenarians’ history, however, is strewn with a mass of
erroneous speculations which have undermined their credibility.
Sometimes false identifications have been made dogmatically, at
other times only as probabilities or possibilities, but the net result
has always been the same—an increased skepticism toward pre-
millennialism.

Those persons confronted with premillenarians’ presentations
need to be conscious of the composite past of prophetic interpre-
tation which has included the following phenomena. The current
crisis was always identified as a sign of the end, whether it was
the Russo-Japanese War, the First World War, the Second World
War, the Palestine War, the Suez Crisis, the June War, or the
Yom Kippur War. The revival of the Roman Empire has been identi-
fied variously as Mussolini’s empire, the League of Nations, the
United Nations, the European Defense Community, the Common
Market, and NATO. Speculation on the Antichrist has included
Napoleon, Mussolini, Hitler, and Henry Kissinger. The northern
confederation was supposedly formed by the Treaty of Brest-
Litovsk, the Rapallo Treaty, the Nazi-Soviet Pact, and then the
Soviet Bloc. The “kings of the east” have been variously the Turks,
the lost tribes of Israel, Japan, India, and China. The supposed
restoration of Israel has confused the problem of whether the
Jews ate to be restored before or after the coming of the Messiah.
The restoration of the latter rain has been pinpointed to have
begun in 1897, 1917, and 1948. The end of the “times of the
Gentiles” has been placed in 1895, 1917, 1948, and 1967. “Gog”
has been an impending threat since the Crimean War, both under
the Czars and the Communists.

Such loose literalism when considered as a whole is no more
precise than the figurative interpretations of which these literalists
are so critical. The breadth of interpretations might even be
