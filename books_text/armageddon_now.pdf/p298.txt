266

Egypt, 45, Si, 84, 92, 105, 134, 139,
142, 164, 166, 168, 173, 189, 192,
195, 198, 202, 205, 207, 212

Eilat, 189

Einstein, Albert, 73

Eldad, Israel, 199

Elim Evangel, 120

England, 20-21, 28

English, B. Schuyler, 173, 181

Epp, Frank H., 197

Esau, 71, 10%, 202

Esther, 102

Eternity, 13, 197, 200, 213

Ethiopia, 155, 180, 208

Euphrates, 17, 20, 22, 26, 46, 105,
118, 135, 167, 198

Europe, 31, $0, 64, 91, 94-96, 110,
119, 137, 140, 145, 147-48, 151,
155, 160, 184, 209

European Common Market, 51, 121,
203, 207, 209, 216

European Defense Community, 184,
216

European Economic Community,
157, 184

European Recovery Program, 137

Evangelical Alliance, 23

Evangelical Christian, 109, 129, 142

Evangelical Free Church, 191

Evangelical Witness, 76

Evangelicals, 12, 169, 192, 197, 210

evangelism, 218

 

Faber, George Stanley, 20-21

False Prophet, 16

false prophet, 64

Far East, 78, 80, 110, 159-60

fascism, 108-09, 115, 120

Federated Council of Churches, 111

Feinberg, Charles L., 163, 174, 201

Ferdinand, Louis, 116

Fereday, W. W., 43, 157

Fifth Monarchy, 17

fig tree,” 25, 42, 60, 195

“final solution,” 137

Finch, Henry, 17

First Baptist Church, Binghamton,
NY, 56

First Baptist Church, Dallas, 213

First Baptist Church, New York, St,
54

First Brethren Church, Washington,
D.C., 177

Armageddon Now!

Fish, Hamilton, 88

Fisher, H. E., 158

Fishman, Hertzel, 43, 74, 101

Fodor, Marcel W., 117

Ford, Henry, 75

Fourteen Points, 64

France, 19, 29, 32, 49, 64, 121, 166,
184, 190

Frankfort, 20

Freedom Party, 200

French Revolution, 22, 53

Fuller Theological Seminary, 170

Fundamentalist, 15, 137, 192, 194

futurist, 29-30

Gaebelein, Arno C., 37, 45, 48-51,
54, 73, 80, 97-98, 115, 118,
120-21, 128, 130, 132, 145-46,
150-51, 157-58

Galway, Henry, 61

Gartenhaus, Jacob, 91

Gaul, 27

Gaza Strip, 189, 192

General Council of the Assemblies of
God, 115, 137-38

Geneva Conference, 196

Genoa Conference, 79

gentiles, 16, 20, 29, 53, 76-77, 92,
137, 164, 190, 192, 195, 211

Germany, 17, 27-29, 50, 53, 79-80,
82, 91-92, 94, 106, 109, 112-16,
137-38, 168, 174, 180

Gesenius, Wilhelm, 152

Gilbert, Dan, 117, 144, 155, 159

Glad Tidings Bible Institute, 178

Gog, 16-18, 21, 27, 32-33, 48, 50,
77, 80, 108, 184, 124, 145-46, 150,
160, 169, 177-78, 182, 184, 188,
216

Golan Heights, 189, 196

Gomer, 27-28, 50, 80, 87, 112-16,
145, 152

Gordon, Ernest, 109

Gortner, J. Narver, 178

Grace Theological Seminary, 113-14

Graham, Billy, 193, 203

Gray, James M., 34, 38, 79, 117

Great Britain, 22, 29, 32-34, 39,
44-45, 48-49, 60, 62, 64, 66-67,
74, 82, 84-85, 87-88, 92, 100,
103-04, 113-14, 118, 122, 126-29,
131, 136, 146, 157-58, 166, 173,
185, 190, 209-10, 213
