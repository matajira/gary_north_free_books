46 Armageddon Now!

the average had been 21.87 inches, in the 70s 24.60, in the 80s
27.69, and finally in the 90s 28.86—an increase of seven inches.
This was interpreted as a restoration of rainfall in preparation
for the restoration of the people, but the article conveniently
failed to mention that complete rainfall records had begun to be
kept only in 1846 and that the average rainfall in the 1850s
had been a greater amount—28.82 inches.*? According to the
Evangel: “Now that the latter rain has returned, the country will
soon blossom as the rose”—in fulfillment of Isaiah 35:1.

One argument raised by opponents of the restoration of the
Jews to Palestine was that there was not enough room for them
in the land. The premillenarians countered with Biblical claims
of even greater territories that exceeded even the wildest Zionist
dreams. In an address at the Mountain Lake Park Bible Conference
in Maryland, Joseph W. Kemp spoke on “The Tewish Tragedy”
and pointed out that God had promised even more land to Abra-
ham in Genesis 15:18: “Unto thy seed have I given this land,
from the river of Egypt unto the great river, the river Euphrates.”
The speaker confessed that this territory had never been more
than partially held by Israel; nevertheless, he asserted that the
“promise waits for complete fulfillment, which will assuredly take
place in the near future.’

Commenting on the suggestion that a new Jewish state would
include Mesopotamia, Our Hope noted that that was “exactly
what God has promised to the natural descendants of Abraham.”*>
In responding at another time to the question of the size of the
land, the editor affirmed that the “immense territory from the Nile
to the fertile plains of Mesopotamia” would belong to Israe} in
the future. He was not necessarily advocating such a move by the
nations, however, for he believed that this would not transpire
until the land had been given to the Jews by the returned Messiah.

These discussions took no account whatsoever of the existing
inhabitants of these territories. The Alliance Weekly approvingly
cited an article from the Boston Evening Transcript which said
that “no one to any extent inhabits and cultivates this fair portion
of the earth.” The author explained that God had controlled his-
tory so that neither the Crusaders nor the Russians had ever been
able to take the land: “Everyone knows that Russia, with super-
stitious devotion, would have populated Palestine, so, when the
