44 Armageddon Now!

leading liberal Protestant weekly has maintained a consistent an-
tipathy toward Zionist aspirations to the present day.

On December 9, 1917, the Turks abandoned Jerusalem to the
advancing British forces under General Edmund Allenby without
attempting a defense—a fact to be colorfully elaborated by the
premillenarian authors over the years. The Evangel carried an
article, “What It Means. The British in Jerusalem,” in which the
author recounted an article from the secular press which recorded
the responses of various Jewish and Protestant leaders to the
question of the meaning of the imminent fall of Jerusalem for
the church and for the world. The Evangel article lamented the
Tesponses which averred that “the Jews would return in unbelief
and that the Christian leaders of today have utterly failed to grasp
the import of what is likely to be one of the most significant
and far-reaching events of this momentous period.” Jesus had
prophesied in Luke 21:24, “Jerusalem shall be trodden down of
the Gentiles, until the times of the Gentiles be fulfilled.” Thus
if Jerusalem fell, it meant that “automatically the time of the
Gentiles closes,""5

In another article probably written confidently before the fall,
A. B. Simpson of the Christian Missionary Alliance was exultant:

How stupendous the significance of this event must be is
impossible for the most intense language to exaggerate.
This great event is therefore a note of time, a signal from
heaven, and the marking of an epoch of history and proph-
ecy. An age-long period of more than twenty-five centuries
is closing, and a new age is about to begin or has already
commenced. However gradual its progression may be and
however slowly its preliminary unfoldings may appear, the
fact remains that we have entered a new zone and we are
already in the beginning of the end.?¢

In a subsequent piece, he called the city’s fall “the best news
in a thousand years,” pointing out that the preparation for the
event had begun with the advent of Zionism in 1897 exactly 1260
prophetic years after the Turks had captured Jerusalem in 637.57
Such a historicist view was also represented in the Evangel in an
article which stated that “all schools of interpretation” were in
agreement that history was in the time of fulfillment of the
prophetic 2,520 years and that the latest date possible was 1934,
