The Certainty of Survival 165

Sayegh recalled that Israel had been condemned by the Mixed
Armistice Commission no less than twenty times for raids across
the armistice lines, and claimed that no Arab state had ever
been condemned by any United Nations organ for such raids on
Jewish territory. He decried the discrimination against Arabs who
had remained in Israel, being subjected to military government,
suffering discrimination in citizenship requirements, receiving de-
pressed wages, and being sentenced without trial. He supported
his contentions from a Jewish source, William Zukerman’s Jewish
Newsletter: “A more flagrant case of discrimination is hard to
find even in the annals of the chauvinistic twentieth century.”

Moody Monthly's July 1955 issue carried the rebuttals of
Karl Baehr, executive director of the American Christian Palestine
Committee, and Mr. Unna. Unna accused the Arabs of responsi-
bility for the 1948 war by refusing to accept partition, or any
compromise, and by invading Israeli territory. He failed to offer
any reason why they should wish to compromise on the confisca-
tion of their homeland. One telling point was his ridiculing of
Sayegh’s claim that no Arab state had been condemned: “I fail
to see any difference in the wordings of the respective United
Nations Mixed Armistice Commission resolutions which frequently
condemned the various Arab countries and those which much less
frequently condemned Israel for defending its borders.” Arab
claims of peaceful intentions were belied, he said, by statements
such as that by Major Salah Salem, Egypt’s minister of propa-
ganda, printed in the Manchester Guardian: “Egypt will strive
to erase the shame of the Palestinian war, even if Israel should
fulfill the United Nations resolutions.” Karl Baehr observed that
Sayegh had failed to remember the Arab massacre of seventy-six
Hadassah doctors and nurses and the Security Council’s double
condemnation of the Arab blockade of Israeli shipping on the
Suez Canal. His analysis was particularly acute when he observed
that “in the Arab-Israel impasse, we are not dealing with an
issue in which one side is all white and the other all black. We
have here a conflict of rights as well as a confusion of wrongs.”
Even that much of a concession was enlightening to premillenarian
readers.™

In his rebuttal Sayegh chose to reinforce his earlier arguments
and attempted to show that the official Zionist policy had been
