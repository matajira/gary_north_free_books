228 Armageddon Now!

90. Evangel, March 12, 1927, p. 3.

91. J. N. Hoover, “Mussolis Is the World Preparing for Antichrist?”
Evangel, November 26, 1927, p. 1.

92. Chalmers, “The Shadow of Armageddon,” p. 6.

93. Reginald T. Naish, The Midnight Hour and Aftert, pp. 172-73.

94. F. E. Howitt, “Does the United States Appear in Prophecy?” The King’s
Business, XXII (April, 1931), 153-54.

95. John H. Baxter, “The Spiritual Values of Armageddon,” Evangel, April
13, 1929, pp. 2-3.

 

CHAPTER 5
1. Prophecy, IL (October, 1931), 7.
2. Agnes Scott Kent, “Palestine Is for the Jew," The King's Business, XXII
(November, 1931), 494.

3. Charles G. Trumbull, Prophecy’s Light on Today, p. 72.

4. The Pentecostal Evangel, July 26, 1930, p. 4. (Hereafter cited as
Evangel)

$. Aaron Judah Kligerman, “Israel and Palestine,” Moody Bible Institute
Monthly, XXX (August, 1930), 587-88. (Hereafter cited as Moody Monthly)
6. As cited in Evangel, December 13, 1930, p. 6.

7. Evangel, December 6, 1930, p. 4.

8. J. A. Huffman, “The Jew and Arab Controversy over Palestine,” The
King’s Business, XXU (September, 1930), 417-18.

9. The King’s Business, XXUL (April, 1931), 149.

10. Our Hope, XLI (1934-35), 377.

11. Evangel, March 3, 1934, p, 5.

12. Myer Pearlman, “Jewish Notes,” Evangel, August 20, 1932, p. 7.

33. Edward Hilary Moseley, The Jew and His Destiny, p, 10.

14, As cited in Evangel, February 1, 1936, p. 11.

15. Evangel, April 20, 1935, p. 5.

16. Evangel, February 24, 1934, p. 4.

17. Evangel, January S, 1935, p. 5.

18. Frank H. Epp, Whose Land ts Palestine? The Middle East Problem in
Historical Perspective, p. 147.

19. Jacob Gartenhaus, What of the Jews?, p. 47.

20. Jacob Gartenhaus, The Rebirth of a Nation: Zionism in History and
Prophecy, p. 128.

21. Leonard Sale-Harrison, “The Approaching Combination of Nations As It
Affects Palestine,” Moody Monthly, XXXVII (September, 1936), 18.

22. Evangel, February 11, 1933, p. 5.

23. Evangel, November 15, 1930, p. 5.

2A Oee 3, Klink, “The Jew—God’s Great Timepiece,” Evangel, May 9,
25. Evangel, April 25, 1938, p. 4.

26. Moody Monthly, XXXI (January, 1931), 239.
