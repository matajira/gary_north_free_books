Footnotes 229

27. Moody Monthly, XXXL (February, 1931), 346.

28. Louis S. Bauman, Shirts and Sheets: or Anti-Semitism, a Present-day
Sign of the First Magnitude, pp. 41-44,

29, The social views of conservative Christians, of whom premillenarians may
be considered a part, have been surveyed in Kenneth K. Bailey, Southern
White Protestantism in the Twentieth Century, and in Lowell D, Streiker
and Gerald S. Strober, Religion and the New Majority: Billy Graham, Mid-
dle America, and the Politics of the 70's,

30. Arthur D. Morse, While Six Million Died: A Chronicle of American
Apathy, p. 169.

31. Evangel, September 23, 1933, p. 5.

32. Evangel, February 24, 1934, p. 5.

33. Evangel, August 31, 1935, p. 5.

34. Wilbur M. Smith, “With the Bible in the Land of the Book,” Moody
Monthly, XXXIX (October, 1938), 69. (Moody Monthly is now the of-
ficial title.)

35. The Alliance Weekly, LXXIV (January 21, 1939), 34.

36. Bauman, Shirts and Sheets, p. 20.

37, Ibid., pp. 50-51.

38. Dbid, p. 50.

39. Arno C. Gaebelein, The Conflict of the Ages: The Mystery of Lawless-
ness: Its Origin, Historic Development and Coming Defeat, p. 100.

40. Thomas M. Chalmers, “The Present Situation in World Jewry,” The
King’s Business, XXV (Jane, 1934), p. 216.

41, Bauman, Shirts and Sheets, p. 18.
42. Time, November 12, 1934.
43. Evangel, May 18, 1935, p. 1.

44, William Bell Riley, Wanted—A World Leader! Riley was president of
the World’s Christian Fundamentals Association.

45. S. A. Jamieson, “The Signs of the Times,” Evangel, April 4, 1931, p. 9.
46. The New York Times, July 20, 1930, p. 8.

47, Ammianus Marcellinus, x: 2,

48. Evangel, March 12, 1932, p. 3.

49. Gaebelein, Conflict of the Ages, p. 30.

50. Cited in Evangel, February 1, 1936, p. 11.

51. Evangel, August 7, 1937, p. 7.

52, Charles S. Price, The Battle of Armageddon, p. 58.

53. Ibid., p. 42.

54, Cited in Evangel, August 22, 1936, p. 7.

55. Trumbull, Prophecy’s Light on Today, p. 67.

56, Riley, Wanted—A World Leadert, p. 27.

57. Evangel, August 28, 1937, p. 7.

58. Evangel, November 6, 1937, p. 7, citing John C. Smith, Advent Herald.
59. Evangel, December 4, 1937, p. 7.

60. Evangel, February 17, 1940, p. IL.

61. Hertzel Fishman, American Protestantism and a Jewish State, p. 45.

 
