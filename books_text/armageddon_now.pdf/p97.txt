The Twenties Roar Faintly—Bear, Beast, and Young Lions 65

gation against the policy of Jewish immigration and the Balfour
Declaration. Later, the commission’s final report recommended
that “Jewish immigration should be definitely limited, and that
the project for making Palestine distinctly a Jewish commonwealth
should be given up.”*? But the British and the French view pre-
vailed. That is not surprising considering Wilson’s support of the
Balfour Declaration back in 1917.

The Arab protest movement eventually caused Britain to re-
interpret the Balfour Declaration in order to avoid an immediate
Arab rebellion. A memorandum by the Colonial Secretary, Win-
ston Churchill, on June 3, 1922, stated that “the terms of the
Declaration referred to do not contemplate that Palestine as a
whole should be converted into a Jewish National Home, but that
such a Home should be founded in Palestine.” This was a blow
to Zionist aspirations and was duly reported by The King's Busi-
ness as such. Churchill was clearly the villain in this situation,
and the journal commented that “in spite of enemies within Israel
and enemies without, the plan of God will be carried through,
and finally all Israel will return to their own land.”?3 Two months
later, the magazine devoted two full pages to a discussion of the
issues. It favorably reported that the United States Senate the
previous May had unanimously passed a resolution favoring “the
establishment in Palestine of the National Home for the Jewish
people” and that this had brought “great joy and satisfaction, and
fresh courage” to the Zionist movement. This was not to be re-
flected in American policy, however, as the Department of State
throughout the 20s and 30s usually avoided supporting Zionist
interests because of American oil interests and general isolationism.
In this article, a policy statement by Churchill was interpreted
favorably, even though his accurate appraisal was anti-Zionist:
“The only cause of unrest in Palestine arises from the Zionist
movement and the British promises in regard to it.” The article
emphasized, rather, the other aspect of his statement: “The task
in Palestine is one that England has imposed upon herself and
which she is bound to perform unless she is prepared to admit
that the word of England no longer counted throughout the Near
East.” Churchill was referring to Britain’s Palestine mandate
which had been approved by the League of Nations on July 24,
1922, but this was interpreted by the writer as a commitment
