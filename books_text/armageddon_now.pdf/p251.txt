Notes

 

 

 

CHAPTER 2
1, Cyrus Ingerson Scofield, ed., The Scofield Reference Bible (New York:
Oxford University Press, 1909).

2, Ernest R. Sandeen, The Roots of Fundamentalism: British and American
Millenarianism, 1800-1930, p. 222,

3, James M. Morton, Jr., “The Millenarian Movement in America and Ita
Effect upon the Faith and Fellowship of the Southern Baptist Convention,”
p. 44,

4. Scofield Bible, Isaiah 10:12, n.
5. Ezekiel 38:2, 3,
6. For Scofield, “kingdom-age” equals the millennium.

7. Peter Toon, ed, Puritans, The Millennium and the Future of Israel:
Puritan Eschatology, 1600-1660, p. 126, Sir Henry Finch, The World's
Great Restauration, or The Calling of the Jews, pp. 2-3.

8 R. G. Clouse, “The Rebirth of Millenarianism,” in Puritan Eschatology,
ed. Peter Toon, p. 49,

9. Ibid., p. 62.
10. Daniel 2:44.

11. B. S. Capp, “Extreme Millenarianism,” in Puritan Eschatology, od.
Peter Toon, p. 68.

12. Ibid., pp. 70-71.
13, Tbid., p. 69.
14. Toon, Puritan Eschatology, p. 127.

219
