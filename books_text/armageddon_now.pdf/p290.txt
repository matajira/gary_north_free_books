258 Armageddon Now!

of the Struggle for Israel to the Sinai Conflict.” Unpublished Ph.D.
dissertation, University of Idaho, 1971.

Koeppen, Sheliah R. “Dissensus and Discontent: The Clientele of the
Christian Anti-Communism Crusade.” Unpublished Ph.D. disser-
tation, Stanford University, 1967.

Leith, D, Malcomb. “American Christian Support for a Jewish Palestine:
From the Second World War to the Establishment of the State of
Israel.” Unpublished senior thesis, Princeton University, 1957.

Mills, Hawthorne Quinn. “American Zionism and Foreign Policy.” Un-
published M.A. thesis, University of California, Berkeley, 1958.

Morton, James M., Jr. “The Millenarian Movement in America and Its
Effect upon the Faith and Fellowship of the Southern Baptist
Convention.” Unpublished Th.M. thesis, Golden Gate Baptist Theo-
logical Seminary, Mill Valley, California, 1962.

Propst, John Henry, Jr. “The Relation of Pessimism to Millennial Ideas.”
Unpublished Th.D. dissertation, Southwestern Baptist Theological
Seminary, Fort Worth, Texas, 1962.

Snetsinger, John G. “Truman and the Creation of Israel.” Unpublished
Ph.D. dissertation, Stanford University, 1970.
