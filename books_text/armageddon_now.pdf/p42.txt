alii Armageddon Now!

youths caught trying to break through the underground to the
mount. One of the radical Jewish leaders explained on Israeli
television that “the people involved in the plotting believed that
the destruction of the two Muslim holy places would provoke
the Muslims to wage a holy war so terrible that the Messiah
would come to save his people from destruction.” We can see
here the potential that premillenarians may not only be guilty
of creating expectations of WWIII, but could be guilty of caus-
ing it.

One of the action groups involved in encouraging the
rebuilding of the temple is the Jerusalem Temple Foundation.
One member is Chuck Smith, whose 25,000-member church pro-
vided $50,000 for Lambert Dolphin to explore new electronic
technologies for underground surveying on the temple mount.
The project was halted becanse Dolphin feared for his life, but
the preliminary conclusion was that the temple could be rebuilt
exactly on its old site without destroying the Muslim shrines.
This scenario was more fully developed in Grant R. Jeffrey’s
Armageddon: Appointment with Destiny (1988), but with a new
twist. Jeffrey believes that the ancient Ark of the Covenant is
not buried on the temple mount as some think, but is hidden
in some church in Ethiopia. This too promises fruitful specula-
tion for the doomsayers as a.p. 2000 impinges upon us,

Means and Ends

One last word about style. Some critics have told me that I
am too abrasive. In response, it has been my practice over the
years to autograph copies of Armageddon Now! with the mod-
erating scripture reference, Ephesians 4:15, “speaking the truth
in love.” But I must confess that what I have observed in the
last fifteen years has caused my love to become tempered with
indignation. For the most part, the people of whom I write are
not hypocrites; they are True Believers. The resulting damage to
Christ’s Church is still the same, however. I fear that the credi-
bility gap will only widen, but I hope that the means that I
have used will help end the misuse of The End.!

1, To preserve the continuity of the page numbers of the 1977 edition with
this one, the Introduction begins on page 11.
