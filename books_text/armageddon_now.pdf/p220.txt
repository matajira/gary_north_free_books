10

Jerusalem! What Now?

 

 

 

 

This, then, said Ezekiel 2,500 years ago, would comprise
the great northern federation of Gog and Magog, and we
are today living in the very day of the fulfillment of these
prophecies.

—Manrtin R. DeHaan, The King’s Business, 1961

That for the first time in more than 2,000 years Jerusalem
is now completely in the hands of the Jews gives a student
of the Bible a thrill and a renewed faith in the accuracy and
validity of the Bible.

—L, Nelson Bell, Christianity Today, 1967

The current build-up of Russian ships in the Mediterranean
serves as another significant sign of the possible nearness of
Armageddon.

—Hal Lindsey, The Late Great Planet Earth, 1970

Since the 1950s the most significant event for the premil-
lenarian was Israel’s conquest of Jerusalem in the Six-Day War
of June, 1967. John F. Walvoord, the president of Dallas Theo-

188
