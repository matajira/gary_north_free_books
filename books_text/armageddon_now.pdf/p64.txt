32 Annageddon Now!

Jews, arguing that “if so literally have been fulfilled the prophe-
cies which foretold their sufferings and their preservation, equally
sure are the predicted grandeurs of their future.”85

In the same year appeared The Approaching End of the Age
by H. Grattan Guinness, an English author. It was widely read
and cited in the United States. Guinness was a historicist. His
calculations placed the last possible culmination of Daniel’s prophe-
cies in 1923; and he believed “that the great closing Armageddon
conflict is at hand.”8? Expecting that Israel would be restored
to Palestine before the end, he saw signs of this in “the elevation
in the condition of the land and people of Israel, the removal of
Jewish (political) disabilities, the formation of the Universal
Israelite Alliance, the exploration and survey of Palestine, the
decay of the Turkish Power.”® The fall of Jerusalem to British
and French troops and the issuance of the Balfour Declaration in
1917 appeared to be such pointed fulfillments of Guinness’ ex-
pectations that his book was reissued in a revised edition in 1918.

An international Prophetic Conference was held in 1886 in
Chicago. Conference proceedings were published the next day
in Chicago’s Inter Ocean newspaper and a bound edition appeared.
within ten days of the closing of the conference.®® Nathaniel West
lectured on “Prophecy and Israel,” reflecting a very deterministic
philosophy of history which is characteristic of premillenarian
attitudes toward Israel.

History itself is Messianic, Events do not come to pass
because predicted, but are predicted because ordained to
come to pass... .

The fortunes of Israel are, have been, and will be precisely
what God intends. ...

A divine causality pervades all. Israel, already in the
front in centuries gone by, shall yet be in the front again.

Another speaker, William G. Moorehead, Professor of New
Testament at the United Presbyterian Seminary, Xenia, Ohio,
spoke on “The Antichrist,” listing “the principles now at work
in our modern society which, if left unchecked, will soon make
the advent of the Antichrist not only possible, but certain.” These
Satanic influences were socialism, nihilism, anarchy, naturalism,
materialism, humanitarianism, and spiritualism.** When this Satanic
socialism became wedded to the Russian “Gog” in the 1917
