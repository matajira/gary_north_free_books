Foreword XxXY

and Palestinian Nationalism,” Fides et Historia (9/85). Popular
support for Israel peaked in the 1973 Yom Kippur War which
pitted the Israeli David against the Arab Goliath. Since then,
however, various factors have tended to erode that support. In
1977, Israel elected a right-wing government which brought
former terrorist leaders into national leadership, people such as
Menachem Begin and Yitzhak Shamir. Jerusalem and the Golan
Heights, the spoils of war, were annexed in violation of inter-
national law. Occupied territories have been turned into Jewish
settlements. In 1982, Israeli forces invaded Lebanon and were
subsequently blamed for encouraging massacres by the Christian
factions there. Direct action tactics of destroying homes of
suspected terrorists has brought international criticism. Premil-
lenarians have responded to this negative press by even more
adamant calls for the faithful to support God’s Chosen People.
Frequently, unfavorable information is just ignored. I have
found as yet no reference to the publication in 1980 of former
Prime Minister Moshe Sheratt’s diaries, which show that much
of the trouble on Israel’s borders has been a result of the con-
scious expansionist policies initiated by the Israelis themselves.
The diaries were published in Hebrew but were excerpted in
English in Livia Rokach’s Israel’s Sacred Terrorism (1980).
Following the Yom Kippur War and the consequent oil crisis
produced by the Organization of Oi] Producing Countries,
Moody Monthly (6/74) published an opinion article by Mal
Couch entitled “Let’s Not Let Israel Down.” Couch was a pro-
ducer of documentary films on Israel who feared that Ameri-
cans might succumb to the Arab blackmail and change their
pro-Israel stance because of the need for oil. He said:

The evangelical public, I believe, must stand behind the na-
tion of Israel.

I do not say this merely for political reasons, but for
biblical ones. Nor do I suggest that everything Israel does
and says is right..., Yet as a people God has an earthly
plan for them....And if they are beloved to God, they
had better be beloved to us.

Couch then epitomized the premillenarian stance by saying,
“Israel has her faults, but I believe we must stand behind her.”

Fourteen years later, Moody (5/88) in a new context carried
a similar opinion article by Kerby Anderson, identified as a
