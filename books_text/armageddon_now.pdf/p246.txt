214 Anmageddon Now!

above: an indication of a neo-evangelical trend toward social
action on all levels.

In recent years some premillenarians have developed a more
discriminating criticism of Israel’s actions, but in times of crisis
such as the Yom Kippur War, many are quick to revert to their
standard twentieth-century response: a nonmoral, deterministic
support of Israel and a belligerent prejudice toward a demonic
Russia,
