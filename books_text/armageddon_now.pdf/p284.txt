252 Armageddon Now!

McConkey, James H. The End of the Age. Fifteenth edition. Pittsburgh:
Silver Publishing Company, 1925.

Manuel, Frank E. The Realities of American-Palestine Relations. Wash-
ington, DC: Public Affairs Press, 1949,

Mather, Increase, The Mystery of Israel's Salvation Explained and
Applyed, or A Discourse Concerning the General Conversion of the
Israelitish Nation. London: John Alien, 1669.

Meyer, Isidore S,, ed. Early History of Zionism in America. New York:
American Jewish Historical Society and Theodor Herzl Foundation,
1958,

Miller, Perry. Errand into the Wilderness. New York: Harper and Row,
1964.

Morse, Arthur D. While Six Million Died: A Chronicle of American
Apathy. New York: Random House, 1967.

Moseley, Edward Hilary. The Jew and His Destiny. Berne, IN: The
Berne Witness, 1939.

Murray, Robert K. Red Scare: A Study in National Hysteria, 1919-
1920. Minneapolis: University of Minnesota Press, 1955.

Naish, Reginald T. The Midnight Hour and After! Seventh edition.
London: Chas, J, Thynne & Jarvis, Ltd,, 1928.

Needham, George C., ed. Prophetic Studies of the International Pro-
Phetic Conference, Chicago: Fleming H. Revell, 1886,

Ober, Douglas. The Great World Crisis, Wheaton, IL: Van Kampen
Press, 1950,

Olson, Arnold Theodore, Inside Jerusalem, City of Destiny. Glendale,
CA: Regal Books Division, G/L Publications, 1968.

Otis, George. The Ghost of Hagar. Van Nuys, CA: Time-Light Books,
1974,

Owen, Frederick, Abraham to Allenby. Grand Rapids: William B,
Eerdmans, 1939,

. Abraham to the Middle-East Crisis. Grand Rapids: William
B. Eerdmans, 1957.

Palestine: A Study of Jewish, Arab, and British Policies. New Haven,
CT: Yale University Press, 1947.

Pankhurst, Christabel. The Lord Cometh, New York: The Book Stall,
1923,

. Seeing the Future. New York: Harper and Brothers Pub-
lishers, 1929,

. Some Modern Problems in the Light of Prophecy. New York:
Revell, 1924.

. The Uncurtained Future. London: Hodder and Stoughton,
1940,

Panton, D. M. The Panton Papers, New York: T. M, Chalmers, 1925.

 

 

 

 
