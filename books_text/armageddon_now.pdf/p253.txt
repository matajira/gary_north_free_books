Footnotes 221

51,

John Cumming, The End: or The Proximate Signs of the Close of This

Dispensation, pp. 92-94.

. Ibid., pp, 95-96,

Cumming, Signs of the Times, pp, 142-43.

. Ibid, p. 152.
. Cumming, The End, p. 213,

Ibid, p. 212.
Ibid, p. 214.

. Ibid., pp. 260-62.
. Ibid., p. 267.
. Ibid, p. 264.
. Ibid., pp. 264-65,
, Ibid, p. 269.
. Ibid, p. 274,
. Ibid, p. 286,

Ibid, p. 276.
Ibid,, p. 275.

. bid, p. 276.

Thi¢., p. 277.

. Ibid, p. 280.
. John Cumming, The Great Preparation; or Redemption Draweth Nigh,

pp. xli-xlif

71.
72.

88.
89.
90.

Tbid., p. xlii.
Toid,, p. xiii.

. bid, p. 166,
. Sandeen, The Roots of Fundamentalism, pp. 59, 98.
. James H. Brookes, Maranatha: Or, The Lord Cometh, p. 418,

Tbid., pp. 392-96. See Genesis 13:14-17, 17:8, and 48:4.
Ibid., p. 424. See Daniel 9:24.

. Ibid, p. 426.

. Ibid., p. 430.

. Ibid, p. 436.

. Ibid, pp. 443-45.

. Sandeen, The Roots of Fundamentatisin, p. 134.

. Ibid., pp. 94-96.

. Ibid, pp. 276-77.

. Nathaniel West, ed., Premillennial Essays, p. 239.

. H. Grattan Guinness, The Approaching End of the Age, p. 368.
. Ibid, p. x.

Ibid., p. 348.
Sandeen, The Roots of Fundamentalism, p. 157.
George C. Needham, ed., Prophetic Studies of the International Pro-

phetic Conference, pp. 122-23.

91.

Ibid., p. 107.
