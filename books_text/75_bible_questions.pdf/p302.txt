Idols for Destruction: Christian Faith and Its Con-
frontatlon with American Society by Herbert
Schlossberg. 344 pp., indexed, hb., $14.95; pb.,
$8.95. Thomas Nelson Publishers, Nashville, TN.

All men are religious. If men will not worship and
serve the Creator, said the Apostie Paul, they will wor-
ship and serve the creature, a god of their own imagi-
nation, In idols for Destruction, Herbert Schlossberg
calls our attention to many of the fashionable isms of
our age, pointing out that, as substitutes for the true
God, they constitute idolatry.

“Western society, in turning away from the Christian
taith, has turned to other things. This process is com-
monly called secularization, but that conveys only its
hegative aspect. The word connotes the turning away
from the worship of God while ignoring the fact that
something is being turned fo in its place.” Your high-
est value, Schlossberg says, is your god.

Thus, Eve was the first humanist: she attempted to
gain wisdom apart from God and His law. The crisis of
civilization is rooted in our answer to this most.basic
question, the question originally faced by Adam and
Eve. Who is the Lawgiver: God or man? What shall be
the basis of our personal, social, and cultural exist-
ence: God’s word or man’s?

A recurring theme in Schlossberg’s work is judg-
ment. Civilizations do not just “rise and fall.” The bibli-
cal explanation for the end of societies is that they be-
come judged through rebellion against the law of
God. The answer for the evils afflicting our culture is a
return to the worship of the true Ged and obedience
to His commandments in every area of life.
