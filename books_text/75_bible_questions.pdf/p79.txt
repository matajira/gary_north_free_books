SUPPLEMENT TO PART I: HISTORIC GREEDS 75,

5. The will of man is made perfectly and immutably free
to do good alone in the state of glory only.

Chapter 10—Of Effectual Calling

1. All those whom God hath predestinated unto life, and
those only, he is pleased in his appointed and accepted time
effectually to call by his Word and Spirit, out of that state of
sin and death in which they are by nature, to grace and
salvation by Jesus Christ; enlightening their minds spiritually
and savingly to understand the things of Gad, taking away
their heart of stone, and giving unto them an heart of flesh;
renewing their wills, and by his almighty power determining
them to-that which is goad; and effectually drawing them to
Jesus Christ; yet so, as they come most freely, being made
willing by his grace.

2. This effectual call is of God's free and special grace
afone, not from any thing at ail foreseen in man, who is alto-
gether passive therein, until being quickened and renewed
by the Holy Spirit he is thereby enabled to answer this call,
and to embrace the grace offered and conveyed in it.

3. Elect infants dying in infancy, are regenerated and saved
by Christ, who worketh when, and where, and how he
pleaseth: so aiso are all other elect persons who are in-
capable of being outwardly called by the ministry of the
Word,

4. Others not elected, although they may be called by the
ministry of the Word, and may have some common opera-
tions of the Spirit, yet not being effectually drawn by the
Father, they neither do nor can come unto Christ, and there-
fore cannot be saved: much less can men not professing the
Christian religion, be saved in any other way whatsoever, be
they never so diligent to frame their lives according to the
light of nature, and the law of that religion they do profess:
and to assert and maintain that they may, is very pernicious,
and to be detested.

The Savoy Confession of Faith, 1658
