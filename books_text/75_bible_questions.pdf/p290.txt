Dedication and Leadership: Learning from the
Communists by Douglas Hyde. Pb., $3.95. Univer-
sity of Notre Dame Press, Notre Dame, IN.

“What distinguishes the Communist movement
from most others and makes it possible for so smalt a
minority to make so great an impact upon our time is
the dedication of the average individual member and
the immense and dynamic force this represents when
all those individuals collectively make their contribu-
tion to the cause.”

Douglas Hyde was a Communist for 20 years and
served as news editor of the London Daily Worker until
he became a Christian. In this challenging work, he
describes the Communist methods of psychological
motivation and techniques of instilling dedication. His
thesis, is simple and direct: Communist training
changes lives. The task of this book is to discover the
feasons why, and to isolate and investigate those
methods which can and should be used by Christians
to create commitment among followers, and to trans-
form followers into inspiring leaders.

A major reason for Communist success is that
“their aim is quite clear. They have never concealed it
and it is something that is enormously meaningful to
every Gommunist. It is a Communist world. .. . never
in man’s history has a small group of people set out to
win a world and achieved it in less time.”

Christians must be driven by the same vision of vic-
tory, of all-embracing world conquest—especially. be-
cause the Christian hope is founded upon the un-
shakeable promises of God. For too lang the principles
cited here, which rightfully belong to us, have been
used by the enemy. it is time to take them back.
