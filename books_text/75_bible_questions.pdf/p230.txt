226 75 BIBLE QUESTIONS

is service. But with service comes responsibility, the
Bible teaches, and with responsibility always comes
authority. Each member's goal should be to set up his
own group. Like a family, you’re a member of the
Parents’ household, but you should aim at setting up
a household.of your own. There is an analogy (though
hardly an identity) between these Bible studies and
families. You could also compare the Bible studies
with missionary societies.

Each member meets one hour a week with the arigi-
nal group. Then he meets one hour with his own
group. He can pass on information from the older
group to the newer, and also from the newer to the
older. You can see how this structure establishes two-
way communication and therefore two-way educa-
tion. The division of labor principle (I Corinthians 12:
Many members but one body) begins to work for
everyone associated with the Bible study program.

Understand that these meetings are informal, un-
Official, and only for Bible study and prayer. They are
not action meetings, or protest meetings, or “anything
else” meetings. | recommend against setting up sec-
ret societies with rituals, initiations, or anything other
than Bible study and prayer. You're not to take orders
from any Bible study leader, other than the order to
leave his living quarters. You're not to issue orders to
anyone, except for asking someone to leave your liv-
ing quarters.

If such meetings are declared illegal by campus
officials, then have them anyway. They are not allowed
by God to bind your conscience, and they are not
allowed by the U.S. Supreme Court to interfere with
your right to free speech in your own living quarters.
They could be sued successfully in any court in the
land for such a prohibition, and they know it. Any at-
