Questionable Answer

“Jesus died to save souls. He did not die to save civili-
zations, or to enable His people to build one. The king-
dom of God is internal, not external. Its all a Christian
can do to get his own life in order, tet alone the world.
Se that's what we should do: /et alone the worid,

My Reply: When the Bible says that Christ is the
savior of ‘all men, especially of those that believe, it
can mean one of two things. First, Christ saves all
men from hell, but gives special blessings to those
who accepted Christ during their lives. Second, it
means that there are two kinds of salvation, one
which heals all men’s earthly environment (by God's
refraining from destroying everything in His wrath),
and another which heals some men’s souls. If we take
the first view, we cannot make sense of Christ's
teachings concerning eternal judgment. This leaves
us only the second view.

Why do we believe that there can be progress, in
time and on earth, before the final judgment? Not
because Christ is coming back physically’ to rapture
His saints, and then to set up a millennial kingdom.
There will be a rapture, but it will take place on the day
of final judgment (I Corinthians 15:23-24), We believe in
the progressive healing of the world because Christ
died for this world. His death was not in vain.

lf Christ died to save the souls of aff men, yet many

perish, then His death was partially in. vain. But He
didn't die to save the sous of all men.

For further study: Num. 14:21; Ps. 22:27-31; 72:19; Isa. 11:9:
Zech. 14:9; Heb. 8:11.

 

194
