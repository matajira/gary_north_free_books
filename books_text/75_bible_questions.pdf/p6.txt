2 75 BIBLE QUESTIONS

economics, sociology, mathematics, and philosophy.
The book also introduces Christian students to a con-
sistently Christian set of alternatives. The fact is, the
presuppositions of aff the academic disciplines, as
taught in secular universities, are hostile to what the
Bible teaches about God, man, and law. (You can
order a copy of this book from Thoburn Press, P.O.
Box 6941, Tyler, TX 75711; $7.50. If you're a coltege
student, you need this book.) .

In certain fields, such as psychology, anthropology,
and sociology, professors who hold the Ph.D. are vir-
tually always self-conscious humanists and should be
regarded by their students as theologically suspect
until they can. prove their innocence by affirming a
biblical view of sin, redemption, and restoration rather
than a Freudian or clinical view of sin. Anyone who
doubts that Freud was totally incorrect, and ‘self-
consciously hostile to Christianity, from start to finish,
should read AR. J. Rushdoony’s short book, Freud,
published by Presbyterian & Reformed Publishing
Co., which aiso publishes Jay E. Adams’ excellent
books on counselling, especially Competent to Coun-
sel. | would say that any instructor who is forthrightly
hostile to Adams’ “nouthetic counselling” techniques
should also be regarded as guilty until proven inno-
cent. (Note: you can get most of the in-print titles
mentioned in this book at a substantia! discount by
joining the Puritan Reformed Book Club for $5/year:
1319 Newport Gap Pike, Wilmington, DE 19804.)

Nevertheless, the sad fact is that many, if not most,
of the instructors on Christian campuses share several
of the basic philosophical and theological premises of
humanism even though they are not self-conscious
humanists themselves. They have been taught cer-
tain assumptions about the Bible and Christianity that
