Can Satan Repent
and be Saved?

Then shail he say also unto them on the feft hand,
Depart from me, ye cursed, into everlasting fire, pre-
pared for the devil and his angels (Matthew 25:41).

ts it possible that demons condemned to heil will
ever receive eternal life? No. The contents of hell will
be dumped into the lake of fire on the day of judg-
ment. “And death and hell were cast into the lake of
fire. This is the second death. And whosoever was
hot found written in the book of life was cast into
the lake of fire” (Revelation 20:14-15).

If hell was fong ago prepared to hold Satan and his an-
gels, then how can they escape? It would be like saying
that God's kingdom is equaily uncertain, even though it,
too, ‘was prepared by God: “Then shall the King say
unto them on his right hand, Come, ye blessed of my
Father, inherit the kingdom prepared for you from
the foundation of the world” (Matthew 25:34), Each
place was prepared by God for specific creatures.

isn’t Gatan a living creature? Can't he feel pain?
{sn't hell an eternal horror for Satan and his demonic
angels? Why does God condemn them inescapably
to eternal torment? If we acknowledge that Satan's
doom is absolutely sure, then we are saying that
Satan has no “chance” of being saved. And if we admit
that he and his angelic followers are inescapably
doomed, why is it so shocking to say that his human
followers are equally inescapably doomed? Is God
being unfair to Satan and his followers?

47
