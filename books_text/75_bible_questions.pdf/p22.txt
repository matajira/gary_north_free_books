Questionable Answer

“God really didn’t condemn Esau before either of the
sons was born. He condemned Esau because He
foresaw the evil deeds that Esau would do later on. So
God isn't unfair, Esau fell by his own evil deeds.”

My Reply: But the text is specific: God did con-
demn Esau before he had done anything evil. The
text does not mention Esau’'s future deeds. The point
is: all men are condemned automatically before they
are born. That is what the doctrine of original sin
means. Esau was a-son of rebellious Adam, just tike
all the rest of us, and was therefore innately evil and
hated by God, just like all the rest of us, before he had
done anything moral or immoral. The amazing fact is
not that God hated Esau. The amazing fact is that God
foved Jacob!

Didn't God tell Rebekah that the elder would serve
the younger? Could Esau have lived a good life and
have reversed that promise? Isaac thought so, and he
was ready to give the blessing to Esau (Genesis 27:4).
But Isaac was wrong; God made it possible for Jacob
to receive the blessing despite his father’s act of re-
bellion (Genesis 27:6-29).

Was Esau helpless? Yes. Was it inevitable that
Jacob get the blessing? That is what God told
Rebekah (Genesis 25:23). Could she rely on God’s
promise? Absolutely. Did Esau have a chance of over-
coming God's promise? Not a chance. Question: is
there any meaning to the word “chance” in Goo’s plan?

For further study: Ps. 5:5; 11:6; Rom. 11:7-10; Eph. 2:3; Heb.
12:16-17.

18.
