Question 72

Didn’t the Prophets Forsee
the Church Age?

Yea, and all the prophets from Samuel and those
that follow after, as many as have spaken, have
likewise foretold of these days (Acts 3:24).

Peter was quite specific: aif the prophets from
Samuel onward foretold the church age. This means
that God gave revelation to Old Testament believers
concerning the great era of the gospel of Jesus
Christ. Peter, in the very inauguration of the church,
referred back to these: Old Testament prophecies.

We are told by dispensationaiists that the church age
was a "great parenthesis” for the Old Testament proph-
ets, that they knew nothing about it. Yet here is Peter tel-
ling his Hebrew listeners precisely the opposite. In this
Passage, he was condemning them for their murder of
the Lord. He was telling them that they were morally
responsible for having failed to recognize in the
ministry of Jesus the fulfilment of prophecy. Notice also
that he referred to “these days” as the time in which
the Old Testament prophecies have been fulfilled.

The doctrine of the “great parenthesis’—that our
era, the church age, had not been planned from the
beginning by God and even foretold by God through
His prophets—is a false doctrine. Yet this is perhaps
the most important of all doctrines in the dispensa-
tionalist system, How, then, can the traditional dispen-
sational system be maintained? Let us have hope that
the wonderful prophecies of future victory on earth will
be fulfilled, as promised, in the church age!”

187
