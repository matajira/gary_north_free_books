ue ston 54

How Can Christians
be Resurrected Before
the Millennium?

He said unte them, An enemy hath done this. The
servants said unto him, Wit thou then that we go
and gather them up? But he said, Nay; lest while ye
gather up the tares, ye root up also the wheat with
them. Let both grow together until the harvest: and
in the time of harvest | will say to the reapers, Gather
ye together first the tares, and bind them in bundles
to burn them: but gather the wheat into my barn
(Matthew 13:28-30).

Jesus’ parable of the wheat and the tares shows us
what we can expect in history. Jesus explained that
the devil sowed the tareés in the field, which is the
world. The harvest is tha end of the world. The reap-
ers are the angels (13:38-39). Thus, there is no break
in history—no gathering of believers to God—before
the end of the world. There will be no “secret rapture”
which takes place at the beginning of the millennium,
or seven years before the millennium. There is no dis-
contiquous event between now and the second com-
ing of Christ in final judgment. The tares, meaning
evil people, are gathered out of the field (the world)
first, not the Christians.

There is growth unto destruction (tares) and growth
unto final victory (wheat). There is maturity in both
ethical camps. Neither side is cut out (“cuts out”) of
history before the end of time. There is continuity be-
tween our works now and a millennial age.

151
