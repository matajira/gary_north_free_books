SUPPLEMENT TO PART I: HISTORIC CREEDS 81

sin always issues forth from this woeful source, as water
from a fountain; notwithstanding it is not imputed to the chil-
dien of God unto.céndemnation, but by his grace-and mercy
is forgiven them. Not that they should rest securely in sin,
but that a sense of this corruption should make believers
often to sigh, desiring to be delivered from this body of
death. Wherefore we reject the error of the Pelagians; wha
assert that sin proceeds only from imitation.

XVI. Of Eterna! Election

We believe that all the posterity of Adam being thus fallen
into perdition and ruin, by the sin of our first parents, God
then did manifest himself such as he is; that is to say,:mer-
ciful and just: Merciful, since he delivers and preserves from
this perdition alt, whom.he, in his eternal and unchangeable
counsel of mere goodness, hath elected in Christ Jesus our
Lord, without any respect to their works: Just, in-leaving
others in the fall and perdition wherein they have involved
themselves:

The Beigic Confession, 1561
