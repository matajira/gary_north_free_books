Appendix A
How to Get
Your Answers

This section is aimed at college students who at-
tend a Christian college. It could also apply to high
school students. To a lesser degree, it can apply to
people in a church Bible study, but as you will see,
there are differences between a Bible study and a for-
mal classroom situation, where the rules for discus-
sion are more rigid.

On the assumption that you're a college student,
you should consider the following observations.
You're paying money to get good questions asked
and hard questions answered. By remaining in
school, you're forfeiting the income you might be earn-
ing if you dropped out (which | don’t recommend).
You're in one school and not another. This involves
costs; the other school might be better (I doubt it,
however). One of the benefits that college is supposed
to provide is an atmosphere of learning, which un-
questionably involves getting instructors to help you
answer your questions.

Any attempt on the part of instructors to keep ques-
tions from being raised is highly suspicious. An out-
right ban on Bible questions in a Christian school is
an admission of intellectual bankruptcy on the part of
your college’s administration. They are stealing your
tuition: money unless they admit openly that students
are not entitled to ask fundamental theological and
philosophical questions. But this admission must be
public and in writing for all to see. Anything less is

205
