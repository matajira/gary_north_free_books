 

“Christ possesses ail power as a result of His resurrec-
tion victory over Satan. The church, however, has no
means of appropriating this power in time and on
garth. Christ grants to the church power over the se-
cond death. He granis it the sacraments. But He
reserves power for Himself, which the church will not
be able to use until after the Second Coming.”

 

My Reply: Why did Paul spend so much space dis-
cussing such comprehensive power, and the inheri-
tance, and Christ's down payrient “to us-ward,” if God
has chosen never to grant Christ's people the visible,
earthly power commensurate (though of course not
identical) with the power that He granted to Christ, the
head of the church? Ephesians 1 discusses God's
predestination of every member of His church, the
magnitude of God’s power and wealth, the headship
of Christ over the church, and Christ's comprehensive
dominion over all creation in His perfect humanity (He
enjoyed this authority in His divinity before the resur-
rection). Where is our dominion?

If the church is His body, “the fulness of him that
fllleth all in ail,” why is it so pathetically unfilled? Is
the kingdom forever doomed to be weak in com-
parison to Satan's troops? Why is that majestic power
which was in principle transferred to the church from
the beginning incapable of becoming visible, in time
and on earth? Or have Christians simply ignored
Goo’s law? In short, why do so many Christians refuse
to exercise the power the gospel has provided? Why
are they so faithiess?

 

For further study: | Con 4:20; Il Tim. 1:7; Rev. 1:6; 2:26-28; 5:10.

 

172
