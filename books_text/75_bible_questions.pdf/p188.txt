Questionable Answer

“This prayer was a prayer of saints in the kingdom of
heaven, meaning the Jewish kingdom. Kt is not to be
prayed by church members, who belong to the king-
dom.of God, a completely differant kingdom age.”

My Reply: “Thy kingdom come.” How many
kingdoms? One kingdom. So we are to pray for its full
development historically—the complete manifesta-
tion of God's will, exercised on earth by His peopte.
The prayer is ethical. It refers to /aw, which is God’s
will for those who live on earth.

Nevertheless, God’s kingdom is manifested in
power. We. do not seek power as such; we pray for
God's will to be applied on earth as it is in heaven. But
Christ announced His total power in heaven and on
earth. His power was based on His performance of
the requirements of God's law. In short, He received
total power because He conformed totally to God’s
ethical requirements for mankind. His perfect human-
fty entitled Him to perfect power.

As we conform ourselves progressively to His will,
we also increase our power, for God increases our au-
thority over human affairs. As His people seek God's
will on earth, and as they discipline themselves and
those under their lawful jurisdiction by the categories
of biblical law, we should expect.to see progressive
blessings and progressive responsibility, both internal
and external, personal and social (Deuteronomy
2871-14).

For further study: Dan, 2:34-35, 44-45; 7:13-27; Amos 9:11-13;
Rom. 1111-32,

 

184
