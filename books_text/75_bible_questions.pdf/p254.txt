250 75 BIBLE QUESTIONS

If you have a burning interest to study in some field,
and some other schoo! offers superior training in that
field, then consider transferring. Sadly, this usually
means a move to a secular college or university
where the quality of the instruction in academic sub-
jects is superior. So is the tibrary. But it's probably un-
wise to “cut and run” to another Christian college
campus.

For one thing, you know the ropes better where you
are. You know the weak links in the chain, the chinks
in the opposition’s armor. You can do more good, help
more freshman students, influence more of your
peers, right where you are.

if you have a poor reputation with other students,
then get that taken care of. Don't run. Your reputation
won't precede you, but your personality defects will
accompany you.

Hf you have a reputation as a troublemaker with the
administration, but not with your fellow students,
you're in an ideal position to recruit many followers.
One of your chief goals is this one: recruit younger
students so ihat they can take over the Bible study
after you graduate or are kicked out. You want to raise
up a continuing squad of dedicated Bible students
who will pass out this book, ask more questions, and
generally make life miserable for instructors who
don't have any answers. After all, life should be miser-
able for instructors who evade questions, or who
threaten students with lower grades if they don’t shut
up, or who refuse even to face the implications of the
ideas they are teaching.

Stay put. Recruit more students. Devise new ways
to get Bible studies started. Figure out some new
distribution system for this book. Don’t let them chase
you out if you can avoid it gracefully. Look at it this
