244° 75BIBLE QUESTIONS

dents; Claims They All Used Post Office Boxes for [m-
moral Purposes.” Were they receiving Playboy maga-
zine? Communist Party tracts? No; Bible study mater-
ials. }can see “60 Minutes” doing a real hatchet job on
that administration! Fund-raising might become just a
tad more difficult.

I do not recommend going to the news media to ex-
pose nonsense inside the administration. tt is best not
to have the secular humanists laughing at the absur-
dities that Christians do in the name of Jesus, even if
they are absurd. Eventually, the story may get into the
local press, but you shouldn't be responsible for it.
Pressure them “inside the camp.” You don't really
want the government to get involved, either.

10. What to Do When You're Called In

What if some dean calls you in to ask you if you're
involved in “immoral meetings"? Tell him no, you're
not. Meetings of any sort? “Well, what do you have in
mind?” Bible studies! You can say nothing, or else
you can tell him yes, occasionally you do study your
Bible. Sometimes you ask questions of other stu-
dents. They help out.

“Ah, hat You ask other students, do you? Well, why
don’t you ask your instructors?”

There, friends, is exactly the opening you've been
waiting for. “Gee, Dean Grump, that’s exactly what fd
like to do. Can you send out a letter to ail faculty mem-
bers requiring them to set aside 10 or 15 minutes at
least once a week to take questions from all stu-
dents? Gee, that would be simply swell!” Dean
Grump has just shot a big, fat hole in his foot. He has
opened the door to student questions. Lots and lots of
student questions. Won't the faculty appreciate this!

But what if he wants you to stop asking questions to
