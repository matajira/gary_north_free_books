74 75 BIBLE QUESTIONS

adopted, sanctified and saved, but the elect only.

7. The rest of mankind God was pleased, according to the
unsearchable counsel of his own will, whereby he extendeth
or withholdeth mercy, as he pleaseth, for the glory of his sov-
ereign power over. his creatures, to pass by and to ordain
them to dishonour and wrath for their sin, to the praise of his
glorious justice.

8. The doctrine of this high mystery of predestination is to
be handled with special prudence and care, that men attend-
ing the will of God revealed in his Word, and yielding obe-
dience thereunto, may from the certainty of their effectual
vocation, be assured of their eternal election. So shall this
doctrine afford matter of praise, reverence and admiration of
God, and of humility, diligence, and abundant consolation to
all that sincerely obey the Gospel.

Chapter 9—Of Free Will

1. God hath endued the will of man with that natural lib-
erty and power of acting upon choice that it is neither forced,
nor by any absolute necessity of nature determined to do
geod or evil.

2. Man in his state of innocency had freedom and power
to will and to do that which was good and well-pleasing to
God; but yet mutably, so that he might fall from it.

3. Man by his fail into a state of sin, hath wholly jost all
abitity of will to any spiritual good accompanying saivation;
$0 as a natural man being altogether averse from that good,
and dead in sin, is not able by his own strength to convert
himseif, or to prepare himself thereunto.

4. When God converts a sinner, and translates him into
the state of grace, he freeth him from his natural bondage
under sin, and by his grace alone enables him freely to will
and to do that which is spiritually good; yet so as that, by
reason of his remaining corruption, he doth not perfectly nor
only will that which is good, but doth also will that which is
evil.
