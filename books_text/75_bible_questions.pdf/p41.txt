_____ Question 11

Isn’t Our Heavenly
Inheritance Fully
Guaranteed?

In whom also we have obtained an inheritance, be-
ing predestinated according to the purpose of him
who worketh all things after tha counsel of his own
will (Ephesians 1:11).

Who is “we”? Paul was writing to someone. He said
that “we have obtained an inheritance.” He included
himself among that select group. Therefore, it must
have been a group made up of specific individuals,
namely, Christians who believed Paul's message con-
cerning God's salvation of His people.

He says explicitly, “we have obtained an inher-
itance.” The inheritance has already been set aside.
True, God has not delivered it. He has made a down
payment on it, however. In Paul's language, God has
offered us an earnest. He says to the readers to whom
he was writing, “ye were sealed with that holy Spirit
af promise, which is the earnest of our inheritance
until the redemption of the purchased possession,
unto the praise of his glory” (vv. 13b, 14).

Does the Holy Spirit of promise lie to us? Does he
offer us (including Paul) a down payment on spiritual
blessings that He may not be able to deliver, because
“we” later depart from the family into which we have
been adopted? Can we trust Him or not? If not, why
does Paul call Him the Spirit of promise? Why nat “the
Spirit of Possibilities’? Is God's promise a “maybe”? Is
the Holy Spirit a “Spirit of Maybe”?

37
