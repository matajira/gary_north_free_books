SUPPLEMENT TO PARTI: HISTORIC.CREEDS 73

Congregationalist

Chapter 3— Of Goo's Etemai Decree

1. God from all eternity did by the most wise and holy
counsel of his own will, freely and unchangeably ordain
‘whatsoever comes to pass: yet so, as thereby neither is God
the author of sin, nor is violence offered to the will of the
creatures, nor is the liberty or contingency of second causes
taken away, but rather established.

2. Although God knows whatsoever may or can come to
pass upon all supposed conditions, yet hath he not decreed
any thing, because he foresaw it as future, or as that which
would come to pass upon such conditions.

3. By the decree of God for the manifestation of his glory,
some men and angels are predestinated unto evertasting
life, and others fore-ordained to everlasting death.

4. These angels and men thus predestinated, and fore-
ordained,-are particularly and unchangeably designed, and
their number is so certain and. definite, that it cannot be
either increased or diminished.

5. Those of mankind that are predestinated unto life,
God, before the foundation of the world was laid, according
to his eternal and immutable purpose, and the secret coun-
sel and good pleasure of his will, hath chosen in Christ unto
everlasting glory, out of his mere free grace and love, with-
out any foresight of faith or good works, or perseverance in
either of ther, or any other thing in the creature, as condi-
tions or causes moving him thereunto, and all to the praise
of his glorious grace.

6. As God hath appointed the elect unto glory, so hath he
by the eternal and most free purpose of his will fore-
ordained all the means thereunto. Wherefore they who are
elected, being fallen in Adam, are redeemed by Christ, are
effectually called unto faith in Christ by the Spirit working in
due season, are justified, adopted, sanctified, and kept by
his power, through faith, unto salvation. Neither are any
other redeemed by Christ. or effectuaily called, justified,
