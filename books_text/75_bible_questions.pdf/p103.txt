ne stion 31

How Can We
Accurately Define Sin if
We Deny God’s Law?

 

Whosoever committeth sin transgresseth also the
law: for sin is the transgression of the law (| John 3:4).

Itis unlikely that any Christian wants to argue that the
New Testament writers wanted to promote sin. Through-
out the Bible, the warning against sinful thoughts and
behavior is repeated, from Genesis to Revelation. But
the question inevitably arises: What exactly constitutes
sin? The first epistle of John focuses on this question.
Few sermons are preached from this epistie.

At the center of any biblical discussion about sin is the
issue of biblical Jaw. (As a matter of fact, at the very center
of discussion of the foundations of any civilization is the
question of some form of law.) What is the relationship
between the biblical definition of sin and biblical taw?

Men are to do good works and shun evil works. But
how are Christians supposed to define sin’? How are
they to discover which act or thought is sinful, and
which is good? James said: “Therefore to him that
knoweth to do good, and doeth it not, to him it is
sin’ (James 4:17). But this still doesn’t answer the
question: What is good? By what standard do we de-
termine what is good? ”

What other answer can there be for a Christian?
Our standard is biblical law. Where else can we go to
find a vatid standard? To human logic? To antichris-
tian religions? To our own feelings? Why look any-
where else but in the Old and New Testaments?

99
