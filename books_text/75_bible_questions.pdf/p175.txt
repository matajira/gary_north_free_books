Question 64

Doesn't the New
Testament Teach that
Christians are Powerful?

The eyes of your understanding being enlightened;
that ye may know what is the hope of his calling,
and what the riches of the glory of his inheritance In
the saints, and what is the exceeding greatness of
his power to us-ward who believe, according to the
working of his mighty power (Ephesians 1:18-19).

Christ has delivered power to His church. He has
also granted us a vast inheritance. Why don’t we see
this? Because we are blind to God's word. Christ has
not given_us the full inheritance, for the world is still in
sin, and so are we, but we have been. given a down
payment, what Paul called an “earnest” a few senten-
ces earlier (v. 14). We are to recognize the magnitude
of both His power and our inheritance, in time and on
earth, a8 we progressively work out our salvation
with fear and trembling. How extensive is Christ's
power? God gave power to Him “when he raised him
from the dead, and set him at his own right hand in
the heavenly places, far above all principality, and
power, and might, and dominion, and every name
that is named, not only in this world, but also in that
which is to come: and hath put al! things under his
feet, and gave him to be the head over all things to
the church, which is his body, the fulness of him
that filleth all in all’ (vv. 20-23). If this isn’t com-
prehensive power, what is? As church members, we
are commanded by the Lord of all power, who was vic-
torious at Calvary.

im
