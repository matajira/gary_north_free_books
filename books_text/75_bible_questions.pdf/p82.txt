78 (76 BIBLE QUESTIONS:

i. Although God knows whatsoever may or can.come to
pass upon all supposed conditions; yet hath he not decreed
anything because he foresaw it as future, or as that which
would come to pass upon such conditions.

ill. By the decree of God, for the manifestation of his
glory, some men and angels are predestinaied unto ever-
lasting life, and others foreordained to everlasting death.

WV. These angels and men, thus predestinated and foreor-
dained, are particularly and unchangeably designed; and
their number is so certain and definite, that it cannot be
either increased or diminished. + .

V. Those of mankind that are predestinated unto life,
God, before the foundation of the world was laid, according
to his eternal and immutable purpose, and the secret coun-
sel and good pleasure of his will, hath chosen in Christ unto
everlasting glory, out of his mere free grace and love, with-
out any foresight of faith or good works, or perseverance in
either of them, or-any other thing in the creature, as condi-
tions, or causes moving him thereunto; and all to the praise
of his glorious grace.

Vl. As God hath appointed the elect unto glory, so hath
he, by the eternal and most free purpose of his will, foreor-
dained all the means thereunto. Wherefore they who are
elected being fallen in Adam, are redeemed by Christ; are
effectually called unto faith in Christ by his Spirit working in
due season; are justified, adopted, sanctified, and kept by
his power through faith unto salvation. Neither are any other
redeemed by Christ, effectually called, justified, adopted,
sanctified, and saved, but the elect only.

VIL. The rest of mankind, God was pleased, according to
the unsearchabie counsel of his own will, whereby he ex-
tendeth or withholdeth mercy as he pleaseth, for the glory of
his sovereign power over his creaturés, to pass by, and to or-
dain them to dishonour and wrath for their sin, to the praise
of his glorious justice.

VII The doctrine of this high mystery of predestination is
to be handled with special prudence and care, that men at-
