218 75 BIBLE QUESTIONS

questions in class over the semester; and 2) marry
him when he can afford it—or maybe even sooner.
(Getting married teaches people how to afford things,
or at least do without things happily.)

“Stonewalling”

This word became popular after it was learned that
President Nixon had told his associates to “stonewall”
all questions. You-may see a lot of stonewalling in
your college career.

You ask your question. You get evasion. What
then? Pursue? “Go for the throat’? Quit? Ask the
same question next week? What?

The safest bet is to ask one—repeat, one—ifollow-up
corollary to the original question. You just need “a lit-
tle clarification.” This is always the best tactic if the
question arises naturally in the course of a lecture.
Anyone can use more clarification. After all, a related
question might appear on an exam sometime. Any-
one in class can appreciate the fact that you're having
trouble understanding.

if you get another equally evasive or clearly incor-
rect.answer, drop it. You can quit. Or you can say,
“Well, | just don’t see how that relates to the problem,
but I don’t want to take up any more class time.”
You're being considerate to the other students. You
also have made it appear that Dr. Stonewall is unable.
to handle tough questions. You have made your point.

If you try to pursue it, you will unquestionably alienate
other students. If no one else wants to chime in, drop it.
There is always another class and 74 other questions.

Be careful not to have a two-man team asking too
many questions. The more students who get in-
volved, the better. It's always harder to stonewall half
