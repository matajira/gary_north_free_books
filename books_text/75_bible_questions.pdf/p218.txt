214. 76 BIBLE QUESTIONS

your instructors are unable to provide them, you must
be satisfied in your mind that you have done your best
to get answers.

Here are some of the reasons why some of your
classmates and most of your faculty are disturbed by
these questions:

A new view of God is presented.

A new view of God's authority is presented.

A new view of Goa’s judgment is presented.

A new view of personal responsibility is presented.
A new view of human depravity is presented.

A new view of the future is presented.

This being the case, there are other very disturbing
questions that come to mind:

Why wasn't | toid about this before?

Was someone deliberately concealing these issues?
Is this really what the Bible teaches?

How can | determine what the Bible teaches?
Are there answers to these questions?
Where can | find valid answers?

Am | intellectually honest?

Will] believe the truth or a lie?

What will God do to me if I believe a lie?

How reliable are my present instructors?
What will new beliefs do to my plans?

What will they cost me to adopt?

Am | going to “sell out” the truth?

Can | believe the Bible?

Can | believe my instructors?

These are disturbing questions for students—
perhaps as disturbing as the 75 original questions are
to the faculty. A student who is not disturbed by any of
this book’s 75 questions is either incredibly mature
and well-informed, or over-confident in his ability to
