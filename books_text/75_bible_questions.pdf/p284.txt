280 75 BIBLE QUESTIONS

ers who haven't done their homework. Every worn-out
liberal slogan seems to get picked up by some Chris-
tian group or another. They dust off these dead
slogans and try to sell them to their followers as if they
were “the latest thing.”

What we need is the reconstruction of every area of
life. We don’t need a “venture inward” into the mystical
depths of our unconscious. We need a venture out-
ward in victory. But to begin such a venture, we need
information. We need to know what the Bible teaches
about God, man, and Jaw. We need to know what it
says about human institutions. Then we'll be ready to
take upon ourselves the “kingdom tasks” that have
been assigned to us by God (Genesis 1:26-28).

| suggest that the easiest introduction is my book,
Unconditional Surrender: God’s Program for Victory
(‘Leadership’). | wrote it rapidly—in two weeks—for
the average reader. It’s simple, it's to the point, and it
won't waste your time. If you’ve read the 75 questions
in the book, you’re ready for Unconditional Surrender.
And at $9.95, it won't bankrupt you.

You can order these books individually from Puritan
Reformed Books, or you can order them as sets:
“Dedication” and “Leadership” from Geneva Minis-
tries, PRO. Box 8376, Tyler, Texas 75711. Or you can
pay retail and buy them one at a time from your local
Christian bookstore. Your Christian bookstore
deserves support. But if you want the sets, get the
available discount from Geneva Ministries (see the
first tear-out sheet).
