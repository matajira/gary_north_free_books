APPENDIX C: HELP FROM THE ACCREDITATION ASSOCIATION 265

There are several associations. They are regionally
based. One of them probably is listed in your schools
catalogue. As of July, 1983, these were the associations:

South

Commission on Colleges

Southern Association of Colleges & Schools
795 Peachtree Si., N.E.

Atlanta, GA 30365

The states covered are Alabama, Florida, Georgia,
Kentucky, Louisiana, Mississippi, North Carolina,
South Carolina, Tennessee, Texas, and Virginia.

West

Accrediting Commission for Senior Colleges
and Universities

Western Association of Schools & Colleges

cio Mills College, Box 9990

Oakland, CA 94613

The states covered are California and Hawaii.

Northwest

Commission on Colleges

Northwest Association of Schools & Colleges
3700-B University Way, N.E.

Seattle, WA 98105

The states covered are Alaska, Idaho, Montana,
Nevada, Oregon, Utah, and Washington.

North Central

Commission on Institutions of Higher Education
North Central Association of Schools & Colleges
159 North Dearborn St.

Chicago, iL 60601
