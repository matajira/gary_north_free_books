APPENDIX 8: THE BIBLE STUDY 251

way: you're a bigger problem for them than they are
for you. Stick around. Give them something to think
about.

Then, too, you don’t want to get into the habit of re-
treating under fire. Life is fuil of people who don't play
fair. You might as well start learning how to beat them
without compromising your sense of justice or the offi-
cial rules of the ‘institutional game. You're getting
good practice right where you are. Perhaps in retro-
spect the most important part of your education will
turn out to be the experience you get in defending
your right to ask questions and set up Bible studies,
and the theological insights you receive by studying
on your own or with other students.

12. What to Do if You Get Expelled

If you get expelled as a result of this book, contact
my office. rit put you in contact with someone who has
been through this experience himself, and he can
show you several ways to get back in, or to make the
administration wish they had never expelled you. He
is very creative in this area. He knows exactly what
can be done to create incentives for the administra-
tion to reconsider.

Once the administration officially expels you, the
school becomes highly vulnerable in terms of its ac-
Creditation, its continuing status as a tax-exempt in-
stitution of higher education, and its reputation. Just
send me a transcript of your college grades and a
copy of the letter from the adminisiration informing
you that you have bean expelled. Rest assured, fi
help you to straighten this dut to your satisfaction. \t
will be the most costly expulsion in your school’s
history. They will wish they had never done it. And just
to top it off, | can probably show you how to get a free
