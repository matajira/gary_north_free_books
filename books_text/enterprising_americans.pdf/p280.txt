THE MODERN WORLD OF ENTERPRISE < 255

It was sometimes charged, of course, that the American people
spent too much on wasteful luxuries and an assortment of kitsch,
or junk, but the indictment was scarcely bome out by the statistics.
In 1959, for instance, the after-tax income of the U.S. was $336
billion, three-fourths of which went for food, housing, clothes, and
transportation. The remaining quarter went into savings ($24 bil-
lion), medical expenses ($19 billion), support of private religious
and welfare institutions ($4 billion ), private education (#4 bil-
lion), and “personal” business such as bank charges and interest
on loans ($17 billion )-and $16 billion for movies, sports, read-
ing, gardening, travel, and related hobbies. With expenditures for
education and medicine rising in relation to spending for amuse-
ments, it was apparent that the U.S. economy, even in the midst of
fabulous production, had been a relatively austere one. It might
have done with a little more “waste” and “frivolity.” And Madi-
son Avenue, far from deserving censure for trying to promote
consumption, should have been praised for doing its best under
difficult circumstances.

The sober affluence of the new middle class was in itself a stabiliz-
ing factor in the economy: with 60 million cars on the road it took
a yearly production of six million cars in the early sixties merely
to meet replacement demands. At the same time, of course, Ameri-
can taste was constantly changing, with people buying electric
blankets one year and the next switching to good paperback books
or high-fidelity records or wall-to-wall carpeting. This constant
change in the use of “discretionary income” gave the economy its
unique dynamism, and precluded almost by definition attempts to
control it from a central watchtower-only low-grade economies
can be so directed and this at enormous social cost. The great
virtue of the American market system was that the consumer vote
still had a controlling influence over the flow of demand and to a
significant degree over the flow of profits and hence investment. In
1961 some 450,000 new businesses were born in the hope of
turning a profit by catering to changing consumer taste, while an
almost equal number closed their doors because they had failed
to meet the market test. Such a system might appear less tidy than
the great centrally controlled economies. But the untidiness was
only a surface manifestation hiding an inner order and discipline.
