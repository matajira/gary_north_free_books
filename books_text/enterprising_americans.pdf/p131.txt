106 . THE ENTERPRISING AMERICANS

Thumb engine fame, succeeded in laying a momentarily successful
cable from Newfoundland to Ireland just before the Civil War. A
more durable transatlantic cable came later. At the time of Morse’s
experiment in New York Harbor it was a foregone conclusion that
electrical impulses could be transmitted over wires strung between
cities on land—but the problem of raising the capital to buy the
necessary wire was a formidable one. Moreover, Morse’s original
idea was to put the wire safely in underground ditches, which would
have involved Herculean labor—and to this end he had enlisted
the help of Ezra Cornell, a New York State entrepreneur who had
theories about overcoming some of the expenses of digging by utiliz-
ing an underground wire-laying machine of his own devising.

Tn 1843, Morse got an appropriation from Congress to run a
test line between Washington and Baltimore. Comell, finally reject-
ing ditches as impractical, strung wire from poles and trees, using
broken bottle necks as insulators. When in 1844 the magical words,
“What bath God wrought,” were tapped out in Washington and
picked up in Baltimore, it was obvious to the smarter men of the
1840's that brokers and bankers would soon have a medium which
would permit them to do business almost anywhere within the hour.
And it was also apparent that the railroads would be able to apply
new safety concepts to the dispatching of trains.

Morse and his associates raised private capital to organize the
Magnetic Telegraph Co., which opened a line between New York
and Philadelphia. Soon there were additional stock companies oper-
ating different lines between different cities and speeding news of
the Mexican War to the eastern newspapers. Western Union, or-
ganized in 1856, brought some order out “of a welter of small com-
panies. In 1859, going against the advice of Amos Kendall, the old
political agent of Andrew Jackson who had become his financial
adviser and fellow telegraph capitalist in the 1840’s, Morse acqui-
esced in the formation of the North American Telegraph Associa-
tion, a near monopoly. Morse lived on well into the post-Civil War
period to collect his royalties, a comfortably fixed old man who
had come a long way from the near starvation of his early days as
an unsuccessful painter and penniless inventor.

With the development of the telegraph, which made national
markets and exchanges a reality, the U.S. was approaching the
