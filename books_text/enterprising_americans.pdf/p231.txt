206 . THE ENTERPRISING AMERICANS

and neck with Daimler. Early European car-makers included the
French firm of Panhard and Levassor, who took over German pat-
ents in France. French cars in particular were initially popular with
the rich of Newport, and their import helped domesticate words like
“chauffeur” and “garage,“ “tonneau” and “automobile” “itself in the
American language.

The reputedly idle rich, led by Winy K. Vanderbilt, also financed
automobile racing, which led to improvements in engine compres-
sion and the durability of tires, and they helped organize the first
U.S. automobile show at Madison Square Garden in 1900. Other
things besides a rising “moneyed” and middle class were favorable
as the century opened. The gasoline engine was to prove a compact
and efficient power plant, which soon dominated the field despite
the early vogue of electrically propelled vehicles and the formidable
Stanley and White and Winton “steamers.” Gasoline itself, for years
an unused by-product of kerosene, became cheap and plentiful.
Carriage-makers like Studebaker and the Fisher brothers and Billy
Durant’s own Durant-Dort of Flint readily reapplied their skills in
the new horseless age. Perhaps most important, America was blessed
with an inspired generation of mechanics who had put some 12 mil-
lion bicycles on the road between 1880 and 1900. It was, in fact,
two of these bicycle mechanics, Orville and Wilbur Wright, who
showed the world in 1903 that a gasoline engine could lift a “flying
machine” into the air. Taking off on December 17 at Kitty Hawk
on the dunes of the North Carolina coast, with Orville Wright at
the controls, the pioneer Wright machine actually flew under its
own power for twelve seconds. Later in the day Wilbur Wright flew
for almost a second longer; and before the day was done the plane
managed to stay aloft on a fourth flight for fifty-nine seconds under
Wilbur’s control before careening to the ground and breaking the
front rudder. These flights went virtually unreported at the time
(only three papers in the United States deigned to mention them
the next moming, and the Wright brothers’ own home-town paper
in Dayton, Ohio, the Journal, refused to give the story any space at
all). Nevertheless, the power of the intemal combustion engine had
been demonstrated under the most exacting conditions.

But if the times were propitious for technological change in
transportation, there remained the disconcerting fact that the U.S.,
