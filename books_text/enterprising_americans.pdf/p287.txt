262. THE ENTERPRISING AMERICANS

With the changing urban-rural balance, whole industries have
come into being to take advantage of the new suburbias. The
shopping center has become omnipresent; Houston’s Galleria, for
example, has not only taken retail trade away from “downtown,”
it has also provided a new cultural and recreational base. The
largest Sears Roebuck and J. C. Penney stores are to be found in
Woodfield Mall, twenty-five miles northwest of Chicago. U. S.
Gypsum, for example, has promoted nine shopping centers; the
Arlen Realty and Development Company promotes the Korvette
discount chain, and new linear “downtowns” along the beltways
around big metropolises force the building of new peripheral roads
designed to by-pass the older by-passes. Meanwhile, the Holiday
Inn, far from “downtown,” becomes the center for business meet-
ings, and industrial as well as scholarly seminars are held in rural
retreats in the Poconos of Pennsylvania or the foothills of Virginia’s
Blue Ridge country.

With the coming of the computer and its memory banks the
“knowledge industry” and the communications industry have taken
on new dimensions. Xerox, with its 4000 copier, pushed its astound-
ing profits to new highs in 1973. With all the difficulties taken out of
copying, the Authors Guild took an understandable fright that
piecemeal pirating of authors’ material might, at some future time,
kill the reprint business. Along with Xerox, the Eastman Kodak
Company and National Cash Register have become interested in
the possibilities of allying microfilm to retrieval systems. An entire
journal might be mailed on microfilm at the cost of a single first
class letter. In the newer world the newspaper and magazine busi-
ness would assuredly suffer, but the newest printing processes are
enabling newspapers to by-pass the old-style composing room; and
the day may come when printers will be compelled to become paste-
up artists as the linotype machine becomes an antique.

At the beginning of the seventies, R. and D. was running at $27
billion a year. But it had suddenly hit a series of dry wells: where
were the 1970 equivalents of the jet aircraft, the computer or the
TV to float new “ladder industries”? Cable TV promised a profit-
able refinement of an established business, as did the communica-
tions satellites that were being pushed into orbit to “bounce” news
and entertainment across the hemispheres. But the changes in com-
