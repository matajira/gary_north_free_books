FREE ENTERPRISES BEFORE THE REVOLUTION - 17

Tn South Carolina the profitable growing of indigo after 1742
brought profits of 33 to 50 per cent and led to the colonization of
the upland interior. Rice, the other staple of the region, became an-
other source of profit when a relaxation of trade restrictions after
1730 permitted the merchants of Charles Town to export some of
it directly to southern Europe-or “south of Cape Finisterre,” as
the charitable exemption read. And in all the southern colonies
the English Navy provided a ready market for naval stores (pro-
duced by “tar burners”) at times when the Baltic had been closed
as a prime source.

With the British trade restrictions resting lightly upon them, the
merchants of Charles Town had less scrabbling to do than their
brothers of the North. There was, to be sure, a stigma attached to
“trade” in a country where good livings were to be had from slave-
manned plantations—and the Draytons and the Middletons and
their descendants lived well on their Cooper and Ashley River
showplaces without demeaning themselves by entering the counting-
house. But plantations were—or should have been—business en-
terprises in and by themselves. Though plantation owners frequently
lived beyond their means, the injunction to industry was taken
seriously even by colonial plantation women: it was the daughter
of a British Army officer, Miss Eliza Lucas (later Mrs. Charles
Pinckney), who first experimented with indigo when her family left
her alone in charge of the ancestral acres a few miles west of
Charles Town. In years to come, as Mrs. Pinckney, Eliza was inter-
rupted in her very knowledgeable pleasure-gardening by messages
from her overseer complaining about the indigo (it would not get
dry), the rice (it needed water), the barn (the Negro carpenter
was busy making barrels ), the indigo ladles (they were too short),
the chickens (they were being eaten by wildcats and foxes), and
the boat (it had not come upriver for the tar). These were details
of an exacting business that needed profits if slaves were to be

imported from Africa and if the lean seasons were to be endured.

To service the growing Carolina economy, at a time when
planters expected to double their capital every three years, a genera-
tion of canny French Huguenots, sons and grandsons of refugees,
Jed in making Charles Town the first port of consequence in the
southern colonies. Here the Manigaults and de Saussures outpaced
the English and Scotch-Irish Gadsdens and Rutledges as money-
