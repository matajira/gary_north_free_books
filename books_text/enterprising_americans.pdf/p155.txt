130 . THE ENTERPRISING AMERICANS

state-chartered bank-note issues, to create a real national banking
system—and this was not finally achieved until 1866, too late to
help Chase in his predicament.

The cost of the war and its immediate aftermath came to some
# billion by 1869, not including interest on the debt and pensions.
To meet the bill the government turned hopefully to taxation. The
Merrill Tariff of 1861 lifted average rates to 25 per cent (increased
to 47 per cent by 1864); a tax on incomes was levied at 3 per cent
in 1861 and raised to 10 per cent on incomes over $5,000 at the
war’s end; and a tax on sales of industrial products in all stages
of manufacture was instituted at rates running up to 6 per cent of
value. The tariff proved disappointing (its tendency was to shut
out goods completely); the income tax was to yield only $347 mil-
lion before it was dropped in 1872; and the industrial-sales levy
was minimized by businessmen by the simple device of putting
separate manufacturing processes (spinning, weaving, dyeing, and
so on) under one corporate roof, which left only a final product
to be taxed.

Unable to pay for the war out of taxation and borrowing at the ,
banks, the government resorted to two other expedients. The first
was the issue of some $400 million in greenbacks—federal “wild-
cat” money, which had only the printing press behind it; the second
was the sale of government bonds directly to the people-the “five-’
twenties, “ “ten-forties,” and so on. The greenbacks stimulated trade
at the cost of inflation, and their value fluctuated in accordance with
the progress of the military campaigns (a greenback dollar was
worth as little as 39 cents in gold after the failure of Grant’s drive
on Richmond in 1864). Eventually the greenbacks were to pay
out at 100 cents on the gold dollar with the resumption of specie
payments in 1879.

As for the bond issues, the government hired a handsome, bearded
man, Jay Cooke of Philadelphia, to put them over in two great
Joan drives. Just looking at him people believed in Cooke; they
willingly parted with their greenbacks for bonds (thus counter-
acting some of the inflation), and they dug deep into hidden
stocks of hard coin to help the government in its extremity. A
person of infinite resource, Cooke reached into every nook and
cranny of the North and West to uncover buyers. His sub-agents
