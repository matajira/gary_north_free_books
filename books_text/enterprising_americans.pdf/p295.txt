270 . BIBLIOGRAPHY

VL. TRUSTS, THE GROWTH OF BIG ENTERPRISE, AND THE RISE
OF THE NEW YORK MONEY MARKET

Allen, Frederick Lewis: The Great Pierpont Morgan (1948); Allen, Wil-
liam H.: Rockefeller: Giant, Dwarf, Symbol (1936); Appel, Joseph A.:
The Business Biography of John Wanamaker (1930); Baruch, Bernard: My
Own Story (vol. 1, 1957); Beard, Miriam: A History of the Business Man
(4938); Bogue, Allan G.: Money at Interest. The Farm Mortgage on the
Middle Border (1955 ), Business History Review-Staff of eds.: Oil's First
Century ( 1960); Corey, Lewis: The House of Morgan ( 1930); Diamond,
Sigmund: The Reputation of the American Business Man ( 1955); Ecken-
rode, H, J., and Edmonds, P, W.: ‘E. H, Harriman (1933); Flynn, John T.;:
God’s Gold ( 1932); Henry, Robert Selph: This Fascinating Railroad Busi-
ness ( 1942); Hidy, Ralph, and Hidy, Muriel: Pioneering in Big Business
(vol. 1, 1955); Hoibrook, Stewart H.: The Age of the Moguls (1953); Hol-
brook, Stewart H.: James J. Hill (1955); Holbrook, Stewart H.: The Story
of American Railroads ( 1947); Hoyt, Edwin P.: The Vanderbilts and Their
Fortunes ( 1962); Josephson, Matthew: The Politicos 1865-2896 (1938);
Josephson, Matthew: The Robber Barons (1934); Kennan, George: E. H.
Harriman, A Biography ( 1922); Lloyd, Henry Demarest: Wealth Against
Commonwealth (1894); Medbery, J. K,: Men and Mysteries of Wall Street
( 1870); Minnigerode, Meade: Certain Rich Men (1927); Moody, John:
The Masters of Capital (Chronicles of America series, 1919); Moody, Jol
The Railroad Builders (Chronicles of America series, 1921); Moody, John:
The Trath About the Trusts (1904); Myers, Gustavus: History of the Great
American Fortunes (1936); Nevins, Allan: John D, Rockefeller: A Biog-
raphy (2 vols., 1940); Redlich, Fritz: The Molding of American Banking—
Men and Ideas ( 1951); Satterlee, Herbert L.: J. Pierpont Morgan ( 1939);
Tarbell, Ida M.: History of the Standard Oil Company (1904); Twain,
Mark, and Warner, Charles Dudley: The Gilded Age ( 1873); Winkler,
John K.: The First Billion: The Stillmans and the National City Bank
(1934); Winkler, John K.: John D, Rockefeller, a Portrait in Oils (1929);
Winkler, John K.: Morgan the Magnificent ( 1930); Winkler John K.:
Tobacco Tycoon, The Story of James Buchanan Duke ( 1942).

See also general sources indicated for this section.

  
 

VIL. THE MAKING OF THE CITIES: THE COMING OF ELECTRIC-
ITY, THE TELEPHONE, NEW TRANSPORTATION

Barnard, Harry: Independent Man; the Life of Senator James Couzens
( 1958); Berth, Christy: The Automobile: Power Plant and Transportation
of @ Free People (1952); Berth, Christy: Wheels of Freedom (1960); Boyd,
T. A. Professional Amateur: The Biography of Charles F. Kettering ( 1957);
Casson, H, N.: History of the Telephone ( 1910); Costain, Thomas B.:
The Chord of Steel (1960); Danelian, Noobar Retheos: A, T. & T.: the
Story of Industrial Conquest ( 1939); Forbes, C. B.: Men Who Are Making
America (19 17); Ford, Henry, with Crowther, Samuel: My Life and Work
(1922); Fortane magazine: February, 1930; January, 1931; January, April,
May, 1932; December, 1933; November, December, 1934; January, May,
