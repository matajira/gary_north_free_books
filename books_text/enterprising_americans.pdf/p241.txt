216 . THE ENTERPRISING AMERICANS

shares for G.M. stock. Presently he scented some powerful compe-
tition coming out of Wilmington where the du Ponts, who had
profited heavily in 1914 and 1915 by the sale of powder to the
Allies, were putting some capital into G.M. In 1915, Pierre du
Pent became chairman of G.M. and the rule of the bankers was
brought to an end. But actual control of G.M. resided in Durant’s
Chevrolet Motor Co.—a clear case of tail wagging dog. In a fur-
ther reorganization in 1918 a newly incorporated G.M. absorbed
the manufacturing facilities of Chevrolet-and Durant was ready
for a second whirl as an automotive Napoleon.

Fearful of any ride on a Durant rocket, Nash and Leland both
chose to quit, the one going to the newly formed Nash Motor Co.
of Kenosha (successor to the Jeffery Co. and parent of the modem
American Motors Corp. ), and the other to form the Lincoln Motor
Co., which was later bought by Ford. But Durant, recognizing a
good thing in Leland’s friend Kettering and in a Leland protégé
named Alfred P. Sloan of the Hyatt Roller Bearing Co., proceeded
to absorb “Boss Ket’s” Dayton Engineering Laboratories and
Sloan’s own Hyatt into General Motors. Along with Delco and
Hyatt, Durant picked off the New Departure Manufacturing Co.,
maker of ball bearings, and arranged for the purchase of a con-
trolling interest in the Fisher Body Co. of Detroit. He also picked
up the Guardian Refrigerator Co., which was to make the Frigi-
daire. And in 1919 he started the General Motors Acceptance
Corp., which was -to play such a large role in the development of
mass installment selling.

Thus, as the twenties opened, the outlines of a new kind of auto-
mobile company, possessed of a broad line of cars, and with its
fingers in all kinds of subsidiary equipment, were plainly discernible.
Durant himself was not to enjoy the pay-off. Sales of all G.M. cars
slacked off in the recession of 1920-21 and G.M. stock plummeted.
Durant sought to support the market into which he had enticed his
friends by buying heavily on margin, but soon reached the end of
his resources. Fearing that his bankruptcy would hurt G.M., Pierre
du Pent and his lieutenant, John J. Raskob, committed the du Pent
Co. late in 1920 to purchasing Durant’s 2,500,000 shares. With
previous acquisitions, this gave du Pent a 27 per cent interest in
