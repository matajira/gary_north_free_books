232. THE ENTERPRISING AMERICANS

was a denial of the free system, and before it was thrown out by
the Supreme Court its critics were referring to it as “Chamber
of Commerce Fascism.” Its inherent contradictions were later freely
admitted by Administration intellectuals themselves when in Roose-
velt’s second term they set up the Temporary National Economic
Committee to restore competitive pricing, while at the same time
embracing the doctrine of Keynesian spending to restore purchasing
power.

The difficulty with this new palliative was that its success de-
pended uniquely on the restoration of profitability in the system.
The Keynesians remembered that their master had argued against
wage cuts; labor, he said, is seldom in a mood to take a cut-back.
But he had certainly not called for money wage increases in a time
of deflation when real wages were going up every time a retail
price fell. To restore both profitability and purchasing power, the
Keynesian formula called for a turn-about in prices through gov-
ernment spending as existing wage rates were maintained. Per-
versely, however, the American disciples of Keynes paid no
to the role which profitability via rising prices pays in luring
investment money from hiding. They overlooked the fact that money

> wage rates in manufacturing advanced some 43 per cent between
1933 and 1939 and real wages by an extraordinary 34 per cent,
which, on Keynes’s own theory, was detrimental to curing the
surplus of labor. Some of this rise was no doubt to be expected
in a period of partial recovery, but much of it flowed out of
government-blessed wage boosts from an unprecedented surge of
union organization. When NRA was buried, the provisions of its
Section 7a were incorporated into the lopsided Wagner Act,
which gave John L. Lewis, Walter Reuther, and others a free
hunting license to push industrial unionism in the basic mass-pro-
duction industries. In a free system labor has the incontestable
right to organize and to bargain collectively; and it had exercised
this right long before the New Deal. But the very rapidity of the
spread of unionism in the thirties, beyond pushing up costs, was
scarcely conducive to restoring business confidence. And the tactics
of the sitdown strike, however effective in bringing companies like
General Motors to heel, did nothing to encourage private investment
in new industrial plant.

heed
