172, . THE ENTERPRISING AMERICANS

mutual financial ruin of both the Central and the Pennsylvania
roads, with no long-term benefit to shippers or passengers, who
needed solvent railroads to provide them with good service over the
decades.

Even Morgan lacked the financial power to force President
George B. Roberts of the Pennsylvania to make peace. Moreover,
since his own partners, the Drexels of Philadelphia, had helped
finance the Pennsylvania in the past, Morgan had qualms about
trying to impose himself on the Central’s great rival by way of stock
purchases and a vicious proxy struggle. Lacking the power to dic-
tate through money, Morgan turned to moral suasion and finally
managed to arrange a conference on his yacht, the Corsair, Steam-
ing up the Hudson in pleasant circumstances, the compelling “Jupi-
ter” of Wall Street talked the situation out with Roberts and Frank
Thomson of the Pennsylvania, and Chauncey Depew, the witty
president of the Central. By the time the Corsair had returned to
the Jersey City docks Morgan had a promise from Roberts to call
off the war. The Pennsylvania agreed to let the West Shore tracks
go to the Central, and in return Vanderbilt and Depew were to
abandon the half-completed South Pennsylvania project to weeds,
moss, and sumac. Years later, the automobile public was to get
an unlooked-for dividend from the whole business when the South
Pennsylvania’s old embankments and tunnels were utilized by the
new Pennsylvania Turnpike.

The success that Morgan achieved on the deck of the Corsair
advertised him as the appropriate doctor for sick concerns. First
the Philadelphia & Reading, which had a big English stock interest,
came to him; next the Baltimore & Ohio; then the Chesapeake &
Ohio. In the relatively flush days of the 1880’s Morgan was able to”
organize syndicates capable of drumming up enough new capital
to put these ailing roads on their financial feet. When the depres-
sion of the nineties brought more railroads-the Erie, the Northern
Pacific, the Norfolk & Western, and the group of southern railroads
organized under the direction of the Richmond Terminal-to the
edge of collapse, Morgan had the money, the prestige, and the ex-
perience to undertake reorganizations right and left. He put the
Southern Railroad together out of the ruins of the Richmond Ter-
minal properties; he devised a “voting trust” to save the British
