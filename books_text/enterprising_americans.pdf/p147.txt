122 . THE ENTERPRISING AMERICANS

for the year, the banks contained less than $30 million on deposit.
The reason, as historian William E. Dodd explains it, is that south-
ern plantation earnings were eaten up by tariffs, freights, com-
missions, and profits that the planters had to pay on northern
manufactured goods. Moreover, the possession of slaves was in itself
a sink for capital. In the North, where workers owned their own
bodies and took care of their own adolescents and aged, labor actu-
ally cost much less for its maintenance and support—and there was
no need for any deployment of funds to catch a worker in the first
place. The South, putting its money into slaves, never seemed to
have a margin to build the mills and factories that might have saved
the planters from paying commission and freightage and tariff
charges to northern and European exporters. The North, on the
other hand, using its capital for other purposes than buying tool
users, steadily increased its stocks of machinery; it had something
solid, something tangible, to show for its capital expenditures. In-
deed, as early as 1846 Daniel R. Goodlow of North Carolina argued
the case that if southern field hands were hired on a contract basis,
capital would be freed to flow into labor-saving improvements.

But, though Goodlow had his partisans, it became increasingly
unhealthy even to offer a speculative criticism of the Cotton King-
dom’s business system. In a rousing document written and published
in the fifties by a North Carolinian named Hinton Rowan Helper,
the South’s delusion should have been made plain. Helper noted
that the value of northern manufactures was some nine times that
of southern crops of all kinds, and his statistics indicated that the
North had a virtual monopoly on industrial capital because the
South had been tying up its profits in human beings who would
have been there to work the plantation economy whether they were
slave or free. Even the hay crop of the North, so Helper insisted,
was worth more than the whole cotton production of the South.
But far from convincing his fellow Southerners, Helper was re-
garded as a turncoat by them.

So it fell that the South had a wholly false sense of confidence
when the first fateful shots were fired at Fort Sumter. Curiously
enough in retrospect, the North for its part at first underrated its
own powers. In New York, businessmen took seriously the rebel
