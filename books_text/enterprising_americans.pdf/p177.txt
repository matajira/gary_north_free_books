152. THE ENTERPRISING AMERICANS

the western Pennsylvania oil region itself, where he was regarded
as a usurper and an outsider, he found that deviousness was the
only solution to his problem. Deaf to his pleas for “co-operation
and conservation,” the Titusville operators hung him in effigy
and sought to band against him. By 1875, John D. Archbold, a
local refiner who had opposed Standard, had succeeded in putting
some twenty-five of the Titusville independents into a big combina-
tion-—the Acme Oil Co.—which would presumably be able to
compete with Standard on its own terms. The curses and lamenta-
tions in Titusville were loud when it was subsequently disclosed
that Acme had secretly become a Standard Oil subsidiary.

Beyond such manipulations, Rockefeller invoked a still more
dread device for forcing his opponents to join with him “or else.”
This device has gone into the history books as the South Improve-
ment Co. To the end of his life Rockefeller insisted that he did not
start the South Improvement Co. himself. Even so, Rockefeller, his
brother William, Henry Flagler, and others high up in Standard
of Ohio owned 900 of the South Improvement Co.’s 2,000 shares.
The working control of South improvement was theirs.

One South Improvement Co. proposition was to exact rebates
running up to 50 per cent of the carrying charges on all of its oil
transported by the Pennsylvania, the Erie, and the New York
Central. This was quite in line with the conventional railroad
practice of the day. Virtually all manufacturers of the time con-
sidered it quite legitimate to get special discounts for bulk ship-
ments and for a guarantee of a steady flow of business. It was an
“economy of scale.” But the South Improvement contract con-
tained something that went far beyond the rebate. It read: “The
party of the second part [i.e., the railroad] will pay to the party of
the first part [the South Improvement Co.] . . . on ail oil trans-
ported for others, drawbacks.” The word “drawback” signified that
out of the regular freight rates paid by South Improvement’s compet-
itors a fourth to a half would be handed over by the railroad to Rocke-
feller and his mates. Even to the moral code of 1872 this seemed
sheer industrial murder. When the Oil City Derrick printed a list
of the South improvement Co.’s directors under a caption, “Behold
‘the Anaconda’ in all his hideous deformity,” righteous indignation
swept the oil fields and flamed out toward the state capitals of
