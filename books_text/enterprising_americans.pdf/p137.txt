112 . THE ENTERPRISING AMERICANS

the mother country. Later, utilizing new processes for puddling and
rolling invented by Henry Cort in the 1780's, the British mills estab-
lished a long lead time in the unfolding industrial revolution. So
even in the middle of the nineteenth century American ax-makers,
sewing-machine makers, and railroad builders were still dependent
on British sources for good iron.

Nevertheless, the U.S. had the makings and the tradition for cre-
ating an industry. The first iron works, at Falling Creek in Virginia,
was destroyed by Indians in 1622, ending “a good project.” So,
properly speaking, the Iron Age in America goes back to the New
England Puritans, who had their small blast furnaces for the pro-
duction of metal needed in pots, skillets, and andirons. America’s
first important iron master, John Winthrop Jr., the son of the
first governor of the pioneer Massachusetts Bay Colony, had a real
modern enterpriser’s flair; he built an early salt works for evaporat-
ing sea water to obtain the preservative that enabled New England
to establish its first fishing industry. When the swamps in back of
Lynn showed the color of bog iron in the water, Winthrop raised
a thousand pounds in England and returned to Massachusetts with
workmen and equipment to set up blast furnaces and a refinery
forge. The result was the famous Saugus works—and a production
of iron amounting to some eight tons a week by 1648. Later Win-
throp started an iron business in Connecticut, whither he had gone
to become govemor of Massachusetts’ daughter-colony.

The industry created by the junior Winthrop in New England, by
the Dutch in Monmouth County, New Jersey, by Colonel Alexander
Spotswood in Virginia above the falls of the Rappahannock, and
by the remarkable Leonard family at several places, was small-
time stuff; at first, it provided barely enough metal for use in black-
smiths’ shops. Nevertheless, it pointed the way for some ambitious
schemes. Spotswood himself, after resigning as lieutenant governor
of Virginia in 1723, built an early American air furnace near
Fredericksburg, where he used bituminous coal for smelter opera-
tions. In Salisbury, Connecticut, local ores put the Green Mountain.
Boy Ethan Allen temporarily into the iron business—and caused
Litchfield County villagers to dream that their town might some
day become the Birmingham of America. Using Salisbury iron,
Philip Livingston started a forge and foundry business across the
