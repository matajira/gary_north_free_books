THE QUEST FOR CAPITAL . 57

“the realization of a commercial Utopia” even as late as the 1860's,
which was some time after complaints had begun to be uttered
against overcrowding and paternalism. Those complaints were to
increase as the farm girls were replaced by Irish immigrants. But
regardless of arguments over the humanity of employing children
and boarding six young farm women to a room, the young U. S.,
through the remarkable memory feats of Samuel Slater and Fran-
cis Cabot Lowell, had caught up with the English in textiles. By
1834 there were six corporations at Lowell operating nineteen
‘mills with 4,000 looms and more than 100,000 spindles. As V. S.
Clark says in his History of Manufactures in the U.S., the decade
of the 18 30’s in Lowell was the “most remarkable decade of prog-
ress, in a single place and industry, as yet achieved in our manu-
facturing history.”

Meanwhile, other industries were sprouting. Wherever a wheel
could be turned or charcoal could be had to fire a forge, some
Yankee or Pennsylvania German or Welshman was certain to be
at work hammering out machine parts for an entirely new breed of
factory designers. Ironworking itself tended to remain a black-
smith’s craft, though there were indeed some small iron-rolling
mills in America at the beginning of the nineteenth century. The
reason for the continued rule of the blacksmith went back to pre-
revolutionary times, when the British had prohibited the flatten-
ing and forging of iron for tools and hardware. But the art of metal
rolling could hardly be denied for very long to Yankees who were
demanding new sinews for new industry.

Curiously, the first impetus to large-scale metalworking in Amer-
ica came not from the blacksmith’s shop but from people who had
learned their craft in the small world of silversmithing, tinworking,
and the alloying of copper with zinc to make brass. It was a silver-
smith, Paul Revere, famous alike as a “Liberty Boy” patriot and a
fashioner of severely beautiful urns and pitchers, who first had the
idea of rolling copper in sheets that would be big enough to stretch
over roofs and the hulls of ships. Dreaming in his little shop on
Boston’s Charter Street, Revere—then in his sixties-decided that
somebody must provide barnacle-resisting copper bottoms for the
young Republic’s Navy if the frigates were to be kept sufficiently
speedy to catch the Barbary pirates at their insulting work of
