84 . THE ENTERPRISING AMERICANS

But the savagery of the Indians’ subsequent assault upon the Ton-
quin was out of all proportion to the insult; in fact, virtually every-
body on board was murdered by the screaming red men. The ship
itself was presumably blown up by one of its last surviving crew
members, who managed to take a hundred Indians to Kingdom
Come along with himself. Such was the life of trade in the early far
Northwest,

   

Brown Brothers

John Jacob Astor

It was almost as chancy a business when it came to pursuing the
fur trade across the plains. Far from being presented with an easy
opportunity of bribing their way through the country of the Plains
Indians by a judicious expenditure of rum, an Astor expedition had
to reckon with the implacably hostile Sioux and Blackfeet, whose
fur-trading allegiance was to the British companies of Canada. The
cry of “Voila les Sioux!” coming from the throats of Canadian
voyageurs in Mr. Astor’s employ was a cry of abject terror, not a
happy harbinger of greedy concourse in which innocent red men
might be cheated of their winter’s catch by an offer of a few glass
beads. Even to get horses from friendly Indians, the Astor party
had to give good value. It was all gravy to the Indians, who had
stolen the horses in the first place.
