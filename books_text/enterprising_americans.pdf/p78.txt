THE ovestror CAPITAL . 53

producing some two million barrels of flour a year in states west of
the Alleghenies by 1837. Flour milling was our first “automated”
business, the precursor of a hundred others to come.

A second point of expansion was textiles, in which the U.S. at
first lagged behind Britain. Wishing to keep the benefits of the great
textile-manufacturing inventions (Hargreaves’ spinning jenny, Ark-
wnhght’s water-driven spinning frame, Crompton’s “mule,” and
Cartwright’s power loom) to themselves, the British had passed
laws that prohibited the export of machine designs and even the
emigration of any British subject who had had textile-mill experi-
ence. To get hold of the English textile know-how, Pennsylvania
and other states had offered bounties—in reality bribes to English-
men to smuggle out the forbidden techniques. In Rhode Island the
retired Quaker merchant Moses Brown, using the capital he had
garnered in the many Brown ventures from spermaceti candles to
the China trade, had backed the firm of Almy & Brown (formed
by his son-in-law) in the building of several water-powered cotton-
spinning machines. But lacking the precise Arkwright specifica-
tions, the devices broke down continuously and there was never
any profit in them.

At this point a remarkable young Englishman put in his appear-
ance on Moses Brown’s doorstep. He was Samuel Slater, a Derby-
shireman who had spent a six-and-a-half-year apprenticeship in the
Derwent River mill of Jedediah Strutt, a partner of the great Sir
Richard Arkwright himself. Strutt had fancied the young Slater,
and had allowed him to experiment with rearrangements of the
Arkwright equipment. While still in his mid-teens, Slater had added
an important cam device that helped distribute the spun yarn on
the spindle more evenly.’ Working with the Arkwright complica-
tion of breaker cards, lap machines, drawing frames, roving frames,
bobbin wheels, spinning spindles, and winding reels, Slater photo-
graphed the relationships of the equipment in his mind and memo-
tized the key dimensions, and then took off for America dressed
as a farm laborer. Arriving in New York, he fell in with the captain
of a Providence sailing packet, who told him of the difficulties of
Moses Brown. A letter to the Quaker merchant brought Slater a
swift reply: “We should be glad to engage thy care so long as they
[the defective water-frame spinning devices] can be made profitable
