THE CIVIL WAR AND ITS AFTERMATH - 133

subsidy as the essential lever. The Republican party has sometimes
been referred to as the stalwart champion of an absolutely pure
Adam Smith type of individualism. This is wide of the mark. It
can be more logically maintained that the Republicans were the
first to succeed in instituting that canny balance of subsidies that
Henry Clay had praised as the American System in the palmy
days of the Whig party. The old slogan “Vote Yourself a Farm”
was quickly fleshed out by the passage of the Homestead Act.
(So much for the settler in Minnesota and Nebraska. ) The
Merrill Tariff, which was amended upward in 1862 and 1864,
gave manufacturers a protection they had long been seeking, with
special favors being doled out to the masters of iron and steel.
(So much for the Northeast and for the booming centers of coal
and iron production in the valleys of western Pennsylvania and
eastern Ohio. ) As for labor, it figured less extensively in the Repub-
lican “plan”—and many workers were to fear the new proposals
for bringing immigrants from Europe to America under actual
working contract. But the tariff, so it was argued, would enable
manufacturers to pay a high wage; and free lands in the prairie
and plains regions were there to constitute a safety valve for
immigrant pressure on the labor market.

Too much can be made, of course, of this Republican system of
subsidies and tariffs in developing the country. The subsidies to
special interests were, in fact, peanuts when compared to the new
torrents of wealth that non-subsidized individuals were shortly
to bring into being in the postwar years. Yet in one critical area
subsidies were to have far-reaching repercussions—namely, on
railroad building. During the war years the northern railroad
system had served the armies well but had not greatly expanded.
The passage of the Union Pacific Railroad Act in 1862, however,
opened new opportunities for transcontinental lines, which were
presently to link both oceans together even while the eastern-
seaboard states and the Deep South were painfully recovering
from the war itself.

Tn accordance with the railroad legislation, the Union Pacific
was to build westward from a point in Nebraska and the Central
Pacific was to go eastward from California with a guarantee of
$16,000 a mile for plains country, $48,000 for mountain country,
