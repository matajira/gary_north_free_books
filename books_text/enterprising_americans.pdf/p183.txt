158 . THE ENTERPRISING AMERICANS

in 1865. By indirection Carnegie went swiftly to his goal. He set
up an office in New York, not as a steelman but as a bond broker.
He spent much of his time in travel, cultivating literary men,
philosophers, English royalty, and U.S. Senators who were in a
position to grant him a high steel tariff. Somehow, though no one
could quite fathom the process, his control of the various steel
companies in which he was so distantly interested always seemed
to increase.

Indignant historians, put off by Carnegie’s colossal vanity in
taking credit from others, have endlessly reiterated that it was
Andrew Kloman who really built up the firm of Kioman & Phipps
in the Civil War period; that it was William Coleman, the father-
in-law of the younger brother, Thomas M. Carnegie, who first pro-
posed that a Carnegie enterprise—the Edgar Thomson mill—be
set up to make steel rails at Braddock, Pennsylvania, by the new
Bessemer process; that it was Captain Bill Jones, a refugee from
the Cambria Iron Works at Johnstown, who supplied the managerial
ability that made the Edgar Thomson mill so profitable; that it was
the relatively late newcomer Henry Clay Frick, owner of the rich
Connellsville coke fields, who persuaded the Carnegie organization
to become a tight vertical trust, commanding its own sources of
coke and iron ore as well as its mills; that it was the hardboiled
Frick, once again, who took on the unpleasant task of breaking
the hold of the Amalgamated Association of Iron and Steel Workers
at the Homestead mill when Carnegie was hiding in Scotland from
the consequences of having written that the great law of the work-
ingman was “Thou shalt not take thy neighbor’s job.”

All of the allegations that Carnegie grew rich on the labors and
ingenuity of men who knew considerably more about steel than he
did are perfectly true. But if it hadn’t been for Andy Carnegie’s
peculiar character there would have been no glue to hold the whole
vast enterprise together in those years when the Bessemer process
was displacing the old puddling processes only to give way in turn
to the great open-hearth furnaces of the modem mill. In this whole
development Carnegie exhibited a unique talent for moving in and
taking control when the time was ripe. In 1871 he scoffed when
William Coleman and Thomas Carnegie proposed entry into the
Bessemer field. Running off to Europe on a bond-selling expedition
