] ] F. o. B. Detroit

Durant’s folly—the dream of 500,000 cars.

Duryea’s “buggyaut” and the Merry Oldsmobile.

Ford’s Model T rolls and reigns supreme for eighteen years.
The Chevy and Chrysler’s Plymouth move up.

An enduring export: muss production.

Th destined founder of General Motors, William Crapo Durant,
for whom the phrase “live wire” seems specifically to have been in-
vented, was approaching forty when the twentieth century opened.
The grandson of a lumber tycoon who had been the Civil War gov-
ernor of Michigan, Billy Durant made his first million dollars in
the carriage business in Flint. Thoroughly bored with the success
of his Durant-Dort Carriage Co., he went off to New York to study
the stock market, but a hurry call brought him racing back in 1904
to save his home town from a threatened business catastrophe. The
automobile company started by David Dunbar Buick, a plumbing-
supply man, was on the rocks—and Durant, as the wealthiest man
in Flint, was asked to take it over.

Billy Durant knew nothing about automobiles. But he had been
making a study of the trust movement, and he had big ideas: he
wanted to become a Napoleon in some line of work. Getting himself
one of the fifty-three Buicks that had been made in two years of
the company’s existence, he took off through the sand and mud of
Michigan’s cutover forest region to assure himself that he had a
salable product. The Buick was probably no better and no worse
than cars that were already being made in Detroit by Henry Ford,

202
