42, . THE ENTERPRISING AMERICANS

was projected and in time the textile town of Paterson, New
Jersey, was to grow up on the site. The society raised $625,000
and actually built a cotton-print mill. But alas for Hamilton’s
hopes, it could find neither skillful managers nor competent machine
designers and artisans; nor could it lure young men away from
the farms at costs commensurate with the operation. In 1796 the
plant closed down. Hamilton, foreseeing the difficulties of the
society, had resigned from it in 1793. -

The uncomfortable truth was that the U.S. had few resources
on which to erect a “holding company” for “useful manufactures”
in the early 1790’s. What Hamilton only fitfully grasped was that
the manufacturing nation of his dreams had to be born out of
long travail and pain. Although the young nation had a going
money system, it was short in both investment capital and techni-
cal know-how. To be sure, some inspired tinkerers, such as Oliver
Evans, Samuel Slater, and Eli Whitney, were already at work on
inventions that presently would revolutionize business enterprise.
But the fruits of their tinkering lay ahead, and neither Hamilton
nor the other Founding Fathers could hurry things. Their glory
was that they provided some ideas which freed man as a producer—
and the ideas would find their way.
