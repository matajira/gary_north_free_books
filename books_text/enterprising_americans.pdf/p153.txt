128.. rHeENTERFRIsING AMERICANS

Morgan-financed episode were probably as good as any to be had
in the pinch. Though Colt had already popularized his breech-
loading six-cylinder revolver, which was used throughout the war
as a side arm, the War Department refused to sanction the new
Spencer breech-loading “magazine gun” for more than limited use
until the very last year of the war. The hand-cranked Gatling gun,
which could fire hundreds of shots a minute, did not find its way
into battle until 1864. Fortunately for the North, the South was no
more enterprising in its use of the available new ordnance: if Lee
had had a breechioader at Gettysburg, the result might have been
different.

In iron, the standoff in the Bessemer-Kelly patent situation made
it impossible for the northern armies to avail themselves of weapons
made out of steel. To get the secrets of good gun metal, Abram
Hewitt of the Trenton mills was sent to England, where he met
with official rebuffs. But, presumably by good detective work among
Staffordshire workmen who favored the Union cause, Hewitt came
home with the needed knowledge. His company did well enough
by the government throughout the war to enable the northern forces
to win once Lincoln had found some able generals. On one hurry
call from President Lincoln, the Trenton mills, aided by a consider-
able amount of what would now be termed subcontracting, pro-
duced thirty mortar beds-or heavy gun carriages-within three
weeks. The mortar beds served Grant well in his campaign to open
the Mississippi. When the War Department delayed in reimbursing
Hewitt for the expenditure of $21,000 on the mortar contract, Lin-
coln remarked to Secretary of War Stanton, “Do you suppose that
if I should write on that bill, ‘Pay this bill now,’ the Treasury would
make settlement?’ The bill was paid with Lincoln’s request at the
bottom of it.

War production involved the Trenton mills and their subcon-
tractors-E, Abbott & Son of Baltimore, Cornell of New York, the
Phoenix Iron Works of Phoenixville, Pennsylvania—in a produc-
tion battle with the Tredegar Iron Works of Richmond, Virginia,
where a West Pointer; Joseph Reid Anderson, operated locomotive
shops, a cannon foundry, and a rolling mill. It was to protect the
Tredegar Works as much as anything else that Lee and Jackson
based all their strategy on the defense of Richmond. With the use
