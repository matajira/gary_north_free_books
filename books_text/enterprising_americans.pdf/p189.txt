164 . THEenrerrrisinc AMERICANS

around the corner. But even as Hadiey spoke, Finley Peter Dunne’s
“Mr. Dooley” voiced the popular opinion that the king was already
on the throne in all but name. “James,” said Mr. Dooley in his im-
personation of Morgan, “call up the Czar an’ th’ Pope an’ th’ Sultan
an’ th’ Impror Willum, and tell thim we won’t need their sarvices
afther nex’ week.”

John Pierpont Morgan, the conjurer of U.S. Steel, was then sixty-
four years old, and did not have another quarter century to live.
But to the public he had suddenly taken on a mythic quality. Nick-
named “Jupiter” (he was also to be called Pierpontifex Maximus ),
he was a great burly figure with a huge red nose and startling eyes
that nobody ever quite dared to stare down. His growl was like
thunder; his word in his own community was taken to be law. In
the long Indian summer of his later life, when he turned some of
his prodigious energies to collecting, his house at Prince’s Gate in
London and his home and adjacent library at Madison Avenue and
Thirty-sixth Street in New York City became stuffed with a king’s
ransom in paintings, ceramics, glass, textiles, sculptures, illumi-,
nated manuscripts, and first editions. His movements, whether
large or small, had the stateliness of a royal progression: his yacht,
the Corsair, dominated the fleet wherever the New York Yacht
Club anchored; he had a special riverboat constructed to take him
up the Nile; he spent part of each year contemplating eternal ob-
jects in the Eternal City of Rome; and when he attended conven-
tions of his beloved Episcopal Church as a lay delegate (he was
senior warden of St. George’s in Manhattan), it was as if the chief
lord temporal of the realm had decided to hobnob with ecclesiasti-
cal peers among the lords spiritual.

This pomp and circumstance flowed out of the earnings of a
banking business, J. P. Morgan& Co., which, located at the famous
“comer” of Broad and Wall streets in New York, seemed to be the
arbiter of the nation’s economic destiny. For one thing, Morgan
acted as a kind of private Securities and Exchange Commission:
stocks and bonds that he sponsored, he let it be known, could be
trusted. For another thing, he served as a kind of private Federal
Reserve System, even bailing the U.S. Treasury out at a stiff price
when, in the depression year of 1895, it ran short of gold. Finally,
as arbiter of the economic decisions of great railroads, Morgan
