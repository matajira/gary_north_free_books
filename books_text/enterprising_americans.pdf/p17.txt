INTRODUCTION XIX
by John Moody and Burton Hendrick, now sixty years old, are still
useful. But, as Allan Nevins has pointed out in a notable paper on
“Business and the Historian,” the historians have generally regarded
“trade and manufacturing with disdain.” The writers of historical
syntheses have left the subject of business history to those whose
taste runs to test-borings undertaken for depth in deliberately narrow
compass.

There are, of course, the purely economic histories, grounded in
a predominantly statistical treatment of events, of which Harold
Underwood Faulkner’s work stands as an excellent example. There
are pertinent and provoking analyses of economic forces as they
have shaped our history: Louis M. Hacker’s The Triumph of
American Capitalism and The Age of Enterprise, by Thomas C.
Cochran and William Miller, come immediately to mind. There
are studies of institutional changes in American business, some of
them done quite admirably by Professor Cochran. There is a vast
literature of attack, ranging from Theodore Dreiser’s novelistic por-
trayal of a corrupt traction monopolist (patterned on the character
of Charles T. Yerkes ) to the type of study that is exemplified by
Matthew Josephson’s colorful and animated The Robber Barons
which, in its turn, was a more sophisticated ordering of the material
(often gleaned from the prejudiced records of Congressional hear-
ings) that Gustavus Myers used so vitriolically in his History of
the Great American Fortunes. As for business biographies, they
have been as plentiful as Falstaff’s blackberries, but they usually
bear the telltale marks of their times and origins. Biographies writ-
ten in the twenties (or subsidized at a later period in the spirit of
that decade) ordinarily err on the puff side; those that came in
the thirties and forties are more often than not savagely or urbanely
iconoclastic, dealing with alleged “merchants of death,” or depict-
ing J. P. Morgan as a creature of tainted magnificence.

Tn recent years, the biographical mode has taken on a new bal-
ance. Allan Nevins’ individual or co-authored studies of key fig-
ures (Abram Hewitt, the ironmaster; Eli Whitney, the inventor;
John D. Rockefeller; the creators of the Ford Motor Company)
are keen blends of narrative and judicial appraisal; the Hidys,
Ralph W. and Muriel E., have explored the genesis of the Standard
Oil Company without recourse to the indignation of a Henry Dem-
