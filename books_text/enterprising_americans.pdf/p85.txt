60. THE ENTERPRISING AMERICANS

textiles. It also turned the American South to the furious exploita-
tion of cotton, and so probably kept the “peculiar institution” of
slavery from dying a natural death. But Whitney’s towering influ-
ence goes well beyond this. He is a key figure in the development
of jigs and dies and other machine tools for metalworking. And it
was Whitney who started making guns from interchangeable parts
and in the process laid the foundation for what was to become the
“American system” of mass production.

Whitney was born in 1765 on a small farm in Massachusetts’
straggly and sandy Worcester County, which lies between Boston
and the fat lowlands of the Connecticut Valley. Hating farm work
because of the endless and meagerly productive chores, the young
Eli set up a forge in his father’s workshop to make penknife blades,
nails, and small items for personal use, and presently branched out
into making hatpins by drawing steel into fine wire. His youthful
fingers knew from the beginning that they had a vocation, but his
mind, distracted by provincial ideas of success, did not. For several
years Whitney taught school in Massachusetts and Connecticut,
and then entered Yale, thinking to become a lawyer. Running out
of money after his graduation at the advanced age of twenty-seven,
he went south to take on a tutoring job. In Georgia he stopped off
at a plantation run by the capable and cultivated Catherine Greene,
widow of one of Washington’s foremost generals, and heard talk
of the difficulty of separating short-staple cotton from its tenacious
green seed. (A Negro slave could clean only a pound a day. )
Within a few days Whitey’s amazing fingers had devised a rough
mechanism for forcing the cotton through a series of narrow slits,
thus effectively cleaning it. With the gin in mind, Whitney then
picked up a business partner, Phineas Miller, a fellow Yale man
who was manager of the Greene plantation, and the two set out to
make and market the new machine.

Luckily for his own future, Whitney had an endlessly frustrating
experience trying to collect royalties on the use of his patent. Every-
where in the South the gin was pirated as cotton production jumped
from five million pounds to thirty-five million in seven years; and
to pursue the pirates proved more than the effort was worth. The
result was a black period in Whitney’s life. But after months of
terrible strain during which he lost his New Haven machine shop
