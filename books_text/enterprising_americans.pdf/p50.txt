BUSINESSMEN JOIN IN AN UNBUSINESSLIKE WAR - 25

outcome. This does not mean, of course, that the Revolution can
be explained in purely economic terms, as some latter-day historians
have made out. The angry merchants of Boston, like other men
of their times, believed that liberty was all of a piece; though many
of them feared that war might unchain “leveling” passions, life
to the thoughtful colonials was not worth the having without
liberty, and liberty without a share in private property was all but
inconceivable. In taking up the cudgels against the British Crown
in such mundane matters as trade and taxation, the colonists felt
they were defending their very existence as free men.

This whole tradition of liberty was, moreover, based on the
“tights of Englishmen” and inherited from the mother country.
At its inception the Revolution was much more of an internal
quarrel as to how a great commonwealth should be run than it
was a war for “self-determination” in the current usage of that
phrase. The Americans of the eighteenth century were above all
politically mature men—mature in their view of liberty and
knowledgeability of the law, and mature in their capacity to
conduct their businesses in the far places of the earth. If their
English rights had been respected, they would not have rebelled.
But such a people could not lightly be pushed around by a head-
strong King and Parliament removed by three thousand miles
of water. As Edmund Burke reminded Parliament in his great plea
for conciliation: “The ocean remains. You cannot pump it dry.”
To make the relevance of Burke’s warning clear, some under-
standing of British eighteenth-century economic history, both
in its commissions and omissions, is necessary. The shorthand word
for Britain’s pre-Adam Smith economic philosophy, which gov-
erned the course of the history, was Mercantilism. It was a
doctrine that favored the creation of monopolies and the preserva-
tion of colonies as sources of raw materials which were to be
exchanged for home country finished goods or processed com-
modities on terms dictated by the mother nation. Not all raw
materials were treated alike by the mother country, and this
naturally led to a distinction between colonies.

Tea, originating in the British East and carried in British ships,
had one priority. But back of tea there lurked sugar, which had
the first priority of all. In Stuart times sugar had been a luxury;
