What Is the ICE? 423

State on a massive scale. Yet he is a professor at a Baptist seminary.

The ICE rejects the theology of the total State. This is why we
countered the book by Sider when we published David Chilton’s Pro-
ductive Christians in an Age of Guilt-Manipulators (3rd edition, 1985).
Chilton’s book shows that the Bible is the foundation of our eco-
nomic freedom, and that the call for compulsory wealth transfers
and higher taxes on the rich is simply baptized socialism. Socialism is
anti-Christian to the core.

What we find is that laymen in evangelical churches tend to be
more conservative theologically and politically than their pastors.
But this conservatism is a kind of instinctive conservatism. It is not self-
consciously grounded in the Bible. So the laymen are unprepared to
counter the sermons and Sunday School materials that bombard
them week after week.

It is ICE's contention that the only way to turn the tide in this nation is
to capture the minds of the evangelical community, which numbers in the
tens of millions. We have to convince the liberal-leaning evangelicals
of the biblical nature of the free market system. And we have to con-
vince the conservative evangelicals of the same thing, in order to get
them into the social and intellectual battles of our day.

In other words, retreat is not biblical, any more than socialism is.

By What Standard?

We have to ask ourselves this question: “By what standard?” By
what standard do we evaluate the claims of the socialists and in-
terventionists? By what standard do we evaluate the claims of the
secular free market economists who reject socialism? By what stand-
ard are we to construct intellectual alternatives to the humanism of
our day? And by what standard do we criticize the social institutions
of our era?

If we say that the standard is “reason,” we have a problem:
Whose reason? If the economists cannot agree with each other, how
do we decide who is correct? Why hasn’t reason produced agreement
after centuries of debate? We need an alternative.

It is the Bible. The ICE is dedicated to the defense of the Bible’s
reliability, But don’t we face the same problem? Why don’t Chris-
tians agree about what the Bible says concerning economics?

One of the main reasons why they do not agree is that the ques-
tion of biblical economics has not been taken seriously. Christian
scholars have ignored economic theory for generations, This is why
