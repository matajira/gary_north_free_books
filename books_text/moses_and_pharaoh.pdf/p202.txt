184 MOSES AND PHARAOH

Christ is Lord, to the glory of God the Father” (Phil. 2:9-11). In time
and on earth, however, not every knee will bow, and among those
who formally bow themselves before Christ, not every heart will
bow. The principle of unconditional surrender is nonetheless valid
even before the final judgment. It will not be consummaiely extended
in history by fallen men, but as the followers of Christ progressively
conform themselves to Christ’s image, to a greater and greater ex-
tent, the preaching of the gospel and the construction of institutions
based on the full application of biblical law will extend Christ’s
kingdom: progressive unconditional surrender, the working out in
history of the definitive unconditional surrender of Jesus Christ to
God the Father.

Pharaoh's Negotiations

It is part of Satan’s imitation kingdom that he, too, requires un-
conditional surrender. He wants men to bow to him and worship
him, which is what he dernanded of Jesus in the wilderness (Matt.
4:9). Pharaoh seemed to compromise with Moses, but at no stage
was he asking for anything less than unconditional surrender from
God, tor Pharaoh asked God to sanction the idea that Pharaoh had
some trace of divinity in him, that he represented true divinity in the
continuity of being between God and man. God refused to com-
promise, for anything less than total sovereignty on His part is a
denial of who He is and what He is. Satan wants “just a speck” of
sovereignty, so that he can successfully deny that God is who God
says that He is. This is not unconditional surrender to God, for it re-
quires that God deny Himself. In effect, it would be the uncondi-
tional surrender of God to Satan, for by having testificd falsely con-
cerning both Satan and Himself, God would thereby have sinned
against Satan. He would have borne false witness concerning the
nature of divinity and false witness against His maligned (not malig-
nant) neighbor. False testimony concerning the nature of God is a
capital offense. This challenge to the validity of God’s self-testimony
was the very heart of Satan’s temptation of Adam and Eve: an asser-
tion that God had testified falsely concerning God, man, and
reality.>

After Pharach finally allowed the Hebrews to depart in total

5. Gaty North, “Witnesses and Judges,” Biblical Economics Today, VI (Aug./Sept
1983). I intend to reprint this as Appendix E of The Dominion Covenant: Genesis when it
goes into its second edition.
