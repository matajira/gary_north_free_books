Unconditional Surrender 179

Hebrew man, or an Hebrew woman, be sold unto thee, and ‘serve
thee six years; then in the seventh year thou Shalt let him go free
from thee. And when thou sendest him out free from thee, thou shalt
not let him go away empty. Thou shalt furnish him liberally out of
thy flock, and out of thy floor, and out of thy winepress: of that
wherewith the Lorn thy God hath blessed thee thou shalt give unto
him” (Deut. 15:12-14). It is revealing that the justification of this law
was the bondage they had experienced in Egypt: “And thou shalt
remember that thou wast a bondman in the land of Egypt, and the
Lorp thy God redeemed thee: therefore I command thee this thing
today” (Deut. 15:15).

Pharaoh wanted to be recognized as a lawful master, for he was
giving liberally of Egypt’s wealth. Was he not treating the Hebrews
honestly, as a brother might treat them? In fact, it was God’s respon-
sibility to bless him, for he was adhering to the law: “It shall not
seem hard unto thee, when thou sendest him away free from thee:
for he hath been worth a double hired servant to thee, in serving thee
six years: and the Lorp thy God shall bless thee in all that thou
doest” (Deut. 15:18). The blessing, Pharaoh insisted. Where is the
blessing?

Pharaoh the slavemaster, Pharaoh the kidnapper, Pharaoh the
treaty-breaker, Pharaoh the slave-wife-divorcer, and Pharaoh the
divine ruler was once again demanding to be recognized as a sover-
eign. He was arguing that he and his predecessors had possessed the
right to violate all of the laws concerning Jawful servitude, enslaving
the Israelites unlawfully, just as surely as Potiphar threw Joseph into
prison unlawfully. He had conducted himself lawfully, he implied,
and now his payment to the Israelites testified, he wanted God to ad-
mit, to his position as a covenant-kecper. Facing a victorious slave
population and their victorious God, Pharaoh wanted to be justified
by works: his liberality in giving the Hebrews their seventh-year
payment. This was not tribute. This was not restitution, This was
not the restoration of the stolen dowry. This was simply lawful pay-
ment for-lawful slaveowning, which had been conducted by a well-
meaning brother in the faith. He wanted all the promiscd benefits of
the law, the blessing in “all that thou doest,” in return for this final
payment to Israel. If he could get God’s blessing, his payment would
wipe the slate clean. God would be testifying to the legitimacy of
Egypt’s past rule over His people.

Pharaoh was not offering unconditional surrender to God. Once
