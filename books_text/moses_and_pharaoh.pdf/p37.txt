Population Growth: Tool of Dominion 19

Hebrew men would have had to marry Hebrew wives (along with
Egyptian wives) in order for the daughters of all the families to have
remained inside the covenant lines. On the other hand, Egyptian
men might have converted to the faith, especially during the period
of Israel’s preeminence in Egypt (e.g., Lev. 24:10), Even apart from
the assumption of multiple wives (some Egyptian), it is obvious that
long lives, high birth rates, and low death rates could have produced
a huge population within two centuries.

Household Servants

We should also understand that the 70 direct heirs of Jacob de-
scribed in Exodus 1:5 were lineal heirs, “out of the loins of Jacob.” But the
total number of households under each lineal heir would have been far larger. Ser-
vants who were circumcised were part of the families, and they would
have come down to Egypt with the direct lineal heirs. These servants
would have participated in the blessings of Goshen, which was the best
land in Egypt (Gen, 47:6). The Pharaoh of the famine gave his best
land to Joseph’s relatives, but this included their entire households. The
size of the land indicates this: the land needed administration. Phar-
aoh even wanted to place his own cattle under the administration of
“men of activity” among the houscholds (Gen. 47:6b). He expected
them to care for the best land of Egypt (Goshen), but this would
have required more than 70 men and their immediate families.

Therefore, when the households of Israel went into bondage
under a later Pharaoh, the descendants of the servants were counted
as the covanantal heirs of Jacob.!3 They also went into bondage.
When the Exodus from Egypt freed the Israelites, all those who had
been part of the familics of Jacob went free, The multitude that
swarmed out of Egypt included the heirs of the circumcised servants of the
70 lineal heirs of Jacob.

How many people actually came down into Egypt during the
famine? It could have been as many as 10,000. One estimate of
Abraham’s household is 3,000, given his 318 fighting men (Gen.
14:14).'* We are not told how many servants were still under the ad-

13. See Numbers 1:4-18 and 7:2-Il for an indication that the princes of each tribe
were the physical descendents of the ewelve patriarchs. Cf. E. C. Wines, The Hebrew
Republic (Uxbridge, Massachusetts: American Presbyterian Press, 1980), pp.
99-100. This was originally published in the late nineteenth century as Book I] of the
Commentary on the Laws of the Ancient Hebrews.

14. Foiker Willesen, “The Yalid in Hebrew Society,” Studia Theologica, XII (1958),
p. 198.

 
