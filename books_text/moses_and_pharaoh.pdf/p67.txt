Imperial Bureaucracy 49

The idea of imperial bureaucracy therefore rests on the idea of the
continuity of being: from God w the planning elite, the representatives
of the people (who may or may not be considered part of this con-
tinuity of being).

The Egyptian State created a bureaucracy so vast, so all-
encompassing, that nothing in man’s history rivaled it until the rise
of the modern industrialized socialist commonwealths. The State en-
shrined the cult of the dead in a desperate attempt to achieve life
beyond the grave.5? Life was seen as static, something which
possesses unchanging continuity with life after death, at least for the
Pharaoh. This static culture was statist to the core.

When the Exodus came, it did not simply free an enslaved
population from physical bondage. It freed them from a static,
hopeless society that was doomed, even if economically successful for
the kings and nobles, to endless boredom—a kind of living death.
The Uiving” death of the Pharaoh’s mummy was mirrored in the living death of
the society. God delivered Israel from a society which was based on the
theology of the divine State. No king in Israel ever claimed to be
divine, for only God has that right of absolute sovereignty. The peo-
ple of Israel, even under the worst of Israel's kings, were never again
to live within the imperial bureaucracy of a centralized divine order,
except when they were again in bondage to foreign rulers.

The freedom which God provided for them was comprehensive,
and the heart of this freedom was religious: the denial of absolute
sovereignty any place on earth except in God’s “holy of holies” in His
temple, the center of which was the ark which contained the sum-
mary of His Jaw. There is sovereignty only in God’s word, not in the
secret labyrinth recesses of some dead man’s pyramid.

52. In the Soviet Union, Lenin's tomb has become the national shrine. They
keep his embalmed body in a glass case for the masses to visit, He is probably the
best-dressed person in the USSR —all dréssed up, with nowhere to go.
