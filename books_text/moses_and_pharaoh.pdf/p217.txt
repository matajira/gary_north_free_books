The Rule of Law 199

tween power religion and dominion religion. The difference is found
in the differing conceptions of man that were proclaimed by the two
religions. The biblical view of mankind is simultaneously universal
and particular (both one and many). There is unity: all men are
made in God’s image, and all men (apart from grace) are ethical
rebels, disinherited by their Father in heaven. There is also disunity:
some men have been regenerated and put under a new household cove-
nant, the household of faith, God’s household, In contrast to pagan
religion, the meaningful differentiation is not between those born in
one geographical area versus those born in another. The differenti-
ation is between birth in Adam’s flesh versus moral rebirth by God's
spirit. It is the “old birth” versus the “new birth” which ultimately
divides men.

There is, on the one hand, a divisive aspect in biblical religion, as
in every religion. It is the division between saved and lost, between
covenant-keepers and covenant-breakers. In short, this division is
ethical, not geographical. The new creation is equally ethical, not
the product of civic rituals of chaos, or the family religion of
placating dead ancestors. On the other hand, there is also a universal
aspect of biblical religion, which in turn creates a universality of
biblical civic order. The link between all men, saved and lost, is the
fact that all men are made in God’s image, and all men have been
assigned the dominion covenant (Gen. 1:26-28), This, in turn, im-
plies the universality of God’s law, for God’s law is the primary tool of
dominion, Since all men are in rebellion against God, all men need
the restraint which biblical law offers. Biblical law provides social
and political order. Thus, the covenantal law structure of Israel is
morally binding on all men. This jaw-order is essentially ethical. All
men are to live righteously and exercise dominion; therefore, all
men deserve the protection of biblical civil law.

Alicns in Israel were to see the beneficial effects of the law and
report back to their own nations concerning the rule of righteousness
in Israel—righteousness which was not confined to citizens only.
‘Therefore, biblical civil law was and still is to be a means of
evangelism.

Behold, I have taught you statutes and judgments, even as the Lorp my
Ged commanded me, that ye should do so in the land whither ye go to
possess it, Keep therefore and do them; for this is your wisdom and your
understanding in the sight of the nations, which shall hear all these statutes,
and say, Surely this great nation is a wise and understanding people. For
