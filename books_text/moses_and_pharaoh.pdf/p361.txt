The Demographics of Destine 343

The guilt-manipulators have spotted their marks— soft-hearted
and soft-headed Christian intellectuals who have become political
“soft touches”— and they are merciless in their pursuit of their in-
tended victims. If they should be successful in their ideological
efforts, millions of Third World victims will join the ranks of the
dead and dying —today’s victims of an earlier “benevolent” colonial
socialism. The international division. of labor will collapse, along
with per capita output, as it has in the Soviet bloc. And it will all be
done in the name of Jesus and Christian justice. 1 am reminded of
James Billington’s remark concerning the French Revolution: “Most
of the conspirators shared this belief in Christ as a sans-culotte at heart
if not a prophet of revolution, The strength of the red curates within
the social revolutionary camp intensified the need to keep Christian
ideas from weakening revolutionary dedication.”>2

The Legalization of Abortion®?

“Population explosion” is a pejorative phrase in the late twentieth
century. Another variant is “people pollution.” There will come a day
when historians and social commentators will look back in disbelief
and disgust at the billions of tax dollars that were granted to public
and private propaganda agencies after 1965 to “spread the word”
about the supposed evils that “inevitably” result from the growth of
population.*+ In industrial nations that are facing literal extinction
in the long run because the birth rate of their citizens is below the
replacement rate of at least 2.1 children per woman, intellectuals are
advocating abortion, mass education programs favoring contracep-
tion, and similar restraints on births. As of 1975, nations that no
longer had fertility rates above the replacement rate included West
Germany, Denmark, Austria, Belgium, France, Holland, Norway,
Sweden, Switzerland, Great Britain, and the United States.5* These

52, James Billington, Fire in the Minds of Men: Origins of the Revolutionary Faith (New
York: Basic Books, 1980), p. 76.

53. For a survey of the history of abortion, from 2050 8.c. (conventional dating),
see Part 2 of the essay by Eugene Quay, “Justifiable Abortion — Medical and Legal
Foundations,” Georgetown Law Review, XLIX (Spring 1961).

54. From 1965 through 1976, governments had spent the equivalent of a billion
and a quarter dollars to promote worldwide programs of population reduction. Well
over $850 million of this came from the taxpayers of the United States. An addi-
tional quarter of a billion had been spent by the Ford Foundation and the
Rockefeller Foundation for this same goal. See Simon, The Ultimate Resource, p. 292.

55. “People Shortage,” Wall Street Journal (Aug. 23, 1979), chart: “West European
Fertility Rates.” The peak in the fertility rate in the U.S. was 3.7, in 1957. By 1975, it
