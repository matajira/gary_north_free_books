The Rule of Law 195

Egypt, to give you the land of Canaan, and to be your God” (Lev.
25:38). He redeemed (bought back) Israel from oppression; there-
fore, Israel is not to become an oppressor. “Thou shalt neither vex a
stranger, nor oppress him: for ye were strangers in the land of Egypt”
(Ex, 22:21; cf. 23:9; Lev. 19:33; and Deut. 10:19). The prophets also
repeated this warning against oppressing strangers (Jer. 7:6-7; Zech.
7:10; Mal, 3:5).°

The rule of law is established unmistakably: “Ye shall not respect
persons in judgment; but ye shall hear the small as well as the great;
ye shall not be afraid of the face of man; for the judgment is God’s:
and the cause that is too hard for you, bring it unto me, and I will
hear it” (Deut. 1:17). This was Moses’ recapitulation of his decision
o create a hierarchy of judges over Israel (Ex. 18), The judges must
not respect persons, for they act as God’s agents. God does not
respect persons: “For the Lorp your God is Ged of gods, and Lord of
lords, a great God, a mighty, and a terrible [God], which regardeth
not persons, nor taketh reward, He doth execute the judgment of the
fatherless and widow, and lovcth the stranger, in giving him food
and raiment. Love ye therefore the stranger: for ye were strangers in
the land of Egypt” (Deut. 10:17-19). This is the biblical doctrine of judicial
love: we are required to render honest judgment, and to bring the rule of law over
ail men, including the stranger. The very next verse tells us why we must
love all men in this way: “Thou shalt fear the Lorp thy God; him
shalt thou serve, and to him shalt thou cleave, and swear by his
name” (Deut. 10:20). Thusc who love God, who cleave to His
name,’ and who swear (give a binding oath) by His name, are to
love His law and apply it without prejudice of persons.

6. One of the continuing themes in the writings of “liberation theologians” is the
evil of oppression. They always equate oppression with economic oppression, and
economic oppression with free market capitalism. What is important to consider
here is the implicitly statist nature of oppression: Isracl was oppressed in Egypt pre-
cisely because the Egyptians did not honor God's law. Hebrews are to honor God's
law and enforce it throughout the land. ‘I'he State is to be restrained by biblical law.
This makes it very difficult for anyone to gain oppressive powers over others; poten-
tial oppressors cannot gain the co-operation of public officials in applying the State’s
monopoly of violence against certain economic groups or organizations. When the
civil government refuses to enforce God's law on ull people, the result is oppression.
The whole of the law must be enforced in order to avoid oppression. The liberation theologians
are universally unwilling to recommend that the civil government enforce the whole
of Old Testarnent law. Thus, they are advocates of oppressing institutions.

7. The Hebrew word translated here as “cleave” —-dawbak— means “to join to” or
“cling to,” the same word used in Genesis 2:24: “cleave unto his wife.” It refers to a
covenant.

 

 
