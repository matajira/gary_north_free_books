312 MOSES AND PITARAOIL

argues that the Sixth Dynasty, the Twelfth Dynasty, and the Thir-
teenth Dynasty all overlapped, because the kings associated with
these “dynasties” often were not pharaohs, but were only officials.
Thus, we really should not think of these parallel groupings as
dynasties. Courville argues that Pepi II was the last significant king
of the Sixth Dynasty, whose personal reign was remarkably long and
therefore had to stretch into the era of the Hyksos.4* He has to con-
clude this because conventional historians believe that the evidence
from Manetho and the Turin Papyrus indicates that Pepi II reigned
from age six to age 100, making him the longest-lived ruler in Egypt-
ian history.*? If the Sixth Dynasty overlapped the Twelfth and Thir-
teenth Dynasties, then Pepi II’s reign must have extended into the
Hyksos era, since Ipuwer addressed his poem or lament to Pepi II.
Therefore, Gourville has to conclude that this king, the son of a very
powerful ruler whose monuments are found all over Egypt,*® was
not the Pharaoh of the Exodus, or even a Pharaoh at all. This is a
major problem with Courville’s reconstruction. Could this powerful
man have been a subordinate ruler during the reign of a weak
Pharaoh, Koncharis, whose. reign ended in the Red Sca? This is
another reason why [ am not yet fully convinced by Courville’s
arguments. There may be some other way to unscramble the con-
tradictions of Egyptian chronology. Neverihcless, there is much to
be said for his thesis, despite some important problems. Courvillc’s
thesis is the place where any self-consciously Christian (i.e., anti-
evolutionary) Egyptologist should begin his investigations.

 

 

Courville’s Reconstruction: Overlapping Reigns

 

Courville is not a well-organized writer. His two-volume work,
The Exodus Problem and Its’ Ramifications, is exasperating. Its index is
atrocious, its footnotes are difficult to master, it does not stick to a
clear-cut chronological development from the front of the book to the
rear, it makes continual references to obscure documents, and it
never really summarizes the thesis. As editor of The Journal of Chris-
tian Reconstruction, I asked him to produce a summary essay of his
thesis for the journal. He submitted an initial manuscript which was
barcly more organized than his book, but he graciously consented to

 

46. Ibid, 1, p. 225.

47, James Henry Breasted, A History of the Ancient Egyptians (New York:
Scribner's, 1908), pp. 127-28.

48. ibid, p. 119.
