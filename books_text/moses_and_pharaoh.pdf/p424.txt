406

bureaucracy &, 48
continuity of, 176
compound, 329
economic, 270-72
envy vs., 75-77
limits to, 331
metaphors, 262, 264
normative, 272
“unlimited,” 332

guardian, 374, 377-78, 383

guilt, 287, 327, 343

gulag, 288-92, 367

Gulag Archipelago, xv, 58, 1198

gypsum, 371

habeus corpus, 286
hail, 106, 116, 117, 120
Hannah, 250
Harawa, 371
Hardin, Garrett, 336
harmony, 271
Harper, F. A., 117
Harrison, Jane, 105n, 384
Harrison, Richard, 301
Harvard University, 304-5
harvest, 278
Hauser, Philip, 344n
Hayek, F. A.
anarchism, 232
anarchy, 233
coercion, 229, 233
command society, 231
Eskimo, 232, 240
evolution, 232
inequality, 75-76, 230
judge-discovered law, 224
legal rules, 203
liberalism, 214, 229
morality, 232
Parliament, 225
privilege, 230
progress, 75-76
prosperity, 212
relativism, 232, 235, 240
safety net, 230, 232, 233
self-interest, 232
social justice, 228-35

MOSES AND PHARAOH

special interests, 234
spontaneous order, 229, 231, 233
utilitarian, 11t
utopia, 234
victimless crimes, 203
welfare State, 229-30

Hazlitt, Henry, xviiin

heaven, 136, 151, 378

Hebrews
army, 183
driven out, 150
faith in Egypt, 145
Fall, 376
fearful, 254-55
fertility, 32
fewest of people, 134
future, 47
low-risk continuity, 145
marching, 156
owned, 120
pessimism, 152
population, 20-25
princes, 19n
slave mentality, 149
tasks, 273
welfarism, 280

Henley, William E., 115

Henry, Patrick, 4

Hequet, 105

herbs, 166n, 172n, 174

Herod, 377

Herodotus, 138, 371

Hezekiah, 146

hierarchy, 205, 206-8, 282

higher law, 224-25

Hill, Marvin, 222n

Hillsdale College, 330n

Himmelfarb, 334n

history
continuity, 175
discontinuity, 156, 158, 173
Egypt's, 7
linear, 265
personal, 175

Hoehner, Harold, [63n

holism, 11, 113

holy of holies, 377
