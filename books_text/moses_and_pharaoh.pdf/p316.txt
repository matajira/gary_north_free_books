298 MOSES AND PHARAOH

institutional testimony to the futility of seeking power apart from
biblical law. It is therefore futile to seek to fulfill the terms of the do-
minion covenant apart from God. It is equally futile to attempt to
escape from the burdens of this covenant. Such an escape leads
directly to historical impotence and slavery under some temporarily
successful group of power religionists. Better to be Moses herding
sheep in Midian than anywhere in Egypt, either as a Hebrew or as a
Pharaoh. Better yet to be Moses confronting Pharaoh, Even better,
to be Moses on the far side of the Red Sea watching Pharach drown.
Better yet, to be in the Promised Land, with a copy of God's law in
your possession, But best of all, to be at work in the wilderness, pro-
gressively converting it into a garden by means of hard work, in
terms of the biblical law which is in your heart, and also in the hearts
of all your neighbors (Ezk. 36:26-29; Jer. 31:31-34; Heb. 8:10-11).
