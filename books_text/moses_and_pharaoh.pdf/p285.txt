The Metaphor of Growth: Ethios 267

and cultural restoration are God’s responses to the ethical decisions
of whole nations or populations.

The New Testament uses the metaphor of the seed in several
passages. The parable of the sower focuses on the effects of the
various environments on four seeds that are scattered (Matt. 13:3-8).
Another parable using the sower focuses on two different kinds of
seeds that are planted by two rival sowers in the same field, the
parable of the wheat and tares (Matt. 13:24-30). Obviously, these
agricultural metaphors are only metaphors. They are used in vary-
ing ways to drive home spiritual points. They. point to time's role in
the process of maturation.

Paul used the metaphor of domestic agriculture to make his case
for the restoration and victory of the kingdom of God. He used the
metaphor of the wild olive branch which is grafted into the root. His
goal was to describe the relationship historically between the Jews
and the gentiles. The regrafting in of the elect Jews at the end of the
era of the gentiles is seen by Paul as the foundation of external recon-
struction and victory—the basis of Paul’s eschatological optimism
(Rom, 11:17-27). His usage of the metaphor makes it plain that the
whole process is unnatural. God, as the plant domesticator, does not
graft the domesticated branch into the wild root, which is the tradi-
tional agricultural practice. He grafts the wild olive branch (gentiles)
into the original root (covenant Israel), Not the process of growth
and decay, but the calculated, purposeful intervention of the active tree
grafter, is Paul’s point. Natural or even traditional practices are not
normative. God's sovereignty is normative.

Classical Culture and Cycles

The possibility of external progress was set before the Hebrew
nation very early. This promise involved the possibility of long-term
economic development as the product of long-term covenantal faithfulness to
God’s laws. Pagan cultures, including the Greeks and the Romans,
took the cyclical processes of nature as normative, and never were
able to develop a progressive philosophy of time.” They were unable

6. John Murray, The Episite to the Romans (Grand Rapids, Michigan: Eerdmans,
1965), IT, pp. 84-90.

7. Ludwig Edelstein argues in his posthumously published book, The Idea of Pro-
‘gress in Classical Antiquity (Baltimore, Maryland: John Hopkins Press, 1967), that the
Greeks and Romans did not ali believe in cyclical time, and that some of the promi-
nent philosophers did belicve that some lincar progress is possible indefinitely. His
