The Optimum Production Mix 101

community have the effect of changing the responses of other people,
especially their parents. Fathers start working harder, or longer
hours, or both.! Parents become more future-oriented in many in-
stances. They begin saving a larger proportion of their income, In
short; the honeymoon is over when Junior arrives. “More mouths to
feed” means more food to feed them in nations that are future-
oriented and whose. people possess what is known as the Protestant
(or Puritan) work ethic.

These observations, however, must not be taken as refutations of
the existence of the law of diminishing returns. On the contrary,
such evidence points directly to the existence of such a law, If it is
true, as it so often appears to be, that “necessity is the mother of in-
vention,” then from whence comes the perceived necessity? If addi-
tional mouths to feed call forth ever-greater exertion and creativity
to feed them, how is it that these mouths produce the desirable
stimulus? Isn’t it because there was an optimum production mix
under the old conditions that was disturbed by the new mouths? Isn’t
it because the new conditions changed the thinking of the former
honeymooners, giving them new incentives to mature and become
more responsible? Isn’t it the very pressure of the optimum produc-
tion mix which calls forth better efforts to find new combinations of
resources (new plans) in response to new economic conditions?

The law of diminishing returns (optimum production mix) is an
aspect of the creation which can, given the proper ethical and in-
stitutional circumstances, lead to human progress. Sinful men need
to be pressured into greater self-discipline, more effective thinking,
and better strategies of innovation. One of the means of calling forth
these better efforts is scarcity. While the law of diminishing returns
would have prevailed in the garden, the direct economic incentives
to overcome scarcity in a cursed world augment this original aspect
of the creation. Thus, it is unwise to de-emphasize the importance of
this law of human action, as Simon appears to do. But the concept.
should not be misused, either. It should not lead men to conclude
that the price of raw materials will necessarily ‘rise if population
grows. The concept should. not be used to justify a zero population
growth ideology.

What we should understand is that the existence of an optimum
production mix is what lures entrepreneurs, inventors, and

16. Ibid, p. 260.
