Imperfect Justice 283

exercising leadership in areas of Hebrew life other than the court-
room. Furthermore, this system would permit more rapid resolu-
tions of disputes. Justice was to be dispensed continually —“at all
seasons” —and speedily.

God gave no explicit revelation to Moses concerning the establish-
ment of a court of appeals, It was an ad hoc decision based on informed
common sense. Jethro, who was a priest of Midian (v. 1), must have
been familiar with the problems of dispensing divine justice. He could
see how large the nation of Israel was. Acknowledging the validity of
the principle of scarcity —in this case, ihe scarcity of time~— Jethro came
to an obvious conclusion. This conclusion involved the acceptance of
man’s limitations. Even in this historically unique circumstance of
men’s access to perfect justice, it was preferable in the vast majority of
cases to obtain speedy human justice rather than delayed divine
justice. Human justice was at least available to everyone, while
Moses’ judgments were allocated by means of lining up. No one could
be sure that his case would even be considered.

The scarcity of time demanded a judicial division of labor. The
division of labor allows men to overcome many of the restrictions
imposed by scarcily in every area of life. In this case, the judge’s
office was broken down into many offices, with specialists at each
level (one in every ten judges) who could take appeals.

The criteria for admission into the position of judge were moral
rather than technical or educational. Uncovetous men (bribe-
resistent), fearful of God, with reputations of truthfulness, were the
preferred judges (v. 21). Ability was also important, but .moral
qualities were emphasized.

Consider the available judges. They had grown up as slaves. The
whole generation, except for Joshua and Caleb, possessed slave
mentalities. Nevertheless, their rule was preferable in most cases to
a system which offered perfect justice in individual cases, but which
had to ration the number of cases. Conclusion: regular. and predictable
justice provided by responsible amateurs is better than perfect justice
provided on a sporadic or “first in line” basis. The burden of dispens-
ing justice had to be shared (v. 22). This was required in order to
permit the people to endure, going their way in peace (v. 23).

The Right of Appeal

The right of appeal was limited to “great matters.” Cases involv-
ing fundamental principle, and those that would be likely to have
