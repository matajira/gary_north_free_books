Total Sacrifice, Total Sovereignty 127

sion from the God of Israel, and that this foreign God had the au-
thority to compel Egypt to suspend all signs of its sovereignty over
Israel for several days, perhaps a week. (The phrase, “three days’
journey,” may have meant a round-trip of three days, or three days
out and three days back, although the latter seems more plausible:
Ex. 5:3.) The Pharaoh had to put the Israelites on their “good behav-
jor,” relying on their sense of justice to return to bondage. To have
done so would have meant abandoning his role as absolute sovereign,

What if he had agreed to Moses’ request? The Israelites had not
requested freedom. God had not instructed Moses to call for a per-
manent release from Egypt. God had told Moses that He intended to
jead them out of Egypt on a permanent basis (Ex. 3:8), but He did
not instruct Moses to demand their release. Moses was only to re-
quest a-week or less of freedom. God promised: “And I am sure that
the king of Egypt will not let you go, no, not by a mighty hand” (Ex,
3:19), or as the New English Bible puts it, “unless he is compelled.”
Why was God so certain? Because He intended to control Pharaoh’s
very decisions: “. . . and the Lorp hardened Pharaoh’s heart, so that
he would not let the children of Israel go out of the land” (Ex. 11:10).
God's active hardening of Pharaoh’s heart was basic to His promise to
deliver Israel permanently: “And I will harden Pharach’s heart, and
multiply my signs and my wonders in the land of Egypt. But Pharaoh
shall not hearken unto you, that I may lay my hand upon Egypt, and
bring forth mine armies, and my people the children of Israel, out of
the land of Egypt by great judgments” (Ex. 7:3-4).

The Question of Sovereignty

God demands absolute commitment from all creatures.
Pharaoh, who was believed by the Egyptians to be an absolute sover-
eign, only needed to proclaim his partial sovereignty in order to
challenge God’s claim of total sovereignty. All he needed to do was to
retain a token sovereignty over Israel to make his claim valid. If the
children stayed, or the wives stayed, or the animals stayed, then the
Israelites were symbolically acknowledging the legitimate divine
sovereignty of the Egyptian State. If they would sacrifice inside
Egypt's borders, or at least not too far into the wilderness, then God's
claim of total sovereignty would be successfully challenged. If the
Pharaoh could extract even the tiniest token of symbolic sovereignty,
then God’s claim to the whole of the lives of all the Israelites would
be invalidated. In other words, if the Pharaoh, as a self-proclaimed
