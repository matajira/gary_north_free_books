84 MOSES AND PHARAOH

stating clearly that the people of Isracl were his, when in fact they
belonged to God. When the Egyptians made restitution, it was ex-
pensive beyond their wildest imaginations. The heirs (firstborn) of
the slave-owners were slain.

Tribute

The Pharaoh, as a self-proclaimed divinity, was viewed as the only
incarnate representative on earth of the Egyptian gods. It was the king
who brought the crucially important annual flood of the Nile.
Frankfort comments on this: “Even as late an author as Ammianus
Marcellus [late 4th century, a.v.—G.N.] knew that the Egyptians
ascribed plenty or famine to the quality of their king—not, in a
modern sense, to his quality as an administrator, but to his effective-
ness as an organ of integration, partaking of the divine and of the
human and entrusted with making the mutual dependence of the
two a source of ‘laughter and wonder.’ ”® It was the king who, as a
divine being, brought moral order to the whole world. Thus, when the
Pharaoh of the famine established policies that assured Egypt's sur-
vival during seven years of famine, thereby reestablishing his owner-
ship of all Egypt, his followers could interpret his acts as inspired.
The Pharaoh could be understood as having reemphasized his own
divinity, and the power of the Egyptian gods, before his people.

By enslaving the Hebrews, the later Pharaoh was elevating the
gods of Egypt above the God of Joseph. The ancient world inter-
preted a military victory by one nation or city-state as the victory of
its gods over the gods of the defeated people. Thus, by enslaving the
Hebrews, the Pharaoh was announcing the sovereignty of Egypt's
gods over Joseph’s God. By bringing Egypt to its knees at the time of
the Exodus, God was ritually announcing His sovereignty in the
most graphic way possible. The Egyptians had lost their prosperity,
their children, and now their jewelry. The gods of Egypt had been
brought low. Only the outright destruction of Egypt’s army, followed
by an invasion and conquest by foreigners, could have made the pic-
ture any more graphic, and these events were shortly to follow. The
Pharaoh-god would perish with his army,

The gods of Egypt paid tribute to the God of Israel. This tribute was
paid by the representative of Egypt’s gods, Pharaoh, as well as his

6. Henri Frankfort, Kingship and the Gods: A Study of Ancient Near East Religion as the
Integration of Society & Nature (University of Chicago Press, [1948] 1962), p. 58.
