The Demographics of Decline 353

stage it is a potential person, and from about eight weeks onwards
has a recognizable individuality as manifested by its circulation and
brain activity. It is well on the road to full personhood, and for most
practical purposes may be considered to be a person. Nevertheless, I
do not wish to draw a line between when a fetus is not @ person and
when a fetus is a person. Throughout the whole of its development the
fetus is potentially an actual person, and deserves the respect and
treatment due to a being with this sort of potential.”"°? This. is
medical ethics? This is a call to responsible decision-making?

If Mary, a virgin who found herself pregnant, had decided to
“take the easy way out” and had aborted her “fetus” in, say, the third
month of her pregnancy, would she have eliminated a true Person?
Or just a potential Person? Dr. D. Gareth Jones offers no principle
that would give us a clear indication. Instead, he offers language that
would have confused her, had she not understood the ethics of the
Bible.

He goes on, and it gets worse. “A fetus is part of a more extensive
continuum, the end-result of which is the emergence of an individual
human being manifesting, under normal circumstances, the myriad
facets that go to make up full personhood. The processes of this con-
tinuum, however, do not begin at conception; neither do they end at
birth.”1°8 The continuum: here is a key idea in the biological specula-
tions of D. Gareth Jones. First, “A new-born baby is a very incom-
plete human person . . .”!09 Second, “A corollary of the continuum-
potentiality argument is that there is no developmental point at
which a line can be drawn between expendable and non-expendable
fetuses, that is, between non-personal and personal fetuses. It may
be preferable to carry out abortions early rather than later during
gestation, but that is a biomedical and not an ethical decision.”1!°
Not an ethical decision? Strictly a biomedical decision? You mean a
strictly technological decision? This is precisely what he means. The
official justification of this monstrous book is that it brings Christian
ethics to bear on biomedical technology, but the end result is the im-
position of the satanic ethics of abortion on the consciences of Chris-
tians in the name of autonomous biomedical technology.

But what about the unstated third but obvious point? What

107. Ibi
108. Idem.

109. Zbid., p. 175.
110, Ibid. pp. 175-76.

p. 174.

 
