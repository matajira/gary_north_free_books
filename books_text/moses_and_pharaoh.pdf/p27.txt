Introduction , 9
the’Bible’s infallibility than itis to:make; mistakes that are'governed
by. the presupposition that Darwinian scholarship. is: the éternal
standard-of truth. .

Confrontation

‘The'first fifteen.chapters of the Book of Exodus déal with-the con-
frontation between God and Egypt. This confrontation, was coni-
prehensive. It involved ‘a dispute between. two radically. different
worldviews. It involved a war between the God‘of the Hebrews and
a false god-ealled Pharaoh. Every aspect of eivilization.was at stake.
It-was not “merely” a war over theology as such: It: was a war over
theology as life. This. commentary brings.inito:the open several areas of.
coiifroniation that previously have not been discuissed.. These subor-
ditiate areas: of confrontation, were inescapably linked. to the main
confrontation between.God_and.Pharaoh. Amazingly, the terms of
even this primary confrontation‘are seldom discussed.

It is-my contention, that essentially :the same confrontation has’
continued from the beginning;-meaning from the garden of Eden. It
has.manifésted itself in-many ways,but.the essential question ‘never
changes: Who.is God? Secondarily, what ts the relationship between Ged.
and: His création? The answers given by. the rulers of Egypt were essen-
tially thesame- answer. proposed: to man by Satan: “ye ‘shall -be as

- gods” (Gen. 3:5). Because the.modern, world has come to a similar
theological conclusion — that, in.the absence of any other.God; man
must be thée-only reliable candidate —the. modern world has come to
sirnilar social and economic conclusions. The rise of totalitarian bu-
reaucracies; in, the twentieth century can and should be discussed: in
relation to the rise of a humanistic variation of Egyptian theology. It
is not that: humanists have adopted: Egypt’s polytheism (though
modern relativism sounds suspiciously like pdlytheism), but rather
that they have, as Darwinians.(or worse), adopted Egypt’s theology
of the continuity of being, with the State, as the most powerful repre-
sentative: of “collective mankind,” serving as the primary-agency of
social organization.

‘The remaining chapters in thé Book of Exodus describe the.con-
tinuation of this same confroitation with Egypt. In this case; how-
ever, the departing slaves'of the now:smashed Egyptian civilization
replaced. their. former rulers as the defenders of the old. order. God
dealt with thenvin very similar ways, though with greater mercy, as
a result of Mosés’ prayer oni behalf of the integrity of God's name and
