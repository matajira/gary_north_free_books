Continuity and Revolution 175

from the garden; the people of Noah’s day learned it; and so did
Pharaoh, though only in his last minutes in a watery grave. God cuts
off the leaven of sin in history, so that the leavén of righteousness can
develop and become the dominant cultural force.

The biblical concept of social change is therefore grounded in the
doctrines of creation, ethical rebellion, redemption, dominion, and
final judgment. In short, the Bible teaches a doctrine of linear time.
We are both pushed and pulled through time, and not by impersonal
forces, but by a personal God. God’s declared and inescapable future
draws us through present history, but always by way of the past.
What has gone before has its influence over us, but so also does ail
that is yet to come. The link between past and future is responsible
decision-making, primarily by God and secondarily by men.

The Exodus was a discontinuous event, yet the covenantal life of
Israel was to be renewed annually by a continuing series of Passover
meals down through the ages. The great discontinuous event (for it
is essentially one event) of the death and resurrection of Jesus Christ
took place once and-only once, yet the Communion meals that an-
nounce His definitive triumph over death and evil are to be con-
tinually celebrated by His people through the ages. In short, a
definitive and completed past event—a discontinuity —is to be ccle-
brated continuously through history, for it points to @ definitive final
event in the fuiure: the final judgment. This next great discontinuity
becomes the foundation of the great future continuity: the New
Heavens and New Earth (Rev. 21, 22).

History is therefore equally influenced both by discontinuities
and continuities, the “one great event” and the “many little events.”
History is simultaneously one and many. In this sense, history
reflects the being of God, which is sirnultaneously one and many.
But above all, historical change is personal: God proposes, God
disposes, and men are fully responsible (Rom. 9:10-24).

In a world of cosmic personalism, the “great men” theory of
history is valid. Great men do produce historical discontinuities that
are crucial. But they make these changes within a framework of
historical continuity. They become crucial as pivotal characters pre-
cisely because there is a broad historical milieu which is ready to be
pivoted. The “great man” is nothing without the “little men,” past
and present, who have participated in the development of the
historical setting that at last makes a radical break with the past.

The law of God is one important aspect of historical continuity.
