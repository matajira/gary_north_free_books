The Optimum Production Mix 103

possible for futurc-predicting entrepreneurs to collect profits that are
the residual of accurate forecasting. !*

An enslaved population is unable to make full use of the informa-
tion which is available to producers, for the source of the continual
correcting of this information must come from a bureaucratic
agency —an agency which does not offer employee incentives com-
parable to employee incentives in a free market for the collection of
accurate and economically relevant information, or incentives for affected
institutions to respond appropriately to new information. If a society
relies heavily on the output of an enslaved population, then its non-
slave members are also unable to make the most efficient use of the
available resources, for the society's information-delivery system is made in-
creasingly unreliable. Neither the slaves nor the taskmasters know what
scarce economic resources really should cost. Without a competitive
market, the planners are flying blind. Those who fly blind eventually
rash.

Conclusion

The Pharaoh decided to tighten the screws on the Israelites, in
doing so, he reduced the freedom of a productive people to increase
the per capita wealth of the Egyptian nation. He imposed new costs
on them which could only reduce their productivity, either by reduc-
ing the division of labor or by grinding them into despair, both of
which would ultimately waste Egyptian resources. He abandoned a
free market in goods and services, He increased the authority of his
bureaucratic State. He brought his judgment on the people of God,
merely because their representatives asked for time off to worship
God.

Because of Egypt's heavy reliance on the slave system, neither
Pharaoh nor the nobles knew what the cost of any resource really
was. The more rigorously he enslaved them, the less reliable was the
economic knowledge available to the planners. The Pharaoh thereby
proclairned his faith in Egypt’s theology: the sovereignty of the
Pharaoh and the bureaucratic State which was incarnate in his own
person. The result was the destruction of both the Egyptian State
and his person.

48. North, Genesis, ch. 23: “The Entrepreneurial Function.”
