The Reconstruction of Egypt's Chronology 305

remained unavailable until the mid-1960’s, when the counter-
culture’s revolution overturned most of the established tenets of
every social science and several natural sciences— the era in which
the myth of neutrality died on university campuses throughout the
world,?? It was republished and once again became a popular book,
and a short-lived semi-scholarly periodical, Pensée, was begun in the
early 1970's to explore his theories in relation to several academic dis-
ciplines, and courses in two dozen colleges that relied on some
aspects of his research were being taught in 1973, although the col-
Jeges were not major universities.28 A scholarly journal, Kronos, is
now devoted to studies of the chronology of the ancient world in the
light of Velikovsky’s thesis. But in the early 1950's, outright lies were
spread about Worlds in Collision, and they were repeated in major
book reviews. It was a classic case of academic suppression.??
Harlow Shapley, the Harvard astronomer who helped launch the
anti-Velikovsky campaign, was still sending out letters in the late
1960's that referred to him a “fraud” and a “charlatan.”9¢

“The Shapleyist proscription of Velikovsky and his revolutionary
astronomical concepts,” Horace Kallen writes, “extended to all who,
even though doubting or questioning the concepts, did take them seri-
ously. One such was Gordon Atwater, fellow of the Royal Astronomi-
cal Society, curator of the Planetarium, and chairman of the depart-
ment of astronomy at New York’s Museum of Natural History, who
had read the manuscript for Macmillan. Although Atwater was skep-
tical of many of Velikovsky’s findings, and doubted that Venus could
have been ejected from Jupiter, he took the records of world-wide
catastrophes in historical times to be evidential. He was dismissed
from both his positions with the Museum the night before This Week
published his review of Worlds in Collision, in which he urged open-
mindedness toward the book. James Putnam, for 25 years with Mac-
millan and the editor who made the contract with Velikovsky, was im-
mediately dismissed from that establishment.”3! Yet the book had

27. Gary North, “The Epistemological Crisis of American Universities” in
North (ed.), Foundations of Christian Scholarship: Essays in the Van Til Perspective
(Vallecito, Galifornia: Ross House, 1976),

28. A list of these courses appears in Pensée, I] (Winter 1973), pp. 37-38.

29. Alfred de Grazia (ed.), The Velikousky Affair (New Hyde Park, New York; Uni-
versity Books, 1966). No major publisher would touch this scholarly analysis of the
Velikovsky thesis and the protest it produced

30, Horace Kallen, ‘Shapley, Velikovsky and the Scientific Spirit,” Pensée, Il
(May 1973), p. 36. Reprinted in Vlikoosky Reconsidrred (New York: Warner, 1977), p.
53

31. Tbid., p. 40; Velikoosky Reconsidered, pp. 62-63.
