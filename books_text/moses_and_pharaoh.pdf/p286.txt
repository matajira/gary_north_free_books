268 MOSES AND PHARAOH

to believe in the possibility of long-term, cumulative social and eco-
nomic progress.* To believe in such a possibility, they would have
had to abandon their commitment to the twin doctrines of cycles and
impersonal fate.°

In the debate over whether classical civilization was or was not
committed to the ultimate pessimism of cycles, this much should be
understood: the historians have not been able to produce evidence of
a confident commitment to the idea of inescapable progress, or a doc-
trine of ultimate victory by man over nature. Nisbet’s book, History
of the Idea of Progress, presents worthy examples of fragments from
classical authors that proclaimed some progress by man over the
ages, and the possibility of continuing progress—if not for mankind
in general, then at least for the beneficiaries of classical culture.!®
But to maintain that the classical world did not adopt almost com-
pletely the idea of historical cycles makes it very difficult to explain.
the Renaissance’s rejection of linear history, Nisbet himself has writ-
ten: “From Augustine down to the modern world, with the exception

 

evidence is weak, however, He has to admit that Plato and Aristotle held to a cyclical
view of time, and that rampant skepticism throughout later Greek culture roilitated
against a widespread belief.in progress, ‘There is no question that neither the Greeks
nor the Romans had any concept of time that is exclusively linear— beginning,
meaningful time, and final judgment—or that is necessarily progressive, meaning
beneficial to mankind, in the long run, Greek and Roman society maintained the
annual chaos festivals until the very cnd, which Edelstein fails to mention. This
points to the cultural and religious commitment of classical civilization to a cy¢lical
view of time, On the cyclical views of Greek science, see Stanley Jaki, The Road of
Science and the Ways to God (University of Chicago Press, 1978), ch. 2.

8. Lam here taking exception with Robert Nisbet’s excellent book, History of the
Idea of Progress (New York: Basic Bouks, 1980), ch. 1. He follows Edelstein, W. K. C.
Guthrie, M. T. Findlay, and his own teacher, Frederick Teggert, in arguing that
there were philosophers in Greece and Rome who held out the possibility of
cumulative increases in men’s knowledge, tcchnological improvements, and even
moral improvement over time. He rejects the thesis that classical thought was domi-
nated by a notion of cyclical time, as argued by John Baillie, F. M. Cornford, W. R.
Inge, R, G. Collingwood, and J. B. Bury, Nisbet’s intellectual enemy in-this debate.
He sees the myth of Prometheus, the god who gave mankind fire, as the archetypal
cxample of human advancement: pp. 17-18.

9. On the collapse of the ancient world’s philosophical base, which led to the col-
lapse of classical civilization, see Charles Norris Cochrane, Christianity and Classical
Culture: A Study in Thought and Action from Augustus to Augustine (New York: Oxford
University Press, [1944] 1957).

10. Cochrane points to the Roman poet Vergil as one who believed Rome might
break out of the familiar cycle of growth and decay, to become eternal Rome. The
Empire's universality might lead to a permanence that other empires had failed to
achieve. {tid., pp. 68-73.

 
