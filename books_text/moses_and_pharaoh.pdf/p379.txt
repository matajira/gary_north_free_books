The Demographics of Decline 361

deaths in that war was 500,000.13? That matched the death toll of the
Greco-Turkish War of 1919-22, which was going on at the same time
as the Russo-Polish war of 1919-21, which claimed 200,000 lives. The
Mexican revolution of 1910-20 —the first successful socialist revolution
in modern times—took 2 million lives.!°* These were all minor wars
in the twentieth century, seldom mentioned in textbooks, On page 155
of his book appears the most horrifying statistical chart ever published
(I say this without fear of contradiction), “The Death Process”:

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Individual Identity
MEN with: MEN,
men | some women, | WOMEN & | TOTAL
say 10% CHILDREN
Millions of deaths
CAMP PRIVATION 20
Enclosed ghetto 1 1
Prisoner-of-war camp 45 45
Concentration camp 2 0.5 25
Labour camp 12 2
CITY PRIVATION 16
Unenclosed ghetto 1 1
iege i 1
Occupation 6 6
Civil dislocation 8 8
DIFFUSE PRIVATION 26.5
‘Transit 15 1.5
Combat 1 i
Economic blockade 2 2
Man-made famine 5 5
Scorched earth 5 5
War dislocation 12 12
HARDWARE 47.5
Big guns 18 18
Small arms— formal execution 4 4
Small arms— massacre 1 1 4 6
Small arms— combat 14 i4
Mixed — demographic 3 3
Acrial bombs 1 1
CHEMICALS— Gas 15 15
TOTALS:
MEN 42 16 19 7
‘WOMEN 2 a1 23
‘GHILDREN 10 10
42 18 50 110
137. Tbid., p. 99.

138. Zid, p. 98.
