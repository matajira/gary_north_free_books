250 MOSES AND PHARAOH

the Passover or other visits, the animal, or its substitute, or its pay-
ment was taken. In the meantime, it could not be used for profit
(Deut. 15:19). The formal or symbolic separation from the mother
took place on the eighth day; to keep it alive, it would have been nec-
essary to let it continue to nurse. This was the case in the dedication of
Samuel by Hannah (I Sam. 1:11, 22-23). Perhaps the family devised
some mechanical means to feed the newborn animals during the wait-
ing period between the separation and the sacrifice or the redemption.

The firstborn male in each family had to be redeemed by means
of a payment to the priests. The family had to pay five shekels
(Num. 18:16). This was done one month after the birth of the child.
‘The same payment could be made to save the life of an unclean beast
(Num, 18:15), Possibly only the donkey had to be redeemed by
means of a lamb; other unclean animals were purchased with money
from death, although it may have been that all animals were
redeemed with lambs.! The firstborn of cows, sheep, and goats had
to die (Num. 18:17).

The language of the firstborn indicates that the firstborn son of
every Hebrew family had to be dedicated to God for service. God
made the following arrangement: “Take the Levites instead of all the
firstborn among the children of Israel, and the cattle of the Levites
instead of their cattle; and the Levites shall be mine: I am the
Lorp” (Num, 3:45). Families then made payment to the Aaronical
priesthood instead of dedicating their own firstborn sons (Num.
3:48, 51), At the time of the segregation of the Levitical priesthood,
each family with a firstborn son made this initial payment (Num.
3:46-47). From that time on, the payment for firsthorn sons went to
Aaron’s house.

Egypt had chosen not to substitute the blood offering required by
God to serve as a substitute payment. The firstborn sons of Egypt
died. There is no escape irom this required sacrifice. This is the only
foundation of human history after the rebellion of Adam. God shows
mercy to the sons of Adam for the sake of Christ’s sacrifice as God’s
firstborn, the high priest, the one set apart by God to become the
firstborn offering.

1. James Jordan suggests that all unclean beasts were “redeemed” by lambs, while
humans were redeemed with silver. He cites I Peter 1:18-19: “Forasmuck as ye know
that ye were not redeemed with corruptible things, as silver and gold, from your
vain conversation received by tradition from your fathers; but with the precious
blood of Christ, as of a lamb without blemish and without spol.”
