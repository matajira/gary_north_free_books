Conclusion 297

the rigors of history (change). The goal was static power — power
over nature, over the netherworld, and over scarcity, But such a
static state of existence can be achieved only in death, Thus, the
monuments of Egypt were monuments of death: the pyramids, the
tombs, and the labyrinths, Their quest for power, meaning freedom
from the God-cursed changes in life, led to their cult of the dead.
The Egyptians hoped for resurrection, but theirs was a resurrection
based on magical manipulation, not a resurrection based on biblical
ethics.

Pharaoh manifested this cult of the dead in his attempt to murder
the Hebrew males. He ‘could not stand the pressures of social
change, particularly the social changes forced upon Egypt by the
high birth rates of the Hebrews. He launched a program of
genocide. In this respect, he was only marginally different from the
humanitarians of the twenticth century. As Schlossberg says:

It is no coincidence that humanitarian policy has reached the zenith of
its influence at a time when death propaganda is so much in evidence. The
arguments in favor of abortion, infanticide, and cuthanasia reveal that the
humanitarian cthic wishes to restrict the right to live and expand the right
to die—and to kill. Humanism is a philosophy of death. It embraces death,
wishes a good death, speaks of the horrible burdens of living for the baby
who is less than perfect, for the sick person in pain. I¢ is intolerable to live,
cruel to be forced to live, but blessed to dic. It is unfair to have to care for
the helpless, and therefore merciful to kill. Those who wish to go on living,
it seems, are guilty and ungrateful wretches dissipating the energies of “loved
ones” who have better uses for the estate than paying medical bills.®

God confronted the Egyptian religion of death by calling into
question its assertion of power. He dealt with the Pharaoh, his
priests, and his people in terms of their religious presuppositions. He
demonstrated publicly for all the world to see that the power religion
of Egypt was a fraud. Pharaoh had no choice in the matter, God de-
cided to make a spectacle out of him and out of Egypt. “And in very
deed for this cause have I raised thee up, for to show in thee my
power; and that my name may be declared throughout all the earth”
(Ex. 9:16).

The Hebrews had this example behind them. They were to re-
mind themselves and their children of the implications of the power
religion. This.was the educational function of the Passover. It was an

8. Schlossberg, p. 82.
