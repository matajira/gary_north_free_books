Continuity and Revolution 151

your shoes on your feet, and your staff in your hand; and ye shall eat
it in haste: it is the Lorp’s passover” (Ex. 12:11). Everything in the
Passover pointed to God’s deliverance of His people from
bondage—literally, an overnight deliverance, Vhe lamb was consumed
in one night. The people were to stand, staffs in their hand, shoes on
their feet, ready to march. Ready to march: out of Ur, out of Haran,
out of Sodom, out of Egypt, and into the Promised Land. God’s peo-
ple were to celebrate a feast as an army celebrates a victory, for their
feast pointed to the coming victory—over Egypt, over Canaan, and
especially over sin (Eph. 6:10-18). It-was a pre-victory feast, celebrated
before marching orders were officially given.

The Passover reminds all future generations of God's people of
the miraculous discontinuity of God's redemption. God passed over: the
houses of the Hebrews, and the destroyer passed through the houses
of His enemies, taking as a lawful sacrifice the firstborn. Then Israet
passed through the Red Sea and finally through the Jordan River.
Had they remained faithful to God, they would have passed through
the wilderness in much less than 40 years, When God “passes
through” a rebellious culture, judgment is at hand. The Hebrews
became instruments of His judgment in Canaan.

Marching Orders

The Passover feast was to remind them of both life and death. It
was to remind them of the need for immediate marching at the command of
God. There was nc time to waste. A shattering of Egypt's founda-
tions was about to begin. The Israelites were being called out of
Egypt. Yet this also meant onc of two things: being called into the
wilderness for the remainder of their lives, or being called into Canaan.
Leaving meant gorng. Going where? The wilderness or the Promised
Land? We can never leave without going; even our departure from
the world demands that we travel to a final destination: the new
heavens and new earth, or the lake of fire. (Heaven and hell are
“holding areas” or “embarkation points,” not permanent resting
places: Rev. 20:12-14.) The Hebrews knew what leaving Egypt
meant: a radical break with their immediate past, and all of its familiar
aspects — a break which would inaugurate a new era of personal re-
sponsibility. Their complaints in the wilderness demonstrated that
they preferred not to remain there, and their refusal to enter the land
of Canaan immediately indicated that they chose not to go there,
