Continuity and Revolution 149

Egypt had brought their Egyptian masters low did they believe that
their external conditions would change. Institutional continuity was still
dominant in their thinking, for ontological continuity between God and
man was still dominant in their thinking. They trusted more in the
theocratic power of Egypt’s gods than they did in the God of Moses.
They believed that they would still remain slaves in Egypt, as their
fathers had been. And if the gods of Egypt were what the Pharaoh
claimed, then there could never be a radical break with the past.

The history of the patriarchs should have warned them against
such a view of history. The creation was itself an incomparably
radical break—a break into history, or better stated, the advent of
history out of nothing. The creation of the species, the creation of
Adam, and the creation of Eve were all radical breaks. Adam’s rebel-
lion was a break, and the signs ofthat break are with men still, since
the creation labors under a curse (Gen. 3:17-18; Rom. 8:19-22). The
flood in Noah's day was a startling break with the past—a clean
sweep of unimaginable proportions, The scattering at Babel, the
calling of Abraham, the destruction of Sodom and Gomorrah, and
the famine of Joseph's day were all discontinuities in history.

On the one hand, historical continuity is guaranteed by the transcendent
plan (decree) of God over history, unshakable in its permanence. Histor-
ical continuity is therefore ethical continuity. Men and societies are
totally responsible before God, who sustains the entire creation and
whose decree is inescapable. On the other hand, actual historical
events are sometimes sharp breaks from the historical continuities
that. preceded the breaks.

What the Passover was meant to teach the Israelites was that
God judges kings and commoners in terms of His law, and that the
foundation of human freedom and human progress is ethical. Which
divinity will men worship, God or some idolatrous representation of
another god, cither natural or supernatural? Which law-order will
men attempt to conform to, God’s or some idol’s?!* The continuity
which relates covenantal faithfulness and institutional blessings
(Deut. 8:18; 28:1-14) is contrasted with the institutional discon-
tinuities produced by God's judgment against ethical rebellion. (Deut.
8:19-20; 28:15-68). The archetypal event in Israel’s history which
was to reveal the relationship between faithfulness and institutional

44. Herbert Schlossberg, Idols for Destruction: Christian Faith and Its Confrontation
with American History (Nashville, ‘Termessee: ‘homas Nelson Sons, 1983).
