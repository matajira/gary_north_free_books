Total Sacrifice, Total Sovereignty 129

as the final earthly authority concerning religion, and also the source of the
rights of religion. God would thereby have acknowledged the legiti-
macy of Egypt's prior claim of lawful authority over the religious
affairs of the Israelites. God would thereby have acknowledged the
right of the Pharaoh to retain at least token authority, which meant
the right of the Pharaoh and his heirs to compete with God at any
time in the future for total authority, The God of the Hebrews might
eventually be compelled to give back to the State what the State had
originally claimed.

Pharaoh had rejected Moses’ initial request because he had be-
lieved that he possessed total authority. At each stage, he gave up
something, but he never was willing to give up everything. He was
willing to relinquish some of his authority temporarily, for as long as
God brought the plagues. However, should God change His mind,
or lose power, or forget the Israelites, then Pharaoh might be able to
re-establish his claim of total sovereignty. Pharaoh viewed this con-
test as a sort of cosmological “tug of war,” in which he retained lawful
authority of at least one end of the rope. God might pull him close to
the line temporarily, but one end was rightfully his. If God would
simply acknowledge Pharaoh’s right to his end by allowing him the
right to set any of the terms of Hebrew sacrifice, then Pharaoh’s case
would not be completely destroyed. If he bided his time, the God of
the Hebrews might go away, leaving Pharaoh with the whole rope.

Tf the State can establish a foothold—even a temporary toehold
—of autonomous sovereignty, then it has established a lawful claim
to as much sovereignty as it can gain through the imposition of raw
power. God's claim to absolute, uncompromised sovereignty is suc-
cessfully challenged by man whenever God is forced to surrender
even token autonomous sovereignty to man. Only if God’s sover-
eignty is absolute can God claim to be the sole source of meaning
and power in the universe. It is fallen man’s goal to share some of
that sovereignty. It is also Satan’s goal, even if man becomes, tem-
porarily, the holder of any fraction of this original, autonomous sov-
ereignty, for if Satan can demonstrate that man has any final (or orig-
inal) sovereignty whatsoever, then God’s claim of total sovereignty
collapses, which is the very essence of Satan’s challenge to God.
Satan can collect from man whatever sovereignty man might snatch
from God, given the fact of Satan’s greater power; but he must first
place man in the same continuity of being with God. This attempt is
always futile, from Genesis to Revelation.
