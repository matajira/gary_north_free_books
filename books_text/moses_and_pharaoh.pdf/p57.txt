Imperial Bureaucracy 39

of cach Osirian’s Religion was his hope of resurrection in a trans-
formed body and of immortality, which could only be realized by
him through the death and resurrection of Osiris.”29

Budge tried to reconstruct the basics of Egyptian religion without
too extensive a reliance on the native Egyptian literature, since “we
find that in no portion of it does there exist a text which is not associ-
ated with magic, that no text contains a connected statement of the
purely religious beliefs which we know the Egyptians certainly pos-
sessed. . . .”80 But magic was basi¢ to Egyptian religion, as Moses’
confrontation with the court magicians indicates. It will not do to at-
tribute such “base characteristics” of Egyptian religion to later
developments, as Budge did, and to link them with foreign gods.3!
The Egyptians believed in a power religion, in contrast to the ethics
religion of the Hebrews.

The gods of the, Egyptians remind us of the nature gods of the
American Indians. Like the Amcrindians, the Egyptians were poly-
theistic. Budge said in 1911 that Egyptologists knew then of at least
three thousand different names of their gods. But he could not resist
adding, as so many anthropologists add to their accounts of pagan
polytheism, “the Egyptians believed in the existence of One Great
God, self-produced, self-existent, almighty and eternal, Who cre-
ated the ‘gods, the heavens and the sun, moon and stars in them,
and the earth and everything on it, including man and beast, bird,
fish, and reptile. They believed that he maintained in being every-
thing which He had created, and that He was the support of the uni-
verse and the Lord of it all.”5? In short, the Egyptians supposedly be-
lieved in the same sort of distant, impotent god that late-nineteenth-
century nominal Anglicans believed in, and this god was just about
as important to the Egyptians in their daily lives as the Anglicans’
god was to the English in 1900.

According to Budge, the Egyptians seldom even mentioned this
god’s name, “Neter.” “No proof of any kind is forthcoming which
shows that the Egyptians ever entirely forgot the existence of God,
but they certainly seem to have believed that he had altogether ceased
to interfere in human-affairs, and was content to leave the destinies

29. E. A. Wallis Budge, “Prefuce,” Osiris: The Egyptian Religon of Resurrection (New
Hyde Park, New York: University Books, [1911] 1961), p. xi,

30. Zid, p. xiii,

31. Tbid., p. xiv.

32. Ibid., pp. xxvii-xxviil.
