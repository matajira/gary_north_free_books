The Reconstruction of Egypt’s Chronology 317

Table I
EGYPTIAN CHRONOLOGY

Reconstruction

Dynastics
by number

I ¢. 2125-1880 III is parallel to late I starting
about one century later than I.

Iv ¢. 1880-1780 First half of II is parallel with IV.
Vv c. 1780-1640 Last half of H is parallel with V.

xr 1692-1480 Tl and V extend briefly into the
cra of XIT. VI is parallel with
XII but starts about 75 years
later and extends about 75 years
past the end of XIE. XIII is com-
posed of subrulers and officials
under XII.

XVI 1445-1028 XVI is Hyksos, ruling parallel
with XV, also Hyksos. XIV, VII
to X were local dynasties ruling
by permission of the Hyksos.
XVII was composed of the kings
during the war of liberation.

Dates and Notes

XVII 1028-700 The dates are for the recomposed
XVIII. XIX is but a brief off-
shoot from XVII dated 840-790
3.¢. XXIII is a line of usurper
kings ruling locally, 776-730 B.c.
XX overlaps late XVIII as re-
composed and was fragmented
after the rule of Rameses IIT.

XXI 710-2 The fragmented rule of XX was
in competition with XXI, com-
posed itself of a dual line of High
Priests ruling from Thebes, the
other at Tanis. Dynasty XXI
soon took over the fragments of
XX. XXII was Assyrian and
competed for control with XXIV,
XXV and early XXVI_

XXVI 663-525 XXIII to XXVI retain the dates
as traditionally held.
