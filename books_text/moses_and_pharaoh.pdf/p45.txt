Population Growth: Tool of Dominion 27

This is the long-term implication of a birth rate below 2.1 children per
woman. It is a birth rate below 2.1 children per woman which alone is
fully consistent with the Bible’s description of the God-hating ethical
rebels: “all they that hate me love death” (Prov. 8:36b). It is this suicidal
birth rate which presently prevails in all Western industrial nations,
This is the population program which Pharaoh hoped to impose on his
enemies, the Hebrews. He was not sufficiently stupid, or so utterly
perverse, to have sought to impose it on his own people.

Pharach saw the necessity of protecting his nations resources from
the prolific Israelites. ‘Three and a half millennia later, fearful and de-
fensive socialists have similar concerns. Bertrand Russell, the British
socialist philosopher and mathematician, saw clearly the dilemma of
socialism: to produce rising per capita wealth, low-productivity social-
ism requires zero population growth. Socialism also still requires the
imposition of harsh penalties against rival populations that continue
to grow, just as it did in ancient Egypt. “Socialism, especially interna-
tional socialism, is only possible as a stable system if the population is
stationary or nearly so. A slow increase might be coped with by im-
provements in agricultural methods, but a rapid increase must in the
end reduce the whole population to penury, and would be almost cer-
tain to cause wars. In view of the fact that the population of France
has become stationary, and the birth rate has declined enormously
among other white nations, it may be hoped the the white population
of the world will soon cease to increase. The Asiatic races will be
longer, and the negroes still longer, before their birth-rate falls suffi-
ciently to make their numbers stable without the help of war and pes-
tilence. But it is to be hoped that the religious prejudices which have
hitherto hampered the spread of birth control will die out, and that
within (say) two hundred years the whole world will learn not to be
unduly prolific. Until that happens, the benefits aimed at by socialism
can only be partially realized, and the less prolific races will have to
defend themselves against the more prolific by methods which are dis-
gusting even if they are necessary. In the meantime, therefore, our so-
cialistic aspirations have to be confined to the white races, perhaps
with the inclusion of the Japanese and Chinese at no distant date.”!9

The more progressive modern socialist ideology appears, the
more satanically backward it becomes..The spirit of Pharaoh still
lives. The anti-dominion defensive spirit of modern socialism has its
roots deep in the past, as well as deep in helt.

19. Bertrand Russell, The Prospects of Industrial Civilization (2nd ed.; London:
George Allen & Unwin, 1959), p, 273. First edition: 1923. He did not change his
views enough to warrant a. revision of this passage.
