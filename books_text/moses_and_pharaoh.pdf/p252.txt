234 MOSES AND PHARAOH

develop his ideas. He has been incomparably diligent in mastering
the scholarly literature relating to these questions. And he has
brought forth a heavily footnoted, self-defeating trilogy, the capstone
of his life’s work. The third volume is devoted to a classic piece of
what he has called “constructivist” rationalism; an_ historically
untested restructuring of the legislative, judicial and executive branches
of civil government, complete with a “model constitution” (chapter
17). Here is “Benthamism” at its utopian worst—the same Ben-
thamism which Hayek has battled against throughout his long
career. 102 In the second volume, he proclaims forthrightly: “, , , we
can always only tinker with parts of a given whole but never entirely
redesign it.”!0% In the third, he proposes a total redesigning of every
nation’s entire institutional system of civil government.

Who Decides?

We are back to the age-old problem: The rule of which law-order?
The rule of how much civil government? Hayek, in secking for formal
rules of civil government—rules that will be applied to all citizens,
irrespective of social or economic position — finds that the humanistic
logic of free market economics cannot be reconciled fully with the
humanistic logic of social and political stability. We are back to
Weber's dichotomy between formal rationalism and substantive ra-
tionalism, The ethics of society supposedly demands that the civil
government intervene in exactly the way that Hayek says is most
dangerous to freedom, namely, to preserve the economic position of a specific
special-interest group. How can this protection be denied to all other
special-interest groups that possess sufficient political power to
rewrite the legislation? This is Hayck’s problem, and he devoted the
second half of his illustrious academic carcer to a study of how fo in-
sulaie a liberal free market society from the effects of the liberal democratic
political order. Human logic did not give him his answer, There are
too many logics, too many ethical views, and no way to make in-
terpersonal comparisons of subjective utility.

Twentieth-century liberal democracy has eroded market free-
dom, Hayek’s eloquent defense of freedom in terms of evolutionary
law and evolutionary morality has not retarded this erosion; if
anything, it has accelerated it. As J. R. Lucas has commented con-

102. Dbid., pp. 19-20,
103. Zbid., p. 25.
