Illegitimate State Power 65

[before] the midwives come into them” (Ex. 1:19).

This passage has bothered far too many orthodox commentators,
One person who could not accept the obvious — that God was pleased
with their successful lie—was John Murray. His chapter on “The
Sanctity of Truth” challenges their actions, just as it challenges
Rahab’s famous lie to the authorities of Jericho concerning the
whereabouts of the Hebrew spies. “Let us grant, however, that the
midwives did speak an untruth and that their reply was really false.
There is sti] no warrant to conclude that the untruth is endorsed, far
less that it is the untruth that is in view when we read, ‘And God
dealt well with the midwives’ (Exodus I: 20). The midwives feared
God in disobeying the king and it is because they feared God that the
Lord blessed them (cf. verses 17, 24). It is not at all strange that their
fear of God should have coexisted with moral infirmity. The case is
simply that no warrant for untruth can be elicited from this instance
any more than in the cases of Jacob and Rahab.”!

Ihave commented elsewhere at some length on the legitimacy of
Jacob's lie to Isaac.? I have also commented on the legitimacy of
Rahab’s lie to the Jericho authorities.? Many of the same arguments
apply here. First, what else could the Hebrew midwives have done
to save the lives of the children, except lie? Second, did Pharaoh
deserve to be told the truth? Did the Nazis in World War II deserve
ta be told where Jews were being hidden? If those Dutchmen or Ger-
mans who hid Jews in their homes to protect them in World War IL
had been approached by the Nazis and asked if they had Jews hidden
in their homes, knowing that all Christians are somehow morally
bound to tell the truth at all times, no matter what, there would have
been a lot of condemned Christians and captured Jews. Silence
under such circumstances would have been regarded as an admis-
sion of guilt, and searches would have been conducted. Third, what
is spying, other than a lie? (This is why the rules of Western warfare
sanction the execution of spies during wartime, but men who are
captured in foreign territory wearing their nation’s uniform are sup-
posed to be treated as prisoners of war.) Fourth, what is wartime

1. John Murray, Principles of Conduct: Aspects of Biblical Ethics (Grand Rapids,
Michigan: Eerdmans, 1957), pp. 141-42.

2. Gary North, The Dominion Covenant; Genesis (Tyler, Texas: Institute for Chris-
tian Economics, 1982), ch. 19: “The Uses of Deception.”

3. Gary North, “Appendix 5,” in R. J. Rushdoony, The Institutes of Biblical Law
(Nutley, New Jersey: Craig Press, 1973), pp. 838-42. Cf. Jim West, “Rahab’s
Justifiable Lie,” Christianity and Civilization, 2 (Winter 1982-83).
