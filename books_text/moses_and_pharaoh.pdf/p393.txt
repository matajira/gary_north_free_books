The Labyrinth and the Garden 375

entry of the child into the womb for birth. . . . It is obvious that the
idea of death-and-rebirth, rebirth through ritual and with a fresh
organization of profoundly impressed sign stimuli, is an extremely
ancient one in the history of culture. . . .”31

The Garden

We know that the garden of Eden was placed eastward in Eden
(Gen. 2:8), When God closed its entrance by placing the cherubim
and the flaming sword, they were stationed at the east of the garden
(Gen. 3:24). This means that the garden itself must have been an
enclosed space. Its walls protected it from those who would enter it
on any terms except those established by God. Man was not to gain
access tothe tree of life by breaking into the closed space (Gen. 3:22,
24). From the day of man’s expulsion, the only way to the tree of life
is through ethical conformity to God’s standards. The standard is
perfection. Regeneration, not the scaling of physical or symbolic
walls, is the foundation of eternal life.

The walled enclosure is therefore a significant design — one of the
most significant in man’s history. Pagan cultures again and again
return to it. They invest it with many meanings. The heart of the
pagan version, the labyrinth, is a closed space in which a winding
pathway is dominant. Men who do not know the secret of the path-
way are trapped, condemned to wander helplessly. Only by knowing
the way out (or in) through the entrance can man attain his eternal
goal. The walls, however, cannot be scaled by man.

Egypt as Garden-Labyrinih

Egypt was a symbol of the garden. In Abraham’s day, it was the
place that was spared during the famine (Gen. 12:10). Abraham
journeyed to Egypt, but then went back up into Canaan, This was a
symbolic resurrection. This was true again in Joseph's day: descent into
a “garden,” which became a labyrinth or wilderness, and then
escape, Jacob wanted his body to be taken up out of Egypt after his
death, and buried in the cave in Canaan in which his fathers were
buried (Gen. 49:29-32). Joseph wanted his bones dug up and
reburied in the promised land (Gen. 50:25). Again, this points to
resurrection. So does Joseph’s experience in Potiphar’s house (the
“garden”) and his experience in prison (“wildemness-labyrinth”), a

31. Ibid., pp. 65-66.
