22 MOSES AND PHARAOH

The 2.5 Million Hebrews

The standard estimation of how many people left Egypt at the
Exodus is 2-2.5 million Hebrews, not counting the “mixed multi-
tudes.” Why is this figure reasonable? The best answer relates to the
number of Hebrews a generation later, after the deaths of all of the
members of the adult Hebrews who fled, with only two exceptions:
Joshua and Caleb.

The generation in the wilderness entered Canaan with approx-
imately the same number of men who had left Egypt 40 years earlier.
There were 600,000 men who left Egypt (Ex. 12:37), and one year
later (Num. 1:1), there were 603,550 fighting men (Num. 1:46), plus
22,273 Levites (Num. 3:43). The number of adult males was only
slowly increasing. When the second census was taken before they
entered the land, 40 years later, the population of the tribes had
decreased slightly, to 601,730 (Num 26:51), plus 23,000 Levites
(Num. 26:62).

What this points to is population stagnation. More important, it
points to at least two generations of stable reproduction: one male
child and one female child per family. Why do I say this? Because
populations that are growing experience the after-effects of prior
high birth rates, cven in later periods when the birth rate in the soci-
ety falls below the bare minimum reproduction rate of 2.1 children
per woman. This is what most Western industrial nations are facing
today: birth rates below the reproduction rate. Neverthcless, the
populations are still growing. The reason is that in previous periods,
there were higher birth rates, and young women born up to 45 years
earlier are still in the child-bearing ages. As these women marry and
begin to have children, the upward curve of population continues to
rise, although it is slowing down. Women may be having fewer chil-
dren than their mothers did, but there are lots of women still within
or entering the child-bearing ages. It takes decades of below-
reproduction-rate births to begin to bring down the aggregate
number of people in a society, as middle-aged women cease having
children, and the very old members of society continue to die off.

What is abnormal at any time in history is for a population to re-
main stable for a full generation. A steady-state population is far
more common on islands or in very small nations, where emigration
is possible or where abortion or even infanticide is practiced as a
means of population control, In the ancient world, steady-state
