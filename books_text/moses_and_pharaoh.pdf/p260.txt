242 MOSES AND PHARAOH

surely as any other aspect of man’s personality. To believe in natural
law is to believe in natural reason; to believe in natural reason, or
“reason rightly understood,” is to believe in the primacy of the
intellect.!1* The Bible does not teach the primacy of the intellect; it
teaches the primacy of faith.

When men say that “the Bible is not in contradiction to scientific
truth,” they generally mean that the Bible is not in opposition to the
discoveries of the autonomous human intellect. This statement is ab-
solutely false, if such a conclusion is intended. The Bible is absolutely
opposed to such universally proclaimed “scientific truths” as evolution
of one species into another through natural selection, or the eternal-
ity of matter-energy, or the doctrine of uniformitarian change. What
a consistent Christian must maintain is this: “The Bible is the foun-
dation for human reason, and no conclusion of the human mind
which is in opposition to revealed truth in the Bible is scientifically
valid,” The truths of science, if they are to be accepted, must be in
conformity to biblical revelation. There are not two truths —“natural
science” and biblical revelation —but rather one truth: science én con-
formity to biblical revelation. And what is true of natural science ~ the
man-interpreted regularities of the observed universe — must also be
true of man’s speculation in other areas of life.

We dare not say that “natural law” and biblical revelation are the
same because the truths of human jurisprudence are “naturally” in
conformity to biblical revelation. We must say, on the contrary, that
the truths of human jurisprudence, insofar as they are true, are true
in spite of man’s twisted, rebellious, “natural” reason. This is why the
Bible required that Israel assemble once every seven years to listen
to the reading of the law of God.

Conclusion

The doctrine of the rule of law is distinctly biblical in origin. A
law-order which is universally binding on all men is an idea which
had its origins in biblical religion. The same God who judges all men

113. The rationalist apologetic methodology of “old Princcton” Seminary is repre-

sentative of this error, Benjamin B. Warfield wrote of “irrational faith, that is, a faith
without grounds in right reason.” Warficld, “Introduction to Francis R. Beattie’s
Apologetics” (1903); reprinted in John E. Meeter (ed.), Selected Shorter Writings of Ben-
jamin B, Waficld—1I (Nutley, New Jersey: Presbyterian & Reformed, 1973), p. 98.
He implicitly contrasted such a faith with Christianity, which apparently is a faith
which és grounded in “right reason.” The problem, of course, is that nobody agrees
about the content or standards of “right reason.”
