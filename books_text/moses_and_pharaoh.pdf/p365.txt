The Demographics of Decline 347

Soviet law toward the family, including a 1934 law against homosex-
uality.”* The anti-abortion law remained on the books until 1955.

Between 1955 and 1965, the total number of legal and illegal
abortions increased by a factor of four, according to published Soviet
estimates.’5 A debate over the theory of overpopulation began in the
USSR in 1965 and 1966, which indicated a weakening of the older
“hard line” position.76 A national network of abortion clinics was in
operation by the mid-1970’s which offered cheap abortions at a price
of around $7 each.?” Some 8 million abortions were being performed
annually by this time.?® Abortion became the major form of Soviet
population control, three to one over contraception.79

The rulers of the Soviet Union now face the demographic and
political problem of a stagnant “white Russian” (European) popula-
tion which confronts a growing Muslim and Central Asian. Soviet
population. “Soviet demographers expected the 1970 census to pro-
duce a figure of over 250 million, with a projection of 350 million by
the end of the century. In fact the 1970 total fell 10 million short and
the 1979 figure produced only 262,436,000, meaning a population of
not much over 300 million in 2000 a.p... What the 1970 census re-
vealed for the first time was a dual birth-rate: low in Slavic and
Baltic Russia, high in the eastern USSR, Central Asia and the
Caucasus. In the 1960s alone the Muslim population leapt from 24 to
35 million, adding another 14 million in the 1970s, giving a total of
about 50 million by the beginning of the 1980s. By this point it was
cléar that at the turn of the century Central Asia and Caucasia
would contribute about 100 million, that is a third, of the total. Even
by 1979, the 137 million Great Russians, a markedly ageing popula-
tion compared to the non-Slavs, felt demographically on the defen-
sive.”8° God will not be mocked!

Abortion Worldwide

The extent of abortion worldwide is, from a biblical standpoint,
horrendously large. A U.S. government publication cites estimates

74, Geiger, Family p. 94.

75. Gail Warshoisky Lapidus, Women in Soviet Society: Equality, Development, and
Social Change (Berkeley: University of California Press, 1978), p, 299, note 25.

76. Pryde, pp. 167-68.

77, Sexual Revolution in Soviet [sic] Straining Strict Morality,” New York Times
(Sept. 25, 1977).

78, Lapidus, p. 299.

79. Idem., note 25.

80. Paul Johnson, Modem Times, p. 711-12.
