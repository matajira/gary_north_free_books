The Demographics of Decline 329

nant which he sware unto thy fathers, as it is this day” (Deut. 8:18),
God’s covenant establishes the possibility of positive feedback, or what
ts also called compound growth.

Population growth is specifically stated to be a covenantal bless-
ing. To deny this is to deny God’s word. There can be no com-
promise here, Therefore, we should expect to find evidence that
population growth is, én the dong run, accompanied by other economic
benefits. Contrary to the assertion of the rebellious former slaves of
the wilderness era (Ex, 14:11-12; 17:1-3; Num. 20:3-4), God does not
bring His people out into the wilderness to kill them. Contrary to the
handwringing of ethically rebellious slaves of our day, God does not
multiply the seed of righteous mankind in order to bring a popula-
tion catastrophe upon them.

Simon's Thesis

Professor Julian Simon of the University of Illinois has written a
devastating book, published by Princeton University Press in 1981,
called The Ultimate Resource. What is this resource? Human creativ-
ity. Simon examined the statistical and theoretical evidence of the
various “doomsday books” published around the world, but especially
in the United States, after 1964. He found all of them to be mislead-
ing prophecies: the coming famine, the coming pollution catastrophe
(dead seas, dead lakes, cancer-producing air), the population explo-
sion, the coming extinction of natural resources (especially “non-
renewable” resources), the energy crisis, and the economic collapse.

What is the evidence? That food is getting cheaper, and has been
for centuries under capitalism. That “non-renewable” resources aren’t
non-renewable, except fossil energy, which is only one among several
energy sources, and even here, there is an ample supply for centuries.
That economic catastrophes do happen, but in the modern world they
are almost always the product of government planning and
mismanagement. That an increasing population, if coupled with
capitalist institutions, has invariably brought with it economic ad-
vance and an increasing per capita income within two generations
and often within one generation. The problem, he says, is not that
Western populations arc increasing, but rather that Westerners are not
reproducing themselves. Birth rates in many Western nations are below
the reproduction rate of 2.1 children per woman. What the bulk of the
historical evidence points to is that shrinking populations bring with
them economic stagnation and declining per capita income.
