226 MOSES AND PHARAOH

become autonomous law-givers. “Unfortunately,” writes Tullock,
“although legislatures realized a long time ago that they were writing
new law, the courts have only very, very gradually come to the
realization that they are doing the same thing. Further, when they
did realize sometime in the nineteenth century, that they were
writing new law, they continued making their decisions retroactive.
It is only in the past ten years that the U.S. Supreme Court has
begun to act as if it realized it was making retroactive decisions. Up
to that time, the Court had always acted as if any decision was the
discovery of a preexisting law rather than the formulation of new
Jaw, although surely judges were aware of the hypocrisy of this posi-
tion for a least a hundred years.”?5

  

Democracy vs. Bureaucracy

Thus, with the abandonment of faith in revealed law that is open
to both judge and jury, citizen and legislature, humanistic civil law
has become perverse. Judges instruct juries to decide only in terms
of the facts, not the validity of the law, when in fact the juries unques-
tionably have the authority and the power to interpret and apply
both. Legislatures write new legislation, but they cannot easily
preserve their own sovereignty; bureaucracies “interpret” these laws
and are nearly autonomous in applying the laws in whatever way
they want, in the name of the legislatures. Elitist law dominates. No
layman is supposed to be able to understand the law. He must
become subservient to the experts. Democracy, which is supposedly
the process of widening the franchise and widening the base of sover-
eignty, becomes progressively bureaucratic and elitist. Increasingly,
clitist rule is governed by the principle of secrecy, or to reverse Presi-
dent Wilson’s dictum, of “closed covenants secretly arrived at.”

Weber has descrihed the process well: “Every bureaucracy seeks
to increase the superiority of the professionally informed by keeping
their knowledge and intentions secret. Bureaucratic administration
always tends to be an administration of ‘secret sessions’: in so far as it
can, it hides knowledge and action from criticism. . , . Political par-
ties do not proceed differently, in spite of all the ostensible publicity
of all Catholic congresses and party conventions. With the increas-
ing bureaucratization of party organizations, this secrecy will prevail

75, Tullock, “Courts as Legislatures,” in Cunningham (ed.), Liberty and the Rule of
Law, p. 133.
