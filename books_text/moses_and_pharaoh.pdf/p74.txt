56 MOSES AND PHARAOH

taining the desired results with the smallest possible expenditure.
But to do this it would have to be able to make calculations. And
such calculations must be calculations of value. They could not be
merely ‘technical, they could not be calculations of the objective use-
value of goods and services. . . . The economic administration may
indeed know exactly what commodities are needed most urgently.
But this is only half the problem. The other half, the valuation of the
means of production, it cannot solve. It can ascertain the value of
the totality of such instruments. That is obviously equal to the
satisfactions they afford. If it calculates the loss that would be incur-
red by withdrawing them, it can also ascertain the value of single in-
struments of production. But it cannot assimilate them to a common
price denominator, as can be done under a system of economic free-
dom and money prices.”8

Mises was too generous here to his ideological opponents, the so-
cialists. Unless the State is defined as the desires of one man — which
is what the Pharaoh could claim — it is not possible for socialist plan-
ners to “know exactly what commoditics are needed most urgently.”
They cannot possibly ascertain “the value of the totality of such in-
struments,” precisely because no planning agency can ever estimate
“the satisfactions they afford.” The satisfactions afforded to a
mullitude of citizens by any single mix of consumer goods cannot
possibly be known; they can only be guessed at. Furthermore, there is
no way for the sactalist planners to judge the failure of their estimations, outside
of massive revolution by the victimized consumers — a contingency made less
likely by the systematic repression by the police and military leaders
of most socialist commonwealths, They know that they cannot possi-
bly make such calculations accurately, and so they spend great quan-
tities of sorely needed capital on the suppression of potentially
violent consumer dissatisfaction.

Rothbard’s summary of Mises’ argument is illuminating: “In
short, if there were no market for a product, and all of its exchanges
were internal, there would be no way for a firm or for anyone else to
determine a price for the good. A firm can estimate an implicit price
when an external market exists; but when a market is absent, the
good can have no price, whether implicit or explicit. Any figure
could then be only an arbitrary symbol. Not being able to calculate a
price, the firm could not rationally allocate factors and resources

8. Ludwig von Mises, Socialism, pp. 120-21.
