370 MOSES AND PHARAOH

ritual occasions and slain there.* Ritual sacrifice and dancing were
connected to the bull-god in the Osirian religion, as was also the case
in the legend of Crete’s King Minos, the deadly Minotaur (“Minos-
bull”), and the sacrifices by Minos of the Athenian youths (the legend
of Theseus, Ariadne, and Daedalus).> Similar links between dancing
and labyrinths also exist in Cornwall, England and also in Scandin-
avia and Northern Russia.¢ It is a dance of death and resurrection.’

Hooke points out that it was very early in the Egyptian
dynasties, in the second dynasty; that the kings began to protect
their burial places. This had not been true earlier. “Some of these
chambers contained the bodies of those who accompanied the king to
the after-life, his women and his body-guard who were killed and
buricd at the time of the royal funeral.”* The tomb of King Perabsen
of the second dynasty was surrounded by a passage, a new feature,
according to Flinders Petrie, who excavated it.? But as Hooke says,
there secms to have been no social or military reason for hiding the
bodies of the kings.!° The pyramids were an extension of this desire
to protect the bodies of the monarchs.

The motivation was religious and ritualistic, not defensive.
Hooke writes: “While pyramid and temple must be considered as
one complex building, the internal construction of the pyramid
became elaborate and labyrinthine in character. Nevertheless, the
labyrinth name became attached to the temple, and it seems proba-
ble that the greater importance of the temple as the place of ritual,
associated with Osiris, Amon or Re, as the case might be, would ac-
count for this. In the pyramid, and later in the rock-cut tombs, the
body of the king was sealed up in his sarcophagus, and the entrances
were blocked up and concealed. Nothing more happened there. But
in the adjoining temple everything necessary for his welfare in the
after-life was attended to. The plan and construction of the oldest
known temple of Osiris at Abydos is interesting, therefore, in con-
nection with the original meaning of the Labyrinth. Especially as
[citing Budge] ‘it is probable that there was a small temple of Osiris

 

4, Ibid., pp. 7, 22-24.

5. Ibid., pp. 24-27.

6. Hans Georg Wunderlich, The Secret of Crete (New York: Macmillan, 1974), p. 289.

7. Robert Graves, The White Goddess: A Historical Grammar of Poetic Myth (rev. ed;
New York: Farrar, Straus and Giroux, 1966), pp. 329-30.

8, Hooke, op. cit, p. Ul.

9, Ibid., p. 12.

10, Bid, p. 13.
