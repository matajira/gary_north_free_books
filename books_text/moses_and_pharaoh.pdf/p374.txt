356 MOSES AND PHARAOH

ethics of sentiment in our day: humanism, “Humanism thrives on sen-
timentality because few religions are more dishonest in their doc-
trinal expressions. Unable to withstand dispassionate analysis,
which would reveal its lack of foundation, it stresses feeling rather
than thought. That is what makes sentimentality so vicious.”!!9

The incomparable hypocrisy of D. Gareth Jones, Ph.D, is found
in the closing paragraph of this chapter: “Decisions relating to the
handicapped should always be difficult and will prove too onerous
for some to bear. This is the knife-edge along which we walk. But as
we do we should be encouraged by the prophecy of Isaiah that, ulti-
mately, ‘then will the eyes of the blind be opened and the ears of the
deaf unstopped. Then will the lame leap like a deer, and the tongue
of the dumb shout for joy’ (Isaiah 35:5-6).”!20 Not if their parents aboried
them, they won't.

‘Jones says, “There is no slick solution.”!?! Oh, but there is. The
slickest of all is the saline solution. It is this solution which burns the
unborn to death. But the heat of such solutions is nothing compared
to the heat which awaits the biomedical practitioners of abortion and
their morally corrupt apologists. Also, the publishers of their tracts.

We can understand Franky Schaeffer’s outrage at Inter-Varsity
Press. He went on Pat Robertson’s 700 Clué television show in the
summer of 1984 and called attention to what should be obvious,
namely, that it was the income from his father’s books which created
the economic base of Inter-Varsity Press in the late 1960's, and now
they are using that capital base to spew out books like Ron Sider’s
Rich Christians in an Age of Hunger and Brave New People,

I agree entirely with his response to all the gibberish about com-
plex moral issues. “The real issue is simple. What do we do now? It is a
choice, not between competing slogans and word games, right or
left, but between godlessness and godliness. Between inhumanity and
humanity. Between life and death. Between Joseph Fletcher!?? and
Jesus. Between the dignity of the individual (whether handicapped,
unwanted, born or unborn) and death as a ‘liberal’ solution for social
problems such as poverty, race, and medical costs. Between a sanctity

119. Dbid., p. 46.

120. Jones, p. 184.

121, ‘eid., p. 169.

122, Joseph Fletcher, Situation Ethics: The New Morality (Philadelphia: Westminster
Press, 1966).
