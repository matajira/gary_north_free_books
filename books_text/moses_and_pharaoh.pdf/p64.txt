46 MOSES AND PHARAOH

members of which half on an average were in session every working day.
‘There were ten thousand officials within the country or outside, five hun-
dred wardens of arsenals, etc. Thus public affairs did not merely demand
the intermittent presence of all the citizens of the Assembly; they required
besides the constant exertions of more than a third of them.**

Consider it: one-third of all the estimated 35,000 to 44,000 resi-
dent male citizens of Athens in the year 431 B.c. were in State ser-
vice.*5 At least 20,000 were “eating public bread,” meaning that they
were either on the payroll or on the dole.** The legend of Pericles,
the legend of Athenian democracy, and the legend of Olympus con-
stitute the basis of the legend —a Renaissance legend— of the glory
that was Athens and the greatness that was Greece. It is the most en-
during of all Greek myths.

The Denial of Time

What kind of society was early dynastic Egypt, the Egypt of the
Pyramid Age? Mumford’s words ring true: “Bureaucratic regimen-
tation was in fact part of the larger regimentation of life, introduced
by this power-centered culture. Nothing emerges more clearly from
the Pyramid texts themselves, with their wearisome repetitions of
formulae, than a colossal capacity for enduring monotony: a capac-
ity that anticipates the universal boredom achieved in our own day.
Even the poetry of both carly Egypt and Babylonia reveal this
iterative hypnosis: the same words, in the same order, with no gain
in meaning, repeated a dozen times —or a hundred times. This ver-
bal compuisiveness is the psychical side of the systematic compulsion
that brought the labor machine into existence. Only those who were
sufficiently docile to endure this regimen at every stage from com-
mand to execution could become an effective unit in the human
machine.”*? The culture denied linear time. It substituted endless
repetition for progress, monotony for hope.

While the recentralization of power by the Pharaoh of the op-
pression did not revive the enormous capital outlays of the Pyramid
Age, it did reflect more accurately than feudalism Egypt’s theology
of the continuity of being. Ii did establish slavery, and it did involve

44, Ibid., p. 126.

45. Alfred E. Ziramern, The Greek Commonwealth: Potitics and Hconomics in Fifth-
Century Athens (Oxford: At the Clarendon Press, 1915), p. 172.

46. Ibid., pp. 172-73.

47. Mumford, “Megamachine,” p. 266.
