The Demographics of Destine 363

minate all those who preferred the latter form of government, and
that the former could only become democratic by the destruction of
luxury and riches, which form the support of royalty; that equality
would never be anything but a chimera as.long as men did not all en-
joy approximately equal properties; and finally, that such an order of
things could never be established until a third of the population had
been suppressed, . . .”!#?

The most impressive testimony came from Gracchus Babeuf, the
communist revolutionary who became a model for Karl Marx.'**
Babeuf and his disciple Buonarroti were the great promoters of the
religion of revolution. “May cverything return to chaos,” Babeuf
wrote in his Plebetan Manifesto, “and out of chaos may there emerge a
new and regenerated world.”'** In 1795, Babeuf gave this account of
Robespierre’s depopulation scheme in his tract, “Sur le Systeme de la
Depopulation, ou La Vie ct tes Crimes de Carrier’: “Maximilien
{Robespierre] and his council had calculated that a real regeneration
of France could only be operated by means of a new distribution of
territory and of the men who occupied it... . He thought that...
depopulation was indispensable, because the calculation had been
made that the French population was in excess of the resources of the
soil and of the requirements of useful mdustry, that is to say, that,
with us, men jostled each other too much for each to be able to-live at
ease; that hands were too numerous for the execution of all works of
essential utility... .”145

Nesta Webster's analysis of the reasons lying behind the social-
ists’ call for systematic depopulation, written in 1919, is as relevant
today —the era of the population hysteria—as it was when she wrote
it: “But could a nation of 25,000,000 be thus transformed? To the
regenerators of France it seemed extremely doubtful; already the
country was rent with dissentions, and any scheme for universal
contentment seemed impossible of attainment. Moreover, the plan
of dividing things up into cqual shares presented an insuperable
difficulty, for it became evident that amongst a population of this size
there was not enough money, not enough property, not enough

142. Ibid., pp. 424-25.

143, Karl Marx and lrederick Engels, “Manifesto of the Communist Party”
(4848), in Marx and Engels, Selected Works, 3 vols. (Moscow: Progress Publishers,
[1969] 1977), 1, p. 134. Cf. George Lichtheim, Marsixm: An Historical and Critical Study
(New York: Praeger, 1963), pp. 61, 89.

144, Cited in Billington, Fire in the Minds of Men, p. 75.

145. Cited in Webster, p. 425,
