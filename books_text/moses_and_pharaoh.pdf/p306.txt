288 MOSES AND PHARAOH

self-restraint of government officials in not burdening the society
with a massive, incomprehensible structure of administrative law.
When civil law reaches into every aspect of the daily lives of men,
the State loses a very important subsidy from the public, namely,
men’s willingness to submit voluntarily to the civil law. Any legal
structure is vulnerable to the foot-dragging of the public. If men
refuse to submit to regulations that cannot be enforced, one by one,
by the legal system, then that system will be destroyed. Court-
jamming will paralyze it. This is a familiar phenomenon in the
United States in the final decades of the twentieth century.

It is possible to bring down any legal system simply by taking ad-
vantage of every legal avenue of delay, Any administrative system
has procedural rules; by following these rules so closely that action
on the part of the authorities becomes hopelessly bogged down in red
tape (procedural details), the protestors can paralyze the system.
Too many laws can produce lawlessness. The courts can no longer
enforce their will on the citizens. At the same time, administrative
agencies can destroy individual citizens, knowing that citizens must
wait too long to receive justice in the courts. The result is a combina-
tion of anarchy and tyranny—the antinomian legacy.

Bukousky’s Protests

This possibility of bureaucratic paralysis is characteristic of al}
administrative systems, even a centralized tyranny such as the
Soviet Union. A classic example is the case of the Soviet dissident of
the 1960's and early 1970's, Vladimir Bukovsky, Bukovsky spent well
over a decade in the Soviet gulag concentration camp system. He
was arrested and sentenced in spite of specific civil rights protections
provided by the Soviet Constitution—a document which has never
been respected by the Soviet bureaucracy. But once in prison, he
learned to make life miserable for the director of his camp. He
learned that written complaints had to be responded to officially
within a month. This administrative rule governing the camps was
for “Western consumption,” but it was nevertheless a rule. Any camp
administrator who failed to honor it risked the possibility of punish-
ment, should a superior (or ambitious subordinate) decide to
pressure him for any reason. In short, any failure to “do it by the
book” could be used against him later on.

Bukovsky became an assembly-line producer of official protests.
By the end of his career as a “zek,” he had taught hundreds of other
