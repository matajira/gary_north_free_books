14
THE RULE OF LAW

And when a stranger shall sojourn with thee, and will keep the passover
to the Lorn, let all his mates be circumcised, and then let him come near
and keep it; and he shall be as one that is born in the land: for no uncir-
cumcised person shall eat thereof. One law shall be to him that is
homeborn, and unto the stranger that sojourneth among you (Ex.
12:48-49),

The Passover was closed to outsiders who had not been circum-
cised. A man’s slave, if he had been purchased with money and
subsequently circumcised, had the obligation of participating in the
Passover rites (Ex. 12:44). He had a place in the family and was
under the sovereignty of God, through his master. The mark of
subordination was in his flesh. In contrast, the foreigner and hired
servant were excluded from the Passover, since they had not visibly
(physiologically) humbled themselves before God, and were there-
fore not part of the covenant: “There shall no stranger eat thereof:
But every man’s servant that is bought for moncy, when thou hast
circumcised him, then shall he eat thereof. A foreigner and an hired
servant shall not eat thereoi” (12:43b-45). No foreigner could eat
leavened bread anywhere in Israel during Passover week (12:19).
Any stranger who wished to participate in the Passover could do so,
if he and all the males of his household were circumcised (12:48).
This indicated his subordination to God and His dominion in the name
of God over his own household. A circumcise
treated as one born in the land, although it is unclear how he could
ever have owned land permanently because of the redistribution
back to the original family owners which was required at the Jubilee
year (Lev. 25:10),

The rites of circumcision and Passover were simple enough.
Biblical law in Israel was public. In fact, it had to be read in its en-

188

 

stranger was to be
