Separation and Dominion 137

Ancient Humanism’s Separation

Basic to many of the ancient cultures was the distinction between
“the people,” the group to which a citizen belonged, and “the others,”
or “barbarians,” who were outside the covenantal membership.
Egypt was no exception. Wilson comments: “In their feeling of
special election and special providence, the Egyptians called them-
selves ‘the people’ in contrast to foreigners.”> So deeply embedded in
Greek and Roman thought was the division between peoples, that
classical legal theory recognized no common law within the city.6
“No one could become a citizen at Athens,” writes Fustel de Coulanges,
“if he was a citizen in another city; for it was a religious impossibility
to be at the same time a member of two cities, as it also was to be a
member of two families. One could not have two religions at the same
time. . . . Neither at Rome nor at Athens could a foreigner be a pro-
prietor, He could not marry; or, if he married, his marriage was not
recognized, and his children were reputed illegitimate. He could not
make a contract with a citizen, at any rate, the law did not recognize
such a contract as valid. . . . The Roman law forbade him to inherit
from a citizen, and even forbade a citizen to inherit from him, They
pushed this principle so far, that if a foreigner obtained the rights of a
citizen without his son, born before this event, obtaining the same
favor, the son became a foreigner in regard to his father, and could not
inherit from him, The distinction between citizen and foreigner was
stronger than the natural tie between father and son.”?

There was also the linguistic difference. The very term “bar-
barian” has its origins in Greek grammar. The Greeks spoke Greek,
of course, while foreigners’ languages all sounded like “bar bar”
incoherent, in other words. This, at least, is the standard explana-
tion of the term, and it is repeated by the influential British historian
of classical culture, H. D. F. Kitto, in the introduction to his book,
The Greeks (1951). Both Kitto and C. M. Bowra argue that “barbar-
ian” did not have a pejorative sense in Homer, but later the term
came to mean inferior status.? Gilbert Murray, whose Five Stages of

5, John A. Wilson, The Burden of Egypt: An Interpretation of Ancient Egyptian Culture
(University of Chicago Press, [1951] 1967), p. 112.

6. Fustel de Coulanges, The Ancient City: A Study on the Religion, Laws, and Institu-
tions of Greece and Rome (Garden City, New York: Doubleday Anchor, [1864] 1955),
Bk. IIE, ch. XI, pp. 192-93.

7, Ibid. Bk. IIT, ch. XI, pp, 196-97.

8. H. D. EF Kitto, The Greeks (Baltimore, Maryland: Penguin, [1951] 1962), pp.
8-10; C. M. Bowra, The Greek Experience (New York: Mentor, [1957] 1964), p. 26.

 
