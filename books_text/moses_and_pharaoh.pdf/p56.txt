38 MOSES AND PHARAOH

from its own weight and inability to generate productive resources, Both
events took place in Egypt: an early disintegration into feudalism,
and then a revival of centralization during what conventional histor-
ians call the Twelfth Dynasty (from Joseph to the fleeing of Moses),
which was followed by national defeat immediately after the Exodus.
Then, after a century or more under the Hyksos (Amalekites), Egypt
experienced a brief rise of power under the Eighteenth Dynasty,?¢ and
then further decline.

Egypt could not throw off the static rule of the pharaohs, for the
Egyptians remained faithful to their monophysite theology, the con-
tinuity of being. The only major change, late in Egyptian history,
long after the Exodus, was an extension of the process of divinization
to the common man, so that he, too, might become Osiris after his
death, as the pharaohs had before him,?” Egyptian culture was re-
markably stable; it was the longest-lived of all the ancient kingdoms,
but it was “life through institutional death.” E. O. James is correct
when he refers to Egypt’s characteristic feature as the cudé of the dead,
one which assumed “gigantic proportions.”2* The pyramids are the
most visible, most impressive, and most representative monuments
to Egyptian religion and society. (The other major Egyptian design
was the labyrinth, discussed in greater detail in Appendix C: “The
Labyrinth and the Garden.”)

The Cult of the Dead

The Egyptian cult of the dead was, in fact, a religion of death and
rebirth. It was also a fertility cult. The voluminous and painstaking
researches of E. A. Wallis Budge in the early years of the twentieth
century have made this clear. “The central figure of the ancient
Egyptian Religion was Osiris, and the chief fundamentals of his cult
were the belief in his divinity, death, resurrection, and absolute con-
trol of the destinies of the bodies and souls of men. The central point

26. Velikovsky argues cogently that the Egyptian king Shishak, mentioned in IL
Chronicles 12:2-4, was Thutmose III of the Eighteenth Dynasty: Velikovsky, Ages in
Chaos (Garden City, New York: Doubleday, 1952), pp. 152-55. The invasion of Israel
by Shishak was in the fifth year of King Rehoboam, or about 925 B.c., according to
Thick’s chronology: Edwin R. Thiele, A Chronology of the Hebrew Kings (Grand
Rapids, Michigan: Zondervan, 1977), p. 75. This dating, of course, is over 500
years after the conventional dating of Thutmose IIT’s dynasty, which is commonly
placed in the early or mid-fifteenth century, 8.c.

27. James, Ancient Gods, p. 61.

28. Tid, p. 57.
