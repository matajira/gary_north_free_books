Honoring God’s Law by Disobeying Evil Humanist Laws 73

geous the civil government appears in the eyes of the public. Thus,
one of the key tactics of the protesters is to provoke the fanaticism
of the authorities by quiet, prayerful civil behavior. It is the civil
government that must be seen by the public as uncivil.

Gandhi mobilized people to march peacefully against the authori-
ties, Wave upon wave of them marched, and were cut down by the
clubs of the soldiers or police. This creates a loss of morale in the
hearts of the righteous police, and an escalating fury in the hearts
of the unrighteous police. Both reactions benefit the long-term
goals of the protesters.

The fanaticism of the protester must be the fanaticism of relent-
less perseverance. The protesters simply refuse to go away. Wave
upon wave of them come to confront the clubs of the civil govern-
ment. The theological doctrine that is the foundation of this strat-
egy is called the perseverance of the saints. It is the fifth point of
the biblical covenant model: continuity. What is hard on one’s
cranium is good for one’s soul, and also good for the righteous
cause.

Counting the Cost

Nevertheless, before getting involved in such a risky and po-
tentially painful protest movement, the prospective protester should
first count the cost. So should the organizers.

For which of you, intending to build a tower, sitteth not down.
first, and counteth the cost, whether he have sufficient to finish it?
Lest haply [it happen], after he hath laid the foundation, and is
not able to finish it, all that behold it begin to mock him, This man
began to build, and was not able to finish. Or what king, going to
make war against another king, sitteth not down first, and consul-
teth whether he be able with ten thousand to meet him that cometh
against him with twenty thousand? Or else, while the other is yet
a great way off, he sendeth an ambassage [ambassador or represen-
tative], and desireth conditions of peace. So likewise, whosoever
he be of you that forsaketh not all that he hath, he cannot be my
disciple (Luke 14:28-33).

There should first be an assessment of the enemy’s counter-
