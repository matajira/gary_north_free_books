CONCLUSION

Ye are the salt of the earth: but if the salt have lost his savour,
wherewith shall it be salted? it is thenceforth good for nothing, but to
be cast out, and to be trodden under foot of men (Matthew 5:13).

For every one shall be salted with fire, and every sacrifice shall be
salted with salt. Salt is good: but if the salt have lost his saliness,
wherewith will ye season it? Have salt in yourselves, and have peace
one with another (Mark 9:49-50).

Salt is good: but if the salt have lost his savour, wherewith shall
it be seasoned? It is neither fit for the land, nor yet for the dunghill;
but men cast it out. He that hath ears to fear, let him hear (Luke
14:34-35).

What is salt good for? Three things. First, it adds flavor to
food. Second, it serves as a preservative. Both of these uses are
blessings. Third, it destroys the productivity of the land. “And
that the whole land thereof is brimstone, and salt, and burning,
that it is not sown, nor beareth, nor any grass groweth therein, like
the overthrow of Sodom, and Gomorrah, Admah, and Zeboim,
which the Lorn overthrew in his anger, and in his wrath” (Deuter-
onomy 29:23). It is therefore a tool of judgment: God’s negative
sanction. Salting over a city was a strategy of military conquest in
the ancient world. “And Abimelech fought against the city all that
day; and he took the city, and slew the people that was therein,
and beat.down the city, and sowed it with salt” (Judges 9:45).

Most people think of the first use of salt. A few may think of

120
