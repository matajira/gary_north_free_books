Covenant-Breaking and Social Discontinuity 105

The Perseverance of the Saints

A saint, if you remember, means someone who has access to
God’s holy (set apart) sanctuary. Through prayer, formal worship,
and the sacrament of communion (the Lord’s Supper), the individ-
ual Christian gains. entrance into the very throne room of God.
He becomes a counsellor io God, just as Moses was a counsellor.
Through prayer, the saint counsels God. He offers suggestions.
Moses’ example is representative of what it is we are to do. Moses
the counsellor challenged God not to do what He said He would
do, namely, destroy the Israelites in the wilderness.

And Moses besought the Lorn his God, and said, Loan, why
doth thy wrath wax hot against thy people, which thou hast
brought forth out of the land of Egypt with great power, and with
a mighty hand? Wherefore should the Egyptians speak, and say,
For mischief did he bring them out, to slay them in the mountains,
and to consume them from the face of the earth? Turn from thy
fierce wrath, and repent of this evil against thy people. Remember
Abraham, Isaac, and Israel, thy servants, to whom thou swarest
by thine own self, and saidst unto them, I will multiply your seed
as the stars of heaven, and all this land that I have spoken of will
1 give unto your seed, and they shall inherit it for ever (Exodus
32: 11-13),

The significant fact here is that God listened to Moses’ counsel
and heeded it. “And the Lorp repented of the evil which he
thought to do unto his people” (Exodus 32:14).

‘What was the basis of Moses’ appeal? God’s honor. He ap-
pealed to God’s past promises to Abraham, Isaac, and Israel
(Jacob). These promises had been based on God’s covenant with
them. He had promised an inheritance to Israel. Would God cut off
this inheritance in the midst of history? If so, then the nations
round about would call God a liar, a deity impotent to bring His
Word to pass in history. He did not appeal to God in terms of the’
good intentions or righteousness of the Israelites; he appealed to
the good intent and righteousness of God. He appealed to Gad’s
