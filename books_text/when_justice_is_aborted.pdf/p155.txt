Are Operation Rescue's Critics Self Serving? 137

including fighting it politically. And then, in the summer of 1988,
a handful of non-violent activists began to “up the ante” by
breaking local property laws in Atlanta, Georgia, and later in other
cities by interposing their bodies between murderous mothers and
their murderous accomplices, state-licensed physicians.

(Strange, isn’t it? Liberals for 70 years insisted that Shuman
rights are more’ important than property rights!” This phrase
supposedly proved that high taxes and government regulation of
the economy are morally legitimate. But these days, the liberals
have spotted a problem with this slogan. A bunch of crazy Chris-
tians have started intruding onto the property of wealthy, state-
licensed murderers ~ excuse me, physicians—to interfere with
the daily slaughter of the innocents. Now, all of a sudden, the
defense of private property is high on the liberals’ list of priorities.
Liberals certainly enjoy taxing the high incomes of physicians, but
they want them to earn those juicy taxable incomes, especially if
those incomes come from killing judicially innocent babies. Popu-
lation control, and all that. And . . . liberals will never actually
say this in print, of course. . . these slaughtered babies are mostly
blacks and Hispanics. You know. Those kind of people! They have
concluded that an abortion is less expensive to the welfare state
than two decades of aid to a dependent child, but they never say
this in public. They think that the cheapest way to “break the cycle
of poverty” is to kill the next generation of the potentially poor.
And never forget: indigent old people are also part of that cycle.)

Trespassing for Dear Life

This tactic of “trespassing for dear life” has now begun to
divide the Christian community. It has already divided Christian
leaders. This division appears to cut across denominational and
even ideological lines. Christian leaders are being forced to take a
position, pro or con, with regard to the legitimacy of this physical
interposition. Like Congress, they prefer to avoid taking sides, but
the pressures can no longer be avoided easily, at least for Reu-
benites.
