Conclusion 127

perform anything like the worldwide transformation of men’s
hearts.
2. The biblical concept of progressive sanctification is therefore
limited to the individual soul, the family, and the local church.
3. The mark of personal holiness is withdrawal from the cultural,
political, and social affairs of this world.

This outlook is analogous to the heresy of Deism. The Deist
god is so far removed from the creation that he does not call it to
repentance; He does not bring sanctions in history. Similarly, the
pietist is “so heavenly minded that he is of no earthly good.”
Because Christians have not had a biblical concept of transcen-
dence — the absolute sovereignty of God — they have adopted an
implicitly Deistic concept of God’s transcendence, and therefore
Christian man’s covenantal distance from this world.

Because the modern Christian’s doctrine of God’s transcen-
dence is incorrect because it is not grounded in the doctrine of the
covenant, so is his doctrine of God’s presence. His view of civiliza-
tion is closer to pantheism’s view than he wants to admit. The
defeatist cultural outlook of pietism is also analogous to the heresy.
of pantheism. The pantheist sees god as so immersed in the
creation that he cannot change it. Similarly, the pietist sees the
Christian as so dependent on his culture that he cannot expect to
change it. He is impotent to make a significant cultural difference.
He surrenders history to Satan and his covenantal agents. He
abandons earthly hope. I am not exaggerating. Listen to pietist
theologian Lehman Strauss on his assessment of the modern world,
in an article titled, “Our Only Hope”:

We are witnessing in this twentieth century the collapse of
civilization, It is obvious that we are advancing toward the end of
the age. Science can offer no hope for the future blessing and
security of humanity, but instead it has produced devastating and
deadly results which threaten to lead us toward a new dark age.
The frightful uprisings among races, the almost unbelievable con-
quests of Communism, and the growing antireligious philosophy
throughout the world, ail spell out the fact that doom is certain. 1
