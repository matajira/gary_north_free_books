Whose Sanctions Will Prevail? 89

is true, if by “prophet” we mean people who are uniquely called
by God to confront kings. There are no more prophets any more.
For that matter, there are no more kings. But Christians can speak
prophetically ~ analogous to the way that prophets spoke.

Did Jeremiah try to organize a public protest? No, he did not
have to. He was content to see Judah fall to the Babylonians. It
‘was his task to warn the rulers of God’s impending wrath, but he
did not organize politically to force them out of office. That would
not have been possible. Jeremiah organized no protests because
he knew that God had given over the nation to its enemies. God
was fed up:

Therefore thou shalt speak unto them this word; Thus saith
the Lorp God of Israel, Every bottle shall be filled with wine: and
they shall say unto thee, Do we not certainly know that every bottle.
shall be filled with wine? Then shalt thou say unto them, Thus
saith the Lorp, Behold, I will fill all the inhabitants of this land,
even the kings that sit upon David’s throne, and the priests, and
the prophets, and all the inhabitants of Jerusalem, with drunk-
enness. And I will dash them one against another, even the fathers
and the sons together, saith the Lop: I will not pity, nor spare,
nor have mercy, but destroy them (Jeremiah 13:12-14).

Tn fact, it is because we are not prophetically endowed regard-
.ing the specific future that we Christians must speak out. We must
preach God’s Word faithfully. We are required by God to speak
prophetically: bringing to the attention of all men the judicial terms
of God’s covenant, personal and corporate, warning them of the
covenant’s promised negative sanctions — sanctions that are ap-
plied in history by God to His enemies, personally and corporately.

There are those who say that God no longer applies his
sanctions in history. These are false prophets. In Jeremiah’s day,
God promised to deal with them harshly. He also promised to
deal harshly with those who listen to them and believe them, Men
should take heed:

Then said 1; Ah, Lord GOD! behold, the prophets say unto
