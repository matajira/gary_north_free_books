The Voice of Lawful Authority 43

God speaking to the person? Through his own conscience or
through the voice of his superior? Soldiers in battle face these
decisions. Middle management people in business face it. Bureau-
crats in government organizations face it. It is not confined to
Christians, and it is not confined to civil government. The question
is; Who best represents God’s moral will in any given situation?

The answer is difficult to determine. Good men will argue
about the answer. They even argue about how to find the answer.
But the fact remains: in history, men face this sort of decision all
over the world. It is a familiar problem throughout history and in
every society. It is the question of lawful authority and lawful
obedience.

Normally, people believe that mutiny is wrong, especially
during wartime. We all agree that a military commander in battle
must be obeyed. Lives of other people depend on the faithful
obedience of a commander’s subordinates. Yet the legal authoriza-
tion of mutiny — indeed, the legal obligation of subordinates to
mutiny — was affirmed by modern humanist international law
during the Nuremburg trials of German. military leaders following
World War II. The lega! right to answer to the court, “I was just
following orders,” was removed from all defendants. High military
officials were sentenced to death and executed by this international
tribunal for their having failed to mutiny against Hitler and the
German high command. This has been called “victor’s justice,”
but it is now the legal precedent that military officers face. (This
prededent may make it difficult for future wars to be settled
peaceably short of the unconditional surrender of one side. Leaders
of the losing side may decide they have nothing to lose by continu-
ing the war, hoping for a miraculous turn of events.) Thus, there
is no escape today from the dilemma of moral choice. Therefore,
even in this seemingly obvious case—the question of military
mutiny — civil courts present citizens with the same two seemingly
opposed legal and moral standards that we find in Romans 13 and
Acts 5.
