86 When Justice Is Aborted

read Baruch in the book the words of Jeremiah in the house of the
Lorn, in the chamber of Gemariah the son of Shaphan the scribe,
in the higher court, at the entry of the new gate of the Lorn’s
house, in the ears of all the people (Jeremiah 36:9-10).

An agent of the ower magistrates was in the crowd, and he
then took the message to his associates.

‘When Michaiah the son of Gemariah, the son of Shaphan, had
heard out of the book all the words of the Lorn, Then he went
down into the king’s house, into the scribe’s chamber: and, lo, all
the princes sat there, even Elishama the scribe, and Delaiah the
son of Shemaiah, and Elnathan the son of Achbor, and Gemariah
the son of Shaphan, and Zedekiah the son of Hananiah, and all the
princes. Then Michaiah declared unto them all the words that he
had heard, when Baruch read the book in the ears of the people.
‘Therefore all the princes sent Jehudi the son of Nethaniah, the son
of Shelemiah, the son of Cushi, unto Baruch, saying, Take in thine
hand the roll wherein thou hast read in the ears of the people, and
come. So Baruch the son of Neriah took the roll in his hand, and
came unto them. And they said unto him, Sit down now, and read
it in our ears. So Baruch read it in their ears (Jeremiah 36:11-16).

The lower magistrates decided at that point to listen to the
words of the prophet. The people had initiated the national fast;
now the rulers'felt led by the example set by the people. When
they heard the message, they decided that the prophet’s warning
should be taken seriously. Then they made a fundamental deci-
sion, They decided to serve as judicial intermediaries between
Baruch and the king. They took a legal stand: interposition.

Now it came to pass, when they had heard all the words, they
were afraid both one and other, and said unto Baruch, We will
surely tell the king of all these words. And they asked Baruch,
saying, Tell us now, How didst thou write all these words at his
mouth? Then Baruch answered them, He pronounced all these
words unto me with his mouth, and I wrote them with ink in the
book. Then said the princes unto Baruch, Go, hide thee, thou and
Jeremiah; and let no man know where ye be. And they went in to
the king into the court, but they laid up the roll in the chamber of
