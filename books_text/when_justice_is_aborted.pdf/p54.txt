36 When Justice Is Aborted

became at most a downtown rescue mission operation to sober up
a few drunks, or a foreign orphanage operation, which all too often
imparted only the ability to read to young people, whereupon the
Communists recruited them because the Communists, rather than
the Christians, had produced a large quantity of literature that
promoted a deeply religious (atheist) vision of earthly victory.

At the same time that the evangelicals were adopting a
worldview based on historical discontinuity, the theological and
political liberals became the advocates of historical continuity.
They successfully stole the Christians’ original vision of earthly
victory, and secularized it. (This successful theft is the source of
the myth still found in church history books that “postmillennial
social optimism is a form of theological liberalism.” Why, then,
was virtually the entire faculty of Princeton Theological Seminary,
the nation’s most prestigious Bible-believing seminary in the nine-
teenth and early twentieth century, both postmillennial and politi-
cally conservative throughout the nineteenth century? Why was
Princeton’s Charles Hodge, a postmillennialist and author of the
famous Systematic Theology, the great opponent of Charles. Darwin
and evolution?)

The Scopes Trial

This self-imposed cultural burial of Christians accelerated in
1925 with the Scopes “monkey trial” (evolution in’ the public
schools) and did not begin to change until the late 1970’s, with the
appearance of the anti-abortion movement and the Presidential
candidacy of Southern Baptist Jimmy Carter. With the coming of
these preliminary signs of “salt and light revival” among funda-
mentalist Christians has also come the growth of doubt regarding
the prevailing eschatologies of earthly despair and Christian cul-
tural retreat, What people believe inevitably affects what they do,
but what people do also affects what they believe,

Quite frankly, one reason why Christians today read and
believe David Chilton’s little book, The Great Tribulation (Dominion
Press, 1987), which argues the commonly held pre-1900 theologi-
