Whose Sanctions Will Prevail? 97

tolerate and even authorize (i.e., sanction) the profit-seeking murder
of the innocent, then they will surely tolerate the persecution of
those who interpose themselves in between the murderers and
their judicially innocent victims. The rulers recognize clearly that
these watchmen are calling rulers to judicial account before God
and men when they interpose themselves between the murderous
sanctions of the abortionists and their intended victims. The more
public and physical the interposition, the more resentful and re-
vengeful the morally corrupt and judicially blinded rulers will be.
They will escalate their negative sanctions as surely as God will
escalate His,

The physical interposition of the saints is biblically legitimate
because the sanctions of the murderers are illegitimately physical.
Because the interposition of the saints is physical, the sanctions
applied by the public authorities are also likely to be physical.
From the very beginning of the protest, the question is not “sanc-
tions vs. no sanctions.” The question is; Whose sanctions? When
the confrontation escalates, the question is not physical sanctions
vs. no physical sanctions. The question is: Whose physical sanc-
tions? Which physical sanctions?

What all Christian protesters must understand before they get
involved in acts of physical interposition is this: without the sup-
port of the lower magistrates, they cannot lawfully and covenan-
tally impose negative physical sanctions against the civil authori-
ties. Non-violent physical interposition is a positive physical sanction for the
unborn child and therefore a negative physical sanction against aitempted
murderers, but it is not a negative physical sanction against the civil magis-
trate. There is nothing in principle that says that protesters cannot
lawfully and-covenantally impose the physical sanction of bodily
interposition in between criminals and victims.

If the interposer predicts that making a citizen’s arrest of the
murderers will not be sustained in court, then he may choose to
test the law.in other ways. The way he does this biblically is to
become the covenantal stripe-bearer. He interposes himself physically
in between the criminal and the intended victim, and thereby risks
