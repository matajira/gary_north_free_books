142 When Justice Is Aborted

T would agree. So, some rationale other than serving as an Old
Testament judge must be found to justify non-violent interposition.
T have attempted to outline such a defense in this book ~ a defense
based squarely on the biblical covenant model.

T now need to devote space to answering several of the non-
covenantal (or imitation covenantal) arguments that have been
offered by Christians. I cannot answer all of them, Indeed, it is
now the responsibility of the Christian critics of interposition to
answer me. I have not tossed out a series of random arguments in
this book; f have presented an integrated case based on the biblical
covenant model. I am waiting to see something equivalent from
anti-confrontational, self-proclaimed covenant  theologi-
ans — something more persuasive than dot-matrix-printed mani-
festos. If they remain silent now, then they are admitting that they
have no case theologically. To admit this is also to admit that their
arguments were designed from the beginning to defend their own
personal inaction and the inaction of their churches rather than
the product of careful theological investigation.

Too many naive Christians have been persuaded by these
sheepfold manifestos with the hidden agendas. They have been
bullied theologically into inaction and confusion. Meanwhile, un-
born babies are being murdered. It is time for the authors of these
manifestos either to answer my book or else reverse or drastically
modify their stated position publicly. I think they will do their
best to avoid taking any of these steps. To which I respond, in
advance: “Theological silence from this point on is not golden; it
is yellow.”

What the reader must understand is that I am taking every
example from published statements from pastors or church offi-
cers. I am not making up any of this, These are real argu-
ments —real stupid arguments — offered by real men who expect
us to take them real seriously.

How seriously should you take these arguments? Decide for
yourself. How seriously should you take the people who offered
them? Decide for yourself. As you read these objections to Opera-
