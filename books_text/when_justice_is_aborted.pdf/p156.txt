138 When Justice Is Aborted
There are two signs in front of abortion clinics:

“No Trespassing”
“Thou Shalt Not Kill”

The “No Trespassing” sign is symbolically stuck into the grass.
The “Thou Shalt Not Kill” sign is literally being carried (or ought
to be literally carried) by an anti-abortion picketer.

The picketers have now begun to realize that they face.a major
moral decision: either ignore the implicit “No Trespassing” sign
or ignore the covenantal implications of the “Thou Shalt Not Kill”
sign, The fact of the matter is that if Christians continue to obey
the abortionists’ “No Trespassing” signs, God may no longer
honor this humanistic nation’s “No Trespassing” sign to Him.
He will eventually come in national judgment with a vengeance.
This is a basic teaching of biblical covenant theology. (It is conven-
iently ignored in the pseudo-covenant theology of the critics.)

A small, hard core of dedicated Christians has now decided
that they cannot obey both signs at the same time. One of these
imperatives must be obeyed, and to obey it, the other imperative
must be disobeyed. This has precipitated a crisis,

There is a much larger group of Christians that pretends that
there is nothing inherently contradictory about these two signs.
There is nothing going on behind closed clinic doors that Chris-
tians have a moral imperative and judicial authorization from God
to get more directly involved in stopping. They prefer not to think
about the two signs. They see the first one and assume that it has
the highest authority.

There have been other “No Trespassing” signs in history.
Outside of German concentration camps in 1943, for instance, But
Christians in Germany honored those signs. They forgot the words
of Proverbs:

If thou faint in the day of adversity, thy strength is small. If
thou forbear to deliver them that are drawn unto death, and those
that are ready to be slain; If thou sayest, Behold, we knew it not;
doth not he that pondereth the heart consider it? and he that
