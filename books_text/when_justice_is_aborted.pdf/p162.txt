144 When Justice Is Aborted

irrelevant to anything except the hope of confrontation-avoiding
Christians that some gullible Christian will take them seriously.
Yet Christian authors and pastors offer such an argument as if it
were serious. A’ Christian should suspect the motives of anyone
who would deliberately distort reality this badly. I suggest that the
critic has a hidden agenda. Nobody comes to conclusions this
preposterous without a hidden agenda.

“Pro-Choice” Ethics in
“Free Will” Language

Does the civil disobedience advocated by Operation Rescue fit the biblical
exception [to the general rule against disobeying civil magistrates]? We believe
the answer to this question is NO, because: . . . (2) Ree vs. Wade (the
law of the land) neither requires abortions nor prohibits them, but makes them
permissible with certain restrictions. (3) The women who choose to have an
abortion are free moral agents responsible before Almighty God for their
actions, including the exercise of the rights of their innacent, unborn child.

So say the deacons of one giant Southern Baptist church. I
have already considered the argument that Roe 2. Wade is not really
morally evil because it does not actually compel abortions. Let us
go to reason #3 in the critics’ list. Change the word “abortion” to
“murder,” and allow the child to be out of the womb for five
seconds. We get this bit of ethical wisdom: “The women who
choose to: murder their newborn children are free moral agents
responsible before Almighty God for their actions, including the
exercise of the rights of their innocent, newborn child.” Are you
in agreement?

No? Then why should you take seriously the moral perspective
of the first version? Why should God take it seriously?

What is the difference between murdering an infant who is five
seconds out of the womb and murdering an infant five hours
earlier? Or five days? Or five weeks? I will tell you what the
difference is: safe pulpits, For now.

Let us consider the argument based on the woman’s “free
moral agent” thesis. This is a real sleight-of-hand (tongue?) argu-
