xii : When justice Is Aborted

probably does not understand that public confrontations are ines-
capably media wars. Not only is such a naive person unlikely to
win the media war, he is probably not going to get involved in
one. “Too unspiritual,” you understand.

Not all the Christian media “got on board” the Louisville
school case, sad to say, although the 700 Club did produce a
couple of reports on the crisis. The popular “we're high on Jesus”
T.V. shows refused, as usual, to take sides. Too controversial. It
might hurt the ratings. (Five years later, national controversy hit
two of these national television ministries and damaged all the rest.
At least two Christian media representatives would have been
wiser to have spent more time covering the Nebraska school war
and less time uncovering their consorts.)

Cass County ordered the church’s school closed. When Rev.
Sileven refused, the county threw him in jail. As word of his arrest
spread, accompanied: by an emotionally moving videotape of the
sheriff hauling him off to jail, hundreds of pastors around the
country began to stream into tiny Louisville. They were not fa-
mous pastors. 'amous pastors stayed discreetly silent. They were
pastors of small congregations who recognized how vulnerable
their churches and schools were.

Local residents deeply resented these “outside agitators” in the
same way that white residents in the South hated the freedom
riders and protesters in the early 1960's. The local residents of
Louisville, Nebraska, like local residents everywhere, worship their
public schools, whether or not they worship God or attend church.
They tithe their children to the state in these schools, generation
after generation; very few of them tithe to a church. Rev. Sileven
was calling into question the morality and legality of the entire
system of state licensing of private schools, and he was gaining
national attention for this protest against this universally accepted
tyranny. The State of Nebraska was being made to look foolish in
the eyes of the nation, and it was Sileven who was the cause of this.
So, Rev. Sileven and his supporters became persona non grata in
Louisville, Nebraska.
