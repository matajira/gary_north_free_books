Introduction 15

ingly triumphant secular humanism. If Christians cannot see the
life-and-death issue of abortion, then they are not prepared to
exercise dominion in any area of civil government.

R. J. Rushdoony wrote a little pamphlet called Adortion is
Murder in 1971, two years before the U.S, Supreme Court handed
down the infamous Roe » Wade decision. Few Christians noticed
the pamphlet. Two years later, in 1973, Rushdoony’s Institutes of
Biblical Law was published. This book identified the historical
background of modern abortion. Abortion is a revival of a moral
issue that brought Christians into conflict with ancient pagan
Rome. There was no reconciliation possible between Rome and
the Church, between the pagan Caesar and Christ. It was only
settled when Christians took over the Roman Empire.

In Biblical law, all life is under God and His law. Under Roman
law, the parent was the source and lord of life. The father could
abort the child, or kill it after birth. The power to abort, and the
power to kill, go hand in hand, whether in parental or in state
hands. When one is claimed, the other is soon claimed also, To
restore abortion as a legal right is to restore judicial or parental
murder {p, 186).

Christians must now make up their minds: Are they going to
assent to legalized murder or oppose it publicly? Are they going
to break the civil law as a means of challenging it as a test case,
or are they going to allow humanists to continue to authorize the
murder of babies? The U.S. Supreme Court has overturned its
own prior rulings at least 150 times, Are Christians ready to give
the Court an opportunity to do it again?

Note to the Reader

T have filled this book with quotations from the Bible. This is
necessary, since so many Christian critics of social action and
especially direct confrontation insist -legitimately, I might
add—that those who propose non-violent protests present an
explicitly biblical case for what they are doing. While I draw upon
examples from history, I use them only as examples; I am making
