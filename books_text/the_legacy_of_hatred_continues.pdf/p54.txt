42 The Legacy of Hatred Continues

near to God, “who has made us partakers in the blessings of His
elect.” He went on to quote from Deuteronomy 32:8-9, a passage
that deals with Israel’s election as God’s people and His portion
and inheritance.* It seems that Clement was thinking of the
church as the elect nation of God, if this was his thought, it would
imply that the church is in the New Covenant what Israel was in
the Old. This is how Kelly reads this passage in 1 Clement: “Cle-
ment of Rome sees in its [the church's] election the fulfillment of
the prophecies that Jacob should become the Lord’s portion and
Israel the lot of His inheritance.”*! If this interpretation of this
passage is correct, Clement was applying Old Testament proph-
ecy to the church, something that Lindsey says began several
centuries later.

Again, the point of these quotations is not that we agree with
every formulation. In particular, it is wrong to see the episode of
the Golden Calf as a definitive breach of the covenant, as Bar-
nabas seems to have argued. Irenaeus was correct that the Jews
rejected the covenant when they put to death the Lord of the Cov-
enant. The point, however, is that Origen was not the first theo-
logian to say that the church is the New Israel, and that the
promises given to Israel in the Old Covenant now apply to the
church. Again, if Lindsey is correct that this view of the church’s
relation to Israel leads to “anti-Semitism,” his charges must be
directed to the whole of Christian theology until the mid-nine-
teenth century development of dispensationalism.*

As the church gradually became dominated by Gentiles, the
urgency of the question of the church’s relation to Judaism and

30. Clement, “First Epistle.” In Ante-Nicene Fathers, vol. 1, pp. 12-43.

31, Kelly, Early Christian Doctrines, p. 190.

32, David Rausch, more consistent in this regard than Lindsey, refers to “two
millennia of Christian Anti-Semitism.” A Legacy of Hatred, ch. 2. Lindsey, trying
to protect his view that the earliest church Fathers were dispensational premil-
lennialists, asserts without a shred of evidence that early Christian views of Israel
were more sympathetic than post-Origenist views. It is an ironic fact that Origen
was onc of the few church fathers who learned Hebrew and whe had face-to-face
encounters with Jewish biblical scholars. Pelikan, Emergence of the Catholic Tradi-
tion, p. 21.
