50 The Legacy of Hatred Continues

version of the Gentiles, the first before the conversion of the Jewes,
they being Branches wilde by nature grafted into the True Olive Tree in-
stead of the natural! Branches which are broken off. . . . The sec-
ond, after the conversion of the Jewes.”!®

The Congregationalist Savoy Declaration (1658) included the con-
version of the Jews in its summary of the church's future hope:

we expect that in the latter days, Antichrist being destroyed,
the Jews called, and the adversaries of the kingdom of his dear
Son broken, the churches of Christ being enlarged and edified
through a free and plentiful communication of light and grace,
shall enjoy in this world a more quiet, peaceful, and glorious
condition than they have enjoyed.”

Prayer for Israel's Conuersion

Because they believed that the Jews would be converted, Puri-
tan and Presbyterian churches earnestly prayed that Paul’s
prophecies would be fulfilled. Murray notes that “A number of
years before {the Larger Catechism and Westminster Directory
for Public Worship] were drawn up, the call for prayer for the
conversion of the Jews and for the success of the gospel through-
out the world was already a feature of Puritan congregations.”
Also, among Scottish Presbyterian churches during this period,
special days of prayer were set aside partly in order that “the
promised conversion of [God’s] ancient people of the Jews may be
hastened.”#! Puritan Independent Thomas Goodwin, in his book,
The Return of Prayers, encouraged people to pray even when they
failed to see their desires realized. Among the things for which the
church should pray were “the calling of the Jews, the utter down-
fall of God’s enemies, the flourishing of the gospel.” Goodwin
assured his readers that all these prayers “will have answers.”2?

18. Quoted in Murray, Puritan Hope, p. 72. Interestingly, some of this same
language — the phrase ‘double conversion of the Gentiles” in particular — was used
by Johann Heinrich Alsted, whose premillennial The Beloved City on, The Saints
Reign on Earth a Thousand Yeares (1627; English edition 1643) exercised great influ-
ence in England. See De Jong, As the Waters Cover the Sea, p, 12.

19, Quoted in Defong, As the Waters Cover the Sea, p. 38.

20. Murray, The Puritan Hope, p. 99.

21, Quoted in ibid, p. 100.

22, Quoted in shid., p. 102
