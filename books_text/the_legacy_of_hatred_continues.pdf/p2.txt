Copyright © 1989 by American Vision, Atlanta, Georgia
Distributed by the Institute for Christian Economics and Dominion Press.
All rights reserved. Published July, 1989, First edition.

No part of this publication may be reproduced, stored in a retrieval sys-
tem, or transmitted in any form by any means, electronic, mechanical,
photocopy, recording, or otherwise, without the prior, written permission
of the publisher, except for brief quotations in critical reviews or articles

Unless otherwise noted, all Scripture quotations are from the New Ameri-
can Standard Version of the Bible, copyrighted 1960, 1962, 1963, 1968, 1971,
$972, 1973, 1975.

American Vision is a Christian educational and communications organ-
ization. American Vision publishes a monthly newsletter, The Biblical
Worldview, which is cdited by Gary DeMar. For more information about
American Vision, write: American Vision, P.O, Box 720515, Atlanta,
Georgia 30328.

Published by Institute for Christian Economics, P.O. Box 8000, Tyter,
Texas 75711.

Printed in the United States of America

Typesetting by Thoburn Press, Tyler, Texas
ISBN 0-930464-29-X
