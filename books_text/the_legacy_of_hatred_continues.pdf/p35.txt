The Law, Eschatology, and “Anti-Semitism” 23

What's the solution to “anti-Semitism”? Love your neighbor
as you love yourself. How do we know when we're loving our
neighbor? The Bible tells me (don’t steal; don’t murder; don’t
covet; don’t slander; etc.). What is it called when the Bible tells
me what to do and what not to do? Obedience. Is God’s law
good? Yes, when one uses it lawfully, such as the protection of
people and the punishment of evil men like “anti-Semites”
(1 Tim. 3:8). If everybody followed God's law, would this be
good for the Jews? Yes! They might even see the love of Jesus in us so
that they might glorify our “Father who is in heaven” (Matt. 5:16).
Remember, “faith without works is dead” (James 2:26). Scrip-
ture does not say that faith without a dispensational eschatology
is dead. Now, that wasn’t so hard, was it?

Marcion Lives

Hal Lindsey and dispensationalists in general have a low re-
gard for the Law of God," similar to the views of the arch-heretic
Marcion (second century a.p.). “Marcion stressed the radical
nature of Christianity vis-a-vis Judaism. In his theology there ex-
isted a total discontinuity between the OT and the NT, between
Israel and the church, and even between the god of the OT and
the Father of Jesus.”'? Dispensationalist teaching is similar to
Marcion’s radical discontinuity between the Old Testament and
the New Testament and his radical distinction between Israel and
the church. How did this affect the Jews?

11. Hal Lindsey is a wee bit schizophrenic on this point. He states the fol-
lowing: “I'm very much involved in urging Christians to be politically active. I'm
very much for Christians’ being active and running for government, seeking to
bring Judeo-Christian morality into our various governments.” Hal Lindsey,
“Weck in Review,” Trinity Broadcasting Network (June 5, 1989). * fudeo-Christian”
morality includes Old Testament biblical law. In the midst of his diatribe against
Christian Reconstruction, Lindsey nullifies his entire thesis by stating this ob-
vious Reconstructionist distinctive: “It is correct to say that Biblical Law should
serve as a pattern for civil law as John Calvin taught.” Lindsey, Road to Holocaust,
p. 157. He wants a “pattera” but no “blueprint.” What is this supposed to mean,
and how does this differ from what Reconstructionists are saying?

12. W. Ward Gasque, 8.V. “Marcion,” The New International Dictionary of the
Christian Church, J. D. Douglas, gen. ed. (rev. ed.; Grand Rapids, MI: Zonder-
van, 1978), p. 620.
