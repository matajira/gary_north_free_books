Postmillennialism and the Salvation of the Jews 49

William Perkins, a leading Puritan teacher and writer, also
taught that there would be a future mass conversion of the Jews.
Similarly, Richard Sibbes wrote that “The Jews are not yet come
in under Christ’s banner; but God, that hath persuaded Japhet to
come into the tents of Shem, will persuade Shem ta come into the
tents of Japhet.” Elmathan Parr’s 1620 commentary on Romans
espoused the view that there would be two “fullnesses” of the Gen-
tiles: one prior to the conversion of the Jews and one following:
“The end of this world shall not be till the Jews are called, and
how long after that none yet can tell.”'*

Speaking before the House of Commons in 1649 during the
Puritan Revolution, John Owen, a postmillennial theologian,
spoke about “the bringing home of [God's] ancient people to be
one fold with the fulness of the Gentiles . . . in answer to millions
of prayers put up at the throne of grace, for this very glory, in all
generations.”"§ Samuel Lee, at one time a member of Owen's
church, believed, as he explained in his popular 1677 book, Jsrae/
Redux, that the Jews would someday return to the land of Palestine, 1

Creeds and Confessions

Councils of the English and Scottish churches also addressed
the question of Israel. The Westminster Larger Catechism, Ques-
tion 191, displayed the same hope for a future conversion of the
Jews. Part of what we pray for ih the second petition, “Thy king-
dom come,” is that “the gospel [be] propagated throughout the
world, the Jews called, the fullness of the Gentiles brought in.”
Similarly, the Westminster Directory for Public Worship directed
ministers to pray “for the Propagation of the Gospell and King-
dome of Christ to all nations, for the conversion of the Jewes, the
fulnesse of the Gentiles, the fall of Antichrist, and the hastening of
the second coming of the Lord.”” In 1652, a group of 18 Puritan
ministers and theologians, including both Presbyterians and
Independents, affirmed that “the Scripture speaks of a double con-

14. All quotations from DeJong, As the Waters Cover the Sea, pp. 27-28.

15. Quoted in Murcay, Puritan Hope, p. 100

16. Peter Toon, God's Statesman: The Life and Wark of John Owen (Grand Rapids,
MI: Zondervan, 1971), p. 152.

17. Quoted in DeJong, As the Waters Cover the Sea, pp. 37-38.
