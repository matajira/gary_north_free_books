This book is dedicated to
Rev. Joseph McAuliffe

who teaches Christians
how to get out of debt
and stay out of debt
through tithing. Every-
one wins except the
potential lenders.
