For I have received of the Lord that which also I delivered
unto you, That the Lord Jesus the same night in which he was
betrayed took bread: And when he had given thanks, he brake
it, and said, Take, eat: this is my body, which is broken for you:
this do in remembrance of me. After the same manner also he
took the cup, when he had supped, saying, This cup is the new
testament in my blood: this do ye, as oft as ye drink it, in re-
membrance of me. For as often as ye eat this bread, and. drink
this cup, ye do shew the Lord’s death till he come. Wherefore
whosoever shall eat this bread, and drink this cup of the Lord,
unworthily, shall be guilty of the body and blood of the Lord.
But let a man examine himself, and so let him eat of that
bread, and drink of that cup. For he that eateth and drinketh
unworthily, eateth and drinketh damnation to himself, not
discerning the Lord’s body. For this cause many are weak and
sickly among you, and many sleep. For if we would judge our-
selves, we should not be judged. But when we are judged, we
are chastened of the Lord, that we should not be condemned
with the world (I Cor 11:23-32).

And let us consider one another to provoke unto love and to
good works: Not forsaking the assembling of ourselves together,
as the manner of some is; but exhorting one another: and so
much the more, as ye see the day approaching (Heb. 10:24-25).
