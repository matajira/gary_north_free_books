182 TITHING AND THE CHURCH

Epistle to the Hebrews traces the New Covenant’s high priest,
Jesus Christ, back to Melchizedek: “And it is yet far more evi-
dent: for that after the similitude of Melchisedec there ariseth
another priest, Who is made, not after the law of a carnal com-
mandment, but after the power of an endless life” (Heb. 7:15-
16). The New Covenant priesthood is not grounded legally in
the Mosaic Covenant’s priesthood. Its legal foundation goes
back to the original priesthood: Melchizedek’s.

In contrast, Rushdoony traces the New Testament church’s
ministry back to the Levites, who did not administer the sacri-
fices. They collected the tithes of the nation. On what legal
basis? He says that they possessed this authority because they
did charitable works. That is to say, their authority was functional,
i.e., related to their economic and social function. “The Levites
had broad functions, including the fact that they were the
teachers (Deut. 38:10). When we restore God’s laws of tithing,
we can re-establish the Christian strength in worship, health,
education, and welfare, and we will have done it in God’s
way.”® This is Rushdoony’s view of church authority: functional
rather than judicial.

The Bible teaches otherwise. The Levites were above all the
guardians of the temple (Num. 18). In this judicial capacity,
they were also guardians of the word of God. Their functions
were extensions of their judicial authority. They were invested by
God with the authority to exclude people physically from the
temple’s sacramentally holy areas.

The issue, then, is inclusion and exclusion with respect to
the sacraments. In the Old Covenant in Abram’s time, the high
priest of Salem possessed this ultimate authority to include and
exclude. Immediately following Abram’s communion meal, God
granted to him the responsibility of circumcising all those who

2. Rushdoony, “Vouchers, Freedom and Slavery,” Chalcedon Position Paper No.
118 (Sept. 1989); ibid., p. 448.
8. See above, pp. 171-73.
