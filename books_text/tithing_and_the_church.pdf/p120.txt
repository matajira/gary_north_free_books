106 TITHING AND THE CHURCH

They prefer to think of the church as judicially impotent. They
prefer to think of the State's physical sanctions as the greatest
possible sanctions. They refuse to regard formal excommunica-
tion as threatening them or anyone else with eternal conse-
quences. Like the humanists, they prefer to fear men rather
than God. They stand in front of the local church and in effect
chant the child’s challenge: “Sticks and stones can break my
bones, but names [‘excommunicant’] can never hurt me!”

Neither the State nor the church is a profit-seeking organiza-
tion. This is why both possess lawful claims on a small part of
the net productivity of their members. Therefore, they cannot
be primary agencies of dominion in history. They are second-
ary agencies of dominion." Thus, I conclude, the tithe cannot
be a primary aspect of dominion. It is a secondary aspect.

Productivity

‘This is not to say that church and State are not economically
productive. They are the source of God’s authorized covenantal
sanctions: the negative sanctions of the sword (State) and the
positive and negative sanctions of the keys of the kingdom
(church). Rushdoony’s language is seriously misleading when
he writes that “church and state are not productive agen-
cies.""5 This is the language of secular libertarianism, not
Christianity. Nevertheless, he makes an important point: “The
state is a protective agency whose function is to maintain a just
order, to insure restitution for civil wrongs, and to protect the
people from external and internal enemies. . . . The church's

14. This is why the Great Commission of Matthew 28:18-20 is not strictly an
extension of the dominion mandate of Genesis 1:26-28, A small portion of the fruits
of dominion are brought to the institutional church. The church is not the source of
these fruits. ‘The institutional church, through its authority to declare someone as an
adopted son of God, brings covenant-breakers formally into the eternal household of
God, but the institutional church is not itself'a family. It possesses greater authority
than the family.

15. Rushdoony, Law and Society, vol. 2 of Institutes of Biblical Low (Vallecito,
California: Ross House, 1982), p. 129,
