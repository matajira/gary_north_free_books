The Chronology of Rushdoony's Ecclesiology 153

he attend a local church. Neither did his wife. (Those of his
children who still lived with him attended Rev. George Milad-
in’s Evangelical Presbyterian Church congregation.) The West-
wood group was filled with people who were unhappy with
their church memberships. They decided to move the Bible
study’s meeting from 3 p.m. to 11 a.m.

These Chalcedon meetings remained Bible studies officially.
Had they been constituted as formal worship services, Rush-
doony would have had to bring one or both of the study
groups into the OPC as congregations. He would then have
had to transfer his membership to the Southern California
Presbytery. He had some enemies in this Presbytery, so it was
convenient for him to remain under the authority of a distant
Presbytery. But in order to maintain this personally convenient
arrangement, he could not offer the Lord’s Supper at the
morning and evening meetings. He did occasionally baptize
children at these meetings, but no formal church membership
accompanied these baptisms.

A Formal Complaint

Eventually, Rev. Sal Solis, a local OPC minister, lodged a
formal complaint to the Northern California Presbytery, He
objected to the times scheduled by Rushdoony’s Bible studies,
which clearly overlapped church worship hours. The conflict
had begun when an elder in his church, Vic Lockman, was
attending the evening Bible studies in Pasadena. Lockman was
told to set a better example and attend the church’s evening
meetings. He refused, citing Presbyterian tradition regarding
compulsory attendance: only once per week. Solis then filed his
complaint against Rushdoony with the Northern California
Presbytery, a complaint which the Northern California Presby-
tery dismissed.” Rushdoony immediately resigned from the

7. Rushdoony referred to this briefly in his November, 1980 Chatcedon Position
Paper No. 17; reprinted in Rushdoony, The Roots of Reconstruction (Vallecito, Califor-
