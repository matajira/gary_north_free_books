156 TITHING AND THE CHURCH

cal.’ So, if we take him at his word, at some point after 1970
he began to regard himself as being under no true church’s
authority.

Nevertheless, he did seek and receive a formal ecclesiastical
connection in 1974. A tiny group, comprising only two small
congregations (one in California, the other in Arizona), the
Anglican Churches of America, provided him with his post-
1974 ordination, i.e., his lawful claim to be called “Reverend.”
This was utterly bizarre. Here was a self-conscious and outspo-
ken ecclesiastical independent who accepted ordination from an
episcopal denomination (officially hierarchical) in 1974, but who
steadfastly refused to start a local congregation until 1991, and
who also refused to take regular communion in a local congre-
gation throughout the entire period.

Rushdoony’s infrequent attempts to define a doctrine of the
church are disjointed and confusing because they represent his
attempts to provide a theological justification for his bizarre
ecclesiastical odyssey. I strongly believe that his writings on the
church, the sacraments, and the tithe can be understood as
reflections of his employment situation after 1965.

Chalcedon’s Overnight Metamorphosis in Late 1991

In November, 1990, Dorothy (“Dolly”) Thoburn left her
husband David behind in Virginia, took their five children, and
flew to Vallecito, where her parents live. Her mother is Grayce
Flanagan, mentioned earlier: a long-term supporter of Chalce-
don, the woman who first encouraged Mr. Rushdoony to come
to southern California in 1965. Dolly initiated a civil divorce,
which became final in March, 1993,

In early 1991, David Thoburn appealed to his church's
elders. He is a member of a Presbyterian Church in America
(PCA) congregation in Reston, Virginia. David wanted to per-
suade his wife to return home. The elders asked David and

10. Rushdoony “Nature of the Church,” Calvinism Teday (Oct. 1991), p. 4.
