88 TITHING AND THE CHURGH

entitlement: the Levites’ service as guardians of the taberna-
cle/tempie’s sacramental boundary. They were required to
stand at this sacramental boundary and restrain (probably
execute) anyone who trespassed it (Num. 18:1-22).! The Le-
vites’ entitlement and the Levites’ task as boundary execution-
ers were explicitly linked by the Mosaic law.

There can be no doubt: the Levites were entitled to the
whole tithe. I ask again: On what legal basis? The text answers:
their service in the temple. But which form of service: sacra-
mental or social? I answer: sacramental. Rushdoony answers:
social. On this seemingly minor issue, the Christian Reconstruc-
tion movement has divided. It will remain divided until one
side or the other gives up its view of the judicial basis of the
tithe, or until one of them disappears. (The latter is more like-
ly.) Contrary to those people who blame all institutional divi-
sions on personality conflicts - even God vs. Satan, I suppose -—
the dividing issue here is ecclesiology: the doctrine of the
church, and has been since 1981.

Church and Tithe

The theology of the tithe is not a minor issue; it is central to
biblical ecclesiology. It is also important for a proper under-
standing of the covenant — specifically, the church covenant.”
The tithe is an aspect of judicial authority in the church, ie.,
point two of the biblical covenant model, hierarchy-representa-
tion.’ This representation is both substitutionary (“Who or
what in history dies in my place?”) and judicial (“Who in histo-

1. On the debate within modern Jewish scholarship on the Levites as execution-
ers— Jacob Milgrom vs, Menahem Haran — see James B, Jordan, “The Death Penalty
in the Mosaic Law,” Biblical Horizons Occasional Paper No. 3 (Jan. 1989), Pt. 8. Mil-
grom argues that the Levites were armed guards; Haran denies this. Jordan agrees
with Milgrom.

2 Ray R. Sutton, That You May Prosper: Dominion By Covenant Qnd ed.; Tyler,
‘Texas: Institute for Christian Economics, 1992), chaps. 10, 11.

3. Ibid, ch. 2.
