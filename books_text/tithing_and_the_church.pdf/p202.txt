188 TITHING AND THE CHURCH

This is why Rushdoony has never completed his long-prom-
ised systematic theology. To go into print in one place with his
views regarding the institutional church would identify him for
all time as heretical to the core. He will not do it. I have now
done it for him. As the co-founder of the Christian Reconstruc-
tion movement, I could no longer remain silent — not after his
challenge in October, 1993." He challenged me, for I have
been the source of criticism regarding his views of the church.
This book is my detailed response to his challenge. But please
understand: I am not looking for a fight, for Rushdoony is
never willing to defend his position when anyone honestly
criticizes him. I am looking only for a clearing of the decks,
once and for all, so that this movement can go forward without
a load of heretical baggage regarding the doctrine of the
church. The doctrine of the church is fundamental to Christian
reconstruction in the broadest sense. To get this doctrine wrong
is to move either toward statism (mainline churches’ error) or
toward patriarchalism (Rushdoony’s error).

If, however, Rushdoony should decide this one time to res-
pond to specific criticisms and refute this book, pay very close
attention to his line of argumentation. Ignore such rhetorical
words as blasphemous. Pay very close attention to his citations
from the Bible and from the historical confessions of the
church, especially the two that he publicly has affirmed: the
Westminster Confession and its catechisms, and the Thirty-Nine
Articles of the Anglican Church. See if he cites any other theo-
logians. See if he sticks to the topic: no side issues.

Wait to decide if he is correct until you have read whatever
1 write in response to his initial response. In formal debate, the
judges’ old rule of thumb holds up: when the debaters are
equally matched, the confrontation is won in the second rebut-
tal, (When the debaters are not equally matched, you can tell
very early.)

13, See above, p. 148, introductory quotation.
