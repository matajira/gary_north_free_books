Tithes, Taxes, and the Kingdom 63
Economic Touchstones

The tithe is the economic touchstone of God’s visible sover-
eignty in history. Collecting it publicly announces the sover-
eignty of the local church over its members, while paying it
reveals the commitment of individuals and families to God.
Whenever this ecclesiastical tax is denied by Christians, they will
be placed under another sovereignty: the State’s. Taxes have
become the economic touchstone of self-proclaimed autono-
mous man’s sovereignty in history, and a giant touchstone it
has become in the twentieth century.

By acknowledging the legitimacy of the State to impose. and
collect taxes, Christians are acknowledging the sovereignty of
the State. They acknowledge the right of revenue agents to
demand explicit statements regarding their income, as well as
examine all of their income records. Christians today even
acknowledge the authority of the State to tax them at levels
vastly beyond the tithe, which Samuel said is a sure sign of
tyranny. The modern antinomian Christian does not take Sam-
uel’s warning seriously. Neither did the Israelites in Samuel’s
day. The results were predictable; Samuel predicted them.

Christians today do not acknowledge the authority of the
local church to impose and collect the tithe. They do not ac-
knowledge their obligation to supply income records, such as
their income tax forms, to prove that they have paid their
tithes. Thus, they are implicitly denying the sovereignty of the
institutional church. It is this implicit confession that has placed
the church in mortal danger from the modern messianic State.

The Power to Tax

“The power to tax is the power to destroy.” So said Chief
Justice John Marshall in his famous opinion in the case, McCul-
loch v. Maryland (1819). The state of Maryland had imposed a

ture in the Western World (New York: Simon & Schuster, 1986).
