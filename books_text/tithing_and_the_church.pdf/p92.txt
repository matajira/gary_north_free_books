CONCLUSION TO PART 1

And I appoint unto you a kingdom, as my Father hath appointed
unto me; That ye may eat and drink at my table in my kingdom, and sit
on thrones judging the twelve tribes of Israel (Luke 22:29-30).

Jesus made it clear that taking the Lord’s Supper is a means
of dominion, The Lord’s Supper is more than a mere conven-
tion, tradition, or empty rite. It is also more than a memorial.
It is a means of extending the kingdom on earth. In our day,
few churches cite this passage prior to the Lord’s Supper
‘There are many reasons for this, but the main one is that few
churches believe in the visible triumph of both the gospel and
the church during the New Covenant era.

I have argued in this section of the book that God’s absolute
sovereignty undergirds His delegation of limited covenantal
sovereignty to church, family, and State. Each of these institu-
tions is established by means of an oath sworn under God,
either implicitly or explicitly, personally or representatively. No
other institution is so established. All other institutions are
under the authority of one or more of these three.

The economic mark of the sovereignty of the church is its
authority to collect and distribute the tithes of its members.at a
rate of ten percent. The economic mark of the sovereignty of
the State (taken as a collective: local through international) is its
authority to collect and distribute tax revenues at a maximum
rate of ten percent of its subjects’ income. The economic mark.
