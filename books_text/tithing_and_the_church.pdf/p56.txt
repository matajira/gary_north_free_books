42 TITHING AND THE CHURCH

Conclusion

There is two-tiered church membership today. There are
halfway covenant members who have been baptized but who for
some reason other than disobedience are not given access to the
Lord’s Table. There are also communing members who have
passed through some judicial barrier: age, confirmation, pro-
fession of faith, a new members’ class, etc. The trouble is, this
system of two-tiered membership is imposed too low in the
hierarchy. The distinction should not be made in terms of
access to the Lord’s Table; it should be made in terms of access
to the franchise. There should not be two-tiered membership
based on communing vs. non-communing members; rather, it
should be voting vs. non-voting members. Voting members
must be tithers and subscribe to the creeds and confessions.

The problem with the organizational structure of modern
Protestant churches is, first, they see the church as a contractual
rather than a covenantal institution. They do not see it as creat-
ed by a self-maledictory oath under God. They do not see it as
existing under unique sanctions and in possession of unique
sanctions. Thus, they view the Lord’s Supper as anything but
what it is: a covenant-renewal ceremony.

Second, because churches reject the continuing validity of
the Mosaic law, they reject the binding character of tithing. But
tithing precedes the Mosaic law, as the author of the Epistle to
the Hebrews points out (Heb. 7:1-10). Tithing is grounded on
the Abrahamic covenant. Churches today pay no attention.

Third, many churches allow open communion. They do not
keep non-church members away from the Lord’s Table. The
result is that membership is seen as no more binding than
membership in a local social club, and in some cases less bind-
ing. With no authority to excommunicate - to keep people
away from the Lord’s Supper —- the church’s most important
sanction is stripped of all judicial significance.

Fourth, those churches that uphold closed communion see
church membership solely in terms of lawful access to the sacra-
