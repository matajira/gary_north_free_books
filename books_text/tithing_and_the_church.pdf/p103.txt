Primary Sovereignty: Church or Family? 89

ty declares me judicially acceptable before God?”).

The proper performance of this representative ecclesiastical
office does mandate certain social services ~ charity, for exam-
ple - but the covenantal-judicial basis of the eldership is not
social; it is sacramental (point four of the biblical covenant
model: oath-sanctions).* A man is not a minister of the gospel
just because he calls himself one or because he is charitable. He
is a minister only because he has been ordained by a lawful
church. Ordained ministers guard the sacraments against pro-
fane acts: boundary violations. That is, they control lawful
access to the sacraments. They include some people and ex-
clude others. The following four aspects of a church are judi-
cially linked: the formal ordination of ministers by other minis-
ters (Le., no self-ordination or ordination by laymen), hierarchi-
cal authority (an appeals court system), ministerial control over
legal access to the sacraments, and the. local institutional
church’s exclusive authority to collect and distribute all of its
members’ tithes in God’s name. To deny any one of these as-
pects of the church is to call into question all four. So it was
under the Mosaic Covenant; so it is under Christ’s New Cove-
nant. Rushdoony has implicitly denied the first two points by
defending ecclesiastical independency, and he has emphatically
denied the other two. He is consistent (or at least he was until
October of 1991).5 His theological critics had better be sure
their theological positions are equally consistent.

The Doctrine of the Church in Christian Reconstruction

The major dividing issue within Christian Reconstruction has
been the doctrine of the institutional church. Officially, the

 

4. Similarly, the office of civil magistrate, called “minister” by Paul in Romans
18:4, is also based on point four: sanctions, in this case, negative sanctions. He
punishes evil-doers (¥. 4).

5. See Chapter 10.
