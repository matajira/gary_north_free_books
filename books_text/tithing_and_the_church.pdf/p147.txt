The Legal Basis of the Tithe 133

ic distinctions between Christ as High Priest and Christ as King
of kings. The Bible teaches clearly that the tithe is mandatory.
It goes to the church, and only to the church. Why? Because Jesus
Christ is the high priest after the order of Melchizedek (Heb.
7). In Rushdoony’s social theory, Christ’s office as High Priest
has no institutional sanctions.

In one limited sense, he is correct. The church technically
cannot excommunicate people who, like Rushdoony, refuse to
join a local congregation or take the Lord’s Supper. But the
church does not need to bring formal sanctions against those
who are selfexcommunicated.” Self-excommunication is excom-
munication. It is sufficient that the church publicly identify self-
excommunicated people as excommunicates. (Rarely does any
local church do this.) Church officers who serve the Lord’s
Supper to such self-excommunicated individuals have denied
their holy offices as guardians of the sacraments. It is not sur-
prising that a loose view of the sacraments is normally accompa-
nied by a loose view of the church and a loose view of the tithe.

The Chalcedon Foundation

Rushdoony for decades has paid his tithe to his own educa-
tional foundation, Chalcedon. He did not belong to any local
church until late 1991, when he declared Chalcedon a church.
Problem: his published theology of the tithe rests on a funda-
mental confusion between the sacramental function of the
church and its educational and nurturing function. His pub-
lished theology of the tithe does not acknowledge the judicial
requirement of the individual Christian to finance the sacra-
mental aspect of the kingdom by means of his tithe, and the
dominion and kingly aspects by means of voluntary donations

20, Jam not referring to Rushdoony's 1992 anointing of Chalcedon asa church.
See Chapter 10, below: subsection on “Chalcedon’s Overnight Metamorphosisin Late
1991.” Tam speaking of his published theology and two-decade absence from a local
church and its communion table.
