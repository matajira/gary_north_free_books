112 TITHING AND THE CHURCH

teaches that the tithe is judicially grounded solely in the coven-
antal authority of the church, which in turn is grounded on its
unique sacramental monopoly. We see this connection between
tithing and sacramentalism in the first biblical example of tith-
ing: Abraham’s tithe to Melchizedek, the priest of Salem, who
gave Abraham bread and wine (Gen. 14:18). It was not Melchi-
zedek’s office as king of Salem that entitled him to Abraham’s
tithe; it was his priestly status, which authorized him to distrib-
ute the positive sanction of Holy Communion: bread and wine.
Rushdoony discusses Melchizedek briefly, but only with respect
to the authority of the priesthood generally; he does not men-
tion the tithe or Holy Communion.”

What is noticeable about Rushdoony’s avoidance of any clear
definition of the church is that he has long refused to define
the institutional church as the exclusive source of the sacrament
of the Lord’s Supper. Instead, he has focused on the church in
the broadest sense, i.e., the kingdom of God. He writes in Law
and Society: “Second, the church is the City or Kingdom of God. It is
thus more than any church (as we call it) or state can be. The
boundaries of God’s church include every ‘church,’ state,
school; family, individual, institution, etc. which is under
Christ’s royal law and rule. But it includes far, far more.”*°
Notice that he placed church in quotation marks when referring
to institutional churches — organizations possessing the authori-
ty to excommunicate. He did not do this with the following
words: state, school, family, individual, institution. Do these
quotation marks indicate an underlying contempt for the au-
thority of local churches?

What, then, of the lawful role of the institutional church? He
has not offered a doctrine of the institutional church in well
over three decades of writing — and he has written a great deal.

29, Rushdoony, Law and Society, p. 368. He does not mention Melchizedek in
Voluine 1.

30. Jbid., p. 337.
