58 TITHING AND THE CHURCH

revealed law, we cannot know what God’s way is. The evangeli-
cal church has rejected God’s law. So, the church has locked
itself inside its cloister. The parachurch ministries have arisen
to fill the gap. The result will be another defeat for Christianity.

We must get it clear: there is no substitute for the church.
When it fails, Christianity suffers a body blow. No parachurch
ministry can do the work of the church unless it is supported
by the churches and is authorized by churches. Parachurch
ministries should be supported by donations from churches and
offerings from Christians. The day one of them adopts the tech-
niques of political direct mail to get tithe-sized donations is the
day God begins to shut it down. And so I say:

CUT YOUR STAFF ANOTHER 10 PERCENT. BEGIN
WITH THE DIRECT MAIL STAFF. THEN GET MORE
CHURCHES INVOLVED. LOWER YOUR GOALS.
DON’T BITE OFF MORE THAN YOU CAN CHEW
WITH LOW-HYPE OFFERINGS FROM SUPPORTERS.
SEND PLEAS FOR MONEY FOR ONE PROJECT AT A
TIME, AND DON’T START ANOTHER PROJECT UN-
TIL THE LAST ONE IS FINISHED. UNTIL THEN,
TAKE MY NAME OFF YOUR MAILING LIST.

I hold the hammer; I call the tune.

But I do not call it for the church: only for the parachurch.
I do not hold the big hammer in history; God does. He is rep-
resented by His church. Yet in my day, very few pastors believe
this. Therefore, others hold the hammers; others call the tunes.

Conclusion

In Dallas, there is a fundamentalist ministry, Christ for the
Nations. It is a very big foreign missions organization. They
make this offer to local indigenous churches on the mission
field: they will supply each congregation with a roof when the
church’s foundation and walls are erected. This makes sense,
