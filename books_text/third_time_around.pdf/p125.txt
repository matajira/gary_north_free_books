PART 3

THE
THIRD TIME
AROUND

Human affairs ave moved from deep springs.
A spirit moves them, It is by the acceptation,
the denial, and the renewal of philosophies
that this society of immortal mortals is
marked, changed, or restored.

Hilaire Belloc

The great intellectual tradition that comes
down to us from the past was never inter-
rupted or lost through such tifles as the sack
of Rome, the triumph of Attila, or all the bar
barian invasions of the Dark Ages. It was lost
after the introduction of printing, the discov-
ery of America, the founding of the Royal So-
ciety, and all the enlightenment of the Renais-
sance and the modern world. It was there, if
anywhere, that there was lost or impatiently
snupped the long thin delicate thread that
had descended from distant antiquity; the
thread of that unusual human hobby: the
habit of thinking.

G. K. Chesterton

 
