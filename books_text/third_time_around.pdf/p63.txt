4

CASTLES IN THE AIR:
THE RENAISSANCE AND
THE ENLIGHTENMENT

per angusta ad augusta *

There is a complex knot of forces underlying any nation once Chris-
tian; a smoldering of the old fires.

Ililaire Belloc

Intent on its ending, they are ignorant of its beginning; and there-
fore of its very being.
G. K, Chesterton

he remarkable explosion of wealth, knowledge,
and technology that occurred during the Renais-
= sance and the Enlightenment completely re-
shaped human society. No institution was left untouched.
Families were transformed from mere digits within the
larger baronial or communal! clan into nuclear societies in
and of themselves. Local communities were shaken from
their sleepy timidity and thrust into the bustle bustle of
mercantilism and urbanization. The church was rocked by
the convulsions of the Reformation, the Counter-Reforma-
tion, anabaptism, deism, and neo-paganism. Kingdoms,

 

54
