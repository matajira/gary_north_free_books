104 THE SECOND TIME AROUND

them to each of us—and thus that they had to match the
rhetoric of liberty with the activity of liberation.

That they did. Their sacrificial efforts demonstrated all
too clearly that when words are supported by deeds, there
is no stopping the fresh wind of hope:

« Elizabeth Ann Seton was born in 1774 to a very promi-
nent New York family. At twenty, she married a profes
sor of anatomy and entered into the cosmopolitan life
of ease and privilege. In 1803, her husband died leay-
ing her with five children to raise, Shortly thereafter,
she was converted and began seeking tangible ways to
express her newfound faith through service. She
worked with the poor, the sick, and the distressed for
some years before she finally focused on the care of
cast-off and abused children. Her efforts to expose the
horrors of illicit abortions drew attention to her work
and enabled her to effect a number of changes in both
legislation and enforcement. When she died in 1821,
she left a small community that has continued to prop-
agate her vision up to the present day.

« An upright Presbyterian minister, Stephen Soyers epito-
mized the Christian valucs and virtues of the antebel-
lum South. A strong opponent of chattel slavery and
the oppressive plantation system, he was nonetheless a
reluctant supporter of the Confederacy on constitu.
tional grounds. During the War Between the States, the
focus of his ministry in central Mississippi was on mercy
ministry. He and his wife had an extensive work among
the families of victims of the war—the poor, orphans,
widows, the disabled, and the dispossessed. When they
discovered that a number of midwives and nurse practi-
tioners were performing abortions and dispensing
chemical parricides among both the enslaved blacks
and free-born whites, they launched a protracted battle
against the minions of death. Though he did not sur
vive to see the end of the war, his work continued long
after—laying the foundations for Mississippi’s strong
pro-tife legislation passed at the turn of the century.
