Rescue the Perishing: The New Pro-Life Emergence 149

* In 1910, Agnes Gonxha Bojaxhiu was born in Skopje,
near what is today. the Albanian-Yugoslay border. After
receiving a Christian education, she joined the Sisters
of Toretto, an Irish order of missionary nuns working
in India. Taking the name Teresa, she taught in a con-
vent school for upper-class Indian girls for two decades
before founding a stum school in 1948. Two years later,
she began the Order of the Missionaries of Charity,
which was dedicated to serving the poor. Providing
food, medical care, and shelter to destitute lepers,
homeless children, and the poorest of the poor,
Teresa’s order has grown into a worldwide movement
of over cighteen hundred sisters in nearly two hundred.
branches. In 1979, she was awarded the Nobel Peace
Prize. To the dismay of many, she used the opportunity
to express her horror at the revival of abortion prac-
tices even within Christendom. In the days that fol
jowed, her protife pronouncements became both more
frequent and more strident. Even after her retirement
from active ministry in 1990, Mother Teresa has main-
tained a high profile wherever and whenever the de
fenseless are threatened.

 

* Variously called the “Guru of the Fundamentalists,”
“Missionary to the Intellectuals,” and “Godfather of
Evangelicalism,” Francis A. Schaeffer was undoubtedly
one of the most influential thinkers, theologians, au-
thors, and apologists of the past generation. After secre
ing for a short time in Presbyterian congregations in
the United States, he moved to Switzerland in 1948 to
begin a unique missionary outreach, to whoever God.
would send 10 his door. Over the years, literally thow
sands of students, skeptics, and searchers found. their
way to the door of the small mountain chalet that he
shared with his wife and four children, Calling his work
L’Abri—the French word for shelter—he set up a study
center and simply attempted to provide “honest an-
swers to honest questions.” Asserting the lordship of
Christ over the totality of life, he wrote a serics of intel-
lectually stimulating books documenting the drift of
