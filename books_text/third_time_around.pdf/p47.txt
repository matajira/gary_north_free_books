A New Consensus: The Medieval Church 35

of just three years, her household grew to imchide more
than forty handicapped children and another twenty men-
tally impaired adolescents and adults.

Before long, she had gained a remarkable reputation
for selflessness, graciousness, and charity. In the trying
times of the early eighth century, those were rare and wel-
come virtues. Barbarian hoards still threatened the fron-
tier. Norse raiders still terrorized the coastline. And petty
feudal rivalries continued to paralyze the interior, After the
fall of Rome in 476, Europe had lost its center of gravity
and became a spinning dreidel. Though Byzantium contin-
ued to flourish in the East, it was not until the slow but
steady encroachment of medievalism covered the entire
continent that any measure of peace or harmony could be
secured. Dympna’s character offered tangible hope that
the high aspirations of Christian civilization could be—and
indeed would be-—one day achicved. Thus, her work on
behalf of the distressed was widely heralded.

Perhaps too.widely.

Eadburh, upon hearing of his daughter's whereabouts,
followed her to Gheel. There was an awful confrontation.
When she refused to return home with him, he flew mto a
rage and slew her.

Amazingly, Dympna’s vision did not die with her that
day. Stricken with sorrow, the citizens of Gheel decided to
continue her mission of mercy. Her medieval ethic took
root in their lives and became their work. That work contin-
ues to the present day: it includes a hospital for the men-
tally ill, a foundling center, an adoption agency, and the
world’s largest and most efficient boarding-out program for
the afflicted and disturbed—run as a private and decentral-
ized association by the Christian families of Gheel.

 

The Dark Ages

The medieval period has commonly been called the Dark
Ages—as if the light of civilization had been unceremoni-
