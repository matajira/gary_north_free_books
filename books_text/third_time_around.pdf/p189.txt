10

ETERNAL VIGILANCE:
HANDING ON THE LEGACY

adhuc sub judice lis est'

 

The subtle barrier was draum which marks today from yesterday; all
the night and its despondency became past and entered memory.

Hilaire Belloc

Despotism can be a development, often a late development and very
often indeed the end of societies that have been very democratic. A
despotism may almost be defined as a tired democracy. As fatigue
falls on a community, the citizens are less inclined for that eternal
vigilance which has truly been called the price of liberty.

G. K. Chesterton

he vencrable aphorism remains as true today as
ever: “He who forgets his own history is con-
demned to repeat it.”?

Commenting on that profundity, the great Russian
iconodule Aleksandr Solzhenitsyn has said that, “If we
don’t know our own history, we will simply have to endure
all the same mistakes, sacrifices, and absurdities all over
again.”3

Further, Timothy K. Jones asserted that:

 

177
