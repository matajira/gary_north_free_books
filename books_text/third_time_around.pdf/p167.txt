Rescue the Perishing: The New Pro-Life Emergence 155

poimtment. As an institutional philanthropic enterprise, it
has been more than a little impotent. But as an outreach of
the historic church, it has had stunning success. The mod-
em pro-life movement has proven that commitment to the
sanctity of life is the consequence of the Spirit's work in
the authentic sacramental church.

Servanthood

The crux and power of the pro-tife movement today does
not lie in some centralized political bureaucracy lobbying
in the corridors of power. Instead, it lies im the deeds of
kindness and compassion—performed in thousands of cri-
sis pregnancy centers, unwed mothers’ homes, alternative
centers, shepherding homes, and mercy ministries all over
the country. The power of the pro-life movement today lies
in its servanthood orientation.

Urgency

More good people have suffered greater extremes of hard-
ship and persecution for the sake of the unborn today than
in any other single movement in recent memory. More
Christians have sacrificed lime, money, energy, reputation,
and even physical welfare than anyone could ever hope to
quantify. They have done this because of the more than fifty
million children killed by abortion every year around the
world. They have done this because the situation is urgent.

Patience

The most difficult lesson for modern Christians to learn is
this one. Partially because the great legacy of the past has
been forgotten for the most part—and so a bifurcation of
time has taken place—and partially because we live in fast
times calling for fast results and instant gratification, we
find working over the long haul a very difficult task. But
even here, we are learning.
