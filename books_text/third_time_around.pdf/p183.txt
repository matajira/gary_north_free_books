May the Circle Be Unbroken: The Lessuns af History Lf

community of faith in its progression through the months
(see Exodus 13:6-10; Psalm 31:15). The early church con-
tinued this stewardship of time, punctuating the years with
liturgical seasons: Advent, Christmas, Epiphany, Lent, Easter,
Asceusion, and Pentecost. Thus, God’s people were cn-
abled and equipped to run the race (see Philippians 2:16),
to fight the fight (see Ephesians 6:10-18), to finish the
course (see 2 Timothy 4:7), and to keep the faith (see 2
Timothy 3:10).

In order to maintain a sense of balanced urgency, the
pro-life heroes of the past paced their efforts through the
sanctification of the time. They were thus able to risk all,
remain decisive, and persevere with singlemindedness.
They were thus able to affirm with the psalmist:

J hear the slander of many; fear is on every side; they
counsel together against me, and they scheme to take
innocent life. But as for me, I will trust in You, O my
Lord; I will say, “You are my Sovereign.” All my times are
in Your hands. Therefore deliver me from the hand of
my enemies, and from those who persecute the inno-
cent. (Psalm 31:13-15)

Clearly, there is no room for procrastination or con-
templation in times of trouble, distress, and calamity. We
are called to seize the day. Decisiveness, determination,
singlemindedness, constancy, diligence, and passion must
inform our agenda. The pace we set should be fervent—be-
cause the task before us is urgent.

If we are going to effectually defend the sanctity of
life—either this time around or even next time around—
then we must learn this fundamental lesson. Nothing—not
the fear for material risk, not the concern over personal
reputation, and not the hesitation of institutional prece-
dent—nothing must be allowed to deter or detour us from
the indispensability of risk,
