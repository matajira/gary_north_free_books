How Firm a Foundation: The Apostolic Church 2L

well have jeopardized his standing, but he was driven by an
irrepressible spiritual imperative.

Hearing of Basil’s solitary crusade, the Emperor
Valentinian took the first step toward the full criminaliza-
tion of child-killing in 374 by decreeing, “All parents must
support their children conceived; those who brutalize or
abandon them should be subject to the full penalty pre-
scribed by law."

For the first time in human history, abortion, infanti-
cide, exposure, and abandonment were made illegitimate.
The sagae were driven underground and eventually out of
business altogether. The tradition of paterfamilias was all
but overturned. The exposure walls were destroyed. And
the high places were brought low.

When Basil died just four years later at the age of fifty,
he had not only made his mark on the church, he had also
altered the course of human history—and he had laid the
groundwork for the flowering of one of the greatest family
dynasties mankind has ever known.

The Biblical Mandate

Basil’s commitment to protecting innocent life was not
rooted simply in a personal preference or prejudice. He
did not subjectively pull his view of the sanctity of life out
of thin air. Instead, it was an obcdient response to God’s
own revelation: the Bible.

It was abundantly clear to him that the Scriptures com-
manded a reverence for life. Embedded in every book and
interwoven into every doctrme was the unwavering stan-
dard of justice and mercy for all: the weak and the strong,
the great and the small, the rich and the poor, the Jame
and the whole, the young and the old, and the born and
the unborn.

The Bible declares the sanctity of life in its account of
God’s creation (see Genesis 1:26-28; Psalm 36:9; Psalm
104:24-30; John 1:3-4; Acts 17:25; 1 Timothy 6:13).
