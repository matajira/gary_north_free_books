170 THE Nex TIME AROUND

passes. It is an ethical imperative to act and act quickly
when lives are at stake, when justice is perverted, when.
truth is in jeopardy, when mercy is at risk, when souls are
endangered, and when the gospel is assaulted.

‘We are admonished to “make the most of our time”
(Ephesians 5:16). We are to “redeem the time” (Colossians
4:5). We are to utilize “every day to the utmost” (Hebrews
3:13), In short, we are to sanctify the time (see Ecclesiastes
3:1-8).

According to the Bible, our time is not our own. It is
not ours to dispose of as we choose. We have been “bought
with a price” (1 Corinthians 6:20). Therefore we are to set
our days, weeks, and years apart to the Lord for His glory
(see Romans 14:6-12).

In the Old Testament, the days were divided into eight
distinct periods: dawn, morning, midday, dark, evening,
and three night watches, These were distinguished in the
lives of believers by times and seasons of prayer (see Psalm
55:17; Daniel 6:10). In the New Testament, the value of
this kind of liturgical clock was affirmed by the followers of
Christ who punctuated and accentuated their urgent task
of evangelization with the discipline of regular spiritual re-
freshment (see Acts 3:1).

Similarly, the weeks of God’s people were ordered with
purposeful sanctity. In the Old Testament, the week cen-
tered around the Sabbath and the attendant sacrifices. In
the New Testament, the week revolved around the Lord’s
day and the sacraments. Thus, each week had its own pace,
its own schedule, its own priorities, and its own order.
Thus, believers were able to give form to function and
function to form (see Deuteronomy 5:12; Hebrews 10:24—
25). The liturgical calendar enabled them to wait on the
Lord and thus to “run and not be weary” and to “walk and
not be faint” (Isaiah 40:31).

Even the years were given special structure and signifi-
cance to reinforce the Biblical conception of decisive ur-
gency. In ancient Israel, feasts, fasts, and festivals paced the
