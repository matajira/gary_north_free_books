Abominations and Admonitions: The Making of Medernism 137

nullified its import and impact at the very time it was most
needed. It abdicated its role as the pacesetter of art, music,
and ideas, yielding to a new cultural and scientific high
pricsthoed, A rapid slide into neo-paganism resulted, evi-
denced by the ascendency and acceptance of Hitler, Stalin,
and Sanger.

Servanthood

A disinterested church inevitably becomes a self-serving
church, After a flurry of evangelistic efforts around the
world in the nineteenth century, missionary activity
dropped precipitously in the twentieth, Relief and develop-
ment projects, works of mercy, the establishment of hospi-
tals, church planting, and outreaches to the needy ceased
to be priorities. The unborn, the infirm, the unwanted, and
the undesired simply could not compete with the jangling
primacy for personal peace and affluence.

Urgency

Righteous indignation and holy zeal became all but endan-
gered species during much of the century. Passions were
turned inward, as were devotions. Virtues became vices,
and the most awful of indulgences became canonized or-
thodoxies. Risk, jeopardy, and self-sacrifice were replaced
by security, certainty, and sel(-gratification. Thus, the only
urgency that drove much of the church during this dark
period in history was its own satisfaction.

Patience

It was not assurance in the promises of God that stirred
and motivated the bulk of the church in the twentieth cen-
tury as much as it was the promises of science or experi-
ence. As a result, the attribute of patience virtually disap-
peared from Christendom; liberals opted for revolutionary
tactics, and conservatives resorted to personal existential
