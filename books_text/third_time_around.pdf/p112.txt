100 THE SECOND TIME AROUND

cents. For a glittering moment, I have seen Christ’s Body
as one.?#

Se monolithic was the consensus that the church
forged, that even the most ardent feminists had to rally
under the pro-life banner. Susan B. Anthony, for instance,
declared:

T deplore the horrible crime of child murder. We must
reach the root of the evil. It is practiced by those whose
inmost souls revolt from dreadful need. No matter what
the motive, love of ease, or a desire to save from sufler-
ing the unborn innocent, the woman is awfully guilty
who commits the deed; but oh, thrice guilty is he who
drove her to the desperation which impelled her to the
crime.?5

Elizabeth Cady Stanton agreed:

When we consider that women are often treated as prop-
erty, it is degrading to women all the more that we
should treat our children as property to be disposed of
as we see fit. 26

As did Matilda Gage:

This subject lies deeper down into woman’s wrongs than
any other, But, the crime of abortion is not one in which
the guilt lies soley [sic] with the woman. Indeed, I hesi-
tate not to assert that most of this crime of child murder,
abortion, or infanticide, lies at the door of all men.27

In less than two decades, the church was able to mar-
shall hostile journalists, ambivalent physicians, reticent poli-
ticians, and even radical feminists to the cause of exploited
mothers and their helpless unborn. They succeeded over-
whelmingly. And they restored the foundations of a glori-
ous legacy of freedom and justice that had always been at
the heart of the remarkable American experiment.
