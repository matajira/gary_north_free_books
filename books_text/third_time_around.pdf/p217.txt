Bibliographic Resources 205

Kinsey, Alfred C., Wardell C. Pomeroy, and Clyde E. Martin. Sex
ual Behavior in the Human Male, New York: W. B. Saunders
Co., 1948.

Rasen, Thomas G. A Pro-Life Manifesto. Westchester, IL: Crossway
Books, 1988.

Kline, Meredith. Treaty of the Great King: The Covenant Structure of
Deuteronomy. Grand Rapids, Ml: Wm. 8. Eerdman’s Publish-
ing Co., 1963.

  

Koop, C. Kverett. Yo Live or Die? Facing Decisions at the
Ann Arbor, MI: Servant Books, 1987.

dof Life,

 

Koster, John PB. The Atheist Syndrome. Brentwood, TN: Wolgemuth
& Hyatt, Publishers, Inc., 1989.

Kowalezyk, John. An Orthodox View of Abortion. Minneapolis, MN:
Light and Life Publishing Co., 1987.

Kreeft, Peter. The Unaborted Socrates. Downer’s Grove, EL: Inter-
Varsity Press, 1983.

Lane, Harold K. Liberty! Gry Liberty! Boston: Lamb and Lamb
Tractarian Society, 1939.

Larue, Gerald A. Euthanasia and Religion. Los Angeles, CA: Hem-
lock Society, 1985.

Latourette, Kenneth Scott. A History of Christianity. New York:
Harper Brothers, 1953.

 

Lester, Lane P. with James C. Ilefley. Cloning: Miracle or Menace.
Wheaton, IL: lyndale House, 1980.

Lifton, Robert J. The Nazi Doctors: Medical Killing and the Psychology
of Genocide. New York: Basic Books, 1986.

Lightfoot, J. B., and J. R. Harmer, eds. The Apostolic Fathers.
Grand Rapids, MI: Baker Book House, 1989.

Lincoln, Abraham. Speeches, Letters, and Papers: 1860-1864, Wash-
ington, DC: Gapitol Library, 1951.

 

Lindstrom, Paul D, 4 Days in May: Storming the Gates of Hell. Ar-
lington, IL: Christian Liberty Press, 1988.
