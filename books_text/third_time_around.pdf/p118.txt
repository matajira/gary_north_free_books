106

THE SECOND TIME AROUND

© Louis Jennings was a pioneer in American journalism.
The editor of the New York Times during the last de-
cades of the nineteenth century, he helped to build the
reputation of that paper, ultimately making it the pre-
mier daily in the entire nation. He was also a commit-
ted Christian and a stalwart in the struggle for life. In
1870, Jennings began a crusade against abortion on the
editorial pages of his paper that finally lead to the
criminalization of the procedure in every state in the
Union. He understood the power of the printed page
and utilized it expertly. He knew only too well that it
would be necessary to provoke a public outrage over
the issue, not simply a stiffening of legislation that
might go unenforced. It was his leadership, and the na-
tional visibility of his paper that ultimately swayed both
the legal and the medical establishments to publicly de-
nounce abortion as murder.

° Leslie Printice was a young widow in New York City when
she first became active in the pro-life miovement A
member of Gardiner Spring’s congregation at the
prominent Brick Presbyterian Church, she was encour-
aged by his sermons on child-killing to take a bold and
active stand. She organized several meetings in her
midtown Manhattan brownstone of doctors, lawyers,
politicians, judges, and community leaders to hear the
facts about the abortion wade. Under the auspices of
the church she set up the New York Parent and Child
Committee. The committee established prayer net-
works, sidewalk counseling shifts, and even alternative
care programs with Christian doctors. It also organized
regular protests in front of Anna Lohman’s five area
abortion franchises. Known professionally as Madame
Restell, Lohman was the boldest, richest, and most visi-
ble child-killer. Tenacious and unrelenting, Leslie led a
rally outside Lohman’s lavish home in 1846 that was by
turns emotional, physical, and fierce. When Lohman
went to trial for the first Gme the next year, Leslie was
there—despite innumerable threats on her life from a
number of the gangsters on Lohman’s payroll—to tes-
