98 ‘THE SECOND TIME AROUND

Genesis to Revelation—and that abortion is thus “no less
than murder.”!9

In 1868, a conference of the Congregationalist
churches issued a stinging denunciation of abortion and its
shockingly wide prevalence. It urged faithful believers to
stand firm in their Scriptural convictions in the strongest
possible terms:

Let imagination draw the darkest picture that reason or
taste could allow, and it would fail to set forth adequately
the outlines and shocking details of this practice. Our
investigation has brought us to the belief that after all
the proper deductions, full one third of the natural pop-
ulation of our land, fails by the hand of violence; that in
no one year of the late war have so many lost life in
camp or battle, as have failed of life by reason of this
horrid home crime. We shudder to view the horrors of
intemperance, of slavery, and of war; but those who best
know the facts and bearing of this crime, declare it to be
a greater evil, more demoralizing and destructive, than
either intemperance, slavery, or war itself?”

The report went on to prophetically condemn the las-
civious and concupiscent lifestyles that drove young men
and women to such extremes:

Fashion, inexorable, tyrannical, with its whirl of amuse-
ments and frivolous enjoyments, has come to demand of
religion not only, but of nature herself, that they bend to
her despotism, and sacrifice on her bloody altar. A low
love of pleasure and ease, vitiated tastes, perverted views
of life, and ruined moral sentiments have so wrought
themselves into our civilization as a low and vicious
leaven, that we have come to consent to customs and
habits that will destroy us as a people inevitably, unless
arrested betimes.”!

In a similar fashion, the Presbyterian churches
adopted a stridently pro-life position at their general as-
sembly in 1869:
