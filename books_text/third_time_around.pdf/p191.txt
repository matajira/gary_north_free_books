Eternal Vigilance: Handing on the Legacy 179

ing considering the fact that the vast proportion of its own
contents record the dealings of God with men and nations
throughout the ages.

Again and again in the Scriptures, God calls upon His
people to remember. He calls on us to remember the bond-
age, oppression, and deliverance of Egypt (see Exodus
13:3; Deuteronomy 6:20~23). He calls on us to remember
the splendor, strength, and devotion of the Davidic king-
dom (see | Chronicles 16:8-36). He calls on us to remem-
ber the valor, forthrightness, and holiness of the prophets
(see James 5:7~11}. He calls on us to remember the glories
of creation (see Psalm 104:1-30), the devastation of the
flood (see 2 Peter 2:4-11), the judgment of the great apos-
tasies (see Jude 5-11), the miraculous events of the exodus
(see Deuteronomy 5:15), the anguish of the desert wander-
ings (see Deuteronomy 8:1-6), the grief of the Babylonian
exile (see Psalm 137:1-6), the responsibility of the restora-
tion (see Ezra 9:5-15), the sanctity of the Lord’s Day (see
Exodus 20:8), the graciousness of the commandments (see
Numbers 15:39-40), and the ultimate victory of the cross
(see 1 Corinthians 11:23-26). He calls on us to remember
the lives and witness of all those who have gone before us
in faith—forefathers, fathers, patriarchs, prophets, apostles,
preachers, evarigelists, martyrs, confessors, ascetics, and
every righteous spirit made pure in Christ (see 1 Corinthi-
ans 10:1-11; Hebrews 11:3-40).

He calls on us to remember. As the psalmist has said:

We must remember the deeds of the Lorp in our midst.
Surcly, we must remember His wonders of old. I will
meditate on all Your work, and muse on Your deeds.
Your way is holy; what god is like unto our God? You are
the God who works wonders; You have made known Your
surength among the peoples. You have by power re-
deemed Your people, the sons of Jacob and Joseph.
{Psalm 77;:11-15)

And again:
