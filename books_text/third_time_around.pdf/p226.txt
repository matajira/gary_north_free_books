214 BIBLIOGRAPIIC RESOURCES

Tyrrell, R. Emmett, Jr. Orthodoxy: The American Spectator Anniver-
sary Anthology. New York: Harper and Row, 1987.

Unger, Ken. True Sexuality. Wheaton, IL; Tyndale House, 1987.

U.S. Commission on Human Rights, Hearing Before the United
State: Commission on Civil Rights: Protection of Handicapped New-
borns, Washington, DC: U.S, Printing Office, 1985.

Vertefeuille, John. Sexual Chaos: The Personal and Social Conse-
quences of the Sexual Revolution. Westchester, IL: Crossway
Books, 1988.

Veyn, Paul. From Pagan Rome to Byzantium. Vol. 1 of A History of a
Private Life. Cambridge, MA: Belknap Press, 1987.

Walton, Rus. One Nation Under God. Nashville, TN: Thomas Nel-
son, 1987.

Ware, Timothy, The Orthedox Church. New York: Penguin Books,
1963.

Wattenberg, Ben J. The Birth Dearth: What Happens When People in
Free Countries Don’t Have Enough Babies. New York: Pharos
Books, 1987.

Weiner, Roberta. Teen Pregnancy: Impact on the Schools. Alexandria,
VA: Capitol Pub, Inc, 1987.

Weiss, Daniel Evan, 100 percent American. New York: Poseidon
Press, 1988.

Whitehead, John W. Arresting Abortion: Practical Ways to Save Un-
born Children. Westchester, IL: Crossway Books, 1985,

Whitchead, John W. The Separation Hlusion. Milford, MI: Mott
Media, 1977.

Willke, Dr. and Mrs, J. C, Abortion: Questions and Answers.
Cincinarti, OH: Hayes Pub. Co., 1985.

Willke, Dr. and Mrs. J. C. Handbook on Abortion. Cincinatti, OH:
Hayes Pub. Co., 1979.

Willke, J. C. Abortion and Slavery: History Repeats, Cincinatti, OH:
Hayes Pub. Co., 1984,
