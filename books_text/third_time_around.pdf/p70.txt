58 THE SECOND TIME AROUND

ties seriously. They began to stand firm in the face of en-
croaching heathenism, And they began to enter into the
battle for life.

‘The elimination altogether of the monasteries and rcli-
gious orders of northern Europe following the meliorations
of the Reformers necessitated a whole new approach to
pro-life activity there. Similarly, the house-cleaning of the
Counter-Reformation. produced new conditions in south-
ern Europe that made many of the old medieval pro-life
methodologies and institutions obsolescent. But rather
than being an obstacle to the ministry of mercy and com-
passion, these new situations and circumstances actually be-
came a catalyst for growth and maturity.

And once again, the church was unanimous in its com-
mitment to life, John Calvin, the leader of the Swiss Refor-
mation said:

The unborn child . . . though enclosed in the womb of
its mother, is already a human being . . . and should not
be robbed of the life which it has not yet begun. to enjoy.
If it seems more horrible to kill a man in his own house
than in a field, because a man’s house is his place of
most secure refuge, it ought surely to be deemed more
atrocious to destroy an unborn child in the womb before
it has come to light?

Defense of the innocents, he argued, was so integral
and indistinguishable from a defense of the gospel that be-
licvers ought to be just as willing to risk severe persecution
for the one as for the other:

Now to suffer persecution for righteousness’ sake is a sin-
gular comfort. For it ought to occur to us how much
honor God bestows upon us in thus furnishing us with
the special badge of His soldiery. I say that not only they
that labor for the defense of the gospel, but they that in
any way maintain the cause of righteousness suffer perse-
cution for righteousness. Therefore, whether declaring
God’s truth against Satan's falsehoods or in taking up
