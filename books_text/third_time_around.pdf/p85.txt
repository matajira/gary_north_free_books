To the Uttermost: The Great Missions Movement 73

Colonizing the Globe

The accession of the Christian culture of Europe as the
world’s dominating sociopolitical force was actually not as-
sured until well into the nineteenth century. In fact, for the
bulk of its first two millennia, Christian culture had been
strikingly unsuccessful in spreading its deleterious effects
beyond European shores, In the Far East, for instance, mis-
sionary endeavors were practically non-existent in China
and paralyzed. by persecution in Japan. In India, the higher
castes were virtually untouched by the gospel, and even the
lower castcs showed only transitory interest. The Islamic
lands were as resistant as always to the inroads of the
church. South America’s conversion to the conquistadors’
Catholicism was tenuous at best. And tropical Africa had
proven to be so formidable and inhospitable that Western
settlements were confined to a few small outposts along the
coast. Clearly, Christianity was still very much a white man’s
religion.

There had been, of course, a [ew bursts of expansion.
In 1453, a series of catastrophic events—both good and
bad—freed European monarchs to cast their vision out-
ward for the first time since the early crusades. That year
saw the defcat of Constantine XI by Sultan Mohammed
II—thus, at Jong last, bringing to an end the storied Byzan-
tine Empire. In addition, the Hundred Years War between.
England and France ended, as did the wars between Ven-
ice and Milan, Russia and Lithuania, and Prussia and Po-
land. The Habsburgs and the Medicis were both bolstered
in their respective realms. And Guttenberg’s press in Mainz
altered the transmission of knowledge and culture forever
with the publication of the first printed book, a Bible.

Explorers began to venture out into uncharted realms.
Scientists began to probe long hidden mysteries. Traders
and merchants carved out new routes, new markets, and
new technologies. Energies that had previously been de-
voted exclusively to survival were redirected by local magis-

 
