144 THE THIRD TIME AROUND

of the American political and cultural landscape. And,
though victories seemed to be few and far between, much
of the church was at long last intimately involved in the
affairs of God’s world once again.

Reinventing the Wheel

As evangelicals began to rediscover their Biblical responsi-
bilities in the world, the issue of abortion emerged as their
premier concern, For many who had been honest enough
to genuinely investigate it, abortion was no longer merely a
matter of theological or legal debate. It was the horrifying
and brutal destruction of human life—children ripped
limb from limb or burned to death by toxic solutions or
crushed and vacuumed out of the womb,

For them, abortion ceased to be an issue. An tissue is,
after all, something that we can reasonably and rationally
discuss around a negotiating table. An isswe is something
that we can compromise on. It is something that involves
give and take. It is something that we can ponder, argue,
and debate. Indeed, it is something that good men and
women can legitimately disagree on. We can juggle its nig-
gling little points back and forth. Or we can do nothing at
all. We can take it or leave it.

As they became more and better informed, Christians
realized that abortion is none of those things. Instead, it is
a matter of life and death, It is a test of faith. It is perhaps
the ultimate test of faith in these difficult and complex
times. And thus, they discovered, it demands uncompro-
mising, unwavering, and unhesitating faithful action.

It was not long before a number of influential voices
began calling on all Christians—Protestant and Orthodox,
as well as Catholic—to answer that call to action:

« In 1973, shortly after the Roe v. Wade decision, James
McFaddon, a longtime National Review staffer, launches
