The Sociology of Suffering 217

representing God and Christ in history. Persecution first, Jesus
and John warned; then the godly exercise of righteous judg-
ment. In this case, the Church was spared God’s negative sanc-
tions. The Church survived, and by surviving, exercised judg-
ment against Old Covenant Israel. Just as Lot brought judg-
ment against Sodom by surviving, just as Moses brought judg-
ment against Egypt in the Red Sea by surviving, just as Daniel
brought judgment against Babylon on that final night by surviv-
ing, so docs the Church of Jesus Christ bring judgment against
the false kingdoms of this world by surviving. The Church
announces God’s covenant lawsuit, and then it awaits God's
negative sanctions against that society which refuses to repent.

But what about Mark 13:10? “And the gospel must first be
published among all nations.” This has to happen in the days of
the looming persecution. Until this kingdom program is fulfil-
led, we are still in the era of persecution, eschatologically speak-
ing. Isn’t this fulfillment still in the future? Not according to
Paul's interpretation the Church of his day. “If ye continue in
the faith grounded and scttled, and be not moved away from
the hope of the gospel, which ye have heard, and which was
preached to every creature which is under heaven; whereof I
Paul am made a minister” (Col. 1:23). Which was preached: his
words could not be any plainer. Paul’s words are na doubt
figurative; they refer to a representative hearing: “. . . the hope of
the gospel, which ye have heard, and which was preached to
every creature which is under heaven.” No one takes all of his
words literally, ie., every creature under heaven: bugs, mice,
etc. Most theologians choose the safer path: to ignore the verse.
But however interpreted, Paul made it clear that these words of
Christ had already been fulfilled in Paul’s day: “And the gospel
must first be published among all nations.” All of these prophe-
sies were fulfilled by A.D. 71: the looming persecution, the
preaching of the gospel to the whole world, the delivery of the
disciples before judges, and their sitting in final judgment over
Old Covenant Israel. Their sitting in judgment over Israel was
fulfilled representatively, yet no less definitively, for Old Cove-
nant Israel is no more.

My conclusion is this: while there can be and is persecution
