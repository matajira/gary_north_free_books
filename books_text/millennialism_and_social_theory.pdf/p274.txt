258 MILLENNIALI5SM AND SOCIAL THEORY

can be sustained apart from continuing widespread conversions
to saving faith? The answer is no.'* Without continuing evan-
gelism and a manifestation of the irresistible grace of God in
history, we can expect nothing better than what New England
experienced: a slow erosion from Calvinism to the rule of law-
yers and merchants, to Arminianism, to Unitarianism, and
finally to Teddy Kennedy. There must be divine intervention
from outside of history (discontinuity) in order to sustain the
blessings of God in history (continuity). Meanwhile, we must
obey God. Continuously. Our job is continuity. God’s job is
discontinuity.

The Christian Church today faces a horrendous problem: it
has no answers to the question, “What is to be done?” It has
not even thought about an appropriate answer. It has denied
the only foundation for constructing a working alternative to
humanism: the biblical covenant model. Its theologians and
leaders have consistently and publicly denied: (1) the continuing
validity of Old Testament law in New Covenant society, (2)
God’s predictable historical sanctions, and (3) the coming of a
millennial era of blessings inaugurated by the Holy Spirit
through the preaching of the gospel and the application of
Gad’s law to human problems.

Because Christian scholars have denied these fundamental
biblical doctrines, they have been unable to formulate a specifi-
cally biblical social theory. They have generally denied that such
a formulation is even possible. They have repeated the liberals’
refrain, “The Bible gives us no blucprints for society.” This of
necessity has led to a fruitless quest to discover non-biblical hu-
manist blueprints that can somehow be made to fit “biblical prin-
ciples,” carefully undefined. This is baptized humanism, and it
has been a way of life for the Church for almost two millennia.

No Time for Silence

With the escalating epistemological, moral, and institutional
collapse of Western humanism, and the growing skepticism that

14, Gary North, Dominion anid Common Grace: The Biblical Basis of Progress (Tyler,
‘Texas: Institute for Christian Economics, 1987), ch. 6.
