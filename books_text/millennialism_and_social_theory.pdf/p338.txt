322 MILLENNIALISM AND SOCIAL THEORY

army of conquest. He is not to see his membership as passive.
He is not there to be entertained.

The Church of Jesus Christ is not a biblical substitute for the
theater, no matter how successful modern ecclesiastical enter-
tainment centers appear to be today, when there is no crisis.
Entertainment churches will not survive a major crisis unless
they become serious. Such churches would not have made it
during the bubonic plague in 1348-50, and they will not survive
God's coming negative sanctions to serve as base camps for
subsequent Christian reconstruction.

The ecclesiastical goal is clear: we must abandon the modern
bureaucratic ideal of the megachurch-entertainment center,
which has too many problems in continuity when the church’s
ringmaster-pastor leaves, retires, dies, or runs off with the choir
director’s teenage daughter. The modern megachurch concen-
trates Christian resources too much, especially human resources.
While a large, well-equipped building is legitimate for the occa-
sional multi-congregation services in the region, it should not be
a permanent local church. In between common services, it
should function as a regional Christian high school, into which
“feeder schools” can scnd their graduates.

In 1986, a study of membership growth in Brazilian churches
revealed this fact: the smaller the member-to-leader ratio, the
faster the growth. The Assemblies of God had a 50-to-one ratio,
while Lutherans were at 1,000-to-one. The Roman Catholics
were 9,000-to-one.®

Organic analogies are dangerous unless they are governed by
the principles of biblical covcnantalism. This analogy is. The
Church's goal is to imitate the amoeba. An amoeba does not die;
it just divides. These new units then divide. Then they also di-
vide. The species multiplies within its host. The churches need
to do something very similar. Eventually, modern humanist cul-
ture will develop a terminal case of Trinitarian intestinal flu
(‘Augustine’s Revenge”).

15. “Protestants Create an Altered State,” Insight Guly 16, 1990), p. 14.
