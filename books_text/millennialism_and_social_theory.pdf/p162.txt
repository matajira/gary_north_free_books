146 MILLENNIALISM AND SOCIAL THEORY

of exclusively personal evangelism that has one primary message
to every generation, decade after decade: flee the imminent wrath
to come, whether the Antichrist’s (the Great Tribulation) or the
State’s (“unnecessary persecution”). This is a denial of the great-
ness of the Great Commission, yet all in the name of the
Great Commission: “Our vision is to obey and fulfill the com-
mand of the Great Commission.”

Mr. Lewis says that we can legitimately participate in politics
as individuals, since our government is democratic: “. . . we en-
courage Christians to get involved on an individual basis, in all
realms of society, including the political arena.” Should our goal
be to change society fundamentally? Hardly. This is an impos-
sible goal. Our goal is to gain new contacts in order to share the
gospel with them. “This is partly to insure that Christians are in
place in every strata of socicty for the purpose of sharing the
gospel message.”*° ‘The purpose of political and social involve-
ment is not to reform the world; it is to tell people about the
imminent end of this pre-millennium world. We are apparently
not supposed to say anything explicitly Christian or vote as an
organized bloc (the way that all other special-interest groups
expect to gain political influence). “To be involved in our
governmental process is desirable; however, it is quite another
matter for the Church to strive to become Caesar.”**

Mr. Lewis does not understand politics: one does not get in-
volved in order to lose; one gets involved in order to win. He
also does not understand society: one does not make the neces-

24. Kenneth L. Gentry, Ju,, The Greatness of the Great Commission: The Christian Enter-
prise in a Falien World (Tyler, Texas: Institute for Christian Economics, 1990}.

25. Lewis, Prophecy 2000, p. 282.

26. Idem.

27, ‘This is traditional democratic theory, but it has never really come to grips with
the reality of political power. The Councit on Foreign Relations and the Trilateral
Commission do not organize voters into blocs. They simply make sure that hey control
who gets appointed to the highest seats of power and what policies are enacted. This
raises other questions, which, being political, are not the focus of my concern here. See
Gary North, Conspiracy: A Biblical View (Ft. Worth, Texas: Dominion Press, 1986; co-
published by Crossway Books, Westchester, Illinois). See also Philip II. Burch, Etites in
American History, 3 vols. (New York: Holmes & Meier, 1980-81); Carroll Quigley, Tragedy
and Hope: A History of the World in Our Time (New York: Macmillan, 1966).

28. Lewis, Prophecy 2000, p. 277.
