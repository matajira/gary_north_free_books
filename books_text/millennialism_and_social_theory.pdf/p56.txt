40 MILLENNIALISM AND SOCIAL THEORY

history are a unit. This is not to deny that God’s absolute pre-
destinating sovereignty is what guarantees His kingdom’s histor-
ical triumph, or that Christians, as members of God’s Church,
are not God's kingdom representatives in history. But the great
debate has come over the inextricable relationship between
biblical law, God’s historical sanctions, and cultural progress
over time. Yet most modern covenant theologians expressly
deny this connection.” They also refuse to define covenant. This
has been going on for four centuries.

A Question of Sanctions

“The history of all hitherto existing society is the history of
class struggles.” This is the opening paragraph of Part I of the
Communist Manifesto (1848).?! The fact that Marx never did
define “class” {i.e., hierarchy) in terms of his theoretical system
did not in the least hinder the growth of the Communist move-
ment.” He clearly had a unified concept of sanctions and esch-
atology, and it was this belief, above all, that motivated his disci-
ples: “Centralisation of the means of production and sociali-
sation of labour at last reach a point where they become incom-
patible with their capitalist integument [covering]. This integu-
ment is burst asunder. The knell of capitalist private property
sounds. The expropriators are expropriated.”

Marx was wrong. In 1989, the death knell of Marxist Commu-
nism sounded, except in Red China, and even in this case, it was
only the application of military sanctions against unarmed stu-
dents that gave Chinese Communism a stay of execution. This
was viewed by the whole non-Communist world on satellite
television, and Red China lost any claim to moral legitimacy.
Lost legitimacy is very difficult to regain. Non-Chinese Commu-
nism lost its moral legitimacy in a much less spectacular way.

20. See Chapter 7, below.

21. Karl Marx and Frederick Engels, “Manifesto of the Communist Party,” in Marx
and Engels, Callected Works (New York: International Publishers, 1976), VI, p. 482.

22. He began to define “class” in the last few paragraphs of his posthumously pub-
lished third volume of Capital. The manuscript then breaks off. He lived for another
fifteen ycars after he ceased working on it.

23. Karl Marx, Capital (New York: Modern Library, [1867)), p. 837.
