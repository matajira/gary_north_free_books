186 MILLENNIALISM AND SOCIAL THEORY

Honesty as the Best Policy

Why do I argue that without the idea of predictable sanc-
tions in history, there can be no social theory of any kind?
Because, first, I am unaware of any social philosophy in history
that has ever denied all forms of predictable sanctions.’ Some
system of predictable sanctions in history must exist if social
theory is conceivable. The question is: Whose sanctions? Second,
IT cannot conceive of such a sanctions-less system. Neither can
you. I think I can prove this. As a simple case study, consider
the familiar aphorism Cervantes’ Don Quixote, “Honesty is the
best policy.” (As in so many other instances, Ben Franklin is
erroneously given credit for having said this first.) In what sense
is this aphorism true? Personally? Culturally? Where is the
proof? What are the legitimate criteria of proof?

What if God's corporate sanctions in history were perverse,
which is what pessimillennialism teaches? What if honesty were
to lead to economic poverty in most individual cases? Then it
must also lead to poverty corporately. Would it still be the best
policy? Only if we insist that only beyond the grave, though not
in history, will honest individuals receive their appropriate re-
wards. (This is the pessimillennialist’s assertion.) But then only
those people who believe in God’s sanctions in a world beyond
the grave would take the aphorism seriously.” In the meantime
— that is, in time — most people would pursue dishonesty. After
all, dishonesty pays in history. Even if honesty and dishonesty
were rewarded equally — i.e., Muether’s inscrutability doctrine
— this state of affairs would serve as a subsidy to dishonesty in a
world in which original sin prevails. The dishonest person
would not be any worse off in history than the honest person.

If we are to examine the truth of the aphorism that honesty
is the best policy, we must ask, answer, and then apply to the
aphorism the Bible's five covenantal’ questions:

 

1. I have been working on a book, Heaven or Hell on Earth: The Sociology of Final
Judgment. {have found that humanist societies immanentize the final judgment.

2, I regard it as ominous that in our day, ostensibly orthodox Christian theologians
have begun ta deny the doctrine of helt and the eternal lake of Gre, into which hell's
contents will be dumped on judgment day (Rev. 20:14-15).

3. On the five-point covenant model, see Ray R. Sutton, That You May Prasper: Domain
