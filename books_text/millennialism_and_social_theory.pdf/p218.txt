202 MILLENNIALISM AND SOCIAL THEORY

the message of this dispensational realism? Pessimism.

“Man cannot change the world.” What in the world does this
mean? That man is a robot? That God does everything all
alone, for both good and evil? Walvoord obviously does not
mean this. So, what does he mean? That men collectively can
do evil but not good? Then what effect does the gospel have in
history? If he does not want to make this preposterous conclu-
sion, then he must mean that men who act apart from God’s will,
God's law, and God’s Holy Spirit cannot improve the world, long-
term. If God is willing to tolerate the victory of evil, there is
nothing that Christians can do about it except try to get out of
the way of the victorious sinners if we possibly can, while hand-
ing out gospel tracts on street corners and running local rescue
missions. The question is: fs God willing tolerate the triumph of
sinners over His Church in history? Ys, say premillennialists
and amillennialists. No, say postmillennialists.

What Walvoord is implying but not saying is that the post-
millennialists’ doctrine of the historical power of regencration,
the historical power of the Holy Spirit, the historical power of
biblical law, God's historical sanctions, and the continuing New
Testament validity of God's dominion covenant with man (Gen.
1:26-28) is theologically erroneous, and perhaps even border-
line heretical. But this, of course, is precisely the reason we
postmillennialists refer to premillennialists as pessimistic. They
implicitly hold the reverse doctrinal viewpoints: the historical
lack of power of regeneration, the historical lack of power of
the Holy Spirit, the historical lack of power of biblical law, and
the present suspension of God’s dominion covenant with man.
(Carl McIntire’s tiny, premillennial, Bible Presbyterian Church
in 1970 went on record officially as condemning any New Tes-
tament application to society of Cod’s cultural mandate of Gen-
esis 1:28.)

Walvoord says that only God can change the world. Quite
true. But who does he think the postmillennialists believe will
change the world for the better? Of course God must change

29. Resolution No. 13, reprinted in R. J. Rushdoony, The Institutes of Biblical Law
(Nutley, New Jersey: Craig Press, 1978), pp. 723-24.
