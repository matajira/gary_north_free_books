240 MILLENNIALISM AND SOCIAL THEORY

The great promoters of early modern postmillennialism were
the Scottish Calvinists and English Puritans of the seventeenth
century. It was they who wrote the Westminster Confession of
Faith and the Larger Catechism, Answer 191 of which is post-
millennial. It was dispensationalism that appeared late — about
1830 — rather than postmillennialism.’ Postmillennialism has
been linked to a defense of biblical law only in the case of the
Puritans, especially the New England Puritans,° and the Chris-
tian Reconstruction movement. In this sense, Christian Recon-
struction is socially (judicially) neo-Puritan.’ It is not an heir of
the other Puritan tradition, best represented by the mid-seven-
teenth-century pietist expositor, William Gurnall.* The pietist
wing of Puritanism emphasized the discipline of personal intro-
spection, extended prayer, and personal, individualistic ethics to
the exclusion of programs for social transformation. It was more
Baptist-individualist in outlook than Presbyterian-covenantal. It
is with us still.

258-54, 806-7. T had challenged Ice on this point several months before Dominion Theology
appeared, in our debate in Dallas. Ice said absolutely nothing in response, yet he dishon-
estly repeated the myth in his book later in the year. (Audiatapes of this nationally broad-
cast radio debate are available from the Institute for Christian Economics.) The myth is
repeated by Marvin Rosenthal, The Pre-Wrath Rapture of the Church (Nashville, Tennessee:
Nelson, 1990), p. 50, and by David Allen Lewis in his book, Prophecy 2000 (Green Forest,
Arkansas: New Leaf Press, 1990), p. 275. Mr. Lewis cites as proof Dominion Theology. The
myth is also promoted by Robert P Lightner, Th.D., a professor at Dallas Seminary: The
Last Days Handivok (Nashville, Tennessee: Nelson, 1990), p. 80. ‘These authors are not
well read in Church history. Professors House and Lightner have no excuse.

5. In response, the standard dispensational apologetic is to point to the ancient
origins of premillenniatism, thereby deflecting attention from the main historical ques-
tion: the date of the origin of dispensationalism. This deflection technique bas worked
quite well in dispensational seminary classrooms, at least with C-average students, so the
defenders of dispensationalism continue to repeat the refrain outside the classroom. Then
they wonder why other theologians and serious Bible students do not take them or their
theology scriously. See, for example, John Walvoord’s review of House Divided in Biblio
theca Sacra (July-Sept. 1990), almost. all of which is devoted to a recapitulation of the
history of non-dispensational premillemnialism.

6. Gary North (ed.), Symposium on Puritanism and Law, Jowmad of Christian Recon-
struction, V (Winter 1978-79).

7. Christian Reconstraction's adoption of the presuppositional apologetics of Cor-
nelius Van Til distinguishes it from the older Puritanism, which was still infused with
secular rationalism.

8. William Gurnall, 4 Christian in Complete Armour, 2 vols. (London: Banner of Truth
Trust, [1655-62] 1964).

9. The Banner of Truth Trust in Scotland and England defends this older, pietistic

 
