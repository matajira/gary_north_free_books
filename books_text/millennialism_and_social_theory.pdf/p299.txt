Our Blessed Earthly Hope in History 283

A Question of Continuity

Institutionally, what do we possess to aid us in our work of
evangelism and cultural conquest? First, we have the Holy Spir-
it, who is God. This means we are in God's presence at ail
times. This is point onc of the covenant: transcendence, yet
presence. Second, we have the three covenantal, hierarchical
institutions established by God: Church, State, and family. Third,
we have God's law and His revelation of Himself in the Bible.
Fourth, we have earthly access to the implements of God’s heav-
enly sanctions: the sacraments of baptism (covenantal disconti-
nuity and a new inheritance) and the Lord’s Supper (special
covenantal presence and renewal). Fifth, we have God’s promise
of both historical continuity and cultural victory in history.

The premillennialist affirms victory but not historical conti-
nuity. The amillennialist affirms historical continuity but not
victory. Only the postmillennialist affirms both historical continuity and
viciory. This three-way division within the Church has led to the
abandonment of biblical covenantalism. The churches have
therefore adopted one of the two rival views of society: organi-
cism or contractualism.* Organicism favors ecclesiocracy (unity of
Church and State), while contractualism favors religious pluralism
(the legal separation of Christianity and State). Covenantalism
separates Church from State and fuses Christianity and State.
There can never be separation of religion and State in any
system. The question is: Which religion?

The goal of the biblical covenantalist is to bring all the insti-
tutions of life under the rule of God’s covenant law. The State
imposes negative sanctions against specified public acts of evil.
The churches preach the gospel and proclaim God’s law. The
family acts as the primary agent of dominion. Voluntary corpor-
ations of all kinds are established to achieve both profitable and
charitable goals. Working together under the overall jurisdic-
tion of God’s revealed law, these institutions can flourish. Bibli-
cal covenantalism produces an open society. It did in Old Cove-
nant times; it docs today. To deny this is 10 argue that God

6. See Chapter 2, pp. 54-87, 38-39.
