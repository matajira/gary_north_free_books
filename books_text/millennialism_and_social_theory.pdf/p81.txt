Covenantal Progress 65

not all that he hath, he cannot be my disciple (Luke 14:28-33),

The point is, as we mature in the faith, these trade-offs be-
tween sin and righteousness become less burdensome, i.e., less
costly. When we walk on God’s path, the “alternative income”
potentially derived from walking on Satan’s path becomes pro-
gressively lower. “For what is a man profited, if he shall gain
the whole world, and lose his own soul? or what shall a man
give in exchange for his soul?” (Matt. 16:26). Let us consider a
concrete example. | could murder my wife and collect the in-
surance money. I might even get away with the crime in this
life. She could do the same to me. The point is, the “forfeited
income” of the face value of each of our insurance policies is of
zero value to us with respect to the act of murder. The trade-off
— “not murdering my spouse vs. the forfeited income” — does
not enter our calculations. It is therefore not a cost to either of
us, economically speaking, for the only valid cost (at least some
economists assure us) is individual psychic cost.’ Furthermore,
if this “non-calculation” were not nearly universal among mar-
ried people (common grace), insurance companies could not
afford to write life insurance policies, and certainly not large
ones. So, the paradox is resolved by progressive sanctification.

The possibility of personal moral progress is always before
each person. But the Bible is specific: widespread moral progress
will produce widespread economic growth. The biblical covenant
links obedience to God's law with God's blessings, which include
prosperity (Deut. 28:1-14).

Christianity asserts that there has been perfection in history.
It also teaches that, by the power of God’s regeneration of
individuals and their progressive sanctification, people can
approach perfection as a limit. We cannot achieve perfection in
history. “If we say that we have no sin, we deceive ourselves,
and the truth is not in us” (I John 1:8). Nevertheless, we are
required by God to work toward perfection. This is why we

19. See James Buchanan, Costs and Choice: An Inquiry on Economic Theory (University
of Chicago Press, 1969). See my discussion of value theory in North, The Dominion
Covenant: Genesis (2nd ed; Tyler, ‘Texas: Institute for Christian Economics, 1987), ch. 4;
North, Tools of Dominion, pp. 1087-1100.
