300 MILLENNIALISM AND SOCIAL THEORY

leader of the 1920's and 1930's, J. Gresham Machen," said it
well in 1932, four years before he and his associates were ex-
pelled from the Presbyterian Church, U.S.A. for their non-com-
pliance with that denomination’s growing theological liberalism:

The presentation of that body of truth necessarily involves con-
troversy with opposing views. People sometimes teil us that they are
tired of controversy in the Church. “Let us cease this tiresome
controversy,” they say, “and ask God, instead, for a great revival.”
Well, one thing is clear about revivals — a revival that does not stir
up controversy is sure to be a sham revival, not a real one. This has
been clear ever since our Lord said that He had come not to bring
peace upon carth but a sword.”

‘The curse of God in history against His Church would be
this: He will bring neither the crises nor a covenantal revival.
‘This would maintain the original satanic disinheritance: from
Adams fall to the present. It would mean that the Incarnation,
death, resurrection, and ascension of Jesus Christ in history were
culturally irrelevant divine discontinuities. It would mean that
the Church of Jesus Christ is merely a rescue mission. Yet it is
this historic outcome of gospel preaching that the pessimillen-
nialists defend. ‘They preach Satan’s defeat of the Great Com-
mission.

If you choose to believe the pessimillennial version of the
Church’s history, that is your self-imposed burden in life. As for
me, I choose optimism. I preach Christ’s resurrection in history.

19. J. GRESSum MAYchen.
20, J. Gresham Machen, “Christianity in Conflict,” in Vergilins Ferm (ed.), Contempo-
rary American Theology (New York: Raund Table Press, 1932), 1, p. 271.
