9

THE SOCIOLOGY OF SUFFERING

And there was also a strife among them, which of them should be ae-
counted the greatest. And he said unto them, The kings of the Gentiles
exercise lordship over them; and they that exercise authority upon them are
called benefactors. But ye shall not be so: but he that is greatest among you,
let him be as the younger; and he that is chief, as he that doth serve, For
whether is greater, he that sitteth at meat, or he that serveth? is not he that
sitteth at meat? but I am among you as he that serveth. Ye are they which
have continued with me in my temptations. And I appoint unto you a
kingdom, as my Father hath appointed unto me; That ye may eat and drink
at my table in my kingdom, and sit on thrones judging the twelve tribes of
Israel (Luke 22:24-30).

Before we get to a more detailed consideration of this pas-
sage, let me note briefly that the Lord’s Supper is a sacrament,
meaning it is a God-authorized means of God’s imposing His
negative sanctions in history: “For he that eateth and drinketh
unworthily, eateth and drinketh damnation to himself, not
discerning the Lord’s body. For this cause many are weak and
sickly among you, and many sleep” (I Cor. 11:29-30). The Prot-
estant Church does not really believe this — a testimony to its
commitment to philosophical nominalism: the sacrament as a
memorial and nothing more, surely not a judicially significant
ritual. This sacrament is not taken very seriously as a means of
bringing God's judicial sanctions in history. Neither, for that
matter, are the imprecatory psalms taken seriously as the
Church’s means of bringing God’s negative sanctions in society.
So thoroughly has nominalism corrupted the Church that its
