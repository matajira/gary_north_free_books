364 MILLENNIALISM AND SOCIAL THEORY

Sutton, Ray R. “Covenantal Postmillennialism,” Covenant Renew-
al (February 1989). Newsletter discusses the difference be-
tween traditional Presbyterian postmillennialism and cove-
nantal postmillennialism.

Terry, Milton S. Biblical Apocalyptics: A Study of the Most Notable
Revelations of God and of Christ. Grand Rapids, MI: Baker,
[1898] 1988. Nineteenth-century exegetical studies of pro-
phetic passages in Old and New Testaments; includes a com-
plete commentary on Revelation.

Toon, Peter, ed. Puritans, the Millennium and the Future of Israel:
Puritan Eschatology, 1600-1660. Cambridge: Jamcs Clarke,
1970. Detailed historical study of millennial views with spe-
cial attention to the place of Israel in prophecy.

Works Critical of Dispensationalism

Allis, Oswald T. Prophecy and the Church. Philadelphia, PA: Pres-
byterian and Reformed, 1945. Classic comprehensive critique
of dispensationalism.

Bacchiocchi, Samuele. Hal Lindsey’s Prophetic Jigsaw Puzzle: Five
Predictions That Failed! Berrien Springs, MI: Biblical Perspec-
tives, 1987. Seventh Day Adventist examines Lindsey's failed
prophecies, yet argues for an imminent Second Coming.

Bahnsen, Greg L. and Kenneth L. Gentry. House Divided: The
Break-Up of Dispensational Theology. Ft. Worth, TX: Dominion
Press, 1989. Response to H. Wayne House and Thomas Ice,
Dominion Theology: Blessing or Curse?. Includes a comprehen-
sive discussion of eschatological issues.

Bass, Clarence B. Backgrounds io Dispensationalism: Its Historical
Genesis and Ecclesiastical Implications. Grand Rapids, MI: Baker,
1960. Massively researched history of dispensationalism, with
focus on J. N. Darby.
