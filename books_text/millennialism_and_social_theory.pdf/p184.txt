168 MILLENNIALISM AND SOCIAL THEORY

The Non-Lessons of History

Let us think about Mucther’s asscrtions for a few moments.
If we can learn nothing of eternal value culturally from history,
since nothing of cultural value carries over into the resurrected
state, then how can we have any confidence that we can learn
anything useful regarding the success or failure of personal
ethics in history? If Christians’ social efforts in history are as
devoid of eternal significance as those of non-Christians — a
variant of the familiar neutrality hypothesis — then why not also
Christians’ personal cthical efforts? If there is no covenantal
relationship between our cultural efforts in history and our
rewards in history, then on what basis can we expect to discover
a covenantal relationship between our personal ethical efforts
and rewards in history?

Furthermore, what about our familistic and our ecclesiastical
corporate efforts? Why single out politics as an area of Chris-
tianity’s necessary historic irrelevance and impotence? Why not
also include the Church and the family? Muether does not men-
tion this obvious implication of his theology of God’s random
historical sanctions. Neither do his common grace amillennial
peers. This would be too much for most Christians to swallow.
“Pessimism, yes, but not ¢at much pessimism!” To say that all
our corporate (institutional) efforts are doomed would be to
commit theological suicide in full public view, and no one wants
to do this. So, they verbally concentrate on politics and culture,
even though their pessimistic worldview cannot in principle be
separated from all other covenantal and social institutions,

The critics of Christian Reconstruction imply (and sometimes
explicitly state) that the primary concern of Christian Recons-
tructionists is political, even though we consistently deny this.
(My slogan is “politics fourth.”)? Muether, for example, calls
his opponents “political utopians.”** Why do these critics of

27. North, Political Polytheism, p. 859. It is my concern afier individual salvation,
church membership, and family membership.

28. Muether, p. 15. He does not identify exactly who he is talking about in this
essay, perhaps because donors’ money to Reformed Seminary is on the line. But he uses
the phrase “political utopianism” to describe theonomnists in his essay in the Westminster
Seminary collection, published several months later: William S, Barker and W. Robert
