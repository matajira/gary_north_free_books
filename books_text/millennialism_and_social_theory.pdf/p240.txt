224 MILLENNIALISM AND SOCIAL THEORY

iron; thou shalt dash them in pieces like a potter’s vessel. Be wise now
therefore, O ye kings: be instructed, ye judges of the earth. Serve
the Lorp with fear, and rejoice with trembling. Kiss the Son, lest he
be angry, and ye perish from the way, when his wrath is kindled but
a little. Blessed are all they that put their trust in him (Psa. 2),
(emphasis added)

Lest we imagine that this is merely another Old Testament
proof text,’? consider Revelation 2:26-29: “And he that over-
cometh, and keepeth my works unto the end, to him will I give
power over the nations: And he shall rule them with a rod of
iron; as the vessels of a potter shall they be broken to shivers:
even as I received of my Father. And I will give him the morn-
ing star. He that hath an ear, let him hear what the Spirit saith
unto the churches.” Let him hear, indeed.

Clay jars, Gaffin writes, are believers “in all their mortality
and fragility.”"* Well, so what? What does this professor of sys-
tematic theology think covenant-breakers are made of, stainless
steel? But, as with every amilennialist, he gets his biblical imagery
backwards. He sees the Christians as clay pots and the covenant-
breakers as rods of iron, from now until doomsday. It is true that
the covenant-breaker is sometimes employed by God as a rod
against us (negative sanctions in history), but never apart from
the promise of a future reversal of the sanctioning relationship:

And it shall come to pass in that day, that the remnant of Israel,
and such as are escaped of the house of Jacob, shall no more again
stay upon him that smote them; but shail stay upon the Lorn, the
Holy One of Israel, in truth. The remnant shall return, even the
remnant of Jacob, unto the mighty God. For though thy people
Israel be as the sand of the sea, yet a remnant of them shall return:
the consumption decreed shall overflow with righteousness. For the
Lord Gop of hosts shali make a consumption, even determined, in
the midst of ail the land. Therefore thus saith the Lord Gop of
hosts, © my people that dwellest in Zion, be not afraid of the Assyrian:
he shall smite thee with a rod, and shall lift up his staff against thee, after
the manner of Egypt. For yet a very little while, and the indignation

12. A proof text is a biblical text proving something that you don't like one little bit.
13. Hbid., p. 211.
