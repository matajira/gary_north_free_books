54 MILLENNIALISM AND SOCIAL THEORY

greatly expand their possessions. The capital and resources
needed to extend God's kingdom across the face of the earth
are assured, all in good, covenant-keeping time. But if men
misinterpret the source of their wealth and attribute it humanis-
tically to the work of their own hands, they become guilty of
idolatry. God will come to judge them in the midst of history.

This is the paradox of Deuteronomy 8: wealth is both a positive
and negative sanction. The Israelites began their fulfillment of the
dominion covenant with wealth that they did not produce. They
were given the law of God, the ultimate tool of dominion.’
They were told to obey it. The reason given is intensely practi-
cal: because God is going to deliver a rich land into their hands.
This in turn will call for continued thankfulness: “When thou
hast eaten and art full, then thou shalt bless the Lorp thy God
for the good land which he hath given thee.” This thankfulness
will be a sign to God of their continued covenantal faithfulness.

The wealth of the land could become a snare to them. Here
is the paradox of wealth. “Beware that thou forget not the Lorp
thy God, in not keeping his commandment, and his judgments,
and his statutes, which I command thee this day.” God then lists
the many economic blessings that can serve as a snare: “
thou hast eaten and art full, and hast built goodly houses, and
dwelt therein; And when thy herds and thy flocks multiply, and
thy silver and thy gold is multiplied, and all that thou hast is
multiplied.” The language points back to the original covenant
with Adam: “And God blessed them, and God said unto them,
Be fruitful, and multiply, and replenish the earth, and subdue
it: and have dominion over the fish of the sea, and over the
fowl of the air, and over every living thing that moveth upon
the earth” (Gen. 1:28). The two tables of the law become cove-
nant-keeping man’s multiplication tables.

So, the external blessings can become the means of man’s
public display of his covenantal rebellion, which is followed by
God’s covenantal wrath. The positive feedback of obedience,
thankfulness, and further blessings can become the negative

1. Gary North, ols of Dominion: The Case Laws of Exodus (Tyler, Texas: Institute for
Christian Economics, 1990).
