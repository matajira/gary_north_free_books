Appendix: The Lawyer and the Trust 349

Premillennialism: Historic

Smurd: Well, sir, I have good news for you. You have named
as a beneficiary of my client’s worldwide trust. All taxes have
been paid. This is a pure windfall to you.

You: This is tremendous news. I’ve been having a terrible
time making ends mect. I'm behind on some of my payments.
This money is just what I need.

Smurd, Ah, yes. The money. Well, there is a lot of it, I can
assure you. But there are certain conditions of this trust.

You: Conditions? What kind of conditions?

Smurd: Well, you must obey the stipulations of the trust,
‘These are found in this book I am handing you.

You: It’s a Bible.

Smurd: Yes, it’s a Bible. You must obey it.

You: You mean all of it?

Smurd: Hardly! Most of it has been annulled.

You: Then what part?

Smurd: Just the New Testament.

You: All of it?

Smurd: In principle, yes.

You: And specifically?

Smurd: Specifically, nobody says exactly.

You: Why not?

Smurd: Because they all subscribe to Christianity Today, and
Christianity Today is careful never to say.

You: Are you telling me that Christianity Today is the only
arbiter of what the Bible says?

Smurd: I wouldn’t put it that way.

You: How would you put it?

Smurd: I’m not at liberty to say.

You: Why not?

Smurd: Lawyer-client secrecy.

You: Who is your client?

Smurd: I’m not at liberty to say.

You: Well, then, if I do what Christianity Today recommends,
what do I get?

Smurd: Assessments.
