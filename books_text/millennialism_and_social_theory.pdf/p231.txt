The Sociology of Suffering 215

then appeals to Christ's the words in Luke 22 that link the
Lord’s Supper to judicial rule in history.

Christ’s words in Luke regarding the Lord’s Supper appear
in no Church liturgy, as far as I know. I have never heard any
reference to this passage prior to taking communion. We are
usually told to do this in rembrance of Him, but not in expecta-
tion of exercising judgment against His enemies as agents of
His kingdom. Yet the message that Christ gave to His disciples
in Luke 22 was certainly consistent with His entire ministry.
First, it presents the contrast between the basis of authority
wielded by covenant-breakers and covenant keepers: power vs.
service. We are not to rule as the gentiles do. Jesus’ ministry
was grounded in the ultimate service: His death for His friends.
“Greater love hath no man than this, that a man lay down his
life for his friends” (John 15:13). “For scarcely for a righteous
man will one die: yet peradventure for a good man some would
even dare to die. But God commendeth his love toward us, in
that, while we were yet sinners, Christ died for us” (Rom. 5:7-
8). Second, His appointment of them as rulers of His kingdom,
even as He received such an appointment from His Father.
Third, the connecting of Holy Communion with Jesus’ rulership
in history: “That ye may cat and drink at my table in my king-
dom, and sit on thrones judging the twelve tribes of Israel.”

This reference to rulership has to be historical. The twelve
tribes of Isracl were still a political unit. The Church would
soon be persecuted by Israel, Jesus had warned them. “But take
heed to yourselves: for they shall deliver you up to councils;
and in the synagogues ye shall be beaten: and ye shall be
brought before rulers and kings for my sake, for a testimony
against them. And the gospel must first be published among all
nations. But when they shall lead you, and deliver you up, take
no thought beforehand what ye shall speak, neither do ye pre-
meditate: but whatsoever shall be given you in that hour, that
speak ye: for it is not ye that speak, but the Holy Ghost” (Mark
13:9-11). There would come a time of suffering under the syna-
gogue of Satan.

A generation later, John was instructed by God to write this
to the Church of Smyrna: “I know thy works, and tribulation,
