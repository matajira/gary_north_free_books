What Is to Be Done? 309

The Postmillennial Hope

I prefer to believe that in the coming millennium, the seventh
since creation, God is going to send His Spirit. I cannot be sure,
but it seems to me that this is the way God works,

1 think this will happen fairly soon. If it doesn’t, then Satan
will be able to boast: “They obeyed your rulc (Gen. 1:28), and
therefore 1 will spend eternity with vastly more souls.” The
multiplication of mankind, minus the saving work of the Holy
Spirit, means the overwhelming defeat of God’s gospel and His
Church in history. This is not prophecy; this is simply applied
covenant theology. You do not need a degree in theology to
figure this out; a hand-held calculator is sufficient.

I do not tell God what to do. I do make strong suggestions to
Him from time to time. My number-one suggestion today is:
“Don’t sit on Your hands too much longer. Otherwise, people’s
faithfulness to the external terms of the dominion covenant —
their multiplication — will become Satan’s most successful jiu-
jitsu operation against You in history.”

I think the long-predicted but institutionally unexpected
revival is imminent. Psychologically, I have to think this way; it
is the only way I can see for God not to be defeated in history
by the very success of the world’s population in meeting the re-
quirements of God’s biological command to be fruitful and
multiply. I do not choose to believe in the historic victory of
Satan as a direct result of people’s obedience to the external
demographic requirement of God's law. I choose instead to
believe in a coming historical discontinuity: mass revival.

The Mindset of the Critics

Non-Christians may be surprised to learn how hostile most
Christians arc to such a vicw of God’s work in history. In his
scathing attack on Christian Reconstruction, Dr. Masters, the
heir of Calvinist Charles Spurgeon, who now occupies the pulpit
of London’s Metropolitan Tabernacle, has this to say about
people who believe that God will save the souls of large num-
bers of people in our day. He is visibly contemptuous of those
who foresee a future outpouring of God’s salvation:
