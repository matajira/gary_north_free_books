248 MILLENNIALISM AND SOCIAL THEORY

guish a work of God from a spiritual counterfeit. Professed
conversions apart from the ethical and judicial requirements of
the biblical covenant are counterfeits. We have seen antinomian
revivals before, and they do not last. They leave in their wake
spiritually burned-over districts, emotional exhaustion, and
humanism, What we need are mass conversions to Christ which
lead men to ask the two crucial questions: “How should we then
live?” and “What is to be done?” ‘Then the new converts must
be directed to the Bible -- all of it, not just the New Testament
— for their answers.
