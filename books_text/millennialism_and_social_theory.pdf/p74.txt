58 MILLENNIALISM AND SOCIAL THEORY

knowledge as social and traditional, growing slowly over time,
being tested in the real world. The second sees knowledge as
the product of an elite corps of neutral, objective philosophers
and scientists. Social progress is the goal of both. The first sees
it as the result of slow social evolution, including free market
competition, The second sces it as the product of central plan-
ning and political power. The first viewpoint is essentially evo-
lutionary and politically decentralist; the second is essentially
revolutionary and politically centralist.”

Each of these social visions, however, presumes the autonomy
of man and man’s institutions. Both views deny the existence of a
God who intervenes directly into history, bringing His sanctions
in terms of His permanent ethical standards. Each vision is
thoroughly humanistic. Both are the product of Enlightenment
speculation. Economists in both camps begin with the presuppo-
sition of agnosticism regarding the supernatural.

Sowell describes the constraints school as holding to a world
of scarcity and inescapable trade-offs in life. We must give up this
in order to gain that. The second school is far more perfection-
ist. It searches for solutions, irrespective of trade-offs. It tends not
to count the costs of action, especially costs of human suffering.
Sowell quotes Thomas Jefferson on the bloodbath of the French
Revolution: “My own affections have been deeply wounded by
some of the martyrs to this cause, but rather than it should have
failed, I would have seen half the earth desolated.”"”

Sowell uses another pair of adjectives to describe the conflict
of visions: prudent vs. perfectionist. The “constraints visionary”
wants change, but prudent change. The “unconstraints vision-
ary” believes in the perfectibility of man. The first thinks that
human nature is fixed; the second believes that human nature
is plastic or flexible.”

It is clear that the conservative social tradition and the free
market economic tradition are both constraints-oriented in their

9. Thomas Sowell, A Conflict of Visions: Ideological Origins of Political Struggles (New
York: William Morrow, 1987), ch. 2.

10, /bid., p. 34. Jefferson, letter of Jan. 3, 1793. The Portable Thomas Jefferson, edited
by Merrill D. Peterson (New York: Penguin, 1975), p, 465.

11. fbid., pp. 25-26
