188 MILLENNIALISM AND SOCIAL ‘THEORY

neither doth a corrupt tree bring forth good fruit” (Luke 6:43).

In any free society, visible sanctions must be imposed in
terms of a publicly announced system of law (Deut. 31:10-13).
These public sanctions must be predictable. This is what law
enforcement is all about: the imposition of negative sanctions
against publicly proscribed behavior. Try to run a family or a
business without law and sanctions. It cannot be done. But if
you accept (“sanction”) the idea that a legal order’s sanctions
can legitimately be random in terms of fundamental law, you have
accepted the legitimacy of tyranny and arbitrary rule

Nevertheless, Christian theologians insist that there is neither
a required system of biblical civil law nor corporate sanctions
imposed by God in terms of this binding legal order.6 The
rejection of the idea of the reality of God’s corporate covenantal
sanctions in history parallels the rejection of the idea that bibli-
cal covenant law is supposed to govern society formally. Those
who deny that biblical law is God’s required corporate standard
also hasten to assure us that God does not bring negative sanc-
tions against societies that ignore this standard. (In order to
avoid being labeled antinomians, they usually assure us that
there ave God-imposed sanctions against evil personal behavior,
but then the same five covenantal questions still need to be
answered. They never are.) If not God's sanctions, then whose?

The problem here is the problem of formally specified judicial
sanctions, A person has the legal right to receive the specified
sanctions, as Paul asserted in his trial (Acts 25:11). Punishment
is a fundamental right. In a classic essay, C. S. Lewis warned
against any concept of civil sanctions in which they are not
spelled out in advance. The indeterminate prison sentence, he
argued, is a license for State tyranny.

5. EA Hayek, The Constitution of Liberty (University of Chicago Press, 1960).

6. They may acknowledge the existence of corporate responsibility and God's
negative sanctions against collectives. Berkhof writes: “We should always bear in mind
that there is a collective responsibility, and that there are always sufficient reasons why
God should visit cities, districts or nations with dire calamities.” He cites Luke 13:2-5. But
he does not mention biblical law. Louis Berkhof, Systematic Theology (London: Banner of
Truth Trust, [1941] 1963), p. 260.
