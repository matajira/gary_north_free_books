12

OUR BLESSED EARTHLY HOPE IN HISTORY

For every high priest is ordained to offer gifts and sacrifices: wherefore
it is of necessity that this man have somewhat also to offer. For if he were on
earth, he should not be a priest, seeing that there ave priests that offer gifts
according to the law: Who serve unto the example and shadou of heavenly
things, as Moses was admonished of God when he was about to make the
tabernacle: for, See, saith he, that thou make all things according to the
pattern shewed to thee in the mount. But now hath he obtained a more
excellent ministry, by how much also he is the mediator of @ better cove-
nant, which was established upon better promises. For if that first cove-
nant had been faultless, then should na place have been sought for the
second. For finding fault with them, he saith, Behold, the days come, saith
the Lord, when I will make a new covenant with the house of Israel and
with the house of Judah: Not according to the covenant that I made with
their fathers in the day when I took them by the hand to lead them out of the
land of Egypt; because they continued not in my covenant, and I regarded
them not, saith the Lord. For this is the covenant that I will make with the
house of Israel after thase days, saith the Lard; I will put my laws into their
mind, and write them in their hearts: and I will be to them a God, and they
shall be fo me a people: And they shall not teach every man his neighbour,
and every man his brother, saying, Know the Lord: for all shall know me,
from the least to the greatest (Heb. 8:3-11). (emphasis added)

In principle, this prophecy of Jeremiah the prophet (Jer.
31:31-34) has been fulfilled in Jesus Christ. It has been fulfilled
definitively. It has not yet been fulfilled progressively. This is anal-
ogous to Christ's perfection, which is imputed judicially to each
new convert as the legal basis of his regeneration. This defini-
tive fulfillment must become progressive in his life. He must
