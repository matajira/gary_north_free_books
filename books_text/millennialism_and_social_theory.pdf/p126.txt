110 MILLENNIALISM AND SOCIAL THEORY

determination on the right course towards the future and critical
reflection on anything presenting itself to us in this course.”

Having presented his chilling forecast of bad things to come,
van Riessen came back in 1960 to assure his Dutch-American
readers that “Christians are going to change the world. They
have to urge mankind to follow God’s will. But knowledge
changes nothing. It is the believing heart that alters the
world.” Unfortunately for the Church, there will never be
many of these believing hearts in history, according to what
amillennialism teaches. So, precisely how will Christians change
the world? Van Til said this Christian influence will only make
non-Christians more aware of their own intellectual inconsis-
tency, thereby bringing increasingly severe persecution against
the Church. Van Riessen did not say how Christianity will
change the world. He did not need to. The Society of the Future
had said enough.

When I was a teenager in a public high school, the boys’
athletic dressing room had this motto painted on the wall:
“when the going gets tough, the tough get going.” Van Riessen
adopted something like this schoolboy’s motto as a substitute for
Christian social philosophy. This, quite frankly, has been amil-
lennialism’s tactic for at least half a millennium.

Question: Wouldn’t the wise person adopt a different version
of the motto? “When the going gets tough, the tough may get
going, but the weak get out of the way.” This has been the
operational motto of Christians for well over a century. They
have read and fully understood four generations of premillen-
nial and amillennial dissertations and tracts, and they have acted
accordingly. Whenever possible, they have bought themselves a
hoped-for oasis, usually with a lot of debt, and they have then
ptayed to God to keep the desert sands outside its boundaries
until they die. Nothing has shortened Christianity’s time per-
spective more effectively than eschatological pessimism. Chris-
tian laymen are not fools. They have read the pessimillennialists’

24. Ibid., p. 308,
25, Hendrik van Riessen, “The Christian Approach to Science,” Christian Perspectives
(1960), p. 3.
