182 MILLENNIALISM AND SOCIAL THEORY

{Paul calls civil government “the sword” [Rom. 13:4], so there is
no way to involve oneself with politics apart from trying to pick
up “the sword.” This does not seem to have occurred to Profes-
sor Muether.) He calls this error-laden impulse “political utopi-
anism” and “theocratic utopianism.”*

Was Old Covenant Israel also utopian? Did God impose
utopian standards on Israel? If not, then why is it that a similar
set of standards is illegitimate today? What it is that makes our
task so utopian? Is the resurrection of Jesus Christ somchow
irrelevant culturally? Is the presence of the Holy Spirit some-
how irrelevant culturally? Are Christians less culturally empow-
ered today than the Israelites were? When amillennialists at
long last address these questions, we will have a much better
understanding of the theological foundations of their eschatolo-
gical system. We will know how seriously to take it. Until then,
however, there is not much reason to take seriously Mr. Mueth-
er’s accusation of Christian Reconstruction’s utopianism.

Ever since the demise of New England Puritanism in the late
seventcenth century, Protestant theology has ignored the funda-
mental covenantal issue of God’s historical sanctions. The theo-
logians of the twentieth century have been adamant: there are
no predictable covenantal sanctions in history. This is an aspect
of the myth of neutrality. But there is no neutrality. There are
always covenant sanctions in history. Therefore, what the denial
of God’s predictable covenant sanctions in history really means
is this: an affirmation of Satan’s exclusive, predictable covenantal sanc-
tions in history, meaning blessings for covenant-breakers and
cursings for covenant-keepers.

A few common grace amillennial theologians have tried to
hide the implications of their eschatology. They have said that
the sanctions in New Testament history are random. But then
they speak of the Church’s “exile,” which brings us back to the
issue of negative sanctions in history. They do not want to be
identified as men who have in fact adopted a perverse imitation
of postmillennialism: the progressive triumph in history of Satan’s
comprehensive kingdom-civilization. This is not the way they want

42. Idem.
