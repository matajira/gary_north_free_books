What Is to Be Done? 305

First and foremost, what message should we bring? It must
include an appeal to the misdirected sense of loyalty that these
gangs are able to call forth from their members. The Church of
Jesus Christ must be presented as a valid institutional option,
one with a better authority structure than the gangs can offer.
We can’t beat something with nothing. Vet, the institutional Church
today neither calls for such loyalty nor expects it. Churches do
not honor each other's excommunications, nor do they expect
their own excommunications to carry weight, either on earth or
in eternity. Their impotent sanctions and lack of respect for
other churches’ sanctions reflects this lack of any real authority
today. Churches have little sense of authority, so they cannot
compete effectively with organizations that do possess this sense,
whether gangs, cults, or secret societies.

There is a scene in the movie “Becket” where Thomas Becket,
the late twelfth-century Archbishop of Canterbury, is confronted
by some of the king’s officers, who have been sent by the king to
arrest him. Becket draws a circle around himsclf and announces,
“The man who crosses this line will have his soul condemned to
hell.” Not one of them dares to cross. Today’s Archbishop of
Canterbury may not even believe in hell. Surely, some of his
recent predecessors haven’t, and most of those prelates under
his authority do not. The Church no longer commands the
respect due to an agency that represents God in history. God’s
sanctions are not taken seriously, so why should the Church’s
authority be taken seriously?

The Church, by not taking itself very seriously, is not taken
seriously by anyone cise. The West's churches suffer from a
distinct disadvantage. Behind the Iron Curtain, churches are
beginning to recognize the power they. possess to affect history.
This realization has not yet penetrated the Western churches.
Evangelism therefore suffers. If this self-imposed cultural and
judicial impotence of the churches continues, their members are
going to suffer persecution. The “equal time for Satan” rhetoric

 

6, Meredith G. Ktine’s theory of God’s inscrutable historical sanctions has played its
small role in undermining the authority of the Church today. While few people have ever
heard of him, they share his view of God's historical sanctions.
