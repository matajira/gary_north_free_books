Eschatology and the Millennium 27

has adopted some version of Hinduism’s doctrine of maya: the
illusion of material reality. Very few of us want to return to a
pre-industrial world without the basic amenities of civilization:
anesthetics, electricity, telecommunications, air conditioning,
and rapid transportation.

But is technological-economic progress really meaningful?
Do scientific inventions change the nature of man? Does per
capita economic growth change the fundamental questions of
death, judgment, and eternity? Christians know the answer: no.
So, when pressed regarding the reality of historical progress,
most Christians return to the broader issue of eschatology and
away from the millennium. They will explain their denial of
historical progress by an appeal to the unchanging issues of
general eschatology. But this shift from narrow millennialism to
general eschatology is deceptive. In reality, they still cling to a
particular view of the Second Coming of Christ. Most Christians
presume the absence of spiritual (and therefore meaningful)
progress in history because most Christians have a discontinu-
ous view of the Second Coming of Christ. This victorious, visi-
ble coming from on high supposedly will not be influenced by
the prior success of Christians in applying God’s law to histori-
cal circumstances. Premillennialists and amillennialists deny that
there will be this kind of cultural success prior to Christ’s Sec-
ond Coming: either at the beginning of the millennium (pre-
millennialism) or at the end of history (amillennialism).

They will admit to ecclesiastical progress. Ask Christians if
there has been progress in revising the creeds, and they will say
yes, unless they are either Greek Orthodox, who deny the legit-
imacy of post-medieval creeds, or members of some Anabaptist
sect that denies the legitimacy of creeds altogether. But most
Christians assume that creedal improvement affects only the
institutional Church, not society at large. Creedal progress is
not seen even as an aspect of social progress, let alone a con-
tributing cause. This presumes a fundamental relationship in
history: the social irrelevance of the historic Christian creeds. It pre-
sumes that there is no continuity between the Church’s creeds
and civilization. Yet it is this which must be proven first, not
presumed, It is Christian Reconstruction’s contention that here
