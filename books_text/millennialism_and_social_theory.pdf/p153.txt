Time Enough 137

these things begin to come to pass, then look up, and lift up
your heads; for your redemption draweth nigh” (Luke 21:28).
But their deliverance came in history. Luke 21 is the chapter
that predicts the surrounding of Jerusalem by the Roman army
in A.D, 70: “And when ye shall see Jcrusalem compassed with
armies, then know that the desolation thereof is nigh. Then let
them which are in Judaea flee to the mountains; and let them
which are in the midst of it depart out; and let not them that
are in the countries enter thereinto” (vv. 20-21). That one-time
deliverance of the early Church is today long behind us. It is
surely time for Christians to begin looking forward, in time and
on earth, for their deliverance, not upward.

Why Continuity?

Men must look to the future and build for the future. They
need to work out their vision of life over time (Phil. 2:12), Ifthe
world were a series of unpredictable events, we could not plan
for the future. Without historical continuity, we would perish.
So, God gives mankind (and even the devil and his angels) the
common grace (i.e., an unearned, undeserved gift) of time.”
For the covenant-keeper, time is onc of his God-given means for
building up his eternal treasure. God’s common grace to him in
history becomes a means of special grace in eternity.

For we are labourers together with God: ye are God’s husbandry,
ye are God’s building. According to the grace of God which is given
unto me, as a wise masterbuilder, | have laid the foundation, and
another buildeth thereon, But let every man take heed how he build-
eth thereupon. For other foundation can no man lay than that is laid,
which is Jesus Christ. Now if any man build upon this foundation
gold, silver, precious stones, wood, hay, stubble; Every man’s work
shall be made manifest: for the day shall declare it, because it shall be
revealed by fire; and the fire shall try every man’s work of what sort
it is. Ifany man’s work abide which he hath built thereupon, he shall
receive a reward. If any man’s work shall be burned, he shall suffer
loss: but he himself shall be saved; yet so as by fire (I Cor. 3:9-15).

11. Gary North, Dominion and Common Grace: The Biblical Basis of Progress (Tyler,
‘Texas: Instilute for Christian Economics, 1987), ch. 1
