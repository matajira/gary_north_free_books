234 MILLENNIALISM AND SOCIAL THEORY

Jesus’ issuing of His Great Commission to the Church:

Now when they were going, behold, some of the watch came
into the city, and shewed unto the chief priests all the things that
were done. And when they were assembled with the elders, and had
taken counsel, they gave large moncy unto the soldiers, Saying, Say
ye, His disciples came by night, and stole him away while we slept.
And if this come to the governor’s cars, we will persuade him, and
secure you. So they took the money, and did as they were taught:
and this saying is commonly reported among the Jews to this day
(Matt. 28:11-15),

Bible-believing Christians must publicly affirm the reality of
the bodily resurrection and ascension of Jesus Christ in history.
This means that Christians must also affirm the consequences of both
the resurrection and the ascension, including their social and cultural
consequences. Amillennialism’s hermeneutic of persecution is
therefore not valid as a primary classification device to evaluate
the entire work of the Church in history. There is more to the
progress of the Church in history than its persecution. In short,
there is more to Christianity’s victory in history than its hypothetical
cultural defeat in history. But this is what amillennialism explicitly
and self-consciously denies. It proclaims cultural defeat.

Schlossberg understands that there has to be more to the
interpretation of history. But as an amillennialist and a non-
theocnomist, he does not speculate in public about what this
might be. He writes: “We need a theological interpretation of
disaster.” The Church has needed this for many centuries. So
have the humanists. The devastating Lisbon earthquake of 1755
shook not just the foundations of Lisbon; it shook the founda-
tions of Enlightenment optimism. So have major catastrophes
ever since. If man is essentially good, then why do such terrible
things happen to large numbers of us?

What the Bible has given us is a covenantal theory of disaster:
men will be called to account in history by God whenever they
systematically refuse to obey His Bible-revealed laws, But this is
too much to swallow for millions of Christians and billions of

26. Idem.
