82 MILLENNIALISM AND SOCIAL THEORY

Specifically requires of society, since this would require an appeal
to the Old Testament case laws, which both sides reject as no
longer judicially binding in the New Testament era. Therefore,
they have no plan of action or social reform. A representative
statement of the social theory of common grace amillennialism
is Bob Goudzwaard’s: “A program of action drawn up to carry
out a blueprint evokes the impression of a short-term realiza-
tion of objectives. . . . What follows, therefore, presents no
program of action.”** This statement appears on page 188 of
a book with only 249 pages of text, and follows a lengthy attack
on both capitalism and Western civilization’s idea of progress.
This is typical of the Dooycweerdian movement: all dynamite
and no cement.” It disappeared from the Christian intellectual
scene in the early 1980's. It had never enjoyed very much influ-
ence outside of Christian Reformed circles."

The Problem of Two Leavens

These common grace Dutch scholars and their North Ameri-
can academic disciples have all been amillennialists. As amillen-
nialists, ney belicve that Satan’s earthly kingdom and influence
will expand over time until Jesus Christ comes with His angels
in final judgment. This assertion of the cumulative, visible
triumph of Satan’s kingdom in history is inherent in all amillen-
nialism. This view of New Testament era history defines amillen-
nialism. Amillennialism, as with premillennialism’s view of
everything that takes place prior to the millennium, is essen-
tially a reversed form of postmillennialism: postmillenniatism for
Satan’s kingdom.” The idea that there can be an “optimistic
amillennialism” is difficult to take seriously. Even the barest

89. Bob Goudzwaard, Capitalism and Progress: A Diagnosis of Western Society (co-pub-
lished by Wedge and Eerdmans, 1979), p. 188. He nevertheless calls for a no-growth,
steady-state economy for Western Europe and North America: p. 194. Behind every
denial of a blueprint there lurks a hidden agenda and a secret blueprint.

40. North, Political Pluralism, ch. 3. See also North, The Sinai Strategy: Economies and
the Ten Commandments (Tyler, Texas: Institute for Christian Econotnics, 1986), Appendix
C: “Social Antinomianisma.”

41, T include here Westminster Theological Seminary.

42. North, Political Polytheism, p. 139.
