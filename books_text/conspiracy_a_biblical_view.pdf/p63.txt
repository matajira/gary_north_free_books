Two Kinds of Conspiracy 47

eats its children.” (I like the wit’s addition: “But not soon
enough.”)

What the average citizen does not readily recognize is the
existence of the other form of conspiratorial organization, the
kind described in None Dare Call It Conspiracy. This kind bases
its strategy and tactics on the principle of “unification through
manipulation.” This form of conspiracy operates under a very
different set of presuppositions and assumptions about the way
to achieve universal ethical unity. Its pre-eminent hypothesis is
this: ideology is ultimately irrelevant. Ideological differences can-
not possibly be ultimate, for we know that all ultimate disagree-
ments are cthical disagreements, and mankind cannot possibly
be ethically divided. By definition, any perceived ethical disuni-
ty just has to be a temporary phenomenon. To admit that such
cthical disunity is fundamental and permanent is to admit that
mankind is net God, for God cannot be ethically divided
against Himself. Therefore, they reject the idea. They believe
that there are always ways to overcome ethical (ideological)
disunity.

The best way to overcome this temporary disunity, of course,
is to make a deal, preferably a business deal. Best of all, make
a business deal at taxpayers’ expense.

Conspiratorial humanists all agree that mankind ought to be
unified ethically, where all men share the same cosmic vision.
But they disagree about the hypothesis that mankind és, in fact,
ethically unified. The “conspirators through execution” ac-
knowledge that ali men clearly are not yet ethically unified;
therefore, some men—indeed, millions of them—will have to be
removed from visible existence (execution, the Gulag) if they
persist in their rebellion against the Truth. What kind of
Truth? Jacobin Truth, proletarian Truth, or Nazi Truth: they
all argue (and act) the same way. The pattern is repeated be-
cause the theology is repeated.

In contrast, the “conspirators through manipulation” dare
not admit such a thing. To do so would deny their theological
