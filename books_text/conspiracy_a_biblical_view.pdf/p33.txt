The Reality of Conspiracies 17

The offense of pages 936-956 of Prof. Carroll Quigley’s
Tragedy and Hope (Macmillan, 1966), None Dare Call It Conspiracy,
and similar conspiracy thesis books, is found in the thesis that
a dedicated conspiracy has quietly captured the power centers
in order to further its own ends against... . Against what?

To identify a conspiracy, you must also identify the “con-
spired against.” The identification of the “conspired against”
establishes which kind of conspiracy thesis the author is pro-
moting. There are Marxist-written conspiracy books that criti-
cize the Rockefeller interests as pro-capitalist. There are “right-
wing” conspiracy theses that are anti-Rockefeller because of the
State capitalism aspect of the Establishment. Usually, the focus
of concern is on politics and/or economic monopoly. Very
seldom is the conspiracy traced back farther than two centuries,
with the exception of anti-Semitic conspiracy theses, and even
these generally begin with the Rothschild family in the late 18th
century.

The Biblical View

The Bible reveals a much longer conspiratorial time frame: a
continuing conspiracy against God and His revealed law-order.
The faces change, but the issue remains the same: e¢hics. Mon-
ey, power, prestige, and influence all flow out of this funda-
mental issue: Which God should men worship? As the prophet
Elijah presented the issue before the people of Israel when they
gathered on Mt. Carmel during the reign of Israel's evil king,
Ahab: “How long halt ye between two opinions? If the Lord be
God, follow him; but if Baal, then follow him.” The next sen-
tence is most revealing: “And the people answered him not a
word” (I Kings 18:21). They never do, until they see who is
going to win the confrontation.

The biblical view of conspiracy neither overestimates the
power of conspiracies nor underestimates it. There is one con-
spiracy, Satan’s, and ultimately it must fail. Satan’s supernatural
conspiracy is the conspiracy; all other visible conspiracies are
