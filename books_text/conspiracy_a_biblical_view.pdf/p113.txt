Replacing Evil With Good 97

utterly ignorant of the conspiracy are in a position to replace
each conspirator in whatever office or job that is presently held
by conspirators. The second possibility is that this same princi-
pled group can gain almost perfect knowledge about every
aspect of the conspiracy, but in doing so, they use up all of
their spare time. Which would you prefer, ignorant victory or
well-informed defeat?

You may respond, “no one can replace the conspirators who
does not know ali about them. Knowledge comes first.” But
does it? What did the people of Israel know about the gods of
Canaan that governed the promised land? Not much. They
were not supposed to know much about them, either; God
forbade them from even mentioning the names of these gods
(Exodus 23:13). Nevertheless, God led them to victory over
these gods under Joshua.

What did the Apostles know about Roman law or Roman
military tactics? Not much, but God defeated the Roman Em-
pire and used His people to replace the older Roman leader-
ship. Christians by the year 325 were the only group left in
Rome which was sufficiently moral and sufficiently productive
to take over the administration of the crumbling Empire. Yet
they had been outcasts: persecuted, politically inexperienced
outcasts. All they had was a moral vision. All they had was
perseverance. All they had was God.

It is not knowledge which saves us. What saves us is grace.
What saves a society is the covenantal faithfulness of its people.

Our job is not to “throw the rascals out” in one glorious
national election. Our job is to replace them steadily by our
own competence. God did not promise Moses that the Hebrews
would conquer the Canaanites overnight, On the contrary, He
promised to preserve the Canaanites until their day of judg-
ment had come, city by city, year by year:

I will send my fear before thee, and will destroy all the peo-
ple to whom thou shalt come, and I will make all thine enemies
