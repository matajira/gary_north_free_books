Tivo Kinds of Conspiracy 51

the introduction of our Order into any state where we are yet
strangers.*

Today, the college and university have taken the place of
reading societies. So have literary reviews and book review
services, so that lazy pscudo-intellectuals can appear to be weil-
informed without actually having to read fat books. Discussion
groups have replaced the literary society. And the biggest dis-
cussion group of all is the Council on Foreign Relations.

But that was only the beginning, Weishaupt said. “In like
manner we must try to obtain an influence in the military acad-
emies (this may be of mighty consequence), the printing-houses,
booksellers shops, chapters, and in short all offices which have
any effect, either in forming, or in managing, or even in direct-
ing the mind of man: painting and engraving are highly worth
our care.”*

Weishaupt’s strategy still holds. First, the military academies
and the military. The Commandant of West Point over the last
generation has always been a C.ER. member’ Senior com-
manders are also CFR members. The C.FR. sponsored many
meetings around the country in 1983-84, as it does every
year—meetings that featured C.FR. members in the military.
These members included: Bernard W. Rogers, Supreme Allied
Commander, Europe; John W. Vessey, Jr., Chairman, Joint
Chiefs of Staff; John A. Wickham, Jr., Chief of Staff, U.S. Army;
John T. Chain, Deputy Chief of Staff, Plans and Operations,
U.S. Air Force; Paul FE. Gorman, Commander-in-Chief, United
States Southern Command, etc." The most spectacular case is
that of Gen. Al Haig, a mediocre West Point student, who was

3, Quoted in John Robison, Proofs of a Conspiracy (1798); reprinted by Western
Islands, Boston, 1967, p. 112
4. Ibid.

5, Susan Huck, “Lost Valo” Ainerican Opinion (October 1977); “Military,” Amer
ican Opinion (July/August 1980).
6. Council on Foreign Relations, Annual Report 1983-1981, p. 56.
