64 CONSPIRACY: A BIBLICAL VIEW

evasive. Why? Because their bottom line on political morality is
pure pragmatism. The basis of their absolute opposition to
Hitler boils down to this: Stalin, Churchill, and FD.R. beat
Hitler. In short, Hitler was a loser. He was a conspirator who got
caught before he had consolidated his power. Herein lies his
offense. To use language from another discipline, he went for
an inside straight, and missed.

There was a period before the War when he looked as
though he would be successful. In this period (you will never
be told in any university classroom), some very powerful and
influential Americans were sending him money. The same sorts
of people who sent Lenin money. The same sorts of people
who got Franklin Roosevelt elected. People on Wall Street.
People who belong to, or have belonged to, the Council on
Foreign Relations (1921-), the Trilateral Commission (1973-),
and similar organizations.

The Wall Street Connection

Want to become unemployable at any university in the Unit-
ed States? Write Wall Street and the Bolshevik Revolution, and have
it published by Arlington House, the conservative publishing
company. Demonstrate that the “kook” theory that New York
bankers and big business leaders financed the Bolshevik revolu-
tion is really not so far off base. Name those businessmen who
actually did it. Show why they did: to win lucrative trade mo-
nopolies with the new Communist government. Show that
Lenin paid off, and that his successors did and still do.

Then write Wall Sireet and FDR. Have Arlington House pub-
lish that, too. Show that Franklin Roosevelt began his career as
a lawyer with the law firm whose principle client was the New
York banking firm of J. P Morgan. Show that he got his first
appointment in government, as Assistant Secretary of the Navy,
because of the intervention of Morgan partner Thomas W. La-
mont. Show that after his ill-fated candidacy for Vice President
in 1920, he became vice president of something else: Fidelity &
