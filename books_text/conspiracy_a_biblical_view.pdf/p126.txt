CONCLUSION

Uf they shall confess their iniquity, and the iniquity of their fathers,
with which they trespassed against me, and that also they have watked
contrary unto me, and that I also have walked contrary unto them, and
have brought them into the land of their enemies; if then their uncircum-
cised hearts be humbled, and they then accept of the punishment of their
iniquity: then will I remember my covenant with Jacob, and also my
covenant with Isaac, and also my covenant with Abraham; and I will
remember the land (Leviticus 26:40-42).

The will to ethics is the first stage. If you will not act morally
in terms of the information you have been given, then you are
a bystander, and not an innocent bystander. Much is expected
from those to whom much has been given (Luke 12:48).

The will to resist is the second stage. Satan and his host, both
supernatural and earthly, have a strategy based on a myth: the
sovereignty of anything except God. To persuade people of the
validity of this myth, they have adopted the tactics of the power
religion. They squash people who get in their way. Not every-
one gets squashed—the enemies of God are not omnipotent,
after all—but some people. They make it seem as though they
can squash anyone and everyone, if they ever choose to do so.
They use a combination of deception and terrorism to achieve
their goals. Thus, the proper counter-measure is to learn the
truth and to resist evil.
