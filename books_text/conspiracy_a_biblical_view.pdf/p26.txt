10 CONSPIRACY: A BIBLICAL VIEW

probably been the most important book in the arsenal of the
conspiracy—vastly more important, at least in the industrialized
West, than anything written by Karl Marx, another dedicated
follower of Darwin.*)

The Establishment’s Conspiracy

T want to make one thing perfectly clear (as Richard Nixon
used to say): everyone believes in the existence of conspiracies.
Conspiracies are organized groups of people who maliciously
plot to undermine whatever it is you believe im. Obviously, what
you believe in is good, so they are evil. Since there are always
fringe groups who have not yet “seen the light,” and who plot
against goodness and true justice, those who believe in good-
ness and true justice need to defend themselves by stamping
out (or at least exposing) these illegal groups. These groups are
clearly illegal, since good and just people get their rulers to
pass laws making such conspiracies illegal. In short, as the
Christian scholar R. J. Rushdoony has written, “The commonly
admitted conspiracies are those of the opposition.”®

There is nothing remarkable in all this. Clearly, it isn’t worth
a whole book. But what the more recent conspiracy thesis books
argue, especially None Dare Call It Conspiracy (1972) and its
sequel, Call It Conspiracy (1985),” is that not only are there con-
spiracies, but that there is one major conspiracy in the twen-
tieth-century, and that this conspiracy has actually succeeded in
capturing the major institutions of modern society: church,

5, Marx wrote to Ferdinand Lassalle in early 1861: “Darwin's book is very
important and serves me as a basis in natural science for the class struggle in histo-
ry.” Karl Marx and Friederich Engels, Correspondence, 1846-1895, edited by Dona
Torr (New York: International Publishers, 1933). p. 125. He had written to Engels
a few weeks earlier: “.. . this is the book which contains the basis in natural history
for our view.” Jbid., p. 126.

6. R. J. Rushdoony, The Nate of the American System (Fairfax, Virginia: Thobuen
Press, [1965] 1978), p. 143.

7. Larry Abraham, Cail It Couspivary (Seattle, Washington: Double A Publications,
1985).
