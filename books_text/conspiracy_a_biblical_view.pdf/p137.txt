Bibliography 121

with the tapes would know. These boys knew precisely what to
look for! Here is another sample request:

January 8, 1973 from 4:05 to 5:34 PM. (E.0.B.)

a) at approximately 10 minutes and 15 seconds
into the conversation, a segment lasting 6
minutes and 31 seconds:

b) at approximately 67 minutes into the conver-
sation, a segment lasting 1] minutes;

c) at approximately 82 minutes and 15 seconds
into the conversation, a segment lasting 5
minutes and 3! seconds.

Only Susan Huck asked the obvious question: How did the
prosecutors know precisely when these incriminating discus-
sions took place? There are only two possible answers: (1)
someone with access to the tapes inside the White House was
Jeaking the information; (2) there was a secret back-up set of
the tapes in the hands of someone who was leaking the infor-
mation. Leaked information would have been illegal for prose-
cutors to use in court, yet this was how they brought Nixon
down.

To my knowledge, no reporter or professional historian has
ever bothered to follow up on this remarkable oddity, or even
mention it. Nobody ever asked: “What person was in charge of
storing those tapes?” It took one of the least known and most
diligent conspiracy historians (Ph.D. in geography) even to
mention the problem. Strange? Not at all. Normal, in fact. Such
is the nature of history and the writing of history whenever the
events in question point to the operation of powerful people
whose private interests are advanced by what appear to be
honorable public activities that cost a lot of money.

Not every exposé is equally reliable in its assessment of the
facts. Similarly, not every conventional history is equally innoc-
uous in what it reveals. Even a poorly researched exposé can
alert us to things to look up in conventional histories.
