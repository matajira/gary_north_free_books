4 CONSPIRACY: A BIBLICAL VIEW

left.5 If any single event was most responsible for this growing
interest, it was the assassination of President John F. Kennedy
on November 22, 1963. A wave of books on the Kennedy assas-
sination has appeared, from Mark Lane’s Rush to Judgment
(1966) to David Lifton’s Best Evidence (1980). One scholarly
bibliography, published in 1980, listed over 5,000 sources on
the subject.” By now there are probably thousands more.

The conclusion of the Warren Commission’s report on the
assassination—Lee Harvey Oswald was a lone killer without any
institutional connections—was predictable. He was just a mix-
ed-up fellow, He was a “nut.” This has been the official coin of
the realm in every post-assassination investigation, from Gar-
field and McKinley to the Kennedys, from Martin Luther King
to George Wallace (who at least survived the ordeal). “The
Lone Assassin, mentally disturbed, strikes again.” And anyone
who doesn’t believe this explanation, it is mplied by establish-
ment commentators, scholars, and official spokesmen, is himself
mentally disturbed. But one thing is sure: when it comes to dhés

 

 

 

form of “mental disturbance,” the mentally disturbed person is
hardly alone. Things have changed since November 22, 1963—
to a great extent, because of November 22, 1963.

The generally skeptical or at least unenthusiastic response of
American readers (a minority of Americans} to the Warren
Commission's report was not predictable back in 1964, when
the Committee began its work. This skepticism startled the
Establishment. So has its aftermath. Skepticism by the American

for the '80s (New York: Prentice Hall, 1983). It is not a blatantly ideolagical book. The
most detailed study ever written on this topic is Philip I]. Burch's Elites in American
History, 3 vols. (New York: Holmes & Mcier, 1980),

5. Holly Sklar (ed.), Trilateratism: The Trilateral Connnission and Elite Planning for
World Management (BO. Box 68, Astor Station, Boston, Massachusetts: South End
Press, 1980); Kees van der Pijl, The Making of an Atlantic Ruling Class (London: Verso,
1984}. Both publishers are quite obscure.

6. Lloyd J. Guth and David R. Wrone, The Assussination of folm K Kennedy: A
Comprehensive Historical and Legal Bibliography, 1963-1979 (Westport, Connecticut:
Greenwood Press, 1980).
