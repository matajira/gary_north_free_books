36 CONSPIRACY: A BIBLICAT. VIEW

legitimate. The Constitution's framers recognized this, and they
attempted to construct a legal order which restrains political
power. But to maintain itself from power-seekers of a rival
faith, a society must be self-governed and self-restrained. Men
Must say to themselves, “My power is limited; therefore, the
State’s power is limited. The State is not Savior; therefore, the
State is not absolutely sovereign. No appeal to the idea of the
State as finally sovereign can be morally valid, and I will resist
all such claims, and also those who make them.”

Historically, this has meant that members of society must see
themselves as under an authority other than the State. There
has to be an enforcer somewhere. In the West, this has always
meant God. For example, we added these words in the 1950's
to our pledge of allegiance to the flag: “one nation, under
God.” Why? Because these words are consistent with American
history. (Also, because Congress and the Supreme Court were
not yet getting their concept of law from that ultimate “little old
lady in tennis shoes,” Madalyn Murray O’Hair.)* From the
beginning, the essence of “the Amcrican experiment” was the
attempt of wise men to design political institutions of legally
limited power.

The limitation of civil power: this is what the U.S, Constitu-
tion was originally all about. This was what The Federalist was all
about. While Hamilton was far more of a centralist than Madi-
son, his political influence after 1800 collapsed dramatically. His
view of the national government as the source of both political
and economic unity did not take deep root in the United States
until after the Civil War. Hamilton did not present a case for
the expansive State in his essays in The Federalist. He wisely
recognized that voters would be hostile to any such suggestion.
Americans in 1787 did not trust the State, and they were wary

4. Mrs. O'Hair and several of her associates disappeared in early 1996. Her son
Bill has been an evangelical Christian for over a decade.
