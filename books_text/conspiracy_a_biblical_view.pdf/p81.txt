Court Historians 65

Deposit Insurance Co, of Maryland, and resident director of
the company’s New York office. Franklin Roosevelt was on the
board of directors of eleven corporations, the president of a
large trade association, and a partner in two law firms, 1921-
28. Show that, by profession, FDR was a banker and an interna-
tional speculator. Entitle Part III, “FDR and the Corporate
Socialists.”

Give an account of his relatives, who were also well-connec-
ted on Wail Street. FDR’s favorite uncle, Frederic Delano, was
appointed in 1916 by Woodrow Wilson to the Federal Reserve
Board, and he was later chairman of the Federal Reserve Bank
of Richmond (1931-36). He was the president of three railroads
along the way.

Then go the whole nine yards. Write Wail Street and the Rise
of Hitler. Even Arlington House won't touch that one. Get Gary
Allen to publish it.

That, of course, is what Antony Sutton did. But why not? He
was already unemployable in high-level academia. He was a
judicious and remarkable scholar who wrote himself out of an
academic career, despite (possibly because of) the erudition of
his performance in Western Technology and Soviet Economic Devel-
opment (3 volumes, Hoover Institution Press), which shows that
95% of all Soviet technology had been imported from, or stolen
from, the West, 1917-1967. Because of what he discovered
when writing this academic study, he concluded that the Soviet
Union must have purchased most of its military technology
from the West, too.

He then made the mistake of publishing this conclusion,
along with the evidence, in a popular form through a conserva-
tive publishing house: National Suicide (Arlimgton House, 1973).
He demonstrated beyond any shadow of a doubt that profit-
seeking U.S. firms have gotten rich by selling the Soviet Union
the military technology that alone made it a credible threat to
the West. One doesn’t voice such embarrassing conclusions to
“the conservative rabble” if one is on the staff of the Hoover
