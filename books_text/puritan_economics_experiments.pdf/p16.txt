8 Puritan Economic Experiments

profits would be shared by all members of the company, but the
colonists had not agreed to the sharing of houses, gardens, and
other improved land. They were informed of these terms only
as they were about to leave for North America, and as they left,
they sent back word to the merchant adventurers that their
agents who had agreed to such terms had not been empowered
to do so.!° But the continuing dependence upon the company for
resources during the first year of the colony’s existence compelled
them to give in to the company’s terms.!!

The story did not end in 1623, when necessity forced the
hands of the colonists. In 1627, the bickering British directors
sold out their interests in the colony to the scttlers for 1800
pounds. The settlers were to spend a decade and a half in paying
off their debt, and at times had to borrow extra time at rates of
30 percent to 50 percent. Nevertheless, they persisted and finally
repaid the debt, in 1642.

In 1627, shortly after buying out the British directors, Gover-
nor Bradford supervised the division of the colony’s assets among
the settlers. First, they divided livestock. There were few ani-
mals, so the 156 people (fewer than 40 families) were divided into
a dozen companies; each company received a cow and two goats.
In January of 1628, the land was divided, this time by random
Jot. Complaints. about unequal housing were forestalled by re-
quiring those who received better housing to make an equalizing
payment to those receiving poorer housing. Peace was preserved.

‘There was one decision, however, which was to prove costly.
Meadow was in short supply, so it was kept in common owner-
ship. Furthermore, fishing, fowling, and water remained “open”
to all settlers.'? The Pilgrims were to have the same difficulties
with the administration of these common fields as their neigh-
bors, the Puritans, were to experience. Only after 1675, when the
commons throughout New England were increasingly distrib-

10. Langdon, Pilgrim Colony, p. 9.
ll. Ibid, p. 26.
12. Hbid., p. 31.

 
