2 Puritan Economic Experiments

tradition, which was much more free market oriented.' Second,
the Puritans never fully broke with the theory of natural law, and
it was the scholastics who had imported and baptized this hu-
manist myth of ancient Greece and Rome. The Puritans did not
attempt to rethink economics in terms of the Bible; instead, they
simply imported medieval scholastic economic categories into
their legislation regarding pricing.

Their land policies were governed by their desire to maintain
the tight control of the two local community institutions. of
church and state. They believed that geography reinforces ethics.
They designed each town to place the church at the center. They
did what they could to restrict the spreading out of families
across the landscape. They kept large parts of the town in
commons ~ areas that were supposed to substitute for the amass-
ing of large privately owned tracts of land that would lead to the
dispersal of families. They saw land hunger as an anti-social
force. It was seen as a kind of centrifugal force that would lead
to the atomization of the towns. They much preferred to spin off
whole new villages rather than spin off families one by one.

The ‘same sort of impulse was reflected in fashion, they
believed. What was needed was hierarchy—a hierarchy that
would be reflected in what people wore. Again, they feared
confusion that is created by economic change, which inevitably
asserts itself in the form of social change. They saw hierarchy as
social, political, and economic. They believed for half a century
that they could preserve social hierarchy by means of the politi-
cal hierarchy: restricting the economy by law.

The period 1630-1720 was a period of great experimentation,
an economic, social, and political laboratory in the New England
wilderness. That experiment transformed the wilderness, and in

1, Alejandro A. Chafuen, Ghristians for Freedom: Late-Scholastic Economics (San
Francisco: Ignatius, 1986); Marjorie Grice-Hutchinson, The School of Salamanca:
Readings in Spanish Monelarp Theory, 1544-1605 (Oxford: The Clarendon Press, 1952);
Murray N. Rothbard, “Late Medicval Origins of Free Market Economic Thought,”
Journal of Christian Reconstruction, 11 (Summer 1975), pp. 62-75.
