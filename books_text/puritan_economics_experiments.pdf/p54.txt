46 Puritan Economic Experiments

the form of tools and training,.to departing indentured servants
(who could be kept in service no more than seven years). Still,
in every society there are higher and Jower, richer and poorer,
and the sumptuary legislation codified these distinctions, For
many years, the subordinate population was willing to acquiesce
in what the Larger Catechism required, an acknowledgement of
their superiors “according to their several ranks, and the nature
of their places.”

The Sumptuary Codes

The Puritan magistrates concluded, as had leaders in Euro-
pean society for centuries, that it is not always easy to identify
members of various classes. In New England, for all intents and
purposes, there were three levels — higher, middle, lower — but
the law codes only recognized two. Puritan legislation borrowed
a practice of the most familistic of all state structures, the mili-
tary: uniforms. The Larger Catechism listed as one of the duties
ofinferiors the “imitation of their [superiors’] virtues and graces,”
but no Puritan leader was so naive as to believe that such a
requirement allowed the “inferior sort” to imitate their superiors’
tastes in fashion. Thus, in 1651, both the magistrates and depu-
ties of Massachusetts agreed on the following picce of legislation,
onc that is unrivaled in American history for its sheer mecieval-
ism — comprehensive, authoritarian, and thoroughly hierarchi-
cal,

Although several declarations and orders have been made by this
Court against excess in apparel, both of men and of women, which
have not yet taken that effect which were to be desired, but on the
contrary we cannot but to our grief take notice that intolerable excesses
and bravery have crept in upon us, and especially amongst the people
of mean condition, to the dishonor of God, the scandal of our profession
[i.e., profession of faith], the consumption of estates, and altogether
we acknowledge it to be a matter of great difficulty, in regard to the
blindness of men’s minds and the stubbornness. of their wills, to set
down exact rules to confine all sorts of persons, yet we cannot but
account it our duty to commend unto all-sorts of persons a sober and

 
