2
PRICE CONTROLS

The court having found by experience, that it would not avail by any
law to redress the excessive rates of laborers’ and workmen’s wages,
etc. (for being restrained, they would either remove to other places
where they might have more, or else being able to live by planting and
other employments of their awn, they would not be hired at all), it
was therefore referred to the several towns to set the rates among
themselves. This took better effect, so that in a voluntary way, by the
counsel and persuasion of the elders, and example of some who led
the way, they were brought to more moderation than they could be by
compulsion. But it held not long.

Gov, John Winthrop '

The little band of Pilgrims who settled Plymouth Colony in
1620 are more famous in children’s textbooks than their neigh-
bors, the Puritans. Plymouth Rock, Thanksgiving, Miles Stan-
dish, and Speaking for Yourself, John, are all ingrained in the
story of America’s origin. Nevertheless, in terms of historical
impact, the Pilgrims never rivaled their Puritan ncighbors. Ply-
mouth Colony remained a relatively isolated and closed society
until it finally merged with Massachusetts in 1692. It was Gov.
John Winthrop, not Gov. William Bradford, who left his mark
on American institutions.

1, James K, Hosmer (ed.), Winthrop’s Journal: “History of Naw England,” 1630-
1649, 2 vols. (New York: Barnes & Noble, [1908] 1966), IT, p. 24.

22
