34 Puritan Economic Experiments

difficulty would be far more obvious to the magistrates in 1650
than it had been in 1639.

Wage Controls in 1641

The last major attempt by the central government of Massa-
chusetts to control wages in peacetime came in 1641. The scar-
city of money—scarce in comparison to prices common the
year before —had disrupted the economic life of New England.
Immigration from England came to a standstill; indeed, a few
energetic Puritans returned to England to participate in the Civil
War against Charles I. Merchants were closing their doors, and
manufacturers were refusing to hire laborers. The General Court
declared that laborers must accept a mandatory reduction of
wages proportional to the reduced price of the particular com-
modity they labored to make, Laborers, the law declared, “are
to be content to partake now in the present scarcity, as well as
they had their advantage in the plenty of former times. . . .”%
At least the magistrates had enough sense to control prices in the
general direction of the market. In tying the laborer’s wage to
the value of his output, they acknowledged the close relation
between the market price of the produced good and the value of
labor’s services. In contrast to modern wage controls during a
depression — wage’ floors, compulsory collective bargaining, ar-
tificial restrictions on entry into labor markets, and so forth
—the Puritans of 1641 understood that laborers should accept
a lower wage if the valuc of their output was falling. Three
centuries later, their descendants were not to show equal wisdom
in the face of a similar collapse in price expectations, even those
who were the officially certified experts in economics, a discipline
undreamed of in 1641. The Puritans used shipping and increased
agricultural output to revive their economy; their descendants
used deficit financing and a world war, and even then their

30. Mass. Col. Recs. 1, p. 326.
