Sumptuary Legislation 49

believed, so they went to considerable lengths to protect men
from their own weaknesses. Then, as now, licensing was: the
primary means of control, and it was equally a source of public
revenue; The annual licensing of taverns, said the Massachusetts
magistrates, is inescapable, “Seeing it is difficult to order and
keep the houses of public entertainment in such conformity to
the wholesome laws established by this Court as is necessary for
the prevention of drunkenness, excessive drinking, vain expense
of money, time, and the abuse of the creatures of God... .”

Although it seems incredible today; shuffleboard was re-
garded as a prime danger. There were not to be scenes of elderly
men spending a Icisurcly afternoon in the park playing this
devil’s game. Such games were a sign of idling—a. waste of
God’s most precious resource, time—and they were especially
prohibited in taverns and wheni practiced by servants and youths.
The magistrates were willing to go to real extremes to stamp out
games, of chance and shuffleboard.'* These regulations extended
throughout the century, unlike virtually all other sumptuary
laws, indicating a continuity of opinion against “vain pursuits.”
(It might be said that at least in New England, shuffleboard was
not to be an old man’s pastime because old men were always
regarded. as fully productive. until they grew feeble; if a man
could work, he was expected to. If shuffleboard drew the wrath
of Puritan magistrates, Leisure World or Sun City or retirement
centers in Florida would have been regarded by them as nothing
short of satanic — the worst sort of wastefulness of men’s produc-
tive capacities.)

As in so many other cases, one colony did not participate in
the sumptuary mania: Rhode Island.'§ But Rhode Island was

13. Mass. Col. Rees., TV, pt. 1 (1654), p. 287.

14. Mass. Col. Recs., 11, pp. 180, 195; TH, p.-102; TV, pt. 1, p. 20; Con. Col. Recs.,
I, p. 289; Pm. Col. Recs., XI, p. 66.

15. On Rhode Istand’s absence of sumptuary legislation, see William B. Weeden,
Economic and Social History of Naw England, 1620-1789, 2 vols. (New York: Hillary
House, [1890] 1963), 1, p. 290. Weeden provides a summary of the various
sumptuary statutes: pp. 226ff.

 
