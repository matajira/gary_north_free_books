44 Puritan Economic Experiments

anything unlawful from their inferiors, or correct them unduly,
or to lead them into temptation, “or any way dishonouring
themselves, or lessening their authority, by an unjust, indiscreet,
rigorous, or remiss behaviour.”3

In a family, church, or voluntary society, these injunctions
can be more casily applied. But the medieval perspective of the
Puritans can be seen in their unwillingness to limit the locus of
the term “family.” They were intent upon transferring the status
requirements of the family to the civil government.

The Familistic State

A family is a limited entity. Members are born into it and
grow to maturity; eventually they die. Sons and daughters leave
to form new families, and this alters the relationship between
parents and children. Parents grow old and sometimes feeble,
so they have an incentive to rear children competently; their. own
future survival may depend upon the maturity and faithfulness
of the children. The parents therefore have an incentive to avoid
keeping offspring in perpetual childhood. The relationships are
intensely personal, and therefore bounded by feelings of love,
honor, loyalty, and directly threatened by feelings of jealousy,
disrespect, or hatred.

The civil government, however, is a completely different
institution, established for different ends, and governed by differ-
ent rules. [ts function is not to father children, rear them, pro-
mote their maturity, or care for them. The state’s function is to
protect men against violence, both domestic and foreign. Inva-.
sions are to be repelled; thieves and bullies are to be restrained.
The state is to be ruled. by formal laws that are predictable,
applying to all members of society.* By its very nature, it is an
impersonal structure; it is not to respect persons in the administra-
tion of justice. Ideally, men are to be ruled by formal civil law,

3. Ibid., ans. 127-130. Ditect quote from #130.
4. FA. Hayek, The Constitution of Liberty (Chicago: University of Chicago Press,
1960).
