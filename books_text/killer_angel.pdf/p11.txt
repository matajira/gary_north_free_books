INTRODUCTION

 

For all the apparent materialism and mass mechanism
of our present culture, we, far more than any of our
fathers, live in a world of shadows.

G. K. Chesterton!

N JANUARY 1, 1900, MOST AMERICANS GREETED
() the twentieth century with the proud and cer-

tain belief that the next hundred years would
be the greatest, the most glorious, and the most glamor-
ous in human history. They were infected with a san-
guine spirit. Optimism was rampant. A brazen
confidence colored their every activity.

Certainly there was nothing in their experience to
make them think otherwise. Never had a century
changed the lives of men and women more dramatically
than the nineteenth one just past. The twentieth century
has moved fast and furiously, so that those of us who
have lived in it feel sometimes giddy, watching it spin;
but the nineteenth moved faster and more furiously still.
Railroads, telephones, the telegraph, electricity, mass
production, forged steel, automobiles, and countless
other modern discoveries had all come upon them at a
