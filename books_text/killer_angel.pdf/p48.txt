KALLer ANGEL

Dodge was especially struck by her sensuous didactae.
Later she would write in her memoirs:

Margaret Sanger was a Madonna type of
woman, with soft reddish-brown hair
parted over a quiet brow, and crystal-clear
brown eyes. It was she who introduced us all
to the idea of birth control, andit, along with
other related ideas about sex, became her
passion. It was as if she had been more or
less arbitrarily chosen by the powers-that-be
to voice a new gospel of not only sex-knowl-
edge in regard to conception, but sex-
knowledge in regard to copulation and its
intrinsic importance. She was the first per-
son I ever knew who was openly an ardent
propagandist for the joys of the flesh. This,
in those days, was radical indeed when the
sense of sin was still so indubitably mixed
with the sense of pleasure. Margaret person-
ally set out to rehabilitate sex. She was one
of its first conscious promulgators.

In the safe environs of the Greenwich Village salon,
surrounded by her radical peers, Margaret honed her
promiscuous and lascivious schtick. She set the stage
for a lifetime of sexual titillation and experimenta-
tion—a life sadly bereft of covenantal commitment.

For her, the success of the social revolution began
with the sexual revolution. If the cause were ever to
prevail culturally, it had to first prevail interpersonally
through the unleashing of carnal passion. If the work-
ers of the world were to unite, then the antiquated
morals that suppressed their true inmost feelings and

38
