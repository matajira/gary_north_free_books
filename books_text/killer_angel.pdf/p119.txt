Tue Bic Liz

which is in Judah, and he went up to the
altar; thus he did in Bethel, sacrificing to the
calves which he had made. And he stationed
in Bethel the priests of the high places which
he had made. Then he went up to the altar
which he had made in Bethel on the fif-
teenth day in the eighth month, even in the
month which he had devised in his own
heart; and he instituted a feast for the sons
of Israel, and went up to the altar to burn
incense. (1 Kings 12:26-33)

Jeroboam instituted a false feast at a false shrine,
attended by false priests, before false gods, and all on
a false pretense. But his lies succeeded in swaying the
people. Jeroboam’s mythology sanctified a whole new
set of social patterns. What would have been unthink-
able before—idolatry, apostasy, and travesty—be-
came almost overnight not only thinkable or
acceptable, but conventional and habitual. Asa result,
the new king was able to manipulate and control his
subjects.

The powerful, the would-be-powerful, and the
wish-they-were-powerful have always relied on such
tactics. Plato and Thucydides observed the phenome-
non during Greece’s classical era. Plutarch and
Augustine identified it during the Roman epoch. Ser-
gios Kasilov and Basil Argyros noted it during the
Byzantine millennium. Niccolo Machiavelli and
Thomas More recognized its importance during the
European Renaissance. And Aleksandr Solzhenitsyn
and Colin Thubron have pointed it out in our own time.

Most of the myth-makers never actually believed
in the gods upon Olympus, across the River Styx, or

109
