Tue WoMAN REBEL

disenchanted American voters for whom brash talk of
equality is a tenet of faith and justice is a badge of honor.

Not a little of the attraction during Margaret’s hal-
cyon revolutionary days was the personal charisma of
the “silly silk hat radical,” Eugene Debs. A former rail-
way worker and union organizer, Debs had become the
personification of socialism for most Americans. He had
run at the top of the Party’s ticket in five different
presidential campaigns—spanning a quarter century of
the nation’s greatest unrest and upheaval. He became
wildly popular among the disaffected as a thoughtful
and plain-spoken champion of the ordinary worker.

His rhetorical appeal was hardly unique; it was in fact
rooted in the standard material-determinist fare of the
day. He claimed that the laborer and farmer were the
oppressed victims of capitalism with its trusts, its indus-
trial tycoons, its utilities magnates, its large property
owners, its corrupt and controlled Congress, and its
ranks of unemployed. He decried the culture-wide at-
mosphere of intolerence, injustice, and heartless greed.

To remedy all these ills, Debs offered the scientific
and reasoned alternative of a “managed economy,” a
“widely distributed means of production,” an “accessi-
ble health care provision system,” and an “ideal sovi-
etized central state.”? He boldly declared that he was “in
revolt against capitalism.”* In fact, he declared an ideo-
logical war against all conventional politicians within
that system, saying:

With every drop of blood in my veins, I de-
spise their laws, and I will defy them. I am
going to speak to you as a Socialist, a Revolu-
tionist, and as a Bolshevist, if you please. The
Socialist Party stands fearlessly and uncom-

29
