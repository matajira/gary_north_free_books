Nores

4. Ibid.

5. Herman Schwartz, Margaret Sanger: A Biography
(New York: Bell Tower, 1968}, 44.

6. Ibid., 48.

CHAPTER THREE

1. Illustrated London News (March 9, 1918).

2. Hlustrated London News (August 16, 1930).

3. Lester McHenry, Fanatical Ideals: A History of
the American Left (New York: Dillard Willings, 1931),
121.

4, Francis X. Gannon, A Biographical Dictionary of
the Left (Belmont, Mass.: Western Islands, 1973), IV:
313.

5. Ibid., 317.

6. Ibid.

7. McHenry, Fanatical Ideals, 88.

8. Gannon, Biographical Dictionary IV: 313.

9. Ibid., 182.

CuHapTer Four

1, Hlustrated London News (May 14, 1932).
2, Gray, 58-59.

CHAPTER FIVE

1. Hlustrated London News (October 2, 1920).

2. Ilustrated London News (April 14, 1917).

3. McHenry, Fanatical Ideals, 129.

4. James Cotton, Paris (London: Fallows Press,
1988), 36.

5. Albert Gringer, The Sanger Corpus: A Study in
Militancy (Lakeland, Ala.: Lakeland Christian College,
1974}, 473.

6. Ibid.

7. Ibid., 477.

113
