KILLER ANGEL

she was an omnipresent whirlwind of energy and starry-
eyed adulation.

She joined the de rigeur Socialist Party and attended
all of its functions. She even volunteered as a women’s
union organizer for the Party’s infamous Local Number
Five, speaking at labor organization meetings and writ-
ing editorials and reviews for the Party newspaper, The
Cail.

By this time, virtually all of the most extreme revo-
lutionary elements of American political life had been
unified in the Socialist Party: the Radical Republicans,
the Reformist Unitarians, the Knights of Labor, the
Mugwumps, the Anarchists, the Populists, the Progres-
sivists, the Suffragettes, the Single Taxers, the Grangers,
and the Communists. Though it never moved much
beyond the fringes of the nation’s electoral experience,
it was able to tap into the anomie and ennui of a signifi-
cant segment of America’s disenfranchised class.

From ten thousand members in 1901, it had swollen
to fifty-eight thousand by 1908. More than twice that
number were recorded four years later. And its voting
strength was many times greater even than that, ac-
counting for more than six percent of all the votes cast
in the disastrously fractious national elections of 1912.

When Margaret and William Sanger entered the fray
that year, the Party had elected twelve hundred public
officials in thirty-three states and one hundred and sixty
cities, and it regularly published as many as three hun-
dred tabloids, broadsides, and periodicals. It was pro-
gressive. It was visionary. And it was making headway
among voters whose interests and fortunes had waned
under the monopolistic grip of industrial mercantilism.
Socialism has always been a peculiar temptation for

28
