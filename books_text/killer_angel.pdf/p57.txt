ARRESTED DEVELOPMENT.

were to “look the whole world in the face with a go-to-
hell look in the eyes,” Another article asserted that
“Rebel women claim the following rights: the right to be
lazy, the right to be an unmarried mother, the right to
destroy ... and the right to love.”* In later issues, she
published several articles on contraception, several
more on sexual liberation, three on the necessity for
social revolution, and two defending political assassina-
tions.

The Woman Rebel was militant, all right. In fact, it
was so militant that Margaret was promptly served with
a subpoena indicting her on three counts for the publi-
cation of lewd and indecent articles in violation of the
federal Comstock Laws.

The Comstock Laws had been passed by Congress in
1873. Their purpose was to close the mails to “obscene
and lascivious” material, particularly the erotic post-
cards and pornographic magazines from Europe which,
during the debauched and confused post-war and Radi-
cal Reconstruction period, were flooding the country.
Anthony Comstock, their chief sponsor, was appointed
a special agent of the Post Office, with the power to see
that it was strictly enforced. For nearly half a century he
fought an almost single-handed campaign to “keep the
mails clean” and to “ensure just condemnation for the
purveyors of filth, eroticism, and degeneracy.”

if convicted—and conviction was practically a fore-
gone conclusion—Margaret could be sentenced to as
much as five years in the federal penitentiary. Fright-
ened, she obtained several extensions of her court date.
But then, deciding that her case was hopeless, she deter-
mined to flee the country under an assumed name. She
had her socialist friends forge a passport, secure passage

47
