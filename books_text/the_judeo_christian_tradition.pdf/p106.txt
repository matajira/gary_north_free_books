92 The judeo-Christian Tradition

for Israel’s sake.”!?

A Most Peculiar Book

Orthodox Jews believe that the Talmud is an inspired book.
They do not treat is as “folklore.” They treat it as authoritative.

The Old Testament forbade Molech worship. “And thou shalt
not let any of thy seed pass through the fire to Molech, neither
shalt thou profane the name of thy God: T am the Lorn” (Leviticus
18:21). This is repeated in Leviticus 20:2-5. What does the Talmud
say about this practice?

MISHNAH, HE WHO GIVES OF HIS SEED TO MOILFCH
INCURS NO PUNISHMENT UNLESS HE DELIVERS LT
‘TO MOLECH AND CAUSES IT TO PASS THROUGH THE
FIRE, I’ HE GAVE fl TO MOLECH BUT DID NOT CAUSE
TT TO PASS ‘THROUGH THE FIRE, OR THE REVERSE,
HE INCURS NO PENALTY, UNLESS HE DOES BOTH.

GEMARA. The Mishnah teaches idolatry and giving to Molech,
R. Abin said: Our Mishnah is in accordance with the view that
Molech worship is not idolatry. . . . R. Simeon said: If to Molech,
he is liable; if to another idol, he is not.

R. Aha the son of Raba said: If one caused all his seed to pass
through [the fire] co Molech, he is exempt from punishment,
because it is written, of hy seed implying, but not all thy seed.!#

This approach to ethics and civil law has become known as
“Talmudic reasoning.”

Much of the Talmud’s space is devoted to diet.’> For example,
it says that cating dates makes a person incligible to render a legal
decision. “Rab said: If one has caten dates, he should not give a

12. Yebamoth 63a.

13. Sanhedrin 64a.

14. Sanhedrin 64b.

45. ‘Vhe New ‘Testament substitutes the Lord’s Supper for the dietary laws of the
Old ‘lestament. Both of these meal requirements have served as screening devices.
The New Testament annuls the Old ‘Testament's dietary laws: Acts 10; 1 Corinthians
8.
