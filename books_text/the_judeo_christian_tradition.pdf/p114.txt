100 The Judeo-Christian Tradition

non-Jews react strongly against the Talmud’s teaching, for exam-
ple, that it is legitimate for a man to have sexual relations with a
little girl, just so long as she is under the age of three? The
Mishnah says: “WHEN A GROWN-UP MAN HAS HAD SEX-
UAL INTERCOURSE WITH A LITTLE GIRL, OR WHEN
A SMALT. BOY HAS HAD INTERCOURSE WITH A
SROWN-UP WOMAN, OR [when a girl was accidentally] TN-
JURED BY A PIECE OF WOOD [IN ALL CASES] THEIR
KETHUBAH IS TWO HUNDRED [ZUZ]; SO ACCORDING
TO R. MEIR.”* Then the Gemara explains: “It means this:
When a grown-up man has intercourse with a little giv] it is
nothing, for when the girl is less than this [annotation: “Lit., ‘here’,
that is, less than three years old”] it is as if one puts a finger into
the cyc; . . “85 Should Orthodox Jews really expect Christians
to accept the moral validity of such a teaching? Surely the vast
majority of Jews today would reject it if they knew about it, which
they do not.

As T said carlier, it might be argued that the rabbis were not
really arguing for such a seemingly grotesque ethical principle,
that it was all some sort of hypothetical debate. This particular
debate in the Talmud concerned the kethubah. The kethubah was
a deed given by a husband to his bride which specified that if he
divorced her, she would receive a monetary payment. ‘Ihe mini-
mum payment was 200 zuz for virgins, but only 100 zuz for
non-virgins.** A defender of the ‘Talmud might argue that what
the Mishnah really teaches is the perfectly reasonable principle
that very young girls who are subjected to the kinds of intercourse
described in the text are to be considered as virgins. While it would
be possible to argue that this law's ethical concern focuses only
on the innocence of the girl) under three year old who is used
sexually abused, and that the words “it is nothing” refer only to

34, Kethuboth Va.
35. Kethuboth Ub.

36. CL “Ketubbah,” in The Principles of Jewish Lao, edited| by Menachem Elon
(Jerusalem: Keter, [19752]}, col. 387. The zuz was the smallest Jewish coin.
