Undermining Justice 135

says of them, And you shall bequeath them to your sons (Lev. 25:46). .
But aren’t bonds as valuable as movable stolen goods? No; “bonds
have no intrinsic value.”?

 

Committing Crimes Rationally

Furthermore, if a person is subject to flogging for a crime
involving the theft of money, Maimonides insisted that he need
not make any monetary penalty payment whatsoever to the victim,
“because one is not subjected to both flogging and paying.”* Why
would a thief be subject to flogging in the first place? Possibly
because he had stolen for a second or third time. We would
imagine that the victim would receive compensation in the form
of a monetary penalty payment, and the civil authorities would
also flog the thief as a warning. Not in Maimonides’ system. But
he did make this clarification: the criminal must become subject
to the monetary penalty and the flogging at the same time; if he
commits two separate offenses, he can be required to suffer both
penalties.

‘What, then, is the economically rational conclusion for thieves?
Steal money, not goods, and be sure you commit a trespass at the
same time that will involve flogging if you are convicted.® Habitual

 

3. Ibid., U1:2, p. 64. Yet he admits elsewhere that “if one burns a creditor's bonds,
he must pay the full debt recorded in the bond —for although the bond is not
intrinsically money, he has caused the Joss of money. . . .” Tbid,, “Laws Concerning
Wounding and Damage,” VIL, p. 185.

4, bid, I11:1, p. 67. He made this one exception: injuring someone, who then
becomes cligible for compensation: ibid., “Laws Goncerning Wounding and Damag-
ing,” 1V.9, p. 173.

5. In the case of robbery — stealing openly by threatening the victim ~he said
that the restitution payment is mandatory, so there can be no flogging, because “any
prohibition the transgression of which may be repaired by restitution does not entail
flogging.” fbid., “Laws Concerning Robbery and Lost Property,” f:1, p. 90, If we are
to accept this explanation at face value, then why did he ever bring up the parailel
issue of crimes that require monetary penalties in relation to flogging? Shouldn’t the
requirement of restitution always climinate the possibility of flogging? There is an
inconsistency here.

  

6. Maimonides did not say what kind of crime would bring a person under both
penallies simultaneously. This makes it difficult to know what he had in mind.
