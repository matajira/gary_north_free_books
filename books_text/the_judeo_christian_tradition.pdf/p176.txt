162 The Judeo-Christian Tradition

absence of a king or emperor. Anti-Semitism always becomes
especially violent in times of a lost war. The Jews must be guilty:
this is the word that is quickly passed round. For are they not as
ready to shoulder hard times without a complaint as they were to
profit in the good? The star of Judah shines bright, and pogroms
break out, whenever the Gentiles have just buried their Nebuchad-
nezzar or their Tiberius with disintegration.”

As this becomes increasingly clear to both orthodox Christians
and Orthodox Jews, T think the response of both groups will be to
de-emphasize the words of mutual condemnation found in the
‘Talmud and the New Testament, This is not to say that cither
group will deny the truth of its respective holy book, but it is to
say that there is a time to emphasize differences and a time to
emphasize similarities. ‘[o put it graphically, if you are in a foxhole
with someone of a rival covenant, and the enemy’s shock troops
are coming over the ridge, your immediate concern is not the
precision of your partner’s theology, it is whether he can shoot
straight and whether he can spare a few rounds of ammo.

T can see the enemy coming. Hand me that 30-round clip,
Yitzhak, and we'll discuss the fine points of our theology later.

The Future of the
Judeo-Christian Tradition

It is fruitless to trace the history of this alleged phenomenon.
Tt never existed. By the time that Jews became a force in history,
the humanist worldview predominated in the West. In fact, it was
the toleration of Jews by Enlightenment socicty that allowed them
to become a force in modern history. This toleration was narrowly
formulated, however. All that the Jews were asked to do in order
to reap the blessings of social and political participation was to
give up the Talmud, and the vast majority of those who survived
World War I had done so by 1948.

This is not to say that there were never any common-ground
interpretations of the Old Testament between Christians and Jews.

2, Ibid, pp. 222-23
