THE MARK OF
THE OLD COVENANT

But glory, honour, and peace, to every man that worketh good,
to the Jew first, and also to the Gentile: For there is no respect of
persons with God. . . . Behold, thou art called a Jew, and restest
in the law, and makest thy boast of God, And knowest his will,
and approvest the things that are more excellent, being instructed
out of the law; And art confident that thou thyself art a guide of
the blind, a light of them which are in darkness, An instructor of
the foolish, a teacher of babes, which hast the form of knowledge
and of the truth in the law. Thou therefore which teachest another,
teachest thou not thyself? thou that preachest a man should not
steal, dost thou steal? Thou that sayest a man should not commit
adultery, dost thou commit adultery? thou that abhorrest idols,
dost thou commit sacrilege? Thou that makest thy boast of the
law, through breaking the law dishonourest thou God? For the
name of God is blasphemed among the Gentiles through you, as
it ig written. For circumcision verily profiteth, if thou kcep the
law: but if thou be a breaker of thc law, thy circumcision is made
uncircumcision. Therefore if the uncircumcision keep the right-
cousness of the law, shall not his uncircumcision be counted for
circumcision? And shall not uncircumcision which is by nature, if
it fulfil the law, judge thee, who by the letter and circumcision
dost transgress the Jaw? For he is not a Jew, which is one out-
wardly; neither is that circumcision, which is outward in the flesh:
But he is a Jow, which is one inwardly; and circumcision is that
of the heart, in the spirit, and not in the letter; whose praise is not
of men, but of God (Romans 2:10, 17-29).

Ts any man called being circumcised? Jet him not become
uncircumcised. Is any called in uncircumcision? let him not be
circumcised. Circumcision is nothing, and uncircumcision is noth-
ing, but the keeping of the commandments of God (I Corinthians
7:18-19).
