202 The Judeo-Christian Tradition

magic, 29-30
Maimonides, Moses
Aristotle, 150
bondservants, 111
Code, (sce Mishneh Toreh)
double standard, 116-20, 127-28
expediency, 118
false weights, 137
gentiles & law, 82
Guide, 108, 146
heathen student, 59
injuries, 112-13,
kidnapping, 138-39
legalism, 115
lost property, 117-19, 126
Mishnch Torah, 106, 108-10, 128-29
Old Testament, 111-15
Orthodox Judaism &, 146
restitution, 113-14, 133-37
RMBM, 106
robbery, 137-38
Sabbath & theft, 136
secret writing, 97-98
slavery, 107
style, 81
thefi, 113-14, 116-17, 133-34, 135-36
victim, 138
, 136-37
marriage, 16
Mead, Sidney, 24, 25
medical schools, 168
Meir, 77, 82
memory, 77
Messiah, 6, 10, 41, 49, 50, 109,
130, 154, 155, 174
Middle Ages, 104, 106-8, 150
Mishnah, 76, 79-80, 89, 117, 140
modernism, 50-5!
Mogen David, 65-67
Molech, 92
Morgenstern, Julian, 1510
Moses, 40, 51, 54, 75

 

 

vieti

 

“Moslem-Christian tradition,” 52
smurder, 62, 127
Murray, John, 7, 62

Napoleon, 153

nationalism, 24

natural law, 33

Nazism, 8, 9-10, 150-51, 181

Neuser, Jacob, 67-68, 79
New Testament, 49, 160

new wine, 68

Nicodemus, xi

Nisbet, Robert, 32

Noah, 60-61, 107

Noahides, 61

occultism, 29, 31, 6%

Old Testament, 48

olive tree, x, 5

Onkelos, 86n

oral law, 76, 80, 89, 99, 103

Orthodox Judaism
Christians’ view, 65
common enemies of, 26
counterfeit, 173
cultural circumcision, 154-55
declining influence, 147-48
diet, 154
education of children, 154
evolutionary ethies, 145-49
humanism &, 153
ignorance about, 26
kingdom, 154
little leadership, 168
Messiah &, 154-55
Old Testament &, 54
Pharisces &, 88
rival religion, 80
scerecy, 4
separation from Jews, 153-54
Talmud, 48, 160
toleration &, 148-49
