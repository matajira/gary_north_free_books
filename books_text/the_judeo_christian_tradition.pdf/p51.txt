3

THE SOVEREIGN MASTER

When Pilate therefore heard that saying, he brought Jesus forth,
and sat down in the judgment seat in a place that is called the Pavement,
but in the Hebrew, Gabbatha. And it was the preparation of the
passover, and aboul the sixth hour: and he saith unto the Jews, Behold
your King! But they cried out, Away with him, away with him, crucify
him. Pilate saith unto them, Shall I crucify your King? The chief
priests answered, We have no king but Caesar (John 19:13-15).

The Jews had a king, they said: Caesar. This king was simulta-
neously religious and political, for the political order of imperial
Rome was considered sacred. Caesar was king of the civil cove-
nant. And he was indeed their politica] sovereign.

But he was not their only king. They lived in God’s kingdom,
and God’s kingdom encompassed Caesar’s. Caesar was under
God, though over them. They knew this, but they kept silent about
it publicly. To have admitted publicly what their covenantal theol-
ogy required them to believe would have been an act of revolution.
They would have faced the same kind of persecution that the early
Church faced for its public affirmation of obedience to Caesar on
a strictly political basis; this was a denial of Roman religion, an
act of sacrilege.’ The leaders of Isracl in Christ’s day were not
ready to take this step. When they were ready —in A.D. 70 and

1. R. J. Rushdoony, The One and the Many: Studies in the Philosophy of Order and
Ultimacy (Fairfax, Virginia: Thoburn Press, [1971} 1978), ch. 6.

37
