10

THE ISOLATION OR
EROSION OF ORTHODOX JUDAISM

Beware lest any man spoil you through philosophy and vain deceit,
after the tradition of men, afler the rudiments of the world, and not
after Christ (Colossians 2:8).

The heart and soul of Orthodox Judaism is its evolutionary
ethical character, not its explicit theology. So radical is this process
theology that Orthodox Jews believe that God Himself is continu-
ally engaged in a study of His own law, in association with the
souls of deceased Jews. This goes on in the Academy on High -a
concept so preposterous that modern Jewish scholars downplay it,
describe it as merely a metaphor, and refuse to consider the
possibility that Jews once took the Talmud and the Old Testament
as literally inspired. (Literalism of ancient texts and ancient relig-
ious beliefs is simply not permitted to the founders of still-existing
Westcrn religions by those who still want the prestige, communal
stability, and tenured security provided by the skeptical heirs of
these still-literalistic religions.) The uninitiate—a very important
word — cannot easily understand this commitment to process.
Rabbi Louis Finkelstein was the head of the Jewish Theological
Society of America. In his 1961 introduction to the reprint of
Solomon Schechter’s Aspects of Rabbinic Theology (1901), he writes:

The view that inquiry into the nature and requirements of
Torah is more than a human need, being a cosmic process, is even
more difficult to communicate to the uninitiate. Doubtless that is

145
