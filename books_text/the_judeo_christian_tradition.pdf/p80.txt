66 The Judeo-Christian Tradition

Mogen David or Magen David. After all, it appears on the state

of Israel’s national flag. Ask him where the latter symbol origi-
nated, and you will get a blank stare. He has no idea.
The fact is, the so-called star of David is a universal pagan

symbol, long pre-dating Judaism. It was adopted by Zivnists in
the late nineteenth century. Before then, it was used as a decora-

tion by Jews, Muslims, and Christians. It was long called the Seal

of Solomon. How many Jews, let alone Christians, have ever been
informed of the following information, presented by Jewish scholar

and art historian Joseph Gutmann?

The Magen David is a hexagram or six-pointed star. It appears
as carly as the Bronze Age and is at home in cultures and civiliza-
tions widely removed in time and geographic area. Mesopotamia,
India, Greece, and Etruria are among the places where it has been.
found — but without any discoverable meaning. Possibly it was an
ornament or had magical connotations, Only occasionally before
the 1890's is it found in a Jewish context; the oldest Jewish example
is from seventh-century B.C.E. {B.C} Sidon, a seal belonging to
onc Joshua ben Asayahu. In the synagogue at Capernaum, Galilee,
a synagogue which may date from the fourth century C.K. [A.D.],
the Magen David is found alongside the pentagram and the swas-
tika, but there is no reason to assume thal the Magen David or the
other signs on the synagogue stone frieze served any but decorative
purposes.

In the Middle Ages, the Magen David appears quite fre-
quently in the decorations of European and Islamic Hebrew manu-
scripts and cven on some synagogues, but appears to have no
distinct Jewish symbolic connotation; it is also found on the seals
of the Christian kings of Navarre, on mediaeval church objects, and
on cathedrals. As a matter of fact, what is today called Magen
David was generally known as the Seal of Solomon in the Middle
Ages, especially in Jewish, Christian and Istamic magical texts, In
the medieval Islamic world the hexagram was popular and was
widely used. Generally known, especially in Arab sources, as the
Scal of Solomon, it gradually became linked with a magic ring or
seal believed to give King Solomon conirol over demons. An early
Jewish source in the Babylonian Talmud (Gittin 68a-b) already
