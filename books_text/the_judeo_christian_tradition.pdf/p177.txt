Conclusion 163

Occasionally there was. ‘ake the case of permanent chattel slav-
ery. (I mentioned this in Chapter 8.) The Talmud first offered the
world the theory that the so-called “curse of Ham” was racial:
Negroes arc supposedly these “sons of Ham.” This became the
universal belicf of Christians and Muslims in the Middle Ages.*
This is not an aspect of the alleged Judeo-Christian that anyone
is proud of today. When the anti-slavery movement appeared in
the late eighteenth century — it was pioneered by the Quakers® — it
was initially resisted by virtually all churches and synagogues.
Only after the American Civil War did all Jews and Christians at
last publicly accept the viewpoint that slavery had been a great
evil. This change in opinion had nothing to do with any alleged
Judeo-Christian tradition. It was in fact a denial of that tradition.

The Judeo-Christian tradition is an historical myth. But it
need not be a future myth. There still remains the possibility that
Orthodox Jews and orthodox Christians can wage effective war
against the secular humanists who have invaded the ranks of the
faithful, and whose political representatives collect vast sums of
money from us in taxes that are then used to finance a worldview
that we oppose. The Judeo-Christian tradition can exist, for the
sake of the peace.

It must begin with the recovery of respect for Old Testament
law. This is what the debate between Christians and Jews has
always been about: the proper interpretation of the Old Testa-
ment. For the sake of the peace, Orthodox Jews can work with
orthodox Christians, and vice versa. The Christians can stop their
antinomian attacks on the law. The Jews can stop avoiding the
specifics of biblical law by appealing to obscure passages in the

3. Winthrop D. Jordan, White Over Black: American Attitudes Toward the Negro,
1550-1812 (Chapel Hill: University of North Carolina Press, 1968), p. 18. See
Chapter 8, Note 5. He cites the Babylonian Talmud (Soncino Press edition), tractate
Sanhedrin, vol. II, p. 745; Midrash Rabbah (Soncino Press edition), vol. I, p. 293.
Reprinted by Bloch Pub. Co., New York.

4. David Brion Davis, Siavery and Human Progress (New York: Oxford University
Press, 1984), p. 87.

5. dbid., p. 108.
