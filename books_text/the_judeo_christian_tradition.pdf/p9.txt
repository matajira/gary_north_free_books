PREFACE

This Jesus hath God raised up, whereof we all are witnesses.
Therefore being by the right hand of God exalted, and having received
of the Father’ the promise of the Holy Ghost, he hath shed forth this,
which ye now see and hear. For David is not ascended into the heavens:
but he saith himself, The Lord said unto my Lord, Sit thou on my right
hand, Until I make thy foes thy footstool. Therefore let all the house
of Israel know assuredly, that God hath made that same Jesus, whom
ye have crucified, both Lord and Christ (Acts 232-35).

What is this book about? It is about Bible-believing Christian-
ity and Orthodox Judaism, It is about the Old Testament, the
New Testament, and the Talmud. It is about the law of God. It
is about the Judeo-Christian tradition, or lack thereof.

The book also is about Israel. It is first and foremost about
what Paul calls the Israel of God: the Christian Church. There is
distressing tendency among premillennial dispensationalists to ig-
nore—one might even say deny— these crucial New Testament
verses: “But God forbid that I should glory, save in the cross of
our Lord Jesus Christ, by whom the world is crucified unto me,
and I unto the world. For in Christ Jesus neither circumcision
availcth any thing, nor uncircumcision, but a new creature [erca-
tion]. And as many as walk according to this rule, peace be on
them, and mercy, and upon the Israel of God” (Galatians 6:14-
16). These verses interpret the Old Testament’s terminology and
prophecies. My principle of biblical interpretation is this: “The
Old Testament does not interpret the New; the New Testament

ix
