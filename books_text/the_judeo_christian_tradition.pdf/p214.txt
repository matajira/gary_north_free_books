200 The Judeo-Christian Tradition

Jesus
ascension, ix
atonement, xii
Balaam, 861
Bridegroom, 11
coin, 39-40
crucifixion, ix
final prophet, 1]
forgiveness, 15
Messiah, 6, 49, 50
Pharisees vs., xi, 80, 84
right hand of God, ix
sacrifice, 177-78
Savior, 177
sorcerer, 86n
sorcerer?, 4]
Jew
civil religion &, 23-24
haggler, 148n
inward, xiv
who is 2, 501
Jewish, 79
Jews
ad hue alliances, 26
assimilation, 8, 56, 57 64, 153
autonomy, 149
beloved of God, 6
cast off?, 5, 8-9
ersion of, xii, 6

  

cout

 

Diaspora, 38
emancipation of, 148-49, 153
enemies of God, 6
evangelism of, xii-xiii
evangelization
“fans,” 152

fulness, 6

ghettos, 124-25
gratieade to, 180
Hasidic, 17-18
Imperial coin, 39-40
intermarriage, 150
isolation, 64

jealousy of, 167
king of, 37
liberalism, 153
magic, 29-30

Paul &, 47
persecution of, 8
rapture &, xii
Reform, 62, 64-65
re-grafting, 16
reincarnation, 17
Rome &, 13
Savior, 177

 

separatism, 5, 57
spared, 15

U.S., 1840, 22
voting, 65
witness to, 53

 

Yemenite, 63

Joel, 12

Jobanan, 61, 82

Johuson, Paul, 10¢

Josephus, 77-78

Judah the Prince, 76

Judaism
abandoning, 17
anticredal, 147
anti-dogma, 147
anti-semitism, 161-62
Ashkenazi, 62-63
assimilation, 150
business, 104
Christianity vs., xiii, 17, 51, 67-68, 70,

130, 160-61

code of silence, 83
conservative, 89
convert to, 17
creeds, 173
Diaspora, 109, 140
dietary laws, 154
discrimination, 21
divided, 50, 62-68, 153-54
emancipation, 153
