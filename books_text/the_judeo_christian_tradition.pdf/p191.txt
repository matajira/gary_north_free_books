APPENDIX
ON EVANGELIZING JEWS

The accusation has been made by Mr. Hal Lindsey that
Christians who do not share Mr. Lindsey’s views on eschatol-
ogy — that is, pre-tribulational, dispensational, premillennial-
ism — hold to a theology which leads inherently and incscapably
to anti-Semitism. He presents this utterly bizarre thesis in his
provocatively titled book,. The Road to Holocaust.' I do not share
Mr. Lindsey’s views on eschatology, nor has the Church of Jesus
Christ throughout most of its history. (Mr. Lindsey’s views on
eschatology appeared in Church history no earlier than 1830.) Is
the Church therefore implicit anti-Semitic? Is my theology inher-
ently anti-Semitic, as he says?

Some Jews say yes. Why? Because the Church believes that
Christians should tell Jews that they, like everyone clsc in history,
need to believe in Jesus Christ as their Savior in order to receive
eternal life, Jesus died for the sins of men, and anyone who does
not accept this sacrifice as his substitute payment to God will go
to hell and spend cternity in torment. “And death and hell were
cast into the lake of firc. This is the second death. And whosoever
was not found written in the book of life was cast into the lake of
fire” (Revelation 20:14-15). Jesus said: “He that believeth on the
Son hath everlasting life: and he that believeth not the Son shall
not see life; but the wrath of God abideth on him” (John 3:36}.
Jesus Christ is our sacrifice — the only sacrifice acceptable to God.

1. Lindsey, The Road to Holocaust (New York: Bantam Books, 1989).

177
