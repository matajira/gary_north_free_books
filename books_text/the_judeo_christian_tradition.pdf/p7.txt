TABLE OF CONTENTS

Preface, 20 ee
Introduction. © 0 ee
1, Two Branches, One Briden 2.2 ee
2. Political Idolatry, 2... ee ee

POINT 1: SOVEREIGNTY
3. The Sovereign.Master, 20.02.

POINT 2: REPRESENTATION

4. Interpretation and Representation... .......
5. A Separate People in Our Midst... 2.2...

POINT 3: LAW
6. The Talmud: A Closed Book, Even When Open. . .
7. “You Have Heard It Said”... 2. 2 ee
8. Rabbi Moshe ben Maimon, “Maimonides”... . . .

POINT 4: SANCTIONS

9. Undermining Justice. 2... ee ee

POINT 5: CONTINUITY

10. ‘The Isolation or Erosion of Orthodox Judaism. . . .
Conclusion. . 2.2... an
APPENDIX: On Evangelizing Jews.
Bibliography. . .

 

Scripture Index. 6. ee
Subject Index... 2... .....- .
About the Author. 6... ee

 

vii

37

47
59

73
84
106

133

145
159
177
191
193
197
205
