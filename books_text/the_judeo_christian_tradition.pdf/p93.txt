The Talmud: A Closed Book, Even When Open 79

arly world, both Jewish* and gentile, commented on the undis-
puted triumph of the Pharisees after the fall of Jerusalem (which
lives on as Orthodox Judaism): “Until the destruction of the
Second Temple in A.D. 70 they had counted as one only among
the schools of thought which played a part in Jewish national and
religious life; after the Destruction they took the position, naturally
and almost immediately, of sole and undisputed leaders of such
Jewish life as survived. Judaism as it has continued since is, if not
their creation, at least a faith and a religious institution largely of
their fashioning; and the Mishnah is the authoritative record of
their labour. Thus it comes about that while Judaism and Christi-
anity alike venerate the Old Testament as canonical Scripture, the
Mishnah marks the passage to Judaism as definitely as the New
Testament marks the passage to Christianity.”** Neusner is cor-
rect when he observes that “the rabbis of late antiquity rewrote in
their own image and likeness the entire Scripture and history of
Israel, dropping whole eras as though they had never been, ignor-
ing vast bodies of old Jewish writing, inventing whole new books
for the canon of Judaism. . . .”

The supremacy of the Mishnah after A.D. 70 meant the
triumph of the Pharisees. Similarly, in the modern era, the
waning of the Mishnah in Judaism has meant the waning of the
Pharisees’ spiritual heirs, Orthodox Jews.

Again, the Mishnah is the written version of the Jews’ oral

23. I do not understand why it is polite to say “Jewish” and frequently impolite
to say “Jew.” The suffix “ish” means “sort of.” Surely, Christians would take offense
if they were referred to as “Christianish.” [ should think that a Jew, if asked by
someone, “Are you Jewish,” would reply, “No. I'm a Jew.” Anyway, an Orthodox
Jew might respond this way. An Cirthodox Jew regards Reform Jews as Jewish, i.e.,
sort of Jews.

24. Herbert Danby, “Introduction,” The Mishnah (New York: Oxford University
Press, [1933] 1987), p. xi

25, Jacob Neusner, “Iwo Faiths Talking about Different Things,” World & I
(Nov. 1987), p, 690.

26. The standard Jewish work on the Pharisees is Rabbi Louis Finkelstein’s study,
The Phariseas, 2 vols. (3rd ed.; Philadelphia: Jewish Publication Society of America,
1963).

 
