The Tatmud: A Closed Book, Even When Open 75

Sea is closer to it.) Jews have called it “the Great Labyrinth” and
“Sphinx-like,”® which is getting even closer, given the occult roots
of the labyrinth and its connection with the Sphinx. R. Travers
Herford, the Unitarian master (yet concealer) of the Talmud,
described it as “a great wilderness.”!° Few Christians have ever
scen a sct; almost no one reads it today, Christians or Jews. An
unabridged version of the Talmud became available in English
only in the early 1950’s— about two generations after the vast
majority of English-speaking Jews had ceased to pay any attention
to it. Remember, it is 34 volumes long, plus a large index volume.
Prior to the ‘mid-twenticth ccntury, it had been a hidden book to
the English-speaking gentile world. As England’s chief rabbi, J.
H. Hertz mentions in his Foreword, “All the censored passages
reappear in the Text or in the Notes.”!' Earlier editions, most
notably Michael Rodkinson’s (1903), had been voluntarily cen-
sored by their editors.

The Talmud is a compilation of the oral teachings of the rabbis
from perhaps 200 years before Christ until the end of the second
century, A.D. (Mishnah), plus an additional three hundred years
of commentary (Gemara). The total is almost seven (possibly
eight) centuries.'? Those who adhere to the Talmud claim that
this oral tradition extends back to Moses. They cite Exodus 24 as
proof: “And Moses came and told the people all the words of the
Lorp, and all the judgments” (3a). Then we read, “And Moses
wrote all the words of the Lorn” (4a). But he did not write the

8. Jacob Schachter, “Talmudical Introductions Down to the ‘Time. of Chajes,”
in Z. H. Ghajes, The Student's Guide Through the Talmud (London: East and West
Library, 1952), p. xvi.

9. Gary North, Moses and Pharaoh: Dominion Religion vs, Power Religion (Tyler,
Texas: Institute for Christian Economics, 1985), Appendix C: “The Labyrinth and
the Garden.”

10. R. Travers Herford, Christianity in ‘Talmud and Midrash (London: Williams and
Norgate, 1903), p. 1.

IL. “Foreword,” Baba Kamma, The Babylonian Talmud (London: Soncino Press,
1935), p. xxvii.

12. Schachter, “Talmudical Introductions,” in Chajes, Student's Guide, p. xvi (foot-
note).
