16 THE TtOAX OF HIGHER CRITICISM

Old Testament in an attempt to substitute natural
law for biblical law. Anyone who fails to understand
the ethical nature of this intellectual conflict does not
understand the history of biblical higher criticism. The
attack on the Old Testament was a fundamental as-
pect of the coming of modern humanist civilization.

Only as a result of the attack by Deists on the author-
ity of Scripture (preparations for which were made, against
their own intentions, by Latitudinarians, Locke and New-
ton), an attack which they made step by step, did the leg-
acy of antiquity in the form of natural law and Stoic
thought, which since the late Middle Ages had formed the
common basis for thought despite all the changes of theo-
logicat and philosophical direction, remain the one undis-
puted criterion. This produced a basically new stage both
in the history of ideas and in the English constitution. This
position already contains the roots of its own failure, in
that the consistent development of the epistemotogical prin-
ciples of Locke and Berkely [sic] by Hume soon showed
that its basic presuppositions were untenable. However,
two irreversible and definitive developments remained,
which had made an appearance with it: the Bible lost its
significance for philosophical thought and for the theorcti-
cal foundations of political ideals, and ethical rationalism
(with a new foundation in Kant’s critique) proved to be
one of the forces shaping the modern period, which only
now can really be said to have begun.'!

 

Reventlow has pointed out that higher criticism

V1. Hbid, pp. 413-14.
