40 THE, HOAX OF HIGHER CRITICISM

ingful reconstruction of what the ancient authors (“re-
dactors”) really wanted to convey to all mankind, de-
spite each one’s short-term goals of political or bu-
reaucratic manipulation. Fourth, he tries to present
a “deeper” message for modern man that transcends the
Bible’s unfortunately jumbled texts. Finally, the higher
critic offers his version of the Bible’s true transcendent ethical
unity. Somehow, this newly discovered transcendent
ethical unity always winds up sounding like the last
decade's political manifesto for social democracy, or
else it sounds like Marxism.

A good statement of this operating presupposi-
tion of textual disunity is J. I., Houlden’s remark that
“There is, strictly speaking, no such thing as ‘the X
of the New Testament’, . . . It is only at the cost of
ignoring the individuality of each, in thought and ex-
pression, that the unified account can emerge. . . .
There can be no initial assumption of harmony.”? So,
it is supposedly illegitimate to speak of “the X of the
New Testament.” Well, how about a Aeavenly Author
of the New Testament? How about solving the equa-
tion as “X =God.” Sorry, says Houlden implicitly,
we cannot begin with any such assumption, Well, then,
how about “the grammar of the New Testament”? We
will posit “X = grammar.” Houlden is then silent, as
befits a man who has implicitly denied the grammati-

2 J. 1. Houlden, Ethics and the New Testament (Middlesex, Eng-
land: Penguin, 1973), p. 2; cited by Kaiser, ibid., p. 13,
