70 ‘HE HOAX OF HIGHER CRITICISM

with questions of economic policy. Christian Reconstruc-
tion is more action-oriented, but it also covers various
aspects of Christian social theory. Covenant Renewal ex-
plains the Biblicat covenant and works out its implications
for the three social institutions of culture: family, church
and state, Dispensationalism in Transition has its em-
phasis on eschatology (doctrine of the endtimes). It chal-
lenges traditional Dispensationalism’s “Code of Silence.”

The purpose of the ICE is to relate biblical ethics to
Christian activities in the field of economics, To cite the
dtle of Francis Schaeffer's book, “How should we then
live?” How should we apply biblical wisdom in the field of
economics to our lives, our culture, our civil government,
and our businesses and callings?

If God calls men to responsible decision-making, then
He must have standards of righteousness that guide men in
their decision-making, It is the work of the ICE to discover,
illuminate, explain, and suggest applications of these guide-
lines in the field of economics. We publish the results of our
findings in the newsletters.

The ICE sends out the newsletters free of charge. Anyone can
sign up for six months to receive them. This gives the reader
the opportunity of seeing “what we're up to,” At the end of
six months, he or she can renew for another six months,

Donors receive a one-year subscription. This reduces
the extra trouble associated with sending out renewal
notices, and it also means less trouble for the subscriber.

There are alsa donors who pledge to pay $10 a month.
They are members of the ICE's “Reconstruction Committee.”
They help to provide a predictable stream of income which
finances the day-to-day operations of the ICE. Then the
donations from others can finance special projects, such as
the publication of a new book.
