The Techniques of Higher Criticism 35

aspect is the no doubt unintentional demeaning of the
intelligence of the lawgiver who was responsible for
the presentation of the material available to us. E.
M. Forster, struck by the cavalier way in which we
treat the past, attributed the attitude to the fact that
those who lived then are all dead and cannot rise up
and protest.”!7

He is being much too kind. The scholars’ “de-
meaning of the intelligence of the lawgiver who was
responsible for the presentation of the material avail-
able to us” is all too intentional, for that Lawgiver is
God Almighty, who will judge every man on judg-
ment day. Higher critics are determined to deny that
such a cosmic Lawgiver exists, and they do their best
to make His laws seem like an incoherent collection
of disjointed and self-contradictory pronouncements,
a judicial jumble compiled by a series of editors who
apparently could not keep clear in their minds any-
thing that was written in the text in front of them that
was farther back or farther forward than three lines.
Somehow, these deceptive ancient masters of language
and textual subtleties could not keep any argument
straight, or remember the plot line of even a one-page
story. Their heavy-handed attempts to revise the an-
cient texts for their own contemporary purposes were
so badly bungled that they succeeded only in so dis-

17. Calum M. Carmichael, Law and Narrative in the Bible: The Evi-
dence of the Deuteronomic Laws and the Decalogue (Ithaca, New York
Corel] University Press, 1985), p. 14.
