42 THE HOAX OF HIGHER CRITICISM

purity and in its humanity.” Such a conclusion is
“highly suspect,” Herr Doctor Boccker assures his read-
ers.3 Why is this conclusion “highly suspect”? Because
ic breaks with the supposed academic ncutrality and
ethical relativism of modern scholarship, especially
modern biblical scholarship.

Young scholars are informed subtly from the out-
sct of their carecrs as undergraduates that they must
always begin with the assumption that all rcligious
faiths are equal (except for fundamentalism, which
preaches an infallible Bible), all pofitical systems are
equal (except for Nazi Germany’s, of course, mainly
because the Nazis lost the war, and South Africa’s,
which is not based on the politics of black Africa: “one
man, one vote, one time only”), and all nations are
equal (except for the United States, which occasion-
ally dares to call the Soviet Union into question). What
this kind of worldview produces is men without spines
who cannot distinguish truth from falsehood, right-
cousness from perversion, or a cause worth dying for
from the latest political slogan. It is only by the com-
mon grace of God that they can distinguish AIDS
from scarlet fever, except that they probably think
that people with scarlet fever should be quarantined.

So, in order to prove all this, higher critics self:

3. Hans Jochen Boecker, Law and the Administration of Justice in the
Old Tastament and Ancient East, translated by Jeremy Moiser (Minnea-
polis, Minnesota: Augsburg, [1976] 1980}, p. 16.
