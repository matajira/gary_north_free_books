220 CHRISTIANITY AND CIVILIZATION

Calvin both presents his case for paedobaptism as well as
defends it against various attacks by employment of the covenant
idea. His positive arguments build initially upon his already
established point of the continuity of the Old and New Covenants.
It is due to the continuity of the covenant with the Jews and with
Christians that enables Christians to baptize their infants:

For he expressly declares that the circumcision of a tiny infant will
be in lieu of a seal to certify the promise of the covenant, But if the
covenant still rernains firm and steadfast, it applies no less today
to the children of Christians than under the Old Testament it
pertained to the infants of the Jews. Yet if they are participants
in the thing signified, why shall they be debarred from the sign?
(IV. 16. 5)

On the other hand, the discontinuity of the covenants in externals
allows Calvin to refute an objection against his view:

The objection that there was a stated day for circumcision is sheer
evasion. We admit that we are not now bound to certain days like
the Jews; but since the Lord, without fixing the day yet declares
that he is pleased to receive infants into his covenant with a
solemn rite, what more do we require? (IV. 16. 5)

Calvin is so adamant that the covenant with the Jews continues into
the New Covenant era that he asserts that to deny this is nothing less
than blasphemy. This is because such a view implies that Christ's
coming actually narrowed God’s grace rather than expanding it:5°

 

at this point are worth citing: “I venture the affirmation: the confusion into which
Luther and Calvin and their respective disciples have tumbled headlong in this mar-
ter is hopeless... . Let anyone read chapters XV and XVI of Book IV of the
Institutio in order and convince himself where the great Calvin is sure of his subject
and where he obviously is not sure of it, but is patently nervous, involved in an
exceedingly unperspicuous train of thought, scolding; where he lectures when he
should convince, secks a way in the fog which can lead him to no goal because he has
none.” Cited in Jewett, op. cit., p. 92. One can only remark that Barth's analysis
does not accord with Jewett’s own frequent comment on how often Calvin’s various
arguments have been employed by his theological descendants! If they were that
“unperspicuous” they would have been discarded long ago. It also appears evident
that Barth’s deepest charge against Galvin that his thought is a “fog” thal leads him
to an non-existent “goal” is a direct negation of Calvin's fundamental concept of
covenant. Can one really believe that Barth faithfully reflects Calvin's thought
elsewhere if he so little appreciates and understands Calvin’s intense coramitment to
the doctrine of the covenant?

30. Beasley-Murray addresses this question in a few words, op. cif., p. 343. His
response attempts to use election as that which “cuts right across the distinctions of
circumcision and uncircumeision: not al! of Israel are Israel (Rom, 9:7, Gal. 4:30).”
