EDITOR'S INTRODUCTION vii

family. Thus, unlike the Baptists, Presbyterians baptize entire fami-
lies, including wives and children; showing that the entire family is
in some ways within the sphere of God's covenant life. On the other
hand, each member of the family stands before God as a true indivi-
dual both in this life and on the day of judgment, so that rebellious
wives and husbands can, in extremity, be divorced, and rebellious
children disinherited. Again, this stands over against some corpora-
tistic forms of Christianity which do not allow divorce. Consistent
Presbyterianism alone is able to produce a genuinely Trinitarian
view of the family.

Second, in many societies the state is given all power over the in-
dividual, while in some athers, the individual is given all power over
against the state. Totalitarianism and anarchism are the result.
Presbyterians have always been in the forefront of the political battle
against both of these extremes. The War for Independence was
characterized by the British as a “Scotch-Irish Rebellion,” that is, a
Presbyterian revolt, Presbyterians object to statism as an encroach-
ment on true liberty, but they also object to anarchy. The Puritans
and Presbyterians had little use for the anarchistic Anabaptisis and
Quakers of their day. As John Cotton put it, “If the people are gov-
ernors, who then shall be governed?” Think about it. That is a good
argument, in the light of the Biblical command to submit to the
powers that be. Presbyterians, unlike the Anabaptists, did not rebel
against the statist powers, but unlike the Romanists and Anglicans,
they did not acquiesce in them either. They submitted and also
worked for change. When forced to do so, they took up arms.
Consistent Presbyterianism alone is able to produce a genuinely
Trinitarian view of the State.

Third, in some circles the church is organized as a top-heavy
corporation, with a legislative bureaucracy at the top which directs
everything below. Indecd, a local church does not really exist unless
it belongs to the larger, corperate church, This is the Roman,
Anglican, and liberal Presbyterian and Lutheran way. At the other
extreme we have the Baptists. Among the Baptists, each church is a
separate corporation. No real connection among the churches is
allowed to exist, and certainly no heirarchy. Independency, in vary-
ing degrees, is the rule. Only among the Presbyterians do we find
the Trinitarian presupposition, the equal ultimacy of the one and
the many, at work. In Presbyterianism, each local church is a real
entity, but so is the connected church at large. The ascending courts
of Presbyterianism are just that: courts of appeal, not legislatures.
They only exist to deal with problems that cannot be handled at the
