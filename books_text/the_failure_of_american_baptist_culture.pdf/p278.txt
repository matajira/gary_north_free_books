264 CHRISTIANITY AND CIVILIZATION

confusion. He condemned their position—contrary to the dictates of
his “liberty of conscience’/“religious liberty” principle.

But, K. D., that isn’t all. ‘This “liberty” idea ends up being
challenged by more than just the religious “enthusiasts.” Williams's
principle was challenged from a different, more radical sector of
free-thinkers in Rhode Island. For this, I need to lay out just a little
Rhode Island background for you.

In 1655, Rhode Island’s right to self-government was confirmed
by Oliver Cromwell under a previously established charter drawn
up in 1644. Willams, as I understand it, wrote to a friend on this
occasion and recounted to him some of the freedoms that the colony
had enjoyed as it drank from the “sweet cup” of liberties. Rhode
Island, he said, had been spared the “iron yolk of wolfish bishops”
{speaking against England], and the “new chains of Presbyterian
tyrants... nor in this colony have we been consumed with the
over-zealous fire of the so-called godly Christian magistrate” [speak-
ing against the Puritan theocracy of Massachusetts]. He continued
in this letter, “Sir, we have not knawn what an excise means; we
have almost forgotten what tithes are, yea, or taxes either, to church
or commonwealth.” Williams, I gather, was glad he was “his own
man,” and not the least bit subject to anyone else either in church or
state,

But in this letter he also talked about some of the problems the
colony had faced. He had not forgotten some of the dissension which
was pervading Rhode Island. ‘lhe first Rhode Island settlements
were made at Providence by Roger Williams in June of 1636, and
at Portsmouth on the island of Aquidneck by the Antinomians
(those who didr’t believe that they were subject to.any law but them-
selves), William Coddingtom, John Clarke, and Anne Hutchinson
in March and April of 1638. They had left Massachusetts so that
they could all have their “liberty” and live together peacefully.

But Goddingtom and Clarke became dissatisfied with the condi-
tions at Portsmouth and left. They weren’t happy, They moved a
few miles farther south in April of 1639 and established a settlement
at Newport. Meanwhile, Providence was having its problems and in
1643, a fellow named Samuel Gorton and other “seceders” left that
city and founded Warwick, Rhode Island. Portsmouth and Newport
had formed a union in March of 1640 and all four settlements were
consolidated in 1647. But this didn’t last long. Some consciences felt
too bound by it. The individualism and “religious liberty” sen-
timents were so strong that no one could get along with anyone else.
In 1651 the union split into two confederations, one of the mainland
