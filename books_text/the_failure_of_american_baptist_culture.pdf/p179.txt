THE BAPTIST FAILURE 167

the need for a Christianity of the ‘Inner word,’ which would reveal
its reality in practical life . . . his one desire was to make way for the
free inward dominion of the Spirit, who alone opens men’s minds to
understand the Scriptures, and who witnesses to His presence
within the hearts of men by the fruits of tranquility, self-sacrifice,
and brotherly love.”# Even though many of the Baptists of his day
pressed for a “literalist” hermeneutic, the deficiencies of their own
approach to the Bible could not resist the “Inner Word” effect on the
Bible. Their subjective theology was brought to consistency. After
all, if man can stand in judgment of Scripture and determine that
four-fifths of it is no longer valid for faith and practice, he cannot be
prevented from other subjective liberties.

_ In Schleiermacher’s Discourses, for example, the inner authority
of the Spirit is connected with a universal religious feeling. “The
‘spirit’ is not tied to the historic Christian community, but, reaching
out beyond its berders, it can allow religious feeling, which is in
itself everywhere the same... . The prophets and seers, Christ
Himself included, are merely those who arouse and enkindle that
spark of direct religious life which is the possession of every human
being.” 36 Schlciermacher, under the influence of the Moravians,
who manifested many Anabaptistic tendencies,3? extended subjec-
tive theology. By universalizing subjective religious feelings he
equated the leading of the Spirit with feeling. It was an historic leap
and destructive to the faith. Man has many kinds of feelings which
are often difficult to distinguish. For example, how does one
distinguish an ecstatic religious feeling from a sexual one? Once the
leading of the Spirit and feeling are coalesced, that becomes a
difficult question to answer. In fact, the Moravians who instructed
Schleiermacher struggled to answer it. Promiscuity had sometimes
become part of their worship because they could not discriminate
among these subjective feelings. Therefore, the subjective standard
of the Anabaptists resulted in a nondistinguishable subjective

 

 

 

feeling-ethics, and contained in embryonic form the ethics which led
to the abuses of Schleicrmacher.

They would often go beyond the Bible in their assessment of the
sins of the day. Manz, for example, once began a long list of de-
nouncements in one of his sermons with what he called the sin of

35. Troeltsch, p. 764

36. Ibid. p. 793.

37. Idem.

38. Rousas J. Rushdaony, Revolt Against Maturity (Fairfax: Thoburn Press, 1977),
pp. 46-47,
