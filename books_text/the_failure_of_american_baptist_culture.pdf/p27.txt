INTELLECTUAL SCHIZOPHRENIA 1

The prohibitionist movement was the “last hurrah” in politics
for American fundamentalists. The backlash against prohibition,
coupled with the backlash of the intellectuals against the Scopes trial
in 1925, buried the fundamentalists for half a century. The chief
spokesman for Bible inerrancy in the period of modernism’s victory
(1923-36) was }. Gresham Machen [MAYchen], a Calvinist and a
Presbyterian who was personally opposed to prohibition. He was
not a fundamentalist.16 At his death in January of 1937, the fun-
damentalist world was left without an intellectually respected
spokesman. Furthermore, the Calvinists were left without a
politically concerned, outspoken upponent to the expansion of Fed-
eral power. Machen was a nineteenth-century liberal in his political
and economic views, His successors at Westminster Seminary were
either silent on political issues (the political conservatives on the
faculty, most notably Cornelius Van Til and John Murray) or not
adherents of Machen’s economic views (most notably Paul Wooley
and, later on, the seminary’s president, E. P, Clowney). It was only
with the publications written by R. J. Rushdoony, beginning in the
early 1960's, that any theologian began to make a serious, system-
atic, exegetical attempt to link the Bible to the principles of limited
civil government and free-market economics. It must also be under-
stood that Rushdoony was not able to get his historical and social
books reviewed in the Westminster Theological Journal throughout the
1960's and the 1970's (with the exception of Institutes of Biblical Law,
a review which was virtually forced into print by a faculty member,
Jobn Frame).1? He became a “nonperson,” despite the fact that che
Journal was filled with lengthy reviews of every liberal and obscure
European theologian imaginable. Only one word fairly describes
this book reviewing policy: blackout.

Thus, the fundamentalists have had no intellectual leadership
throughout the twentieth century. Only with the revival of interest
in creationism, which was made possible by Rushdoony’s support
and Presbyterian and Reformed’s initial investment for The Genesis
Flood, did the fundamentalist movement begin to get involved in
arguments outside theclogy narrowly defined. When a more sys-
tematic fusion of theology and conservative social and political con-
cerns finally became available—a revival of Machen’s outlook—

16, Ned B. Stonehouse, J. Gresham Machen: A Biographical Memoir (Philadelphia:
Westminster Theological Seminary, [1954] 1977), Machen’s book, Caristianity and
Liberalism (1993), was a kind of testament for aati-modernism. (Reprinted by
Eerdmans.)

17. Frame, Wistminster Theological Journal, Vol. 38 (Winter, 1976).
