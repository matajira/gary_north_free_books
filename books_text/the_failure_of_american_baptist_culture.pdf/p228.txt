214 CHRISTIANITY AND CIVILIZATION

“regenerate” members of a gathered church, but of those who have
some claim to the promise of the covenant. In fact, Calvin includes
himself among those who have been severed from the covenant by
ingratitude, and who have been restored by divine grace. Further,
Calvin also is struck by the warnings of apostasy and prays that he
be kept from falling away again as a hypocrite.

If modern Baptists object to this approach by claiming that the
baptistic approach is much simpler and more likely to maintain the
purity of the church by its insistence on regenerate church member.
ship, it is important to realize that the problems that Calvin has here
struggled with are applicable to them as well. Is it not true thar
many “regenerate” people have walked an aisle or sought baptism
and have received the ordinance of baptism as adult believers only
to fall away from their profession? It is this fact of experience itself
that indicates the impossibility of inerrantly practicing the
regenerate church concept. If anyone has ever been baptized and
then later shown himself to be a genuine hypocrite who has finally
apostatized from the truth, the reality of a regenerate church
membership is disproved. While Calvin’s approach may not appear
ideal to the baptistic viewpoint, it nevertheless is the only approach
that can handle the state of the church as it really exists in this world.

Perhaps no better illustration from Calvin’s writings of this in-
terplay between law, letier-spirit, and the genuine and hypocritical
peoples of the covenant or church can be found than his comment
on Genesis 21:12. There Calvin speaks of the “perpetual condition
of the church”. Calvin says that the church or the spiritual kingdom
of Christ is born of the /aw. From the law, two types of children are
born — those born of the letter and those born of the Spirit. The first
are illustrated by Hagar who is the letter giving birth to Ishmael
who is an adulterous son. Overagainst these two are Sarah who il-
lustrates the Spirit, and [saac who is the true son. Calvin proceeds to
say that the church has children of the Jetter or adulterous sons who
are born into slavery to the law and are so hypocrites.*In his day,
these children of the letter, or adulterous sons in slavery to the law
and hypocrisy are the members of the papal church. One can now
understand why he called them “bastards” in his comment on Hosea
2:4, 5 cited above. On the other hand, Calvin sees the true sons of
the Spirit as those who are born into liberty as the sons of God.
These, of course, are the Protestants, although Calvin does not say
so in this passage.” The first group, Calvin says, are “apparently

 

25. Galvin admits that there are hypocrites in the Reformation as well in Zeph.
1:2, 3, Micah 3:11, 12 will be cited below saying the same.
