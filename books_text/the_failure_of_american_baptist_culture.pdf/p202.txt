188 CHRISTIANITY AND GIVILIZATION

go beyond the temporal life, and the promises given them would
rest in present and physical benefits. If this doctrine should ob-
tain, what would remain save that the Jewish nation was satiated
for a time with God’s benefits (as men fatten a herd of swine in a
sty), only to perish in eternal destruction? (IV. 16. 10)5

While Calvin’s invective strikes the modern reader as extreme, it
nonetheless indicates Calvin’s deep feelings on the issue. But more
importantly, it must be noticed that Calvin’s concern is not simply
for the sacrament of infant baptism, but for what he felt to be the in-
herent and inevitable danger to all of Scriptural doctrine if the
Anabaptist argument was to be accepted. If infant baptism is to be
overturned, then the continuity of the Old Covenant with the New
Covenant must be denied. But to do this, Calvin argues, is to make
the Old Testament saints nothing more than recipients of material
blessings from God at the expense of their salvation,’ In light of this,

5. All citations of Calvin's Zustituies are from the translation of Ford Lewis Battles
in The Library of Christian Classics series. All citations of Calvin’s commentaries are
from the Calvin Translation Society as reprinted by Baker Book House, 1979. These
works will be referred to simply by Scripture reference.

6. This conception of the discontinuity of the Old and New Covenants is repeat-
edly seen not only in the carly Anabaptists, but also in the modern Anti-pacdobaptist
writings. For the Anabaptists writings, of. George H. Williams, The Radical Reforma-
tion (Philadelphia: Westminster Press, 1975), pp. 828-32; Leonard Verduin, The
Reformers and Their Stepchildren (Grand Rapids: Baker, 1980), p. 209#.; Jan J. Kiwiet,
Pilgram Marbeck (Kassel: J. G. Oncken Verlag, 1957), pp. 101, 102, August Baur,
Zusinglis Theologie (Halle: Max Niemeyer, 1889), I1:228-29. For modern Anti-
paedobaptist writings, o. Beasley-Murray, op. cit., pp. 337, 338; Estep) op. cif., pp.
86, 87; Gill, op. cit., p. 903; Jewett, of. cil, pp. 93, 96. Gill yues yo far as to deny that
the Abrahamic covenant was even a covenant of grace! He writes, “Now that this
covenant was not the pure covenant of grace, in distinction from the covenant of
works, but rather a covenant of works, will soon be proved; and if so, then the main
ground of infant’s baptism is taken away... .” This reminds one of the efforts of
dispensationalists to discover #0 new covenants in the New Testament so that their
Church/Israel distinction can continue. Gf John F. Walvoord, The Millennial
Kingdom (Grand Rapids: Zondervan, 1976), pp. 210, 218. Gill goes on to argue that
the covenant of grace referred to in Galatians 3 refers to God's covenant with
Abraham in Genesis 12, but not to this “covenant of works” in Genesis 17. Thus in
both the Baptist and Dispensationalist approaches, the clear teaching of the con-
tinuity of the Old and New Covenants, has compelled some of their writers to search
out a “second” covenant so that their structures might conform better to the
evidence.

7. Calvin has in view bere Servetus and sume of the Anabaptists: “Indeed, that
wonderful rascal Servetus and certain madmen of the Anabaptist sect, who regard
the Israelites as nothing but a herd of swine, make necessary what would in any case
have been very profitable for us” (11. 10. 1). Not all Anabaptists would have argued
as perversely as Servetus. Nevertheless, as was seen in the citation from IV. 16. 10,

 
