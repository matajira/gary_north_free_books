THE BAPTIST FAILURE 175

Triune God. God the Father gave His only begotten Son to be the
Church’s representative in salvation. Thus, analogically, men should
elect representatives to rule over them (Acts 6:1ff.). Representative
rule has been expressed only where Christianity has been estab-
lished. ts essential government around the Trinitarian model is the
explanation. Furthermore, the Trinity rules with plurality. The
“plurality principle” has thus been part of Christian civilization as
well. Anabaptist sociology, except where influenced by Calvinistic
and Reformed thought, conspicuously lacks this Trinitarian feature.
How can it express a plurality principle of leadership when every-
thing is defined around the individual who is separated from the
world?

~ Third, the Bible criticizes the separational aspect of the sociology
of Anabaptists. Again the refutation is found in the Trinity. God the
Holy Spirit proceeds from the Father and the Son (John 15:26; 16:7).
The Spirit regenerates (John 1:13-14) and sanctifies (I Thessalo-
nians 5:23) the church as part of that procession. Thus the church is
processional, expansive. Separational theology leads to a recessionad
church. The positive leaven of the New Covenant is thereby
mitigated. The Seriptures do not teach separation from the state.
They say that the nations, which involves the state, are to be made
disciples (Matthew 28:18-20). Thus, the Anabaptists have failed
because they seek to be holier than God by operating contrary to the
Trinitarian model. God operates in plurality, but they have con-
structed a society around singularity, individuality. Because this er-
rer concerns the governmental structure of Anabaptist sociology it
can been seen in the other spheres of society.

The Church

The “social contract” government of the Anabaptisis is the first
problem, with their ecclesiology.°® Like Rousseau in the political
realm, they maintain that the church is created by the “gathering” of
the people. As they come together, a mutual agreement is made to
form a “voice of the people—voice of Gad"? type of church/society.
Consequently, it is ruled by the people or congregation. In Baptist
churches the people vote on everything, and when discipline is ta be
carried out, they callectively decide. Contrary to the congregational

 

58. James
Trust, 1974)
59. Idem.

Bannerman, The Church of Christ (Edinburgh; The Banner of Truth
423

 
