CHRISTIANITY AND RELIGIOUS LIBERTY 251

learn much by listening to those least likely to give a truthful report.
The strange thing, K. D., is not that truth can be found in the least
likely places— we've all seen God confront us with truth out of a
source we thought unlikely to teach us anything. But the odd thing
is that a man with a fallible conscience can distinguish between the
truth and falsehood. He criticized the Puritans for their dogmatism
over non-negotiable religious truth — how could they or anyone else
for that matter, say with absolute certainty that one thing was in-
fallibly true and therefore non-negotiable in any religious, social, or
political setting? Yet he turned aroynd and said to the same Puritans
he criticized:

Ladd, it is a glorious character of every true disciple or scholar of
Christ Jesus, to be never too old to learn. It is the command of
Christ Jesus to his scholars, to try all things: and liberty of trying
what a friend, yea what an (esteemed) enemie presents, hath ever
(in point of Christianity) proved one especial means of attaining
to the truth of Christ. For I dare confidently appeal to the cons-
ciences of God’s most knowing servants, if that observation be not
true, to wit, that it hath been the common way of the Father of
Lights, to inclose the light of his holy truths, in dark and obscure,
yea and ordinarily in forbidden books, persons and meetings by
Satan stiled conventicles.

That also is in his Bloody Tenent Yei More Bloody. On the one hand,
grace is so far removed from man as to leave him with a confused
and uncertain conscience. This prevents him from being a judge of
absolutes. Yet, on the other hand, grace is so connected with nature
that it can be found in the most unlikely places and we have the
ability to find it! At one and the same time, we are imcapadle of
establishing Biblical righteousness based on the revealed, objective,
intelligible Word of God, and yet so capable of discerning righteous-
ness that we can find it, and must defend it, even from the darkest
sources, On the one hand we are prevented; on the other we are re-
quired. Conscience cannot tell us what is absolutely true according
to the Bible — that violation of Biblical law is punishable by the civil
authority —but it must tell us that we can find undisputable truth in
strange places, and know it when we see it.

Because of our consciences, Williams seems to say, we cannot
know for certain that the laws of the Bible are absolutely true and
relevant. We may be mistaken in our judgments at some point and
persecute Christ. Yet, because of our consciences, we can know for
sure that the light of truth is found in dark places. At one point we
can count on our consciences, and at another, we cannot. Tell me,
