SOGIAL APOLOGETICS 61

This is such a tragic staternent. It is all the more tragic when one
realizes that the.great majority of the evangelicals and fundamentalists in-
volved in social action accept the idea. It is the practical denial of
Hebrews 4:12:

The Word of God is quick and powerful, and sharper than any
two-edged sword, piercing even to the dividing asunder of soul
and spirit, and of the joints and marrow, and is a discerner of the
thoughis and intents of the heart.

The Bible tells us to bring the Bible into our conversation and our
social action.

For as the rain cometh down, and the snow from heaven, and
returneth not thither, but watereth the earth, and maketh it bring
forth and bud, that it may give seed to the sower, and bread to the
eater, so shall my Word be that goeth forth out of my mouth: it
shall not return unto me void, but it shall accomplish that which I
please, and it shall prosper in the thing whereunto I sent it (Isa.
55:10-11).

The Bible is powerful and effective precisely because it is the voice of
the Father which the natural man knows down to the joints and
marrow, but refuses to know. He hears this voice, but refuses to
hear. How shall we make him hear? Do we use the words which
man’s wisdom teaches (I Cor. 2:13), depending on our own rhetori-
cal gifts for persuasion? Or do we use the methods of the Spirit,
depending rather on the grace of God for success? Men must submit
to the Lord and His Word if our culture is to be saved.

How then shall they call on Him in Whom they have not believed?
and how shall they believe in Him of Whom they have not heard?
and how shall they hear without a preacher? So then faith cometh
by hearing, and hearing by the Word of God (Romans 10:14, 17).

Answers to political problems are found in the Bible. We must con-
front legislators with the Word of God.

“T will speak of thy testimonies also before kings, and will not be
ashamed” (Psalm 119:46). It is the Word of God that tells men who
they are and what they must do to save themselves and their society,
Satan has deluded Christians into thinking that man is not “dead in
trespasses and sins” (Eph. 2), but alive and religiously neutral.5° He
has then convinced us that we must cater to the unbeliever's scholar-
ship and scientific outlook by not bringing up the Word of God. The

50. The gospel of redemption comes into enemy tervitory, See Van Tit, A Christian
Theory of Knowledge (Grand Rapids, Mich: Baker Book House, 1969), pp. 26-27.
