BAPTISM, REDEMPTIVE HISTORY, AND ESCHATOLOGY 133

The following representative statements by baptisis reveal the
biblical-theological constructs underlying their thealogy.

Walter J. Chantry:

But the New Testament Church is come of age. It is by way of
contrast, inward, spiritual, and personal.5”

Old Testament Israelites had expectations which were territorial
and special. Conquering of the promised land to expel oppressive
Gentiles was a firm hope. Though Jesus twice drove money-
changers from the temple, he did not really cleanse that building
of its corruptions. . . . By way of contrast Christ's kingdom is
inward. It comes mightily but secretly in the hearts of men.*#

Some recent post-millennialists have fallen into future expecta-
tions similar to those of dispensational pre-millennialists. . . .
This kingdom is identified in their thinking with external, visible,
temporal blessings. It is even supposed by some to be identified
with world-wide governmental allegiance to Mosaic laws... .
Such anticipations cannot survive the scrutiny of Jesus’ kingdom
teaching. A gradual, spiritual, inward advancement of the
kingdom continues until the end of the world.5¢

David Kingdon:

According to Jer. 32:33, Gad will write his law on the hearts of his
people. The emphasis has shifted from the external ceremonies
and institutions of the Old Covenant to the possession of inward
spiritual life.6°

[Paedobaptists] are dwelling in the sphere of the theocracy of
Israel rather than in the realm of the recleemed community, the
church.6!

Paul K. Jewett:

With the advent of the Messiah—the promised seed par excellence

 

A growing number of theologians opine that eschatology is probably the most
important area of theological debate today. On its resolution hang a number of
critical questions, baptism being one. The paedobaptist-baptist debate will not be
settled by exegesis and counter-exegesis of lexis imunedialely connected to the issue.
1t will be settled in the courts of eschatalogy and the redemptive-historical context in
which these texts are understood.

57. “Baptism and Covenant Theology,” Baptist Reformation Reaiew, st.

58. Gads Righteous Kingdam: The Law's Connection with the Gospel (Edinburgh: The
Banner of Truth, 1980), p. 51.

59. Zbid., p. 59.

60. Children of Abraham (Haywards Heath: Carey Publications, 1978), p. 34.

61. foid., p. 39.

 
