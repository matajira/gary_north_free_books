42 CHRISTIANITY AND CIVILIZATION

men as a challenge to their thinking and living, then he must be
offered without compromise. Nothing short of the Christ of the
Scriptures, as presented in historic Reformed theology, can
challenge men to forsake their sin and establish them in truth and
life"? The “Religious Right” has consigned itself to long-run defeat
by hiding its Biblical distinctives.

Il. Evaluating Christian Social Action

Proposition I; Only “Postmillennial, Theonomic Reconstruc-
tion” Can Provide the Proper Goals, Motivation, and Standard
for Biblical Social Action.

This proposition is easily explained. What is it that those engaged
in Christian social action should seek to accomplish? How should
they accomplish this? What should motivate them? For some, the
perspective in which these questions are answered is exceedingly
short-run. Ever since its invention in 1830, a curious doctrine known
as “the Rapture” has left most dispensationalists paralized and
unable to think or plan beyond the next fiscal year. For such men,
political involvement is concerned mainly with a few isolated laws:
abortion, homosexuality, the Equal Rights Amendment, This paper
reflects a more Biblical approach: “Postmillennial, Theonomic
Reconstruction,” as it is becoming known. As Postmillennialists, we
hold an “Eschatology of Victory.” The Church is promised success
in her efforts to make the nations Christian (Matt. 28:18-20).4 As
“Theonotnists,” we hold that not one jot or tittle of Old Testament
Law will pass away until the “Eschatology of Victory” becomes a re-
ality. And as “Reconstructionists,” we hold that it will not become

3. Hid, pp. 3, 5.

4, Post-millennialism is the doctrine that Christ returns after the world-wide pros-
perity of the gospel, Christ does not rcturn to set up His Kingdom; He comes to
judge and melt away the heavens and the earth, Por an excellent, albeit short,
defense of postmillennialism, sce David H, Chilton, Productive Christians in an Age of
Guill Manipulators, published by the Institute for Christian Economics, P.O, Box
8000, Tyler, TX 75711, chapter 17. Also see Part Three of Gary North’s Unconai-
tional Surrender: Ghd Program for Viclorg (Geneva Press, 708 Hamwvassy, Tyler, TX
75701).

5. “Theonomy,” from the Greek, Theos, “God,” and Nemes, “law,” means simply
“God's Law.” Greg Bahnsen's book, Theonomy in Christian Ethics, defends the thesis
that the Old Testament Law, axcept where specifically set aside in the New Testa-
ment, is still binding on the Christian as a rule of life, and the “judicial law” binding
upon all eivil magistrates. The argument that this would constiture a “thencracy’
of course, rue. A “thencracy” is literally the rule of God in society. The

 

 
