CALVIN’S GOVENANTAL RESPONSE 245

born of the Word of God, and thercfore in a sense, the sons of God.”
The latter group, however, are “born of the incorruptible seed of the
Word” and hence are true sons. For Calvin, then, “law” can result
in slavery and hypocrisy or it can result in liberty and true sonship.
What makes the difference? The answer is found in a proper under-
standing of the letter-spirit distinction. To absolutize the distinction
results in an Anabaptist conception of the church. Yet this view
leaves the Old Covenant saints as without the Spirit’s blessing. Nor
can it explain why there are covenant-breakers in the New Cove-
nant era, if the difference is taken as absolute. On the other hand,
Calvin’s interpretation of a comparison from lesser to greater ex-
plains the Old Covenant saints’ experience of salvation, how David
can delight in the law and Paul can be terrified by it, and how there
can be covenant-breaking even in the New Covenant. The result is
a Reformed conception of the church that recognizes the im-
possibility of having a totally “regenerate” church membership.
Calvin’s view recognizes that the unity of the covenants in all the
ages demands that the church also be arranged along the lines of the
covenant. While all of this may seem complex, it can be simplified if
it is studied in graphic form. This chart atternpts to incorporate the
main points considered so far.

Calvin’s View of the Relationship of the Church
and the Covenant Throughout History

Ghrist
Creation/Fall or Redetnption NT. Glorification

Promise—lsrael |” Fulfillment — World
Letter/Iesser— Comparison — Spirit/Greater

Always The Same In Substance NEW COVENANT. Dirt tn Administration y
i | COVENANT”
Exit: SRRAKING

1. Ingratitude

 

GENERAL ELECTION

Entrance
1. Baptism:

(Circumcision)
2. lection ——pi——} -—__foVENANT KEEN spDisobedience
, : at SPIRIT 4 > > Repentance

Ni
G -
3. Faith Os, Hypocrisy

THE CHURCH

  
 
 

   

 

 
 
   

W
Al
RI

 

  

 

 

 

 

It would perhaps be helpful to provide a few more specific ex-
amples from Calvin's writings to illustrate how he viewed this matter
