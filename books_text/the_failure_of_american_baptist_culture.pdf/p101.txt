THE MORAL MAJORITY: AN ANABAPTIST CRITIQUE 85

Civil Religion

Orthodox catholic Christianity has never questioned whether or
not the state should be Christian. All areas of life, says the orthodox,
are claimed by Christ, and must answer to Him. There is a kind of
Christianization appropriate to the state, and another kind ap-
propriate to the individual. A Christian state is one which conforms
to revealed Divine law. A Christian church is one which properly
administers the preached Word and sacraments. A Christian person
is one who is converted in his or her heart. These are different ways
in which the rule of Christ is made manifest in the various spheres of
life.

For reasons which we shall! investigate in the next section of this
review, Mr. Webber rejects the notion of a Christianized state.
Biblical Christianity is, in his view, limited to the institutional
church and to the souls of believers. Thus, when Mr. Webber asserts
that “the relationship between religion and the American govern-
ment from the very beginning has been one of civil religion, not
biblical Christianity” (p. 38), he is engaged in a tautology, for in his
view biblical Christianity can have no relation to the state by defini-
tion. A similar example of tautologous reasoning is on p. 19, “Ina
fallen world there can be no such thing as a ‘Christian nation’
America is not now, nor has it ever been a Christian nation.” From
the standpoint of orthodox Christianity, it is entirely possible that
America may once have been a relatively Christian nation; but from
Mr. Webber’s gnostic-anabaptistic perspective, there is no such pos-
sibility.

Thus, for Mr. Webber, “civil religion” is always bad. At the
same time, Mr. Webber issues some good criticisms of the brand of
civil religion advocated by the Moral Majority. “The idea that alt
people of high principle should unite to recover America’s moral
heritage is a moralism...” (p. 105). Precisely; and a neutral
moralism is not Christianity. “True moral reform comes as a result
of faith in Jesus Christ, not from the desire to create ‘national
solidarity and stability’. . . . The most powerful weapon the evan-
gelical church has against the breakdown of morals in our culture is
not the restoration of a civil religion, It is rather the preaching of
Jesus Christ as Lord, the invitation for people to join with Christ’s
church, and a renewed understanding of the church as the universal
society of God’s people called to live in obedience to Jesus Christ's
teachings” (p. 106). As far as they go, these statements are a
salutary warning against a merely moralistic civil religion.

 
