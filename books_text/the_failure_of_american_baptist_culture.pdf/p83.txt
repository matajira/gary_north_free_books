SOQIAL APOLOGETICS 67

avoid submitting to the Lord of hosts. Can we blithely hand history
over to the humanist and Jet him “judge for himself’? That is
already his problem!

Public Consensus

Next, Kelly says, “we have public consensus on our side.”5? Do
we? Kelly himself cites an amazing example of how indicators of
public opinion can be. manipulated. Even if an honest poll revealed
that a majority of Americans said that they were opposed to abor-
tion, even to the point of agreeing with the Bible that it is a capital
crime, I should still be skeptical. I should bet that if they were the
typical juror, trying such a case, the defense attorney would ask,
“What if she were your daughter?” and the jury would unhesitatingly
vote to acquit. Mast “conservative” Americans talk big, but when
they have to push the button, they really aren’t opposed to abortion
after all.6* If, to prevent an abortion, Mr, and Mrs. Joe Christian
would have to get involved personally and help a young girl through
her pregnancy and then perhaps adopt the child themselves, we
would find them saying they were “opposed to abortion, sure,” but
they “aren’t fanatic about it!” Legislation without works is struck
down (James .2:20).

Could Elijah appeal to “public consensus” (I Kings 19:10)? Should
we?

Setence

Aller all the hassle we've had with the evolutionists, it seems a bit
odd to have appeal to “science.” What Kelly means is that the
“public consensus of scientists” is on our side. This is, of course, a
far more reliable source of argument than the public consensus of
historians, Historians, as we have seen, tend to make the source of ul-
timate truth their own evaluation of observable facts. 69 Scientists, as
we all know, are “objective.” And because of their dispassionate
searching for truth, the consensus of scientific opinion never
changes. The Supreme Court, in Roe v. Wade, recognized medical
and scientific authority. Acknowledging that medical technology has

67. Ibid., p. 65, Cf p. 66.

68. Their principle of action is a selfish, humanistic one, not a theistic one, and is
subject to immediate change. This is why evangelism must be a part of our social
action.

69. Jéid., p, 28. Even secular scientists such as Kuhn and Polanyi are admitting
thar science is not strictly without pre-theoretical presuppositions,
