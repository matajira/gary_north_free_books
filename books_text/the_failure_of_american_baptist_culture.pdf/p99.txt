THE MORAL MAJORITY: AN ANABAPTIST CRITIQUE 83

more than a little tension from time to time between the two. More-
over, the idea that there was no freedom of dissent in Puritan society
is sorely mistaken. To be sure, Quaker women were not permitted
to parade nude in the street, and Roger Williams was made to leave
when he persisted in disrupting society (and not because he was a
Baptist, by the way), but there was a good deal of diversity in New
England, and those holding private opinions contrary to the
religious establishment were not molested unless they stirred up
trouble.

Mr. Webber is opposed to the notion that America was ever a
Christian nation. The reason for his opposition to that notion is
taken up in the next section of this review, but one effect of this
prejudice is thar he insists that the founders of America did not
intend to set up a Christian nation. There are two problems with
this. First, properly speaking the founders of America should not be
dated in 1776 but in 1620-1680. The fact that America separated
from Britain in 1776 does not mean that some wholly new culture
popped into existence at that point. As a civilization, America was
already 150 years old, and was definitely Christian in consensus.

Second, although some of the most brilliant men at the time of
the War for Independence were Deists, such as Franklin and
Jefferson, the large majority of the men involved in the production
of the Constitution were active, practising Christians.* A large
number were Presbyterian elders, for instance. At this point, Mr.
Webber has fallen for the mythology promoted by Public Television,
The facts are otherwise. The Christianity of this period may have
been more superficial than is desirable, but the fact remains that
America in its earlier centuries was Christian in intent, and largely
so in content.

Lastly, but sadly not least, Mr. Webber's discussion of the plight
of black people in America is highly stereotyped. “It was zot
uncommon to sell a husband to one master and a wife to another.
Children were jrequently severed from their parents and sent to other
masters where they were deprived knowledge of or contact with
their parents” (p. 72, italics mine). This simply is not the case,
despite the romantic portrayal of black suffering in such
dreamscapes as Roois and other modern fiction. As Fogel and
Engerman have argued, farnilies were very seldom broken up under

4, OF, M.B. Bradford, A Worthy Company: Brigf Lives of the Framers of the U.S. Con-
stitution (Box 425, Marlborough, NH: Plymouth Rock Foandation, 1982).
