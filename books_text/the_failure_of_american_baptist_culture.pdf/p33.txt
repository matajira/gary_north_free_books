INTELLECTUAL SCHIZOPHRENIA 17

There is no “brute factuality.” Every fact is an interpreted fact. It is
interpreted by God, the Creator. It is reinterpreted by men, either
as faithful adherents to God and God's revelation (interpretation) of
Himself and the creation, or else as self-proclaimed autonomous in-
terpreters. If this is true in science, then it is equally true in social
and political aflairs. Because fundamentalists after 1925 tended to
grant to natural scientists and social scientists the right to interpret
“brute factuality” by means of “objective investigations” —during
the very period when the humanists were beginning to lose faith in
the possibility of objective investigations—they abandoned the
realms of politics and scholarship. They protected their ecclesiastical
realm as best they could, allocating few if any resources for a sys-
tematic reconquest of the world outside the church. The myth of neu-
trality was the histortcal handmaiden of the pletist retreat from the external
social world.

The fundamentalists ultimately failed in their attempt to
preserve a degree of autonomy for their tightly knit theological
world. They were invaded, year by year, by modernisi theologians
and political liberals, in their churches and in their tiny liberal arts
colleges. They required their colleges faculty members to eam
Ph.D.s at humanist universities, and then found, to their surprise,
that these Ph.D.-holding teachers wound up teaching the discarded
humanist fads they had learned about in graduate school. Van Til
warned Christians in 1932, but nobody paid much attention: they
hed denied the cosmic significance of Christ. He wrote:

We realize this if we call to mind again that if once it is seen
that the conception of God is necessary for the intelligible inter-
pretation of any fact, it will be seen that this is necessary for all
facts and for all laws of thought. If one really saw that it is neces-
sary to have God in order to understand the grass that grows out-
side his window, he would certainly come ta a saving knowledge
of Christ, and to the knowledge of the absolute authority of the
Bible, It is true, we grant that it is not usually in this way that men
become true Christian theists, but we put it in this way in order to
bring out clearly that the investigation of any fact whatsoever will
involve a discussion of the meaning of Christianity as well as of
theism, and a sound position taken on the one involves a sound
position on the other. It is well to emphasize this fact because
there are Fundamentalists who tend ta throw overboard all
epistemological and metaphysical investigation and say that they
will limit their activities to preaching Christ. But we see that they
are not really preaching Christ unless they are preaching him for
what he wants to be, namely, the Christ of cosmic significance.
Nor can they even long retain the soteriological significance of
