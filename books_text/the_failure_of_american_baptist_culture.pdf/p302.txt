288 CHRISTIANITY AND CIVILIZATION

evoke lively debate” (p. 10). Indeed, anyone interested in Anabaptist/
Baptist theology will find Davis's conclusions thought-provoking. As
Wegner noted, the point of interest stems from the source of Davis's thesis:
Albrecht Ritchl, a nineteenth century German theologian who linked
Anabaptistic movements of the sixteenth century to the Franciscan monastic
movement of the wwelfth. Ritschl perceived Anabaptism as flowing out of
the Roman Catholic Church, This goes contrary to the view, popular in
many Baptist circles, which sees Baptist religion as the true expression of
Christianity, existing in oppression in small sects and groups down through
the centuries, until the Reformation. Moreover, the Ritschl thesis holds that
Anabaptism, in its early phases at least, was Medieval in theology, and not
Protestant,

The subtitle of the book, A Study in Intellectual Origins, makes it clear that
Davis intends to examine the Anabaptist movement from an ideological
point of view, and this is the most important feature of the book. Davis
states, in his introduction, “Any historian investigating the transfer of ideas
from medieval predecessors to Anabaptism faces a serious problem with the
sources. Due to their persccution, even the Anabaptist leaders had little
time to write in depth; sermons were rarely written at all, and even their
brief polemical pamphlets were published only with the greatest of
difficulty... . They tended in their writings to avoid references to either
influential historical antecedents or to contemporary influences. . . . But
one must not assume that there were none; rather, the task of discerning re-
quires a droader approach. For example, one must ask not only what books
their leaders read and with whom they associated in formative years, but
also what patterns or presuppositions seem to affect their interpretations of
Scripture and what mental, emotional, and spiritual preconditioning is ex-
pressed in their theological affirmations, religious life-style, and condemna-
tions of their opponents” (p. 33, emphasis added).

Davis's analysis leads him to connect Anabaptism with the ascetic move-
ments within the Medieval Roman Church. In the second chapter, he
develops what he calls the “Christian ascetic tradition” from the early desert
monks to the Devote Moderna movernent in the fifteenth century. Asceticism
is seen as the separation of oneself from the world and anything associated
with it, by a process of self-denial. In the third chapter, Davis demonstrates
the influence of the ascetic heritage on the early Anabaptists, looking at
several aspects of the Anabaptist movement: their leadership, ex-monks
who became Anabaptists, Anabaptist preaching, and the polemics between
the Anabaptists and the magisterial Reformers, Calvin, for insvance, re-
ferred to the Anabaptists as monks (p. 127). These two chapters make the
relevant historical connections.

The fourth chapter concentrates on theological connections, and here
