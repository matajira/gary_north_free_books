70 CHRISTIANITY AND CIVILIZATION

significance is already agreed upon by both parties to the debate.
The question is rather... what is the final reference point re-
quired to make the “facts” and “laws” intelligible. The question is
as to what the “facts” and the “laws” really are. Are they what the
non-Christian . . . assumes they are? Are they what the Chris-
tian . . , presupposes they are? The answer to this question can-
not be finally settled by any direct discussion of “facts.” It must, in
the last analysis, be settled indirectly.7*+

“Practical Compromise”

The preaching of the whole counsel of God will, undoubtediy,
offend many. It is for this reason that the “Religious Right” would
like to tone down their message. Will the humanists listen to us
when we tell them they must believe and obey the Bible? Kelly says,
“Approaching the world at large with a Bible in hand is an exercise
in futility.” To hide the Bible frorm our opponent is to communicate
our message in a more “practical manner.”75 The Reformed
defender of the faith has a different answer:

As for the question whether the natural man will accept the truth
of such an argument, we answer that he will if God pleases by His
Spirit to take the scales from his eyes and the mask from his face.
Tt is upon the power of the Holy Spirit that the Reformed
preacher relies when he tells men that they are lost in sin and in
need of a Savior. The Reformed preacher does not tone down his
message in order that it may Gnd acceptance with the natural
man. He does not say that his message is less certainly true
because of its non-acceptance by the natural man. The natural
man is, by virtue of his creation in the image of God, always ac-
cessible to the truth; accessible to the penetration of the truth by
the Spirit of God. Apologetics, like systernatics, is valuable to the
precise extent that it presses the truth upon the attention of the
natural man. The natural man must be blasted out of his
hideauts, his caves, his last lurking places. Neither Roman Catho-
He nor Arminian methodologies have the flame-throwers with
which to reach him, {n the all-out war between the Christian and
the natural man as he appears in modern garb it is only the atomic
energy of a truly Reformed methodology that will explode the last
fortress to which the Roman Catholic and the Arminian always
permits him to retreat and dwell in safety. (The use of such mar-
tial terminology is not inconsistent with the Christian principle of
love. He who loves men most will tell them the truth about them-
selves in their own interest. )76

74, Van Til, p. 100.
75. Kelly, p. 66.
76. Van Til, pp. 104-105,
