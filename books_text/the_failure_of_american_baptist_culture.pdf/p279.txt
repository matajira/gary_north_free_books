GHRISTIANITY AND RELIGIOUS LIBERTY 265

towns and the other of the island towns. Finally, in 1654 Roger
Williams was able to bring back some unification to the colony, but
only after some confrontation with his own notion of “liberty.”

Getting back to this letter Williams wrote, in it he said that
Rhode Island had “long drunk of the cup of as great liberties as any
people that we hear of under the whole heaven.” But he also said
that the “sweet cup hath rendered many of us wanton and too ac-
tive.” He was, no doubt, becoming a slave to his own idea of “lib-
erty.” One example of this can be seen in his attempt to secure some
protection for the colony.

Aware of dangers to the colony’s well-being and the possibility of
its complete demise, Williams instituted compulsory military ser-
vice. Now I’m not advocating conscription and the draft system,
K. D.; I'm bringing this up only to show that Williams’s conscience
and reason moved him in a particular direction. Now others in the
colony were not interested in going in that direction, You will be in-
terested in reading what one man has said about the opposition he
received: “The leaders of this libertarian opposition were the Bap-
tists, who denounced the bearing of arms as un-Christian and con-
scription as an invasion of religious liberty and of the natural rights
of the individual. This opposition was itself radicalized by the crisis
precipitated by Williams, and the logic of the pacifist opposition to
conscription and arms-bearing led them straight to the ne plus ultra of
libertarianism: individualist anarchism. The opposition—led by
Rev. Thomas Olney, former Baptist minister at Providence,
William Marris, John Field, John Throckmorton, and Williams
own brother Robert—circulated a petition charging that it was
‘blood-guiltiness, and against the rule of the gospel, to execute judg-
ment upon transgressors, against the private or public weal.’ In
short, government itself was anti-Christian.” The principle upon
which the colony was founded was now bringing about its destruc-
tion. The conscience of Williams was telling him one thing; the con-
sciences of the other Baptists were telling them something different.
The Lord was leading in opposite directions and any convergence
was impossible. They ended up with “individualist anarchism.”
Each claimed that his position was “Christian,” but on differing
bases. The one group thought that what Williams was doing was an
invasion of religious liberty and of the natural rights of the in-
dividual. Williams had a slightly different response. The same man
T quoted just a minute ago explained his defense as follows: “He
likened human society to a ship on which all people were passengers,
All may worship as they pleased, be graciously declaimed, but none
