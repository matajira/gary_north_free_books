170 GHRISTIANITY AND CIVILIZATION

workings have been disastrous. In each sphere — state, church, and
family ~the impracticality of Anabaptist theology arises. Therefore,
attention must be turned to the social side of Baptist theology.

The State

“After the fall of Robespierre . . . those who sought to keep
alive the high hopes of the early revolutionary era no longer focused
their faith on the ongoing process on innovation in society as a
whole, but instead reiveated to the secure nucleus of a secret society
where intense conviction need not be compromised by the diffuse
demands of practical politics.”** After the Reformation, the Ana-
baptists acted similarly, for the same characteristics of the
underground revolutionary era are found in them. They too, pulled
away from society, and their underlying presuppositions became
more manifest.

The firs. premise of Anabaptist theology regarding the staie, is
that ChrisLianity can not Christianize it. “The idea of a radical social
reform, which regards the existing order of Society and property as
radically incapable of developing Christian personality and Christian
love in any comprehensive way, was held only by the sects, and by
them only in the measure in.which they passed from patient
endurance of persecution, under the influence of the eschatological
idea of the Kingdom of God and in the expectation of its speedy
realization, into the attitude of a thoroughgoing reform according to
the ideal of the Kingdom of God and of primitive Reason. Further,
the more the idea of the Natural Law of the Stoics agreed with these
hopes, the more this reform became democratic and cornmunistic.
These were the sole supporters of a Christian social ethic which was
radical, allowed no compromise, and did not accept the existing
social order.”#5

In a telling way Troeltsch captures the features of the fatlure pre-
supposition of the Anabaptists. With inward theology, the hope of
the conquest of God over the State is lost. The loss of that hope sur-
faced in the forms of passivity and radicalism. Interestingly —as will
be developed later—many of the Anabaptists reacted out of that
hopelessness, and precipitously tried to bring their ideals into reality
by anarchy. But most avoided the world.

44, James H. Billington, Fire in the Minds of Men (New York: Basic Books, Inc.,
Publishers, 1980), p. 86. Italics mine.
45, Troelssch, p. 804.

 
