AS TO ROGER WILLIAMS AND HIS “BANISHMENT” 237

eject an incongenial intruder. And, that one of radically hostile
opinions, under these circumstances, and with the world all before
him where to choose, should persist in forcing himself upon them;
and, being resident among them, should spend his strength in
decrying their fundamental principles, nor merely, but in doing his
utmost to cut the very hands by which their social order was held
together, was a thing as much more intolerable to them than would
be a similar procedure to'the Vineland settlement, or either of those
close “communities” which now exist among us; as the necessary
perils of an experiment in process of trial two centuires and a half
ago under nearly every conceivable disadvantage, on the edge of a
savage wilderness, must overweigh the petty risks of a modern
pleasure venture in the science of sociology. And how long even
Vineland would tolerate the presence of one who should disturb its
peace in any manner kindred to that in which Roger Williams
disturbed that of the Massachusetts Colony; and how much the well-
informed community should pity such a disturber upon his conse-
quent ejectment; I leave others to judge.

2. Not less essential is some careful consideration of the essence
of the man. It is difficult to look over the grand delights of the
achievements, and the loftiness of the mature quality, of some who
have filled large space in the public eye, to note minutely the follies
of their early days. And there was so much of sweetness, wisdom,
and true nobility in the adult development of Mr. Williams, as to
make it hard for us to remember that he always had great faults, and
that those faults were of a kind to make his immaturity uncomfort-
able to others. In itself, no student could desire to go back now to
draw his frailties from their dread abode; but if the Justification of
others become his inculpation, the truth must be spoken. It would
be a curious study of character to follow exhaustively the traces he
has left of himself upon the history of his time—in what he did and
said, and wrote; and in what others wrote to, and of him, and said
about him. Those were days of free and rugged speech, when even
the best of men sometimes allowed themselves to suspect and
stigmatize the motives of others, and to employ bitter words in so
doing; and just allowance must be made for this. But after all due
deduction, it will unquestionably be concluded that Mr. Williams
did somehow exceptionally provoke the censures of the good. When
he lived in Massachusetts, he was evidently a hot-headed youth, of
determined perseverance, vast energy, considerable information,
intense convictions, a decided taste for novelty, a hearty love of can-
troversy, a habit of hasty speech with absolute carelessness of conse-
