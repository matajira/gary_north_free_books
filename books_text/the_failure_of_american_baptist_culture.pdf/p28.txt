12 CHRISTIANITY AND CIVILIZATION

Machen’s spiritual and institutional heirs ran for cover, hoping that
the embarrassment would soon go away, in much the same way that
fundamentalists ran from Jimmy Carter in 1980.

In the speakers’ room at the National Affairs Briefing Con-
ference, I spoke with Robert Billings, who had worked in Jerry
Falwell’s Moral Majority organization. (He subsequently was ap-
pointed to a high position in the Department of Education.) We
were speaking of the conference, and what a remarkable event it
was, We agreed that it was unfortunate that Rushdoony was not
speaking. He said: “Tf it weren't for his books, none of us would be
bere.” I replied: “Nobody in the audience understands that.” His
response: “True, but we do.”

The Myth of Neutrality

From the earliest days of the Christian-church, those who have
served as apologists of the faith have been influenced (a better word
might be “infected”) by the myth of neutral human reason. !* It has
been an article of faith that there is a common human reasoning, a
common universe of discourse, between Christians and non-
Christians. This common reason has been called many things: natu-
ral reason, natural law, natural reason rightly understood, right rea-
son, and the principle of noncontradiction. In recent centuries, it
has been called the scientific method, or the objective method.

If there were no universal discourse, communication between
people of varying religious faiths (or rival political faiths) would be
impossible. So there is, clearly, some sort of link, Cornelius Van Til
argues that this link is the common creaturehood of ali men under God. Paul
said: “And [God] hath made of one blood all nations of men for to
dwell on all the face of the earth. . .” (Acts 17:26). He also wrote to
the church at Rome concerning the Gentiles, “which have not the
law,” but who “do by nature the things contained in the jaw” (Rom.
2:14). These Gentiles “shew the work of the law written in their
hearts, their conscience also bearing witness” (Rom. 2:15). Not that
they have the law of God written in their hearts. That, said the
author of the Book of Hebrews, quoting Jeremiah 31:31-34, is
something only regenerated Christians possess (Heb. 8:9-11). But
they have the work of the law in their hearts — sufficient knowledge to
restrain them somewhat and to condemn them ethically before God

18. Gf. Van Til, A Christian Theory of Knowledge (Presbyterian & Reformed, 1969).
See also his multi-volume classroom syllabus, Christianily in Conflict (1962-64).
