120 CHRISTIANITY AND CIVILIZATION

7, The sealing of the Day of Judgment through the Resurrec-
tion. Paul applies this “negative” aspect to the covenant of the resur-
rection when preaching to the Athenians (Acts 17:31).

8. The subjugation of all of Christ's enemies (I Pet. 3:22),

9. The triumph of the Gospel (Matthew 28:18-20).

10. ‘The final judgment of the heavens and the earth, all iniquity
being burnt up,?” .

The meaning of baptism, then, has broadened and developed
into its full biblical-historical-redemptive context. It signifies in-
dividual regeneration and justification and cleansing from sin, to be
sure, but much more than these. It is a sign and seal of the broader
realities of the covenant also: of the last judgment realized in human
history through the Cross, of the pouring forth of the Spirit of God,
of the subjugation of all human and heavenly enemies of Christ.

When baptism is administered to the repentant sinner and his
children, both adults and children are sealed into the covenant of
grace, Their membership in the covenant is unconditional. The
elements of baptism are not dependent upon faith for their
efficacy.2® Baptism is a solemn religious oath and is of deep
significance and eternal power. Biblical theology and the redemptive
eschatological perspective insists upon the objective significance of
the sacrament: objective in the sense of having meaning and
significance apart from the response and heart condition of the reci-
pient. It is deeply meaningful to the church and to the King of the
Ghareh, and it is inevitably of eternal significance to the one who
receives the sign.

Baptism can never, therefore, be understood as a mere “dry

 

preached the aeon of the New Covenant in uverview. He was a forerunner of the Son
of Man, preparing his way. Part of his way was the administration of divine
vengeance. John’s baptism of repentance was also a sign of the “wrath to come,” of
the “axe already laid at the root of the trees” (Luke 3:7, 9). Viewing the aeon of
Christ with the first and second comings telescoped together, he underscores the
wrath and judgment of God upon sin exccuted by the Son of Man in this Age. “And
his winnowing fork is in His nand, to clean out his threshing Hlvor and to gather the
wheat inta His barn; but He will burn up the chaff with unquenchable fire” (Luke
3:17, cf. Isa. 63:1-6). Compare this with the presentation of the Cross: “Now is judg-
ment upon this world; now the ruler of this word shall be cast out” (John 12:31).

27. Peter compares baptism with the flood, The family of Naah was brought safely
through the waters, and this corresponds to our baptism (I Peter 3:20, 21), The
fload, and by parallel, baptism, point to the final destruction of the wicked (II Peter
325-13).

28, We speak metonymically, of course. The elements have no inherent power,
‘but the truths dignified by the elements, because they are the very Word of God, do
have inherent power.

 
