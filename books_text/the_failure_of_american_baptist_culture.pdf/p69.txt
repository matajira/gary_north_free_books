SOCIAL APOLOGETICS 53

God created men and left them alone to work out the implications of
their having been created in His image. But without concrete
Biblical law as a basis of definition and public policy, it turns out
that “human dignity” is as elusive a term as “love.” How does one
“love” one’s neighbor other than by fulfilling God’s law with respect
to him (Romans 13:8-10)? “The Dignity of Man” can be and has
been defined in almost every conceivable way. Schaetter is con-
cerned not merely with abortion, but also with infanticide and
euthanasia. Our care for the handicapped and disabled also has to
be considered in all this. But if one starts from man alone, or even
from “man-as wonderful,” one takes the path that leads to relativism
and ethical imprecision. Schaeffer cites many humanists who believe
that aborting the severely handicapped is the only compassionate
thing to do, These situation ethicists speak of “meaningful human-
hood,”28 even as Schaeffer extolls the worth of every individual life.
Christians who begin with the “rights” and dignity of man, instead
of the rights and dignity of God, face the tortuous road of autono-
mous human reason. Somehow, starting with man,®9 these Christians
must reason their way to the proscription of abortion. And they
must compete with the humanists, who have had far more practice
than we at making man the measure of all things.

The courts have consistently taken various “rights” of men and
used ther against the Kingdorn of Christ. The California supreme
court ruled “that California must pay for welfare abortions because
the state constitution explicitly guarantees the right of privacy.”5°
The concept of a “right to life,” that is, the mother’s “right to (quality)
life,” was used by the U.S. Supreme Court in Roe vo, Wade and Doe v.
Bolion to legalize abortions.#! Justice Douglas, in his concurring
opinion, cited the existence of virtually every “right” imaginable;
from the right to life to the right “to loaf” 32

28. Schaeffer, Whatever, pp. 784.

29. And ironically, when it comes to public policy, this scems to be the way
Schaeffer starts. His approach is not.as consistent as it is at times in other of his
works, The “rights” rhetoric works against him.

30, Walter Isaacson, “The Battle over Abortion,” Time (Apr. 6, 1981), p. 24.

31. Of. Doe a Bolton, 410 U.S. 179, at 190. See also Justice Douglas’ concurring
opinion at 214,

32. 410 U.S, 179, 213. Jerry Falwell has said he would be willing to die, appar-
ently, in defense of the right ta loaf. Says he, “We philosophically disagree with the
American Civil Liberties Union, but we would die for their right to do what they are
doing.” Falwell, ibid. Ingram has picked this notion apart: “This is utter nonsense.”
Itis, he continues, a defense of “a claim to the worst of all moral evils, the right to be
wrong.” Ingram, Rights, pp. 16-17.
