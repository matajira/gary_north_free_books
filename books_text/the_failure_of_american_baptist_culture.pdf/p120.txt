104 CHRISTIANITY AND C1VILIZATION

doctrinal agreement was the sine qua non for interdenominational
cooperation during revival campaigns. Liberal church leaders like
Episcopal Bishop James Pike and United Methodist Bishop Gerald
Kennedy were invited to take prominent roles in his crusades, thus
scandalizing the fundamentalists.

The Jesus People movement of young, college educated, born-
again believers was the third step in the process of accommodation.
Because these young people had picked up new values in the secular
counterculture, they sought to Christianize some of these values in
their movement. Hence the expression was born, “getting high on
Jesus.” When these “Jesus People” grew up, shaved, and cut their
hair, they gradually took their place among the leaders of the liberal
establishment in America. This new class of leftward leaning young
evangelical and charismatic writers, thinkers, and activists espoused
all the causes traditional revivalism had called “un-American,”
“pro-communist,” and “humanistic.” This very important fourth
step in the evangelical accommodation process has now been over-
shadowed by the must larger and more visible constituency of New
Right evangelicals and fundamentalists (e.g., the Moral Majority),
but as Quebedeaux observes, “Their importance today is still more
than meets the eye” (p. 87).

The final step in” the accommodation process was the
Charismatic Renewal that occurred among Pentecostals, Roman
Catholics, and Protestants in the late 60’s and early 70’s. An
instrumentalized and undogmatic message was combined with the
same kind of popular psychology fashionable in the wider society to
join these three primary branches of American Christianity. All of
these factors contributed to the effective accommodation of conver-
sion and salvation to the broad, ecumenical themes of popular
religion. Thus in a matter of a few decades, the dogmatic
exclusivism of the revivalists was replaced by accormmodation—
compromise. Why? The biggest reason is that while Christianity
was once given away freely, it is now artfully and persuasively sold
over the airwaves every year to the tune of $1 billion annually.

Because of the scarcity of the dollar, and the demand of the con-
sumer that modern religion satisfy the needs, wants, and desires of
the affluent, bored, impatient, and anxiety-ridden society that he is
a part of, success, second, is a very important value that has been
added to modern religion. In order to accomplish it, modern
religion has had to modify the Protestant work ethic. Instead of
diligent, systematic work for the glory of God, popular religion has
“mentalized” the work ethic that made America one of the
