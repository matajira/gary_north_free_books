CALVIN’S COVENANTAL RESPONSE 195

ing the same promise by the spiritual sacrament of infant baptism
that the Old Covenant believer claimed in the spiritual sacrament of
infant circumcision,

Il. Calvin’s Explanation of the Differences Between the Covenants:
The Relationship of Law te Gospel and Letter io Spirit

Having argued ardenily for the essential unity of the Old and
New Covenants, Calvin is conscious that his opponents can charge
him with failing to come to grips with the numerous biblical
testimonies to the differences between them. To this matter he next
turns bis attention,

 

sons of Ketucah, itis precisely Abraham’s faith as a covenarcal head of the household
that alluws this sign to he administered.

In respect to Israel, faith was still required although as Calvin points out so
forcefully, hypocrisy often took the place of true faith: “He says, that the Israelites
had transgressed the covenant of the Lord, and conducted themselves pertidiously
against his law. He repeats the same thing twice, for the covenant and the law are
synonymous; only the word, law, in my view, is added as explanatory, as though he
had said, that they had violated the covenant of the Larc, which had been sanctioned
or sealed by the law. God then had made a covenant with Israel, which he designed
to be comprehended in the tables. Since then it was not unknawn to the Israelites
what they awed to God, they were covenant-breakers. It was then the doubling of
their crime, as the Prophet shows, that they had not fallen through mistake when
they tranagressed the covenant of the Lord, for they had been more than suificiently
(aught by the Jaw what faith and what purity the Lord required of thera: at the same
time, the covenant which the Lord so openly made with chem was yet neglected”
(Hosea 8:1). Note below Calvin's comments on Gen. 21:12.

‘The point really is chat Jewett is refusing to see that it is the New Testament itself
that presents baptism as the legitimate expression of the spiritual reality of the Old
‘Testament practice of circumtision. Contrary to Jewett's assertion, Paedobaptists do
not try to turn Old Testament circumcision into New Testament baptism. Rather,
they accept the fact that the New Testamen| itself declares that baoti:
sion, in their proper spiritual meanings. So then, even though Jewett is bothered by
all the paedabaptists who repeatedly “intone such conventions as ‘circumcision was
* he nevertheless must not only admit this fact (which
he does, p, 89), but rust also corne to grips with its implications. Jewert is entirely

 

 

a dy circurnci-

   

 

not exelusively a national sign,

correct that the history of redemprion has shown us that the material aspects of cit-
cumczision have been jettisoned in its New Tescament form uf baptism. Nevertheless,
it is precisely the history of redemption that demands the spiritual equivalency of the
twa sacraments! If the Old Testament sign which was frought with such spiritual
significance and was demanded by God to be given: to infants who yet could not con-
sciously believe (as Jeweit himself forcefully argues), then haw can the validity ol”
infant baptism be gainsayed? Thus, the Paedahaptist does not make the error of
reading the Old Testament as if it were the New. He reads the New in light of its own
expression of its relationship of continuity with the Old. Rather, it is the Anti-
paedobaptist who reads the New Testament in discontinuity with the Old Testament
and with itself.

   

 
