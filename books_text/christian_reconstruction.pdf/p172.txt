150 CHRISTIAN RECONSTRUCTION

mation professed the doctrine of predestination by God. “All
Protestant churches which came into being out of the Reforma-
tion hold to that doctrine in their creeds. The Presbyterians and
the Reformed of Holland and Switzerland and Germany, Angli-
cans, the Huguenots, the Covenanters, the Puritans, the Pietists
of Germany, the Pilgrims of America, were all firm believers in
this great doctrine of predestination.”*

It is no accident that it has been Calvinists who have devel-
oped a comprehensive social theory that places all men and
institutions, including civil government, under the sovereign
rule of God. Authority to rule is ministerial, derived from God
and also limited by Him. The State’s right to exist is not based
on the “will of the people” but on the will of God (Romans
13:1-4). The Calvinist believes that

the ultimate source of authority is not the state itself, as in Hegel
and contemporary absolutist philosophers; nor in the people, as in
modern democratic thought; nor ia a classless society, as Marx
taught; but in the will of the triune God. It is God who ordains the
state, confers upon it its legitimate powers, and sets limits upon its
actions. The state is not the source of law, nor of the concepts of
right and wrong, or of justice and equity.*

 

Calvinistic social theory had its greatest impact on the West-
ern world: A limited State and a free people bound by the sov-
ereign rule of God, Arminianism now predominates in the
church. This too has social and political implications. If man is
sovereign in salvation, which Arminianism implies, since God
cannot save until man exercises his will, then man is equally
sovereign in the social and political spheres. To throw off Cal-
vinism is to open the door to apostasy’ and tyranny.* There is

5. D. James Kennedy, Truths that Transform: Christian Doctrines for Your Life Today (Old
Tappan, NJ: Fleming Id. Revell, 1974), p. 81.

6. C. Gregg Singer, fohn Calvin: His Roots and Fruits (Nutley, NJ: Presbyterian and
Reformed, 1977), pp. 33-34.

7. The revivals of the early nineteenth century brought about a “Popular theology
[that] had descended from Calvinism to Arminianism, and from there to universalism,
and so on down the ladder of ertor to the pits of atheism.” John B. Boles, The Great
Revival, 1787-1803: The Origins of the Southern Evangelical Mind (Lexington, KY: The
University Press of Kentucky, 1972), p. 100.

8. A Mervyn Davies, Foundation of American Freedom: Calvinism in the Development of
