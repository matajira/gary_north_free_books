84 CHRISTIAN RECONSTRUCTION

dinances” (Ezekiel 36:26-27), The New Testament summarizes
it this way: “If any man is in Christ, he is a new creature; the
old things passed away; behold, new things have come” (2 Co-
rinthians 5:17, NASB). All of this requires a belief in the sover-
eignty of God. Only God can make dead men live. Only God
can make a dead culture thrive. Noted Reconstructionist scholar
Rousas J. Rushdoony summarizes it this way:

The key to remedying the [modern] situation is not revolution, nor
any kind of resistance that works to subvert law and order. The New
Testament abounds in warnings against disobedience and in summons
to peace. The key is regeneration, propagation of the gospel, and the conver-
ston of men and nations to God's law-word.?

Clearly, there is no hope for man except in regeneration?

Politics, a conservative economic policy, and other social-ori-
ented agendas are not the ultimate answers to man’s dilemma.
Man is a sinner in need of salvation. He can not make proper
evaluations of how he ought to live in the world until he has a
new heart that guides a new mind.

If any critic of Christian Reconstruction fails to recognize this
distinctive, then that critic has not done his homework. He has
not read what Reconstructionist authors have written over and
over again: personal regeneration is essential before any appre-
ciable change will occur in the broader culture.

Keep in mind that we espouse Christian Reconstruction. There
will be no reconstruction unless there are Christians. While
unbelievers can follow the Word of God and benefit by its wis-
dom, it is only the Christian who can fully understand the full
implications of what God’s Word is all about. The non-Christian
has the work of the law written in his heart (Romans 2:15), but
not the law itself (Hebrews 8:9-13).*

2. Rousas J. Rushdoony, The fastitutes of Biblical Law (Phillipsburg, NJ: Presbyterian
and Reformed, 1973), p. 113.

3. Ibid., p. 449.

4. John Murray, The Epistle to the Romans, 2 vols. (Grand Rapids, Michigan: Eerdmans,
1959), 1, pp. 72-76.
