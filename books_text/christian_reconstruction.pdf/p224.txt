202 CHRISTIAN RECONSTRUCTION

Weber, Timothy P. Living in the Shadow of the Second Coming:
American Premillennialism 1875-1982. Grand Rapids, MI: Zon-
dervan/Academie, 1983. Touches on American dispensation-
alism in a larger historical and social context.

Wilson, Dwight. Armageddon Now!: The Premillenarian Response
to Russia and Israel Since 1917. Tyler, TX: Institute for Christian
Economics, (1977) 1991, Premillennialist studies history. of
failed prophecy, and warns against newspaper exegesis.

Woodrow, Ralph. Great Prophecies of the Bible. Riverside, CA:
Ralph Woodrow Evangelistic Association, 1971. Exegetical study
of Matthew 24, the Seventy Weeks of Daniel, the doctrine of the
Anti-Christ.

Woodrow, Ralph. His Truth Is Marching On: Advanced Studies
on Prophecy in the Light of History. Riverside, CA: Ralph Woodrow
Evangelistic Association, 1977. Exegetical study of important
prophetic passages in Old and New Testaments.

Zens, John. Dispensationalism: A Reformed Inquiry into Its Lead-
ing Figures and Features. Nashville, TN: Baptist Reformation
Review, 1973. Brief historical and exegetical discussion by a
(then) Reformed Baptist.
