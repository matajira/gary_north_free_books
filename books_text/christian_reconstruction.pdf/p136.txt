114 CHRISTIAN RECONSTRUCTION

in common is another question altogether, When the agnostic
and atheist come, the difficulties multiply in trying to prove a
natural law theory, especially in the area of particulars.

The reason for this difficulty seems to be that for those who really
believe in creation and the supreme dominion of God, the principle
is too obvious to need proof; whereas for those who do not believe
in creation there is no basis on which to build proof.”

A natural law basis for moral behavior can be developed only
when there is an already-operating biblical ethic. William Black-
stone, the great English Jurist of the eighteenth century, wrote
that natural law must be interpreted in terms of the revealed
law, the Bible. “If we could be as certain of the latter [natural
law] as we are of the former [revealed law], both would have an
equal authority; but, till then, they can never be put in any
competition together.”* The Bible shaped Blackstone’s con-
ception of natural law, although he rarely referred to the Bible
in his commentaries.” But this in itself might be indicative of
how pervasively a biblical ethic influenced him.

Could there ever be a prohibition, for example, against
polygamy based on natural law? While the Bible tolerated po-
lygamy and established laws to govern it to protect the family
unit, it never condoned it (Genesis 2:18-24; Leviticus 18:18; 1
Corinthians 7:2; 1 Timothy 3:2). Many in Israel, including such
rulers as Gideon, David, and Solomon, adopted the polygamous
practices of the surrounding nations. Of course, polygamy be-
gan soon after the fall (Genesis 4:19, 23; 26:34; 28:9; 29:15;
36:2; 1 Samuel 1:1-2). “Polygamy has always been odious
among the northern and western nations of Europe, and, until
the establishment of the Mormon Church, was almost exclusive-
ly a feature of the life of Asiatic and of African people. In com-
mon law, the second marriage was always void (2 Kent, Com.

13. Gerard Kelly, Medico-Moral Problems (Dublin: Glonmore and Reynolds, 1955), p.
167, Cited in Daniel Callahan, Abortion: Law, Choice and Morality (New York: Macmillan,
1970), pp. 310-11.

14. William Blackstone, Commentaries on the Laws of England, 4 vols. (Chicago, IL:
University of Chicago Press [1765] 1979), vol. 1, p. 17.

15. North, Political Polytheism, pp. 322-24.
