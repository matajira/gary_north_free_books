36 CHRISTIAN RECONSTRUCTION

before him with burnt offerings, with calves of a year old? Wilt
the Lorn be pleased with thousands of rams, or with ten thou-
sands of rivers of oil? shall I give my firstborn for my transgres-
sion, the fruit of my body for the sin of my soul? He hath
shewed thee, O man, what is good; and what doth the Lorp
require of thee, but to do justly, and to love mercy, and to walk
humbly with thy God?” (Micah 6:6-8). This message has not
changed.

Christian Reconstructionists are falsely accused of saying that
men are saved in some way through political activism. This is
utter nonsense. Men are saved by grace through faith, and
nothing else. R. J. Rushdoony, who was the primary early de-
veloper of the Christian Reconstruction viewpoint (1963-73),
made this plain in 1973. He had this to say about social and
political progress: “The key is regeneration, propagation of the
gospel, and the conversion of men and nations to God’s law-
word.”* Again, “The key to social renewal is individual regener-
ation.” Critics who accuse Christian Reconstructionists of
teaching a doctrine of political salvation are spreading a gro-
tesque falsehood. If they had read our materials — and very few
of the published critics have — they would know better, They
are bearing false witness, long after we have published detailed
clarifications of a position that was already clarified in numer-
ous other works.°

Judicial Evangelism

Nevertheless, political activism is one of the ways that righ-
teousness is expressed in history. Why should Christians deny
this? Surely, one of the factors that led to the anti-Communist
upheavals in Eastern Europe in late 1989 was the self-conscious

4, R. J. Rushdoony, The Instinwes of Biblical Law (Nutley, New Jersey: Craig Press,
1973), p. 113.

3. Bid., p. 122.

6. Gary DeMar and Peter Leithart, The Reduction of Christianity: A Biblical Response to
Deve Hunt (Ft. Worth, Texas; Dominion Press, 1988); Gary DeMat, The Debate Over
Christian Reconstruction (Ft. Worth, Texas: Dominion Press, 1988); Greg L. Bahnsen and
Kenneth L. Gentry, Jt., House Divided: The Break-Up of Dispemsational Theology (Tyler,
Texas: Institute for Christian Economics, 1989).
