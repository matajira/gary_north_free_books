Humanism and Politics 39

those who worship man as God.

Christians disagree with each of the above humanist asser-
tions.

1, Original ownership belongs to God. God, not man, created, owns,
and controls the carth. God is sovereign.

2. The Creator rules the creature, but God has delegated subordi-
nate ownership to mankind. God is in charge; man is fully responsi-
ble to God.

3. God has made the rules (laws). The Ten Commandments express
the fundamental principles of God’s law.

4, Men are responsible before God to abide by the rules. Man pro-
poses and disposes only within the eternal decree and plan of God.
God judges man in terms of His law.

5. The future belongs to God and God’s people.

Here we have it: two rival religions based on two rival views
of God. This earth is the battlefield in this war of ideas. The
two religions are locked in deadly combat. This conflict takes place
in history. The humanists have had a much clearer view of the
true nature of this historical battle than the Christians have.
They have planned for it far longer than the Christians have.

Politics is a major part of this battlefield. “Not the only one,”
the Christian hastens to add. “On the contrary,” the humanist
immediately replies, “politics is by far the most important part.”
Even here, the two religions disagree.

‘We must not make the mistake that the humanists have so
often made: identifying politics as the heart of the battle. This
war is taking place on ail fronts: church, State, education, art,
economics, and all the areas of life that are engulfed in sin and
in need of revival and reform. Politics is one aspect of this fight,
but it is not the central aspect, for politics itself is not central.
The worship of God is central. The central issue therefore is this:
Which God should mankind worship? The God of the Bibie or
a god of man’s imagination?
