Subject Index 217

Political Polytheism, 15n, 34, 77n,
114n, 172
Political Vulnerability, 43
Polygamy, 109
Pope, 135
Postmillennialism
Augustine, 128
Calvin, John, 128-29
Calvinism, 62
Dabney, Robert L., 130-31
Definition, 87-89, 154-57
Edwards, Jonathan, 129-30
“Faith in Man,” 62-65, 72
Hodge, Charles, 130
Isaiah’s Millennial Vision, 72,
165
Kingdom, 87-9
Israel, 132-39
Lee, Francis Nigel, 178
“Liberalism,” 127-31
Revolution, 140-43
Time, 141
Pound, Roscoe, 115
Power Religion, xviii, 40, 49, 52,
71
Poythress, Vern, 19
Premillennialism (covenantal), 14,
65, 87n, 155
Presbyterian Guardian, 14n
Presbyterians, 150
Presuppositionalism, 89-92, 148
Pretribulational Rapture, 13
Princeton, 15
Prison Reform, 17, 82
Progressive Sanctification, 104
Prophecy and the Church, 177
Prophecy 2000, 70
Public Broadcasting System, 162
Public Education, x
Puritans, xii, 53, 150, 152-53

Quantum Physics, xii

Quest for Community, 45

Racism, 166n
Read, Leonard E., xi
Reduction of Christianity, 2, 36n,
93, 125n, 173n, 175
Reformed Distinctives, 20
Reformed Theological Seminary,
xiv
Religion and Politics, 37
Religious Conflict, 41
Religious Persecution, 7
Remnant Review, xiv, xv
Republic, 121
Revival, 51
Revolt Against Maturity, xiv
Revolution, 48, 140-43
Reynolds, Barbara, 37
Reynolds vs. United States (1878),
115
Rich Christians in an Age of Hun-
ger, 173
Roberts Rules of Order, xiv
Roepke, Wilhelm, x
Roman Catholic Church, 128-30,
147
Roman Empire, 34
Rosenberg, Alfred, 8
Rothbard, Murray N., x
Rushdoony, Rousas John
Constitution, 123-24
Democracy, 124
Christian Reconstruction, ix-
xv, 21, 124
Fundamentalism, 64
“Godfather of Christian Re-
construction, 123
History, 64
Institutes of Biblical Law, 11, 20,
21, 36, 84n, 175, 178
Intellectual Schizophrenia, ix
Law, 11
