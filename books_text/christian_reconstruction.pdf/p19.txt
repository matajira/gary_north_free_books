Preface xix

evaluation: self-evaluation first, and then the evaluation of
everything else. God’s law tells us what God thinks of the works
of selfproclaimed autonomous man: “But we are all as an un-
clean thing, and all our righteousnesses are as filthy rags; and
we all do fade as a leaf; and our iniquities, like the wind, have
taken us away” (Isaiah 64:6). It is not a pretty self-portrait, so
autonomous men refuse to look at it. Meanwhile, Christians
today are afraid to mention its existence, out of concern for the
sensibilities of autonomous men, with whom they have an un-
spoken alliance.”

Nevertheless, covenant-breakers cannot escape the testimony
of God in everything they think, see, and do. They know the
truth, and they actively hinder it, to their own damnation.

For the wrath of Gad is revealed from heaven against all ungod-
liness and unrighteousness of men, who hold [back] the truth in
unrighteousness;* Because that which may be known of God is
manifest in them; for God hath shewed it unto them. For the invisi-
ble things of him from the creation of the world are clearly seen,
being understood by the things that are made, even his eternal
power and Godhead; so that they are without excuse: Because that,
when they knew God, they glorified him not as God, neither were
thankful; but became vain in their imaginations, and their foolish
heart was darkened. Professing themselves to be wise, they became
fools, And changed the glory of the uncorruptible God into an
image made like to corruptible man, and to birds, and fourfooted
beasts, and creeping things. Wherefore God also gave them up to
uncleanness through the lusts of their own hearts, to dishonour
their own bodies between themselves: Who changed the truth of
God into a lie, and worshipped and served the creature more than
the Creator, who is blessed for ever. Amen (Romans 1:18-25).

Common Ground: Disinheritance

Each person is made in God’s image. This is the common
ground among men — the only common ground. We are born
the rebellious sons of the Creator God. We are all of one blood:
“God that made the world and all things therein, seeing that he

7. See below, Chapter 9.
8. Murray, Romans, I, pp. 36-37.
