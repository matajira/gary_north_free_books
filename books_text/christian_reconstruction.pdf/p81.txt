The Four Covenants of God 59

Therefore let all the house of Israel know assuredly, that God hath
made that same Jesus, whom ye have crucified, both Lord and
Christ {Acts 2:32-36).

This eschatological passage is a development of the great
promise of Psalm 110:

A Psalm of David. The Lorn said unto my Lord, Sit thou at my
right hand, until I make thine enemies thy footstool. The Lorp
shall send the red of thy strength out of Zion: rule thou in the midst
of thine enemies. Thy people shall be willing in the day of thy
power, in the beauties of holiness from the womb of the morning:
thou hast the dew of thy youth. The Lox hath sworn, and will not
repent, Thou art a pricst for ever after the order of Melchizedek.
The Lord at thy right hand shall strike through kings in the day of
his wrath. He shall judge among the heathen, he shall fill the places
with the dead bodies; he shall wound the heads over many coun-
tries. He shall drink of the brook in the way: therefore shall he lift
up the head.

Jesus Christ, the great high priest, is the Lord over history,
the King of kings. He alone possesses both offices of high priest
and King. This is why there must be separate institutional
authorities on earth: priests and magistrates, both lawful rulers,
but each with his own office and sphere of legitimate authority.
Each is under God and therefore under obligation to proclaim
and enforce God's law. The church is not under the State;
neither is the State under the church. Both are under God.

Where does Jesus sit? At the right hand of God. Where is
this? In heaven. Does this mean that Jesus will not return to
earth to sit on an earthly throne in Jerusalem or some other
city? Yes. He reigns in history from on high. To leave His
throne in heaven, He would have to give up power and author-
ity, unless that heavenly throne were also to come to earth,
along with His Father, next to Whom He sits. But this transfer
of God's throne of judgment to earth will happen only at the
resurrection of the dead and the final judgment.

The Coming of the Kingdom
Much of modern evangelical Christianity believes that
