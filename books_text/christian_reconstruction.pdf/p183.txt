What Is the Proper Response? 161

is humanism’s view of social change, not the Bible's. This rc-
veals the extent to which modern pietism has been influenced
by contemporary humanism’s worldview. When pietists hear the
words “Christian Reconstruction,” they, like the humanists,
think of a theology based on the ideal of political action
through legislative reform — a narrow ideal, indeed. This would
mean the rule of biblical law. They oppose Christian Recon-
struction for this reason. There has developed a kind of unstat-
ed operational alliance between them. They are united against
biblical law.>

There are times when | think that Christian Reconstruction-
ists are today the only people who think that political action is
not primary. But no matter how many times we say this in
print, our critics refuse to listen. They interpret our words in
their own way. They systematically refuse to understand what
we have in mind, meaning what we have repeatedly written.
(One reason for their confusion is that they refuse to read what
we write.)

This book is a short, concise, and representative example of
what Christian Reconstructionism is, and also what it isn’t. It is
short enough and cheap enough to give even the laziest critic
an accurate survey of Christian Reconstructionism. There will
be no further excuse for misrepresenting our position.

Will this end the misrepresentation? Not a chance! Misrepre-
sentation sells almost as well as sensationalism does. Therefore,
this book is not mercly a positive statement of our position. It
is also a rebuttal. We are tired of the lies and misrepresenta-
tions. We intend to leave the liars without excuse.

This presents us with a dilemma. Should we respond at all?
Or should we remain passively quiet, as if we had not been
attacked, and not merely attacked, but misrepresented and in
some cases even slandered? Should we give our followers the
impression by our silence that we are incapable of answering,
thereby giving credence to our attackers’ accusations? Or should
we present our case forcefully, which of necessity means refut-
ing our opponents’ cases forcefully? If we do, our opponents

3, See Chapter 9, above.
