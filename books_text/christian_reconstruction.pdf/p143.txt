Do Christian Reconstructionists Reject Democracy? 121

“Democracy, I do not conceive that ever God did ordain as a fit
government either for church or commonwealth. If the people
be governors, who shall be governed?”*In the Federalist Papers
(No. 10), Madison writes that democracies are “spectacles of
turbulence and contention.” Pure democracies are “incompati-
ble with personal security or the rights of property... . In
general [they] have been as short in their lives as they have
been violent in their deaths.”* Francis Schaeffer described law
by majority opinion, certainly a definition of democracy, as “the
dictatorship of the 51%, with no controls and nothing with
which to challenge the majority.” Schaeffer deduces a simple
implication of this definition of democracy: “It means that if
Hitler was able to get a 51% vote of the Germans, he had a
right to kill the Jews.”*

Democracies degenerate into exploitation because some vot-
ers discover that they can vote themselves political and financial
favors out of the public treasury. Those seeking power through
majority rule always vote for the candidate promising the most
benefits. The results are certain: democracies collapse because
the public treasury is milked dry, due to greater voter demand.
A dictatorship normally follows.

Actually, our constitutional government is a “republic,” a
system in which the law, not the majority, is supreme. Democra-
cies can degenerate into what Francis Schaeffer called the “tyr-
anny of the 51%.” If whatever the majority wants becomes law,
then a government will become oppressive of its minorities. If
the will of the majority is the law, then an old majority can be
overturned by a new majority, Hitler, it should be recalled, was
elected to public office. Reconstructionists, on the contrary,
press for the enforcement of God's law, which requires just

8. Letter to Lord Say and Seal, The Puritans: A Sourcebook of Their Writings, 2 vols.,
eds. Perry Miller and Thomas H. Johnson (New York: Harper and Row, (1938) 1963),
Vol. 1, pp. 209-10.

4. Alexander Hamilton, James Madison, and Jobn Jay, The Federalist, ed. Jacob E.
Cooke (Middletown, CT: Wesleyan University Press, 1961), p. 61.

5. Francis A. Schaeffer, The Church at the End of the Twentieth Century (Downers Grove,
IL: InterVarsity Press, 1970), pp. 93f.

6. Idem,
