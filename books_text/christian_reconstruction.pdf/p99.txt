Conclusion, Part I 77

The Alliance

The modern church does not believe that Solomon’s words
apply to the New ‘Testament era. They apply only to the final
judgment. Non-Christians do not believe that the words will
ever apply to man. Here we see the heart of the problem of
both the modern church and modern humanism. Neither side
takes Solomon’s words seriously. The humanist denies that God
will bring anyone into judgment, either in time or eternity.
Christians believe in God’s final judgment, but they are far less
confident about His judgments in history.' They are also un-
willing to say which commandments are still binding today, let
alone in what specific ways.

So, there is today an operational alliance between the pietist churches
and the humanists’ world order. Both sidcs implicitly agrce not to
raise the question of God’s commandments in public debate.
Both sides are happy to debate the issues of the day apart from
any reference to God's law.*

This cozy alliance is today being challenged at every point by
two developments: (1) the growing awareness in the thinking of
a minority of Christian leaders and a majority of humanist
leaders that there is an inevitable war between two New World
Orders: Christ’s and autonomous man’s, (2) the theological
system known as Christian Reconstruction, which for the first
time in church history offers the biblical foundations of a com-
prehensive alternative to humanist civilization. I say “at cvery
point” in order to make clear that J am not limiting my discus-
sion to politics. The challenge of Christian Reconstructionism is
much broader and far deeper than mere politics. Our concern
is government, but not simply civil government. We begin our
call for reconstruction with self-government under God. From
there we go to church government, family government, and
only then to civil government. My slogan is “politics fourth!”®

1, Gary North, Millenniatism and Social Theory (Tyler, Texas: Institute for Christian
Economics, 1990).

2. Sce Chapter 9, above.

3. Gary North, Political Polytheism: The Myth of Pluralism (lyler, Texas: Institute for
Christian Economics, 1989), p. 559.
