196 CHRISTIAN RECONSTRUCTION

Hodge, A. A. Outlines of Theology. Enlarged edition. London:
The Banner of Truth Trust, (1879) 1972. Nineteenth-century
introduction to systematic theology in question-and-answer
form.

Hodge, Charles. Systematic Theology. 3 volumes. Grand Rap-
ids, MI: Eerdmans, (1871-73) 1986. Old standard Reformed
text; volume 3 includes extensive discussion of eschatology.

Kik, J. Marcellus. An Eschatology of Victory. N.p.: Presbyterian
and Reformed, 1975. Exegetical studies of Matthew 24 and
Revelation 20.

Murray, Iain. The Puritan Hope: Revival and the Interpretation
of Prophecy. (Edinburgh: Banner of Truth, 1971). Historical
study of postmillennialism in England and Scotland.

North, Gary, ed. The Journal of Christian Reconstruction,
Symposium on the Millennium (Winter 1976-77). Historical and
theological essays on postmillennialism.

North, Gary. Millennialism and Social Theory. Tyler, TX: Insti-
tute for Christian Economics, 1990. A study of the failure of
premillennialism and amillennialism to deal with social theory.

Owen, John. Works, ed. William H. Goold. 16 volumes. Edin-
burgh: The Banner of ‘ruth Trust, 1965. Seventeenth-century
preacher and theologian; volume 8 includes several sermons on
the Kingdom of God, and volume 9 contains a preterist sermon
on 2 Peter 3.

Rushdoony, Rousas John. Cod’s Plan for Victory: The Meaning
of Postmillennialism. Fairfax, VA: Thoburn Press, 1977. Theo-
