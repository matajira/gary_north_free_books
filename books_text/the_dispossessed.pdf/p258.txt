242 Tue Dispossessep

without contravening the Bible’s gracious program for relief. If
you have established a rule that the homeless must attend morn-
ing and evening prayer and Lord’s Day worship—as you should
have —stick to it. If you have established a rule that the homeless
in your shelter must eat in a common dining hall, be account-
able for their daily schedule, and abide by a curfew—as you
should have—stick to it. We all learn best and grow most when
we know our boundaries, when discipline is expected, when ac-
countability is enforced, and when rules are applied. “Sin is law-
lessness” (1 John 3:4). Anarchy and autonomy are no help to
anyone.”

7. De take precautions. Never put individual families in
physical or legal jeopardy. Thoroughly interview and investigate
each person who comes to the Church for help. The Scriptural
prerequisites of submission and obedience will instantly elim-
inate the professional panhandlers. Even so, every precaution
must be taken to ensure the safety of the families and workers in
the homeless ministry. Document every applicant with ap-
propriate forms, documents, and legal liability releases. We
must be as innocent as doves, but we should simultaneously be
as wise as serpents (Matthew 10:16).*

8. Don’t ever give up. You will undoubtedly have bad experi-
ences. You will face difficult situations and heart wrenching cir-
cumstances, But we have been called to walk by faith and not by
sight (2 Corinthians 5:7). We have been commissioned by God
Almighty for our task. So, we must obey. No matter what.?
