160 ‘THE DispossEsseD

The Conspiracy

Absalom was the passionate third son of David, King of
Israel. His personal comeliness and charisma was matched in
greatness only by his undisciplined ego and ambition, Thus, he
was forever getting himself into trouble and embroiling the pal-
ace in controversy and scandal (2 Samuel 13:38-39; 14:28).
When finally his father received him back into favor, the old king
was repaid by a plot against his throne.

And Absalom used to rise early and stand beside the
way to the gate; and it happened that when any man had
a suit to come to the king for judgment, Absalom would
call to him and say, “From what city arc you?” And he
would say, “Your servant is from one of the tribes of
Israel.” Then Absalom would say to him, “See, your
claims are good and right, but no man listens to you on
the part of the king.” Moreover, Absalom would say,
“Oh, that one would appoint me judge in the land, then
every man who has any suit or cause could come to me,
and I would give him justice.” And it happened that
when a man came near to prostrate himself before him,
he would put out his hand and take hold of him and kiss
him. And in this manner Absalom dealt with all Israel
who came to the king for judgment; so Absalom stole
away the hearts of the men of Israel (2 Samuel 15:2-6).

Playing the part of the people’s advocate, Absalom stole
away their hearts. With delicious whisperings and twisted mur-
murings he plied circumstances in his favor. With great skill and
evident adroitness he slanted the facts, edited the truth, and fil-
tered the news always with an eye toward the ratings.

Then, at the peak of the game, he upped the ante.

Now it came about at the end of forty years that Ab-
salom said to the king, “Please let me go and pay my vow
which I have vowed to the Lord, in Hebron. For your
servant vowed a vow while I was living at Geshur in
Aram, saying, ‘If the Lord shall indeed bring me back te
Jerusalem, then I will serve the Lord.’” And the king
said io him, “Go in peace.” So he arose and went to
