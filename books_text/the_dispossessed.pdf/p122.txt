106 Tue DispossesseD

money for the king’s tax on our fields and our vineyards. And now our
flesh is like the flesh of our brothers, our children like their chil-
dren. Yet behold, we are forcing our sons and our daughters to
be slaves, and some of our daughters are forced into bondage al-
ready, and there is no power in our hands because our fields and
vineyards belong to others’” (Nehemiah 5:1-5, italics added).

The nobility in Israel, those who controlled the power of the
state, were using their wealth and position to oppress the poor.
They were using high taxes to force poor landholders and land-
lords into bankruptcy and slavery. Nehemiah was furious, and
rebuked the bureaucrats, saying “The thing which you are doing
is not good; should you not walk in the fear of our Gad because
of the reproach of the nations, our enemies?” (Nehemiah 5:9).
Happily, the nobility repented of their usury, and returned the
lands to the poor.

According to North, “Covenantal Law governs the sphere of
economics. Wealth flows to those who work hard, deal honestly
with their customers, and who honor God. To argue, as the
Marxists and socialists do, that wealth flows in a free market
social order towards those who are ruthless, dishonest, and
blinded by greed, is to deny the Bible’s explicit teachings con-
cerning the nature of economic life. It is a denial of the covenan-
tal lawfulness of the creation.”** The story of Nehemiah and the
nobles of Jerusalem is a bane to Marxists and socialists, to the
leaders of “model” nations like Nicaragua, Sri Lanka, and Tan-
zania, and to the various U.N. bureaucrats, because it turns the
tables and shows things as they really are.

North concludes, saying, “Critics of the capitalist system have
inflicted great damage on those societies that have accepted such
(Marxist and socialist) criticisms as valid. Men have concluded
that the private property system is rigged against the poor and
weak, forcing them into positions of permanent servitude. His-
torically, on the contrary, no social order has provided more op-
portunities for upward social mobility than capitalism.” As
Scripture so clearly demonstrates, it is the state controlled econ-
omy, the abolition of private property, the socialist system, that
is rigged against the poor and weak.

The Legacy of England

Every socialist experiment in history has ended in dismal
failure. At no time have land reforms, rent controls, or state in-
terventions worked in favor of the poor and homeless. In fact, as
