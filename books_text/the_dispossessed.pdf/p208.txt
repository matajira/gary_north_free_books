192

Tue DispossesszD

their cry because of their taskmasters, for I am aware of
their sufferings. So I have come down to deliver them
from the power of the Egyptians, and to bring them up
from that land to a good and spacious land, to a land
flowing with milk and honey, to the place of the Canaan-
ite and the Hittite and the Amorite and the Perizzite and
the Hivite and the Jebusite. And now, behold, the ery of
the sons of Israel has come to Me; furthermore, J have
seen the oppression with which the Egyptians are op-
pressing them. Therefore, come now, and I will send
you to Pharaoh, so that you may bring My people, the
sons of Israel, out of Egypt” (Exodus 3:7-10).

Whenever righteous people have been exiled, alone, in a dry

and weary wasteland, God has come to their rescue and given
them Shelter. He has given them a Home.

A father of the fatherless and a judge for the widows,
is God in His holy habitation. God makes a Home for the
lonely; He leads out the prisoners into prosperity. Only
the rebellious dwell in a parched land (Psalm 68:5-6).

That is why throughout the ages, His people have cried out

in praise, saying,

T love Thee, O Lord, my strength. The Lord is my
rock and my fortress and my deliverer, My God, my
rock, in whom I take refuge; my shield and the horn of
my salvation, my stronghold. I call upon the Lord, who
is worthy to be praised, and I am saved from my ene-
mies (Psalm 18:1-3),

In Salvation God not only washes away our sins (1 Corinthi-

ans 6:11) and Restores us to fellowship (1 Corinthians 1:9), he also
Redeems us from our wanderings, and gives us a Home (Psalm

107

11-9).

This great privilege comes to us “by grace through faith, and

this not of ourselves, it is the gift of God” (Ephesians 2:8-9).
There is absolutely nothing that we can do to earn it, merit it, or
deserve it.
