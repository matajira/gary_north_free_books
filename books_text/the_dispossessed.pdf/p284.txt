268

55.

Tue DispossesseD

Humanism (Tyier, TX: Geneva Ministries, 1985), pp. 34-38. Jordan’s book
is a sustained demonstration that ancient Baalism and modern secular hu-
manism are identical at root,

John Whitehead, The End of Man (Westchester, Itinois: Crossway Books,
1986), pp. 37-38.

Chapter 10— Leave By the Dogtown Gate: Transiency

w

a

pone

10.
1.
42.
13.

14,
15,
16.
17.
18.
19.
20.
21.
22,
23.

24,
25.
26.

27.
28.
29.
30.
31.
32.
33.
34,

. John Steinbeck, The Grapes of Wrath (New York: The Viking Press, 1939).
. Alton Ford, Transiency (Santa Cruz: Det Radino Press, 1982), p. 7.
. Tristran Holbeare, Time Bomb: The Decline of American Industry (New York:

Singletary Publications, 1984), p. 202.

Ibid

Barry Bluestone and Bennett Harrison, The Deindustrialization of America
(New York: Basic Books, 1982).

. The Houston Post, November 21, 1982.
. Holbeare, p. 202.

Tbid.

. Bluestone and Harrison, p. 99.

Tid.

Ibid.

Holbeare, p. 202.

George Grant, “Boomtown to Bust-town” (Humble, Texas: The Christian
Worldview, 1984).

Ibid.

Ibid.

ibid.

Ibid.

Ford, p. 4.

Bluestone and Harrison, p. 100.

Thid.

Emma Lazrus, “The Great Colossus,” inscription on the Statue of Liberty.
Bluestone and Harrison, p. 100.

For a recent example, see Richard D, Lamm and Gary Imhoff, The Immi-
gration Time Bomb (New York: Truman Talley Books, 1985).

Karl de Schweinitz, England's Road to Social Security (Philadelphia: Univer-
sity of Pennsylvania Press, 1943), p. 39.

Adam Smith, An Inquiry into the Nature and Causes of the Wealth of Nations
(London: Penguin Books, 1962) 1:170-171.

8. Humphreys Gurteen, A Handbook of Charity Organizations (Buffalo, New
York: Charity Organization Society, 1882), p. xxi.

Ibid., p. xxii.

Ibid., p. xxiv.

Inside the U.N., December, 1985.

HABITAT News, December, 1985.

See chapter 7,

See chapter 8.

See chapter 9.

Ford, p. 17.
