Mama Don't Take My Kodachrome Away: The Media ‘159

ing the parameters of this public issue and dictating its social
agenda. Homelessness, as it is perceived by most Americans, is
almost entirely an invention of the media moguls, a product of
guilt and pity holiday machinations.

Sadly, most educated people know full well what the press is
doing on these occasions, and as a result are skeptical, become
hardened, and pass the matter off.

Cashing In

Recognizing the central role the media plays in defining and
directing the homelessness issue, the United Nations has planned
a massive media blitz to promote the ways and means of the In-
ternational Year of the Homeless.'® And recognizing the critical
importance of the holiday season for the success of any such
media campaign, the U.N. has geared up for an unprecedented
Thanksgiving to Christmas publicity burst. After their spectac-
ular successes with previous International Years, agency officials
have been confident from the start that their efforts will pay off
and pay off big.”

Utilizing USA for Africa, Band Aid, Comic Relief, Live
Aid, and Hands Across America as a spring board, various ad-
vocacy groups and U.N. agency heads together were able to
finalize plans for six major TV news documentaries, three movie
length features, a multimedia exhibition sponsored by HUD,
seventeen corporate sponsors for T'V ads and billboards, as well
as innumerable magazine and newspaper spreads — all to be un-
leashed on the public during 1986 and 1987 holidays.”

The newsmakers and truthmakers intend to create a great
deal of “news” and a great deal of “truth” in a hurry. They intend
to cash in. They intend to be the lever and fulcrum to effect a
social welfare revolution if not a social revolution. They intend to
steal the hearts of the people under the cover of philanthropic
concern, thereby establishing their rule in name as well as in
fact. They intend to “make a difference.”

Like Ben Thompson, they will use their video images. They
will wield their tools of electronic alchemy to turn anecdotes into
“news,” “news” into “truth,” and “truth” into “power.” Conspiracy
buffs, alert! This one’s for real.”
