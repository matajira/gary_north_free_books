150 ‘Tur. DispossesseD

the liberty to pursue opportunity. Thus what they need are the
open arms of the Biblical system, not the cold steel trap of the
humanist system.

In the U.S. this Biblical freedom does exist at least in part.
Thus, unlike the third world squatter camps and the refugee
hovels in the U.N.’s model nations, the tent cities in Houston,
Phoenix, Los Angeles, etc. were only temporary. Dislocation
and homelessness gave way to new starts, new opportunities,
new homes—for most, anyway.

“The truth shall make you free . . . and you shall be free in-
deed” ( John 8:32, 36).
