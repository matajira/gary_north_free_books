Leave By the Dogtown Gate: Transiency 145

impetus for the fulfillment of a “Manifest Destiny,” have been
more often regarded as threats to the security and stability of the
social and economic order.”4

In 1662, during the fourteen year reign of Charles II, Eng-
land passed the Law of Settlement.%* An amendment to the Eli-
zabethan Poor Laws, this statute was designed to empower local
justices, churchwardens, and overseers to expel outsiders from
settling in a particular parish or county. If the magistrates, by
whatever objective or subjective measure ihey chose, deter-
mined that the settler was undesirable, or incapable of support-
ing himself without resorting to relief, then he could be sent back
to the place from which he had come.

The statute was an act of parochial caution. Since relief was
a local responsibility, the magistrates wanted to make certain
that they only had to care for their owen poor. They wanted noth-
ing to do with someone else’s poor, someone else’s problem,
“Keep the outsiders out,” they cried. The king answered with
the Settlement Law, thus establishing residency requirements
for the poor, restricting their travel, limiting their labor options,
and narrowing the focus of relief.

The effect of the law was to keep people where they were. It
stymied opportunity. It discouraged initiative. It created labor
imbalances. And it turned some counties into virtual prisons of
deprivation. Forced ghettoization. The great economist Adam
Smith thought the situation abominable enough to devote a sec-
tion of his Wealth of Nations to a critique of it. He argued not only
that the law was a tyrannical infringement of citizens’ liberties,
but also that it restricted the “free circulation of labor” so essen-
tal for growing economies.

In 1795, after more than a century and a quarter of protest,
and owing greatly to Smith’s concerns, the Settlement Law was
superceded by a series of new statutes variously called the “Relo-
cation Laws,”2¢ the “Colonial Laws,”2? and the “Resettlement
Laws.”8 The pendulum had swung to the opposite extreme.
These laws, rather than coercively containing the poor, required
the forceful eviction and conveyance of the poor. The relief rolls,
the workhouses, the debtors’ prisons, and the destitute counties,
it was thought, could be emptied by moving the poor and home-
less to the colonies or to developing regions where labor short-
ages prevailed. Forced migration.
