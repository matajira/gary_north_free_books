Leave By the Dogtown Gate: Transiency 43

They came to the Sunbelt because it was being touted as the
nation’s “job mecca.” The Houston Chronicle had the largest “Help
Wanted” section in the U.S.8 Copies of the Sunday edition sold
for as much as $20 in Detroit unemployment lines. It was a
boomtown, and it drew economic refugees from the north like
moths to a candle flame.

According to the U.S, Department of Commerce, almost
seven million people moved to the Sunbelt from the northeast
and the midwest.’ Another 4.7 million moved to the West.!®
“This stream of migrants is so vast,” said one study, “that if they
all had come from the six New England states, this entire region
would have been left without a single man, woman, or child.”"!

In 1979, 8%, or almost one in every twelve Americans sixteen
years of age and older, were living in a state different from the
one they had lived in just five years earlier. By 1984, that percen-
tage had jumped to 12% .!2

Unfortunately, the economic promise of the Sunbelt was illu-
sory. Though certainly healthier than the postindustrial Mid-
west and Northeast, the job market simply could not absorb the
thousands of new workers.'? The housing industry was unpre-
pared for the sudden influx.'* The schools, already struggling to
keep up, were strained to the breaking point.'5 Social service
agencies were buried beneath an avalanche of need.'© Thus,
those who followed their hopes and dreams south all too often
were sorely disappointed. They could see evidence of boom-
town’s boom all about them, but were unable to tap into its
riches themselves. !7

Having sacrificed everything to make the move, in most cases,
the migrants could ill afford long, protracted searches for jobs or
apartments. As hours stretched into days, days into weeks, and
weeks into months and still no jobs were to be found, many of
them wound up homeless. Living out of the back of their cars, in
public campgrounds, under bridges, and in abandoned ware-
houses, they were caught between the allure of promise and the
din of reality. They wound up like Anya and Elz, living by their
wits, wandering from place to place, and hoping against hope.

The incidence of homelessness during this most recent
migratory surge, says Alton Ford, “is certainly the highest since
the earliest days of the Oakie exodus during the Great Depres-
sion. Perhaps it’s the highest ever.”'8
