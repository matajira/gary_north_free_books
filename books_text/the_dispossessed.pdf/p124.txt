108 Tue Dispossessep

constituency from which England’s burgeoning Industrial Revo-
lution would draw its laborers. Had England been able to con-
tinue in a contentedly pastoral fashion, the rapid, almost urgent
growth of its manufacturing sector would certainly not have oc-
curred.# England would probably have lagged behind the pace
of France and Spain, and modern capitalism would have taken
an entirely different shape.* Through great economic growth
and the advancement of emerging technologies, virtually all of
the displaced peasants were soon employed gainfully and shel-
tered adequately. Free enterprise fitfully pushed past a rough
stretch and began to stride forward once again.

Second, the public at large was forced to re-examine its faith,
and the application of that faith to the world at large. Obedience
to Christ was not limited, they now understood, only to “spirit-
ual” matters, but affected all of life, including tough issues like
the care of the poor.*” As a result, a series of “social security”
measures were implemented at the behest of the king and the
parliament on the parish level, culminating in the Elizabethan
Poor Laws in 1601.

Clearly, the free enterprise system was not then, nor is it now
“perfect.” Nothing is in this poor, fallen world, But even in the
midst of its greatest failures, the free enterprise system is able to
adjust, to push ahead, to reform, and ultimately to emerge
stronger and more beneficent than ever before. And that is a
claim that socialism simply cannot make. Not now, not ever.

Conclusion

If homelessness is to be overcome in any measure, affordable
housing must be made available somehow. That is a given. But
how? Many cities and municipalities have tried to regulate the
current housing stock through rent controls, hoping to contain
prices to affordable levels. Many nations, like Nicaragua, Sri
Lanka, and Tanzania have gone even further, actually confiscat-
ing lands and redistributing them,

Well intentioned or not, these measures inevitably hurt the
very people they were supposed to help. When the opportunities
afforded by the free enterprise system are subverted unbiblically
through theft and regulatory interference, the poor are hurt most
of all.

There is a crying need for low-income housing. Demand.
