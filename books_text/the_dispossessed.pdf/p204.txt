188 The Dispossessep

When Cain rebelled, God poured out the full measure of that
Curse upon him,

Then the Lord said to Cain, “Where is Abel your
brother?” And he said, “I do not know. Am I my
brother’s keeper?” And He said, “What have you done?
The voice of your brother’s blood is crying to Me from
the ground, And now you are cursed from the ground,
which has opened up its mouth to receive your brother’s
blood from your hand. When you cultivate the ground,
it shall no longer yield its strength to you; you shall be a
vagrant and a wanderer on the earth.” And Cain said to
the Lord, “My punishment is too great to bear! Behold,
Thou hast driven me this day from the face of the ground;
and from Thy face I shal! be hidden, and I shall be a vag-
rant and a wanderer on the carth . . .” (Genesis 4:9-14).

He became homeless, Indeed later when he finally tried to
settle down, he went to the land of Nod (Genesis 4:16). “Nod”
means “wandcring.” Cain became a nomad, a man without a
home, a man marked by dispossession.

Again, when the world turned from God in wicked rebellion,
He cursed it, blotting out all mankind, save Noah and his family
(Genesis 6:5-8, 7:23). The ungodly were driven from their
homes and dispossessed to the uttermost.

When the wicked tried to reverse the consequences of the Fall
by their own efforts, by building a tower into the heavens, God
again uprooted them. They wanted to prevent themselves from
being scattered from the land (Genesis 11:4). They wanted to
establish a new Eden, a New Age, by the strength of their own
hands, and by the cunning of their own minds. But the great ex-
periment at Babel failed and the rebellious were scattered out of
the land away from their homes (Genesis 11:8).

The region of Sodom and Gomorrah, once a lush and abun-
dant land not at all unlike Eden (Genesis 13:10), and home to a
rich and proud people (Genesis 14:1-24), was judged when those
people turned to harlotries and abominations. It became a land of
“brimstone and salt, a burning waste, unsown and unproductive”
(Deuteronomy 29:23). Its only surviving inhabitants were cast out
of their homes to take refuge in the hills and caves (Genesis 19:30).
