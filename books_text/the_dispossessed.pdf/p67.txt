Making Hay; The U.N. Resolution ot

U.S. to adopt those “models” as the next step toward the egalitar-
ian destruction of the West.
They want to dispossess us all in the name of the dispossessed.

Conclusion

Homelessness is a global problem. Recognizing that, the
United Nations designated 1987 as the International Year of the
Homeless. But far from being a benevolent attempt to spur real
solutions to that global problem, the “Year” is simply another
tool for the U.N, to capitalize on a worthy cause for the advance-
ment of its own ends. The “Year” is simply an opportunity for
globalists to make a lot of political hay.

Meanwhile, however, the problem remains unsolved, once
again proving that humanism is the most inhuman of philoso-
phies. While the bag ladies of Amsterdam, and the orphans of
Calcutta, and the street urchins of Bangkok, and the squatters of
Managua, and the refugees of Chad, and the wandering gypsies
of Byelorussia languish in utter deprivation, the “advocates” for
justice and equality launch ideological diatribes and spawn
propaganda campaigns against their anti-globalist adversaries.
While 100 million to one billion struggle against the ravages of
homelessness in the Third World, they focus their righteous in-
dignation against the U.S., with its 250,000 to three million
homeless.

Why? Because the worthy cause of homelessness —like the
other worthy causes the U.N. has deemed to champion—has
been subverted to serve the U.N.’s own political and messianic
aspirations.
