Stull Crazy After All These Years: Mental Illness 65

This awareness is woven into the very fabric of reality: in the
warp and woof of creation (Romans 1:20) and in the very con-
sciousness of the human mind (Romans 1:19).

Men cannot get away from this central reality, the reality that
undergirds all sanity. But they try, anyway. They run from the
truth of God by running from the world and running from them-
selves. Thus they become irresponsible, destructive, and suicidal
{Proverbs 8:36). They open themselves up to oppression and
possession (Proverbs 1:10-18; Ephesians 2:1-3; 1 Timothy 4:1-2),

This was the Gerasene’s root problem. He had run from his
responsibilities to God and his responsibilities under God in
order to escape from the inescapable, In the process he had been
taken captive by demons and driven to utter insanity. Jesus
knew this and acted accordingly.

As Jesus was getting into His boat for the return trip, “The
man who had been demon possessed was entreating Him that he
might accompany Him, And He did not let him, but He said to
him, ‘Go home to your pcople and report to them what great
things the Lord has done for you, and how He had mercy on
you. And he went away and began to proclaim in Decapolis
what great things Jesus had done for him; and everyone mar-
veled” (Mark 5:18-20).

The man wanted to continue his life of irresponsibility by tag-
ging along with Christ’s entourage. Long “dead” to his family,
having followed a downward spiral of depravity and derange-
ment to the tombs, he now wanted to perpetuate that revolt
against maturity. Under the cover of religious devotion he
wanted to procced unabated with his frivolous, devil-may-care,
unreliability,

Jesus refused his request.

Instead, He prescribed a simple, yet comprehensive, rehabili-
tation program for the man. First, he was to return home and
take up his responsibilities. And second, he was to bear testi-
mony of the grace and mercy of God.

The Word of Christ had freed him of the demonic enslave-
ment, It had brought him to his senses, returned him to his right
mind, and reoriented him to reality. But as miraculous as that
first step was, it was only the first step in the Gerasene’s recovery.

He needed to be rehabilitated through the discipline and
routine of family life, through the reinforcement and encourage-
ment of community life.
