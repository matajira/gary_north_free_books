240 Tue DispossEssED

charity extravaganzas. First, find out what the needs are in your
community, If there is already adequate emergency shelter but
no food program or job placement service or trade school out-
reach, then obviously you don’t want to start off building alms-
houses. Do a demographic study. Find out what services already
exist. Talk to social service workers, police, community associ-
ations, hospital administrators, school teachers, and of course
other pastors. Bring focus and purpose to your work.?

3. De work in concert with others. Learn from existing pro-
grams. Supplement, don’t supplant. We must begin to coordi-
nate our efforts with existing programs in other Churches. We
must network, perhaps even set up a computer link between
ministries to share information, resources, and ideas, Our ulti-
mate aim must be to do the work of the Kingdom, not to ad-
vance our own individual causes or reputations.*

4, Don't compromise Scriptural concerns just for the sake of
harmony. Coordination and cooperation are important and it is
doubtful that Christians will ever be able to make a significant
dent in the monolithic culture of humanism until and unless we
comprehend the necessity of catholicity, but we must never yield
to the heresy of “peace and cooperation at any price.” The princi-
ples of service, covenant, and grace must be enforced. On this,
there can be no compromise. Scripture cannot be broken.5

5. Do make certain that the charity offered by your Church
is distinctively ecclesiastical and not simply a conservative pri-
vate initiative program along the lines of the “Luther-esque”
charities, as we discussed in chapter 12. Make certain you offer
the homeless True Refuge. Make certain that the very best of the
historic Christian models, pastoral and monastic, are woven into
the ministry: The homeless should be nurtured daily on the Word
revealed, the Word made manifest, and the Word incarnate.

The Word renews the minds of the poor. Through the teaching
of Scripture, the way and will of God is revealed. Right doctrine
shatters old habits, explodes bad thoughts, and establishes real
hope. The Gospel changes people. Thus, our charity agenda must
not simply be one more conservative, deregulated, family-
centered, work-oriented, and decentralized program. It must be
forthrightly evangelistic. The poor need good news. They need
the Good News.

The Word readjusts the poor both to Gad’s society and to the
