134 Tue Dispossessep

When the Israelites failed to drive the Canaanites out of ihe
land, but instead intermarried with them, the problem of the re-
lationship between Yahweh and Baal was raised. According to
Biblical scholar Arthur Cundall, “Yahweh had given Israel a
considerable victory over the Canaanites and thus His suprem-
acy was unquestioned.” Unfortunately though, “The average
Israelite associated Him with the wilderness in which they had
spent the major portion of their lives. In Canaan they were de-
pendent upon the fertility of the land, which, in popular
thought, was controlled by Baal. Many, therefore, conceived it
wise to pay a deference to the pagan god.” This tendency to fol-
low the course of expediency was aggravated by the sensuality
and materialism of the Canaanite cultus. Baalism was not only
expedient, it was fashionable and fun! So, the Israelites struck a
devil’s bargain. They continued to honor Yahweh, but no longer
as the one, true God. Instead, in the interests of security —agri-
cultural security—they placed another god before Him.

According to John Whitehead, “Humanism can be defined
as the fundamental idea that people can begin from human rea-
son without reference to any divine revelation or absolute truth,
and by reasoning outward, derive the standards to judge all mat-
ters. For such people, there is no absolute or fixed standard of
behavior. They are quite literally autonomous . . . a law unto
themselves. As such, there are no rights given by God; no stand-
ards that cannot be eroded or replaced by what seems necessary,
expedient, or even fashionable at the time. Man, it is presumed,
is his own authority, his own god in his own universe.”5> That
being the case, Israel’s quest for security—their Baalism— was
blatantly and classically humanistic.

The Israelites cither forgot about or ignored the revelation of
God concerning their security (Deuteronomy 28) and reasoned
for themselves, They determined on their own authority to fol-
low the “necessary,” “expedient,” and “fashionable” course to se-
curity. Many understood the nature of their devil’s bargain but
proceeded to capitulate to the passion and pragmatism of the
moment anyway (2 Kings 15:34-35). The lesser of two evils,
don't you know? The price you have to pay, don’t you see?

In time, Baal worship became public policy, enforced by the
government (1 Kings 8:4). This too was a matter of security. You
can almost hear Jezebel or Ahab saying, “The security afforded
