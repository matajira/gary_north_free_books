SIXTEEN

PROOF IN THE
PUDDING:
A BIBLICAL REALITY

 

It can be done.

Homelessness can be conquered.! Biblically. One person at a
time. One day at a time.

“I spose the proof’s in the puddin’, An’ I s’pose I’m the pud-
din’. I spose.”

“Yeah, I think you're right, Earl. You're the pudding. You're
the proof.”

He smiled. His dark eyes danced gaily with the kind of sheer
joy that comes with a hard fought, hard won victory.

I returned the smile. The victory was indeed sweet.

Earl had been homeless, But no more. He had been unem-
ployed for nearly three years — since his Navy discharge. But no
more. He had devolved into heavy drinking, careless carousing,
and aimless wandering. But no more. He had begun drifting
further and further from reality, out of touch, out of sight, out of
mind. Literally. But no more.

“Ain’t no magic wands. Ain’t no easy ways out. It’s been
tough, I admit. But it’s been worth it. Ever’ bit.”

I just smiled again.

He visited the Church first. He zipped in and out of the Sun-
day service so quickly that I missed him the first two times he
came. I made a special effort to catch him the third Sunday, and
I did, out in the parking lot. He didn’t tell me then that he was
homeless. But I could tell. Not that he was particularly unkempt
or seedy, He just had that hollow look of sadness. There was that
barrier, that wall that almost all homeless erect between them-
selves and the rest of the world.

235
