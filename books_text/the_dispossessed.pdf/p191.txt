The Golden Ship: The Secular Shift 175

The Biblical notion of the covenant was of necessity ignored.**
Again, every change in the social welfare system in the West
since 1600 has simply been a development of this “Luther-esque”
presupposition: The prerequisites for relief eligibility are to be
secular, nof covenantal,

Third, though Luther himself never intended his plan to be
anything but compassionate, most of the magistrates who imple-
mented it made certain that public relief was thoroughly stigma-
tized. In most instances, it was punitive and repressive, acting
more as a deterrence to the dole than anything. Since relief had
become a benefit of citizenship dispensed bureaucratically like
any other governmental service, it was no longer necessarily
hedged from harshness by compassion and grace as charity had
been. It was susceptible to every wind of doctrine that blew across
the political landscape. Again, every change in the social welfare
system in the West since 1600 has simply been a development of
this “Luther-esque” presupposition: relief entitlement is not charity.

Martin Luther was, of course, a great hero of the faith in
many, many ways. But in the area of poverty relief he introduced
a bane that the Church still labors under today.

The Long Term and the Short Term

James B. Jordan has written that “the three faces of Protest-
antism were, and are, the imperial or nationalistic face, the sec-
tarian or drop-out face, and the catholic face. The Reformers
can fairly easily, though roughly, be divided into these three
groups. There were drop-out anabaptists; there were those who
looked to the state for reformation; and there were those who
sought to reform the Church in a catholic manner, apart from the
state. In brief, the Lutherans and the Anglicans tended to be
magisterial in their approach, setting the prince or the king over
against the Pope of Rome. Calvin and Bucer, along with some of
the other Swiss Reformers, focussed more on a reformation of
the catholic Church, and avoided nationalism ,”57

He continues, asserting that “Luther provided a convenient
way for the princes of Germany to do what they had always
wanted to do: take over the visible power of the Church. Luther
so stressed the personal and charismatic aspect of the Gospel,
over against the institutional side, that his movement fitted nicely
with the designs of the princes. At the same time, from a political
