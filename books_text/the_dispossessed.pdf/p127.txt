EIGHT

EPITAPH IN RUST:
UNEMPLOYMENT

 

His arms and body stil! showed signs of abnormal strength in
the thin, narrow bands of muscle rippling beneath his T-shirt,
But that strength looked entirely out of place in the sterile office.
Stifled. Uneasy. He sat, placidly waiting, resignation etched on
his face. :

The woman behind the desk ignored him. She busily shuffled
and sorted the papers and files that littered the room — artifacts
of a hundred other failed lives, The office had a single window
with a spectacular view of a garbage-strewn alley. In the room
there were four filing cabinets, a single potted poinsettia, long
since dead, two ashtrays, both full, and on the walls there were a
few old shop safety posters, faded, water stained, and torn. Insti-
tutional. Musty.

“Now then Mr. , . . uh, Mr. Gallin,” she said not looking
up, “Let’s see what we can do for you.” Scanning a single sheet,
she issued forth with a series of unconvincing “ums” and “ahs,”
and then, “Well, . . . right now, I’m afraid... .”

“I know! I know!” he cut in, “same ol’ song an’ dance . . . but
hey, it’s okay. No excuses necessary.” He rose to go.

She now looked up at him. Her eyes met his for the first time.
They were steely grey and opaque. Dull. Unseeing. “Sorry. . . .”

He was out the door before she could finish the all too famil-
iar liturgy of condescension. She sighed. Setting the “Gallin” file
aside, she reached for the next. “Only twenty more minutes till
lunch,” she thought.

Mick Gallin irresolutely made his way down the long corri-
dor thinking, “Okay, What now?” Into the men’s room, over to
the bank of sinks, splashing lukewarm water on his face, he pon-

il
