226 Tre Dispossessep

that it matched the best of the best— it rivaled the automation of
many “corporate farms.”

But then the bottom fell out. In November 1983, Gerry went
to see his banker in order to get a loan for seed and fertilizer, his
perennial post-harvest, pre-tilling chore. To his utter astonish-
ment, the banker turned him away, It seems that because of the
now-plummeting values on prime Missouri cropland, Gerry's
equity had been more than cut in two. His debt service was tri-
ple “what it should have been.” So, even though he had never
been delinquent on his payments —not once in the thirteen years
he'd done business with the bank—he was no longer considered
a “good credit risk.”

Without credit, Gerry was unable to put in a crop that year.
And despite the fact that he stil! had income from his dairy cows
and rental properties, without a crop he was unable to keep up
with his bills.

Eighteen months later, Gerry’s farm was sold at an auction.
He was lucky. The auction brought forty-five cents on the dollar.
Most auctions do well to get half that. Even so, the creditors got
everything and suddenly Gerry was without a job, without a
home, without anything.

“Thirteen years of work and what have I got? . . . Nothin’ at
all,” he said. “Nothin’ at all. . . an’ I’m tellin’ ya’ what. If it can
happen to me, it can happen to anyone. The American farmer is
in trouble. Aw’ if he’s in trouble, the rest of the country is in dou-
ble trouble.”

Double Trouble

According to the Census Bureau, only 14% of this nation’s
poor live in the decaying inner cities.! Another 47% live in the
large metropolitan areas or urban suburbs.? But all the rest, a
full 39% of the total poor population, live in rural regions.? Of
the more than 34.6 million poor Americans, 13.5 million live in
the country.* They are farmers, ranchers, loggers, hired hands,
or migrant harvesters. They are people that supply our grocery
shelves, stock our markets, and produce the raw materials on
which our industry depends. And many are now homeless as
well as poor. As many as one-third of our nation’s homeless are
from rural areas.>

And it doesn’t look to get better in the near future.
