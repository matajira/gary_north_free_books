8 Tre DispossesseD

And that was it. We were shuffled off like cattle, while the old
man in the street continued to lie there, untended and unnoticed.

Hell’s Color

Once again on the second floor, there were many more men
than chairs, so there was a lot of sitting on the stairs and slouch-
ing against the walls. One wizened and irascible old codger
seemed especially disturbed by the close quarters, He shook his
crutch and traded immensely profane insults at those who came
too close. Another, in a drunken stupor, stumbled and fell down
a flight of stairs, hitting his head against a radiator. No one even
looked up. Still another proceeded unabashedly to urinate on the
wall. He had been mumbling nothing much at steadily increasing
volumes, and this vile act of exhibitionism and bravado scemed
to be his last-gasp attempt at securing the room’s attention. Still,
no one even blinked an eye. No one noticed. Resigned, the man
quietly retired to a vacant spot against the wall.

I scanned the stuffy room. I scrutinized every face. The un-
touchables. America’s pariahs. Surplus. Disposable.

They all looked groggy and bleary-eyed—some apparently
from drink, some from drugs, some from lack of sleep, many
from all three. Most seemed so ravaged by illness, addiction,
madness, and sheer neglect that I could not imagine them ever
making their way back into society's mainstream. They were
hopelessly lost. And a dark cloud of misanthropic gloom
descended over me.

“Ain't a pretty sight, is it?” ‘The small man beside me had
been fidgeting constantly since we'd entered the room. He spoke
slowly, hesitantly. “I never knew hell came in this color.”

I smiled. Quite a line, Black comedy, But the man remained
humorless, looking at me in dead seriousness. He had not meant
it as a joke.

Immediately in front of us, one man suddenly grabbed an
empty chair and attempted to break it over his neighbors’ head.
They were shouting and wailing at one another. In my haste to
get out of the way, I jostled a sleeping drunk on one side of me,
and fell across the small disconsolate man on the other. Everyone
was yelling now. I was terrified.

Ii took almost fifteen minutes for the guards to untangle the
mess, ‘The strong bullying the weak. The hale brow-beating the
