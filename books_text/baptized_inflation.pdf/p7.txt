x Bapiized Inflation

ties.z It revealed that this group was controlled by homosexuals
throughout the early decades of the twentieth century, and that
their most prestigious members in this era John Maynard
Keynes, G. Lowes Dickenson, G. E. Moore, and Lytton Strachey
— were all dedicated homosexual perverts. So blatant was their
public commitment to sodomy that Fabian socialist Beatrice
Webb was concerned in 1911 when her protégé Bertrand Russell
returned to Cambridge to teach, for he had been an Apostle as a
student there in 1892, and she feared that he might get involved
with them again: “. .. we have for a long time been aware of its
bad influence on our young Fabians .*

The Apostles was not simply some undergraduate club. Mem-
bers continued to attend meetings for decades. In Keynes’ fourth
year at Cambridge, he became Secretary, yet there was only one
undergraduate member. * Like the powerful Yale University secret
societies, Skull and Bones (George Bush, William Buckley, etc. )
and Scroll and Key, membership was gained as an undergrad-
uate, but the links continued throughout life.

Deacon’s summary is important for what follows in this book:
“Homosexuality probably reached its peak in the Society when
Strachey and Maynard Keynes formed a remarkable partnership
in conducting its affairs. Here were two minds both devoted to
achieving power and influence in their respective ways. Keynes
himself was the chief protagonist of the homosexual cult, obsessed
with the subject to an abnormal degree for one with a good intel-
lect and wide interests. So obsessed, in fact, that when, years
later, he married the Russian ballet dancer, Lydia Lopokova, the
news was received with outraged horror among such friends.
Some of them never forgave him; others maliciously speculated
what was the real reason for the marriage. It slowly dawned on
them that this was all part of Keynes’s power game.

“The Apostles repudiated entirely customary conventions and

2, Richard Deacon, The Cambridge Apostles: A history of Cambridge University dlite
intellectual secret society (London: Robert Royce Ltd., 1985).

8, Ibid., p. 62.

4, ibid., p. 79.
