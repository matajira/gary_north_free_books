Introduction B

however, “a significant logical and methodological reconstruction
in economics” thereby giving “us a new way of looking at things”
so that we now have a “more complete understanding of the struc-
ture and functioning of the economic system. .. . “2‘

In Economics and Man Dr. Vickers has as his purpose, first, to
set forth a Keynesian system of Christian economics and, second,
to redirect the “misdirected” efforts of North and Rushdoony who
have allowed “considerable confusion . . . to enter economic
argument from a purportedly Christian perspective. It was in
order to contribute to a correction of that perspective that we have
developed the entire argument of this book in the manner and in
the order we have adopted.”

Conclusion

This book is written from the belief that it is Dr. Vickers, not
North and Rushdoony, who has contributed to the “considerable
confusion” in the debate about Christian economics. I cannot
claim to be calm and evaluative always, for I am overwhelmed on
occasions that so many things which do not make sense — common
or otherwise — are taken for granted and passed off as “serious”
economic scholarship on a more than gullible Christian public.
However, I have endeavored at all times to provide answers to
those ideas that Vickers would have us believe are Christian and
economic.

There are no “brute” facts — facts independent from other
facts, and independent from God’s authoritative revelation. All
knowledge resides in God, and since He knows everything ex-
haustively, all facts exist in God-given and God-interpreted rela-
tionships. Man’s knowledge, on the other hand, is forever finite.
He struggles to put facts into some coherent whole, or else he
rebels against the idea and accepts the concept of brute factuality,
where meaning (relationship of ideas) does not and cannot exist.
However, because man is limited in knowledge and does exist in
