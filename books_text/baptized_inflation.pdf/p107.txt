6
ANTINOMIANISM AND AUTONOMY

The Christian thinker, while he can in no sense devalue the impor-
tance of the individual and personal experience of the salvation which
God has set forth for His people in Christ, nevertheless is aware that it
is in the midst of the world that God has redeemed ond established His
church, This world is the world over which God reigns as Creator and
King, and the Christian. necessarily sees it as part of his God-given
task to understand, and where possible to influence, societal structures
80 that they are brought into closer conformity with the scripturally
articulated preceptive will of God, It is not that it is proper to speak of
the fact or possibility of a Christian society. For society is fallen,
apostate, inherently and structurally pagan, '

Here is the heart and soul of Dr. Vickers’ world-and-life view.
He believes that Christ is a King, but that Christ’s kingship will
never be manifested in time and on earth. Jesus regenerates men,
but regenerate men never have sufficient numbers or sufficient in-
fluence to reconstruct the world’s pagan institutions along Biblical
lines. Therefore, since God’s people will not be able to do this, Dr.
Vickers implicitly argues throughout his book, God no longer has
given us a law structure by which we might reconstruct this world, ond, even
more important psychologically for Dr Vickers, a law structure which God
holds us responsible to promote,

In order for us to establish that what Dr. Vickers is calling for
is a disguised version of socialism or communism, it has been nec-

1, Vickers, Economics and Man, pp. 44-45.
83
