206 Baptized Inflation

have cause to be more conservative in their expectations. Perhaps
the grape harvest this year is super-abundant, so grapes are a lit-
tle easier to come by. Meanwhile, beef prices are soaring because
the supply of beef had been overestimated, and there will be some
shortage of prime beef this season. All these factors contribute to
the valuations which each person makes about the commodities
and services on his or her scale of preferences, and a free market is
one where these changing decisions are registered in the market
place as people transact their business.

Authoritarian Continuity

Suppose, though, someone were to object to these changing
valuations. He dislikes this world of changing prices and fluctuat-
ing conditions. Instead, he thinks that prices should be held con-
stant. Suppose also that he is a government official with the au-
thority to issue a minimum price order. “Starting today, all buyers
will pay $5 a pound for prime beef irrespective of market condi-
tions or changing valuations which buyers and sellers may make.”
Such a decree looks good at first glance, at least to buyers who
were willing to pay over $5 a pound and sellers who were unable
to compete at prices under $5. Prices are regulated. Now we all
know where we are going. How much simpler budgeting will be
from now on! Right?

There is a missing factor in this equation. Our benefactor may
have noble ideas, but he is begging the question. What about
those subjective valuations which individual human beings make?
It may appear all very well to decree beef will be sold at $5a
pound, but what happens if prospective buyers are willing to pay
only $4? What will happen is quite simple: they will not buy the
beef until its price goes down to the level they are willing to pay.
Each of us has his own mind, his own opinions, his own scale of
preferences for all those things which are desired. Human beings,
made in the image of God, think and act in such a way as to reflect
their personality, their individuality, and their uniqueness in the
universe which God has created. But our mysterious benefactor
wants to do away with all that. Instead, his valuations, his scale
