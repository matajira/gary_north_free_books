12
THE GREAT UNMENTIONABLE: UNEMPLOYMENT

There are no inherent reasons in a modern capitalist or quasi-
capitalist economy why the system will automatically equilibrate at a
situation. of full employment. 1

In his crucially important book, The Structure of Scientific Revolu-
tions (1962), Thomas Kuhn describes the way in which a particu-
lar academic discipline changes its collective mind. These infre-
quent major revolutions involve an academic guild’s rejection of
many of the truths of an earlier era, and the adoption of new in-
sights that were considered taboo, or preposterous, by the masters
of the guild prior to the revolution. This transformation, he says,
seldom involves large numbers of the existing members who
change their minds. Instead, they retire or die, and younger men
who have adopted the new viewpoint replace them.

Why do these revolutions occur? Kuhn says that they take
place w-hen bright people, who are either outside the guild “or are
too young to have invested very much in developing insights (or
professional papers) that favor the existing outlook, begin to take
notice of certain anomalies that the guild chooses to ignore. Per-
haps it is a theoretical inconsistency. More likely it is some result
of experimental inquiry which cannot be explained well by the ex-
isting world-and-life view of the guild, what Kuhn calls its para-
digm. As more and more bright people focus attention on the
anomalies, the older masters get upset. They charge younger men

1, Vickers, Economics and Man, p. 186.
201
