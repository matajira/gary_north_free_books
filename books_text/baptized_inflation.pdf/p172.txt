Say’s Law 151

Keynes never bothered to be honest enough in his scholarship to
quote Say accurately, and it is somewhat surprising to find that a
Christian economist wants to follow the same intellectually shoddy
route. Of course, Dr. Vickers may have a legitimate excuse: he
may never have read J. B. Say, just as he seems never to have
read Thomas Sowell’s book on Says Law.

“Goods for Goods”

Say’s Law merely stated that a general overproduction of all
commodities is impossible because, in economic terms, goods
always exchange against goods, money being merely an inter-
mediary acting as a common denominator showing the exchange
ratio which exists between, for example, cars and bicycles, shirts
and trousers, or potatoes and rice. In the words of Say, “you will
have bought, and every body must buy, the objects of want or
desire, each with the value of his respective products transformed
into money for the moment only.”!* Money is a medium of ex-
change, a commodity which facilitates the exchange of goods and
services. But Say’s Law depends upon exchange ratios (prices) be-
tween goods and services being in perfect balance — that is, in
equilibrium — a qualification Dr. Vickers (and Keynes) conven-
iently chooses to ignore.

Dr. Vickers writes: “The logic of this argument implied that if
supply created its own demand at any given or specified level of
production and employment, supply would similarly create its
own demand at all conceivable levels of employment. There could.
not therefore exist any obstacles to the full employment of the
total work force available and willing to work.” “ Here we find the
great Keynesian misstatement of Say’s Law. (Apparently Dr.
Vickers has made little effort to check primary sources which
Keynes quoted. Had he done so, he would have discovered that

14, Jean Baptiste Say, “Of The Demand Or Market For Products “ in Henry
Haulitt (od.), The Critics of Keynesian Economics (New Rochelie, New York: Arling-
ton House, [1960] 1977), p. 18.

15, Vickers, A Christian Approach to Economics and the Cultural Condition, pp.
59-60.
