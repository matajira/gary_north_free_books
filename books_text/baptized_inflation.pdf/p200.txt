Mumble, Mumble 179

Furthermore, if inflation is the increase of money, the investi-
gator is more likely to ask himself “What happens to the money
after it is created? It is spent into circulation. Who benefits from
the goods that are purchased by the spenders (or the votes that are
purchased)? Who are the recipienis of this newly created money?
What advaniages accrue to them, if any? Who are those who gain
access to this new money later? What are the disadvantages, if
any, of ‘standing toward the end of the line’? What happens to
which prices in what sequence from the time the new money is issued.
to the time that it no longer has any measurable effects?” In short,
as Lenin so aptly epitomized the science of politics, who, whom?
Who wins, who loses? If the State has the legal monopoly of issu-
ing money, then initially the State and its beneficiaries win. At the
end of the process .. . ? Revolution?

The crucial economic issue of monetary theory therefore be-
comes the question of relative prices, not the aggregate price level. The
crucial issue becomes the economic effects of newly created money
on particular segments of the economy, on certain specified inter-
est groups. Only at the tail end of economic analysis does a per-
ipheral and subordinate question appear: the effect of monetary
inflation on “general prices,” meaning a statistical index number.

Those who define inflation as an increase in the money supply
tend to be those who examine relative prices and their effects on
the way the economy functions. They examine the prices that you
and I examine as we go about our task of buying and refraining
from buying. Those economists who define inflation in this way
tend to be in the Austrian School. The man who for half a century
has called his colleagues to focus on relative prices rather than
aggregate prices in their search for economic understanding is
Frederick A. Hayek. He has continually criticized Keynes and the
Keynesians for their unsalutary neglect of relative prices.? By im-
plication, he also criticizes the monetarists and the Chicago
School economists for this same neglect.

2,F. A, Hayek, A Tiger By the Tail: A 40- Years’ Running Commentary om Keynesian-
ism » Hayek, edited by Sudha R, Shenoy (Landon: Institute of Eomomic Affairs,
1972).
