Mumble, Mumble 187

steadily transferred to those who gain access to the new money
early, and who spend it on goods and services, from those who re-
ceive it later, and who face higher selling prices than would other-
wise have been the case. It is all done by fictional bookkeeping,
and therefore deserves the description “fiat money” — created out
of nothing.

An economist who defines inflation to mean an increase in the
money supply is thus talking about two phenomena in modern so-
ciety. On the one hand, he is talking about the manufacture of
notes and coins by the government, and on the other hand, he is
talking about fractional reserve banking policy which also
“manufactures” additional money. The fact that all modern bank-
ing systems are controlled by the federal authorities through na-
tional reserve banks is proof that even this method of increasing
the money supply is ultimately controlled by State officials.

Effect

We have seen the cause of increasing the money supply, but
what is its ¢fec!? The law of supply and demand, contrary to
Keynesian opinion, still operates. An increase in the supply of
any good, say potatoes, will result — other things remaining un-
changed —in a lowering of the money value of that particular
good. When we consider that commodity in terms of money, we
can say its (money) price has declined. We now need less money
to obtain it. Or, we could look at the same transaction and say
that the value of money has increased because we can now purchase
a larger quantity of that good with the same amount of money.
Still another way we can put the same idea is to say that as com-
modity sellers, we now need to sell more of that good in order to ob-
tain the same quantity of money (or any other economic good we
wish to exchange for it).

If potatoes were money, that is, the medium of exchange in so-
ciety, we would now say that prices have risen; it would now cost
more potatoes to obtain any other good or service. Like a coin
which can be looked at from either side or from its edge, so any
transaction can be viewed from several vantage points. Money is
