70 Baptized Inflation

no coincidence that in the past our forefathers condemned social-
ism, for the y had Biblical warrant to do so. The Thirty-Nine Articles
of the Anglican Church (#38), and the Belgie Confesston’s (#36) con-
demn the Anabaptists and their attempts to establish community
of goods. ?°

Vickers on Private (Sort of) Property

Dr. Vickers acknowledges the concept of property rights. He
says, for example, that “the right of property carries with it, it is
clear, the right of disposal. ... ”'! There is “in the parables of
Christ. . . an affirmation of the right of private property. . . .712
But Dr. Vickers does not intend such property rights in the sense
of having final and exclusive control. Instead, he argues that ex-
clusive ownership exists “in the general case... .”13 According to
Dr. Vickers, this is an implication of the Reformation and its
understanding of ‘the individual person. ... ”!* You would have
to conclude that his language is a bit vague. It would not be easy
for a social reformer to reconstruct society's economic institutions
by using Dr. Vickers’ definition as his guideline.

Dr. Vickers’ sole defense of his own view of property rights is
that apparently it was the same view which was put forward by
those during the period of the Reformation. This historical asser-
tion, however, is a debatable point. Private property rights had.
major support during the Reformation. According to Gottfried.
Dietze,
the Reformation was probably the most revolutionary event in modern
church history. It challenged the most powerful church on the earth. It

10. In an age when the communist philosophy of ownership pervades the
Church, it is not surprising to find attempts to abolish previously accepted doc-
trines condemning socialism. Thus, the Reformed Churches which adhere to the
Belgic Confession are embarrassed by its condemnation of the Anabaptist move-
ment to the extent that some have altered this article of faith.

11 Vickers, op. cif., p. 118.

12, Ibid., p. 126.

18, Ibid, p. 292, emphasis in original,

14, Idem.
