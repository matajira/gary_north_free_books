Sovereignty and Money 163

final tie was cut by President Nixon in 1971, but the case against
gold had been decided long before this legislative act.

Warehouse Storage Receipts (Banking)

The rise of paper currency in relationship to gold is also of
great interest ... and I do mean interest. When gold and silver
served as currency, goldsmiths and silversmiths often performed a
valuable service of storing these commodities for safe keeping.
They were the first bankers who issued receipts to the depositors.
The recipients of these warehouse receipts in turn had a legal
claim on that amount of gold or silver stock held by the banker.
People soon found, however, that in transactions it was not neces-
sary to go to the bank to get the metal to trade. Others were will-
ing to accept the banker’s receipt in the transaction knowing they
could present the receipt at the bank and be paid the full value of
the receipt.

This procedure operated exactly in the same manner as the
check does in modern society. A check is nothing more than a
receipt which entitles the holder to obtain money from someone’s
account. There is one essential difference between modern check-
ing accounts and the old system of warehouse receipts: the old.
receipt was a receipt for something which had value as an eco-
nomic good. It was not necessary for kings and governments to
legislate a value for gold or silver, for it was determined in the
market place by the free actions of human beings.

Fractional Reserves

The bankers got greedy. They realized soon enough that peo-
ple did not keep coming to them in order to redeem their ware-
house receipts for the actual precious metal. The bankers began
issuing more receipts for gold than they had gold in reserve. This
was the origin of fractional reserve banking. They did this because
they could lend out the newly created money and gain interest on
their money. This was the origin of the boom-bust economic
cycle. Fractional reserve banking is a form of fraud. It results in
painful depressions that are the result of euphoric, fiat-money-
