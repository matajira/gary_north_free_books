Conclusion 263

tual cowardice, but that is the way the academic game is played.

The truth of the matter is Keynesianism has been refuted a
thousand times over in the past 200 years. (That’s right, for
Keynesianism is nothing but a rehash of the old mercantile theor-
ies which the early classical economists demolished with consider-
able ability. )? The fact that Samuelson could honestly admit the
language in Keynes’ General Theory does not always seem to make
sense is evidence of precisely that point: it is a nonsensical book.
Yet now we have Dr. Vickers presenting Keynesianism to us first,
on the pretext that it is sound economic theory, and second, that it
is Christian economic theory.

The fact is, however, that his book does not make any serious
use of any of these famous pre-Keynesian economists. He men-
tions a few of them occasionally, but his book is in no sense reliant
upon the classics. We find more footnotes to an obscure and un-
identified writer named H. F. R. Catherwood than to Adam
Smith and the other economists Dr. Vickers parades around in his
Preface. In fact, he hardly even cites Keynes’ General Theory, which
is understandable, since there is very little evidence that he has
read it. Like most post-Keynesians, Dr. Vickers does not rely on
its convoluted arguments and peculiar definitions when it comes
down to writing his own book. It is a lot easier to quote from the
post-Keynesian “crowd” than to struggle with The General Theory. I
don’t blame him a bit.

My analysis of Dr. Vickers has concentrated on establishing
two things. First, he is prepared to misstate his opponents in order
to score debate points. (Fortunately for his opponents, his style is
so muddled that he never quite scores, ) This is intellectually dis-
honest and poor scholarship. Second, we have seen that he tries to
argue for a supposed third way in economics by denying that eco-
nomics is an either-or choice between socialism and capitalism.
We have seen, however, that he fails in this regard, for he categor-
ically rejects the use of Old Testament law and history to guide us

3, L, Albert Hahn, The Economics of Hlusion: A Critical Analyses of Contemporary
Economic Theory and Practice (Wells, Vermont: Fraser, [1949] 1977), ch, 10: “Mer-
cantilism and Keynesianism.”
