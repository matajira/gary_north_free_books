xvi Baptized Inflation

lical law, and use the plain teachings of the Bible to reconstruct
the field of economics. That would be terrible, Dr. Vickers assures
us in Economics and Man. Better to use the teachings of a God-
hating, principle-hating, State-loving homosexual pervert as our
guide to economic wisdom, Dr. Vickers proclaims. He staked his
career on that premise, and lost. Ian Hedge shows just how badly
he lost.

Hedge’s book is uncompromising. It pulls no punches. It does
not profess to be a “gentlemanly exchange of opinions .” It recog-
nizes that Dr. Vickers has proclaimed falsehood in the name of
truth, economic perversion in the name of academic scholarship,
and antinomianism in the name of biblical principle. Dr. Vickers’
Economics and Man is a defense of the “received wisdom” of the
Keynesian revolution — an academic revolution which has led the
whole world to the brink of economic catastrophe: an interna-
tional orgy of debt, inflation, and broken promises by the State.

Hedge also realizes that the fate of civilization hangs in the
balance in our day, and that Dr. Vickers has his thumb on the
scales — on the side of evil. Hedge recognizes that the economic
recommendations made by Dr. Vickers and his far more influen-
tial Keynesian academic peers have pushed the world into evil,
and therefore toward God’s righteous judgment. Like an Old Tes-
tament prophet confronting a fourth-level false prophet of some
rebellious king of Israel, Ian Hedge points the finger at Douglas
Vickers and warns him: “Thus saith the Lord!”

I wonder if Dr. Vickers will listen. False prophets never did in
the Old Testament.

Dr. Vickers isn’t used to such “undignified” discussions. Tough
providence for him. He has become an evangelist for a pervert, a
defender of lies, and a scoffer at biblical law, all in the name of
Jesus. He deserves everything this book gives him.

Now, a word of warning to the reader. This book bogs down in
several sections, not because Hedge can’t write clearly, but be-
cause he quotes Dr. Vickers verbatim. You will not believe just
how bad published writing can be until you struggle with a para-
graph or two from Economics and Man. Like any sane person, you
