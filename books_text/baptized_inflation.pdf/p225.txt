The Great Unmentionable: Unemployment 205

which is left over. People change, they grow older, their likes and
dislikes alter; limited resources and not so limited wants means a
constant juggling of all desires on a scale of values. Today a new
auto is on the top of a rich man’s valuation scale, tomorrow a world
cruise. Although a certain price will be paid for a commodit y today,
tomorrow may bring another price. It could go lower if something
else becomes more important, or if that item climbs the ladder of
our wants, what we are prepared to pay for it will increase.

Market Continuity

Consequently, there can be no such thing as unchanging
prices. Surprisingly, though, in a free market prices tend to be
rather stable. For mass-produced items, there is a tendency for
prices to fall, especially for products that have been introduced re-
cently, and which are now the focus of intense competition from
new producers who have entered the market. Where there are a
number of people acting in accordance with their scale of prefer-
ences, the price results of the very often marked changes within
each individual's preference scale are levelled out across the mar-
ket as a result of the large number of people in the market. There
is a kind of averaging process going on. Buyers drop out of one
market, and may be replaced by others who have similar, if not
identical, preferences. While one buyer omits or lowers auto-
mobiles on his scale of desires, another raises autos on his list,
thus mitigating against the choice of the former. In all this, the
market possesses relative stability. Participants can form generally
accurate expectations about what will be available tomorrow, and
at what price.

This does not mean that the market cannot be subject to wild
fluctuations. Anyone who follows the stock markets knows that
markets can, and do, change quite dramatically and often quite
rapidly. But this is because there has been a fundamental shift in
valuations by a large number of people at one time. A particular
gold stock plummets, possibly because the latest mining report
has indicated that expected yields will not be achieved. Investors
have been overly optimistic on their anticipated profits, and now
