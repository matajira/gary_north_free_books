Man's Rebellion and Socialism 81

is no longer free to travel, no longer free to buy foreign books or
journals, once all the means of foreign contact can be restricted to
those of whom official opinion approves or for whom it is regarded
as necessary, the effective control of opinion is much greater than
that ever exercised by any of the absolutist governments of the sev-
enteenth and eighteenth centuries.”5*

Conclusion

The policies suggested by Dr. Vickers are simply socialism in
disguise - and not very well-disguised at that. But they are social-
ism in fact. Dr. Vickers does not admit that the Keynesian policies
he offers are socialistic; instead, he tries to argue they are Chris-
tian. He does not prove his case that he is not an implicit socialist.
He fails to see that regulations must usurp private ownership. As
Rushdoony has noted, “Property can be alienated by expropria-
tion, injury, restrictive legislation, and a variety of other means.”
Dr. Vickers’ claim that the “new guidelines” and “new kinds of reg-
ulatory frameworks” are not a call “for the specific control and
direction of any person’s economic activity” is deceptive, to say the
least, if he implies these regulations can exist and, at the same
time, maintain private property rights in the sense we have
argued here. “ By definition, regulations must specifically control the in-
dividual at some point; otherwise they are not regulatory.

Dr. Vickers will never be content until the supposed causes of
disharmony are removed. He believes that government economic
planning and regulations can remove many of these causes of dis-
harmony. His policies move inexorably towards complete control
by the authorities. We call such a system socialism. To give it any
other name is self-deception. 57 Socialism is not a system of plan-
ned harmonies; it is a system of planned chaos .** To masquerade

54, Friedrich A. Hayek, The Road To Serfdom (Chicago: University of Chicago
Press, [1944] 1976), p. 92, n. 2,

55, Rushdoony, Institutes, p. 497, emphasis added.

56, Vickers, Economics and Man, p. 837.

57. Mises, Socialism, p. 2.58,

58, Mises, Planned Chaos (1949); reprinted in Socialiem, Epilogue.
