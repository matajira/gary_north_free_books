Antinomianism and Autonomy 97

breakers, and they have declared tog-ether: “We want to live under
any law but Old Testament law.” In short, the anti-theocrats are
inescapably antinomians — anti-God’s law — and therefore they
have become implicitly the defenders of the idea of autonomous
man,

Freedom Under God

Does atheocracy lack checks and balances, as some critics
have argued? Nothing could be further from the truth. There are
clearly defined and delineated roles for the State, for the priest-
hood, for the tribe of Levi, and for the family. For example, taxes
are limited (Ex. 30:11-16), people are told tobe charitable to their
neighbors who are not as fortunate as they (Deut. 14:22-29), debts
are for a limited period only (Deut. 15:1-6), and the list goes on.

Rushdoony has pointed out that “Few things are more com-
monly misunderstood than the nature and meaning of theocracy.
Itis commonly assumed to be a dictatorial rule by self-appointed
men who Claim to rule for God. In reality, theocracy in Biblical
law is the closest thing to radical libertarianism that can be had."8"
A theocracy dees not mean totalitarian rule by some human per-
son or institution. Neither does it lack checks and balances. It is
the fact that the Scriptures lay down limited functions for ali
earthly institutions which provides the necessary checks and bal-
ances in a fallen world. For example, itis because the Scripture
forbids even the civil authorities taking property which belongs to
someone (I Kings 21:3,20; of. Ex. 20:15) that a person may find
refuge and security in possessing the property in the first place.
Understandably, socialism-promoting antt-theocrats within the church are
repelled by this very limitation of State power. They hate Biblical law be-
cause they love the State.

A theocratic civil government isone which acknowledges the
God of Scripture as the source of all legitimate power and author-
ity. Writes Rushdoony: “There are thus a variety of spheres of

33, The Meaning of Theocracy, Chalcedon Position Paper No. 15, available from
Chalcedon, P, 0, Box 158, Vallecito, CA 95251.
