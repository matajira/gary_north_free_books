xxiv Baptized Inflation

view in their tenured classroom security. If they go into print, as
Douglas Vickers did, they can expect “the treatment .” Mean-
while, let them pick up the battered, semi-conscious body of their
colleague, dress him in a new tweed jacket, and tell everyone that
he only suffered minor cuts and bruises at the hands of unspeak-
able reconstructionist ruffians.

By the way, those who are unfamiliar with Christian academia
need to be made aware of another aspect of these public beatings.
Once the skewered colleague is out of earshot, his fellow scholars
sit around reading reconstructionist materials, chortling among
themselves. “They really caught him on that one, didn’t they? Ho,
ho, ho.” They may resent us, but they enjoy seeing their colleague
publicly deflated, yet they can hardly admit their delight publicly.
This is one reason why they buy our books, although in plain
brown wrappers. They like to see an occasional beating. Like the
rest of us, they like to see stuffed shirts get the stuffing knocked out
of them once in a while. They are not complete ignoramuses; they
recognize that the victim really is an intellectual lightweight.
They have had to sit through his boring monologues in faculty
club meetings on numerous occasions. Furthermore, they are not
altogether free from envy: the enjoyment of watching some
“superior” sort get knocked off his pedestal. He got a book into
print; éhey never have. But no one inside the faculty lounge would
admit this moral weakness, even to himself. Nevertheless, they
buy our books. Their colleague, even with his new tweed jacket,
never again looks quite the same in their eyes. He suspects as
much, too. That is why I enjoy publishing a book like this one.

The best example of this is Ronald J. Sider, whose Rich Chris-
tans in an Age of Hunger was the rage on neo-evangelical campuses,
1977-1982. He got the usual free ride. Then ICE published David.
Chilton’s Productive Christians in an Age of Guilt-Manip ulators in 1981,
and a second edition in 1982. Sider was stunned. The free ride
was over. He wrote a second edition in 1984, promising on the
cover that he had replied to his critics. In fact, he replied to none
of his published critics, especially Chilton. Within a few months,
Chilton had written and ICE had published the third edition of
