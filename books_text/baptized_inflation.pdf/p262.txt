The Keynesian Disaster 243

These are the banks, you understand, that are taking advantage
of the deregulation of U.S. interstate banking and the regional
bank crisis to rush in and gobble up local “problem” banks. These
are the banks that the U.S. government regards as the system’s
long-term hope, the lenders of next-to-last resort. After that lies
only the Federal Reserve System’s money creation machine.

These debts amount to more than just economic theory. It is
not simply that the economics profession teaches that debt is the
road to prosperity, the manner in which stones will be turned into
bread, to use Mises’ apt description. There were other contribut-
ing factors. Debt became a way of life in the West, especially in
the United States.

First, there was the incredible post-war boom in productivity.
The pent-up demand after five years of monetary inflation, coupled
with the lifting of price controls (repressed inflation, Rdpke called
it), exploded in a wave of consumer buying. Now that manufac-
turing was for consumption rather than war purposes, the quan-
tity and variety of goods increased significantly. The optimism of
the post-War period made Americans future-oriented in a new
way: they planned to become more productive in the future, so
they borrowed to buy that dream house, hoping to pay off the
loan. It worked, too. As a result, the wealth of American citizens,
in comparison to other nationalities, increased quite dramatically.
United States citizens had more spendable income, more ability
to save, more reasons to look for opportunities to turn those sav-
ings into profits. So they taught their children the same lesson.
And so long as productivity increases per capita can continue to
grow faster than the debt burden, the program can continue. But
productivity takes capital and entrepreneurship, and debt-based
consumer buying as a way of life has begun to erode the thrift
impulse. So did the loss of Christian faith. So did Keynesian eco-
nomics.

The result can be seen in the following chart. The ratio be-
tween after-tax income (dropping because taxes are rising) and
total debt payments has more than doubled, 1950-1985. This chart
graphs a revolution in Americans’ thinking about debt.
