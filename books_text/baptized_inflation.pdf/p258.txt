The Keynesian Disaster 239

other producers. We forecast the economic future as best we can.
We deal with inescapable uncertainty.

Tn this sense, and in this sense only, does supply constitute de-
mand, or vice versa. It is not that all things produced are desired
by others. The ten-year flop of Economics and-Man is proof enough
of that reality. It could easily be described as a Keynesian
pyramid. It did not create its own demand (contrary to Say), nor
did anyone’s demand for it create its production. It “was an’entre-
preneurial venture which cost both author and publisher more
than they bargained for. The publisher sat on a pile of unsalable
books (at the low, low price of $6.95) for a decade, and the author
attracted few or no disciples. The only beneficiary was Exposition
Press, the “vanity” publisher that got Dr. Vickers to fork over
thousands of dollars to print his subsequent unsalable book. I
would estimate that even at zero price, for A Christian Approach to
Economics and the Cultural Condition, there would be greater supply
than demand. Dr. Vickers created a non-economic good.

The Debt Bomb

Couple this idea— that if only we had more money we could
become wealthy - with the willingness of people to borrow against
the future in order to obtain present temporary consumer satisfac-
tions, and you have produced a recipe for an impending disaster.
On the one hand, there is one group of people, the “haves,” who
are looking for opportunities to lend out their wealth in order to
make more profits, while on the other there is another group, the
“have-nots,” whose desire is just as strong to get that money. Once
this is combined with the Keynesian fallacy of spending to create
wealih, both groups are willing to ignore common sense and.
undertake projects which, by any stretch of the imagination, have
little chance of success. For example, to lend money on govern-
ment projects that have no basis for generating the ability to make
the repayments, is an exercise in futility. It is the construction of
pyramids. To lend money to bankrupt nations in order for them
to pay interest charges to New York banks, rather than finance the
development of a viable economic project, produces a result similar
