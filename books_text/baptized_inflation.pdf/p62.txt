36 Baptized Inflation

these is to betray the possibility of successful and meaningful results
right at the beginning of the journey. It is to shunt the engine of eco-
nomic discovery immediately onto tracks that exit from meaning and intelli-
gibility, at least ag far ag the ultimate objectives of inquiry are concerned. 15

When “meaning and intelligibility” are the issue, Dr. Vickers’
style is a bit suspect. But I digress. Dr. Vickers wants to avoid “the
tendency to equate the legitimate limits of economic objectives
and behavior with the narrowly defined interests of atomistic, per-
sonal, individualistic satisfactions .. .” as well as the “deeper cat-
egories behind such pragmatic expressions as the capitalism-
socialism debate. ... ”!® After all, according to Dr. Vickers, if we
think in the area of individualism, we are ‘in danger of being trap-
ped in the fruitless morass of anarchies, exploitation, and social
disharmonies.”!” There are much “deeper springs and motivations
of human action” than are to be found in the “capitalism-socialism
and collectivism-individualism dichotomies. ... 7

In other words, we should not think in traditional categories of
economics. The reason why we should not think in this manner is
not stated, and unfortunately his writings do not clarify precisely
what these “deeper springs” are, or what he perceives to be the
“anarchies, exploitation and social disharmonies.” Moreover, Dr.
Vickers argues that the avoidance of these “deeper springs” also
leads to the avoidance of “the logical and ethical demands of the
notion of stewardship. . . .”°

Now this is an interesting method of argument on Dr. Vickers’
part. Not one concrete economic theory or Scriptural reference is
offered, yet Dr. Vickers is endeavoring to manipulate his readers’
consciences in such a way that we feel guilty for having had such a
“simplistic” view of economics: an either-or choice between capital-
ism and socialism. * In addition, “Economics is suddenly no longer

 

15, Vickers, Economics and Man, p. 8.

16. Ibid, pp. 2, 8.

17. Ibid, p. 2.

18. Ibid, p. 8.

19, Idem.

20. For further examples of argument by guilt manipulation, see David
