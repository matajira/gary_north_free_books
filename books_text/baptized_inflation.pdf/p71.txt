46 Baptized Inflation

teleology.”"® In short, Dr. Vickers recognized very clearly that
North was not an advocate of the innate harmony of interests as
the foundation of a free market social order. Had Dr. North be-
lieved in such a defense, he would never have adopted his views on
the necessity of the civil government’s enforcement of Biblical law.
But if Dr. Vickers recognized this, why does he argue the opposite?

There are three possible answers to this question. First, Dr.
Vickers chooses not to read carefully. In short, he is lazy. Second,
Dr. Vickers tries to read carefully, but he is unable to do it. In
short, Dr. Vickers is a mental incompetent. Third, Dr. Vickers
self-consciously parodied Dr. North, just as he self-consciously
parodied Adam Smith and the classical economists. In short, Dr.
Vickers is: 1) a lazy scholar, 2) a mental incompetent, or 3) a
kmave.

I doubt that he is lazy; he has not only read lots of insufferably
dull academic tomes, he has even gone to the trouble of writing
several of them. The turgidity of his writing style does indicate
mental incompetence, but his determined inability to state his
opponents’ position accurately, or even give a standard college
textbook account of their beliefs, indicates his self-conscious dis-
torting of rival viewpoints. What any C+ grade average upper
division economics major knows is not true, Dr. Vickers tries to
palm off as standard knowledge.

In short, the man cannot be trusted. Whether his weakness is
primarily intellectual or moral, the reader should decide for him-
self after finishing this book and then by rereading (if it seems
worth the effort) Dr. Vickers’ two “Christian” economics books.

Conclusion
There was indeed a God-designed harmony of interests prior
to the Fall. After the rebellion of man, the ground was cursed.
(Gen. 3:17-19), and other changes took place. The ethical goal of
the institutional reconstruction of society still exists. We are to
work to build a society in which the harmony of interests is @ogres-

20, Vickers, Economics and Man, p. 358.
