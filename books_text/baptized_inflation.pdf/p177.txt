156 Baptized Inflation

A similar situation exists with the government’s competing
bank. Since all interest rates are determined by government pol-
icy through the Reserve Bank, the competition is limited to ser-
vice and facilities. The Australian government has been able to
use the nation’s Post Offices as venue for Commonwealth Bank
customers to carry out limited banking transactions outside nor-
mal banking hours; therefore the competition has been strongly
biased in the government’s favor.

Competition? Obviously it is competition of a kind. But
whether it is “proof” that governments and private industry can
really compete on an open and equal basis is another question.
Dr. Vickers does not suggest that these examples are “proof,” but
Keynesians have a distinct tendency to use such comparisons to
support their thesis that governmental participation in the econ-
omy will have a beneficial effect. They are far less ready to em-
brace an alternative suggestion: that” government participation
causes many of the economic disharmonies currently exhibited in
all nations around the world.

Here we have further evidence of the contradictions inherent
in Dr. Vickers’ book and Keynesianism in general. Dr. Vickers
does not present a single cogent argument against the classical
economists, nor has he proven his assertion that a free market sys-
tem cannot work. His arguments are misleading, and to “refute” a
misstatement of someone’s position is not to refute the issue at all.
Dr. Vickers’ sole achievement is to lead the reader away from the
truth of the matter and to a distortion of those views with which he
holds little, if any, agreement. By using the device of misstate-
ment, Dr. Vickers avoids confronting the actual theories with
which he disagrees. As this is the case, there is every reason for
the reader to reconsider the claims of classical and neo-classical
economic theory.

Entrepreneurship

Supply does not “automatically” create its own demand (Say),
any more than demand “automatically” creates supply (Keynes).
Acting men plan for the future. In their capacity as producers,
