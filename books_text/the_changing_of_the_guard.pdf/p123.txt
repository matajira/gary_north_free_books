Prayer and Precept OL

The Nehemiah Model

The disciples could look back across the pages of history and
see how essential it was for world-changing action to be preceded
by world-changing prayer, They could look back to the example of
Nehemiah for instance:

So it was, when I heard these words, that I sat down and wept,
and mourned for many days; I was fasting and praying before the
God of heaven. And I said: “I pray, Lord God of heaven, O great
and awesome God, You who keep Your covenant and mercy with
those who love You and observe Your commandments, please let
Your ear be attentive and Your eyes open, that You may hear the
prayer of Your servant which I pray before You now, day and
night, for the children of Israel Your servants, and confess the sins
of the children of Israel which we have sinned against You. Both
my father’s house and I have sinned. We have acted very corruptly
against You, and have not kept the commandments, the statutes,
nor the ordinances which You commanded Your servant Moses.
Remember, I pray, the word that You commanded Your servant
Moses, saying, ‘If you are unfaithful, I will scatter you among the
nations, but if you return to Me, and keep My commandments
and do them, though some of you were cast out to the farthest part
of the heavens, yet I will gather them from there, and bring them
to the place which I have chosen as a dwelling for My name.’ Now
these are Your servants and Your people, whom You have redeemed
by Your great power, and by Your strong hand. O Lord, I pray,
please let Your eat be attentive to the prayer of Your servant, and to
the prayer of Your servants who desire to fear Your name; and let
Your servant prosper this day, I pray, and grant him mercy in the
sight of this man.” For I was the king’s cupbearer (Nehemiah 1:4-11).

Nehemiah really did something.

He prayed.

He prayed a prayer of confession. He prayed a prayer of con-
trition. Instead of immediately throwing his political weight
around, exercising his political clout, and calling in his political
favors, he prayed. Instead of charging boldly before the throne of
Artaxerxes, he humbled himself before the throne of God.

He prayed.
