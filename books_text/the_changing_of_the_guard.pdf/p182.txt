150 The Changing of the Guard

survey data might include pro-abortion or anti-abortion, pro-gun.
control or anti-gun control, pro-ERA or anti-ERA, pro-defense
spending or anti-defense spending.

So, for example, a Christian in politics could tell his personal
computer to pull out-the names of everyone who is pro-life, pro-
family, and has given at least three times in the past and has at-
tended at least one meeting where he spoke. The computer scans
the files in less than three seconds and produces the names of
everyone who fits this description.

POLSYS designer, Larry Pratt tells the following tale with
relish, “When I was in the Virginia Legislature, the State Board of
Health was committed to putting in a regulation that would have
used public funds to finance abortions. We figured that the vote
would be 7 to 2 or 8 to 1 in favor.

“We think the Board tipped off the pro-abortion forces in May
1980 that there would be public hearings in September. The oppo-
sition had almost five months to organize. I was a state assembly-
man, and I didn’t hear about the hearings until mid-August. But I
had POLSYS, and they didn’t.”

Pratt went down to his basement, went through his data base
of 9,000 names, and pulled out 7,500 pro-life people. The printer
cranked out the labels, and that evening he mailed a letter asking
the life advocates to write to the Board of Health, both as a group
and as individual members. He gave the date of the hearings.

The result? Within two weeks, the Board received 9,000 let-
ters, petitions, and telegrams. In five months the pro-abortion
forces, using traditional methods, had mustered only 2,000. In
addition, more than 500 pro-lifers showed up at the hearings,

“It was incredible,” Pratt says. “My letter didn’t mention the
Governor, but his desk was flooded with protests. He said later
that in two terms, this was one of the three top responses—‘spon-
taneous outpourings,’ he called them—he had received. Well,
maybe the other two were spontaneous. Ours looked spontaneous,
and, in one sense, it really was. I hadn’t told people to write to
him, But POLSYS made it look spontaneous. The opposition
never knew what hit them.”
