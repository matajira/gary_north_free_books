42

The Changing of the Guard

The founders of the American republic understood this essen-

tial calling and thus fully integrated politics. and faith,

The dangers we face as a nation would be greatly reduced if

only we had such men today.

You also, as living stones, are being built up as a spiritual
house for a holy priesthood, to offer up spiritual sacrifices accept-
able to God through Jesus Christ. For this is contained in Scrip-
ture: “Behold I lay in Zion a choice stone, a precious cornerstone,
and he who believes in Him shail not be disappointed.” This
precious value, then, is for you who believe. But for those who dis-
believe, “The stone which the builders rejected, this became the
very cornerstone,” and, ‘A stone of stumbling and a rock of
offence”; for they stumble because they are disobedient to the
word, and to this doom they were also appointed. But you are a
chosen race, a royal priesthood, a holy nation, a people for God's
own possession, that you may proclaim the excellencies of Him
who has called you out of darkness into His marvelous light; for
you once were not a people, but now you are the people of God;
you had not received mercy, but now you have received mercy
(1 Peter 2:5-10).

Let us then take up the mantle of our priesthood and guard

the land.
