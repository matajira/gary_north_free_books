188 The Changing of the Guard

Will the Federal budget change? If not, why not?
In short: Will conversion to Christ make a visible difference in
our civilization? If not, why not?

The Great Commission

What the Biblical Blueprints Series is attempting to do is to
outline what some of that visible difference in our culture ought to
be. The authors are attempting to set forth, in clear language, fun-
damental Biblical principles in numerous specific areas of life. The
authors are not content to speak in vague generalities. These
books not only set forth explicit principles that are found in the
Bible and derived from the Bible, they also offer specific practical
suggestions about what things need to be changed, and how
Christians can begin programs that will produce these many
changes.

The authors see the task of American Christians just as the
Puritans who came to North America in the 1630’s saw their task:
to establish a city on a hill (Matthew 5:14). The authors want to see a
Biblical reconstruction of the United States, so that it can serve as
an example to be followed all over the world. They believe that
God’s principles are tools of evangelism, to bring the nations to
Christ. The Bible promises us that these principles will produce
such good fruit that the whole world will marvel (Deuteronomy
4:5-8), When nations begin to marvel, they will begin to soften to
the message of the gospel. What the authors are calling for is com-
prehensive revival—a revival that will transform everything on
earth.

In other words, the authors are calling Christians to obey God
and take up the Great Commission: to disciple (discipline) all the
nations of the earth (Matthew 28:19).

What each author argues is that there are God-required prin-
ciples of thought and practice in areas that some people today be-
lieve to be outside the area of “religion.” What Christians should
know by now is that nothing lies outside religion. God is judging all
of our thoughts and acts, judging our institutions, and working
through human history to bring this world to a final judgment.
