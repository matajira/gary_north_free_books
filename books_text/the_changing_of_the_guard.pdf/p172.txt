140 The Changing of the Guard

Most Christian families fight abortion on the basis of “a right
to life.” We fight injustice on the basis of “a right to freedom.” And
we fight poverty on the basis of “a right to food.” In short, we have
established our movement on human rights.

‘The Bible does not guarantee human rights. At the time of the
fall, man lost all rights except the right to death (Romans 5:12).
“The wages of sin is death,” says Paul (Romans 6:23), and “All
men have sinned” (Romans 3:23), Thus, every second of life on
earth is the undeserved gift of God. Restraint of sin is found in
God's Law, not in some subjective theory of abstract rights.

What this means is that all protection, all justice, all compas-
sion, and all fairness are given to men on the basis of grace and an
adherence to the Word of God, not rights. The Bible does not
teach human rights as such: privilege apart from responsibility. It
teaches human blessing and protection stemming from grace-
provoked obedience.

As godly families we must fight for the oppressed, for the un-
born, for the hungry, for the poor, and for the dispossessed, but
not because of their rights. We must fight for them, not on the
basis of a human assertion of what is good and fair. We must fight
for life and liberty on the basis of the only absolute, the only
standard, the only immutable mandate that exists: the Word of
God. If we attempt to protect the unborn and the dispossessed
with rights, we will inevitably fail, for we have asserted no higher
value than that of the gay rights movement, the women’s rights
movement, or the abortion rights movement. But if we attempt to
protect the unborn and the dispossessed with an uncompromised
and unapologetic stand on the Word of God, we will inevitably
succeed, Justice is attained only when the Law of God is obeyed.

‘We cannot hope to win the battle for life if we continue to use
the same tactics as the humanists. All of their gains have come as
they have asserted the primacy of rights. Let’s not make the same
mistake, Instead of standing up for rights, it is time to stand up
for the Bible. Instead of claiming judicial immunity, it is time to
demonstrate to a fallen and corrupt culture that the only hope for
justice, mercy, order, and life is in the strict obedience to the Word
