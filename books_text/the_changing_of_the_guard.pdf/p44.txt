2 The Changing of the Guard

and that there will be no end to the increase of His government or
of peace (Isaiah 9:7). It freely confesses what Nebuchadnezzar
was forced to admit: “His dominion is an everlasting dominion,
and His kingdom endures from generation to generation” (Daniel
4:34), Christian political action is nothing more and nothing less
than the declaration: “Jesus is Lord”— ihe most basic of all Christian
declarations (Romans 10:9).

For a Christian to ignore the realm of politics is to ignore the
regency of Jesus Christ. It is to commit the foolish error of Nebuchad-
negzzar and thus to risk the open shame of Nebuchadnezzar.

Sadly, most twentieth century evangelical Christians have fol-
lowed in that Babylonian king’s ignoble footsteps. We have ig-
nored the fact that Christ bears the ensigns of the theocracy: a
crown of glory and honor (Hebrews 2:9), a sword of truth and jus-
tice (Revelation 1:16; 2:16), a scepter of righteousness and authority
(Hebrews 1:8), a crest of promise and consolation (Revelation 5:5),
a name of prerogative and transcendence (Philippians 2:9-10), and
a title deed of absoluteness and finality (Matthew 11:29). We have
acted as if the Lordship of Christ were not the least bit real. We
have acted like Nebuchadnezzar, As a result, we have, like Neb-
uchadnezzar, suffered utter humiliation. The enemies of the Gos-
pel scoff and ridicule. They persecute and antagonize. They run
roughshod over the weak and helpless, They pollute the land with
abomination and desecration. And all the while, we are scuttled
off to pasture like so many bovine indigents (Daniel 4:33). All for
the lack of a simple declaration that Christ is the Lord over the to-
tality of life—that God rules.

Of course, Nebuchadnezzar learned Ais lesson. His humilia-
tion convinced him beyond any shadow of a doubt that it is God
who “removes kings and establishes kings” (Daniel 2:21); it is God
who appoints over the land whomsoever He will (Daniel 5:21); it
is God who rules on earth as He does in heaven (Psalm 110:1-3).
Nebuchadnezzar learned his lesson and changed his heart and his
politics. It remains to be seen if we will learn our lesson. It remains
to be seen if we will translate our humiliation into a confession of
Christ’s exaltation to the throne of authority and rule.
