130 The Changing of the Guard

And He Himself gave some to be apostles, some prophets,
some evangelisis, and some pastors and teachers, for the equip-
Ping of the saints for the work of the ministry, for the edifying of
the body of Christ, till we all come to the unity of the faith and the
knowledge of the Son of God, to a perfect man, to the measure of
the stature of the fullness of Christ (Ephesians 4:11-13).

The Church is to train God’s people for the work of the minis-
try. If our nation is to have thoroughly equipped pastors, then the
Church must train young men for the ministry of the Gospel
(Romans 10:14-15). If our nation is to have thoroughly equipped
teachers, then the Church must train young mothers and fathers
for the ministry of education (Titus 2:1-15). If our nation is to have
thoroughly equipped craftsmen, artists, musicians, philosophers,
doctors, laborers, lawyers, scientists, and merchants, then the
Church must train them for the ministry of acculturation (2 Tim-
othy 3:16-17). And of course, if our nation is to have thoroughly
equipped magistrates, then the Church must train them for the
ministry of political action.

That does not mean that the Church must be the central de-
pository for every discipline and specialty on the face of the earth
—a true university. But it does mean that the Church must apply
the Bible to every discipline and specialty on the face of the earth,
training believers to exercise their various callings to the glory of
God.

Now there are diversities of gifts, but the same Spirit. There
are differences of ministries, but the same Lord. And there are di-
versities of activities, but it is the same God who works all in all.
But the manifestation of the Spirit is given to each one for the
profit of all (1 Corinthians 12:4-7, 12).

The Church should train young men and women for political
action through information: distributing facts and figures on the
central issues and candidates of the day, issuing educational liter-
ature, holding seminars, rallies, and workshops, etc.

The Church should train young men and women for political
action through activism: picketing the death clinics of abortion-
