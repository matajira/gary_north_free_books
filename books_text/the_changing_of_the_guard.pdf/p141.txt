CONCLUSION

“The Biblical concept of the covenant,” according to Gary
North, “is the Bible’s most important doctrine relating to the rela-
tionship between God and man.” This is because the covenant is
the pattern for that relationship,

God’s dealings with us are always covenantal. He judges us
covenantally. He comforts us covenantally. He fellowships with us
covenantally. He disciplines us, rewards us, and cares for us cove-
nantally.

James B. Jordan describes the covenant as “the personal,
binding, structural relationship among the Persons of God and
His people. The covenant, thus, is a social structure.” It is the
divine-to-human/human-to-divine/human-to-human social struc-
ture. It is the means by which we approach, deal with, and know
God and one another. It is the pattern of our relationship and our
relationships.

According to Scripture, the covenant has at least five basic
component parts. It begins with the establishment of God’s nature
and character: He is sovereign. Second, it proclaims God’s au-
thority over His people: He has established order and structure.
Third, the covenant outlines His stipulations and ethics: the peo-
ple have responsibility and responsibilities. Fourth, it establishes
God's judicial see: He will evaluate and judge His people's work.
And, finally, the covenant details God’s very great and precious
promises: the people have a future, an inheritance.

This outline of the covenant can be seen, in at least an oblique
fashion, in God’s dealings with Adam (Genesis 1:26-31; 2:16-25),

109
