160 The Changing of the Guard

your money in the future by giving to godly candidates. Attend
rallies. Work at the polls. Just get involved.

Initiating activism:

You should make your faith visible. Let your testimony be
clear to all, Write letters. Make resolutions. Compile a mailing
list. Spend some time in the picket lines at a local abortion clinic.
Participate in a home schoolers’ rally. Support a local baby rescue.
Use alternative resistance tactics,

Whatever you do, do all for the glory of God, under the au-
thority of the Word.

Whatever you do, do all in light of the ten basic principles of
the Biblical blueprint for political action.

Whatever you do, do if now. Only then will we witness a
changing of the guard.
