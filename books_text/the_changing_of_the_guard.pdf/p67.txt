IV. Judgment/Sanctions

4
SINS OF OMISSION

Then the men rose from there and looked toward Sodom, and
Abraham went with them.to send them on the way, And the Lord
said, “Shall I hide from Abraham what I am doing, since Abraham
shall surely become a great and mighty nation, and all the nations
of the earth shall be blessed in him? For I have known him, in.
order that he may command his children and his household after
him, that they keep the way of the Lord, to do righteousness and
justice, that the Lord may bring to Abraham what He has spoken
to him.” And the Lord said, “Because the outcry against Sodom
and Gomorrah is great, and because their sin is very grievous, I
will go down now and see whether they have done altogether accord-
ing to the outcry against it that has come to Me; and if not, I will
know.” Then the men turned away from there and went toward
Sodom, but Abraham still stood before the Lord (Genesis 18:16-22).

The cities of Sodom and Gomorrah were consumed with vile
and detestable sin. The people were perverse (Genesis 19:5). They
were violent (Genesis 19:9), They were arrogant, careless, and self-
ish (Ezekiel 16:49). They were haughty, self-destructive, and abom-
inable (Ezekiel 16:50). They were utterly wicked (Genesis 18:20).

But that is not why God destroyed them.

Despite all the debauchery, lasciviousness, and blasphemy,
God was willing to spare the cities,

And Abraham came near and said, “Wilt Thou indeed sweep
away the righteous with the wicked? Suppose there are fifty right-
eous within the city; wilt Thou indeed sweep it away and not spare
the place for the sake of the fifty righteous who are in it? Far be it

35
