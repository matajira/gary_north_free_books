For Further Reading 163

—_______.. This Independent Republic (Tyler, Texas: Thoburn
Press, 1978),

Schaeffer, Francis, A Christian Manifesto (Westchester, Illinois:
Crossway Books, 1981).

Phyllis Schlafly ed., Child Abuse in the Classroom (Westchester,
Illinois: Crossway Books, 1984).

Ray Sutton, That You May Prosper: Dominion By Covenant (Fort
Worth, Texas: Dominion Press, 1987).

ee» Who Owns the Family? (Fort Worth, Texas:
Dominion Press, 1986).

Robert Thoburn, The Christian and Politics (Tyler, Texas: Thoburn
Press, 1984).

Henry Van Til, The Caloinistic Concept of Culture (Philadelphia:
Presbyterian and Reformed, 1959).

Peter Waldron with George Grant, Rebuilding the Walls: A Biblical
Strategy for Restoring America’s Greatness (Brentwood, Tennessee:
Wolgemuth and Hyatt Publishers, 1987).

E. C. Wines, The Hebrew Republic (Uxbridge, Massachusetts:
American Presbyterian Press, 1980).
