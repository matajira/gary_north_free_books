16 The Changing of the Guard

throned on high. We need only to recognize His Lordship in word
and in deed.

Christian political action then is simply submitting to the au-
thority of God in every sphere of life through the application of

" His Word in every institution in life. It is a confession that “the
earth is the Lord’s and everything in it, the world and all who live
in it? (Psalm 24:1).

Nebuchadnezzar had to learn the hard way that God will not
tolerate any other perspective. But there is no need for us to share
in his shame and humiliation.

Our republic was built on an understanding of God’s rule and
all the liberties we enjoy can be traced directly to the foundation
that that understanding laid.

If we are to have any hope of preserving our civilization from
the ravages of sin and destruction, we must once again affirm the
Lordship of Christ. We must root our political activity in the rule
and reign of God Almighty.

Summary

Nebuchadnezzar, the greatest of all the ancient kings, was
deposed by God Himself to demonstrate that He alone is the Most
High Ruler of all things.

God, in fact, sovereignly rules the entire cosmos: the forces of
creation, the course of history, the hearts and minds and ways of
men, and the.nations of the earth.

Christian political action is therefore an acknowledgement of
the theocracy of heaven and earth, It is the simple declaration that
“Jesus is Lord.”

This declaration of Christ’s authority became the basis of the
American political system.

And it must be the basis of our political efforts as we struggle
against a Nebuchadnezzar-like messianic state.

We must once again sound the clarion call of liberty and jus-
tice: “Our God reigns.”
