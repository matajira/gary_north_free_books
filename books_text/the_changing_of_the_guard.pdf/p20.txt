xx The Changing of the Guard

tics itself is not central. The worship of God is central. The central
issue therefore is this: Which God should mankind worship? The
God of the Bible or a god of man’s imagination?

The humanists see the state as man’s most powerful institu-
tion, and therefore politics is man’s most important activity.
Their’s is a religion of power, so they make the state the central in-
stitution. They make the State their Church.

Because the humanists have made the state into their agency
of earthly salvation, from the ancient Near Eastern empires to the
Greeks to Rome’s Empire’ and to the present, Christians need to
focus on this battlefield, but we must always remember that poli-
tical battles are important today primarily because our theological
opponents have chosen ta make their first and last stand on the political bat-
uefield. Had they chosen to fight elsewhere, it would not appear as
though we are hypnotized with the importance of politics. Chris-
tian Reconstructionists are not hypnotized by politics; humanists
and piestists are hypnotized by politics. Nevertheless, we are will-
ing to fight the enemy theologically on his chosen ground, for we
are confident that God rules every area of life. He can and will
defeat them in the mountains or on the plains (1 Kings 20:28), in
politics and in education, in family and in business.

What Christians should say in response to humanism’s poli-
tical theology is that God’s Church, as the institution entrusted by God
with His Word and His sacraments, is the central institution of history.
The Bible teaches that the gates of hell will not stand against the
onslaught of the Church. The Church will be in heaven above and
on the eternal earth after the final judgment. Neither the state nor
the family can legitimately make such a claim. Thus, Christians
must work to reform the Church even more diligently than they
should work to reform politics. If the institutional Church is theo-
logically and morally corrupt, then Christians will lose on the
other battlefields.

Only recently have both humanists and Christians begun to

5. R. J. Rushdoony, The One and the Many: Studies in the Philosophy of Order and
Ultimacy Fairfax, Virginia: Thoburn Press, [1971] 1978), chaps. 3-5.
