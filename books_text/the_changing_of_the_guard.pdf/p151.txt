il
WHAT THE CHURCH SHOULD DO

According to God’s design, Church and state are separate insti-
tutions. They have separate jurisdictions. They have separate au-
thorities, They have separate functions.

A Biblical social order depends on this kind of institutional
differentiation, When any one institution begins to encroach upon.
another, chaos and tyranny inevitably result. God’s system of
checks and balances begins to break down.

The state must not meddle in the affairs of the Church. That
is not its God-ordained concern. The Church is outside of the
state’s jurisdiction. The framers of the American Constitution
recognized this fundamental plank of liberty and thus the first ar-
ticle in the Bill of Rights states, “Congress shall make no law
respecting an establishment of religion, or prohibiting the free ex-
ercise thereof.” The state has no authority over the Church and
therefore must not regulate or interfere in the work of the Church.
Local municipalities and even individual commonwealths might
render support to the Church (as often they did), but never were
they to control the Church. Church and state are separate.

Likewise, the Church must not meddle in the affairs of the
state. That is not its God-ordained concern. The state is outside
of the Church’s jurisdiction. Again, the framers of the American
Constitution wanted to protect their fledgling nation from any
and all tyrannies: both statism (in the form of imperialism, social-
ism, or democracy) and oligarchy (in the form of caesaro-papism,
agathism, or ecclesiocracy). ‘The Church has no authority over
the state and therefore must not regulate or interfere in the work
of the state. They are separate.

n19
