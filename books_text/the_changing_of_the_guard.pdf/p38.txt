6 The Changing of the Guard

political actions be recommended (Deuteronomy 15:4-8). Only
then can strategies be outlined, tactics designed, and programs in-
itiated (Joshua 1:8).

That is where Part II comes in. There specific steps of action
are outlined for churches, families, and individuals who, having
rediscovered the world, wish to change it— Biblically.

Christian philosopher Cornelius Van Til has stated, “The
Bible is authoritative on everything of which it speaks. And it
speaks of everything.” Even of such “mundane” matters as poli-
tical action. Thus, to evoke Scripture’s blueprint for our cosmo-
politan culture’s complex dilemmas is not some naive resurrection
of musty, dusty archaisms. “More than that, blessed are those who
hear the Word of God, and keep it” (Luke 11:28), For, “the Scrip-
ture cannot be broken” (John 10:35).
