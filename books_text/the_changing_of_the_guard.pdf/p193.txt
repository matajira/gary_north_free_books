FOR FURTHER READING

Bring the cloak that I left with Carpus at Troas when you
come—and the books, especially the parchments (2 Timothy 4:13).

Walter Brueggemann, The Land (London: SPCK, 1978).

Lynn Buzzard and Paula Campbell, Holy Disobedience: When Chris-
tians Must Resist the State (Ann Arbor, Michigan: Servant Books,
1984).

David Chilton, Paradise Restored: An Eschatology of Dominion (Tyler,
Texas: Institute for Christian Economics, 1985).

. Productioe Christians in an Age of Guilt-Manipulators,
3rd rev. ed. (Tyler, Texas: Institute for Christian Economics,
1985).

Gary DeMar, God and Government: A Biblical and Historical Study
(Atlanta, Georgia: American Vision Press, 1982).

——______. God and Government: Issues in Biblical Perspective
(Atlanta, Georgia: American Vision Press, 1984).

. God and Government: The Restoration of the Republic
(Atlanta, Georgia: American Vision Press, 1986).

———_____- Ruler of the Nations: The Biblical Blueprint for Civil
Government (Fort Worth, Texas: Dominion Press, 1987).

George Grant, Bringing in the Sheaves: Transforming Poverty inte Pro-
ductivity (Atlanta, Georgia: American Vision Press, 1985).

. The Dispossessed: Homelessness in America (West-
chester, Illinois; Crossway Books, 1986).

—___.. In the Shadow of Plenty: Biblical Principles of Welfare
and Poverty (Fort Worth, Texas: Dominion Press, 1986).

161

 
