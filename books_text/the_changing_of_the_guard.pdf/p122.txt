IV. Judgment/Sanctions

9
PRAYER AND PRECEPT

And it came to pass, as He was praying in a certain place,
when He ceased, that one of His disciples said to Him, “Lord,
teach us to pray, as John also taught his disciples.” So He said to
them, “When you pray, say: ‘Our Father in heaven, hallowed be
Thy name. Thy kingdom come. Thy will be done on earth as it is.
in heaven. Give us day by day our daily bread. And forgive us our
trespasses, as we forgive those who trespass against us. And lead us
not into temptation, but deliver us from evil’” (Luke 11:1-4).

Christ's disciples were to be world-changers. They were to take
the Gospel to every corner of the globe (Acts 1:8). They were to
convert and disciple the nations (Matthew 28:19-20). They were
to throw down vanities, captivate minds, and demonstrate true
piety (2 Corinthians 10:3-6). They were to be salt and light (Mat-
thew 5:13-16), occupying the land for Christ (Luke 19:13).

They were, in short, to be “doers of the Word, and not hearers
only” (James 1:22). They were to take action.

But the first action they were to take was to pray.

Before they established political agendas, called for caucuses
and conventions, rallied the forces, or deluged the ballot boxes,
they were to pray.

They were to work for the coming of the Kingdom. They were
to see that God’s will was done on earth. But— they were supposed
to start at the start.

And the start is prayer.

It always has been. It always will be.

90
