54 The Changing of the Guard

judge, you will be judged; and with the same measure you use, it
will be measured back to you” (Matthew 7:1-2),

So, what were the terms of judgment? What were the Biblical
prerequisites for exercising the ministry of political action? What
political-standard did the men have to adhere to?

God-Fearers

First, they had to adhere to the Biblical standard of godliness.
They had 40 be men who feared God (Exodus 18:21). According to
Jethro, this is an inescapable prerequisite. After all:

‘The fear of the Lord is the beginning of wisdom; a good under-
standing have all those who do His commandments. His praise en-
dures forever (Psalm 111:10).

The fear of the Lord is the beginning of knowledge, bus fools
despise wisdom and instruction (Proverbs 1:7),

The fear of the Lord prolongs days, but the years of the wicked
will be shortened (Proverbs 10:27).

In the fear of the Lord there is strong confidence, and His chil-
dren will have a place of refuge. The fear of the Lord is a fountain
of life, to avoid the snares of death (Proverbs 14:26-27).

Better is a little with the fear of the Lord, than great treasure
with trouble (Proverbs 15:16).

A nation whose leaders fear God will suffer no want (Psalm
34:9). It will ever be blest (Psalm 115:13). It will be set high above
all the nations of the earth (Deuteronomy 28:1). Ancient Israel’s
greatness can be directly attributed to her leaders’ fear of God:
Abraham was a God-fearer (Genesis 20:11); Joseph was a God-
fearer (Deuteronomy 10:12); as were Job (Job 41:23); Joshua
(Joshua 24:14); David (2 Samuel 23:3); Jehoshaphat (2 Chroni-
cles 19:4); Hezekiah (Jeremiah 26:19); Nehemiah (Nehemiah
5:15); and Jonah (Jonah 1:9).

Clearly, those who exercised the ministry of political action in
Israel would likewise have to be God-fearers.
