56 The Changing of the Guard

(2 Thessalonians 2:12-13), obeyed (Romans 2:8), loved (2 Thessa-
lonians 2:10), and manifested (2 Corinthians 4:2),

Clearly, those who exercised the ministry of political action in
Israel would not only have to fear God, they would have to be
scrupulously honest as well.

Selfiess Servants

Third, they had to adhere to the Biblical standard of selflessness.
They had to be men of impeccability, hating covetousness and dis-
honest gain (Exodus 18:21). According to Jethro, this too is an in-
escapable prerequisite, After all;

Yes, they are greedy gods which never have enough. And they are
shepherds who cannot understand; they all look to their own way,
every one for his own gain, from his own territory (Isaiah 56:11),

I, the Lord, search the heart, I test the mind, even to give
every man according to his ways, and according to the fruit of his
doings. As a partridge that broods but does not hatch, so is he who
gets riches, but not by right; it will leave him in the midst of his
days, and at his end he will be a fool (Jeremiah 17:10-11).

Behold the proud, his soul is not upright in him; but the just
shal} live by his faith. Indeed, because he transgresses by wine, he
is a proud man, and he does not stay at home. Because he enlarges
his desire as hell, and he is like death, and cannot be satisfied, he
gathers to himself all nations and heaps up for himself all peoples.
Shall not all these take up a proverb against him, and a taunting
riddle against him, and say, “Woe to him who increases what is not
his— how long? And to him who loads himself with many pledges?”
(Habakkuk 2:4-6).

For what is 2 man profited if he gains the whole world, and

loses his own soul? Or what will a man give in exchange for his
soul? (Matthew 16:26).

Covetous leaders often brought calamity upon the entire
nation of Israel: Balaam, in loving the wages of unrighteousness
(2 Peter 2:15); Achan, in hiding the contraband treasure of Jericho
(Joshua 7:21); Eli’s sons, in taking the flesh of the sacrifice (1 Sam-
uel 8:3); Saul, in sparing Agag and the booty of war (1 Samuel
