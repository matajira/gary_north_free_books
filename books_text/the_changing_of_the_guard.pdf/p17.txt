Editor's Introduction xvii

This denial of the Kingdom aspects of Christ’s personal reign
in history is clearly preposterous. ] know of no other dispensation-
alist author who has ever argued such a thing. Why is it prepos-
terous? Because in Isaiah 65, we are told that the New Heaven
and New Earth will become manifested before the final judgment,
for sinners will still live and die among the saints (Isaiah 65:20).
We know that sinners will not be operating beyond the day of
judgment. Thus, there are preliminary manifestations of the New
Heaven and New Earth in history. Second, we are also told con-
cerning Christ and His Kingdom: “Then comes the end, when He
delivers the Kingdom to God the Father, when He puts an end to
all rule and all authority and power” (1 Corinthians 15:24). Third,
we are told that Christ will deliver up His Kingdom to God the
Father only after death has been conquered at the final judgment
(1 Corinthians 15:25-27). Yet the best-selling spokesman of funda-
mentalist theology in the mid-1980s rejects all of this in a desper-
ate, futile attempt to internalize every aspect of Christ’s Kingdom,
in order to persuade his readers that they have no God-assigned
Kingdom responsibilities to pursue,

This denial of any aspect of God’s Kingdom in history repre-
sents @ radical rejection of civic and cultural responsibility —a rejection
which is the psychological (if not theological) culmination of tradi-
tional dispensational theology. Even though a handful of dispen-
sational social activists would no doubt reject his conclusions, he
does speak for the millions of dispensationalists in the pews who
have made him rich and also for most of those in dispensational
pulpits. Note also that the leading dispensational institutions have
not publicly rejected his spokesmanship of their theological posi-
tion. Perhaps they are afraid to challenge him, or they may ac-
tually agree with him. In either case, this silence in the face of
radical cultural retreatism marks the last gasp of traditional dis-
pensationalism; it can no longer be taken seriously as a relevant
theological system, ‘The dispensational movement has at last pub-
licly run up the white flag of surrender to the secular humanists.

The divisive issues of eschatology and Biblical law can no
longer be contained by the pietists who lead the evangelical com-
