Cavetousness and Conflict 201

unscrupulous. There will be jobs to be done about the badness of which
taken by themselves nobody has any doubt, but which have to be executed
with the same expertness and efficiency as any others. And as there will be
need for actions which are bad in themselves, and which all those still
influenced by traditional morals will be reluctant to perform, the readiness
to do bad things becomes a part to promotion and power. The positions ina
totalitarian society in which it is mecessary to practice cruelty and intimida-
tion, deliberate deception and spying, are numerous. Neither the Gestapo
nor the administration of a concentration camp, neither the Ministry of
Propaganda nor the §.A. or S.S. (or their Italian or Russian counterparts),
are suitable places for the exercise of humanitarian feelings. Yet it is
through positions like these that the road to the highest positions in the
totalitarian state leads.*

Hayek’s book was intended to demonstrate how totalitarian
societies develop out of the attempt of socialist planners to mold the
economy into a centrally directed framework. He argued that in
theory, nothing must deviate from the central economic plan, since
human freedom will thwart any such plan. Thus, the power to
redistribute wealth in accordance to some preconceived statist pro-
gram eventually destroys human freedom and therefore thwarts per-
sonal responsibility to act as a steward under God. Covetousness, when
legislated, becomes a major foundation of totalitarianism.

Hayek’s little book evoked outraged cries of “foul!” from the
statist intellectuals when it first appeared. Herbert Finer’s Road to
Reaction is perhaps the best example. But year by year, decade by
decade, The Road to Serfdom has grown in stature, until il is now con-
sidered a classic. Jt stays in print, and has served as the financial
backbone of the University of Chicago Press’ paperback division.
‘Twentieth-century voters and politicians have not yet been blessed
with the moral courage to act in terms of Hayek’s arguments against
the centrally planned economy, but those intellectuals who even
bother to worry about the problem of human liberty in relation to
the economy have steadily begun to take Hayek seriously on this
point. Four decades after the Road to Serfdom first appeared, the hu-
manists who write scholarly books have at last begun to catch up
with the wisdom of the average book buyer who made Road to Serfdom
a bestseller in 1944. (It even appeared in the Reader’s Digest in 1944 as
a condensed book.)

5. FA, Hayek, The Road to Safdom (University of Chicago Press, 1944), pp.
150-51,
