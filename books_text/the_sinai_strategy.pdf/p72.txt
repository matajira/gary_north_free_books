46 THE SINAI STRATEGY

have long life and not suffer the judgment of Satan’s kingdom.

The comparative growth rates are, of course, symbolic. Egypt’s
case was literal, and the Hebrews should have recognized the power
of God to bring His word to pass. Nevertheless, some pagan soci-
eties have gone on in their rebellion far longer than four generations.
The Roman Empire is one historical example, although the Pax
Romana lasted less than two centuries before the Empire began to
be subjected to major crises. The point is, compared to the long-term
growth of God’s kingdom, in time and on earth as weil as beyond the grave,
Satan's earthly kingdoms ave short-lived. The mercy which God shows to
pagan kingdoms by not bringing judgment on them the moment
they transgress His Jaw is ultimately a form of judgment. They
receive common grace, meaning an unmerited and temporary gift of
an extension of time without judgment, but this only increases the
magnitude of the eventual wrath of God.

We should not expect to see Satan’s kingdom cut down overnight
in the future, after having attained a position of universal dominion.
The process of growth for Satan’s kingdom is not continuous. The
“negative feedback” phenomenon of external judgment repeatedly
cuts back the growth of Satan’s external dominion long before it
achieves worldwide dominion. These verses point to a far different
future: the steady growth of Christ’s kingdom as the deaven of righteous-
ness overwhelms and replaces thc God-hindered leaven of Satan's
kingdom.3?

The Gambler

Satan’s kingdom does manifest itself intermittently during tem-
porary periods of exceedingly rapid growth, but this growth cannot
be sustained for “a thousand generations.” The growth rate of Satan’s
kingdom is the growth rate of the gambler who has a string of suc-
cessful bets, or the highly leveraged (indebted) investor who predicts
the market accurately for a time and multiplies his wealth with bor-
rowed money. Such growth is rapid, but it cannot be sustained. It is
the growth rate of a person who has limited time, and who must
make his fortune in onc lifetime. He requires rapid growth, for he
has no faith in long-term growth over many generations, The com-
pound growth rate must be high, and it must be rapid, for it will not
last for long.

32, Gary North, Unconditional Surrender, pp. 183-92.
