Graven Images and Compound Judgment 4

neth, it shail die. The son shall not bear the iniquity of the father,
neither shall the father bear the iniquity of the son: the righteousness
of the righteous shall be upon him, and the wickedness of the wicked
shall be upon him” (Ezk. 18:20), We therefore must interpret the uni-
que phrase, “visiting the iniquity of the fathers upon the children,” in
terms of this clearly stated principle of judgment.

What we have in view here is a covenantal framework of refer-
ence. The Hebrews had just come out of Egypt. They and their an-
cestors had labored under slavery. The year of release had not been
honored by their captors. Year after year, the Egyptians had built up
their cities by the use of Hebrew labor. This capital base kept ex-
panding. The wages that would have been paid to free laborers, as
well as the capital that was to be given to slaves in the year of release
(Deut. 15:13-14), was retained by succeeding generations of Egypt-
ians. Thus, the later generations became the beneficiaries of the
compounding process.?5 They were richer, they supposed, than their
ancestors because they possessed the visible manifestations of labor
extracted illegally over decades.

Then came God’s judgment. With the compound growth of the
visible benefits came the compound judgment of God, Both had
built up over time. The final generation suffered incomparable judg-
ment because they had not repented, made restitution voluntarily,
and freed the Hebrews. For God not to have judged that final gener-
ation in terms of the benefits they had received illegally — benefits
conveyed to them as a continuing legacy from their ancestors—
would have been an injustice on the part of God.

Repeated Iniquities

The iniquities of the fathers were repeated by the sons. The
fathers escaped the full temporal retribution of God. In this sense,
God showed them mercy, in time and on earth. But the sons also did
not repent. They continued in the sins of their fathers. If anything,
they enjoyed the luxury of sinning even more flagrantly, because
they were the beneficiaries of a larger capital base —a capital base of
evil.

How long will God allow the sins of the heirs to go on? Unto the
third and fourth generation. How long had the Hebrews been under

 

Gary North, Moses and Pharaoh, ch. 6: “Cumulative Transgression and Resti-
tution.”
