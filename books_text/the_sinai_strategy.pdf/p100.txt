74 THE SINAI STRATEGY

pass final judgment against him, Adam attempted to render autono-
mous judgment. By asserting such autonomy, he thereby rendered
Judgment against God’s word and in. favor of the serpent’s an-
nounced estimation of the low or zero likelihood of God’s punish-
ment for Adam’s disobedience.

Adam and Eve could have refused to accept Satan’s evaluation of
the effects of eating from the tree. They could have waited for Gad to
return and pass judgment against the intruder, and then sit down
with them to eat of the tree of life. This communion meal with God
was postponed by their rebellion and their subsequent ejection from
the garden. The celebration of Passover and the Lord’s Supper
points to a future meal with God after He pronounces final judgment
against sin and Satan’s forces, but it also points back to the “meal
that might have been.”

Rest: God's and Man's

Adam’s rebellion was linked to the question of the sabbath. God
had created the world, including Adam, in six days, and He rested
the seventh day, The sabbath day was man’s first full day of life. This
day began with rest, since God’s original creation activity had ended
the day before. Man was the capstone of God’s creation, the final
species to be created, but he was nevertheless under God’s sover-
eignty as a creature. The whole creation, except for one tree, had
been delivered into Adam’s hand. The day after the sabbath, the
“eighth day,” meaning the cighth day after God first announced, “Let
there be light,” Adam was to have gone forth to subdue the carth as
God's subordinate.

Rest means something different for God than it means for man.
God rested on the seventh day, after His work was over, and after He
had pronounced judgment on it, announcing its inherent goodness.
For God, rest is 2 testimony of His absolute independence. He cre-
ated the world out of nothing, It is dependent on Him; He is in no
way dependent on it. For creatures, on the other hand, rest means
subordination. Rest means that God is absolutely sovereign, and that
man is absolutely dependent on God. Man begins with rest, for he is
subordinate. God ended with rest, for He is absolutely sovereign.

Adam did not rest in his position of dependence under God. To
have accepted the. first day of the week as God’s gift of rest, to have
admitted that the creation was finished, would have meant the ac-
ceptance of man’s perpetual position as a re-creative sovercign, not an
