Sabbath and Dominion 89

Jerusalem in 70 4.p,18 The old humanism has bet a great deal on the
inauguration of a humanist millennium, the dream of a humanist
New World Order. So has New Age humanism.!® When these
dreams do not come true, and the premillennial expectations of fun-
damentalists concerning the imminent Rapture also do not come
true, men will again begin to ask: “What must we do now?” Francis
Schaeffer's title, How Shall We Then Live?, will be the religious ques-
tion of the next two decades.

The millennium as such is not limited to a thousand years. The
millennium as millennium began with the fall of Jerusalem -— the end
of the old order. The interim transitional period was called “the last
days.” It could extend well into the “eighth day,” or eighth millen-
nium, which will begin sometime around the year 3001. But there
could very easily be a specific manifestation of the millennium as well as
the general manifestation. The specific manifestation would be ex-
actly what Revelation 20 refers to, a thousand years in which Satan
will be uniquely chained up and rendered civilizationally impotent.
It would be most fitting if this full-blown restraining period would
begin in precisely the period that the humanists and occultists have
looked to for centuries as the beginning of their millennial reign.

There are several theological problems with predicting the in-
auguration of the long-awaited sabbath millennium sometime
around the year 2000. One is that the world may be older than 6,000
years. Another is that there could be a period of “Babylonian captiv-
ity” in between the year 2000 and the full manifestation of victory —a
70-year period comparable to the era between the birth of Christ and
the fall of Jerusalem. A third problem is that the symbolism of the
week, however convenient, really has no clear temporal manifesta-
tion in terms of millennia.2! Symbolic convenience, after all, is not
necessarily the focal point of God's work in time. But the most im-
portant problem will appear if, in fact, a great period of worldwide
conversions does begin close to the year 2000 (or 2070). This is the
subsequent risk of prophesying the advent of the “eighth day” — the
second coming—close to the year 3000 (or 3070). But the attempt to

18. David Chilton, Paradise Restored: A Biblical Theology of Dominion (Tyler, Texas:
Reconstruction Press, 1985).

19. Alberto Villoldo and Kenneth Dychiwald, Millennium: Glimpses into the 2ist
Century (Los Angeles: Tarchcr, 1981).

20. Chilton, Paradise Restored, ch. 13.

21. This, by the way, is James Jordan's opinion: Geneva Review, No. 18 (March
1985), p. 1.
