The Rights of Private Property 165

payment. The owner who says, “I'll use it my way,” is saying, “Fil
pay for my decision.” He tums his back on the money or goods
offeréd by consumers for his property.

Thus, ownership is a social function, Men must act as steivards for
the consuming public, or else pay the price. There is no such thing as a
free (gratuitous) lunch. There is also no such thing as cost-free own-
ership of scatce economic resources. The existence of free markets ~
institutional arrangements for open, competitive bidding — enables
consumers to price all economic assets according to their subjective
evaluations. Free markets lead to accurate. objective evaluations
(prices) of the. collective decisions of potential buyers. Free markets
aid consumers in establishing their will over producers. Producers
are free agents, but they are not cost-free agents.

There are two ways to impose your will on another person: re-
ward and penalty, the carrot and the stick. The stick relies on coer-
cion, Coercion is a legal monopoly of the civil government. Thus,
consumers are.to rely on the carrot approach. “Do it my way,” they
assert, “or suffer the consequences.” What are the consequences?
Forfeited income.

The market is not some autonomous institution which thwarts the
“little guy.” It is an institution which promotes the interests of every
asset-owning participant. It provides consumers with the ultimate in-
stitutional carrot: a legal order which allows them to make competi-
tive bids to the owners of the resources that they want to buy. The
market is a social institution which places daily inescapable burdens of
ownership on every resource owner. As Mises writes: “Ownership of
the means of production is not a privilege, but a social liability. Capi-
talists and landowners are compelled to employ their property for the
best possible satisfaction of the consumers. If they are slow and inept
in the. performance of their duties, they are penalized by losses. If
they do not learn their lesson and do not reform their conduct of
affairs, they lose their wealth, No investment is safe forever. He who
does not use his property in serving the consumers in the most effi-
cient way is doomed to failure. There is no room left for people who
would like to enjoy their fortunes in idleness and thoughtlessness.””

38, North, Dominion Covmant: Gmesis, ch. 23: “The Entrepreneurial Function.”

39. Gury North, An Iniroduction to Christian Economies (Nutley, New Jersey: Craig
Press, 1973), ch. 28: “Ownership: Free But Not Cheap.”

40. Ludwig von Mises, Human Action: A Treatise on Economics (3rd ed.; Chicago:
Regnery, 1966), pp. 30-12.
