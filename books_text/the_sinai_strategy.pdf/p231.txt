Covetousness and Conflict 205

than any other excludes us from his fellowship.”® It is clear that
Hodge regarded envy as the most dangerous of all the sins. Zf was this
kind of preaching, generation after generation, which made possible the eco-
nomic development of the Protestant West. It is the absence of such preach-
ing in the twentieth century that has damaged the economic institu-
tions of Western capitalism — the source of the West’s productivity.

Hoeksema also identified covetousness as discontent. “The sin of
covetousness is the desire to possess anything apart from God,
against His will; anything that he does not give me and ‘that evi-
dently He does not want me to have. . . . If the sin of covetousness
could be rooted out of society, most of our economic problems would
be solved. Covetousness is the root of all the sinful unrest in society.
The same is true of international life and relationships; if the sin of
covetousness were not so deeply rooted in the heart of the depraved
man, most, wars, if not all, would be eliminated. ‘Take covetousness
away, and there would be no reason for men to fly at one another's
throats, and you could hardly conceive of the possibility of war. .
Positively, this means, of course, that the tenth commandment en-
joins us to be content with what we have: Christian contentment is
perfect satisfaction with what one has, for the sake of God in Christ
Jesus our Lord, and that, too, in the midst of a corrupt and covetous
world.”9

Hoeksema is correct: discontent is the heart of sin’s problem, be-
ginning with Satan’s discontent with God’s sovereignty. Discontent is
an aspect of all sin, for if men were contented with righteousness and
the fruits of righteousness, they would not rebel against God. Cove-
tousness is a specific form of discontent: the desire to possess
another's goods at all cost, including the:other man’s loss. As Mat-
thew Poole, the Puritan commentator, wrote in the seventeenth cen-
tury: covetousness is the “inward and deliberate purpose and desire
of a deceitful or violent taking away of another man’s goods, but this
is forbidden in the eighth commandment.” Thelt is forbidden; cov-
etousniess is the inzward desire that leads to theft or fraud. Ir is the evil
desire which overwhelms the law’s restraint on the sinner, the desire
to have another man’s property, whether or riot the other man bene-

8 Did., ILL, pp. 464-65.

9. Herman Hoeksema, The Triple Knowledge: An Exposition of the Heidelberg Cate-
chism, 3 vols. (Grand Rapids, Michigan: Reformed Free Publishing Association,
1972), MI, pp. 427-28.

10. Matthew Poole, Gommentary on the Whole Bible, 3 vols. (London: Banner of
‘Truth Trust, [1683] 1968), £, p. 160.
