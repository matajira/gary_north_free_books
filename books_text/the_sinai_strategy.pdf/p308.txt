282 THE SINAI STRATEGY

peculiar people . . .” (I Pet. 2:9a). No doubt, most pricsts rested on
the national sabbath day in Israel, No doubt, most Christians rest
on the Lord’s day. Would it not be proper to acknowledge the legiti-
mate exceptions ~ profanations of the Lord’s day that are blameless?

Rural Life Forever?

We have to ask ourselves this fundamental question: Did God es-
tablish the self-sufficient rural soctety as His perpetual soctetal standard? Is
this standard stil morally binding on Christian cultures? The
Mosaic sabbath was specifically created as a means of preserving an
economy that adhered to a six-one rhythm of the workweek. Even
the most seemingly trivial violation of the pattern, namely, stick-
gathering, was to be punished by death. It is difficult for us to imagine
the smooth operation of a modern industrial economy within the
stated framework of Numbers 15. But the law was not perfectionist. It
did allow exceptions with respect to Passover. It is likely that similar
exemptions existed for other celebrations for Hebrews with unique oc-
cupations. But there is no list of exceptional occupations in the Old.
Testament which proves that such exceptions did exist, other than for
the priesthood itself. It should be clear that anyone appealing to the
elders for an exemption would have had to prove his case, namely,
that his occupation unquestionably required seven-day operations.

It is true that automation is steadily reducing the number of pco-
ple who must be employed on any given day, but engineers and
emergency servicemen must be there to keep the equipment run-
ning. The moral issue of using services that require only a few men
to violate the Lord’s day, simply because there has been a change in
technology, is still a question of right and wrong. In any casc, could
such a technology ever have developed, had Sunday workers been
prohibited from the very beginning in the light and power industry?

What the strict sabbatarian is calling for is a drastically reduced
material standard of living one day per week, an alteration of
modern life styles so radical that its consequences for the economy
can barely be contemplated. The Puritan-Scottish interpretation of
the Mosaic standard is undeniably rigorous: no cooked meals, no
restaurants, no television, no radio, no newspapers delivered on
Sunday or Monday morning (Sunday production), no hot water

50, In Athens, Georgia, a university town, the Sunday newspaper in the 1950's
was delivered on Saturday night, and there was no Monday morning newspaper,
James Jordan informs me.
