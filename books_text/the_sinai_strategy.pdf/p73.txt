Graven Images and Compound Judgment 47

Paganism and gambling are closely linked philosophically,
especially in periods of declining social order. Rushdoony writes:
“Gambling comes to have a religious prominence and passion in the
minds of men, so that it is more than a mere pastime: it is a hope for
life... . The gambler denies implicitly that the universe is under
law; he insists that ‘all life is a gamble, and a falling brick can kill
you, and totally meaningless events always surround you, because
chance, not God, is ultimate. Since chance, not God, rules the
universe, causality does not prevail. It is therefore possible to get
something for nothing, and the gambler, knowing what the odds are,
nevertheless expects chance to overrule law and enrich him.”5? The
gambler believes in law-overcoming chance, or luck. Such an
outlook was dominant during the Roman Empire, and it destroyed
the foundations of classical civilization.34

Such an outlook is also the ideology of the revolutionary. Faith in
the great revolutionary discontinuous event, the run of successful
bets, or the overnight “killing” in the market marks the short-run view
of fallen man. Continuity holds no promise of victory for him, for he
knows that time and continutty are his great enemies. The run of luck for a
gambler cannot hold; the law of averages (statistical continuity)
eventually réasserts itself. Similarly, the traditions and habits of men
(social and ethical continuity) thwart the revolutionary, if the
revolutionaries cannot capture the seats of central power overnight,
in a top-down transfer of power to the newly captured central gov-

 

 

33. R. J. Rushdoony, Politics of Guilt and Pity (Fairfax, Virginia: Thoburn Press,
{1970] 1978), p. 217.

34, Charles Nortis Cochrane, Christianity and Classical Culture: A Study of Thought
and Action from Augustus to Augustine (New York: Oxford University Press, [1944]
1957), p. 159,

35. Karl Marx, who spent most of his life in sclf-iraposed poverty, inherited a for-
tune in 1864. As the money was being sent in chunks, Marx invested in the stock
market. He wrote to Engels on July 15: “If I had the money during the last ten days,
{ would have been able to make a good deal on the stock exchange. The time has
come now when with wit and very little money one can make a killing in London.”
As his biographer reports, a year later he was again begging for money from Engels.
Robert Payne, Marx (New York: Simon & Schuster, 1968), p. 353. Marx was sup-
ported entirely by Engels from the carly 1870’s until his death in 1883.

In contrast to Marx's profligate, gambling ways was his uncle, Lion Philips, who
despised his nephew. Philips founded the Philips Gompany, which is still one of the
largest manufacturing companies in Europe. In the United Starcs, it is known as the
North American Philips Gompany, or Norelco. In the early 1960's, this innovative
firm invented the audiocassette tape and tape recorder, which launched a technolog-
ical revolution. Compound growth was still operating at Philips.

 
