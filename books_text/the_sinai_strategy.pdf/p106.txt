80 THE SINAT STRATEGY

era is John Murray's statement. Mr. Murray was a leading
twentieth-century Calvinist scholar, and a Scot—a Scot whose views
on the sabbath were too lax to gain favor in the Scottish Frec Kirk,
so he came to the United States to teach. He acknowledged the “ele-
ment of truth” in the statement “by good men, that we do not now
under this.economy observe the Sabbath as strictly as was required
of the people of Israel under the Old Testament.” For one thing, they
were not allowed to kindle a fire. For another, the death penalty was
imposed. “Now. there is no warrant for supposing that such regula-
tory provisions both prohibitive and punitive bind us under the New
Testament. This is particularly apparent in the case of the capital
punishment executed for Sabbath desecration in the matter of
labour. If this is what is meant when it is said that observance is not
ag strict in its application to us as it was under the Mosaic law, then
the contention should have to be granted.”6 Murray, however,
offered no exegesis to explain how the requirement of sabbath obser-
vance has survived, but without the civil sanctions attached to
Mosaic sabbath law.

FN. Lee, a South African Calvinist sabbatarian who, like Mr.
Murray, left his native land to teach in other English-speaking na-
tions, writes in his doctoral dissertation on the sabbath (1966) that
the capital punishment provisions of the sabbath law have been
abrogated. “It is important to realize that these aspects of the weekly
sabbath, even though they were ordained by God, were only of.tem-
porary ceremonial and/or political significance, and were not intrin-
sically normative for the permanent weekly sabbath as such,
although they were certainly temporarily normative for the Sinaitic
weekly sabbath of Israel from Sinai up to the death and resurrection
of Christ in which events all these aspects were fulfilled.” (Lee has
begun to alter his position since the time of publication of his disser-
tation in the early 1970's. He tells me that he believes that Old Testa-
ment law is still in force in this age, but he is not yet ready to recom-
mend, categorically, that the death penalty should be imposed in all
cases of sabbath violations, although continued willful desecration
might be sufficient reason to execute the rebel, he says.*)

6. John Murray, “The Sabbath Institution” (1953), in Collected Writings of John
Murray, 4 vols, (Carlisle, Penmsylvania: Banner of ‘Iruth, 1976), Vol. I, p. 211.

7, FN, Lee, The Covenantal Sabbath (London: Lord’s Day Observance Society,
1972), p. 30.

8. Cf, FN. Lee, Christocracy and the Divine Saviors Law for All Mankind
(Tallahassee, Florida: Jesus Lives Society, {1979]), p. 7.
