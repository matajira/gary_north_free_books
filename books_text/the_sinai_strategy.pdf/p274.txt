248 THE SINAI STRATEGY

that members could rest on the seventh day (Saturday) and celebrate
on the evening of the first day, which was a working day in Israel.
They could do this in good conscience precisely because they knew
that God honored their faith, Like the priests who sacrificed on the
sabbath, profaning it blamelessly, the early Christians worked on the
Lord’s day, profaning it, but it was not held against them.

This is not to say that the ideal situation is not the Lord’s day as a
day of rest and worship, universally recognized, universally re-
spected, except in cases of emergency or merciful labor. But Paul
was careful to warn the church at Rome that it should not burden its
new members with rigorous regulations concerning a special day of
the week. Yes, they were to commune together. But whatever they
did on the Lord’s day—and in Rome, most of them must have
worked —they were to do it in faith. The sabbath ideal is to grow out
of respect for the principle of resurrection, the basis of man’s release
from sin and eternal death. The institutional church sets the pattern
with its special day of worship, which can be made binding on mem-
bers (Heb. 10:25). But it cannot legitimately force its members to
honor the one-six pattern of rest. That pattern is built into Christ’s
kingdom, but Paul makes it clear that the cansctence is to guide men to
this conclusion, not compulsion. In fact, he was writing against one
man’s criticizing another ~- moral compulsion. Hf moral compulsion.
is forbidden, then how much more ecclesiastical compulsion? And
how much more than this, compulsion by the civil government?

The Early Church Fathers on Rest vs, Worship

This distinction between Sabbath rest and Lord’s day worship
was unquestionably made by the early church fathers. Until the
fourth century, church fathers generally condemned the “idleness” of
the Jewish sabbath, and commanded church members to devote
Sunday to worship and acts of mercy. Bauckham comments: “For
Tertullian, the meaning of the Sabbath commandment for Chris-
tians was ‘that we stil] more ought to observe a sabbath from all ser-
vile work always, and not only every seventh day, but through ail
time.’!2 It is entirely clear that for all these writers the literal com-
mandment to rest one day in seven was a temporary ordinance for
Israel alone. The Christian fulfills the commandment by devoting all
his time to God. The rationale for this interpretation depended, of

12. Tertullian, An Answer io the jews, ch. [V.
