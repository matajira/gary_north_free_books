Liberation Economics 21

“It should now be clear,” Fuller continues, “why the necessity for
obedience in no way clashes with sola gratia (‘by grace alone’), for the
Doctor is administering his cure just from the sheer joy he has in ex-
tending a blessing to others and in being appreciated for what he
does. The Doctor does not bless people because they are the work-
men who have rendered some necessary service to him which obli-
gates him to reimburse them with medical care. It should also be
clear why the obedience of faith is sola fide (‘by faith alone’), for obe-
dience is impelled wholly by faith and is not something added on to
faith as though it were coordinate with it. . . . Finally, there should
be no difficulty in understanding how the Doctor receives all the
glory (sola gloria), the credit for the cures that are performed, and for
the additional patients that flock to his clinic because of the glowing
testimonies of those who have already experienced partial healing.”!?

Those who worship any god other than the God who reveals His
standards in the Bible are worshippers of a false god. No other god,
no other goal, no other standard is to replace men’s faith in the living
God who delivered Israel. God is primary; there is no secondary
God. From this it follows that those who proclaim a law-order alien to the
one set forth in the Bible are thereby proclaiming the validity of the werd of some
other god. They have become idolators— perhaps not conscious idola-
tors, but idolators nonetheless. They are aiding and abetting the
plans of men who worship another god. A god’s personal (or imper-
sonal) attributes are revealed by its law-order. To proclaim a rival law-
order is to proclaim a rival god. Pluralism is political polytheism.

Biblical Economics and Liberation

Can men legitimately have confidence in the law of God in eco-
nomic affairs? Yes. Why is this confidence justified? Because the
same God who delivered Israel from the Egyptians also established
the laws of economics. This means that the basis of these economic
laws is not man, or random chance, or historical cycles, or the im-
personal] forces of history, but instead is the sustaining providence of
God. The guarantor of the reliability of economic law is a personal
Being who delivers His people from those who defy His law.

Biblical economics is liberation economics. Anti-biblical economics is
therefore bondage economics. Those who present themselves as de-
fenders of liberation economics, but who refuse to be governed in

13, Jbid., pp. 119-20.
