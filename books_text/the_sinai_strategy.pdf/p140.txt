li4 THE SINAI STRATEGY

only at present costs, neglecting future benefits, such as the care
which the unborn might provide them in their old age. They have faith
in the compasstonaie and productive State the great social myth of the
twentieth century. They want its benefits, but they never ask them-
selves the key question: Whe will pay for their retirement years? Not
the shrinking number of children, who are even more present:
oriented, even more conditioned by the statist educational system,
even more unwilling to share their wealth with the now-
unproductive aged of the land. With the dissipation of capital, the
productive voters will resist the demands of the elderly. The genera-
ttons go to war against one another — the war of politics.

The pseudo-family State is an agent of social, political, and eco-
nomic bankruptcy. It still has its intellectual defenders, even within
the Ghristian community, although its defenders tend to-be products
of the State-supported, State-certified, and State-aggrandizing uni-
versities. This pseudo-family is suicidal. It destroys the foundations of
productivity, and productivity is the source of all voluntary charity.
It is a suicidal family which will pay off its debts with inflated fiat
currency. Its compassion will be limited to paper and ink.

The impersonalism of the modern pseudo-family, along with its
present-orientation—a vision no longer than the next election — will
produce massive, universal failure of the welfare system. It has
already donc so. The rapid escalation of Federal anti-poverty pro-
grams has created more poverty, except for the middle-class bureau-
crats who operate the programs. The great economic experiment
of the twentieth century is almost over, and all the college-level text-
books in: economics, political science, and sociclogy will not be able to
justify the syste, once it erodes the productivity which every para-
silic structure requires for its own survival. Like the Canaanitic eul-
tures of Joshua’s day, the end is in sight for the modern, messianic,
welfare State economies. They have decapitalized their envy-driven,
guilt-ridden citizens. Only to the extent that citizens hide their eco-
nomic assets or vote to reverse the politics of envy will they escape the
clutching hand of today’s spendthrift, senile pseudo-parent.

Conclusion

It is imperative for Christians to abandon the religion of human-
ism. It is imperative that they fulfill their responsibilities as members

50. Charles Murray, Losing Ground: American Social Policy, 1950-1980 (New York:
Basic Books, 1984),
