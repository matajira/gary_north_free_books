CONCLUSION

Y shail therefore keep my statutes, and my judgments: which if a man
do, he shail live in them: I am the LORD. your God (Lew 18:5).

The ten commandments set forth God’s laws of life. They do not
provide life, but they set forth the standards of life. This is why Jesus
Christ came to earth to fulfill the terms of the law, not to annul them
(Matt. 5:17-19).1 Without His willingness and ability to live out
these laws, in time and on earth, God would not grant eternal life, or
even temporary earthly life, to any law-breaker.

These laws were presented to the Israelites by God in the form of
a covenant treaty.2 Men inescapably live in ‘terms of covenants:
either before God or before Satan, and always with each other.
‘Thus, these laws of life are necessarily covenantal laws, both social
and personal, both general and particular? What are the covenantal
goals of God's laws of life in society? Social peace and economic blessings,
“peace and prosperity.” There is no other way to interpret Deuteron-
omy 28:1-14: the list of external and internal blessings is comprehen-
sive. Furthermore, the list of cursings is long and threatening: Deu-
teronomy 28:15-68. What we need to understand is that God’s law is
intended to create conditions leading to peace, harmony, and
wealth,

The ten commandments also lay down the religious, legal, and
economic foundations that are necessary for the creation and long-
term maintenance of a free market economy. In other words, observ-

1, For a detailed consideration of Christ’s words in Matthew 5:17-19, sce Greg. L.
Bahnsen, Theonomy in Christian Ethics (2nd ed.; Phillipsburg, New Jersey: Presbyter-
ian & Reformed, 1984), ch, 2.

2, Meredith G, Kline, The Structure of Biblical Authority (rev, ed.; Grand Rapids,
Michigan: Eerdmans, 1975), pp. 113-71.

3, James B. Jordan, The Law of the Covenant: An Exposition of Exodus 21-23 (Tyler,
‘Texas: Institute for Christian Economics, 1984), ch. 1.

ait
