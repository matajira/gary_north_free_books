32 THE SINAI STRATEGY

An idol is_a means of negating the Creator-creature distinction,
Men believe that they can approach God, placate God, and even
control God through bowing to an idol. Yet idols are radically dis-
tinct from God, as this passage tells us: men are not to worship any.
aspect of the creation, whether in heaven, on earth, or under the
earth.!?

Idols are weak. The Hebrews had seen that idols had not pro-
tected the Egyptians, and their children would see that the idols of
the Canaanites would be equally impotent, At best, idols put men
into contact with demonic beings that can manifest power, but noth-
ing comparable to the awesome power of God. These rites place men
into bondage to underworld spirits that can control them, even as
men hope to control the spirits and the external environment by
means of idol worship.

God forbade the use of tools in the construction of His altar, “Ye
shall not make with me gods of silver, neither shall ye make unto you
gods of gold. An altar of earth thou shalt make unto me, and shalt
sacrifice thereon thy burnt offerings, and thy peace offerings, thy
sheep, and thine oxen: in all places where I record my name I will
come unto thee, and I will bless thee. And if thou wilt make me an
altar of stone, thou shalt not build it of hewn stone: for if thou lift up
thy tool upon it, thou hast polluted it. Neither shalt thou go up by
steps unto mine altar, that thy nakedness be not discovered thereon,”
(Ex, 20:23-26),1? The Hebrews were not allowed to design and build
at their own discretion the shape of the place of atonement before
God, God provided the raw materials, and they were not to reshape
them.

When the early church spread the gospel, the image-makers
suffered financial losses. Acts 19 records the confrontation between
the evangelists and the silversmiths who made the images of the tem-
ple of Diana. The leader of the craft guild, Demetrius, warned his
colleagues: “Morcover ye see and hear, that not alone at Ephesus,
but almost throughout all Asia, this Paul hath persuaded and turned
away much people, saying that they be no gods, which are made
with hands” (v. 26). The gospel had negative economic consequences
for the pagan craftsmen of idols.

The prohibition of graven images was not a universal condemna-

12, Lam indebted to Prof. John Frame’s class syllabus, Doctrine of the Ghristian Life,
for these insights,
13. Deut. 27:5; Josh. 8:30-31.
