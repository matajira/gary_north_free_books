326 THE SINAI STRATEGY

for all eternity it seems silly, especially in light of the fact that Peter
did say that a total contribution to the common treasury was not re-
quired. Since Troost'does not think that the Bible provides us with
concrete data concerning economic affairs, it does not seem logical
for him to bring up the matter at all. If he means simply that Chris-
tians should, on occasion, be willing to give up some of their private
wealth to the Christian community, then he has not said very much,
Troost then mentions the fact that “the New Testament is not so-
cially revolutionary” in the eyes of some Christians. He says that the
New Testament, at least on its surface, “does not radically condemn
the situation in which its authors preached and wrote” (p. 33). It
even accepted slavery as an institution, as Paul's epistle to Philemon
indicates. Troost realizes that the New Testament is, in this practical
sense, profoundly conservative — it did not attack directly the social
fabric of Roman society, This disturbs him, and therefore he returns
to his old theme: “It would, however, be entirely at Variance with the
spirit and intention of the gospel, with the Message, if from the
above we were logically to draw up socio-economic conclusions
which would then have to be applied in practical politics. Not a few
Christians perpetuate in this way an economic and political conservatism.
The same goes for progressivist-socialistic conclusions from biblical
‘data’. . .” (p. 34). Common property in Acts 4:32 is somehow rele-
vant today; conservative elements in the Bible are not. He reasserts
himself once again: “The biblical message of the kingdom of God
does not directly address itself to the betterment of human society
which includes, arnong other things, property relations. But, to be
sure, it does indeed affect them!” To be sure of what? How does it
affect them? In his answer, ‘Troost arrives at a position of total anti-
nomian mysticism: “In order to exercise our property rights in every-
day life in the right manner, and to handle our possessions before the
face of God in a way pleasing to him, nothing less is required than the
merciful intervention of God, from above, through the Holy Ghost.
Unless regenerated, common sense will change nothing, Renewal
must come from the top down; it will not come up by itself from the
bottom, Our natural reason can achieve nothing here” (p. 34).
Consider what Troost is saying. The Bible, he has said, does not
provide any concrete data—no applicable kind of special revelation
—in the area of economic and political affairs. Yet he is also saying
that “Our natural reason can achieve nothing here.” Not only is there
no special revelation in social affairs, there is no general revelation
