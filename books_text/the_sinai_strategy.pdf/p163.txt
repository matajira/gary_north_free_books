The Yoke of Co-operative Service 137

libertarian concept of the illegitimacy of lifetime contracts, The lib-
ertarian’s universe’ could not bind a man to perform any sort of
future labor service. It certainly could not require him to love, cher-
ish, and support a recently abandoned wife. She may have given
him her youth in the days of her beauty—her “high-yield capital”
stage, or her “high exchange value capital” stage — but once this capi-
tal is gone, she is without legal protection. Thus, the radical imperma-
nence of libertarian contracts would threaten the social fabric of any soci-
ety so shortsighted as to adopt this utopian philosophy as its founda-
tion. ‘The future-ortentation provided by the safety of permanent vows
in a godly society could not exist in a consistently libertarian society.
There would be no institutional means of enforcing the terms of
covenants, and this would eventually reduce men’s confidence in the
enforceability of shorter-run contracts. A society which rejects the
binding nature of covenants will not long retain the economic bless-
ings of binding contracts.

Conclusion

The protection of man’s life, wife, and property is what a biblical
social order offers. The woman is protected, too. The time perspec-
tive of such a society will be longer term than a social order (dis-
order) characterized by adultery, divorce, illegitimate births, and
single-parent households. Whenever a social order is marked by suc-
cessful attacks against private property and also by the removal of
stringent sanctions against adultery, the social order in question has
departed from the standards set forth in the Bible, It has adopted an
anti-biblical religion, whatever the official pronouncements of its
leaders, including its church leaders.

A survey of 950 religious teachers and counsellors which was
conducted by the University of Houston in 1984 revealed that of the
500 who responded to the questionnaire, 40% did not believe that
premarital heterosexual sex is immoral, and that 87% believed that
adultery should not be a crime.** When the religious and political
leaders of a society begin to wink at adultery, they will soon enough
wink at coercive wealth redistribution, confiseatory taxation, and

24. Associated Press story, Tyler Morning Telegraph (Dec. 28, 1984). Sixteen per-
cent said that adultery is not morally wrong, 9% were uncertain, and 75% said it is
morally wrong. But almost none of them thinks the civil government has any role in
punishing adulterers. Only 53% said that the legal system ought to limit marriage to
people of oppasite sexes.

 
