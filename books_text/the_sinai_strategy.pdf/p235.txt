Covetousness and Conflict 209

tian leaders, but has come from humanists who are defenders of
nineteenth-century economic liberalism—a perspective which ‘itself
was a secularized and Darwinian version of biblical sucial cthics.'

What has been called “the climate of opinion” in any given era is
a most powerful social force. This is why it is imperative that Chris-
tians develop and preach a systematically biblical social. program.
Because Christians have neglected this critically important task, the
secularists have taken the lead in setting the climate of social opin-
ion, This climate of opinion has subsequently influenced the think-
ing of Christian intellectual leaders. The competing conclusions of
the god of humanism, autonomous man, have become the standards
for Christian thinkers and policy-makers.

Not all Christian scholars are socialists, of course, but it is a
widely held opinion that any social and economic framework is ac-
ceptable to Christians. Those Christians who believe that any eco-
nomic framework is acceptable (except one based explicitly on
biblical law, of course), just so long as Christians have the right to
preach the gospel of personal salvation, are faced with a problem: By
what standard can a Christian legitimately conclude that all eco-
nomic frameworks are acceptable to Christ? Furthermore, if any and
all social and economic frameworks are legitimate before God, then in what way
can the preaching of the gospel influence the soctal institutions of the day? How
can they be reformed? And if they do not need reform, how is it that
rebellious, sinful men have succeeded in creating social institutions
that are not in need of reconstruction? How, in short, can Christians
avoid constructing a social order on the shifting sands of warring hu-
manist philosophies, special-interest groups, power-seekers, and
contradictory social and political programs? Is the Bible irrelevant to
social institutions?

Conclusion

Social peace is a major goal of biblical law —the social peace demanded
by the prophet Isaiah: “They shall not hurt nor destroy in all my holy
mountain: for the earth shall be full of the knowledge of the Lorn, as
the waters cover the sea” (Isa. 11:9). The juridical foundation of such peace
is biblical law. The ten commandments serve as the basis of long-
term, God-blessed social peace.

16. Gary North, Zhe Dominion Covenant: Genesis (Tyler, Texas: Institute for Chris-
tian Economics, 1982), Appendix B: “he Evolutionists’ Defense of the Market.”
