Familistic Capital 113

and cach receives. The relationship is both personal and economic.
But today the modern State intervenes. It provides the children’s
education. It lays claim to future payments (taxes) by the children
when they have reached maturity. Of necessity, it must try to buy the
love (votes) of those children when they reach maturity. The children
often remain subservient to the State-parent, unwilling to launch in-
dependent lives of their own, given the costs of breaking the financial
and emotional tie with the welfare office. Children, the covenant
family’s primary resource, are stolen by the modern State. The State
promises old age support. The State promises health care for the
aged. The State provides State-financed and State-licensed educa-
tion for the young. The State attempts to replace the benefits of the
family, and simultaneously must require the same sort of financial
support from the adults during their productive years. The relation:
ship is impersonal and economic. The relationship is, by law, coer-
cive and bureaucratic.

Impersonalism and Capital Consumption

This disastrous attempt of the civil government to replace the
functions of the covenant family eventually destroys the productive
mutual relationships between generations. It destroys the personal
bond, making the young in general legally responsible for the old in
general. The family name—so central to the life of a godly social
order -is erased, and computerized numbers are substituted. The
incentives for families to preserve their capital, whether for old age
or for generations into the future, are reduced, for each generation’s
economic future is ne longer legally bound to the success and pros-
perity of the children. “Eat, drink, and be merry, for tomorrow there
will be government checks.” But the dissipation of family capital,
when it becomes a culture-wide phenomenon, destroys economic
productivity, which in turn destroys the tax base of the State. The
State cannot write the promised checks, or if it does, the monetary
unit steadily grows worthless, as fiat money inflates the price level.

By abandoning the principle of family responsibility, the modern
messianic State wastes a culture’s capital, destroys inheritance, and
makes more acceptable both euthanasia (which reduces the expense
of caring for the unproductive elderly) and abortion (which reduces
the expense of training and caring for the unproductive young).
Lawless men, in their productive years, increasingly refuse to share
their wealth with dying parents and squabbling children. They look
