The Economic Implications of the Sabbath 285

civil magistrates to become vital? Which industries used to be vital,
but are no longer vital? Public utilities? (What about cable television
service?) The healing professions? (What about cosmetic plastic sur-
gery?) Should they be allowed to charge a fee for “emergency
service’? What is an emergency service?

Having somehow solved the problem of providing legal defini-
tions ‘of vital services and products, the State’s authorities would
then have to decide what market pressures really face these indus-
tries. Are they really required to operate seven days a week? They
may say that they are, but are they? What criteria should be used by
civil magistrates to determine the true state of affairs? Which factors
determine the economics of any: particular profession or industty
and its market? Foreign competitors (including competitors across
the county line, or state line)? Consumers’ buying habits (including
consumers across the county line or state line)?

The One-World State

Perhaps most important, how can we grant such a decision-
making authority to the civil government without seeing the creation
ofa vast, arbitrary, powerful bureaucracy? These questions concern-
ing “works of necessity and mercy” and “true state of market com-
petition” are enormously complex. They cannot even be decided on
a local basis, given the worldwide division of labor. They cannot
even be decided nationally. They have to be decided by a one-world
State—a State that has the power to enforce its decisions.

The Mosaic sabbath was to be enforced in Israel, whatever its
exemptions for specific occupations, despite tribal practices or
preferences. With the breaking of the old wineskins of Israel's econ-
omy (taken in the broader sense of “economy”), the church that
would impose the Mosaic sabbath laws now faces an enormously
more difficult and complex task. How can it define the problem
areas? How can it enforce its decisions internationally? And if solu-
tions can be found to these questions, there is always the critical one
remaining: How can a one-world State enforce the Mosaic sabbath
without becoming top-heavy, imperial in nature, and a threat to the
very idea of decentralized Christian institutions? How can the
Mosaic sabbath be enforced in international markets without
destroying the legal basis of freedom, namely, predictable law en-
forced by an impartial civil government?

Will sabbatarians now argue that nations have to come to an
