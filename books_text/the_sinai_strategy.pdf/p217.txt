The Value of a Name 191

who I was, they were willing to risk their money and subscribe. I
communicated in an effective way to one group, but not to the other.
Motivation and name-identification are closely linked.

What about pictures of rugged cowboys (one might say “worn-
out cowboys”) that sell cigarettes? Marlboro has used “Marlboro
Country” ads for two decades to gain and retain a large share of the
cigarette market in the United States. Is this somehow immoral?
Consider what. went before. Marlboro originally catered to women
and had a red filter tip. This approach in the 1950’s was a resounding
flop. The company dropped it in 1954. The second approach was a
resounding success. Was either approach innately immoral (setting
aside the question of whether cigarettes as such are somehow im-
moral)? Has the public been misled in the latter case, but not in the
former? Or are buyers somehow pleased to smoke “he-man” cigaret-
tes rather than “women’s” cigarettes? Apparently, they are unwilling
(or were) to buy red filter tipped cigarettes that attempted to sell to
women. But did red filter tips convey “true” information about fem-
ininity? Was the early Marlboro cigarette more a women’s cigarette
than a man’s? Or were the advertisers simply trying to position the
cigarette in the market by a subtle (or not too subtle) appeal to the
buyers’ imagination? Is it wrong to give a consumer a sense of be-
longing to a “special breed” of men, even when nobody believes it?
And if it is wrong, why do buyers return, year after year, to the com-
panies that offer them illusions —harmless illusions —that buyers re-
spond to? What possible benefit would the consumer or the seller de-
rive from an endless series of ads that announce: “This product is bas-
ically the same as all the others, but we want you to buy ours, since we
like our present employment opportunities.” How exciting would that
be, even though most of us know that such a disclaimer is essentially
true, and that competition keeps most of the products within any
given price range basically comparable (though not identical)?

The point which cannot be avoided is this: the very competitive
structure which provides incentives for one company to improve a
product, and for others to follow this lead, is heavily dependent on
advertising to create the desire to buy in the minds of the readers or
viewers. The advertising system, so widely criticized, is itself one
foundation of the competitive system that makes “miracles” available
to the public at competitive prices, The “evils of advertising,” mean-
ing effective, motivating advertising, are absolutely fundamental to
free market sales. The zolundarism that lies at the heart of the market
