Covetousness and Conflict 207

reducing thereby the proportion of income going to support law en-
forcement. The society is blessed in two ways: reduced crime (in-
cluding the crime of statist wealth redistribution programs) and in-
creased output per capita. Men wind up with more wealth after taxes,
They increase their opportunities for responsible action before God and men,

The Modern Welfare State"

The twentieth century, since the outbreak of World War I, has
abandoned the tenth commandment. Divorce and remarriage of the
sinful partner are common events. Men covet their neighbors’ wives.
They covet their neighbors’ goods. (Coveting a man’s goods is cer-
tainly less of a threat to the integrity of his family unit than the
coveting of his wife.) The rise of massive taxation, including the
inflation tax, has led to the spread of covetous political programs.
The graduated income tax, with its increasingly burdensome rates of
taxation for those with higher income, has been proclaimed in the
name of social justice, even Christian social justice.!2 Nevertheless,
the combination of graduated income taxation, the psychology of
debt (and even tax deductions for interest payments in the United
States), and the control of money by the State and its licensed
agents, the banks, has led to ruinous taxation of the middle classes.
Men are tempted to vote for more wealth redistribution programs,
and then they are tempted to pay for them by means of monetary
inflation. This enables both individuals and the State to-repay loans
with depreciated money. “A little inflation” seems to be beneficial in
the early years, since it fosters an economic boom." It involves the
destruction of the creditors’ interests, but who cares about creditors?

11, The phrase “welfare Staie” first altained prominence in 1949, writes historian
Sidney Fine, and bas come to be associated in the United States with the administra-
tion of President Harry Truman, 1945-53; Sidney Fine, Laissez Faire and the General
Welfare State: A Study of Conflict in American Thought, 1865-1901 (Ann Arbor; University
of Michigan Ann Arbor Paperback, [1956] 1964), Preface.

12, John G. Bennett, who taught ethics at Union Theological Seminary in New
York, and who served as president of that institution, writes concerning needed
social. reforms: “The third reform is changes in the tax system that would close
loopheies for the rich and in many ways bring about a more equal distribution of
wealth. The adoption of the idea of a progressive income tax was in itself an early
breakthrough of great importance.” Bennett, The Radical Imperative: From Theology to
Social Ethics (Philadelphia: Westminster Press, 1975), p. 153.

13. James Dale Davidson, The Squeeze (New York: Summit Books, 1980).

14, Ludwig von Mises, Human Action: A ‘Treatise on Economics (3rd ed.; Chicago:
Regnery, 1966), ch. 20.

 

 
