The Economic Implications of the Sabbath 243.

subordinately re-creative week was supposed to be a one-six patiern,
with God’s pronouncement of judgment against Satan coming at the
end of the seventh day, or possibly at the beginning of the next-day
(evening). Adam’s rébellion led to a curse: God’s imposition-on man
of a God-imitating six-one pattern, with rest to come only at the end
of man’s week.

Jesus Christ, by redeeming His people, annulled the six-one pat-
tern of the cursed week. He did not restore the original (pre-Fall)
pattern of one-six, since He changed the day on which the Lord’s
day is celebrated to the day after the Hebrew sabbath — what Chris-
tian commentators for at least 1,800 years have called the eighth day.
Therefore, He established a one-six-one pattern—rest, work, and
judgment. This judgment comes on the day of the Lord, the arche-
typal Lord’s day. This is why the Lord’s day is celebrated in New
Testament times on the day following the Hebrews’ seventh-day sab-
bath: it points to the final judgment and the inauguration of a new week,
the full manifestation of the New Heaven and the New Earth. The
first day of redeemed man’s week is now the eighth day after the ini-
tiation of God’s work, not the seventh day after. It represents a re-
creation, a new week which re-establishes a one-six pattern, but which
also implies the one-six-one pattern as a herald of the total regenera-
tion and re-creation of all things. The shift to the eighth day testifies
to Christ’s new creation.

Conscience: The New Locus of Enforcement

Paul was concerned with the souls and consciences of his readers.
The Colossians passage mentions meat, drink, holy days, and sab-
baths. He was doing his best to convince his readers that there had
been a definitive break from Old Testament law with respect to these
four features of Hebrew life. He knew that Judaizers were criticizing
the Christian Hebrews for their abandonment of these external tests
of faith, and he did not want his readers to feel guilty. No one could
legitimately judge them with respect to these four issues. No one
could turn to the Mosaic law and confront them with the Mosaic
tules, instructions, and regulations regarding meat, drink, holy
days, and sabbaths. This did not mean that the old rules were evil. It
meant that the Judaizers had no right to criticize Christians for no
longer adhering to the old forms. New applications of the Old Testa~
ment's general principles in these four areas are now binding in New
Testament times.
