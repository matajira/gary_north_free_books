194 THE SINAI STRATEGY

Information costs are inescapable. Men are not omniscient. The Bible
warns men against the sin of presumption, the sin of seeking to be
God. A godly society recognizes that information is not a free (gratu-
itous) good. [t recognizes the need for establishing institutions that
enhance the spread of accurate, motivating, and self-correcting. knowl-
edge. The West has overcome this cost barrier more effectively than
any society in history precisely because the West has honored the
laws protecting property, including the property right in one’s name
or company mark. The transmission of more accurate information
through advertising, independent testing, and brand-name recogni-
tion has created the modern marketplace, with its relative lack of
“hard bargaining” between buyers and sellers.

The modern market transfers the competitive bargaining process
to a far more fair and beneficial system: buyers vs. buyers, and
sellers vs. sellers. The better the participants’ knowledge of market
alternatives, the less benefit one bargainer (buyer or seller) has over
the other (seller or buyer). Better information protects the weaker
party in any economic exchange. The face-to-face hard bargaining
which. characterizes the Middle Eastern bazaar or other trading
areas takes too much time to conduct transactions, and it puts too great
a premium on monopolistic psychological manipulation. The average
buyer or seller is protected by a broadly based (“impersonal”)"! free
market, with its highly developed systems for transmitting accurate
knowledge concerning available economic alternatives.

A man’s reputation is to be protected, for good or evil, whether in
a-court of law or in the court of public opinion. Whether he is right-
eous or evil, efficient or incompetent, his reputation should reflect
his true condition, The jury, the history book, and the free market
are all institutions that render judgment. They must render righteous,
accurate judgment. Rendering judgment is basic to the ninth ‘com-
mandment, and parallels the fourth commandment’s sabbatical day
of judgment by God. Evil men seek continuity not by establishing a
righteous family name but instead by means of crime and false
testimony. The result is a lack of rest for any society which refuses to
judge men by God’s standard. Where there is no true judgment, there
can be no rest—none for the wicked, but more to the point, none for
the societies that refuse to punish the wicked.

UL, On the proper and impproper use of the word “impersonal” as it relates to the
operations of the free market, see North, The Dominion Covenant: Genesis (Tyler,
Texas: Institute for Christian Economics, 1982), pp. 9-11.
