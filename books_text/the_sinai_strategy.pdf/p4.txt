Copyright ® 1986
‘ Gary North

ISBN 0-930464-07-9

Cover design by George Grant
Cover illustration by Randy Rogers
‘Typesetting by Thobum Press, Tpler, ‘Texas

Published by
The Institute for Christian Economics
P.O. Box 8000
Tyler, Texas 75711
