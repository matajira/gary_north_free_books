Introduction 3

tions with fear and trembling (Phil. 2:12). Finally God will declare
them righteous before all men and angels at the day of judgment,
when the removal of the curse will be complete. In short, what was
definitive at Calvary—Satan’s defeat~is being progressively re-
vealed in history, and will be finally revealed at the day of judgment
and in eternity.

Because Christians in our day have failed to understand these
fundamental biblical principles, they have misunderstood the mean-
ing of “spiritual.” David Chilton’s comments are to the point: “When
the Bible uses the term Spiritual, it is generally speaking of the Holy
Spirit... . To be Spiritual is to be guided and motivated by the Holy
Spirit. It means obeying His commands as recorded in the Scrip-
tures. The Spiritual man is not someone who floats in midair and
hears eerie voices. The Spiritual man is the man who does what the
Bible says (Rom. 8:4-8). This means, therefore, that we are supposed
to get involved in life. God wants us to apply Christian standards
everywhere, in every area. Spirituality does not mean retreat and
withdrawal from life; it means dominion. ‘The basic Christian confes-
sion of faith is that Jesus is Lord (Rom. 10:9-10)— Lord of all things, in
heaven and on earth. As Lord, He is to be glorified in every area
{Rom. 11:36). In terms of Christian Spirituality, in terms of God’s re-
quirements for Christian action in every area of life, there is no rea-

 

son to retreat.”?

But how do we know that we are being Spiritual? By looking to
the Bible in order to discover the principles of Spiritual living. What
are these permanent principles called in the Bible? The daw. Modern
Christians may prefer to-use some other word to describe these fixed,
permanent principles —rules, guidelines, blueprints for living—but
the Bible calls these principles the law of God. This is why faith in,
respect for, and obedience to the law always accompany true Spiritu-
ality.

Let us return to the question at hand: Does the Bible speak to
every kind of problem that man has? It does. While I believe in the
third possibility, meaning that the Bible provides the only source of
true principles of knowledge, with God the Creator as the only
source of order, I believe also in the fourth possibility: the continuing
validity of many Old Testament laws. We have ignored these laws in
modern times, and we have paid a heavy price. We will pay an even

2. David Chilton, Paradise Restored: A Biblical Theology of Dominion (Tyler, Texas:
Reconstruction Press, 1985}, p. 4.
