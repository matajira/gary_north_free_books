280 THE SINAI STRATEGY

acknowledged the problems men face in complying with its terms.
The law was neither perfectionist nor antinomian, Within the
framework of the law, there is no temptation facing man which is in-
surmountable; God offers a way of escape (J Cor. 10:13).

The normal requirement was that each family should celebrate
the Passover on-the fourteenth day of the first month of the year.
There was an institutional arrangement which enabled each man to ful-
fill the terms of the covenantal celebration. This should convince us
that the celebration of the New Testament version of the Passover,
namely, the weekly communion feast, normally takes place on the
day of rest, but this should not be absolute in every instance.

Worldwide Trade: Passover vs, Dominion?

The question then arises: What if the Hebrew were on a distant
journey? What if he couldn't return to Jerusalem even for the second.
Passover? If the journey were limited to the Middle East, there could
be time available to return. But what if the Hebrew were visiting
North America on a trading mission? They did journey this far in
the days of Solomon, although conventional historians refuse to face
the evidence. A remarkable piece of evidence for just such a jour-
ney is the Los Lunas stone near Los Lunas,*? New Mexico. The
alphabet used was a North Canaanite script which was in use as
early as 1200 B.c., and would have been no later than 800 B.c.*
Here is what the inscription says:

Tam Yahweh your God thal brought you out of the lands of Egypt.
You shall not have any other gods beside me.

You shall not make for yourself any graven image.

‘You shall not take the name of Yahweh in vain.

Remember the day of the Sabbath, to keep ‘it holy.

Honor your father and your mother, so that your days may be long on
the land which Yahweh your God is giving to you.

46. Barry Fell, Bronze Age America (Boston: Little, Brown, 1982).

47. This is the correct name, despite the normal Spanish usage of the article “los’
as masculine and “as” suffixes as ferninine.

48. This estimation was made by someone who was not familiar with, or who
chose ta ignore, Velikovsky’s reconstructed chronology. But it needs to be under-
stood that after 750 2.c., Velikovsky’s dating corresponds closely with the conven-
tional dating of the ancient world. Thus, the late conventional date of 800 3,c.
would be placed by Velikovsky’s dating in roughly the same era. In short, the in-
scription was made no later than the era of the prophet Isaiah.
