Sabbath and Dominion 87

so the renewed Adamic Covenant at Sinai pointed forward to the work of
Christ and the Future New Sabbath Covenant to come.

It was the third day, and the third month (29:1, 16): For the significance
of this we nced to look at Numbers 19:11-12: The man who is unclean from
contact with a corpse is ‘to be cleansed on the third day and again on the
seventh day. ‘his double resurrection pattern is found all through the Scrip-
tures. For instance, in John 5:21-29, Jesus distinguishes a first resurrection,
when those dead in sin will hear the voice of Christ and live (v. 25); and a
second resurrection, when those in the grave will come forth to a physical
resurrection. (v, 29). The first resurrection comes in the middle of history to
enable men to fulfill the duties of the old creation, The second resurrection
comes at the end of history to usher men into the new creation.

Jesus was raised on the third day, thereby inaugurating the New Cove-
nant in the midst of che week of history. Christians live between the third
and seventh days'of history, Spiritually resurrected and in the New Cove-
nant, but physically mortal and assigned to complete the tasks of the Old
Adamic Covenaril. The fact that the law was given at Sinai on the third
day, and in’the third month, was a provisional anticipation of the third-day
resurrection yet to come in Christ.

The third-day resurrection was only provisional under the Old Cove-
nant, so it had to be repeated year after year. Thus, every year, the third
day after Passover, there was a waving of the first fruits before the throne of
God (Lev: 23:5, 7, 10, 11). This was a prophecy of the resurrection of our
Lord Jesus Christ, which came three days after Passover. Jesus’ third-day
resurrection, however, was not provisional but definitive, and never to be
repeated 18

The Prophesied Thousand Years

Consider the millennial implications .of Jordan’s analysis.
Sometime around the year 2000, there will be a one-time-only fusion
of-symbolic “days.” The sixth “day” (millennium) comes to a close.
This symbolically closes the six days of fallen mankind’s labor. The
sixth was also the day on which God created man and delivered the
dominion mandate to him. As Jordan points out, the sixth day. was
also the day of re-covenanting between God and man at Sinai. On
that day, God's law was delivered to Israel.

Simultaneously with this closing of the sixth “day” will be the
closing of the second “day” (millennium) since the death of Jesus
Christ, On the third day, He was resurrected. On this third day, true

15. James B. Jordan, 7‘he Law of the Covenant: An Exposition of Exodus 21-23 (Tyler,
“Lexas: Inslitute for Christian Economics, 1984), pp. 56-58.
