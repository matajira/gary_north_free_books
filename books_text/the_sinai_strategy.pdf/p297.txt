The Economic Implications of the Sabbath 21

the recreations of life. It has become popular in recent years to dis-
miss this picture of the Puritans as a myth. It is not a myth. It is
rooted in reality—at least the reality of the documentary record.

I spent several years working with the primary sources of the col-
onial American Puritans, especially their sermons and legislative
records. If someone were to tell me that the Puritans were a fun-
loving lot because of their fondness for taverns, I would reply:
“How do you know they enjoyed taverns?” There is only one reason-
able reply: “Because I read all the laws they passed regulating them.”
In every town and in the records of the Massachusetts Bay Colony, a
recurring legislative concern. was the control of taverns: hours they
could be open, the kinds of games that could be played in them (they
were obsessed with the evils of shuffleboard, and the laws repeatedly
took notice of this notorious deviant behavior), restricting access to
taverns by apprentices, and so forth. The legislators did their best to
minimize the operations of these dens of iniquity.

What about Puritan poetry? There was Milton, whose reputa-
tion as a Puritan is somewhat questionable (though I think on the
whole he was in the Puritan camp}. There was no one else of com-
parable reputation. Anne Bradstreet, the poetess of North Andover,
Massachusetts, had a collection of her poems published without her
knowledge in England in 1650, and twenty-eight years later, a larger
collection was published in Boston. By this time, she had been dead
for six years.“ The other great colonial Puritan poet was Edward
Taylor, who forbade his heirs to publish any of his poems, and which
did not see publication until 1939. His manuscript book was not even
discovered in the Yale University Library until 1937.5* Not until 1968
was a full-length edition of seventeenth-century American poetry
published. Meserole’s comments are appropriate: “In New England
particularly, there were strictures against too consummate an atten-
tion to poetry. ‘A little recreation,’ asserted Cotton Mather, was a
good thing, and one should not contemplate an unpoetical life. But
to turn one’s mind and energies wholly to the composition of verse
was to prostitute one’s calling, to risk opprobrium, and most impor-
tant, to lose sight of the proper balance God envisioned for man on
earth. The sheer quantity of verse that has come down to us proves
that these strictures were not completely heeded. It is similarly clear

33, Hudson T. Meserole, “Anne Bradstreet,” in Meserole (ed.), Scoenteenth-Century
American Postry (Garden City, New York: Anchor, 1968), p. 3.
34, Ibid., p. 19.
