The Economic Implications of the Sabbath 291

during the period they spent in the wilderness. We.are not told specific-
ally in the Mosaic law that cooking was permanently abolished on the
seventh day. At the same time, the experience in the wilderness was to
have given them indications concerning the cooking schedule preferred
by God, and that schedule involved storing up cooked food the day be-
fore the sabbath, just as it involved storing up extra firewood.

When we come to the New Testament, we face a more difficult
problem. The Lord’s day should be timed from dawn to.dawn. The
communion meal in the first-century church was an evening meal.
Must we conclude that this communion meal, the central: weekly
event in the life of the corporate church, prohibited the eating of
freshly cooked food? Does the Sunday evening meal have to be cooked
on Saturday night or even earlier on Saturday? Would we not expect
the wives in the early church to have prepared their best meal of the
week for this night? On the.other hand; is the Lord’s day to become a
day of cooking competition? In modern churches, the existence of
Sunday evening church suppers stands as a testimony to sabbatarian
contusion.

Meals, whether cooked or leftovers, leave messes behind, What
are wives to do, leave the crumbs lying on the table for the benefit. of
rodents and insects? But if they clean up the table and kitchen in
their households, haven't they violated the Lord’s day? If so, is this a
case of Lord’s day desecration comparable to the desecration of the
priests, that is, blameless? May they use hot water to wash dishes?
Can they legitimately (blamelessly) draw such hot water out of the
tap? If so, someone is on duty at the local public utility company,
serving the needs of the Lord’s day-desecrating wives. It takes power
to heat water. It also takes a water company to deliver water that is
to be heated.

The modern church has given no systematic thought to these
issues. The Protestant churches have their Sunday evening covered
dish suppers, and no one goes away feeling guilty about having
cooked on the Lord’s day or having caten cooked food on the Lord’s
day. But the ethical question still remains: Is cooking on the Lord’s
day a sin?

Evading the Problems

These are relevant issues. The fact that they are not discussed
seriously by modern defenders of the Puritan-Scottish sabbath is an
indication of the political impotence of those who defend it. They
