184 THE SINAI STRATEGY

differentiate one product or service from competing products or ser-
vices, specialized sellers could begin to invest greater quantities of
capital in the enterprise. These family businesses could be more con-
fident of selling into a stable market, since product or service loyalty
among buyers was beginning to develop. The Aigh costs to consumers
of shopping around, of searching for alternative product or service
substitutes, make name-brand identification a convenient economic
shorthand. This is as true today as it was in the ancient world.

As certain families, especially those engaged in craft production,
found ways of differentiating their products from those of their com-
petitors, they could convert this recognition in the marketplace into
money or bartered goods. Perhaps a family head possessed a unique
skill, or special knowledge of marketing. Others may have become
known for their sense of honor. These family traditions became
capital assets. The family name, which could be stamped on many
products, became an early form of familistic capital. This was
especially true among artists. With greater name identification, con-
sumers found it less expensive to identify desirable products. This
helped to extend market transactions, for consumers could make
more purchases because of the savings that resulted from the better
information to consumers as a result of name-brand identification,

This analysis is-a form of “hypothetical history.” We cannot find
ancient family budget records that say, “Today, we saved 10% of our
monthly budget because we reduced our search costs.” We know that
certain craftsmen gained reputations for excellence. We then analyze
this fact from the point of view of economics. We know that people
want to reduce their costs of searching for bargains. We know that in
the Hebrew commonwealth, the preservation of the family name
was of paramount importance, Such a concern always has economic
implications. We can conclude that buyers understood that pro-
ducers would want to maintain their names’ good reputations. Thus,
buyers could adopt the “short cut” of substituting the producer’s
name for an involved testing of the product or an expensive search
for information.

The wider the reputation of a seller, the wider is his market. When a
product’s reputation is high, additional marketing expenditures may
be reduced without reducing sales. This makes international trade
less expensive, just as it makes domestic trade less expensive. But in
foreign trade, reputation for quality counts for more, since
foreigners may have great difficulty in returning a defective product
