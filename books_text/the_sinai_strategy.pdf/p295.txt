The Economic Implications of the Sabbath 269

costs effectively insulate interior rural communities from the benefits
and competition of world markets. Trade is overwhelmingly aimed
at high-value, low-volume products bought and used by the rich, the
powerful, and the well-connected. The division of labor is minimal,
and output-per-unit-of-resource-input is low. Per capita productivity
is low, and therefore per capita income is low.

The Puritan-Scottish interpretation of the Mosaic sabbath laws
could be enforced in ancient Israel without wiping out whole seg-
ments of the economy only because per capita income was low, eco-
nomic expectations were low, and the international market for goods
did not affect most of the products in use in rural areas. More than
this: if the Puritan-Scottish view of the Mosaic.sabbath laws had re-
mained in force, the sabbath-honoring economies of the world would
probably still be predominantly rural, characterized by a minimal
division of labor. The rhythm of the one-six week is suitable only for
rural societies, if that rhythm is mandatory on all citizens on the same
day.

Do we want to.argue that God has determined that the low divi-
sion of labor of rural life is a moral requirement forever? Do we want
to argue that God has created limits on the development of world
trade in the form of a rigid sabbath code which forces all men within a
covenantally faithful society into an identical one-six weekly pattern?

As far as I am able to determine, questions like these have not
been dealt with by defenders of a New Testament version of the
Mosaic sabbath. Only because those who defend such a view of the
Lord’s day have seldom been in positions of formulating or enforcing
national economic policy, have they been able to avoid the hard real-
ity of sabbatarianism. They have not thought through the economic
implications of their position.

When I raised some of these questions in. the appendix that ap-
peared in Institutes of Biblical Law in 1973, I expected to see strict sab-
batarians respond, to propose answers, or at least modifications in
their position that would enable them to avoid the obvious implica-
tions of their position. So far, I have waited twelve years to reccive a
single letter or see a single refutation in print. Rushdoony’s bgok has
been read by many influential theologians and Christian leaders,”

32. What is rather amazing is how few copies Institutes of Biblical Law has sold, in
comparison to its influence. It has unquestionably begun to reshape the thinking of
numerous conservative Christian Icaders, yet as of early 1985, it had. sold under
18,000 copies in twelve years, Its press runs have always been small — sometimes as
