96 THE SINAI STRATEGY

their portion of the earth to Ged’s glory. Long life is an integral part of
the dominion covenant.

Since the fulfillment of the dominion covenant involves filling the
earth, it is understandable why long life should be so important. It is
one critical factor in the population expansion which is necessary to
fulfill the terms of that covenant, the other being high birth rates. God
has pointed clearly to the importance of the family — indeed, the central
importance of the family—in fulfilling the terms of the dominion cove-
nant. The parents receive the blessing of children (high birth rate),
and the children secure long life by honoring their parents. Or, to put
it even more plainly, a man gains the blessing of long life, including
the ability to produce a large family, by honoring his parents. The
way in which the people of a civilization define and practice their fam-
ily obligations will determiné their ability to approach the earthly
fulfillment of the dominion covenant. Without a close adherence to
this, the fifth commandment, no society can hope to receive and keep
the capital. necessary to fulfill the terms of the dominion covenant,
especially the human capital involved in a population explosion.

Parental Sovereignty

Parents possess limited, derivative, but completely legitimate
sovereignty over their children during the formative years of the chil-
dren’s lives. When children reach the age of civil responsibility, one
sign of their maturity is their willingness to establish families of their
own (Gen, 2:24). Responsibility therefore steadily shifts as time goes
on. Eventually, the aged parents transfer economic and other re-
sponsibilities to their children, who care for them when they are no
longer able to care for themselves, The man in his peak production
years may have two-way financial responsibilities: to his parents and
to his children. Maximum responsibility hits at an age when,
because of economic and biological patterns, a man attains his max-
imum strength, This shift of responsibility is mandatory, given the
mortality of mankind. The Bible provides guidelines for the proper
transfer of family responsibility over time.

The requirement that men honor their parents preserves the con-
tinuity of the covenantal family, and therefore it preserves the continuity of
responsibility. The totally atomistic family unit is probably impossible;
where it exists, the culture which has created it will. collapse. Mutual

 

3. For an historical example of just such a social collapse, see Colin Turnbull, The
Mountain People (New York: Simon & Schuster, 1973).
