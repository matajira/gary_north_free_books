Sabbath and Dominion 85

forward to dominion. Dominion begins with our labor on the day fol-
lowing the Lord’s day of rest, just as it was supposed’ to begin for
Adam, The communion meal, like the Passover meal, is to be cele-
brated in, the evening. Also like the Passover meal, it looks forward to the
next morning. But the victory is behind us. Deliverance came defini-
tively at Calvary, We are strengthened in our faith the sight before
we are to go forth to exercise dominion, just as the Hebrews were
strengthened in body by their Passover meal the night before God
delivered them from bondage.

The Sabbatical Year

‘The church has never honored a:sabbatical year, nor has any
civil government. The land is not rested, debts are not cancelled,
and the whole law is not read publicly before the gathered nation.
Why not?

The New Testament has internalized the locus of sovereignty for
the enforcement of the sabbath. Men are to rest the land, but not as
a nation, and not simultaneously. The civil government honors
Paul’s dictum that some regard one day (or year) as-equal to any
other, and some regard one as special, to-be set apart for rest:

A farmer might decide to rest his whole farm one year in seven,
An alternative arrangement would be to rest one-seventh of his land
each year. A Dutch-American immigrant informed me that at the
beginning of the twentieth century, it was:common in Holland for
land owners to lease their agricultura] land with a provision that
each year, one-seventh of the Jand would not be planted.

With respect to debt, the civil government does not select one
year in seven for the cancellation of all debts. What it probably
should do is to declare that it will not enforce any debt obligation
beyond the seventh year, on the assumption that long-term debt is
prohibited by the Bible. This would also apply to. the government's
own debt structure. This would prohibit the building up of huge
debt obligations that are never intended to be repaid. It would also
reduce a major political impetus for iriflation:. debtors trying to
defraud creditors. It would remind men that we are limited creatures
and should not presume to look beyond seven years of productivity.
Men might still continue to write debt contracts beyond seven years,
but these contracts would not be enforceable in a civil court, and
creditors would face higher risks of default. Creditors would become
wary. This limitation on contracts is the biblical society’s equivalent
