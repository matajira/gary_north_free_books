180 Marx’s Religion of Revolution

The price system is the heart of the free market economy. It
is the mechanism by which supply is balanced with demand
{assuming that there is no inflation of the currency by either the
banking system or the civil government).’ It informs the con-
sumer of the relative availability of economic goods; simultane-
ously, it alerts the entrepreneur to the success or failure of his
previous economic planning. The private firm is able to operate
in a rational manner because prices provide the vital data con-
cerning demand, interest rates, alternative possibilities for in-
vestment, and the present cost of labor and raw materials. From
the point of view of the entrepreneur, prices are indispensable;
they enable him to estimate the value of future sales, and this in
turn permits him to make a rational decision concerning the
purchase of capital goods — goods of the so-called “higher or-
der.” The overall economic system can thus allocate its scarce
resources according to consumer demand; a balance of produc-
tion can be established between consumers’ goods and produc-
ers’ goods. Without this pricing mechanism, men would be al-
most blind in their economic decisions; nothing beyond a very
primitive subsistence economy could be possible. Long run eco-
nomic planning of any complexity would be out of the question.

Marx on Money

Of fundamental importance to the price system is a common
medium of exchange. It requires, in short, the existence of money.
Money has taken many forms throughout history, but it must
display four qualities: scarcity, divisibility, durability, and port-
ability. For large payments, or course, gold has fulfilled these
demands most efficiently, since it is very scarce, extremely dura-
ble (it does not corrode), divisible (it can be cut with a knife),
and relatively portable. But whatever form money takes, it must
be present in any economic system that is based on the division
of labor, for without it, there would be no common unit for
making comparisons of relative cost. It is the most important of

 

 

7. CL. Mises, The Theory of Money and Credit (New Haven, Connecticut: Yale
University Press, 1963), ch. 7; Human Action, ch. 20.
