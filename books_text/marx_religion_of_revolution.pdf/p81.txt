1
THE BIOGRAPHY OF A REVOLUTIONARY

For Marx was before all else a revolutionist. His real mission
in life was to contribute, in one way or another, to the overthrow of
capitalist society and of the state institutions which it had brought
into being, to contribute to the liberation of the modern proletariat,
which he was the first to make conscious of its own position and its
needs, conscious of the conditions of its emancipation.

Frederick Engels (1883)!

Karl Heinrich Marx, the bourgeois son of a bourgeois father,
was born in Trier, in what is now Rhineland Germany, on May
5, 1818. He was a Jew by birth, but in 1816 or 1817, his father
joined the state’s official Christian church,? and he saw to it that
his children were baptized into his new faith in 1824.3 After a
brief fling with a liberal, pietistic form of Christianity, young
Karl became a dedicated humanist, He took his humanism to

1. Frederick Engels, “Speech at the Graveside of Karl Mars,” (1883), im Karl
Marx and Frederick Engels, Selected Works, 3 vols. (Moscow: Progress Publishers,
1969), 3, p. 163. [T have dropped the use of the word “vol.” in the footnotes to
Marx’s various works, The Arabic numeral following the title of the book or the
year of its publication is the volume number.]

2. [Robert Payne, Marx (New York: Simon & Schuster, 1968), p. 21; Boris
Nicolaievsky and Otto Maenchen-Helfin, Keri Marx: Man and Fighter (London:
Methuen, 1936), p. 5. The latter book dates Heinrich Marx’s “conversion” as
sometime between the summer of 1816 and the spring of 1817. Reminder: I use
brackets to indicate material added in 1988.]

3. [Payne, Marx, p. 21; Nicolaievsky, Karl Marx, p. 6. Franz Mehring incor-
rectly confuses the date of Heinrich’s “conversion” to Christianity, 1817, with the
date of his children’s baptisms, 1824: Franz Mehring, Karl Marx: The Story of His
Life (Ann Arbor: University of Michigan Press, [1933] 1962), p. 1.]

7
