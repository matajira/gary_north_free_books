Preface litt

tise on differential calculus and various other mathematical manu-
scripts; he learned Danish; he learned Russian. Among his pa-
pers Engels, who knew only too well the defenses behind which
Marx barricaded himself, found ‘over two cubic meters of books
on Russian statistics alone.’78 The word ‘excuse’ appears even
ina letter from Marx himself to the Russian translator of Capital;
in it he counts himself lucky that publication in Germany is
prevented by anti-socialist legislation and that fortunately fresh
material from Russia and the United States provides him with
the excuse he is looking for to continue with his research instead
of finishing the book and publishing it.”79 Raddatz then reveals
that even this excuse was a lame one; the Prussian censors
regarded Marx’s books as social-democratic or non-revolution-
ary communism (which boggles the imagination), and so there
was no legal excuse for prohibiting their importation.®° What I
argue is that this was not Marx’s mid-life crisis; this was his
inconsistent-system crisis.

There is true irony here. In constructing his critique of
capitalism, Marx explicitly adopted the classical economists’
erroneous intellectual legacy, the labor theory of value. The
classical economists argued that the source of all economic value
is human labor. We date the advent of modern economics with
the “marginalist revolution” of the early 1870's, when three
economists ~ England’s William Stanley Jevons, the Switzerland-
residing French economist Léon Walras, and Austria’s Carl
Menger ~ abandoned the labor theory of value and adopted a
subjective theory of value.®! One error that results from the labor
theory of value is the idea that activity is a meaningful economic
substitute for production. The obvious nature of the error should

78. Engels to Friedrich Adolph Sorge, 29 June 1883.

79. Marx to Nicolai F. Danielson, 10 April 1879, All cited in Raddatz, Kart
Marx, pp. 236-37.

80. Ibid., p. 287.

81. Karl Pribram, A History of Economic Reasoning (Baltimore: Johns Hopkins
University Press, 1983), ch. 18; RS. Hovey, The Rise of the Marginal Utility School,
1870-1889 (Lawrence: University of Kansas Press, 1960); Emil Kauder, A History
of Marginal Utility (Princeton, New Jersey: Princeton University Press, 1965).
