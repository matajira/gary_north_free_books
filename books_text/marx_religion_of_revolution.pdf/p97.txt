The Cosmology of Chaos 23

his standards are constantly changing through time; his refer-
ence points are always shifting. Man still faces the chaos of flux
on the one hand and the despotic claims of absolute law on the
other. Both claims are made on him simultaneously; they are
philosophical corollaries of each other.

These philosophical questions are important, in spite of the
fact that they appear to be quite nebulous. The discussions
concerning the so-called “New Morality” revolve around this
basic question of the permanence of ethical standards. The cur-
rent [1968] “hippie” movement is vitally concerned with the
whole problem of social norms, contemporary legal codes, and
the effects which science has on the freedom of man. In 1967, the
leader of an underground hippie secret society in San Francisco,
the Psychedelic Rangers, granted an interview to a Newsweek
reporter. The motto of the Rangers, he said, is this: “The psyche-
delic baby eats the cybernetic monster.” By this, he explained,
the group means that the modern LSD-drug culture will sweep
over the technological civilization of the West, The crushing
burden of burcaucratized, computerized life will be liberated by
men and women seeking escape through the use of drugs, much
as the citizen of Huxley’s Brave New World used “soma.” This
hippie expects to have both internal freedom and the wealth
provided by mass production: “That doesn’t mean back to sav-
agery. It doesn’t mean we’re going to tear down all the computer
systems. It’s only a question of the mind being tuned enough,
so that it’s involved in making things better. And this will result
in a civilization that is super-beautiful. We're out to build an
electric Tibet.”® The best of all possible worlds: the mass produc-
tion of the West and the mystical retreat of the East. Kant’s
dream is going to be fulfilled in a psychedelic America: there
will be a unification of the realm of internal freedom and the
cybernetic realm of science,

Hegel
In the 19th century, the résolution of Kant’s dualism was

6, Newsweek (Feb, 6, 1967}, p. 95.
