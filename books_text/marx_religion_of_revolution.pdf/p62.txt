Ixii Marx’s Religion of Revolution

cious, but the law commands that my style be modest. Grey, all
grey, is the sole, the rightful colour of freedom. Every drop of dew
on which the sun shines glistens with an inexhaustible play of
colours, but the spiritual sun, however many the persons and
whatever the objects in which it is refracted, must produce only
the official colour!”'! Prussian censorship is still with us: the grey
sludge style that is still required for the Ph.D dissertation ~ an
invention of the Prussian state education system ~ as well as for
academic discourse in general. Marx was partially hampered
by this tradition early in his career, and so was J. But within a
few years of having received our doctorates, we both escaped.

Nevertheless, in reviewing this manuscript for publication, I
was impressed by the stylistic similarities it has to at least my
more academically oriented books (¢.g., The Dominion Covenant:
Genesis). I was confident in 1966-68 that I knew what I was
talking about when it came to Marx’s thought, and that confi-
dence was reflected in the book’s style. I have not changed my
mind since then. {If I had, it would have constituted one of those
rare conversion experiences.) The only major change in my
thinking regarding Marx is my present reduced opinion of his
intellect, Today, I would not rate him as a profound thinker.
He was at best a third-rate economist, and he was seldom at his
best. His writing style reveals a grotesque combination of aca-
demic drudgery and infantile temper tantrums, in contrast to
Engels, who wrote with verve.

In my earliest high school and undergraduate term papers,
I used short paragraphs. In my graduate school papers, I tended
to use long paragraphs, Once I escaped academia, my style
reverted to a midway point. At the same time that I was writing
this book, I was writing book reviews for my friend Joe! Blain,
who was the book review editor of the Riverside (California)
Press-Enterprise, and that training forced me to shorten my para-
graphs. (I strongly recommend book reviewing as the best way
to begin a writing career — the grown-up’s version of the high

101. Karl Marx, “Comments on the Latest Prussian Censorship Instruction”
(1842), Collected Works, 1 (New York: International Publishers, 1975), p. 112.
