118 Marx’s Religion of Revolution

modity. It has a very definite flaw, since on this definition it is
impossible to explain the phenomenon of rent. Many economic
goods have neither been produced by human labor nor produced
for any market, yet they command a price. Marx struggled
unsuccessfully with this problem: “The waterfall, like the earth
in general, and like any natural force, has no value, because it
does not represent any materialized labor, and therefore it really
has no price, which is normally but the expression of value in
money. Where there is no value, it is obvious that it cannot be
expressed in money. This price is merely capitalized rent. The
ownership of land enables the landowner to catch the difference
between the individual profit and the average profit.”

The problem is not solved: by an appeal to land-ownership,
If it is true that the waterfall “really has no price,” then how
does it command a price? If it is true that “where there is no
value, it is obvious that it cannot be expressed in money,” then.
why is it expressed in money? By definition, the waterfall con-
tains no value, since value was defined by Marx as congealed
labor time (as distinct from use-value), yet he was forced to
admit that a waterfall may, in reality, command a price anyway.
There is clearly a contradiction here. It stems from his strangely
narrow definition of “commodity” which for him did not mean
just an economic good, but only an economic good produced by
human labor for a market. The 19th-century economist, Eugen.
von Béhm-Bawerk, commented on this strange definition:

From the beginning he only puts into the sieve those exchangeable
things which contain the property which he desires finally to sift out
as “the common factor,” and he leaves all the others outside. He acts
as one who urgently desiring to bring a white ball out of an urn takes
care to secure this result by putting in white balls only. That is to say
he limits from the outset the field of his search for the substance of the
exchange value to “commodities,” and in doing so he forms a concep-
tion with a meaning narrower than the conception of “goods” (though
he does not clearly define it}, and limits it to products of labor as

23. Ibid., 3 (Chicago: Charles H. Kerr, 1909), p. 759. [Capital, 3 (New York:
International Publishers, [1967] 1974), p. 648.)
