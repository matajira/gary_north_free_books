Appendix A
SOCIALIST ECONOMIC CALCULATION

The problem of economic calculation is the fundamental problem of
Socialism. That for decades people could write and talk about
Socialism without touching this problem only shows how devastating
were the effects of the Marxian prohibition on scientific scrutiny of
the nature and working of a socialist economy.

Ludwig von Mises (1922)'

‘What is economic science? This question has baffled even the
best of economists for at least two centuries. Airtight definitions
are, of course, impossible; no matter what the abject of a defini-
tion may be, neither human language nor thought permit abso-
lutely rigorous definitions, Nevertheless, we can at least ap-
proach a definition narrow enough to be useful, excluding enough
extraneous material to allow some kind of understanding. In the
past, many definitions of economics have been popular: the
science of wealth, the study of welfare, and the science of human
avarice? In this century, Lionel Robbins has provided us with
the most generally accepted definition: economics is the science
of economizing; it is the study of the allocation of scarce resources
among competing ends. His book, The Nature and Significance of
Economic Science (1932), has become the standard work on the
epistemology of economics. He spelis out his position in no

1. Ludwig von Mises, Socalism (New Haven, Connecticut: Yale University
Press, [1922} 1951), p. 135,

2, Israel M. Kirzner surveys the vatious definitions of economics in his book,
The Economic Point of View (Princeton, New Jersey: Van Nostrand, 1960).

177
