242 Marx’s Religion of Revolution

In March of 1843 Marx lost his job, yet was married in
June — not to some proletarian, but to Jenny von Westphalen,
his old sweetheart, the daughter of a high and respected Prussian
official. Their long honeymoon was spent on a tour through
Switzerland where, Jenny later related, they literally gave money
away. Jenny’s mother had given the couple a small legacy for the
trip.

Marx spent the next few months reading and writing articles.
(The journal for which he was writing went through one issue,
and was immediately confiscated by the authorities, never to be
revived.) At the end of the year he and his new bride went to
Paris. These are hardly the activities of some starving proletarian
philosopher.??

Poor Little Rich Boy”!

There are a handful accounts of Marx’s financial status
during the years 1844-48. All of them point to the samc fact: he
lived high on the hog. I have pieced together the fragmentary
and sometimes conflicting data as best I can. In March of 1844,
while he was living in Paris for about fourteen months, Marx’s

 

Engels when he reached the end of this forced labour and 1 saw what he must have
gone through all those years. I shall never forget the triumph with which he
exclaimed: ‘For the last time!’ as he put on his boots in the morning to go to the
office for the last time. A few hours later we were standing at the gate waiting for
him. We saw him coming over the little field opposite the house where he lived.
He was swinging his stick in the air and singing, his face beaming. Then we set the
table for a celebration and drank champagne and were happy. I was then too young
to understand all that and when I think of it now the tears come to my eyes.” Marx
and Engels Through the Eyes of Their Contemporaries (Moscow: Foreign Languages
Publishing House, 1972), p. 163. How touching!

Eleanor committed suicide in 1898. (Her sister Laura died the same way in
1911,) It is notable that Eleanor’s estate was valued at [pounds] 1,909, which was
a small fortune in 1898. She had inherited Marx’s estate, mainly his books’
royalties, Her bankrupt communist husband then inherited everything. Payne,
Marx, p. 530. Not a bad windfall for a bigamist who had been secretly and illegally
married the year before to another woman, a 22-year-old actress, to whom he
immediately returned {ibid., pp. 525, 530-31).

20, Payne, Marx, p. 92,

21, Lam writing this subsection in July, 1988,
