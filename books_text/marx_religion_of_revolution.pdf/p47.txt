Preface xlvii
The Posthumously Published Marx: Dead Ends

Marx was a Ph.D-holding academic in the classic Germanic
mold, the archetypal Dr. Drudge. He kept in his files a lifetime
of unpublished notebooks that are filled with some of the most
turgid prose in history, and because Lenin’s Bolsheviks won in
Russia in October of 1917, scholars feel compelled to plow
through many published volumes of Marx’s posthumously ed-
ited notes, letters, and polemical tirades, in order to avoid being
accused of not having done their homework. Worse, they some-
times argue as though these unpublished notes were actually
more important in understanding “the trae Marx” than his
published works. (“A niche, a niche, my kingdom for an aca~
demic niche!”} A representative example of this academic prefer-
ence for notebook-sniffing is the amount of attention paid to
Marx’s 1857-58 notebooks, published in German only in 1933,
and in English in 1973, the Grundrisse, which is appropriately
subtitled, Foundations of the Critique of Political Economy. Translator
Martin Nicolaus’s assertion in his 59-page Foreword is typical:
“The Grundrisse challenges and puts to the test every serious
interpretation of Marx yet conceived.” In other words, “Hey,
everybody, look what I found!”

It seldom seems to have occurred to these people that the
reason the Grundrisse was not published in Marx’s lifetime is that
Marx did not regard it as worth publishing. Every college stu-
dent knows that it would be unwise to submit his notes and first
drafts along with his term paper, but scholars of Marxism who
are desperately searching for an academic niche conveniently
ignore the obvious. You can imagine an author’s outrage if he
had his notes for his books stolen by a colleague who then
published them along with an introductory essay that announced
to the world: “These notebooks are really more representative
of this man’s ideas than his published books are.” The thicf
would be hooted into silence. But once the victimized author is

64. “Foreword,” Karl Marx, Grundrisse (New York: Vintage, 1973), p. 7. The
Grundrisse appears as Volume 28 of the Collected Works (New York: Taternational
Publishers, 1986).
