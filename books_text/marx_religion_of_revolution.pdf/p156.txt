82 Marx’s Religion of Revolution

poor people without needs and wants. ‘Community [in this case]
is only a community of labor and equality of salary paid out by
the Communist capital, the community as the universal capital-
ist.’ The obvious indication of this kind of brute Communism,
which Marx understood as a primitive generalization of private
property, is the Weibergemeinschaft, the community of women.
This Communism is inhuman, not because it destroys capitalism
but because it makes capitalism broader, more radical and more
absolute. It does not transcend capitalistic society but even lags behind
some of the more progressive aspects of private property. Nevertheless
Marx thought at this time that at least from the theoretical point
of view this kind of Communism was a stage through which one
necessarily had to pass.”!5!

Dialectics: State or No State

Here is one of the dialectical results of the Marxian system:
itis simultaneously a call to total revolution against the state and
a program to create an absolute state. Marx’s ultimate vision
was a hope in a society which needed no state, yet it was to take
a period of state-planned inhumanity to bring this world into
being. Marx tried to compensate the total authoritarianism with
a vision of a truly human society to come; this was Marx’s “pie
on the earth” dream. “But these defects are inevitable in the first
phase of communist society as it is when it has just emerged after
prolonged birth pangs from capitalist society.”!5? The “mature”
Marx of 1875 then drew his picture of the coming paradise: “In
a higher phase of communist society, after the enslaving subordi-
nation of the individual to the division of labour, and therewith
also the antithesis between mental and physical labour, has
vanished; after labour has become not only a means of life but
life’s prime want; after the productive forces have also increased
with the all-round development of the individual, and all the
springs of co-operative wealth flow more abundantly ~ only then

151. Iring Fetscher, “The Young and the Old Marx,” in Lobkowicz (ed.), Marx
and the Western World, pp. 29-30,
182. Gritigue of the Gotha Program, in Selected Works, 3, p. 19.
