The Cosmology of Chaos 105

expands, because his wants increase; but at the same time the forces
of production increase, by which these wants are satisfied. The freedom
in this field cannot consist of anything else but of the fact that social-
ized man, the associated producers, regulate their interchange with
nature rationally, bring it under their common control, instead of being
tuled by it as by some blind power; that they accomplish their task
with the least expenditure of energy and under conditions most ade-
quate to their human nature and most worthy of it, But it always
remains a realm of necessity, Beyond it begins that development of
human power, which is its own end, the true realm of freedom, which,
however, can flourish only upon that realm of necessity as its basis.
The shortening of the working day is its fundamental premise.?*+

Marx went around the issue without ever confronting it
directly: how can society regulate its interchange with nature in
a rational manner, thus bringing it under society’s common
control, while still maintaining the freedom of man within that
society and within that “rationalized” universe? The material
realm of production, Marx admitted at last, “remains a realm
of necessity.” Only beyond production can mankind find true
freedom, yet the whole foundation of the Marxian system is that
man is man only in the sphere of free, voluntary productivity.2!5
After struggling with one of the most. profound philosophical
problems which can confront the secular thinker, and after rais-
ing the whole question of production in the future society, Marx
resolved the issue with these words: “The shortening of the
working day is its fundamental premise.”?!6 The paucity of the
answer is staggering, incredible! If so much misery had not been
launched by Marx’s labors for the forces of revolution, and if so
many lives had not been destroyed in the name of Marx, that
answer would be amusing in its pathetic quality.

214, Capital, 3, pp. 954-55. [Capital, 3, p. 820.]

215, On the contradiction within Marxism between free, unspeciatized produc-
tion and mechanized factory life, see Robert GC. Tucker, “Marx as a Political
Theorist,” in Lobkowicz (ed.), Afarx and the Western World, pp. 130-31,

216, Marx, Capital, 3, p. 955. [Capital, 2, p. 820.4
