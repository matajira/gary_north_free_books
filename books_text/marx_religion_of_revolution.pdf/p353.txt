Engels, x1
entrepreneurship, Ixxiii
Hayek on, 195n
Mars’s classes, 58-59
Marx’s predictions, 113
need for simplicity, 66-67

Schwarz, Fred, xxiv

science, 56, 193

science-ideal, 24

secret societies, 76-78

Seidenberg, Roderick, 53

Seligman, E. R. A., 110-111

Senghor, Leopold, 3

Sennholz, Hans, 154n

service, 166-168

Shafarevich, Igor, inviii

Sherman, Howard, |

shortages, 102

Shoul, Bernice, 139n

shuffle, xvi

size of firms, 126-127

stave labor, 209

Slochower, Harry, 5in, 4n

slogans, xxx

Smith, Adam, 149, 153

social science, xxviii

socialism, 59

revolution &, 73

socialogy, 153

Solzhenitsyn, A., 173-174

sovereignty, xiv, xxii

Sowell, Thomas, 130n.

stagnation, 193-194

Stalin, Joseph, 166

starvation, 204

state
defined, 100-101
dictatorship of proletariat
disappearance of, 102-3
origin, 49

statistics, xii, 227-229

stage theory, 95

Strauss, D. F., 27

Index 279

subsistence, 194

Superstructure, xviii, 65, 166
superstructures, 48, 50

surplus value, 120-21, 145-149, 161
Sweezy, Paul, 128, 152, 153

tacks, 222

tactics, 94-95,

technology, 193-194, 205-206
television, 193n

terror, 206

textbooks, 227

theory and practice, xvii
Third World, xxxix

Tillich, Paul, 32

time, 87, 160, 162, 169-170, 170-172
tolkachi, 218

Toulmin, Stephen, xii

trade cycle, 141-142

trade unions, 73, 92
Treadgold, Donald, xix
Trotsky, xxxi

truth, 55-56

Tucker, Robert C., 85, 89

uncertainty, 157, 159

unknown, xv

USSR
agriculture, 207, 209n, 230
arbitrariness, 226
black market, 218
blat, 218
borrowed technology, 205-208
bureaucracy, 221
coercion, 206, 210
competition, 221
computers, 197
confidence, 169-170
consumption, 210-212
decentralization, xxxvi-xxxvii, 215,

219, 224, 231

economic revolution, 204
Five-Year Plan, 214
