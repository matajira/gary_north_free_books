256 Marzx’s Religion of Revolution

a lesson for others. Had bourgeois London not given him a place
to hide and work ~ analogous to the Old Testament’s cities of
refuge — we would never have heard of this third-rate materialist
philosopher and fourth-rate classical economist. In short, Marx
did his best to undercut the very foundations of his own exis-
tence, And today we find that in those nations that are officially
Marxist, anti-Communist ideas are the coin of the realm, Noth-
ing temains of Marxism except its quest for power. Paraphrasing
bourgeois intellectual Lincoln Steffans, the Communists have
seen the future at close range, and it does not work.

“Those who hate me,” says Wisdom in Proverbs 8:36, “love
death.” Karl Marx hated God. Above all, he hated God. He was
therefore ultimately suicidal — economically, politically, and in-
tellectually. Two of his daughters killed themselves. The Revolu-
tion eats its own. But not soon enough.
