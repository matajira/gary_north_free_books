Preface xxix

as unpaid editors — unpaid by me, that is. My overdue thanks
go to the overburdened taxpayers of Governor Ronald Reagan’s
California (who later became the overburdened taxpayers and
national debt carriers of President Reagan’s America).°

American higher education is a highly ritualized process. It
is considered unbecoming on the part of an uncertified young
scholar to challenge the life’s work of a major historical figure,
especially one who has shaped the thinking and lives of large
segments of the professional academic guild. Graduate students
in the social sciences and the humanities generally find it safer
to write narrowly focused, seemingly objective but in fact favor-
able studies of major intellectual figures, if they write about
major figures at all. Two sensible dissertation goals are: 1) pick
a figure you identify with who is forgotten by almost everyone,
and especially everyone on your doctoral committee, or 2) pick
a famous figure whose life’s work is favored by your dissertation’s
primary advisor. I ignored this common-sense strategy. I wrote
a book critical of Marx prior to advancing to candidacy. Fortu-
nately, I was never criticized publicly in any way by my profes-
sors, for which I was grateful. Writing the book also helped me
to win a Weaver Fellowship in 1968 from the Intercollegiate
Studies Institute, which I had unsuccessfully competed for the
year before its publication.

Marx: Graduate Student for Life

Writing a negative critical book is a good post-Ph.D exercise
for a newly certified scholar, but it is a sign of immaturity when
a scholar spends his whole life criticizing the ideas of others,
never putting together a positive alternative, It is evidence that
he has no positive alternative. What I have just described is the
intellectual career of Karl Marx. Marx never stopped writing
long-winded critical refutations of his opponents. His books’

9. Gary North, The Lest Train Out (Fi. Worth, Texas: American Bureau of
Economic Research, 1983), Part I: “The Failure of ‘Reaganomics’; see also the
paper by Murray N. Rothbard, “Is There Life After Reaganomics?” (Washington,
D.C.: Ludwig von Mises Institute, 1987).
