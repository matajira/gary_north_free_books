Soviet Economic Planning 223

it will pay the manager to break up the production process into
numerous and semi-autonomous units, thus creating several un-
necessary production steps in order to profit from as many “value
adding” steps as possible. The central planners have, for half a
century, battled the lower stages of the bureaucracy on such
problems as these, but the system is self-defeating. The enter-
prises are merely following the profit motive; whatever the cen-
trally imposed targets may be, managers will operate to excess
in terms of them.”

For several years (especially after 1961), there has been
considerable discussion of the possibility of instituting quasi-
market pricing for some consumer goods. The name usually
associated with this recommendation is ¥. Liberman. He has
called for three targets: volume of output, assortment variations,
and delivery schedules. Enterprise profits are the only other
guide to be followed, given these three basic considerations.”
This would help to upgrade quality, thus insuring greater con-
sumer satisfaction. He claims that this would still permit full
central planning, but both domestic critics and foreign.observers
have argued that this system, once begun, could not be stopped.”

It is not clear as to where the Liberman experiments in the
130 enterprises will lead. Philippe Bernard believes that the
Status quo will stand; there will be no extension of the market
into areas of industry besides textiles, fur, and clothing, where
Liberman’s recommendations are already prevailing.” Robert

76. Nove, “The Problem of ‘Success Indicators’ in Soviet Industry,” Economica,
XXV (1958); in Leeman (ed.), Capitalism, Market Sociatism, and Central Planning, pp.
78-90.

77, Shaffer, “Ils and Remedies,” op. cit., p. 22, gives a sammary of the Liberman
proposals.

78. For examples of Liberman’s writings with a scattering of some of his critics?
essays, see Myron E. Sharpe (ed), Reform of Soviet Economic Management (2 vols.;
White Plains, New York: International Arts & Sciences Press, 1966). Cf. the
discussion by Marshall 1. Goldman, “Economic Controversy in the Soviet Union,”
Foreign Affairs, XLI (1963); reprinted in Bornstein and Fusfeld (eds.}, The Soviet
Economy, pp. 339-51,

79. Philippe Bernard, “Postscript,” in his Planning in tke Sooiet Union (New York:
Pergamon Press, 1966), p- 295.
