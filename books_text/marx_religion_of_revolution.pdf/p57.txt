Preface Iii

1896.°! Nevertheless, several of Marx’s most prominent ideas to
a great extent have shaped the thinking of twentieth-century
liberal humanists, e.g., atheism, dialectical materialism, eco-
nomic determinism, the class struggle in history, the stage theory
of economic and historical development, historicism, the revolu-
tionary fusion of theory and practice, and most recently, aliena-
tion (especially alienation from one’s own bourgeois origins and
present economic identification). It is Marx’s alienation theme
that captivated the minds of humanist scholars in the late 1960’s
and 1970’s®—men who apparently saw themselves, in their
taxpayer-funded, non-profit, tenured security, as neglected vic-
tims of capitalism who were suffering from alienation in an
alienated’ world. It just had to be the “system’s” fault that they
felt so alienated; otherwise, they were themselves at fault and in
need of repentance and reform rather than the capitalist world.
This is why the academic world “discovered” the notebooks of
the “young Marx.”

The “Young Marx” and the “Mature Marx”

A young man’s book may be worth republishing, depending
on what he has accomplished in the meantime. At the age of 26,
the age I was when this book appeared, Karl Marx wrote a series
of brief manuscripts in 1844 which have become widely known
as The Economic and Philosophic Manuscripts of 1844. (Catchy title!)
They were not published during his lifetime. If you were to read
them with no prior information about who wrote them, you
would understand why they were not published. If they had been
written in the notebooks of someone named Herman Schmidt,
they would never have been published at all. They were first
published in a complete English-language edition in early 1964

91. “The Unresolved Contradiction in the Marxian Economic System,” in
Shorter Classies of Eugen von B3bm-Bawerk (South Holland, Ilinois: Libertarian Press,
1962). This book was first published in English as Karl Marx and the Close of His
System, Tt should probably be translated as: “Upon the Completion of the Marsian
System.”

92. Ci. Bertell Ollmian, Aliscation: Marx’s Concept of Man in Capitalist Society
(Cambridge: At the University Press, [1971] 1975).
