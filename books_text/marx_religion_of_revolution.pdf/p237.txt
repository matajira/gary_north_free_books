CONCLUSION
(1988)!

Thou sawest till that a stone was cut out without hands, which
smote the image on his feet that were of iron and clay, and brake them
to pieces. Then was the iron, the clay, the brass [bronze], the silver,
and the gold, broken to pieces together, and became like the chaff of
the summer threshing floors; and the wind carried them away, that
no place was found for them. And the stone that smote the image
became a great mountain, and filled the whole earth (Dan. 2:34-35).

History manifests a war between two organizational princi-
ples of international civil government, kingdom and empire.
Christ’s international kingdom is decentralized. Satan’s interna-
tional kingdom is centralized, characterized by a top-down bu-
reaucratic system of issuing commands. Satan does not possess
God’s omniscience, omnipotence, and omnipresence, so he must
rely heavily on his own hierarchy (or as C. S. Lewis calls it in The
Screwtape Letters, “the lowerarchy”). The larger that Satan’s em-
pirg becomes, the more overextended he becomes. Like a man
who attempts to juggle an increasing number of oranges, Satan
cannot say no to his assistants, who keep tossing him more
decisions. Eventually, every empire collapses. The principle of
empire cannot Jong sustain human government: church, state,
or family.

In the colloquial phrase, empires always bite off more than
they can chew. The Bible teaches that human empires were
always replaced by other empires, until the advent of Christ’s

1, A shorter version of this essay was published as “The Fifth Kingdom: Battle
for the Title,” Creation Social Science and Humanities Quarterly, X (Spring 1988).

163
