Copyright ©1989 by Gary North

All rights reserved. No part of this publication may be repro-
duced, stored in a retrieval system, or transmitted in any form
or by any means, except for brief quotations in critical reviews
or articles, without the prior, written permission of the publisher.
Published by the Institute for Christian Economics.
Distributed by Dominion Press, Publishers

Post Office Box 8204, Fort Worth, Texas 76124,

Originally published in 1968

Typesetting by Nhung Pham Nguyen

Printed in the United States of America

ISBN 0-930464-15-X
