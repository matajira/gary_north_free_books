The Myth of Marx’s Poverty 243

friends in Germany had collected 1,000 talers for him,”* which
was the equivalent of three years’ income for a Silesian weaver
working 14 to 16 hours a day.” Shortly thereafter, Raddatz says,
another 800 talers arrived.4 To this was added the money he
earned from his 1,800 franc annual salary from Vorwarts,> plus
the 4,000 francs he had received from the “Kéln Circle” of
liberals who had funded the short-lived newspaper, the Rheinische
Zeitung. To this, Raddatz says, should be added another 2,000
francs that Marx received for the sale of proof sets of the Deutsche-
Franzisische Jakrbiicher™ 1 have not found any confirmation of
this additional 2,000 francs, however, so J do not count it. In any
case, his total income, as Raddatz correctly observes, “should
have been enough for several years.”*8 Armold Ruge had sarcasti-
cally remarked in a letter of 1844; “His wife gave him for his
birthday a riding switch costing.100 francs and the poor devil
cannot ride nor has he a horse. Everything he sees he wants to
‘have’ ~ a carriage, smart clothes, a flower garden, new furniture
from the Exhibition, in fact the moon.””%

Marx was expelled from Paris in early 1845. He fled to
Belgium. He was begging for money within a few months, Pre-
dictably, during the next three years in Brussels, he did not earn

 

92. Hal Draper, The Marx-Engeis Chronicle: A Day-by-Day Chronology of Marx @
Engels’ Life @ Activity (New York: Schocken, 1985), p- 29, This is an exhaustive and
indispensable volume.

23. According to an estimate—perhaps exaggerated—by Wilhelm Wolff in
1944. Less than one taler a day was a net working wage for a weaver in Silesia in
1844. See the extract from his 1844 essay in Frank Eyck (ed.), The Revolutions of
1848-49 (New York: Barnes & Noble, 1972), p. 22. Wolff complained that retired
high army officers received pensions of 1,000 talers a year. Wollf was Marx’s
benefactor who left him a smalt fortune in 1864: see below.

24. Fritz J. Raddatz, Karl Mars: A Political Biography (Boston: Little, Brown,
[1975]. 1978), p. 46.

25, Ibid., p. 283, note 20.

26. Iid., p. 61.

27. For the life of me, I cannot imagine anyone paying this much for proof copies
of a journal that survived only one issue.

28. Ibid, p. 58

29, Cited in ibid, p. 47,

 
