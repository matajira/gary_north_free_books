The Myth of Marx’s Poverty 255

than you can find in the “proletarian” workshops of Detroit or
Chicago, The well-fed bourgeois intellectuals have far more of
an affinity for the ideas of Marx and Engels than today’s indus-
trial proletariat does. Marx’s ideas were born in the university
and its intellectual underground, were nurtured during years of
yoluntary withdrawal from economic production, and flowered
in declining years of luxury, far removed from the environment
of the displaced proletariat.

The “tragedy” of Marx’s “poverty-stricken” life consisted
only in the fact that if he had lived in the mid-twentieth century,
he could have avoided those fifteen years of self-imposed trouble.
There are today plenty of tax-exempt foundations that make a
point of supporting such revolutionary conspirators in the high
style which he experienced throughout most of his life.

Karl Marx set the pattern, both intellectually and finan-
cially, for the present generation of well-fed, well-subsidized,
bourgeois intellectuals. An economist who could not economize,
a revolutionary organizer whose organizations invariably fell
apart, a secular prophet whose prophecies did not come true, a
self-proclaimed autonomous man who spent his life on Engels’s
dole and in hock to the pawnbrokers, the self-proclaimed spokes-
man of the working class who never did an hour’s manual labor
in his life, the inventor of a theory of inevitable industrial revolu-
tions that have in fact only occurred in backward rural societies,
the man who predicted the withering away of the state whose
ideas have revived the ancient quest for world empire, Karl
Marx’s life was a living testimony to the failure of bad ideas.
The only people who still take his ideas seriously are bourgeois
intellectuals, heretical middle-class pastors, and power-seekers
who want to become tyrants for life —the kind of people Marx
despised, that is, people very much like himself,

On the bourgeois dole for his entire life, he spent his days
criticizing the very economic structure which permitted him his
leisure time: capitalism. He attacked “Bourgeois Liberalism,”
yet it was that system of liberal attitudes and broadmindedness
which produced an atmosphere of intellectual freedom, without
which he would have been imprisoned and his books burned as
