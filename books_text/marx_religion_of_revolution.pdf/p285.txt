Soviet Economic Planning . 211

percent of the production of the U.S. Thus, the claims of Soviet
planners that the Soviet Union has increased production by 26
times since 1913 are absurd. A figure something under a six-fold
increase is closer.*!

We have seen, in short, that Soviet economic growth has
been paid for to a large extent by the enforced minimum con-
sumption levels of the Soviet population. Until recently, these
citizens have had very little to say concerning the allocation of
scarce resources in their country —the resources which they
have been responsible for producing. Certainly, gross output
figures can be greatly increased when, as Bergson says, “gross
investment absorbs nearly half the increase in output realized
under the first two five-year plans, and 60.7 per cent of that
achieved from 1940 to 1950.4? To a limited extent, the economic
tide has been turning in the Soviet Union, as the preferences of
the consumers are being taken into consideration to a larger
extent than before. Again, quoting Bergson: “The share of house-
hold consumption in the increase in output under the first two
five-year plans is only 9.1 per cent. For 1940-1950, the corre-
sponding figure is 29.4 per cent, but from 1950 to 1955 it is 53.2
percent.” Unfortunately for the official Soviet growth rate, this
shift has caused (along with several other factors) a slowing
down. Most non-Marxist Western observers agree that after
1958 the formerly high rates of growth began to taper off. The
old problem of economic life reasscrts itself today in the Soviet
Union: you cannot consume goods that are not produced.
Campbell’s evaluation is striking: “The Soviet Union is very,
very, very far behind the United States in terms of the amount

41. Ibid, p. 268. The usually accepted figure is that the Soviet output is at
one-third of the U.S. Jasny believes that Nutter’s figures do not reflect Russia’s
potential economically, but Nutter is not concerned with potential as much as
actual accomplishment. Cf. Jasny, Soviet Industriatization, p. 25. The six-fold increase
up to 1955 is the accepted figure: Gerschenkron, Economic Backwardness, p. 2675
Bergson, Reat National Income, p. 216.

42. Bergson, Economies of Soviet Planning, pp. 311-12.

43, Ibid, p. 312.

44, Nove, The Soviet Economy, p. 156.
