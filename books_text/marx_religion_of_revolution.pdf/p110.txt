36 Marx’s Religion of Revolution

need to maintain physical existence. Yet the productive life is the
life of the species. It is life-engendering life. The whole character
of a species. —its species character —is contained in. the char-
acter of its life activity; and free, conscious activity is man’s
species character. . , . The animal is immediately one with its
life activity. It does not distinguish itself from it. Tt is ds life
activity, Man makes his life activity itself the object of his: will
and of his consciousness, He has conscious life activity. . . .
Conscious life activity distinguishes man immediately from ani-
mal life activity.”

In this perspective, the whole of man’s existence is inter-
preted as a part of this single sphere, production: “Religion, family,
state, law, morality, science, art, etc., are only particular modes
of production, and fall under its general law.”*5 Meyer has
pinpointed the source of this element of Marx’s thought: “Marx
has appropriated for his system a Promethean image of man the
creator, man the provider, man the tamer of his environment.
He has identified himself with a glorification of material achieve-
ments which, before him, had been an essential part of revolu-
tionary liberalism, part of the ideology of the rising bourgeoi-
sie.”°7 Mars, in spite of his emotional attacks against bourgeois
ideals, could not escape the influence of the presuppositions of
the Enlightenment.*8

35. “Estranged Labor,” EPM, p. 113. [Collected Works, 3, p. 276.]

36. “Private Property and Communism,” EPM, p. 136, [Collected Works, 3, p-
297]

37. Alfred G. Meyer, Marxism: The Unity of Theory and Practice (Ann Arbor:
University of Michigan Press, 1954), p. 75. Cf. Veljko Koraé, “In Search of Human
Society,” in Erich Fromm (ed.), Socialist Humanism (Garden City, New York:
Doubleday Anchor, 1966), p. 3.

38. Donald Clark Hodges has attempted to sever Marx’s thought from the
Enilightenment, but his arguments are not very convincing. Sce his essay, “The
Unity of Marx’s Thought,” Science and Society, XXVULE (1964), pp. 316-23. For
Marx's attacks on the ideals and institutions of bourgeois life, see section 11 of the
Manifesto of the Communist Party (1848), in Selected Works, 1, pp. 121-25. [Collected
Works, 6, pp. 499-503.] Also, see his essay, “On the Jewish Question,” which is far
more hostile to bourgeois life as such (especially money) than it is critical of Jews
as such. It was written in 1843 and published in 1844; reprinted in T. B. Botto-
more’s edition of Marx, Karl Marx: Early Writings, [Collected Works, 3, pp. 147-74.]
