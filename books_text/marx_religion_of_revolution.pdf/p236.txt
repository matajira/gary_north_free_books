162 Marx’s Religion of Revolution

Nevertheless, the vision which Marx and his followers have
held cannot be refuted by a step-by-step dissection of his eco-
nomic system. Communists have never held to the system merely
because of its particular insights into the nature of capitalist
production and distribution. The system is held in faith because
it promises a better world for secular, apostate men. Marxism
fulfilled the needs of 19th-century industrial men who were ready
to destroy the system under which they lived. It provided an
aura of scientific infallibility in an age which worshiped science.
It simultaneously appealed to a side of man’s nature which is
never wholly absent: his desire for total destruction of the pre-
sent. Men want to escape from history, since they believe that it
is history which has limited them. Their world is filled with
uncertainty, scarcity, and death; that this has been the result of
man’s apostasy and disobedience to God is something which
they dare not admit. If they did, it would demand repentance.
Throughout history, the cosmology of chaos has appealed to
such men, for it offers the promise of total liberation from the
bondage of time. Liber, in fact, was a Roman god of chaos, and
it is from his name that we derive the world “liberty.” Thus, the
popularity of the hammer as a revolutionary symbol: it is the
means of shattering the present world order. Marxism, in com-
bining the two myths of scientific infallibility and revolutionary
action, offered hope to those who have sought to escape from
history. This is the essence of Marx’s religion of revolution; it is
the same appeal which has dominated all the chaos cults as far
back as recorded history extends.
