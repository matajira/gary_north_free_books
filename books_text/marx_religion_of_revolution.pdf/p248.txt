174 Marx’s Religion of Revolution

on: ‘Go ahead, give it up.’ We already hear voices in your
country and in the West— ‘Give up Korea and we will live
quietly. Give up Portugal, of course; give up Japan, give up
Israel, give up Taiwan, the Philippines, Malaysia, Thailand,
give up ten more African countries. Just let us live in peace and
quiet. Just let us drive our big cars on our splendid highways;
just let us play tennis and golf, in peace and quiet; just let us mix
our cocktails in peace and quiet as we are accustomed to doing;
just let us see the beautiful toothy smile with a glass in hand on
every advertisement page of our magazines.’”!®

Conclusion

The Bible teaches that God deals covenantally with nations,
even at the final judgment and beyond. Thus, nations are under
the terms of the covenant, either explicitly (ancient Israel) or
implicitly (all nations under God as Judge). The covenant proc-
ess of blessings and cursings is therefore called into operation in
the history of nations. National continuity and discontinuity
must be viewed as an outworking of this fourth point of the
Biblical covenant.

History has seen the rise of empires. They have all failed.
They are satanic imitations of the implicitly (though not histori-
cally) unified kingdom of Christ on earth. The tendency of
Christ’s kingdom is toward expansion. This leavening process is
also a feature of Satan’s imitation kingdom, But his kingdom is
on the defensive since Calvary. Whenever Christian nations
remain faithful to the terms of God’s covenant, they experience
blessings leading to victory over time. Whenever they have apos-
tatized, they have faced judgment and have had their inheritance
transferred to other nations, either through military defeat or
economic defeat.

The West now faces its greatest challenge since the fall of the
Roman Empire. The formerly Christian West has abandoned
the concept of the covenant, and with it, Christianity’s vision of
victory in history. The Marxists have stolen this Biblical view-

18, Solzhenitsyn: The Voice of Freedom (Washington, DG: AFL-CIO, 1975), p. 12,
