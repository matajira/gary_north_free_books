The Myth of Marx’s Poverty 249

me,” he once wrote, citing the Roman Republic’s playwright
Terence, thereby proclaiming his personal commitment to radi-
cal humanism. Nothing human was foreign to Marx, one is
tempted to add, except steady employment. In 1864, he had
squandered a fortune. The money had been advanced (given)
to him by Engels, plus what he inherited from his mother’s
estate, plus a huge inheritance from Wilhelm Wolff. In 1865,
broke again, he was offered the opportunity to write a column
each month on the movements of the money-market. He refused
to accept the job, never bothering to so much as offer an explana-
tion.4

Karl and Jenny Marx were simply not capable of handling
money with any degree of success. Three things served to allevi-
ate their economic hardship in this bleak period of their lives.
First there was Helene (Lenchen) Demuth, the Marx’s house-
keeper. She had grown up as a servant in the von Westphalen
home, and Jenny’s mother sent her to be with the Marxes in
1846, She remained with the family until the death of Karl Marx
in 1883, As Payne’s biography of Marx demonstrates, she was
the keeper of the family purse, and she kept it as solvent as
possible. She also bore Marx an illegitimate son in 1851 — a son
Marx was never willing to acknowledge for fear of embarrass-
ment in London’s revolutionary circles - another hitherto ig-
nored fact which Payne’s book brings to light,

The Inheritances

A second factor was the advance on his inheritance from his
mother (who had not yet died) which he received in early 1861.
Karl’s mother paid off his old debts, and through the executor
of her estate, her immensely successful industrialist brother-in-
jaw Lion Philips.“ Marx received £160, part of which he spent

43, Mehring, Kar! Mars, pp. 342-43. This took place in 1865, the year following
Marx’s massive inheritance,

46. Lion Philips, Marx’s uncle by marriage, became the founder of one of
Europe's most powerful companies, the Philips Electrical Company, of which the
North American Philips Company (Norelco) is a subsidiary. See Payne, Afarx, p.
330.
