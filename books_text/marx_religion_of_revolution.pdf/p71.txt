Preface lxxi

of religion, namely, the initial and fundamental alienation of the
human creature, root of all alienation, and that it can demon-
strate how this alienation came about. This particular truth was
arrived at gradually, in the course of long and bitter struggles.
Born of religion, philosophy grows up in ground [that] religion
has prepared and battles hard against it, not always victori-
ously.”"!6 Roche rejects this sociological linc of reasoning, based
as it is on the religious theme of human alienation. “Thus, recent
efforts to turn Marx into a sociological critic of alienation - an
exercise based on his Hegelian, Baudelaire phase in Paris — are
fundamentally nonsense. The mature Marx was not a social
worker: the man who could assert in Capital that ‘individuals are
dealt with only insofar as they are personifications of economic
categories, embodiments of particular class relations and class
interests’ was hardly a caring person. Marx viewed alienation,
that is, the resentment felt by an oppressed class, say, the prole-
tariat under capitalism, as a necessary concomitant of progress.
A happy proletarian was, for the post-Hegelian Marx, suffering
from an acute case of ‘false consciousness’; alienation became the
badge of true class consciousness and rising revolutionary fervor,
not a cause for tears and lamentations.”!"7

Nevertheless, there. remains a deeply religious impulse in
Marx’s post-Hegelian use of the theme of alienation. Remove the
religious impulse of Marxism, and you have removed its heart,
The beast would not survive that operation. Marx always wanted
to overcome human alienation. After 1845, he described human
alienation in terms of his theory of changes in the mode of
production, but from the beginning of his communist phase, he
had tied the origin of human alienation to the origin of private
property, and therefore he had tied the overcoming of alienation
to the abolition of private property. He wanted to live in a world
in which the post-revolutionary communist mode of production
had eradicated alienation. He never wavered in his faith that the
only means of overcoming alienation is the communist revolu-

116, Henri Lefebvre, The Sociology of Marx (New York: Pantheon, 1968), pp. 3-4.
117. Roche, History and Impact, p. 9,
