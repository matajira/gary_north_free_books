XXvi Marx’s Religion of Revolution

“The Player,” published in the Berlin literary magazine, Athe-
naeum. Payne reprints it.

Look now, my blood-dark sword shall stab
Unerringly within thy soul,

God neither knows nor honors art.

The hellish vapors rise and fill the brain.

Till I go mad and my heart is utterly
changed.

See this sword—the Prince of Darkness
sold it to me,

For he beats the time and gives the signs.

Ever more boldly I play the dance of death.

But what about Halle’s equating of Marx with St. Paul?
That was what bothered me most in Halle’s essay. That Karl
Marx offered mankind a religious drama is certain; that it had
anything in common with the experience or theology of St. Paul
is a misreading of Marx’s religion, for his was a modernized
version of ancient paganism’s religion of revolution,

Shortly before I read Halle’s essay, I had read R. J. Rush-
doony’s booklet, The Religion. of Revolution (1965), which intro-
duced. me to the ultimate goal of ancient pagan religion: to
regenerate the world through chaos. Rushdoony demonstrated

 

York: International Publishers, }975), alchough the essay on “Union of the Faithful
with Christ” is mysteriously placed in the Appendices. It is clear that in his late
teens, Marx was a liberal, pietistic Christian, Yet by the time he reached his early
twenties, he was a confirmed atheist.

5. Unknown Karl Marx, p. 59. It is this poem, Oulanem, and the seemingly
overnight loss of Marx’s faith, that led Pastor Richard Wurmbrand, a victim of
many years of torture in Communist prisons, to conclude that Marx made some
sort of pact with the devil: Marx and Satan (Westchester, [linois: Crossway, 1985),
ch. 2. Wurmbrand cites Albert Camus, who claimed in 1951 that the Soviet Union's
Marx-Engels Institute has suppressed the publication of 30 volumes of materials
by Marx: The Rebel: An Essay on Man in Revolt (New York: Vintage, [1951] 1956),
p- 188. Wurmbrand wrote to the Institute, and received a reply from M. Mtched-
‘ov, who insisted that Camus was lying, and then went on to explain that over 85
volumes are still unpublished, due to the effects of World War II. He wrote this in
1980, This was 35 years after the War ended. Marx and Satan, pp. 31-32.
