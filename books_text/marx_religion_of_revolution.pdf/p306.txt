Appendix C
THE MYTH OF MARX’S POVERTY*

If T had had the money during the last ten days, I would have
been able to make a good deal on the stock exchange. The time has
now come when with wit and very little money one can really make a
Killing in London.

Karl Marx (1864)!

One of the most widely believed ideas in the world is that
man is purely the product of his environment: social, economic,
physical, educational, genetic, or a combination of all of them.
This belief system is what social scientists call environmental deter-
minism. It is a very ancient heresy. In fact, it is the ancient heresy
regarding cause and effect in human action. It first appeared in
the garden of Eden. God asked Adam if he had eaten from the
forbidden tree. Adam’s answer was pure environmental deter-
minism: “The woman whom thou gavest to be with me, she gave
me of the tree, and I did eat” (Gen. 3:12). Then God asked the
woman what she had done. Her response was along the same
lines: “The serpent beguiled me, and I did eat” (Gen. 3:13).

Both answers were historically accurate but judicially irrele-
vant, Yes, the woman had sinned. Did this exempt Adam? Yes,
the serpent had sinned. Did this exempt Eve? Both Adam and

*The bulk of this appendix was published as “Poor Karl,” American Opinion
(April 1971).

1. Marx to Frederick Engels, M4 July 1864: cited in Robert Payne, Marx (New
York: Simon & Schuster, 1968), p. 354, [Collected Works, vol. 41, p. 546.]

232
