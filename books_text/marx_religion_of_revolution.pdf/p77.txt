Introduction 3

“Philosophies that are incompatible cannot debate one another.
There must be some common ground, some problem of life
which both accept as crucial and to which the philosophies offer
different answers. Otherwise instead of a debate there is simply
the revelation of different premises or different concepts of the
fanction of philosophy.” Between Christianity and Marxism
there can be no meaningful “dialogue.” Charles Hodge, the great
19th-century Calvinist theologian, put it this way: the last issue
of history will be the conflict between “Atheism and its countless
forms and Calvinism. The other systems will be crushed as the
half-rotten ice between two great bergs.”* Neither the consistent
Marxist nor the consistent Christian can hope for a reconcili-
ation between the two systems; it is a question of total intellec-
tual warfare. Members of both sides are convinced that their
ultimate triumph is inevitable. The issue is basically a conflict
in the realm of faith.

It is interesting to note that in recent years, certain human-
ists within the churches and outside of them have attempted to
reconstruct Marx in their own image. This has been done in
order to make Marx appear more palatable to the modern world.
For the Christian, however, these efforts have accomplished
precisely the reverse; they have exposed the demonic features of
a humanism which can embrace something as grotesque as the
Marxian system. The orthodox Christian is not convinced by
Leopold Senghor, the President of the Republic of Senegal, when
he asserts that “Churchmen themselves cannot deny Marx’s
contributions and they accept his positive values.”® Nor is there
much. to be thankful for when we read a statement such as the
one made by Santiago Alvarez, a Spanish Marxist: “Thus, logic
tells us that the way to test the two positions ~ the Marxist and

3, Sidney Finkelstein, “Marxism and Existentialism,” Science and Society, XXXI
(1967), pp. 59-69.

4, Charles Hodge, Princeton Sermons (London: Banner of Truth Trust, 1958),
p.xv,

5. Leopold Senghor, “Socialism Is a Humanism,” in Erich Fromm (ed.),
Socialist Humanism: An International Symposium (Garden City, New York: Doubleday
Anchor, 1966}, p. 54. For a disturbing confirmation of Senghor's suggestion, see the
