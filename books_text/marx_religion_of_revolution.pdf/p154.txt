80 Marx’s Religion of Revolution

the revolutionary overthrow of the political and social conditions
that had engendered the Second Empire [of Napolcon III], and,
under its fostering care, matured into utter rottenness.”!4* Marx
did not really understand the Commune, but the use he made
of it demonstrates that he saw it in terms of political and social
regeneration — collective salvation.!5

Revolutionary practice gives the workers a sense of self
realization, since through revolutionary action alone does class
consciousness develop. Marx, in 1850, characterized this mes-
sage: “You have got to go through fifteen, twenty, fifty years of
civil wars and national wars not merely in order to change your
conditions but in order to change yourselves and become quali-
fied for political power. . . .”46 Again, a semi-religious func-
tion is accomplished; Caillois’s thesis is that only in war and
bloodshed can modern man approximate the destructive psy-
chology of the chaos festivals. The communal feeling of collective
devotion to a higher cause in modern civilization can be experi-
enced only in warfare.'4? Marx's belief that proletarian con-
sciousness can be achieved only in struggle is very similar to
Caillois’s position; in this sense, the proletarian revolution has
the same religious and psychological function as the festival had
in the ancient world,

Engels, in his forthright manner, wrote to Marx in 1857, just
as the depression of 1857 was beginning. He had been recu-
perating from an illness in the United States. His language is
clearly religious: “The bourgeois filth of the last seven years had
stuck to me to a certain extent after all, ifit is washed away now

144, Marx, The Civil War in France (1871) in Selected Works, 2,-p, 212, On the
history: of the Commune, see the Marxist version by Lissagaray, History of the
Commune of 1871 (London: Reeves and Turner, 1886), translated by Marx’s daugh-
ter, Eleanor Marx Aveling. For more accurate accounts, see Frank Jellenek, The
Paris Commune of 1871 (London: Victor Gollancz, 1937); Alistair Horne, The Fall of
Paris (New York: St. Martins, 1965).

14, Similar statements can be found in the essay: Selécted Works, 2,-pp. 200-1,
222,

146. Minutes of London Central Committee of the Communist League, 15 Sept.
1850: Correspondence, p. 92. [Collected Works, 10, p. 626.}

147, Caillois, Man and the Sacred, ch. 4.
