92 Marx’s Religion of Revolution

tate to use it: this was his message to the working class. We have.
already seen how he admitted that in England and America
there might be some small chance to avoid the Revolution in the
transition to socialism, He even praised the Ten Hours Act
which shortened the working day in England. Of course, in this
later period he was trying to gain the support of the English trade
unions, and the ballot-box appealed to them.!° As Marx himself
realized, he was not “wholly forthright in his famous Address to
the Working Men’s Association: “It was very difficult to frame
the thing so that our view should appear in a form acceptable
from the present standpoint of the workers’ movement. In a few
weeks the same people will be holding meetings for the franchise
with Bright and Cobden. It will take time before the reawakened
movement allows the old boldness of speech. It will be necessary
to be fortiter in re, suaviter in modo [bold in matter, mild in man-
ner],."174

Marx’s appreciation for the cooperative movement has led
Martin Buber to. place Marx in the camp of the European
utopian thinkers. In his Paths in Utopia, Buber argues that Marx
wanted to create a sense of communal membership in his post-
revolutionary society, and for this reason Marx saw in the co-
operatives the sign of a coming transformation of society.!"5
Marx, in this sense, was a Utopian Socialist, although he had
criticized his utopian predecessors for the lack of insight into the

173, Marx, The Inaugural Address of the Working Men'’s International Association
(1864), in Selected Works, 2, pp. 15-16. {Collected Works, 20, p. 10.] Cf. A. Lozovsky
{pseudonym of Solomon A. Dridzo), Marx and the Trade Unions (New York: Interna-
onal Publishers, 1938), esp. pp. 23-25, 48, 167-70. Lichtheim’s interpretation of
the document is straightforward: “The Inaugural Address is in a sense the Charter of
Social Democracy.” Marxism, p. 113. Perhaps so, but this does not mean that Marx.
intended it to be such, As a corrective to Lichtheim’s view, see Cole, The Meaning
of Marxism, ch. 7, esp. pp. 181-90.

174, Marx to Engels, 4 Nov. 1864: Correspondence, p. 163.

175, Martin Buber, Paths in Utopia (London: Routledge and Kegan Paul, 1949),
ch. 8. For Marx’s comments on the co-operative movement, see the Inaugural
Address, in Selected Works, 2, pp. 16-17. [Collected Works, 20, p. 11.} Capital, 3, p.
527. [Capital, 3, p. 445.] It is interesting that a co-op publishing firm was the first
company to publish Marx's complete economic writings in this country. This, of
course, was Charles H. Kerr & Co.
