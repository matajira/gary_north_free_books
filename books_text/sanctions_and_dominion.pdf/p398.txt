360 SANCTIONS AND DOMINION

through adoption. Their sons and grandsons did not quite
replace the adult males of Moses’ generation and Joshua’s
combined.

So, most of the 600,000 males were probably members of
Joshua’s generation. If so, then each family bore fewer than two
children who reached maturity, for the 600,000 men a genera-
tion later included grandsons of Joshua’s generation. My con-
clusion is that their birth rates were low or else the mortality
rate for children was high. The first possibility seems more like-
ly. God brought them under a curse in the wilderness: very low
birth rates. But He did not kill off large numbers of the fourth
generation.

The 22,273 non-Levite, biological (matrix-issued), firstborn
sons of Israel, from one month old to age 19, constituted four
percent of the fighting-age male population of 603,550. To be
in replacement-rate mode, there had to be approximately
22,000 fathers. Some fathers might have been childless; others
might have two sons; but nationally, the 22,273 firstborn testi-
fied to an upper limit on the number of men in Joshua’s gener-
ation. Again, to maintain the replacement rate, there could
have been no more than about the same number of men in
Moses’ generation. If there was growth, however, then the
number of Moses’ generation was less: fathers producing more
than one son. If the growth rate was doubling, as Aaron’s four
children testified to, there were 22,000 fathers and 11,000
grandfathers. This indicates how thoroughly gentile, genetically
speaking, Israel’s army was at the time of the first mustering,
and how important covenantal adoption was in Israel’s found-
ing as a nation. Most of the 603,550 were ex-gentiles. This is
why they were numbered separately from the biological first-
born. This was serious covenantal evangelism.

There is a weak link in this scenario. |. regard it as the major
weak link. If the maximum number of men in the third genera-
tion was in the range of 22,000 ~ no higher than 30,000 - then
there probably were not this many men at the beginning of the
