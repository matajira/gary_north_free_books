The Lure of Magic 187

The boundary between supernatural power and natural causa-
tion is controlled by the priest because he is the master of ritu-
al. Even his spoken words can take on the character of ritual.
They become formulas of power rather than prayers of suppli-
cation.

In tapping the rock, Moses acted as a pagan priest. When he
relied on the rod as the means of bringing water from the rock,
he adopted the mentality of the magician. This rod had served
him as an implement of sanctions, both negative (the Nile’s
cursed water) and positive (Horeb’s blessed water).° Moses had
become psychologically dependent on this rod. In his mind, it
became a tool of supernatural power rather than a symbol of
supernaturally delegated judicial authority. He did not regard
God’s word as authoritative; rather, it was God’s word plus: the
rod. As a prophet, Moses was to speak events into existence by
repeating God’s word. It was Gad’s word that was the basis of
Moses’ authority, not the visible implement of authority. But
because Moses had repeatedly used this visible indicator of his
prophetic authority as a means of invoking the predictable
historical sanctions associated with the Old Covenant’s prophet-
ic office, he adopted the mentality of a pagan priest. He moved
from biblical covenantalism to pagan realism. He moved from
judicial invocation to magical manipulation.

This move was not absolute. Moses still brought a judicial
invocation against Israel: “Hear now, ye rebels; must we fetch
you water out of this rock?” (v. 10b). He identified them as
rebels who relied on public displays of God’s power rather than
His promises. But in saying this, he condemned himself, for he,
too, relied on public displays of God’s power rather than His
promises. That was why he tapped the rock. God had promised
Moses to bring water in response to Moses’ public verbal invo-

9. “And the Lorp said unto Moses, Go on before the people, and take with thee
of the elders of Israel; and thy rod, wherewith thou smotest the river, take in thine
hand, and go” (Ex. 17:5).
