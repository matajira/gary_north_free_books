CONCLUSION

Harden not your heart, as in the provocation, and as in the day of
temptation in the wilderness: When your fathers lempled me, proved me,
and saw my work. Forty years long was I grieved with this generation,
and said, It is @ people that do err in their heart, and they have not
known my ways: Unto whom I sware in my wrath that they should not
enter into my rest (Ps, 95:8-L1),

The Book of Numbers is the Pentateuch’s book of sanctions:
the fourth book in the Pentateuch. Oath/sanctions is point four
of the five-point biblical covenant model.’ The Book of Num-
bers is an integral part of the five books of Moses. Its theme —
sanctions — is integral to the five-point biblical covenant model.

The book begins with the mustering of the holy army of
God. This was the second mustering. The first had taken place
about seven months earlier (Ex. 38:26). The third and final
mustering took place just before the conquest of Canaan (Num.
26). A numbering required the payment of atonement money
for the blood to be shed in the subsequent battles of the army
(Ex. 30:12, 15).

1. Ray R. Sutton, That You May Prosper: Dominion By Covenant (2nd ed.; Tyler,
Texas: Institute for Christian Economics, 1992), ch. 4.

2. Gary North, Tools of Dominion: The Case Laws of Exodus (Tyler, Texas: Institute
for Christian Economies, 1990), ch. 32.
