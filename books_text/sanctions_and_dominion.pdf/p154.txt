116 SANCTIONS AND DOMINION

Numbers 7 backtracks 30 days, for the events of Numbers 1
took place on the first day of the second month (Num. 1:18).

The princes or chieftains of Israel delivered the offering to
Moses. God instructed Moses to distribute the carts and oxen to
the families of Gershon and Merari, but not to Kohath, which
was Moses’ family. Kohath was closest to the Ark of the Cove-
nant. This created a special holiness burden. “But unto the sons
of Kohath he gave none: because the service of the sanctuary
belonging unto them was that they should bear upon their
shoulders” (Num. 7:9). The closer to the inner circle, the great-
er the ritual responsibility, the greater the danger of profanity,
and the greater the holiness of those serving. In contrast, the
closer a Levite was to the non-Levitical tribes, the larger the
required physical burden of sacrifice on behalf of these tribes.

Levite families that were closer to the outer rings of holiness
bore the brunt of the physical burdens: transporting the imple-
ments of the tabernacle and defending the Ark from the first
wave of any attack on the holy of holies. The two families in the
outer rings of holiness were given the primary burden of trans-
porting the implements of sacrifice, for they lawfully bore the
implement of defense: the sword. They were the sanctions-
bringers against invaders: Merari first and then Gershon. Twice
as many wagons filled with offerings went to Merari as to Ger-
shon (Num. 7:7-8) because Merari had to transport twice as
much, Merari served in the outer ring of the three concentric
circles of authority.” The last line of defense was Kohath. More
of the Kohathites would survive an unsuccessful attack than the
Gershonites; more of Gershon would survive than Merari.
Conversely, God would kill more of the Kohathites than the
Gershonites for profane acts, while Gershon was more at risk
than Merari.

by the priests for subsequent sacrifice,
2. See Chapter 3, above: section on “Hierarchy and Inner Circles,” subsection on
cles of Authority,” pp. 60-61,

  
