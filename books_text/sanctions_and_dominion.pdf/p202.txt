164 SANCTIONS AND DOMINION

among them: I am thy part and thine inheritance among the
children of Israel. And, behold, I have given the children of
Levi all the tenth in Israel for an inheritance, for their service
which they serve, even the service of the tabernacle of the con-
gregation” (Num. 18:20-21). This law had important implica-
tions for both the social order and the political order of Israel.

Social Implications

There is always the problem of envy and jealousy in a soci-
ety. The jealous person thinks: “You have something I want. I
will take it from you.” This was the sin of Korah and Dathan.
The envious person thinks: “You have something I want. I
cannot get it from you. I will destroy it so that neither of us can
enjoy it.” This had been the sin of the Philistines in regard to
Abraham: “For all the wells which his father’s servants had
digged in the days of Abraham his father, the Philistines had
stopped them, and filled them with earth. And Abimelech said
unto Isaac, Go from us; for thou art much mightier than we.
And Isaac departed thence, and pitched his tent in the valley of
Gerar, and dwelt there. And Isaac digged again the wells of
water, which they had digged in the days of Abraham his fa-
ther; for the Philistines had stopped them after the death of
Abraham: and he called their names after the names by which
his father had called them” (Gen. 26:15-18). The Philistines
were envious: better to fill up these wells with dirt rather than
retrieve water from them today but risk having Abraham or his
heirs claim them later and benefit from them.

The system of Levitical inheritance kept jealousy and envy at
a minimum. The Israelites were required to pay tithes to the
Levites, but the Levites could not inherit rural land. Only later,
if Israel became a predominantly urban society, would this
restriction on rural land ownership fade as a major restraint on
the wealth of Levi. The Levites had to be supported by the
nation, but only in proportion to the prosperity of the nation.
