Land to Match Men’s Skills 289

(Judg. 5:16-17, 23). She used the negative sanction of retroac-
tive ridicule because she had no civil negative sanction at her
disposal. Moses also refused to establish an extra-biblical legal
basis of exclusion from the covenant of Israel.

if other tribes demanded some sort of additional reward for
their participation in the conquest, they might defect if Moses
refused to grant it. The army of Israel might be depleted. So,
Moses took a risk. Announcing a negative sanction against the
other tribes ~ sharing their inheritance in Canaan with these
non-combatants if they refused to accept the two tribes’ offer —
might backfire on him. He had two counter positions: one
stated, the other implied. First, he offered a positive sanction:
a 26 percent increase in post-war land allocation for the other
nine and a half tribes. They would receive a larger percentage
of land that was more suitable for farming than Gilead, which
was presumably what the other tribes preferred. They were not
cattlemen. Second, he implied a negative sanction. He would
call off the invasion. He did not say this, but if there were mass
defections — too many free riders sitting on the sidelines ~ this
was the implication. This would have meant dividing up Gilead
among all twelve tribes: a major reduction in tribal spoils.

The representatives of the other tribes assessed the offer and.
decided to accept it. The rewards outweighed the costs. By
consenting to the offer, they would gain the military coopera-
tion of the two and a half tribes. They would also gain a signifi-
cant increase in the post-war land distribution inside Canaan by
forfeiting land outside Canaan that was more beneficial to
cattleémen than to other agricultural producers. This land would
probably be even less valuable after the conquest; for it would
be more distant from whatever city God would choose inside
Canaan as the city of the tabernacle. This would mean longer
and more expensive journeys to attend the three annual feasts.
It is not surprising that they agreed to the offer.

It is also not surprising that the Israelites had trouble dis-
placing all of the Canaanites. The inability of Joshua to transfer
