How Large Was Israel's Population? 337

malé Levites were there? Four hundred, perhaps? How, and
how fast, did this Levite population grow from 400 to so many
that 22,000 seemed reasonable to the forger (sorry: “redactor”)?
Was the redactor so confused that he inserted numbers for the
tribe of Levi that totaled four times larger than the number of
all the other 12 tribes combined (using Petrie’s estimate of
5,600)? Translating ‘eleph in Numbers 1 as “family” rather than
“thousand” leads to a dead end. It was an obvious dead end on
the day it was proposed in 1906.

Counting the firstborn was required because there had to be
a substitute for them: the Levites. The Levites as a tribe would
substitute for the firstborn on a one-to-one basis. If there were
more firstborn sons than Levite males, someone would have to
pay the Levites five shekels per extra firstborn. The Bible does
not. say who would have to pay. The allocational question was
this: Which families had born the “excess” 273 children? If all
of the families were counted, and the comparison was made, on
what basis would a particular tribe or family be assessed the five
shekels? Would it be those families whose firstborn were born
later than the others, i.e., families of those firstborn who consti-
tuted the excess? This would seem to be fair, but we are not
told.

The ratio of firstborn sons to adult males constitutes a long-
recognized problem. There were 603,550 adult males (Num.
1:46). There were 22,273 firstborn (Num. 3:43). Wenham
writes: “This means that out of 27 men in Israel only 1 was the
first-born son in his family. In other words, an average family
consisted of 27 sons, and presumably an equal number of
daughters.”"” Milgrom also cites this ratio."* The firstborn
were not adults — age one month and older. The ratio is clearly
impossible demographically. Because this dilemma is based on
biblical texts, it requires a solution consistent with the texts.

17. Wenham, Numbers, p. 61.
18. Milgrom, Numbers, p. 339.
