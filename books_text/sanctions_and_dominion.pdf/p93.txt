THE HIERARCHY OF
SERVICE/SANCTIONS

And the Lorn spake unto Moses, saying, Bring the tribe of Levi
near, and present them before Aaron the priest, that they may minister
unto him. And they shall keep his charge, and the charge of the whole
congregation before the tabernacle of the congregation, to do the service
of the tabernacle, And they shall keep all the instruments of the taberna-
cle of the congregation, and the charge of the children of Israel, to do the
service of the tabernacle. And thou shalt give the Levites unto Aaron and
to his sons: they are wholly given unto him out of the children of Israel.
And thou shalt appoint Aaron and his sons, and they shall wait on their
priest’s office: and the stranger that cometh nigh shall be put to death
(Nun, 35-10).

The English word “hierarchy” comes from the Greek word
for priest (hierus), We think of a hierarchy of command in terms
of an image: a vertical chain. This hierarchy may be judicial; it
may be merely functional. [t is associated with point two of the
biblical covenant model: hierarchy or representation."

This law, as with all the other Mosaic laws, was theocentric.
In this case, however, the theocentric character of the law was

1. Ray R. Sutton, Phat You May Prosper: Dominion By Covenant (2nd ed.; ‘Tyler,
‘Texas: Institute for Christian Economics, 1992), ch. 2.
