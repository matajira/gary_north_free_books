96 SANCTIONS AND DOMINION

The Firstborn Sons of Levi

The firstborn son received a special inheritance (Deut. 21:
17). The presence of such an inheritance is what identified the
recipient as a firstborn son. Because of their firstborn legal
status, the Levites were entitled to payments from the other
tribes for every firstborn son (Num. 3:47). Because of their
firstborn legal status as Ged’s firstborn (Ex. 4:22), the Israelites
were entitled to restitution payments for their forced servitude
in Egypt, which they collected from inheritances that would
otherwise have gone to the dead firstborn sons of Egypt (Ex.
12:33-36). It is in this context that we should interpret Num-
bers 5:5-8:

And the Lorn spake unto Moses, saying, Speak unto the children of
Israel, When a man or woman shall commit any sin that men commit,
to do a trespass against the LORD, and that person be guilty; Then they
shall confess their sin which they have done: and he shall recompense
his trespass with the principal thereof, and add unto it the fifth part
thereof, and give it unto him against whom he hath trespassed. But if
the man have no kinsman to recompense the trespass unto, let the
trespass be recompensed unto the Lor», even to the priest; beside the
ram of the atonement, whereby an atonement shall be made for him.

This law was an extension of the law of restitution found in
Leviticus 6:5. It was a law that penalized sin by requiring a
restitution payment of 20 percent. But it also rewarded volun-
tary confession, since the penalty for theft was normally double
restitution (Ex. 22:4), and could be four-fold (dead or sold
sheep) or five-fold (dead or sold ox) (Ex. 22:1).°

This extension of the law specified the priest as the final
claimant to both the replacement and restitution payments. If
the victim could not be located or was dead, then his relative
would receive the payment. If the relative could not be located,

9. Gary North, Leviticus: An Economic Commentary (Tyler, Texas: Institute for
Christian Economics, 1994), ch. 6.
