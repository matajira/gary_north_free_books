78 SANCTIONS AND DOMINION

hierarchy was annulled with it. The torn veil of the temple
pointed to the torn condition of tribal boundaries. The Mosaic
priesthood ended, and with it, the tribal separations.

What the Bible denies is the legitimacy of judicially fixed
hierarchies in non-covenantal institutions. The Bible does not
promote equality. There is no equality in heaven (I Cor. 3:14),
nor is there equality in hell (Luke 12:47-48). There is a bed-
rock individualism in biblical sociology because there is an
inescapable individualism in final judgment. No person can
transfer responsibility to another person and thereby escape the
consequences of his actions (Gen. 3:12-13). Yet there is also a
bedrock corporate element in biblical sociology: final judgment
is announced to two great collectives: sheep and goats, saved
and lost (Matt. 25).

I mention this because, as a late twentieth-century social
theorist, | am well aware of the conflict between liberalism and
conservatism, a conflict that cannot be mediated by radicalism.
Conservative sociologist Robert Nisbet has described it well: “If
the central ethos of liberalism is individual emancipation, and
that of radicalism the expansion of political power in the service
of social and moral zeal, the ethos of conservatism is tradition,
essentially medieval tradition. From conservatism’s defense of
social tradition sprang its emphasis on the values of community,
kinship, hierarchy, authority, and religion, and also its premo-
nitions of social chaos surmounted by absolute power once
individuals had become wrenched from the contexts of these
values by the forces of liberalism and radicalism.””’ The twen-
tieth century has seen the fruition of conservatism’s fears: two
world wars, Communism, Nazism, and the alienation and des-
pair produced by individual moral debauchery. Yet we should
not ignore an insight of the mischievous libertarian humorist, P.

26, Robert A. Nisbet, The Sociological Fradition (New York: Basic Books, 1966), p.
11,
