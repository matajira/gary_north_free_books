156 SANCTIONS AND DOMINION

Reality imposes sanctions. Evaluation must match reality in
order to be successful. Those who evaluate reality accurately
reap rewards. Those whose evaluations fail to match reality
suffer losses. This is the great law of entrepreneurship: those
who forecast the future accurately and act accordingly prosper;
those who do not forecast accurately and act accordingly lose.
Assets move from those who evaluate properly to those who
evaluate improperly.
