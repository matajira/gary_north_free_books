294 SANCTIONS AND DOMINION

ing large families (Gen. 1:28) would not be initially penalized by
less land per family member.”

God also cautioned them regarding negative sanctions if they
refused to impose total annihilation against the Canaanites.
“But if ye will not drive out the inhabitants of the land from
before you; then it shall come to pass, that those which ye let
remain of them shall be pricks in your eyes, and thorns in your
sides, and shall vex you in the land wherein ye dwell. Moreover
it shall come to pass, that I shall do unto you, as I thought to
do unto them” (Num. 33:55-56). The Israelites were not im-
mune from God’s negative sanctions in history. To the degree
that they adopted the religion of Canaan, they would be treated
analogously. The judicial issue was therefore not bloodline but
confession of faith and obedience to the terms of the covenant.
The sanctions were covenantal.

First War, Then Peace

The pattern of victory over Canaan was Old Covenant sab-
batical: first work, then rest. The sabbath commandment is the
fourth commandment.’ It has to do with the negative sanction
of work and the positive sanction of rest. Israel had to wage
war for six years before gaining its rest.*

Israel was not told in advance that the removal of the Ca-
naanites would take six years. God did tell them that it would
not be an overnight process, since the animals of Canaan were
not to be allowed to escape from the domination of mankind. “I
will not drive them out from before thee in one year; lest the
land become desolate, and the beast of the field multiply

2. Over time, large families would mean smaller plots of land. This is why the
jubilee land law tended toward urbanization, where the jubilee inheritance law did
not apply. See Gary North, Leviticus: An Economic Commentary (Tyler, Texas: Institute
for Christian Economics, 1994), pp. 416-22.

3. Gary North, The Sinai Strategy: Economics and the Ten Commandments (Tyler,
Texas: Institute for Christian Economics, 1986), ch. 4.

4, See Chapter 19, footnote 8.
