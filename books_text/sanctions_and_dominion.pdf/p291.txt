Oath and Sanctians 253

establish a new family through marriage, or receive citizenship
from a civil government are invalid. On the contrary, there can
be no binding covenantal membership apart from such a oath.
itis the oath that seals the covenant judicially. For example, the
difference between fornication and marital sexual union is not
physical; it is judicial. The presence of a judicially binding
public oath differentiates the latter from the former.

The New Testament sometimes seems to be hostile to oaths,
but the context of these passages indicates that public oaths of
covenant ratification and renewal were not an issue. Oaths
invoked in non-covenantal relationships were the issue. Jesus
said:

Again, ye have heard that it hath been said by them of old time,
Thou shalt not forswear thyself, but shalt perform unto the Lord thine
oaths: But I say unto you, Swear not at all; neither by heaven; for it is
God’s throne: Nor by the earth; for it is his footstool: neither by Jerusa-
Jem; for it is the city of the great King. Neither shalt thou swear by thy
head, because thou canst not make one hair white or black. But let
your communication be, Yea, yea; Nay, nay: for whatsoever is more
than these cometh of evil (Matt. 5:33-37).

Jesus referred here to a traditional practice of publicly prom-
ising to perform some act and covering his promise with the
veneer of additional authority invoking supernatural authority.
This practice was never valid. There are lawful public oaths
taken to covenantal institutions; there are lawful private oaths
to God, but there are never private oaths to other men that are
lawfully invoked by an appeal to God or the supernatural. A
person who seeks to establish a unique degree of authority for
his promise to another person by invoking supernatural hierar-
chy and supernatural negative sanctions is misusing the oath.
He is covering his personal testimony with the aura of covenan-
tal authority. This is a violation of the commandment not to
take God’s name in vain. It is a boundary violation: profana-
