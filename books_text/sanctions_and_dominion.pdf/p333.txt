Sanctions and Inheritance 295

against thee. By little and little I will drive them out from be-
fore thee, until thou be increased, and inherit the land” (Ex.
23:29-30).

Moses began his public presentation to the conquest genera-
tion by listing the places where Israel had rested along the way.
They had wandered, rested, and wandered again. They had
found no permanent rest. They had moved 33 times from the
time of the exodus to the death of Aaron on Mt. Hor (Num.
33:5-38). Aaron’s death occurred in the fortieth year after the
exodus (v. 38). Then they began a final series of wanderings:
eight resting places in less than one year (vv. 41-49).

These final wanderings were different: Israel defeated major
enemies. This time, they were not driven out; rather, they
drove out others. They were able to build up both their confi-
dence and their land holdings through force of arms. They had
been attacked repeatedly; they had won repeatedly. This began
a psychological transformation of the nation: from a defensive
to an offensive mentality. They had not initiated these wars, but
they had won them. This was in preparation for their crossing
of the Jordan River: the move to total offense.

The exodus generation had maintained the peace by fleeing
whenever challenged, Their solution to an external challenge
was a retreat. They had been told by Moses that God was not
going to give them a definitive victory in their lifetimes, This
made them defensive. They did not want trouble. Their inheri-
tance was cut off. They had no intention of placing their lives at
risk for the sake of the inheritance of their children. They
demonstrated this one-generation time perspective by refusing
to circumcise their children (Josh. 5:7). The mark of a legal
claim on the inheritance that had been promised to Abraham
was circumcision, The exodus generation refused to impose the
mark of the covenant on their heirs. They were saying, in
effect, “If we cannot inherit, then why should our sons inherit?”

The generation of the conquest had lived as wanderers
because of their parents’ rebellion and cowardice. They had
