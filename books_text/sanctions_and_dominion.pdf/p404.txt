366 SANCTIONS AND DOMINION

cally redefining firstborn so as to eliminate the Passover’s sons
from the numbering.

For the person who resists a major redefinition, mine is
technically possible though speculative: mass adoption. The
enormous number of adult male Israelites at the time of the
first numbering, if compared to the small number of firstborn
sons, indicates that the Israelites had adopted huge numbers of
fleeing gentiles into the nation sometime during the year fol-
lowing the exodus.

This places adoption at the very center of Israel’s history as
a nation. There must have been prior adoptions: surely of the
household servants who came into Israel; probably of residents
of Egypt in the years prior to the oppression. But the ratio of
adult males to firstborn sons ~ 27 to one — can be explained in
terms of a mass adoption out of the mixed multitude, either at
the time of the exodus or in the months that followed, but
before the Exodus numbering.

One thing is certain: Israel was a nation of recruits. From God’s
recruiting of Noah, then Abram, then Jacob’s servants, and
perhaps at the exodus, Israel had been a nation of adopted
recruits. This was Ezekiel’s clear testimony to the nation: “And
say, Thus saith the Lord Gon unto Jerusalem; Thy birth and
thy nativity is of the land of Canaan; thy father was an Amorite,
and thy mother an Hittite. And as for thy nativity, in the day
thou wast born thy navel was not cut, neither wast thou washed.
in water to supple thee; thou wast not salted at all, nor swad-
dled at all. None eye pitied thee, to do any of these unto thee,
to have compassion upon thee; but thou wast cast out in the
open field, to the lothing of thy person, in the day that thou
wast born. And when I passed by thee, and saw thee polluted
in thine own blood, I said unto thee when thou wast in thy
blood, Live; yea, I said unto thee when thou wast in thy blood,
Live. I have caused thee to multiply as the bud of the field, and
thou hast increased and waxen great” (Ezek. 16:3-7a).
