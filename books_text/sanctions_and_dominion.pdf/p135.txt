The Firsthorn and Negative Economic Sanctions 97

then the priest. received it. There was no escape from the tres-
passer’s liability. By identifying the priest as a person with a
final claim on the property, the Mosaic law made clear the legal
status of the priests: God’s firstborn sons among the Levites.
Their responsibility before God was greater than that of any other jud-
icially representative group in Israel.

There is no New Covenant principle that would remove this
firstborn legal status of the institutional church. As the guardian
of the civil oath, the institutional church still performs a judicial
function of the Mosaic priesthood. It is this function that enti-
tles the church to payments from convicted criminals.”

Monetary Policy

In Numbers, God specified the firstborn son’s redemption
price: five shekels of the sanctuary (Num. 3:47). Five shekels of
silver was also the entry price for a male child adopted into the
tribe of Levi (Lev. 27:6).? Because the judicial intent in both
cases was related to Levitical inheritance, the shekels must have
been of the same value.

A currency unit could be called a shekel, but the priestly
shekel was mandatory for making payments to God’s ecclesiasti-
cal agents. In times of widespread monetary debasement (Isa.
1:22), God could not be lawfully cheated by those who would
have offered a shekel of lower value, even if both currency
units were called “shekel.” It would have been a profane act to
offer such a debased payment to the Levites.* Every time the
shekel of the sanctuary is mentioned, the text says that it weighs
20 gerahs."! This informed the nation what the sanctuary’s
shekel weighed. People could then compare the market's shekel

10. Ibid., ch. 4, section on “The Priestly Office.”

AL. Tbid., ch. 6.

12. On the entry price system, see ibid, ch. 36.

13. This would explain the presence of money-changersin the temple area (John
2314-15),

14, Exodus 30:13; Levicicus 27:25; Numbers 3:47; 18:16; Ezekiel 45:12.
