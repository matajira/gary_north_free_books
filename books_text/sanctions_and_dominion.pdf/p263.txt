Dividing the Inheritance 225

tance by finishing what neither your tribe nor the other tribe
has been able to complete: the conquest.” In other words, he
said: “All right, you self-proclaimed mighty men of war, you
tough guys: go out and exterminate some really tough guys
who are armed with iron chariots. Mighty is as mighty does.
Put your muscle where your mouths are.” Did they have a
legitimate legal claim? Let them prove their claim on the bat-
defield.

The heirs of Joseph had a plausible legal case: they were in
fact two families. They had been counted as two families in the
musterings. Joshua took this into consideration. But he did not
grant them their request irrespective of what they would do to
enforce their claim. Combined, they were larger than other
tribes. But numbers are as numbers do. They had to prove
their case by evicting Canaanites. That is, they had to do some-
thing extra in order to validate their claim. They could keep
any extra ground they conquered. This answer was Joshua’s
way to head off criticism from the other tribes. The other tribes
might come back and complain: “Joseph was entitled to one
share, just like the rest of us. He could not lawfully bequest
what was not his to give. Let the heirs of his two sons accept
this without trying to get their hands on our land.” Joshua
would have an answer: “They did not take away your land;
they took away Canaanites’ land. They earned their extra por-
tion on the battlefield. It is only fair that they should share in
a larger inheritance.”

This was a special case: legal rather than demographic. The
next seven casés were not special.

And there remained among the children of Israel seven tribes,
which had not yet received their inheritance. And Joshua said unto the
children of Israel, How long are ye slack to go to possess the land,
which the Lorp God of your fathers hath given you? Give out from
among you three men for each tribe: and I will send them, and they
shall rise, and go through the land, and describe it according to the
