56 SANCTIONS AND DOMINION

manifested geographically. This law governed the Aaronic
priesthood. The central - literal and figurative — service of the
Aaronic priesthood was associated with the holy of holies. The
holy of holies marked the central focus of Israel: the earthly
dwelling place of God, the place of His name. The holy of
holies was the geographical link between heaven and earth. In
it rested the Ark of the Covenant.

Guardians

The Mosaic priesthood guarded the boundaries associated
with the holy of holies. The priests in the narrow sense were
these who officiated in the sacrifices: the sons of Aaron. In a
broader sense, the priesthood was the tribe of Levi. In the
broadest sense, Israel was a nation of priests (Ex. 19:6). They all
were to guard the tabernacle by imposing physical sanctions on
those who violated a series of concentric boundary markers:
from the holy of holies to the nation’s boundaries.

Priests were the assigned agents of bloodshed inside the
sacrosanct boundaries associated with the holy of holies. The
narrowly defined priests shed the blood of animals to placate
God. The more broadly defined priesthood defended the taber-
nacle from profane invaders who had no lawful access. The
most broadly defined priesthood was the army of the Lord, a
holy army, which defended the nation because the nation was
God’s sanctified dwelling place.

The legal basis of the Levitical priesthood of pre-exilic Mosa-
ic Israel had begun in the wilderness with the golden calf inci-
dent (Ex. 32). First, there was an act of corporate rebellion in
which the high priest, Aaron, had participated. Aaron’s act of
rebellion had been a re-capitulation of the original sin of Adam,
who ate a forbidden covenantal meal as mankind’s representa-
tive high priest. This corporate act of rebellion involved the
whole nation. God required a bloody sacrifice to atone for it.
There is no atonement apart from the shedding of blood (Heb.
9:29).
