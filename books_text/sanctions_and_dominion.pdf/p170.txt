132 SANCTIONS AND DOMINION

ten homers: and they spread them all abroad for themselves
round about the camp” (vv. 31-32).° They began to eat. They
had enough food for a month; they would not enjoy it for an
hour. “And while the flesh was yet between their teeth, ere it
was chewed, the wrath of the Lorp was kindled against the
people, and the Lorp smote the people with a very great
plague” (v. 33). They got more than they had bargained for.

Aaron and Miriam

Moses’ brother and sister were not satisfied with their au-
thority. “And they said, Hath the Lorp indeed spoken only by
Moses? hath he not spoken also by us? And the Lorn heard it”
(Num. 12:2), God brought the three of them into the cloud at
the door of the tabernacle. Then He identified Moses as far
more than a prophet: “And he said, Hear now my words: If
there be a prophet among you, I the Lorn will make myself
known unto him in a vision, and will speak unto him in a
dream. My servant Moses is not so, who is faithful in all mine
house. With him will I speak mouth to mouth, even apparently
[openly, NASB}, and not in dark speeches; and the similitude
{likeness] of the Lorp shall he behold: wherefore then were ye
not afraid to speak against my servant Moses?” (vv. 6-8).

God removed the cloud. Miriam was now leprous. Biblical
leprosy was a disease of God’s judgment.’ God did not strike

8. The Hebrew words “cubits face earth” were translated as “two cubits high
upon the face of the earth.” This translation cannot be correct. A pile of dead quail
three feet high in an area in the range of 713 miles would constitute billions of quail.
If it was a day’s journey across the camp - say, 30 miles — then the radius was 15
miles. The formuta pi times r-squared gives 731 miles. The phrase “cubits face earth”
should be translated as “cubits above the face of the earth,” ie., the height at which
the quail flew into the camp: about three feet, where they could be hit with any
heavy implement. This was the interpretation of Rashi: “This means that they were
Aying at a height of two cubits fiom the ground so that they reached just up to a man’s
breast...” Rashi, Chumash with Targum Onkelos, Haphtaroth and Rashi's Commentary, A.
M. Silbermann and M. Rosenbaum, translators, 5 vols. (Jerusalem: Silbermann
Family, [1934] 1985 [Jewish year: 5745)), IV, p. 88 (Num. 11:81).

9. Gary North, Leviticus: An Economic Commentary (Tyler, Texas: Institute for
