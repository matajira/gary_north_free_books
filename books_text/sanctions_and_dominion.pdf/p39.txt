INTRODUCTION

Behold, the days come, saith the Loro, that I will make a new coven-
ani with the house of Israel, and with the house of Judah: Not according
to the covenant that I made with their fathers in the day that I took them
by the hand to bring them out of the land of Kgypt; which my covenant
they brake, although I was an husband unto them, saith the Lorn (Jer
31:31-32).

Harden not your heart, as in the provocation, and as in the day of
temptation in the wilderness: When your fathers tempted me, proved me,
and saw my work. Forty years long was I grieved with this generation,
and said, It is a people that do err in their heart, and they have not
known my ways: Unio whom I sware in my wrath that they should not
enter into my vest (Ps, 95:8-11).

The Psalmist offers as a warning the Israelites’ wilderness
experience, which is the central focus of the Book of Numbers.
The wilderness experience was a curse: a negative sanction.
This curse was announced in God’s wrathful oath that the
exodus generation would not inherit the Promised Land. They
would die in the wilderness. Thus, what might have been a
temporary transition period in the lives of the exodus genera-
tion became their lifetime experience. The Promised Land was
associated with rest from their labors. Israel would not gain this
rest during their lifetimes. “So I sware in my wrath, They shall
not enter into my rest.”
