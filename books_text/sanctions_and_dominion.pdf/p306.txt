268 SANCTIONS AND DOMINION

that there was to be mercy shown to some and none to others:
“Now therefore kill every male among the little ones, and kill
every woman that hath known man by lying with him. But all
the women children, that have not known a man by lying with
him, keep alive for yourselves” (Num. 31:17-18).

The Defeat of Paganism’s Gods

These women were pagans by profession of faith and life-
style, yet they were eligible to become wives. How could the
purity of marriage be maintained? Because these women were
captives who were no longer under the covenantal authority of
pagan gods. The gods of the pagan world were local gods, gods
of the city-state. When a city was defeated, so were its gods.
When a city was utterly wiped out, so were its gods. These
captive women now had no ritual connection with the gods of
their city.? Those gods had been wiped off the face of the
earth. Israel was instructed to destroy foreign cities and every
male inside its walls. This was the proof of the defeat of the
city’s local gods.

When the gods of a pagan city outside of Canaan fell to
israel, Israel was to have no fear of them again. In contrast, the
gods of Canaan were a continuing threat, since they were tied
to the land itself. To spare anyone in those cities in which Israel
intended to dwell was not allowed. “But of the cities of these
people, which the Lorp thy God doth give thee for an inheri-
tance, thou shalt save alive nothing that breatheth: But thou
shalt utterly destroy them; namely, the Hittites, and the Amor-
ites, the Canaanites, and the Perizzites, the Hivites, and the
Jebusites; as the Lorp thy God hath commanded thee: That
they teach you not to do after all their abominations, which

2. They may not have had any connection hefore, except through their fathers.
This was the case in Greece and Rome. Women did not participate in the rites of the
city, only in household rites.
