248 SANCTIONS AND DOMINION

the sentiment of justice; it was the sacred formula.”* Women,

not having access to the religious rites of the city, could not
lawfully invoke this formula.

It took over three millennia for the Bible’s principle of the
oath-bound female judge to be honored on a widespread basis
in society. Women in the West received the civil franchise in
the years following the First World War (1914-18), the war that
also ended kingship in the West. Kingship was allowed by the
Mosaic Covenant (Deut. 17), but it was a less preferred judicial
order than rule by judges (I Sam. 8). A female judge was in
general preferable to a king under the Mosaic system, but it
took the enormous social disruption of World War I for the
West to acknowledge this politically by driving out its kings and
giving women the vote. Bible-believing churches resisted this
political development. Pagan suffragettes - freethinkers and
Spiritualists’ — and their masculine, liberal, and radical political
allies first promoted it on a national level in England and the
United States. The women’s suffrage movement in the United
States was secular."° In the United States in the late nine-
teenth century, there was an alliance in the North and West
between anti-liquor temperance societies, dominated by women
and including Protestant evangelicals, and the women’s suffrage
movement, but the national leaders of the evangelical churches
did not support women’s right to vote. In the American South,
the suggestion was resisted strongly."

When the church and Christians refuse to extend God’s
kingdom principles in history, covenant-breakers may decide to

8, Ibid., IL-xi, p. 191.

9. Richard J. Carwardine, Evangelicals and Politics in Anicbeltum America (New
Haven, Connecticut: Yale University Press, 1993), p. 32. He cites Anne Braude,
Radical Spirits: Spiritualism and Women’s Rights in Nineteenth-Century America (Boston:
Beacon, 1989).

10. Eleanor Flexner, Century of Struggle: The Woman's Rights Movement in the United
States (rev. ed.; Cambridge, Massachusetts: Harvard University Press, 1975).

1], Robert T: Handy, A Christian America: Protestant Hopes and Historical Realities
(2nd ed.; New York: Oxford University Press, 1984), pp. 80-81.
