48 SANCTIONS AND DOMINION

ture.”"® Furthermore, “what the statistics of human action
really show is not regularity but irregularity. The number of
crimes, suicides, and acts of forgetfulness . . . varies from year
to year.”"* The biologist, writes sociologist-historian Robert
Nisbet, can predict furure changes in some environmentally
controlled population, but “It is very different with studies of
change in human socicty. Here the Random Event, the Maniac,
the Prophet, and the Genius have to be reckoned with. We
have absolutely no way of escaping them. The future-predicters
don’t suggest that we can avoid or escape them - or ever be
able to predict or forecast them. What the future-predicters, the
change-analysts, and trend-tenders say in effect is that with the
aid of institute resources, computers, linear programming, etc.
they will deal with the kinds of change that are not the conse-
quence of the Random Event, the Genius, the Maniac, and the
Prophet. To which I can only say: there really aren't any; not
any worth looking at anyhow.”’*

 

Government statistics are used by economic planners, includ-
ing the central bank, to regulate the national economy. Not that
these statistics are accurate or even useful. Older data are con-
stantly being revised. But they create the illusion that govern-
ment planners are capable of making effective representative
decisions for consumers on the basis of an overall economic
plan. The planners supposedly are capable of devising compre-
hensive, scientific, economic input-output grids, inserting the
latest data, and presto: an accurate picture of the economy
emerges. This picture then supposedly enables them to forecast
the future effects of their official decisions. This is a politically
convenient myth. Academic studies of government forecasting

13. Ludwig von Mises, The Ultimate Foundation of Economic Science (Princeton,
New Jersey: Van Nostrand, 1962), p. 56.

14, Ludwig von Mises, Theory and History: An Interpretation of Social and Economic
Evolution (New Haven, Connecticut: Vale University Press, 1957), pp. 84-85.

13. Robert A. Nisbet, "The Year 2000 and All That,” Commentary (June 1968), p.
66.
