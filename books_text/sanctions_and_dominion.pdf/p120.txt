4

THE FIRSTBORN AND
NEGATIVE ECONOMIC SANCTIONS

And the Lorp spake unto Moses, saying, And I, behold, I have taken
the Levites from among the children of Israel instead of all the fersthorn
that openeth the matrix among the children of Israel: therefore the Levites
shall be mine; Because ail the firstborn are mine; for on the day that I
smote all the firstborn in the land of Egypt I hallowed unto me all the
firstborn in Israel, both man and beast: mine shall they be: I am the
Lorp (Num. 3:11-13).

The theocentric focus of this law is God’s ownership. This
passage announced God’s unique proprietary claim on the
Levites because of their position as the sacrificial substitutes for
the firstborn sons of Israel. As Creator, God owns everything,
but He established here a special claim on the firstborn, includ-
ing animals. This special claim had its origin in God’s execution
of the firstborn sons of Egypt.

God hallowed (kawdash) the firstborn. The Hebrew word
kawdash means holy or sanctified. The word is also used with
respect to the sabbath. “And God blessed the seventh day, and
sanctified [kawdash] it: because that im it he had rested from alt
his work which God created and made” (Gen. 2:3). “Remember
the sabbath day, to keep [kawdash] it holy [kaadash]” (Ex. 20:8).
