34 SANCTIONS AND DOMINION

In other words, at the very center of the formation was the Ark
of the Covenant, the dwelling place of God. This had to be
protected by the nation, at the cost of their lives. The last de-
fensive barrier was the tribe of Levi.

This meant that the Levites did not put themselves at risk as
a tribe in the initial confrontation with the enemy. Only if the
enemy broke through the lines did the Levites go into battle.
From the point of view of military risk, the Levites were pro-
tected by the structure of the army’s formation.

Defending the Tabernacle

Because members of the other tribes could not approach the
tabernacle when it was being moved by the Levites (Num.
1:51), they probably would have hesitated to pursue invaders
who had broken through the lines and who were approaching
the tabernacle. It is not said that God held non-Levites respon-
sible for approaching the tabernacle as its would-be defenders,
but this silence would have produced psychological hesitancy ~
often fatal to military defenders. In all likelihood, they were not
allowed to approach the tabernacle in wartime, even for the
sake of defending the Ark. The later example of Uzzah, who
reached out to steady the Ark as it was being moved, indicates
that this was the case: when he touched it, he was killed by God
on the spot (II Sam. 6:6-7). So, once the outer lines were
breached by the enemy, the Levites would have fought alone.
This made them even more dependent on the other tribes. The
Levites were warriors, but their task was different: to defend
the tabernacle, not to defend the land.

A military commander has the obligation to estimate what his
forces are. Mustering was a pre-war event. Because they were
warriors with a defensive assignment, to protect the tabernacle,
it was lawful for the Levites’ commander to number them. The
list of mustered tribes does not include the Levites in either
instance in Numbers; this population figure is always given
separately. “But the Levites after the tribe of their fathers were
