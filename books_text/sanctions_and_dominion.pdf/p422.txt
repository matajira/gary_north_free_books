384 SANCTIONS AND DOMINION

blood avenger, 301, 302-305,
306-308

bloodshed
atonement, 21-22, 26,
45, 56, 94, 309

broken neck, 94
circumcision, 8
conquest, 282-83
covenantal, 26
deliverance (Passover),
87
donkey, 94-95
Egypt's sons, 95
golden calf, 90-91
mixed multitude, 362-63
numbering, 44-45, 53
open roads, 304
priesthood, 56
Boaz, 303
booty, 273, 274, 277, 335
Borgias, xv
boundaries
city of refuge, 310
holiness, 79
house, 307
judicial, 185-86
metaphysical, 181-83
oath, 253-54
sacred, 69, 74, 77, 79-80
sanctions &, 158
symbolic, 183-85
tabernacle, 157-58
transgression, 69
tribal, 239, 241
brass serpent, 195-98
British Israelism, 239
broken neck, 94-95
bureaucracy, 76

bureaucrat’s rule, 211

cadeuceus, 195.
Cain, 92, 174, 185
Caleb, 20, 134, 136, 188, 290n,
327, 328
Calvin, John
eschatology, xxiii
Kline vs., xix-xxii
sanctions, xix-xx
Servetus &, xxiv
Ten Commandments, xi
Calvinism, xvii-xvili, xxiv, xxi,
xxiti
Ganaan
allocating, Chap. 14
conquest, 7-10, 53, 296
disinheritance, 6-7, 188,
325
eschatology, 297
fear of Israel, 275-76
holy war, 23
inheritance, 27, 231-41
numbering, 20, 22
Promised Land, 1, 7-8,
140-48, 149-50, 152,
216, 231
spies, 133-37
cannibalism, 209n-210n
capital (dominion &), 3
capitalization, 280-82
captivity, 271 (see also exile)
caste, 74, 80-81
casuistry, 315-16, 317
cattle, 84, 87, 127, 176, 228,
265-66, 278, 280, 282, 287,
290
