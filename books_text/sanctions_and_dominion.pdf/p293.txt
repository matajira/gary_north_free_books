Oath and Sanctions 255

swear by the temple, sweareth by it, and by him that dwelleth therein.
And he that shall swear by heaven, sweareth by the throne of God, and
by him that sitteth thereon (Matt. 23:16-22).

Jesus challenged all such man-made rules. The word of a
covenant-keeper should be simple and direct. There must be
no verbal tricks, no counterfeit oaths subsequently declared null
and void because of an imprecise formula. James repeated this
warning: “But above all things, my brethren, swear not, neither
by heaven, neither by the earth, neither by any other oath: but
let your yea be yea; and your nay, nay; lest ye fall into condem-
nation” (James 5:12),

A covenant oath does not invoke surrogate phrases for heav-
en. It invokes God’s name. This oath is a direct verbal appeal to
God because it is taken under a unique institution that has been
authorized by God to impose sanctions in His name: church,
family, or State. This authority to impose God's sanctions in
history is what identifies the institution as covenantal.

Social Contract Theory

The four biblical covenants — personal, ecclesiastical, familial,
and civil — have served as models for other relationships and
institutions. The corporate covenants linking God, man, and
other men have been imitated by atheistic contracts. A contract
is analogous to a covenant, but with God’s name removed from
the formal agreement. It does not invoke God’s name or His
sanctions in enforcing the agreement's stipulations.

The civil covenant under God has become a civil contract
among men. There is no historical evidence that such an act of
contracting ever took place. This historical event is completely
hypothetical. This was well understood by Rousseau, whose
Social Contract (1762) was preceded by his earlier study of the
subject, his dissertation on inequality. In that essay, he wrote of
the state of nature, the hypothetical pre-contractual judicial
condition: “Let us begin then by laying facts aside, as they do
