352 SANCTIONS AND DOMINION

and took it. They had two options: they could go out of Egypt
into the wilderness or into Philistia on their own, or they could
link up to the nation whose God had just smashed the Egyptian
social order. Add to this the miracle of the manna: nearly free
food” until the conquest (Ex. 16).

A lot of them could have chosen the latter option: to stay
with the Israelites. This explains the presence of the mixed
multitude with the Israelites (Ex. 12:38). It is possible that some
of these people were adopted into the families of Israel as full
members. For the sake of argument, let us consider the possi-
bility that.che bulk of the mustered Israelites were recent adop-
tees, and the firstborn sons were biological sons, not adoptees.
If true, this would solve the 27-to-one demographic problem.

Biological Sons vs. Adopted Sons

The firstborn sons were biological sons of Israel (Num. 3:12).
They were the minor sons of Joshua's generation. Let us as-
sume that this strictly biological definition of firstborn governed
the mustering process. Those who were subsequently ingrafted
into the nation through adoption were not counted as firstborn
sons retroactively back to Passover, nor were their children,
who had not been born under the covenant. These pre-muster-
ing adoptees added to the number of fighting-age males, but
they and their children were not counted as firstborn. Firstborn
was biological, not judicial: “opens the matrix” (Ex. 13:15).

After the completion of the tabernacle, the Levites were set
apart by God as a separate tribal offering in place of biological
firstborn sons, as we have seen (Num. 3:12-13). This revelation
came after the other tribes were mustered and just before Levi
was numbered. This judicial substitution of Levites for firstborn
minor sons was a one-time event that took place four months
after the mustering in Exodus.

29, There had to be grinding and cooking.
