192 SANCTIONS AND DOMINION

Sihon: it hath consumed Ar of Moab, and the lords of the high
places of Arnon” (Num. 21:27--28).

The question arises: When did this final series of wars begin?
Did the generation of the exodus initiate them? Or was it the
generation of the conquest? It was the latter. Miriam and Aaron
were dead by the time the battles began. They died in the
fortieth year after the exodus (Num. 33:38). By this time, all of
the older generation had diced off except Moses. They had died
during the 38 years from the exodus to Israel’s arrival at Ka-
desh-barnea (Deut. 2:14), This took place prior to the journey
to Mt. Hor (Num. 33:36-37), where Aaron died.

Moses was old. It was he who would announce the terms of
the inheritance to the next generation: the Book of Deuterono-
my is a record of these terms. This second giving of the law
(deutero, nomos) was preparatory to national covenant renewal:
the circumcision of the conquest generation (Josh. 5:7). God's
prophecy to Abraham regarding the inheritance of the fourth
generation (Gen. 15:16) was about to be fulfilled. What we see
in this chapter is the manifestation of a new psychology of
victory in Israel. There was one final rebellion. After this, open
rebellion ended until after the conquest.

Hormah

Hormah was the city where the Israelites had suffered a
major military defeat. They had attempted to prove Moses
wrong regarding their inability to prosper militarily after their
attempted stoning of Joshua and Caleb. “Then the Amalekites
came down, and the Canaanites which dwelt in that hill, and
smote them, and discomfited them, even unto Hormah” (Num.
14:45). That legacy of defeat would now be reversed.

Chapter 21 is an account of several wilderness wars. The first
was the war with king Arad the Canaanite. He lived in the
south, outside the borders of Canaan (Josh. 12:6-7, 14). He
started a war with Israel, and he took some of them prisoner
(Num. 21:1). “And Israel vowed a vow unto the Lorn, and said,
