8

DEFERRED GRATIFICATION

Speak unto the children of Israel, and say unto them, When ye be
come into the land of your habitations, which I give unto you, And will
make an offering by fire unto the Lorp, a burnt offering, or a sacrifice
in performing a vow, or in a freewill offering, or in your solemn feasts,
to make a sweet savour unto the Loro, of ihe herd, or of the flock. Then
shall he that offeveth his offering unto the Lorn bring a meat offering of
a tenth deal of flour mingled with the fourth part of an hin of oil. And
the fourth part of an hin of wine for a drink offering shalt thou prepare
with the burnt offering or sacrifice, for one lamb (Num. 15:25).

The theocentric focus of this passage is God’s ownership of
the Promised Land. When making an offering, the Israelite
would have to offer oil, bread, and wine to God. This represen-
tative or token offering pointed to God’s ownership of the
source of bread and wine: the land. This law would come into
force only after the inheritance had been delivered. The law
did not apply in the wilderness.

Who is the giver of gifts? God. He reasserted His claim on
Israel by reminding them of the inevitability of the Promised
Land. He promised again to give the land to them. It would be
the Jand of their habitation. This means that it would be the
land of God’s habitation, Obviously, the wilderness was not to
be their place of habitation. It was merely a transitional resi-
