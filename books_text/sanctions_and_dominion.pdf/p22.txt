xxii SANCTIONS AND DOMINION

dismissed. such a view of historical causation as pagan to the
core. Yes, he said, following David, good things sometimes
happen to bad people and bad things to good people, but this
is merely Satanic deception. “When such is the state of matters,
where shall we find the person who is not sometimes tempted
and importuned by the unholy suggestion, that the affairs of
the world roll on at random, and as we say, are governed by
chance?”> With respect to his theory of visible cause and
effect in history, Kline succumbed to the temptation.

The theological contrast between Kline and Calvin could not
be sharper. In the name of Calvin, Kline has abandoned Cal-
vinism and has substituted an ethical dualism consistent with
Lutheranism, Anabaptism, and, for that matter, Enlightenment
humanism. His theory boils down to this: in this world, God
does not defend or extend His law by means of humanly pre-
dictable corporate sanctions. On this point, covenant-breakers
are in full agreement with Kline. (So, from what I can see, are
most of his colleagues at Westminster Seminary.)

The Christian Ghetto: Living Under Humanism’s Sanctions

Couple Kline’s view of God’s unpredictable corporate sanc-
tions in history with the amillennialism of sixteenth-century
Calvinism, and the result is ghetto Christianity: the mentality of
a defensive community of besieged and culturally doomed.
Christians — “cannon fodder for Christ.” Its unofficial slogan is:
“Of the ghetto, by the ghetto, for the ghetto!” With respect to
Christian civilization, these ghetto theologians deeply believe,
“Once lost, always lost.” Christianity must remain a strictly
defensive operation culturally. Although Christians created
Western civilization, once the humanists conquered it in the

28. John Calvin, Commentary on the Book of Psalms (Grand Rapids, Michigan:
Baker, [1557] 1979), III, p. 122.

29, Gary North, Westminster’s Confession: The Abandonment of Van Til’s Legacy
(Tyler, Texas: Institute for Christian Economics, 1991).
