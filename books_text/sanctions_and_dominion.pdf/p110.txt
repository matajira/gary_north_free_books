72 SANCTIONS AND DOMINION

to provide free freight hauling services. Those who were in-
volved in transporting the tabernacle became priests for the
duration of the journey: full-time priestly servants of God. The
free market’s pricing principle — high bid wins ~ did not apply
to the ecclesiastical services performed by the Levites. The tithe
did.

The Mosaic Covenant clearly established the principle of
tribal interdependence. In Mosaic Israel, the tribes other than
Levi were covenantally incapable of serving God sacramentally
by themselves. They became covenantally dependent on mem-
bers of the tribe of Levi to serve as intermediaries between
them and God. The inter-tribal link among the other dozen
tribes was the tribe of Levi, which served all the others and
collected tithes from them.

The Levites were heavily (though not exclusively) dependent
economically on the other tribes for their income. They were
more dependent in the early stages of Mosaic Israel's history
than God intended them to be as time went on. God made
them economically dependent initially by way of the laws of
landed inheritance: they did not participate in the original
distribution, nor could they buy up rural land or inherit it.!*
As the nation grew in numbers and wealth, however, this eco-
nomic dependence would have been reduced by the increasing
value of urban property in relation to rural land. Mosaic law
was biased against capital in rural land, for the law favored
population growth: fewer miscarriages (Ex. 23:26) and longer
life spans (Ex. 20:12) for covenantal obedience. Population
growth in the context of a fixed supply of rural land, with all
male heirs inheriting, leads to ever-smaller family allotments.’”
Under such conditions of covenantal blessing, the Levites, who

16. With this exception: “And if he will not redeem the field, or if he have sold
the field to another man, it shall not be redeemed any more. But the field, when it
goeth out in the jubile, shall be holy unto the Lorn, as a field devoted; the possession
thereof shall be the priest's” (Lev. 27-20-21). See North, Lewiticus, ch. 87.

17. Thid., ch. 34,
