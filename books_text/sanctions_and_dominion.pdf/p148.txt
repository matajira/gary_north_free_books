110 SANCTIONS AND DOMINION

peace, though I walk in the imagination of mine heart, to add drunk-
enness to thirst: The Lorp will not spare him, but then the anger of the
Lorp and his jealousy shall smoke against that man, and all the curses
that are written in this book shall lie upon him, and the Lorp shall blot
out his name from under heaven (Deut. 29:18-20).

When a person under the oath-bound covenant has God’s
name removed from him, his name is blotted out in history.
The curses will come on him if he fails to repent and take up
God’s name again. This is the structure of biblical law. But who
formally removed God’s name from a person under the Mosaic
Covenant? The final earthly authority to do this was the priest-
hood, with the sons of Aaron comprising the high court prior
to the exile.

When the sons of Aaron departed into apostasy under Eli,
the nation lost the war with the Philistines. The Ark was lawful-
ly removed from the tabernacle in times of war. But in Eli’s
day, it was captured on the battlefield by the Philistines (I Sam.
4). After the Philistines sent the Ark back by cart, it was not
immediately returned to the tabernacle. The sons of Aaron no
longer offered sacrifices in the presence of the Ark. Only under

David’s kingship was the Ark returned to Jerusalem (II Sam.
6:17).

Good News from False Prophets

For the priests to have blessed Israel when Israel was in
rebellion would itself have been an act of rebellion. This would
have been a public manifestation of the nation’s covenant rebel-
lion. To call down God's blessings on rebellious people is to
break covenant with God. The mark of a false priesthood was
the invocation of God’s blessing of peace on a nation in ethical
rebellion. Even Ahab, the consummate evil king of Israel, un-
derstood this. He knew the difference between a prophet who
told him what he wanted to hear and a prophet who told him
the truth. He just refused to listen to the truth.
