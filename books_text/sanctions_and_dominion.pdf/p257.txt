Dividing the Inheritance 219

they would be granted larger territories within the general
region. The modern commentator, Jacob Milgrom, thinks that
this is the correct approach, with this modification: the families,
too, would receive their plots in terms of their size, not by lot.*
But is he correct?

Tf allocation was strictly by lot to each family, then the pre-
sumption is that the plots allocated were all the same size. But
the allocation was not strictly by lot. There was also a consider-
ation of family size. The question is: Which family? Was a fami-
ly determined on a “one numbered warrior, one family plot”
basis? Or was it based on the family name within each tribe? Or
was it some sort of mixture?

Individual Plot or Tribal Plot?

In the second Numbers mustering, each tribe’s census was
broken down into families, and each family was named. In the
first Numbers mustering, only Levi’s report was broken down
by family names (Num. 3:17). The other tribal families were not
named. After each tribal name, this phrase occurs: “. . . after
their families, by the house of their fathers, according to the
number of the names, from twenty years old and upward, all
that were able to go forth to war.” The distinguishing mark of
this earlier numbering is this phrase: “according to the number
of the names.” In the numbering described in Numbers 26, this
phrase does not occur. Instead, the name of each family ap-
pears. This points to the importance of family name in the
second wilderness. numbering. Each family knew that it would
be a part of a victorious military campaign. Each knew what the
terms of the inheritance were. Each had its name recorded in
anticipation of the victory.

By identifying family names, meaning the names of the sons
of the twelve patriarchs (Joseph’s two sons initially counting as

4, Jacob Milgrom, The JPS Torah Commentary: Numbers (New York: Jewish Publi-
cation Society, 1990), p. 481.
