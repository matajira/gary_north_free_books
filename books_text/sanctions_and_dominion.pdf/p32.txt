xxxii SANCTIONS AND DOMINION

however. He immediately moves from the question of civil law
to the church, calling on the church to exercise only the power
of excommunication. This is an illegitimate line of argument.
The two systems of covenantal sanctions are judicially separate:
State vs. church. Any discussion of church sanctions as if these
in some way constitute the whole of the kingdom’s earthly sanc-
tions is in error. If the kingdom is more than the institutional
church, which it is, then a covenant theologian must discuss
civil sanctions in terms of covenantal law. But Rev. Engelsma,
whose theology becomes pietistic at this point, prefers to discuss
only church sanctions. He wants his readers to imagine that
only church sanctions possess the legitimate designation of
kingdom sanctions in history. He writes: “For the church is a
spiritual realm. She does not, e.g., put adulterers and homosex-
uals to death. Where there is public, impenitent practice of
these sins, the church exercises discipline, which is a spiritual
key of the kingdom of heaven. Her purpose is the repentance
of the sinner, so that she may again receive him into her fellow-
ship.”*”

This logically irrelevant comment deflects the reader’s atten-
tion from the crucial judicial issue: the function of civil sanctions
in a Christian commonwealth.*® No author in the Reconstruction-
ist camp has suggested or implied that the institutional church
has the authority to impose civil sanctions.” The issue of crimi-
nal sanctions is a State matter. It is here that Christians, as Chris-
tians, are required by God to suggest explicitly biblical defini-
tions of crime. But Rev. Engelsma has already ruled out any
appeal to the Mosaic law as a possible standard for definitions
of crime. Why? He offers no exegetical or hermeneutical rea-
sons; he apparently just does not like the Mosaic law.

47. Engelsma, “Jewish Dreams,” op. cit., p. 175.

48, Kenneth L. Gentry, “Civil Sanctions in the New Testament,” in Theonomy: An
Informed Response, edited by Gary North (Tyler, Texas: Institute for Christian Eco-
notnics, 1991), ch. 6.

49. Gentry, “Church Sanctions in the Epistle to the Hebrews,” ibid., ch. 7.
