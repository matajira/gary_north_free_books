Tithe and Sanctions 173

Conclusion

The tithe is grounded legaily in the sovereignty of ordained
officials over the administration of the sacraments. It is the
biblically mandated payment for sacramental services rendered.
In Mosaic Israel, these services involved animal and other
sacrifices. They also involved Passover and the two other man-
datory national feasts. The Levites were guardians of the taber-
nacle area, while the Aaronic priests were the guardians of the
Ark of the Covenant, This guardianship had to be paid for.
The Levites received a tithe of the net income of the Israelites;
the priests received a tithe of the income of the Levites. Income
flowed up the chain of ecclesiastical command.”*

By giving the Levites a tithe rather than rural land as the
tribe’s inheritance, God balanced both the social order and the
political structure of Mosaic Israel. Levites served as legal advi-
sors in every region. They had local allegiances economically,
but they also had a national allegiance judicially: the priest-
hood. Neither the king nor local tribal leaders could exercise
primary infiyence over the Levites as a tribe. Levites owed their
allegiance to a different chain of command, ecclesiastical rather
than political:

The Israelites were not buying their salvation with their
tithes. They were paying for human services associated with the
operation of the sacrificial system. The tithe was not a market
price, i.e., high bid wins. Rather, it was a priestly price: propor-
tional giving. The poor man and the rich man paid the same
proportion. This made sure that the day-to-day administration
of the sacrificial system involved an equal economic sacrifice for
all. This form of equality was the equality of the percentage of
forfeited income, not the equality of price. The equality of price
— one price for all men - would have burdened the poor more
than the rich. It would also have created the illusion that salva-

24, See Chapter 3, above: section on “The Hierarchical Flow of Funds and
Service.”
