356 SANCTIONS AND DOMINION

of adopted sons also grew. Moses numbered all of the males of
Levi above one month old, not just firstborn sons (Num. 3:39).

Given the mass adoption theory, a relatively small Israelite
population existed on Passover night. We know that there were
about 22,000 biologically firstborn males one year after the
exodus. This was every firstborn male above one month old
(Num. 3:40). If the definition of firstborn was common to both
Israel and Egypt on Passover night, which I think was the case,
and only household-resident firstborn Egyptian sons died,
which I also think was the case, then the firstborn Israelites
were unmartied sons living in their fathers’ households. For
reasons already offered, I argue that firsiborn in Numbers meant
firstborn males under age 20. I use the 22,000 figure as a mar-
ker. There would also have been somewhere in the range of
22,000 firstborn females, But bear in mind that what consti-
tuted a firstborn son in Numbers was not exactly the same as at
Passover. The definition had changed: males under age 20.

If Israel’s population prior to the exodus had been at the
replacement rate level, there would have been a one-to-one
ratio between firstborn and the total population of each gender:
every person a firstborn. This would mean that the number of
children of Joshua’s generation was about 45,000, There were
45,000 parents and possibly even 45,000 grandparents. Israel’s
population would have been somewhere in the range of
135,000 people. Again, this assumes zero population growth.
But if Aaron's family size was typical - four sons — then there
was actually considerable population growth. This means that
there were fewer grandparents (Moses’ generation) than par-
ents (Joshua’s generation).

If Aaron's family and Zelophehad’s wilderness family of five
daughters (Num, 26:33) were typical, then the Israelites were
multiplying above the replacement rate by a factor of two.

33. Zelophehad’s family was not typical in the wilderness. It was abnormally
large. Replacement-rate demographics were dominant. Pethaps he kept trying for a
