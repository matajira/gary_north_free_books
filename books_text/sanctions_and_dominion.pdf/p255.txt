Dividing the Inheritance 217

This command to allocate portions of the land followed the
second wilderness numbering of Israel (Num. 26:1-2). The
nation had already been involved in a series of defensive wars
against Canaanites who dwelt outside the boundaries of the
Jordan. Israel did not initiate them. (The conquest of Jazeer
may have been an exception.) Israel was victorious over these
nations and had begun to occupy large tracts of real estate, but
only because the previous holders had attacked Israel rather
than allowing Israel access through their lands. This was the
down payment on Israel's inheritance, prophesied by Abraham:
“But in the fourth generation they shall come hither again: for
the iniquity of the Amorites is not yet full” (Gen. 15:16). Now,
the iniquity of the Amorites had become full. The Amorite
tribes outside of the boundaries of Canaan had launched a pair
of offensive campaigns against Israel (Num. 21:23, 33), which
they lost. This marked the beginning of the conquest.

By Lot or by Need?

The nation numbered 601,730 men of fighting age (v. 51),
which was very close to what it had been a generation earlier.
Once this was ascertained, God laid down the law of spoils.
First, it was by family size. “To many thou shalt give the more
inheritance, and to few thou shalt give the less inheritance: to
every one shall his inheritance be given according to those that
were numbered of him” (v. 54). Second, it was by lot. “Notwith-
standing the land shail be divided by lot: according to the
names of the tribes of their fathers they shall inherit” (v. 55).
Taken at face value, these two rules are inconsistent. If distribu-
tion is strictly by lot, then there is no way to allocate property
in terms of “larger families—-more land.” A plot of land will go
to the family selected by lot.

Pre-modern rabbinical commentators were not agreed on a
way to resolve this, Some, following Rashi, argued that the Holy
Spirit (Ruack Hakodesh) allocated unequal portions to the fami-
lies. “Although the portions were not of equal area because, as
