How Large Was Israel’s Population? 353

We might assume that the foreign adoptees were grafted
into Israel covenantally through circumcision, but this may not
have been the case. Those who came out of Egypt were circum-
cised (Josh. 5:5), but this presumably refers only to Israelites at
the time of the crossing of the Red Sea. This great miracle
allowed everyone accompanying them to escape, including the
mixed multitude. There is no reason to believe that the passage
in Joshua refers to the mixed multitude, who would not have
been circumcised. This miracle of dry passage must have per-
suaded the mixed multitude, just as it persuaded the Canaan-
ites, that God was with Israel. At this point — after the Red Sea
exodus ~ some would have asked to be adopted into Israel.
Perhaps Israel circumcised these newcomers, but perhaps not.
Israel did not circumcise those sons who were born in the
wilderness. If adoption did take place without circumcision, a
lot more of the mixed multitude males would have consented
to be adopted.

Why would Israel have agreed to this mass adoption? Be-
cause of their graciousness? Perhaps, but the thought of adding
a huge number of potential fighting men to the army would
surely have been a major motivation. This decision would soon
cost the Israelites a lot of money: the payment of silver at the
first mustering and presumably also at the second and third.
The adopters had to fund the adoptees’ payments. The mixed
multitudes had not received the inheritance of the Egypt-
ians."" Perhaps they had some silver, but if they were escaping
slaves, this is doubtful. The immense number of adoptees in
comparison with the number of biological Israelites meant that
this adoption into the army of the Lord must have been ex-
tremely expensive for each Israelite family. Each family would
have had to fund the atonement payment of its adoptees. Ex-
panding the army of the Lord was a costly venture for each

30. See Chapter 4, above: section on “Passover, Sanctions, and Succession,”
subsection on “Sanctions and Inheritance," pp. 86-88,
