260 SANCTIONS AND DOMINION

consciously or unconsciously follow Mr. Darwin; but before Mr.
Darwin, they followed Newton. Some single law, like the law of
gravitation, swung each system of thought and gave it its princi-
ple of unity.” This shift in outlook from Newtonianism to
Darwinism in social theory was basic to the American Progres-
sivism. It justified the creation of a planned economy.

The checks and balances built into the federal government
by the Constitution had become a hindrance to effective politi-
cal action, he said. This language of balances reflects mecha-
nism. We need to overcome this mechanical way of thinking,
Wilson insisted: “The trouble with the theory is that govern-
ment is not a machine, but a living thing. It falls, not under the
theory of the universe, but under the theory of organic life. [t
is accountable to Darwin, not to Newton, It is modified by its
environment, necessitated by its tasks, shaped to its functions by
the sheer pressure of life. No living thing can have its organs
offsct against each other as checks, and live. On the contrary, its
life is dependent upon their quick cooperation, their ready res-
ponse to the commands of instinct or intelligence, their amica-
ble community of purpose. Government is not a body of blind
forces; it is a body of men, with highly differentiated functions,
no doubt, in our modern day of specialization, but with a com-
mon task and purpose. Their cooperation is indispensable,
their warfare fatal. There can be no successful government
without leadership or without the intimate, almost instinctive,
coordination of the organs of life and action. This is not theory,
but fact, and displays its force as fact, whatever theories may be
thrown across its track. Living political constitutions must be
Darwinian in structure and in practice.””

This was the Progressives’ worldview: the State as a central-
ized agency of reform in which sufficient political power is

24. Woodrow Wilson, The Constitutional Gavernment of the United States (New York:
Columbia University Press, [1908] 1961), pp. 54-55.

25. fbid., pp. 86-57.
