3
KIDNAPPING

And he that stealeth a man, and selleth him, or if he be found in
his hand, he shall surely be put to death (Ex. 21:16).

In Chapter: 2, I set forth my thesis that tfie pleonasm, “he
shall surely be put to death,” is binding on the civil authoritics
when the State initiates the prosecution of the covenant lawsuit,
but it does not bind the victim when he initiates the prosecution.
‘We must examine the implications of this principle in the case of
kidnapping, a crime that is bound by the terms of the pleonasm.

Before getting to this problem, however, we must search for
the theocentric principle that governs the crime of kidnapping.
James Jordan quite properly lists kidnapping under the general
heading of violence..The nature of violence biblically is that it
represents an attemptcd assault on God, an attempt to murder
God by murdering His image.' He lists other aspects of violence:
the desire of sinful men to play god, the desire to achieve autono-
mous vengeance, and. sado-masochism.? Violence’ should be un-
derstood as a sinner’s rebellious attempt to achieve dominion by
power? It is a form of revolution. The preaching of the gospel is
intended to reduce violence.

Ultimately, this crime and its civil penalty should be under-
stood in terms of the assumption of a éheocentric universe. Jordan’s
assessment is valid: “The death penalty is appropriate because

1, James B. Jordan, The Law of the Covenant: An Exposition of Exodus 21-23 (Yyler,
Texas: Institute for Christian Economics, 1984), p. 93.

2. Ibid., pp. 93-96.

3. Ibid, p. 98.
