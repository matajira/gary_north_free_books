The Ransom for an Eye 127

ounces, but the criminal had not been willing to pay this much.
‘The criminal keeps what he wants: the 100+ ounces of gold that
the victim might have accepted in payment, but which the crimi-
nal refused to offer. The criminal would rather have this larger
quantity of gold than keep his eye. There is what the economists
call “reservation demand” for this money; the criminal pays with
his eye for his continued possession of the money.

None of this suggests that the criminal can buy justice. Justice
is what the court provides when it tries the case and imposes the
victim’s preferred sanction, up to the limit of the law. The crimi-
nal is buying a specific sanction that he prefers by offering the
victim an alternative which the criminal hopes the victim will
prefer, It is an auction for flesh, not an auction for justice,

The Private Slave Market

To give the criminal access to capital sufficient to make the
offer, the State must allow another auction for flesh: a slave
market. Deny this, and the criminal is thwarted in gaining what
he wants, and so is his victim. The most valuable asset @ criminal
may posscss is his own ability to work. If he is denied the legal
right to capitalize this asset, he may not be able to offer a suffi-
ciently high bid to the victim to avoid mutilation.

The modern democratic theorist professes horror at such a
thought. Why? Because the modem State’s disciples want the State to
have a monopoly on the slave tnarket. The State imposes prison as the
alternative to both restitution and slavery — an alternative which
benefits neither the victim nor the potentially productive criminal.

At this point, we return once again to the basic theme of the
Book of Exodus: the choice between slavery to man and service to God. It
is therefore the question of representation: Who is represented by
the State, God or autonomous man? When autonomous man is
represented by the State, then tyranny or impotence is the result.
Autonomous man seeks to enslave others, for he seeks to imitate
God, just as Satan imitates God. The State becomes the primary
agency of this enslavement process. It should not be surprising
to learn that the call for the abolition of chattel slavery in the
United States began-in the 1820’s in the Northeast, where the new
