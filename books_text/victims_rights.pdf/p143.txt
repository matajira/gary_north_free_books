The Ransoin for an Eye 131

judges to render honest judgment. They are forbidden to take
bribes (although it is not forbidden for righteous people to offer
bribes to corrupt judges). Judges are to render honest judgment
because the Bible requires it and because God requires it, not
because it is made personally profitable for them to do so. When
citizens distrust the judicial system, a fundamental weakness ex-
ists in the socicty. Bribes are a sign of such weakness and distrust.

The judges establish the initial penalty payment in the case
of a notorious ox that has killed a person (Ex. 21:30). What about
in the case of the crime of mutilation? Shouldn’t the judges set the
penalty? In the case of a non-injurious, accidental, premature
birth caused by another man’s violent behavior, the husband
establishes the penalty, and the judges then impose it. “If men
strive, and hurt a woman with child, so that her fruit depart from
her, and yet no mischief follow: he shall be surely punished,
according as the woman’s husband will lay upon him; and he
shall pay as the judges determine” (Ex. 21:22). This implies that
the judges can overrule the husband if the penalty is thought by
them to be excessive. The authority of the judges is supreme in
this case.

If it is true that the Bible requires the judges to assess the
penalty in the case of bodily mutilation, just as they do in the
case of criminal manslaughter (the owner of the notorious ox),
then they must make the decision: economic restitution or physi-
cal restitution. Both are legitimate forms of vengeance; both are
true forms of restitution. If the judges are solely responsible for
making this determination, then sovereignty is transferred to them
and away from the victim and the criminal, who might prefer to
come to a different, more mutually beneficial transaction. This
raises the question of righteous judgment. Why should the victim
and the criminal be excluded from the process of the setting of the
penalty? After all, in the case of the non-injurious premature
birth, the husband has ‘the opportunity of setting a preliminary
penalty, Why not in the case of mutilation?

19. Cf. Gary North, “In Defense of Biblical Bribery,” in Rushdoony, Institutes of
Biblical Law, Appendix 5; North, Tools of Dominion: The Case Laws of Hxodus (‘Lyler,
Yexas: Institute for Christian Economics, 1989), pp, 793-800.
