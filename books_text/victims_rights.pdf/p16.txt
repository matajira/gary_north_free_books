4 VICTIM’S RIGHTS

but fortunately for us, Jesus has abolished it, or at least drastically
softened its harsh aspects. Jesus, in other words, is meek and mild,
but His Father in heaven is mean and harsh. Fortunately for us,
we are assured, Jesus has persuaded His Father to change His
mind about the penalties of the law. God the Father insisted on
civil justice; fortunately for us, Jesus insists only on love.* “No
creed but the Bible, no law but love!” has been the battle cry of
fundamentalist antinomians throughout the twentieth century.

It is not only the fundamentalists who have been promoters
of this antinomian view of God’s law and sanctions in history. It
has been the whole Church, with the exception of the Calvinist
Protestant Reformers — William Tyndale is a good example,> but
so are Bucer® and Calvin’? ~ and the seventeenth-century Puri-
tans. They alone were willing to affirm a positive vi of Old
Testament jaw. Since then, it has been all! downhill: from the
Newtonian revolution — Newton, it should be noted, was in pri-
vate a dedicated alchemist and an anti-Irinitarian mystic’ —
through natural law theory and Scottish Common Sense ration-
alism? to modern neo-orthodoxy and neo-evangelicalism. God’s
people have hated His law. In response, He has steadily removed

 

4, See Lindsey, Road to Holocaust, p. 158.

5. See especially his Prologue to his translation of Jonah (1531) and his Prologue
to Romans (1534). GE Reventlow, Authority of the Bible, pp. 106-7.

6, Buecr, De regna Christi (1550); Reventlow, pp. 83-86

7. Jobn Galvin, The Counant Enforced, edited by James B. Jordan (‘Tyler, ‘Texas:
Tustitute for Christian Econamics, 1990).

8. See especially J. E. McGuire and P.M. Rattansi, “Newton and the Pipes of
’an,” Notes and Queries of the Royal Society of Londan, XXT (1966), pp. 108-43. See also
Betty J.T. Dobbs, The Foundations of Newton's Alchemy; Or, “The Hunting of the Green
Lyon® (Cambridge: Cambridge University Press, 1977); Keith Thomas, Religion and
the Decline of Magic (New York: Scribner's, 1971), pp. 226, 292, 352, 644; John
Maynard Keynes, “Newton the Man,” in Newton ‘Tercentemary Celebrations (Camb-
ridge: Cambridge University Press, 1947); Frances A, Yates, Collected Essays, vol. 3,
Teas and Ideals in the North European Renaissance (Loudon: Routledge & Kegan Paul,
1984), p. 121, On Newton's anti-Trinitarianism, see Gale E, Christianson, Jn the
Presence of the Creation: Isaac Newton and His Times (New Vork: Free Press, 1984), pp.
470, 564.

9. S.A. Grave, The Scottish Philosophy of Common Sense (Oxford: Clarendon Press,
1960). See also Sydney E. Ahlstrom, “The Scottish Philosophy and American Theol-
ogy,” Church History, XXIV (1955), pp. 257-72.

 

 
