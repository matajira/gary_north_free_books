272 VICTIM’S RIGHTS

ons,?6 but the most important one is that they thwart the biblical
principle of restitution.

Emptying Prisons and Stoning Sons

Prisons need to be emptied. The biblical way to accomplish
this is to revive the biblical practices of execution for habitual
criminals (Deut. 21:18), corporal punishment (Deut. 25:1-3), and
restitution, It is interesting that the justification for executing
habitual criminals rests on that bugaboo of all pietism, the execu-
tion of the rebellious son. It is a case of “if this, then how much
more fhat.” If it is mandatory that a man bring his incorrigible
adult son before the elders for gluttony, drunkenness,2? and verbal
rebellion, how much more ready will a society be to execute
repeatedly violent individuals or members of a professional crimi-
nal class! Remove from the law books the law regarding the civic
execution of the rebellious son, and you thereby remove the one
and only biblical sanction for executing professional criminals. The
“three-time loser” penalty of American jurisprudence®® has disap-
peared; in its place has come a criminal class of far more than
three felony convictions—and most of these professionals are
paroled early.

Incorrigible sons and incorrigible criminals are to be removed
from: society: *. . . so shalt thou put cvil away from among you;
and all Israel shall hear, and fear” (Deut. 21:21b). Rushdoony
has identified the importance of this law for society: “Such persons
were thus blotted out of the comnmionwealth. When and if this law
is observed, ungodly familics who are given to lawlessness are
denied a place in the nation. The law thus clearly works to
eliminate all but godly familics.””?

26. James R. Brantley and Marjorie Kravitz (eds.), Altematines io Institutionaliza-
tion: A Definitive Bibliography, published by the National Criminal Justice Reference
Service of the National Institute of Law Enforcement and Criminal Justice, a division
of the Law Enforcement Assistance Administration, U.S. Department of Justice
{May 1979}, 240 pages.

27, Seven-year-olds are not drunkards; this verse deals with adult rebels.

28. A man convicted of a felony for the third time used to receive life imprison-
ment without possibility of parole.

29. Rushdoony, Jnstituies, p. 380.
