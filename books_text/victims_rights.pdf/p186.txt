174 VICTIM'S RIGHTS

ity of the civil government. Highways are one example. If people
are to use the highways, they need protection, both as drivers and
pedestrians. The civil government erects stop signs and stop lights;
it places other road signs along the highways, so that drivers can
drive more safely and make better high-speed decisions. Similarly,
residential areas and school zones are restricted to slower traffic.
This protects pedestrians and home owners who would otherwise
face the continual threat of high-speed vehicles that are difficult
to control in tight quarters.

The posting of a speed limit is essentially the same as a
private citizen who posts a “no trespassing” sign, or a “beware of
dog” sign on his property. The sign serves as a substitute for the
“cover for the pit”; the sign, like the cover, is a device for protecting
the innocent. Where children in cities are forced to cross busy streets,
local governments hire crossing guards to control traffic and help
younger children across the street. Sometimes, older students in
@ grammar school serve as unpaid crossing guards in a safety
patrol. In some communities, fenced, overhead ramps are built
across busy highways. The fence scrves as a means of protection
for 1} pedestrians who might fall off the overpass ‘and 2) motorists
who face risks from vandals who would drop heavy rocks onto the
passing cars bencath. But fences are expensive, and they cannot
be built in every residential area. Thus, the civil government
establishes speed hmits, and it posts signs that warn drivers of
these limits.

A philosophy of nearly risk-free existence would impose speed
limits of no more than a few miles per hour on all drivers, except
perhaps on specially designed highways. But voters, who are both
pedestrians and drivers, would not long tolerate such utopian
restrictions. In most places in the United States, voters drive far
more hours during the day than they walk. So they will not allow
defenders of the rhetoric of risk-free living to have their way. They
make judgments as individuals that legislators must respect in the
aggregate: speed limits that meet the needs of voters, both as drivers and
pedestrians, or the parents of pedestrians. Oncc the speed limit
is posted, people make personal adjustments, both as drivers (by
slowing down to approach the legal limit, but Ictting pedestrians
