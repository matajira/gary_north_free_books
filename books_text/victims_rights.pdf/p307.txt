Violent Crime in the United States, 1980 295

pays. Moreover, some authorities insist that most crimes are not
reported to the police and that only 1'/2% of all crimes arc
punished, which is to say that 98!/2% of the crimes committed
go unpunished.”*

Aging and Crime

The rate of crime began to drop in the early 1980’s in the
United States. The only reasonable hope that citizens of the United
States seem to have for continuing this reduction in crime in the
near future, apart from a religious revival, is that with a falling
birth rate, the number of young men, especially unmarried young
men, ages 18-24, as a percentage of population, will fall. Older
men commit fewer crimes. They get married, and marriage re-
duces crime. Gilder points out that about 3 percent of criminals
are women; only 33 percent are married men. “Although single
men number 13 percent of the population over age fourteen, they
comprise 60 percent of the criminals and commit 90 percent of
major and violent crimes.”* In short, there is little evidence that
tinkering with the criminal-investigation system will bring relief
to the victims. The causes of crime are too complex.

By the late 1980’s, major US. cities began to experience a
rapid escalation of violent crime, especially murder, as the drug
culture began to be organized on a highly businesslike basis.
An estimated 50,000°* to 80,000*” youths in the Los Angeles area
now belong to gangs. Homicides per year peaked in Los Angeles
County at 350 in 1980, fell to about 200 in 1982, and then rose
again, beginning in 1984, to about 400.3%

33. Walter Bums, “Justified Anger: Just Retribution,” émprimis, IIL (June 1974),
published by Hillsdale College, Hillsdale, Michigan.

34. George Gilder, Naked Nomads: Unmarried Men in America (New York: Quadrangle/
New York Times Book Co., 1974), p, 20.

35. “Dead Zones,” U.S. Neus & World Report (April 10, 1989).

36. Los Angeles Secks Ultimate Weapon in Gang War,” Wall Street Joureal (March
30, 1988).

37, “Turf Wars,” ibid, (Dec. 29, 1988).

38, Idem.
