INTRODUCTION

Let your light so shine before men, that they may see your good
works, and glorify your Father which is in heaven. Think not that I
am come to destroy the law, or the prophets: I am not come to destroy,
but to fulfil. For verily I say unto you, Till heaven and earth pass,
one jot or one tittle shall in no wise pass from the law, till all be
Sulfilled. Whosoever therefore shall break one of these least command-
ments, and shall teach men so, he shall be called the least in the
kingdom of heaven: but whosoever shall do and teach them, the same
shall be called great in the kingdom of heaven (Matt. 5:16-19).

The first sentence of this passage from the Sermon on the
Mount is important: “Let your light so shine before men, that
they may see your good!works, and glorify your Father which is
in heaven.” This is Jesus’ vision of the city on a hill: “Ye are the
light of the world. A city that is set on an hill cannot be hid.
Neither do men light a candle, and put it under a bushel, but on
a candlestick, and it giveth light unto all that are in the housc”
(Matt. 5:14-15), It is Jesus’ confirmation for His people of Israel’s
original requirement:

Behold, I have taught you statutes and judgments, even as the Lorp
my God commanded me, that ye should do so in the land whither ye
go to possess it. Keep thereforc and do them; for this.is your wisdom
and your understanding in the sight of the nations, which shall hear al!
these statutes, and say, Surely this great nation is a wise and under-
standing people. For what nation is there so great, who hath God so
nigh unto them, as the Loxp our God is in all things that we call upon
him for? And what nation is there so great, that hath statutes and
judgments so righteous as all this law, which I set before you this day?
(Deut. 4:5-8).
