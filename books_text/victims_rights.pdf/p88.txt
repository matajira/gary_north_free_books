76. VICTIM'S RIGHTS

of his acts. He generally lives for the moment. His long-term fatc
is total destruction on the day of judgment. He discounts this,
refusing to act in terms of this knowledge. That day scems too far
away chronologically, and God is not visible. “Perhaps God is not
going to enforce the promised penalty. Maybe God doesn’t even
exist,” the criminal thinks to himself. Therefore, God sets the civil
government’s penalty so high that even a present-oriented crimi-
nal will feel the restraining pressure of extreme risk, even if his
psychological rate of discount is very high. The severity of the
earthly punishment testifies to the severity of the eternal punish-
ment. lt serves as an “earnest” or down-payment on ctcrnity.

The Bible teaches us that history is Imear. History has a
beginning and an end. ‘Lhe Bible also teaches us that our thougbts,
as well as our deeds, have consequences in history and also in
eternity beyond the grave (Matt. 5:28). It tells men to redeem
(buy back) their time (Eph. 5:16), to work while there is still light
(John 9:4), If God-fearing people must be educated and motivated
for them to believe such doctrines, then we have to come to grips
with the reality of a world in which membcrs of a criminal class
reject all these doctrines. More than this: members of a profes-
sional criminal class sclf consciously live in terms of @ rizal set of
altitudes toward time, personal responsibility, and the consequences
of human action.

The possibility of the death penalty for kidnapping forces the
potential kidnapper to count the cost of his transgression. Remem-
ber, a person’s perception of total cost (including risk) is affected directly
by his perception of time. If men discount the future greatly, as Esau
did with respect to his birthright, then they will accept low cash
bids for future income.” Prescnt-oriented men discount future
benefits and future curses alike; the distant future is of very little
concern to them. As Harvard political scientist Edward Banfield
comments: “At the present-oriented end of the scale, the lower-
class individual lives from moment to moment. If he has any

 

 

Professor Banfield on Time Horizon: What Has He Taught Us About Crime?” in
Randy E. Barnett and John Hegel 111 (eds.), Assessing the Criminal: Restitution, Retribu-
tion, and the Lagal Process (Cambridge, Massachusetts: Ballinger, 1977).

22. North, Dominion Covenant: Genesis, pp. 126-28, 182-83.
