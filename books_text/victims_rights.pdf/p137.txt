The Ransom for an Eye 125

ance policies often specify that the coverage is for civil damages
rather than criminal acts. ‘This is crue of most autcémobile insur-
ance policies. Policies specify. exactly what is: to be covered — the
famous insurance industry principle of “the large print giveth, but
the fine print taketh away.”

Policies actually designed by criminals to co-insure would be
extremely unlikely. Violent criminals seldom think ahead. They
do not work well with others. They are essentially anti-social
people. A system of insurance company-subsidized crime could
not last very long without government financial aid.

 

The Auction for Human Flesh

By allowing the substitution of an economic payment for ac-
tual physical disfigurement, the judges unquestionably do author-
ize an auction for human flesh, If a convicted criminal is allowed
to pay the victim in order to avoid physical mutilation, he is
participating in an auction. Such an implicit auction may sound
crass, but so does poking out an innocent person’s eye. So does
all criminal behavior. Covenant-breaking men may not like to
think of criminal behavior in such terms, but.this is what the Bible

 

teaches. Sin is the evil, not economic restitution.

We begin our economic analysis of this auction process with
a consideration of the victim. Let us assume that hc has lost his
eye. He tells the judges that he wants to see the other man’s eye
poked out, just as his was. He offers the criminal no choice
between mutilation and restitution. Because the victim initially
offers no alternative sanction, the criminal is then allowed to
make a single counter-offer, if he wants to. Assume that he makes
this counter-offer: 100 ounces of gold instead of losing his eye.!®
Perhaps he is a skilled crafisman who needs both cycs. Perhaps

  

16. As we shall sce, this connter-offer is allowed because the victim did not affer
the criminal a choice between mutilation and economic restitution. Lf the victim
specifies a choice between mutilation and a money payment, he is not entitled to
accept less money, since this would indicate that he had riot been honest when he
specified the initial conditions.’ On the other hand, if the criminal should propose a
uon-monetary payment, the vielim would be entitled to consider it, since this would
constitute a different kind of offer from that specified by the victim. See subsection
betow, “Limiting One's Original Demands,”
