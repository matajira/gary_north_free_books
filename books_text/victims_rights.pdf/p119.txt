Legitimate Violence 107

if the innocent person could prove perjury on the part of his
accuser could he demand that the civil government impose on the
latter the penalty that would have been imposed on him.)”3

Not every Bible commentator has seen the “cye for eye”
sanction as primitive. Shalom Paul writes: “Rather than being a
primitive residuum, it restricts retaliation to the person of the
ollender, while at the same time limiting it to the exact measyre
of the injury — thereby according equal justice to all W. F
Albright, the archeologist who specialized in Hebrew and
Palestinian studics, wrote: “This principle may seem and is often
said to be extraordinarily primitive. Bur it is actually not in the
least primitive: Whereas the beginnings of fex talionis are found
before Isracl, the principle was now extended by analogy until it
dominated all punishment of injuries or homicides. In ordinary
Ancient Oricntal jurisprudence, men who belonged to the higher
social categories or who were wealthy simply paid fines, otherwise
escaping punishment. . . . So the lex talionis (is) . . . the princi-
ple of equal justice for all!”?> Albright understood some of the
implications of the passage for the principle of equal justice for
all, meaning equality before the law. Nevertheless, the myth of
“primitive” legislation still clings in people’s minds.2 Tt seems to
some Christians to be a needlcssly bloody law. In a reaction
against the rigor of this judicial principle, liberal scholar Hans
Jochen Boecker goes so far as to argue that Old Testament law
was not actually governed by lex talionts,?’ that it only appcars in

 

with (falsc) testimony concerning grain or money, he shall bear the penalty of that
case.” CH, paragraphs 3-4; Ancient Near Eastern Texts, p. 166.

23. A moral judicial. system would impose on the accuser or his insurance com-
pany all court costs, plus the costs incurred by the defendant in defending himself.

24, Shalom Paul, Studies in the Book of the Covenant in the Tight of Cuneiform and
Biblical Law (Leiden: E. J. Brill, 1970), p. 40

25. WE. Albright, History, Archaeology, and Christian Humanism (New York, 1964),
p. 74; cited in ibid, p. 77,

26, Hammurabi’s “code” has similar rules: “If a, seignior has destroyed the eye
of a member of the aristocracy, they shall destroy his eye, [Phe has broken a(nother)
seignior’s bone, they shall break his bone.” GH, paragraphs 196-97. If an aristocrat
has destroyed the eye of a commoner, however, the lex falionis did not apply: he paid
one mina of silver (CH 198). Ancient Near Eastem Texts, p. 175.

27. Hans Jochen Buecker, Law and the Administration of justice in the Old Testament

 
