124 VICTIM’S RIGHTS

at large cannot complain that the judges are playing favorites.
The judges are not “respecting persons.” If a rich man loses
money, while the victim has lost the use of his body, this result
has been the decision of the victim, not the judges. What is
essentially a private dispute, victim vs. criminal, rather than a
conflict between classes, has been settled by the disputants. The
victim has made his choice. Outsiders therefore have no valid
moral complaint against the judicial system. ‘his keeps the ideol-
ogy of class conflict from spreading to the general population.
This is a very important feature of the justice system in an era of
class conflict, meaning an era of rhetoric by competing elites in
the name of various classes.

Insurance for Criminals?

Should the victim be denied the option of specifying the form
of vengeance? Does it thwart justice to set up a judicial system
where a rich criminal can offer to “buy his way out??!> Worsc,
what if his rich insurance company can offer to buy his way out?

Tf criminals could escape the likelihood of physical violence
by means of monetary restitution, they might start buying insur-
ance contracts that would enable them to escape the economic.
penalty of inflicting physical violence. ‘This could be regarded as
licensing criminal behavior. No onc is going to co-insure another
man’s eye with his own eye, but the public has already sct up
co-insurance for monetary claims. Thus, by allowing economic
restitution for crimes of violence, criminal behavior might be made
less costly to the criminals.

 

Onc answer to this objection is that insurance companics are
unlikely to insure a person from claims made by victims if the
man is a repeat violator, The risk of writing such contracts-is too
high. Private insurance contracts are designed to be sold to the
general public, and to keep premiums sufficiently price competi-
tive, sellers exclude people known to be high risks. Low-risk buy-
ers do not want to pay for high-risk buyers. Furthermore, insur-

 

15, If the criminal could “buy his way out” by bribing the judges, then justice
would be thwarted. But judges in a biblical system represent the victims, not the
State. If they represent a victim who wishes to be “bought off,” where is the injustice?
