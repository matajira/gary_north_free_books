80 VICTIM'S RIGHTS

private coercive activity of kidnapping Africans thousands of miles
away.

This policy worked only because 1) the British navy enforced
its regulations against the slave traders, 2) a majority of citizens
in the recipient nations were steadily educated to reject the idea
of the legitimacy of involuntary servitude, and 3) slavery’s defend-
ers were defeated on the battlefield, in the case of the American
South in the 1860’s. The economic lesson: disregarding the needs
and preferences of slave-holders (the final users) by outlawing
slavery led to the reduction of the entire slave trade. The profit-
ability of the international slave trade was reduced. We learn that
there are cases where State coercion is valid, when that coercion
is directed against private coercers. The anti-slave trade legisla-
tion recognized the complicity of slave-owners (final users) in the
coercive international slave trade. The market for slaves was not
a free market, for the supply side of the equation was based on
coercion.

Monopoly Returns and Reduced Crime

There is a curious myth that laws against evil acts do not
reduce the total number of these acts that criminals commit.
Some critics even go so far as to argue that the very presence of
the law subsidizes evil, in the case of laws against the sale of illegal
drugs or laws against prostitution. Somehow, passing a law makes
the prohibited market more profitable, and therefore the law leads
to greater output of the prohibited substances or services. This is
a very odd argument when it comes from people who defend the
efficiency and productivity of laissez-faire’ economics.

A fundamental principle of economics is this: the division of
labor is limited by the extent of the market. This was articulated

 

 

slave-breeding centers, especially in the Virginia tidewater region, where soil-eroding
agricultural techniques had reduced the land’s output, and therefore had reduced
the regiona) market value of the human tools who produced the output. This region
began to export slaves to buyers who cultivated che fresher soils of Louisiana and
Mississippi. See Alfted H, Conrad and John R, Meyer, “The Economics of Slavery
in the Ante-Bellum South,” journal of Political Economy, LXVI (April 1958); reprinted
in Robert W. Fogel and Stanley L. Engerman (eds,), The Reinterpretation of American
Exonomic History (New York: Harper & Row, 1971), ch. 25.

 

 
