The Ransom for a Life 143

God (Gen. 9:2). A. beast must somehow suppress this fcar—an
internal warning from God —in order to kill a man. Beasts arc
responsible creatures; they are to be hunted down and killed for
this form of rebellion. Some domesticated beasts are responsible
outward to other. beasts, upward to man, and, through their
masters, upward to God.8

The Bible deals with the liability problem by making owners
personally responsible for the actions of their animals. If their
animals cause no problems, there will be no penalties. The more
dangerous the animals, the more risky the ownership. Clearly,
Exodus 21:30 is a case-law application of a general principle
regarding the responsibilities of ownership. The principle can be
extended to ownership of other animals besides oxen, and also to
related instances of personal financial liability for damages in
cases not involving animals,

The law makes it clear that the owner may. not profit in any
way from the evil act of the beast. He is not permitted to salvage
anything of value. The beast is stoned — the same death penalty
that a guilty human would receive—and the owner does not
receive the carcass. Its flesh may not be eaten (v. 28). The beast
is treated as if it werc a human being. Its evil act brings death — not
the normal killing of oxen, which allows owners to eat the flesh
or sell it to those who will, but the death of the guilty. The guilty
beast is no longer part of the dominion covenant. It can no longer
serve the economic purposes of men, except as an example. It has
to be cut off in the midst of time, just as a murderer is to be cut
off in the midst of time.

Why Stoning?

J. J. Finkelstein discusses at considerable length the question
of the stoning of the ox. While similar laws regarding the goring
ox are found in many ancient Near Eastern law codes, the He-
brew law is unique: it specifically requires stoning of the ox that

8, The incomparable biblical example of upward responsibility of an animal
toward man is Balaam’s ass. “And the ass said unto Balaam, Am not | thine ass,
upon which thou hast ridden ever since I was thine unto this day? Was I ever wont
to do so unto thee? And he said, Nay” (Num, 22:30),
