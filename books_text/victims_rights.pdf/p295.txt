Conclusion 283

A second civil goal of biblical restitution is to make possible
the full judicial restoration of the criminal to society after he has
paid the victim what he owes him. The State is not to concern
itself with the psychological restoration of the criminal, the victim,
or society in general. The State’s jurisdiction is strictly limited to
the realm of the judicial: restitution, The psychological state of the
criminal is between himself and God, as is the psychological state
of the victim. Nevertheless, as in the case of the salvation of any
individual by God’s grace, judicial restoration is the first step
toward psychological restoration.

The modern U.S. practice of never again allowing convicted
felons to vote is clearly immoral. Under biblical law, a convicted
criminal becomes a former convicted criminal when he has made
full restitution to his victims. In this sense, he is “resurrected”
judicially. After he has paid his debt to his victims, he must be
restored to full political participation. To segregate the former
convicted criminal from any area of civic authority or participa-
tion is to deny judicially that full civil restoration is made possible
by means of God’s civil law.

The third civil goal of biblical restitution is not intuitively
obvious, but it may be the most important goal for the modern
world. A system of biblical restitution is required in order to
reduce the likelihood that citizens will come to view the civil
government as an agency that lawfully initiates programs leading
to personal or social transformation. The State’s task is to assess
the economic damage that was inflicted on the victim and then
impose judgment on the convicted criminal that will reimburse
the victim for his loss, plus a penalty payment. Normally, this
means double restitution, The State is not an agency of creative transfor-
mation, It is not to be regarded as a savior State. Men should not seek to
make the State an agency of social salvation. It is supposed to enforce
biblical civil law -no more, no less. The State is not supposed
to seek to make men righteous; its God-assigned task is to restrain
certain specified acts of public evil. Theft is one of these acts.

Civil government is an agency of visible judgment in history.
Justice demands judgment. The judgments handed down by civil
government acknowledge the historic judgments of God, as well
