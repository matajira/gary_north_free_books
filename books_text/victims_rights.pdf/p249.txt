Guardian of the Oath 237

God nevertheless wants criminals brought to justice in his-
tory. The Bible places the responsibility of pursuing justice on the
individual who is most likely to want to see the criminal brought
to justice: the victim. Because the crime was ultimately against
God and His mandated social order, the victim becomes God’s
primary representative agent in pursuing justice. The victim is
also uniquely motivated to begin this search for incriminating
evidence, since he is the loser, and he will receive a restitution
payment upon confession by, or conviction of, the criminal. As I
have argued elscwhere, if he refuses to pursue the criminal or
bring charges against him, the civil court is not to intrude on the
case, unless he is a minor or legally incompetent.’ Thus, when
he begins his investigation of the crime, he is serving as God’s
primary covenantal agent. He is officially gathering information
to be used in a covenant lawsuit against the criminal. He is acting
as an agent of two courts: God’s heavenly court and His earthly
civil court.

In a sense this does not do full justice to the victim’s unique
legal position. The civil court’is to some degree the agent of the
victim, since the victim, in his legal capacity as a victim, is a
representative of God. The victim alone determines whether or
not to prosecute the covenant lawsuit; the court is to support his
decision.. If he brings a covenant lawsuit in his own name, he
inevitably also brings it in God’s name, for God was the primary
victim. The civil court is to examine the evidence and announce
judgment, but this judgment is made in the name of the two
victims: God and the earthly victim. The civil court is an agent
of the victim in a way that the ecclesiastical court is not. The civil
court acts to defend the victim’s rights, whereas the priest acts to
defend the civil court’s authority.

In a court, there must be interrogation of the suspects. God
in the garden publicly interrogated Adam and Eve regarding the
facts of the case. It is a crime to testify falsely in God's court or
in man’s. False testimony is intended to deflect God’s justice.
Offering it implics that God can be deceived, or at the very least,
deterred from bringing negative sanctions in history. It rests on a

7. North, Tools of Dominion, pp. 279, 294-95.
