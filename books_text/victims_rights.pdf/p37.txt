The Covenant Lawsuit 25

lied about who is truly sovereign over tbe universe. He would
have given false testimony against the true god, man. God would
have been guilty of calling man to worship a false god, which is
a capital offense (Deut. 13:6-9). He would also have been guilty
of false prophesying, another capita! offense (Deut. 13:1-5). Adam
and Eve had sought to indict God for a capital offense; they were
subsequently executed by Gad. So are ali their heirs who persist
in refusing to renounce the judicial accusations of their parents,
who represented them in God’s court.

In His grace, God offered them a judicial covering, a tempo-
rary stay of execution, which was symbolized by the animal skins
(Gen. 3:21). This symbolic covering required the slaying of an
animal. God offered them time on earth to repent. He offered
them a way to make restitution to Him: the blood sacrifice of
specified animals, He did this because He looked forward in time
to the death of His Son on the cross, the only possible restitution
payment large enough to cover the sin of Adam and his heirs.

His Son’s representative death is the basis of all of God’s gifts
to mankind in history. Grace is an unearned gift, meaning a gift
earned by Christ at Calvary and given by God to all men in
history. Christ's restitution payment.serves as the basis of common
grace to covenant-breakers in history and special grace to covenant-
keepers in history and eternity.!? The words of Christ on the cross
are the basis of common. grace in history: “Then said Jesus,
Father, forgive them; for they know not what they do” (Luke
23:34). Ignorance of the law is no excuse, but Jesus Christ grants
grace to the ignorant anyway. He paid God’s price; He suffered
God’s sanctions; so He has the right to grant temporal (common)
forgiveness on no terms at all, and eternal (special) forgiveness
on His own terms.

Criminal and Victim as
Covenantal Representatives

Adam and Eve served as Satan’s representatives when they
had communion with him, thereby bringing a covenant lawsuit

13. Gary North, Dominion and Common Grace: The Biblical Basis of Progress (Tyler,
‘Texas: Institute for Christian Economics, 1987).
