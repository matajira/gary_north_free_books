248 VICTIM’S RIGHTS

State of private property rights. They cannot estimate perfectly, for
they cannot know the psychic costs and benefits involved in the
minds of the conflicting parties. But they can make general, “un-
scientific” estimations, given the image of God in all men, and
given the created environment in which all men live. This is an
important application of biblical revelation to economics: if there
is no universal humanity — no universal human nature — and no
Creator who serves as the basis for man’s image, and no creation
governed by the Creator in terms of His value and His laws, then
it is impossible for the judges legitimately to have confidence in
their estimation of social costs, social benefits, private costs, and
private benefits. Without our knowledge of objective economic
value provided by God’s plan and His image in man, objective
economic value becomes epistemologically impossible. Judges
would then be blind in a sea of exclusively subjective economic
values, a world in which it is philosophically impossible for men
to make interpersonal comparisons of subjective utility.’

 

The Principle of the Fire Code

In the case of a single violator or a few potential violators,
there are two reasons justifying the coercive intervention of the
civil government. First, to use the biblical cxample of fire, a man
who permits a fire to get out of control may see an entire town
burned to the ground. There is no way, economically, that he can
make full restitution. In fact, it would be almost impossibly expen-
sive to estimate the value of the destroyed physical property, let
alone the loss of life, or the psychological anguish of the victims.
Therefore, in high-risk situations, the civil government can legiti-
mately establish minimum fire prevention standards. (Analogously,
the civil government can also legitimately establish medical quar-
antines to protect public health: Lev. 13, 14.)

Carl Bridenbaugh, in his study of urban life in seventeenth-
and carly eightcenth-century colonial America, discusses this prob-

6 North, Dominion Covenant: Genesis, ch. 4: “Economic Value: Objective and
Subjective.”

7. Gary North, Tools of Dominion: The Case Laws of Exodus (Tyler, Texas: Institute
for Christian Feanomics, 1990), Appendix D: “Ihe Epistemological Problem of
Social Cost.”
