288 VICTIM’S RIGHTS

other form of profit-secking, risk-avoiding behavior.!? One scholar
even argucs that on the whole, over the last seven centuries,
homicides as a proportion of total population have declined by a
factor of 10 in Britain.'+ But the American public is aware of the
fact of violent crime, whatever the causes.!> The March 23, 1981,
issues of both Time and Newsweek, the two most widely read U.S.
news magazines, ran articles on violent crime: “The Plague of
Violent Crime” (Newsweek) and “The Curse of Violent Crime”
(Time), (We might also consider conducting a research project
on “spying and petty theft in the news magazine publishing indus-
try.”)

In the United States between the periods 1930-34 and 1975-
79, population grew by 84 percent, 123 million to 226 million.
Homicides went up by almost 600 percent, from 14,618 to 101,044,
Homicides per 100,000 population climbed from 11.9 to 44.7.
Interestingly, the number of civil executions per homicide dropped
by over 99 percent, from one per 18.8 to one per 33,681. The
growth in homicides was relatively low from the 1935-39 era until
1945-49. But the curious fact is that homicides per 100,000 of
population dropped from 1946 until 1962, from 6.9 murders per
100,000 to 4.5. By 1972, it had climbed to 9.4.'* Homicides went
from 44,000 in 1960-64 to 101,000 in the 1975-79 period.!? In Los
Angeles, the increases were comparable: population increase was

13. Between 1968 and 1979, about 250 articles on crime by economists appeared;
before that, there had been only a handful. D. J. Pyle, The Economics of Crime and Law
Enforcement: A Selected Bibliography (New York: Rand Institute, 1979). Most coono-
mists believe that the key essay that launched the field was Gary Becker's “Grime
and Punishment: an Economic Approach,” Journal af Political Economy, LXXVT (1968),
pp. 169-217; reprinted inv Gary $. Becker and William M, Landes (eds.), Essays in the
Economics of Grime and Punishment (New Vork: National Bureau of Economic Research,
1974), ch. 1.

14. Ted Robert Gurr, Grime and Justice: An Anmual Review of Research, Vol. UI.

1. On crime rates, sce Donald J. Mulvihill and Melvin M, Tumin (eds.), Crimes
of Violence, Vol. 11 of the staff report to the National Commission of the Causes and
Prevention of Violence (Washington, D.C.: Government Printing Office, 1959), p. 54.

16. James Q. Wilson, Thinking About Crime (New York: Basie Books, 1975), pp.
5-6.

17. Statistics compiled by the staif of California State Senator H. L, Richardson,
based on the Federal Bureau of Investigation’s Uniform Crime Reports and the US.
Department of Justice’s Sourcebook of Criminal Justice Statistics, 1979.
