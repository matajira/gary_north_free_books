The Ransom for a Life 149

dence, biblical law resirains the diserctionary authority of the
State’s representative in the more scrious cases of negligence. This
restrains the State.

Herc is the viewpoint of the modern humanistic State: the
State as an agency that possesses the judicial authority and obli-
gation to search men’s hearts, and to render formal judgment in
terms of its findings. ‘This view of State power asserts that the
State possesses an ability that only God possesses: the ability to
know man’s heart. ‘The prophet Jeremiah asked rhetorically: “The
heart is deceitful above all things, and desperately wicked: who
can know it?” (Jer. 17:9). His answer was clear: “I the Lorp
search the heart, I try the reins, even to give every man according
to his ways, and according to the fruit of his doings” (Jer. 17:10).
The human judge can make causal connections based on public
evidence, but he cannot scarch the defendant’s heart. Any asser-
tion to the contrary necessarily involves an attempt to divinize
man, and in all likelihood, divinize man’s major judicial represen-
tative, the State.

The Goring of a Slave or a Child

“Tf the ox shall push [gore] a’ manservant or a maidservant;
he shall give unto their master thirty shekels of silver, and the ox
shall be.stoned” (Ex. 21:32). Normally, the death penalty could
be imposed on the owner of the ox. In this case, however, the
penalty was tixed by law: 30 shckels of silver.

Thc wording here is peculiar. To “push” means, in this in-
stance, to kill. In verse 29, “push” did not mean to kill, “But if
the ox were wont to push with his horn in time past, and it hath
been testified to his owner, and he hath not kept him in. . . .”
Had “to push” meant “to kill,” the ox would have been executed
upon conviction. An ox that killed someone was stoned to death
(v. 28). Thus, “push” in verse 29 had to’ mean something other
than killing. But with respect to servants, the word “push” or
“gore” is used in the sense of “gore to death.” This is why the ox
is executed: a human being has died.

Why the comparatively small penalty??! Why is the death of

21. Thirty pieces of silver were a lot of money in terms of what they could buy,
but not compared to what the victim’s heirs could normally impose.
