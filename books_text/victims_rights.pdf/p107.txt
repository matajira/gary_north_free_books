The Costs of Private Conflict 95,

numerous local Klan-type groups exist, but they have little influ-
ence.!® But the Klan’s former power testifies to the fact that when
civil courts fail to dispense justice and therefore lose thcir legiti-
macy in the eyes of large numbers of citizens, societies will cventu-
ally see the rise of private dispensers of “people’s justicc.”
Without a sense of legitimacy, the authority of public courts
is threatened. The courts need legitimacy in order to gain the
long-term voluntary cooperation of the public, meaning self
government under law, without which law enforcement becomes
both sporadic and tyrannical, No legal system can afford the
economic resources that would be necessary to gain full compli-
ance to an alien law-order in a society whose members are unwill-
ing to govern themselves voluntarily in terms of that law-order.”
If the courts do not receive assent from the public as legitimate
institutions, they can maintain the peace only by imposing sen-
tences whose severity gocs bcyond people's sense of justice, which
again calls into doubt both legitimacy and legal predictability.

Judicial Pluralism and Social Disintegration

A civil government that refuses to defend a law-order that is
seen as legitimate by the public is inviting the revival of the duel,
the feud, and blood vengeance. If the public cannot agrce on
standards of decency, then the courts will be tempted to become
autonomous. Widespread and deep differences concerning religion
lead to equally strong disagreements over morality and law. Relig-
jous pluralism leads to moral and judicial pluralism, meaning
unpredictable courts. Religious pluralism is an outgrowth of poly- °
theism, Polytheism inescapably leads to what we might call “poly-
legalism.” Too many law courts decide in terms of conflicting
moralities. Only the strong hand. of centralized and bureaucratic

 

was overturned by the U.S. Supreme Court in 1925 in a landmark case, Pierce v.
Society of Sisters, which has remained the key Court decision in the fight for Christian
schools.

19. As one southemer described the Kian: “It is made up mainly of gasoline
station attendants and KBI informers. The members can casily spot the informers:
they are the only ones who pay their monthly dues.”

20, Gary North, Moses and Pharaoh: Dominion Religion vs. Power Religion (Lylet,
Texas: Institute for Christian Economics, 1985), pp. 291-94.
