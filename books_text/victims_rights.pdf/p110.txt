98 VICTIM’S RIGHTS

spread alongside the gospel of salvation. The unpredictable vic-
lence of State power was thereby reduced. In private. relation-
ships, men were not allowed to vent their wrath on cach other in
acts of violence. Those who violated this law became economically
liable for their actions,

The duc! or brawl is by nature a direct challenge to the
authorily and legitimacy of the civil government. It transfers to
individuals operating outside the State — the God-ordained mo-
nopoly of violence —a degree of legal immunity from civil judg-
ment. It transfers sovereignty in the administration of violence
from the State to the individual. It is not surprising, therefore,
that onc program of legal reform recommended by some contem-
porary libertarian anarchists is the legalization. of dueling, The
duel is scen as a private act between consenting adults and there-
fore sacrosanct. (Sacrosanct: from sacre = sacred rite, and sanctum
= holy and inviolable. Also related to sanction = legal and sover-
eign authority, or a judgment by a legal and sovereign authority.)
