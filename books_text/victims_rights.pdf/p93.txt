Kidnapping 81

by Adam Smith in Chapter 3 of Wealth of Nations (1776). Another
basic principle is this one: the greater the division of labor, the
greater the output per unit of resource input — in short, the greater
the efficiency of the market. When the market increases in size, it
makes possible an increase in cost-effective production. Advertis-
ing and mass-production techniques lower the cost of production
and therefore increase the total quantity of goods and services
demanded. This is well understood by all economists.

Nevertheless, there are some people who still belicve that laws
against so-called “victimless crirnes”— sins that they do not re-
gard as major transgressions, I suspect — actually increase the
profitability of crime. On the contrary, such laws increase the risk
of the prohibited activitics, both to scllers and consumers. Prices
rise; the market shrinks; per umit costs rise; efficiency drops. What
such laws do is create monopoly returns for a few criminals. But
the critics of such laws conveniently forget that monopoly returns are
always the product of reduced output. This, in fact, 1s the conventional
definition of a monopoly. ‘Thus, civil laws do reduce the extent of
the specified criminal bchavior.* They confinc such behavior to
certain criminal subclasses within the society. Biblically speaking,
such laws place boundaries around such behavior.

There is no doubt that nineteenth-century laws against the
slave trade drastically reduced the profitability of the international
slave trade. These laws increased the risks for slavers, reduced
their profits, and narrowed their markets. The result was a drop
in output (slavery) per unit of resource input.

Household Evangelism

Apart from the one exception provided by the jubilee law, the
Old ‘Testament recognized the legitimacy of involuntary slavery
of foreigners only when the slaves were female captives taken after
a battle (Deut. 20:10-11, 14). To fight a war for the purpose of
taking slaves would have becn illegitimate, for this was (and is)
the foreign policy of empires. It is truc that the jubilee law did

33. Cf. James M. Buchanan, “A Defense of Organized Cirime?” in Ralph Andreano
and John J. Siegfried (eds.), The Esonomics of Crime (New York: Wiley, 1980), pp.
395-409.
