240 VICTIM’S RIGHTS

in vain in a formal judicial conflict must then seek legal covering
by the church. The reason why the oath is guarded by the church
is that the church alone can lawfully invoke the eternal negative
sanctions of God against an individual.!° Thus, by invoking the
oath in court, the criminal necessarily brings himself under the
authority of the church.

The modern practice of allowing atheists to affirm to tell the
truth in court, but not to swear on the Bible or in God’s name, is
a dircct affront against God and against the church as the guard-
jan of the oath. It is also inevitably an act of divinizing the State
by default. The State becomes the sole enforcer of the affirmation.
In such a worldview, there is no appeal beyond the State and its
sanctions. The atheist’s affirmation is therefore a judicial act de-
manding the removal of God from the courtroom. Thus, it re-
quires the creation of a new oath system, with the State as the
guardian of the oath. The State acts not in God’s name but in its
own, Rushdoony’s comments arc on target: “If a witness is asked
to swear to tell the whole truth and nothing but the truth without
any reference to God, truth can be and is commonly redefined in
terms of himself. The oath in God’s name is the ‘legal recognition
of God’! as the source of all things and the only ground of true
being. It establishes the state under God and under His law. The
removal of God from oaths, and the light and dishonest use of
oaths, is a declaration of independence from Him, and it is war-
fare against God in the name of new gods, apostate man and his
totalitarian state.”!?

Conclusion

The biblical State can lawfully impose negative sanctions
against a perjurer, but only on behalf of the victim. The State
cannot lawfully pronounce the eternal negative sanctions of the
oath against anyone. The State can lawfully require an oath, but

10, Gary North, The Sinai Strategy: Economics and the ‘Ten Commandments (Tyler,
Texas: Institute for Christian Economics, 1986), pp. 52-56.

11. T. Robert Ingeam, The World Under God’s Law (Houston, Texas: St. Thomas
Press, 1962), p. 46.

12, Rashdoony, Jnstitules of Biblical Law, p. 115.
