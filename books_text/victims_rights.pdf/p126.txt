114 VICTIM'S RIGHTS

had suffered the same punishment. He had removed their ana-
tomical “tools of dominion”; now he had his removed.?

Problems of Interpretation

This incident raises some difficult exegetical questions. Was
the “eye for eye” principle literally applied in ancient Israel after
the defeat of Canaan? Did Isracl’s courts really poke out people’s
teeth and cycs? If not, why not? Or is it merely that there are no
clear-cut biblical records of such physical penalties being imposed
by Israelite judges on Tsraelite citizens?

The incident also raises some difficult historical questions. In
the Christian West, judges have consistently refused 10 impose “eye
for cyc” physical penalties. In non-Christian societies, permanent
physical vengeance is quite common, e.g., Islam’s Shari’a law. Why
not in.the West? What is it about inflicting permanent physical
routilation — in contrast to whippings or other relatively imperma-
nent forms of physical violence ~ that so repels We:

 

 

mers?

 

The West’s Kuture-Orientation

The West’s impulse toward dominion in history is onc possi-
ble answer. ‘The West has been futurc-oriented, as a dircct result
of its Christian cschatological heritage: a faith in linear history, with
a God-creatcd beginning, a God-sustaining providence, and a
God-governed final judgment? This vision of linear time made
possible the development of modern science.t The future-orienta-
tion of the West, especially from the seventeenth century onward,

 

graphs 196-201. Ancient Near Eastern Taxts Relating to the Old Testament, edited by James
B. Pritchard (3rd ed.; Princeton, New Jersey: Princeton University Press, 1969), p.
175.

2, Without a thunb, a person cannot grasp a tool or weapon. Without a big
toe, he cannot balance himself easily. See James B. Jordan, Judges: God's War Against
Humanism (Tyler, Texas: Geneva Ministries, 1985), pp. 4-5.

3. Karl Léwith, Meaning in History University of Ch
“The Biblical View of History.”

4. Stanley Jaki, The Ruad of Seience and the Ways ta God (University of Chicago
Press, 1978), chaps. 1, 2; Sciece and Creation: From elemal cycles to an oscillating universe
(Edinburgh and London: Scottish Academic Press, [1974] 1980); “Ibe History of
Science and the Idea of an OscillaGng Universe,” in Wolfgang Yourgrau and Allen
D. Beck (eds.), Cosmology, History, and Theology (New York: Plenum Press, 1977)

 

  

cago Press, 1949), ch. 11:

  

 
