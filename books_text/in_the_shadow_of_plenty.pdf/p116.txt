100 In the Shadow of Plenty

history (Job 42:1-2). He showed a thorough understanding of the
doctrine of divine providence (Proverbs 21:1). He obviously com-
prehended the multigenerational nature of the covenant bond
(Lamentations 5:19). He displayed a keen awareness of the power
of prayer (2 Chronicles 7:13-14). But more than anything else, his
response gives testimony to his utter dependence upon God, and
his confidence in Biélical problem solving (Psalm 34:17-18). He
wanted to do things God's way, in God’s time, with God's help, in
accord with God’s will.

So, at every turn, Nehemiah prayed. When he appeared be-
fore the king to make petition to rebuild the walls of Jerusalem, he
prayed (Nehemiah 2:4), When he entered into the ruined city to
begin the task, he prayed (Nehemiah 2:12). When threats of vio-
lence and conspiracy jeopardized the fledgling reconstruction
projéct, he prayed (Nehemiah 4:2). When there were crises
among the people that required his judicious hand, he prayed
(Nehemiah 5:19). When an attempt on his life threatened the en-
tire project, he didn’t panic—he prayed (Nehemiah 6:9)..When
his own brethren turned against him, he prayed (Nehemiah 6:14).
And when he completed the work, all that he had set his hand to
do—that’s right; you guessed it—he prayed (Nehemiah 13:31).

Of course, praying wasn’t aii that he did. It was simply the
foundation of all that he did. He planned (Nehemiah 2:5-6). He
laid groundwork (Nehemiah 2:7-8), He enlisted help (Nehemiah
2:9). He encouraged (Nehemiah 2:17-18). He motivated (Nehe-
miah 4:14-20). He organized (Nehemiah 3:1-32). He anticipated
difficulty and made provision for it (Nehemiah 2:19-20, 6:1-44).
He improvised (Nehemiah 4:21-23). He. worked (Nehemiah
4:23). He sacrificed (Nehemiah 5:14-19). He led (Nehemiah
13:4-30). And he governed (Nehemiah 7:i-7). But undergirding
all these activities was his constant reliance upon Almighty God.
Undergirding them all was prayer.

Nehentiah knew that it was pointless to attempt anything
apart from God’s blessing and purpose,

“Unless the Lord builds the house, They labor in vain who
build if; Unless the Lord guards the city, The watchman stays
