TABLE OF CONTENTS
Editor’s Introduction ....... 06 cccce cece seen tere enee xi

Part I: THE BIBLICAL BLUEPRINT:
10 PRINCIPLES

Author’s Introduction ...... 00.00 c eee eee eeee
. Word and Deed Evangelism .
. The Samaritan Legacy .
. Dominion by Service .............5
. Hi Ho, Hi Ho, It’s Off to Work We Go
. Sheaves For the Provident.........,
. Charity Begins at Home
. Uplifting the Uplifters . .
. The Unbroken Circle ..
. Exceeding What is Written
. The Foundation of Peace ...........+55%

 
  
   
  
 
  
  
 
   
 
   
 
 
  
 
 

SCOnnapone

Part II: THE BIBLICAL BLUEPRINT:
3 STRATEGIES

11. What the Government Should Do .....:..

12, What the Church Should Do ...

13. What Families Should Do...............
For Further Reading. .
Scripture Index . .
Subject Index........
What Are Biblical Blueprints?

 

vii
