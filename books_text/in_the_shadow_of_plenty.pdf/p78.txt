62 In the Shadow of Plenty

The Confidence Factor

When David was fleeing the wrath of King Saul, the symbol of
God’s provision, the Showbread, became for him actual provision.

Then David came to Nob, to Ahimilech the priest. And Ahim-
ilech was afraid when he met David, and said to him, “Why are
you alone, and no one with you?” So David said to Ahimelech the
priest, “The king has ordered me on some business, and said to
me, ‘Do not let anyone know anything about the business on which
I send you, or what I have commanded you.’ And I have directed
my young men to such and such a place. ‘Now therefore, what do
you have on hand? Give me five loaves of bread in my hand, or
whatever can be found.’” And the priest answered David and said,
“There is no common bread on hand; but there is holy bread, if the
young men have at least kept themselves from women,” Then
David answered the priest, and said to him, “Truly, women have
been kept from us about three days since I came out. And the
vessels of the young men are holy, and the bread is in effect com-
mon, even though it was sanctified in the vessel this day.” So the
Priest gave him holy bread; for there was no bread there but the
showbread which had been taken from before the Lord, in order to
put hot bread in its place when it was taken away (1 Samuel 21:1-6).

On the surface, this incident —focusing as it does on the Sab-
bath Showbread—seems to have nothing whatsoever to do with
gleaning, but Scripture ties the two together, making them insep-
arable concepts.

The Showbread was an important part of the worship of God’s
covenant people. It is variously called the “continual bread”
(Numbers 4:7), the “sacred bread” (Hebrews 9:2), the “bread of
ordering” (1 Chronicles 9:32}, and the “bread of presence” (Exodus
35:13). It was an everlasting symbol (Exodus 25:30) of the “ever-
lasting covenant for the sons of Israel” (Leviticus 24:8). It was
meant to remind the people that God is man’s Provider and Sus-
tainer, that God will ever and always feed them from His bounti-
ful table. It was a surety that God dwelt amidst His chosen and
cared for them. The Showbread was a kind of visual sermon par-
alleling Christ’s own Sermon on the Mount:
