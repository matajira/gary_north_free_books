Dominion by Service 39

What should the young king do? Where should he turn?
Whom could he trust?

Then the Lord said to Isaiah, “Go out now to meet Ahaz, you
and Shear-jashub your son, at the end of the aqueduct from the
upper pool, on the highway to the Fuller's field, and say to him:
"Take heed, and be quiet;.do not fear or be fainthearted for these
two stubs of smoking firebrands, for the fierce anger of Rezin and
Syria, and the son of Remaliah, Because Syria, Ephraim, and the
son of Remaliah have taken evil counsel against you, saying, “Let
us go up against Judah and trouble it, and let us make a gap in its
wall for ourselves, and set a king over them, the son of Tabeel”—
thus says the Lord God: “It shall not stand, Nor shall it come to
pass. For the head of Syria is Damascus, And the head of Damas-
cus is Rezin. Within sixty-five years Ephraim will be broken, So
that it will not be a people. The head of Ephraim is Samaria, And
the head of Samaria is Remaliah’s son. If you will not believe,
Surely you shall not be established”’” (Isaiah 7:3-9).

In the midst of the crisis, Isaiah came to the king beseeching
him to put his trust in the Lord, Though his armies be defeated,
though his city be besieged, though his hopes bé dashed, Ahaz
could rely on the faithfulness of the King of kings. He could turn
unto Him. After all, what was Rezin before the awesome glory of
the Almighty God? What was Pekah before the sovereign Maker
of heaven and earth? Ahaz could rest and relax. He could wait in
silent confidence for God to rescue him from his adversaries
(Psalm 7:1), for the Lord would be his rock and his salvation, his
stronghold in times of calamity (Psalm 62:2). The Lord Himself
would be a shield about him, the glory and the lifter of his head
(Psalm 3:3)..

But no, that wasn’t sufficient for faithless Ahaz. He wanted
something “more secure” than the promise of divine intervention,
divine favor. So. . .

At the same time King Ahaz sent to the kings of Assyria to help
him (2 Chronicles 28:16).

God’s way wasn’t good enough for Ahaz. In fact, God himself
wasn’t good enough for Ahaz. He preferred the throne of Nineveh
