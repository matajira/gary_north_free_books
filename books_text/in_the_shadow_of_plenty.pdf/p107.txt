The Unbroken Circle 91

faithfulness, she and al] her loved ones were “grafted in” (Romans
11:17-24), “no longer strangers and foreigners, but fellow citizens
with the saints and members of God’s household” (Ephesians 2:19).

But not only was she grafted into the covenant, she was ulti-
mately grafted into the royal family of Judah (Matthew 1:5), and
the Messianic line (Ruth 4:18-29).

All this, because she was willing to submit to the terms of the cove-
nant, the God of the covenant, and the people of the covenant.

The Sojourner in the Land

‘There are no racial barriers in the Kingdom of God (Galatians
3:28-29). There never have been (Psalm 87:1-7). There never will
be (Isaiah 2:2-4; Micah 4:1-4). The requirements for citizenship
are ethical (Psalm 15:1-5), Anyone might be admitted to the circle
of the covenant if he was willing to submit to its demands, even a
wretched harlot from Jericho.

There were safeguards to be sure, to protect Israel from pagan
pollution, from wolves in sheep’s clothing. So, for instance, there
were ceremonial restrictions (Exodus 12:48-49), marital restric-
tions (Deuteronomy 7:1-6), and restrictions on cohabitation
(Joshua 6:23). But, because the Jews themselves were at one time
sojourners themselves in Egypt (Genesis 15:13; Exodus 22:21;
Deuteronomy 10:19, 23:7), they were to treat the foreigners in
their midst with respect and acceptance.

Whether the sojourner was a part of an entire tribe, such as
the Gibeonites (Joshua 9:1-27), or one of the remnant Canaanite
people, or simply an individual settler, he was to receive full jus-
tice (Exodus 22:21, 23:9; Leviticus 19:33-34). He was to share in
the full inheritance of the Kingdom (Ezekiel 47:22-23). He was to
be loved as'a brother (Deuteronomy 10:19). He was included in
the provision made for cities of refuge (Numbers 35:15; Joshua
20:9), in the charity network (Leviticus 19:10, 23:22; Deuteron-
omy 24:19-21), and had equality under the law (Leviticus 24:22),
He was even ranked with the fatherless and the widow as being
defenseless; and so the Lord Himself was his protection, judging
all his oppressors (Psalm 94:6, 146:9; Jeremiah 7:6, 22:3; Ezekiel
22:7; Zechariah 7:10; Malachi 3:5).
