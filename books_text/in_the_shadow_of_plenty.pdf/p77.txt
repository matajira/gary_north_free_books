Sheaves for the Provident 64

that you were a slave in Egypt, and that the Lord your God
redeemed you from there; therefore I command you to do this
thing, When you reap your harvest in your field, and forget a sheaf
in the field, you shall not go back to get it; it shall be for the
stranger, the fatherless, and the widow, that the Lord your God
may bless you in all the work of your hands. When you beat your
olive tree, you shall not go over the boughs again; it shall be for the
stranger, the fatherless, and the widow. When you gather the
grapes of your vineyard, you shall not glean it afterward; it shall be
for the stranger, the fatherless, and the widow. And you shall re-
member that you were a slave in Egypt; therefore I command you
to do this thing (Deuteronomy 24:17-22).

When you reap the harvest of your land, you shall not wholly
reap the corners of your field when you reap, nor shall you gather
any gleaning from your harvest. You shall leave them for the poor
and for the stranger: I'am the Lord your God (Leviticus 23:22).

God’s opportunity society provided protection and opportunity
for aliens and sojourners (Exodus 23:9), for travelers (Deuteron-
omy 23:24-25), for orphans and. widows (Deuteronomy 24:19),
and for the needy and oppressed (Leviticus 19:9-10): If they were
willing to submit to the terms of God’s covenant, willing to labor,
willing to glean from the edges of the field and the tops of the
trees, then they would be able to make it. They would be able to
transform poverty into productivity.

That is real charity. That is Biblical charity.

The opportunity provided by the gleaning laws might seem to
be an isolated, historical and cultural incidence, hidden away
amidst all the other obscurities of the Mosaic Law Code. Actually
though, gleaning is a prominent feature of the Biblical blueprint for
welfare, spanning over 1500 years of revelation, a millennium and
a half! In fact, it is de prominent feature.

Gleaning is the primary means of exercising charity in the
Law, of course (Leviticus 19:9-10; Deuteronomy 24:17-22), but it is
also the primary means in the Old Testament books of history
(Ruth 2:2), and in the Prophets (1 Samuel 21:1-6). Gleaning prin-
ciples also take a high profile in the Gospels (Mark 2:23), and in
the New Testament Letters (2 Thessalonians 3:10).
