Copyright © 1986 by George Grant

All rights reserved. Written permission must be secured from the
publisher to use or reproduce any part of this book, except for
brief quotations in critical reviews or articles.

Co-published by Dominion Press, Ft. Worth, Texas, and Thomas
Nelson, Inc., Nashville, Tennessee.

Printed in the United States of America

Unless otherwise noted, all Scripture quotations are from the
New King James Version of the Bible, copyrighted 1984 by
‘Thomas Nelson, Inc., Nashville, Tennessee.

Library of Congress Catalog Card Number 86-050794
ISBN 0-930462-17-3
