xiv In the Shadow of Plenty

principles. These principles work, unlike people on public welfare.

This means that God, and only God, can permanently heal
hearts, minds, families, and nations. But men who are in rebel-
lion. against God do not want to hear this message. They want
healing, but they want it on their terms. They are unwilling to be-
come humble before God, but they know that to gain healing,
they must become humble before the healer. The state is the self-
proclaimed healer in this century, so men become humble before
the state, as the institutional incarnation of man.

A few humanists don’t become humble before the state. These
are the people who think they run the world. They think they con-
trol their environment because they think they control the state.
They don’t; God controls the state (Romans 13:1-7). But He
allows them to deceive themselves for a time, and to become in-
struments of His wrath, sometimes even against God’s people,
who have drifted away from His principles of righteousness.
Those who think they run the world through power become the
modern-day equivalents of the Philistines, Assyrians, and Baby-
lonians, who brought God’s Old Covenant people under tem-
porary judgment. God always destroys these pagan judges.

One of the judgments that God’s people are brought under
today is the tax-financed welfare system. The tax burden to sup-
port the various welfare. programs takes the lion’s share of the Fed-
eral budget each year. Christians are under bondage to the state
and. the tax collector because they have preached and believed
that God’s mandatory tithe of ten percent of income tothe church
is somehow optional, that Christians should “give as the Spirit
moves them,” and this so-called Spirit (men’s own deceptive
hearts) has for a hundred years “moved” Christians to give a lot
less than ten percent. (What the real Holy Spirit tells men is
found in the Bible: a mandatory ten percent tithe.) The tithe is
built into-man’s heart. It is a covenantal requirement before God.
So, under the pressure of guilt, voters have allowed the state to
collect four times. the tithe they would have owed to God. Either
we pay a tithe to’ God, or we pay far more than a tithe to Caesar.
