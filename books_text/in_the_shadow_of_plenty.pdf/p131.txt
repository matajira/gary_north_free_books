The Foundation of Peace 115

faith, who for the joy that was set before Him endured the cross,
despising the shame, ‘and has sat down at the right hand of the
throne of God (Hebrews 12:1-2).

The future is ours.

But the days are urgent. Humanism’s empire of perversity
and idolatry, of greed and gluttony, is collapsing like a house of
cards. Peace is nowhere to be found.

The battlefields of Europe, Southeast Asia, Central America,
and the Middle East give vivid testimony that humanism’s hope
for peace on earth is a false hope. The economic ruin of
Nicaragua, Ethiopia, Afghanistan, Poland, and Russia give vivid
testimony that humanism’s hope for utopia is a false hope. The
ovens of Auschwitz, the abortuaries. of L.A., the bathhouses of
New York, and the nurseries of Bloomington give vivid testimony
that humanism’s hope of medical and genetic perfectibility is a
false hope. The ghettos of Detroit, the barrios of West San. An-
tonio, the tent cities of Phoenix, and the slums of St. Louis give
vivid testimony that humanism’s hope of winning the “war on
poverty” is a false hope.

But the Biblical hope has never yet been found wanting.

So, what are we waiting for?

There is starving in the shadow of plenty. There is a job that
must be done. And only we can do it.

Oh, sure, it is a monumental task. There is no denying that to
tackle the hunger, homelessness, and hopelessness that blights our
land will require massive resources, unending commitment, and
diligent labor. Dominion doesn’t happen overnight. Peace isn’t
won in a day.

But . . . God has given us His blueprints. And His plan can-
not fail.

It must be admitted that “there are giants in the land”
(Numbers 13:33) and that “we appear to be grasshoppers in our
own sight,.and in theirs . . .” (Numbers 13:33).

But ... . God has given us His promises. And His Word can-
not fail.

Certainly, long term unemployment, the deinstitutionaliza-
