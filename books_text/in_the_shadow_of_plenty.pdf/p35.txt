Word and Deed Evangelism 19

he also mobilized a veritable army of workers for emergency relief
at a time when central Europe was struck with one disaster after
another,

Not only was John Calvin's (1509-1564) Geneva known
throughout the world as the center of the Reformation, the hub of
the greatest revival since Apostolic days, it was also renowned as a
safe haven for all Europe’s poor and persecuted, dispossessed, and
distressed.

Not only was George Whitefield (1714-1770) the founder of
Methodism (John Wesley was brought into the movement and
then discipled by Whitefield) as well as the primary instigator of
the Great Awakening in America, he was also the primary patron
of Georgia’s first orphanage and the driving force behind that col-
ony’s first relief association and hospital.

Not only was Charles Haddon Spurgeon (1834-1892) “the
prince of the preachers” proclaiming the good news of “Christ and
Him crucified” throughout Victorian England, he was also the
founder of over 60 different charitable ministries including hospi-
tals, orphanages, and almshouses.

Not only was Dwight L. Moody (1837-1899) America’s pre-
mier evangelist throughout the dark days following the Civil War,
he was also responsible for the establishment of over 150 street
missions, soup kitchens, clinics, and rescue outreaches.

And on and on and on the story goes. From Francis of Assisi
to Fraricis Schaeffer, from Polycarp to William Carey, obedient
believers have always cared for the poor, the helpless, the orphan,
and the widow. They wed word and deed.

For them, charity was, and is, central to the gospel task. And
as a result, souls were saved, nations converted, and cultures
restored. The message of their mouths was validated and authen-
ticated by the work of their hands. The “peace that surpasses all
understanding” became the inheritance of many. because God’s
faithful covenant people kept His commandments.

Isaiah knew that. So did Augustine, Bernard, Wyclif, Hus,
Calvin, Whitefield, Spurgeon, Moody, and countless others
throughout the church’s glorious march through the ages. They
