124 In the Shadow of Plenty

The only way to remove work disincentives from the welfare
system is to remove entitlements. If benefits are to be given at all,
they should carry with them obligations, expectations, and re-
sponsibilities,

Entitlements are an oppressive bondage upon the poor. They
are discriminatory. They are wicked.

Is this not the fast that I have chosen: To loose the bonds of
wickedness, To undo the heavy burdens, To let the oppressed go
free, And-that you break every yoke (Isaiah 58:6)?

Entitlements must go-

Minimum Wage Laws

Entitlements are not the only government sanctioned tools of
discrimination. Minimum wage laws too, cause high unemploy-
ment among low-skilled workers, eliminating them from the main-
stream of society.

Supposedly designed to protect workers on the lower end of
the economy. from exploitation, the laws actually work to exclude
thern from the economy altogether. After all, if a wage of $3.35 an
hour must be paid to every worker, then what kind of workers will
be hired? Very simply, only those whose skills aré “worth” $3.35 an
hour or more. If a laborer is inexperienced and unskilled, he has
little chance of breaking into the market. The minimum wage
then effectively eliminates the opportunities of the poor. Only crime
is unencumbered by minimum wage laws. Ever wonder why.
street crime is so enticing to today’s impoverished youth?

But, besides the questions of skill or experience, minimum
wage laws also raise the question of race, to the detriment of
minorities. If a racist employer is forced by the government to pay
the same minimum wage to blacks, whites, and hispanics, his hir-
ing criterion ceases to be economic and becomes instead preferen-
tial. Whom will he hire, an unskilled, inexperienced black, or an
unskilled, inexperienced white? By levelling the market interests,
racists are encouraged to discriminate. Blacks and other minorities
suffer.
