Hi Ho, Hi Ho, It’s Off to Work We Go 55

“the oppressed go free” (Isaiah 58:6); it. involves dividing bread
with the hungry, bringing the homeless poor into the house, and
covering thé naked (Isaiah 58:7). It involves transforming poverty
into productivity.

Charity to the sluggardly, on the other hand, involves admoni-
tion and reproof (2 Thessalonians 3:15; Proverbs 13:18). The com-
passionate and loving response to a sluggard is to wem him. He is
to be warned of the consequences of immorality (Proverbs 5:10),
of sloth (Proverbs 6:11), of deception (Proverbs 10:3),. of negli-
gence (Proverbs 10:4), of selfishness (Proverbs 11:24), of boastful-
ness (Proverbs 14:23), of slackfulness (Proverbs 19:15), of drunk-
enness (Proverbs 21:17), of gluttony (Proverbs 23:21), and of
thievery (Proverbs 28:22). Charity to the sluggardly does not add.
to his complacency by making life increasingly easier to abuse
through promiscuous entitlement programs. Instead, charity to
the sluggardly equips and enables him to move beyond depend-
ency, beyond entitlement.

Subsidizing sluggards is.the sare as subsidizing evil. It-is subsi-
dizing dependence. It is ultimately subsidizing. slavery— moral slavery
first, and then physical slavery. On the other hand, refusing to
care for the oppressed is the same as endorsing evil. It is endorsing in-
Justice. It is ultimately endorsing slavery— again, moral and physical.

Implementing Care

The Apostle Paul, master laborer for the Kingdom of Christ,
understood all this, and thus built into the Biblical blueprint for
welfare basic work requirements; requirements that ultimately
made distinctions between the oppressed, or deserving poor, and
the sluggardly, or undeserving poor.

But we command you, brethren, in the name of our Lord Jesus
Christ, that you withdraw from every brother who walks disorderly
and not according to the tradition which he received from us. For
you yourselves know how you ought to follow us, for we were not
disorderly among you; nor did we eat anyone’s bread free of
charge, but worked with labor and toil night and day, that we
might not be a burden to any of you, not because we do not have
