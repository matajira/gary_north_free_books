48 In the Shadow of Plenty

In so doing, Ahaz violated one of the most basic principles in
all of Scripture: God’s people must not depend on the world or the
ways of the world or the men of the world; the sovereign Lord
alone is their security.

Dominion is accomplished through service, so when we allow
evil doers to serve as our security, we yield authority and domin-
ion to them.

The church is charged with being the nursery of the Kingdom.
Thus, the church must bring the benefits of Kingdom to the na-'
tions —not the bureaucrats, not the benefactors, but the church.

Thus when we compromise ourselves with a reliance on gov-
ernment to do cur job, we not only yield undue authority and un-
warranted dominion to them, but we also limit the effectiveness of
our evangelistic impact.

Charity is too important to be left to.the state. It is also way
too dangerous to be left to the state. It is time we did our job.
