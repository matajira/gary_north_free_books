u
WHAT THE GOVERNMENT SHOULD DO

In light of these ten basic principles in the Biblical blueprint
for welfare, what should the government do?

Obviously, it shouldn’t be doing what it is doing now.

In every way, shape, and form imaginable, government wel-
fare obstructs compassion, short circuits efficiency, and stymies
progress. Government welfare is a hopeless mess.

Wouldn’t it be better to create a messless hope?

Only thirty cents of each anti-poverty dollar actually reaches
the poor. The other seventy cents is gobbled up by the govern-
ment’s lumbering bureaucracy. Thus, it takes lots and lots of dol-
lars even to make a show of it. Not surprisingly, welfare spending
in the U.S. has risen by at least 5% every year since the “war on
poverty” was announced. Social welfare spending has never been
cut—not in the Nixon administration, the Ford administration, or
in the Carter administration.

What about the Reagan administration? Despite all the liberal
moaning and groaning and the media hype over so-called “budget
cuts,” the’ Reagan administration has progressively increased
social welfare spending every year it has held office. In 1981, it
spent $68.7 billion more than the year before. In 1982, it spent
$54.2 billion more than that. In 1983, it spent $45.8 billion more
than that. And in 1984, it spent $39.6 billion more than that. The
only thing it has cut is the rade of increase in spending.

And what has this incomprehensible deluge of spending
wrought? How are the lives of the poor improved? :

They aren’t. If anything, the poor are worse off than ever before.

121
