Word and Deed Evangelism 15

We owe everything to Christ. Our riches, however defined,
come from Him. He experienced poverty to make our blessings
possible. He became a servant for our sake. He exercised charity
on our behalf.

And then came the uliimate charity: He suffered death and sep-
aration from God His Father for the sake of placating God’s eter-
nal wrath against us.

He made Himself of no reputation, taking the form of a ser-
vant, and coming in the likeness of men. And being found in ap-
pearance as a man, He.humbled Himself and became obedient to
the point of death, even the death of the cross. Therefore God also
has highly exalted Him and given Him the name which is above
every name, that at the name of Jesus every knee should bow . . .
(Philippians 2:7-10).

Talk about serious charity! Yes, Jesus brought a message of
judgement (Matthew 23:13-36). Yes, Jesus brought a message of
great hope (Matthew 28:18-20). But He never let those words
stand alone. He authenticated them with deeds.

What Does Charity Prove?

To challenge men with the gospel, we must first love them.
Isaiah loved the people of Judah. He sacrificed his whole life to
bring the message of salvation to: those few who would listen. A
man who has been loved by God is to show love to others: first, by
proclaiming the coming judgement of God, second, by announc-
ing His gracious escape, and third, by demonstrating commit-
ment to God above, caring for the poor and helpless.

Though I speak with the tengues of men and of angels, and
have not charity, I am become as sounding brass, or a tinkling
cymbal. And though I have the gift of prophecy, and understand
all mysteries, and all knowledge; and though I have all faith, so
that 1 could remove mountains, and have not charity, 1 am noth-
ing. And though I bestow all my goods to feed the poor, and
though I give my body to be burned, and have not charity, it
profiteth me nothing (1 Corinthians 13:1-3 KJV).
