4 In the Shadow of Plenty

citizen for productivity and self-sufficiency. It was supposed to
usher in a new era of abundance and prosperity. According to its
champion, President Lyndon Johnson, it was supposed “to elim-
inate the paradox of poverty in the midst of plenty.”

But more than two decades later, the paradox remains. The
“war on poverty” is a dismal failure.

Poverty is actually increasing. In 1950, one-in-twelve Ameri-
cans (about 21 million) lived below the poverty line. In 1979, that
figure had risen to one-in-nine (about 26 million). Today, one-in-
seven (33.7 million) fall below the line.

More than one-fourth of all American children live in poverty
{up from 9.3% in 1950 and 14.9% in 1970). And for black children
under age six, the figures are even. more dismal: a record 51.2%.

Today, 81% percent of elderly women livirig alone live. in pov-
erty, all too often in abject poverty, up from a mere 37% in 1954,

As many as three million. Americans are homeless, living out
of the backs of their cars, under bridges, in abandoned ware-
houses, atop streetside heating grates, or in lice-infested public
shelters. Even at the height of the depression, when dust-bowl ref-
ugees met. with the “grapes of wrath” on America’s highways and
byways, there have never been so many dispossessed wanderers.

Crime is. up. Educational standards are down. Unemploy-
ment figures have finally climbed down from “recession” highs to
“recovery” lows, but before the bureaucrats strike up the band,
close scrutiny should be given to the fact that long-term, and hard
core unemployment continues unabated, mching toward record
highs every month.

Amidst all this human carnage, where have the masterminds
behind the “war on poverty” been? What have they been doing?

Very simply, they have been squandering vast amounts of
time, money, and resources.

In 195i, spending for all the government's social welfare pro-
grams barely topped $4 billion. By 1976, the “Great Society” had
far superceded the legacy of “Camelot,” spending $34.6 billion. In
1981, welfare activists were appalled by the “scrooge sentiment” in
Washington when social welfare spending was “limited” to a
“mere” $316.6 billion!
