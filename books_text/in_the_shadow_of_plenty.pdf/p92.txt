76 In the Shadow of Plenty

But the worst of it was not the gross waste of billions upon bil-
lions of dollars. The worst of it was the awful human waste that
resulted.

Decentralized, Law-ruled, family-centered Biblical charity is
personal. It is intimate. It is flexible, It is efficient. It is compas-
sionate.

Centralized government welfare, on the other hand, is a
bumbling, fumbling, uncoordinated monster. It blunders its way
along, splintering families; crushing incentive, decimating pride,
and foiling productivity, It naturally falls into the traps of blatant
mismanagement, fiscal irresponsibility, and misapportioned
authority.

Why? Because it is today an aspect of the civil government.
And civil governments are. inherently bureaucratic these days.
They do everything “by the book.” In other words, the system is
innately impersonal.

‘When we depersonalize and centralize the apparatus for char-
ity, when we yield the responsibility to care for the poor to these
professional humanitarians, charity ceases to exist altogether.

Isn’t it about time that was changed?

Isn’t it about time we learned the lesson of Moses and Jethro?

Conclusion

‘The sixth basic principle in the Biblical blueprint for welfare is
that charity must be decentralized and family-oriented in order to
function properly.

Moses was faced with an almost insurmountable task in dis-
pensing justice in Israel. He tried to do it all by himself. But his
father-in-law, Jethro, showed him how to utilize the natural gifts
and leadership abilities in others to get the big job done: decen-
tralize.

The family is the most dynamic example of how decentraliza-
tion can work effectively and efficiently under the Law of God,
especially in the area of charity. God designed the family to be
flexible, accountable, compassionate, and diligent. Thus, the
family was and is best able to handle the tasks of overseeing glean-
