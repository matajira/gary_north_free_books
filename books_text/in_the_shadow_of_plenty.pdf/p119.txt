Exceeding What Is Written 103

We also have the prophetic word made more sure, which you
do well to heed as a light that shines in a dark place, until the day
dawns and the morning star rises in your heart; knowing this first,
that no prophecy of Scripture is of any private interpretation, for
prophecy never came by the will of man, but holy men of God
spoke as they were moved by the Holy Spirit (2 Peter 1:19-21),

All Scripture is given by inspiration of God, and is profitable
for doctrine, for reproof, for correction, for instruction in right-
eousness, that the man of Ged may be complete, thoroughly
equipped for every good work (2 Timothy 3:16-17).

To go beyond. Scripture would have meant to evade the pur-
poses of God (I Corinthians 4:6). The more Nehemiah drew near
to God in prayer, the more he depended on Him for guidance, the
more he realized that Scripture was the only rule hé needed for life
and godliness. God’s will was to be found in God’s Word. God
Hirnself was clear enough on that score:

You shall not add to the Word which I command you, nor take
anything from it, that you may keep the commandments of the
Lord your God which I command you (Deuteronomy 4:2).

Whatever I command you, be careful to observe it; you shall
not add to it nor take.way from it (Deuteronomy 12:32).

Every Word of God is pure; He is a shield to those who put
their trust in Hirn. Do not add to His words, Lest He reprove you,
and you be found a Har (Proverbs 30:5-6).

Diligence in prayer always drives God’s people to. dependence
on the Word, Nehemiah was not a solitary example. Such was the
case with David (Psalm 51:1-19), and Jeremiah (Lamentations
§:1-22), and Jonah (Jonah 2:2-9), and the disciples (Acts 1:8-14),
and the Jerusalem church (Acts 2:1-47).

Likewise, a failure to seek God in fellowship leads invariably
to a violation of God’s Word and a rejection of His purposes. Such
was the case with Cain (Genesis 4:3-8), and Korah (Numbers
16:1-35), and Balaam (Numbers 22:2-40), and Saul (t Samuel
13:5-14).
