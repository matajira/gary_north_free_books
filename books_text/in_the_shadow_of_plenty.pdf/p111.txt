The Unbroken Circle 95

Diligence is required because diligence is blessed with pros-
perity.

Family participation is required because families are the basic
building blocks of society.

Even more than these, though, obedience is required. Submission
to the standards of the Kingdom is required. In order to take ad-
vantage of the covenant privileges, a man must be in the covenant
or dependent on the covenant. Even when the church reaches out
into the streets, and lanes, and hedgerows, drawing in the
cast-offs and dregs of the land, responsibility must be enforced,

Jesus said,

-» “When you give a dinner or a supper, do not ask your
friends, your brothers, your relatives, nor your rich neighbors, lest
they also-invite you in back, and you be repaid. But when you give
a feast, invite the poor, the maimed, the lame, the blind. And you
will be blessed, because they cannot repay you; for you shall be
repaid at thé resurrection of the just.” Now when one of those who
sat at the table with Him heard these things, he said to Him,
“Blessed is he who shall eat bread in the kingdom of God!” Then
He said to him, “A certain man gave a great supper and invited
many” and sent his servant at supper time to say to those who were
invited, “Come, for all things are now ready.” But they all with one
accord began to miake excuses. The first one said to him, “Ihave
bought a piece of ground, and I must go and see it. I ask you to
have me excused.” And another one said, “I have bought five yoke
of oxen, and I am going to test them. T ask you to have me ex-
cused.” Still another said, “I have married a wife, and therefore I
cannot come.” So that servant came and reported these things to
his master. Then the master of the house, being angry, said to his
servant, “Go out quickly into the streets and lanes of the city, and
bring in here the poor and the maimed and the lame and the blind.”
And the servant said, “Master, it is done as you commanded, and
still there is room.” Then the master said to the servant, “Go out
into the highways and the hedges, and compel them to come in,
that my house may be filled. For I say to you that none of those
men who were invited shall taste of my supper” (Luke 14:12-25).
