2
THE SAMARITAN LEGACY

And behold, a certain lawyer stood up and tested Him, saying,
“Teacher, what shall I do to.inherit eterna} Jife?” He said to him,
“What is written in the law? What is your reading of it?” So he an-
swered and said, “You shall love the Lord your God with all your
heart, with all your soul, with all your strength, and with all your
mind,” and “your neighbor as yourself.” And He said to him, “You
have answered rightly; do this, and you will live” (Luke 10:25-28).

It was supposed to be a test. Straightforward. Simple. A test of
orthedoxy. A test of theological skillfulness. Not a trap, really.
Just a test.

“You shall love the Lord your God . . .” Jesus said, “. . . and
your neighbor. . .”

Well, so far, so good. Christ's Sunday School-word-perfect-
never-miss-a-beat response should have been music to the ears of
any good Phatisee. He-unhesitatingly upheld the Mosaic Law. He
was careful “not-to exceed what is written” (1 Corinthians 4:6). He
was impeccably orthodox.

If that were the end of the story, it would be less a story than a
dry recitation of doctrine: true, good, and necessary, but not par-
ticularly gripping. But of course, the story doesnt end there. It
goes on: true, good, necessary, and gripping.

The lawyer just wouldn't let Jesus off the hook. He continued
to cross-examine the Lord. He pressed the issue. Sinful men love
to do this. They want God on the hot seat. They want God in the
dock.

Jesus wouldn't make Aim look like a fool!

25
