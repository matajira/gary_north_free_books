The Penal Code as an Instrument of the Covenant Community 235

ness — between God’s people and the world —is “blurred.” Does
this imply that Christians should encourage a civil society where
people act as evil as they can, so that the contrast between them
and law-abiding Christians will be ever so clear? In opposition to
Bibza’s thinking here, Paul teaches that it is a “lawful” use of
God’s law to restrain public misdeeds by it (I Timothy 1:8-10).
Those who are “the light of the world” (over against the darkness
of sin) are also called upon to be “the salt of the earth,” preserving
it from spoil and decay (Matthew 5:13-14).

Moreover, there is no credibility to the notion that punishing
rape, blasphemy, and other crimes defined by God’s law forces
people to “look like believers” in any distinctive way. After all,
not all non-rapists are in fact believers! We must dismiss Bibza’s
poorly conceived argument that the use of penal sanctions (at
least God’s sanctions) is wrong because it restrains criminal activ-
ity and thereby — allegedly — “blurs the distinctions between the
church and the rest of unbelieving society.”

Are the Penal Sanctions
Only for the Church?

Richard Lewis criticizes the theonomic position by attempting
to explain the death penalty in Old Testament Israel as “the only
means of purging away the evil” from God’s kingdom — then
claiming that in the New Covenant this aim is accomplished by
excommunication instead.? There are obvious flaws in this reason-
ing. First, death was not at all “the only means” for purging away
evil from Israel. Exile would have accomplished the same. And if
death alone would do, then every crime in Israel should have

  

2. Personal report on “The Applicability of the Penal Gode of the Old Testa-
ment” (privately distributed in connection with a study committee for the Presbytery
of Northern California, O.P.C., 1982). Similarly, Lewis Neilson argues that “the
penal laws reflected the church” in Old Testament Israel, and that “all the judg-
mental powers of Israel passed to the church as we know it” today in the New
Testament (Gods Law in Christian Ethics: A Reply to Bahnsen and Rushdoony (Cherry
Hill, New Jersey: Mack Publishing Co., 1979}, p. 36). He uses an ambiguous and
misleadingly broad sense for “church” in this reasoning. Food “nourishes” a person,
and the Bible too “nourishes” a person; but it would be a howling fillacy to think
that the Bible has now replaced the ancient use of food. The penal sanctions of the
Old Testament were applied by magistrates, not priests.
