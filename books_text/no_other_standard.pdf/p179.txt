God’s Law and Civil Government Today 163

fenses (chapters 5 and 6, respectively, in his booklet). Howezer, he
does so only after the main supporting texts for the theonomic view
(viz., Matthew 5:17-19 and Romans 13:1-7) have been removed
from consideration in gaining a “preponderant impression” of the
New Testament outlook. That Neilson finally concludes that the
trend of the New Testament is not supportive of the theonomic
perspective under these conditions is hardly one of the logical
wonders of the world! If an attorney were to attempt this line of
reasoning in a court of law (e.g., telling the jury that his client
gives the impression of being innocent when all of the prosecutor’s
key evidence has been removed from consideration) he would be
held in contempt or fired from the case! It is more than just a bit
unfair, even if we are going to argue in terms of “overall impres-
sions,” to gain our impression of the New Testament’s tenor by
arbitrarily precluding indicators which are (apparently) contrary
to what we hope to show. This procedure does not give us an
overall impression, but simply one that has been cut down to size.
Imagine what it would be like to defend Christ’s virgin birth from
an overall New Testament impression once the supportive passages
had been removed, only then to be told that this non-supportive
overall impression now stands in contradiction to the purportedly
supportive evidence, thereby contradicting your interpretation of
those passages themselves! We would rightly think that a refuta-
tion had been spun out of thin air.

In the second place, when Neilson does get around (in chapter
7) to “reinserting” the key theonomic passages, the point he hopes
to have made is that everything now hangs upon these passages,
for the theonomic argument apart from them has been under-
mined by his (truncated) overall imprcssion of the New Testa-
ment’s tenor. However, he chooses first to consider these key
passages separately, so that once again he can gain the advantage
of comparing an overall impression with an individual evidence
isolated from its total context. If his point, as he says, is to ask
what the overall spirit of the New Testament is regarding the
question of civil sanctions for certain “religious” offenses, then his
consideration of the theonomic supportive passages in isolation is
again a mite unfair. It is not until page 50, at the very end of his
