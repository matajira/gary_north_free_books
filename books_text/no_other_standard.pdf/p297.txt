The Exegesis of Matthew 5 281

However, he has one more argument to set forth. He notes
that the verb in this clause, pareltha, is in the third person singular,
and he recalls that a neuter plural subject in Greek can take a
third person singular verb; he concludes, then, that panta is being
treated like a neuter plural subject. Fowler’s reasoning here is as
fallacious as his Greek grammar. In the first place, the practice
of using a singular verb with neuter plural subjects pertains to
words, not phrases. In the second place, Fowler commits a notori-
ous logical fallacy here: affirming the consequent. He reasons: (1)
Ifa subject is neuter plural, then it can take a singular verb; (2)
“one jot or one tittle” is a subject taking a singular verb; and (3)
therefore, “one jot or one tittle” is a neuter plural subject. To put
it simply: (1) If P, then Q; (2) Q; (3) Therefore, P. One just as
well could argue: (1) If Castro shot Kennedy, Castro is a scoun-
drel; (2} Castro is a scoundrel; (3) therefore, Castro shot Ken-
nedy. Such reasoning patterns are totally unacceptable (for they
overlook other causes or reasons for the second premise than the
one suggested in the first premise). Blass and Debrunner suffi-
ciently explain why “pass away” is in the singular; this is what
we expect when the subject of the clause is two singular subjects
connected by a— precisely the situation in Matthew 5:18.

Fowler’s treatment of genetai, taking it to mean “fulfill,” is a
further flaw in his interpretation of Matthew 5:18, His appeal to
a parallel with 5:17 is based on his own unproven assumptions,
and his appeal to “root meaning” is simply contrary to fact for
this common and colorless Greek verb. Fowler just reads into this
word the heavy theological overtones he seeks to find. Fowler’s
interpretation of this verse would, moreover, suggest that the Old
Testament commandments (e.g., prohibition on bestiality) passed
away when Jesus himself perfectly obeyed them — which is theo-
logically and ethically intolerable, as though three years after
Jesus’ dramatic statement it became morally permissible for peo-
ple to commit bestiality, the law having now passed away!

Finally, it should be noted that Fowler’s interpretation creates
a contradiction within Matthew 5:18, for it portrays the saying
of Jesus as specifying two conflicting points of termination for the
