13

THE PENAL CODE AS AN INSTRUMENT
OF THE COVENANT COMMUNITY

Were Gentile Crimes Not an Insult to Jehovah?

In an evaluation of theonomic ethics, Jim Bibza once made
the amazing statement that homosexuality, false prophecy, and
blasphemy were punished with death in Israel only because such
actions would be a poor testimony to unbelieving nations about
the type of God worshiped in Israel. But such things would not
have been punishable in Gentile nations, asserts Bibza, because
“when a Babylonian or Assyrian engaged in false prophecy, blas-
phemy, etc., it said nothing about Yahweh.”! This is not at all
theologically accurate. The false prophecies of Babylonians were
a direct insult and presumptuous sin against the one and only,
living and true God of all creation. To prophesy something He
has not revealed or to prophesy in the name of another deity was
and is a momentous sin which despises the prerogatives and
holiness of Yahweh. This was just as true within Israel as outside
Israel, which explains why the Old Testament condemns the
idolatry, superstitions, homosexuality, etc. of the heathen nations
outside Israel (e.g., Sodom, Ninevah).

We must also question the cogency of Bibza’s alleged explana-
tion of the uniqueness of the death penalty within Israel for the
crimes he mentions (viz., they would give the heathen a wrong
idea about Yahweh). In Biblical perspective, blasphemy, homo-
sexuality, etc. are capital crimes even if only Israelites are wit-

1. Jim Bibza, “An Evaluation of Theonomy” (privately distributed from Grove
City College, Spring, 1982), p. 4

233
