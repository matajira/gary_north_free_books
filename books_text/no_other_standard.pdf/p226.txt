“The earth also is polluted under the inhabitants thereof because
they have transgressed the laws, violated the statutes, broken the
everlasting covenant.”

Isaiah 24:5

“Perhaps the Noahic covenant . . . applies to the world at large.
But here the agency of man in applying a sanction is invoked only
with regard to murder. This means that the only sanction re-
quired of all civil government by God’s covenant with all mankind
is the death penalty for murder.”

Dan McCartney,
Theonomy: A Reformed Critique (1990), p. 147

“While one might appeal to the fact that the Noahic covenant
was made with all the living creatures as well as Noah and his
seed (thus having universal scope), someone else could just as
well appeal to Romans 1-3 to show that the whole world is also
under the Mosaic law (thus having universal scope as well).”

Theonomy in Christian Ethics
(1977), pp. 462-463
