38 NO OTHER STANDARD

little place or relevance, however, in a critique of the theonomic
position, It is just here that critics have fallen into a notorious logical
fallacy (viz., arguing ad hominem). Its fallacious nature can be seen
from a number of angles.

(1) The theonomists who are criticized by the critics for their
bad personalities (or for using careless expressions) are also pro-
fessing Christians. If their bad personalities were proper grounds
for critics to reject theonomy as a position, they would likewise
be proper grounds to reject the Christian faith altogether — which
would be absurd. (2) Theonomists themselves have been critical of
other theonomists for the harshness or carelessness about which
the critics complain. Obviously it is not the theological position
itself which is the culprit, then, since adherents of that position
both display and decry the bad personal traits or habits which are
in view. (3) Indeed, you find this to be the case among non-
theonomists as well. Both camps contain individuals who fall
short of the mark in Christian maturity or sanctification. Both
have “embarrassing advocates” as well as models of Christian
grace and love. It would be too easy a task (and unedifying) to
amass a list of the pugnacious, arrogant, divisive and churlish
behavior or remarks of various anti-theonomic writers and preach-
ers. Such a personal laundry-list would not have anything to do
with refuting their views as a theological position, though. I only
ask that opponents of theonomy apply the same restraint in their
attempt to criticize the view.

An example of responsible and irresponsible attitudes toward
criticizing theonomy can be found in one article by two authors.
Douglas Chismar and David Rausch criticize theonomic ethics in
an article for the Christian Century [Aug. 3, 1983], though not by
addressing themselves to theological substance, but for the most
part by engaging in ridicule and personal panning sustained by a
number of false allegations (which could be easily disproved).
This led subsequently to Mr. Chismar publishing an embarrassed.
retraction [Nov. 9, 1983]. We would all greatly prefer not to have
been misrepresented and personally defamed in the first place, of
course, but I must say how much J respect Mr, Chismar’s humil-
ity and efforts to clear the air.

For his own part, by contrast, Mr. Rausch has now pushed
