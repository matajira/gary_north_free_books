16 NO OTHER STANDARD

in church history, particular applications of the theonomic thesis,
sociological speculation concerning the “movement,” etc. Although
such matters are but a sidelight to the hermeneutical and theologi-
cal controversy, they are of interest to me, naturally, and are areas
where I think there has been considerable misunderstanding. But
that will need to await another day.”

29. By way of acknowledgment, I wish to extend my gratitude to Gary North for
patiently supporting this publication through to its completion. I am grateful to the
Southern California Center for Christian Studies for supporting my writing and
lecturing, granting me time to write this book. (For information abouc the center and
its activities, write to SCCCS, P, O. Box 18021, Irvine, CA 92713). The affection and
extra help around home given me by my four children during the compressed time
of composing and preparing the book will be remembered with thankfulness. Finally,
T wish to thank Virginia Bahnsen and Mare Porlier for their valuable help in
preparing and proofreading the manuscript — truly labors of love. Above all I thank
the Lord Jesus for being so gracious as to cali me into His eternal kingdom, and I
pray that my thinking and teaching might more faithfully serve Him who is worthy
of all praise as the Lamb of God and King of kings.
