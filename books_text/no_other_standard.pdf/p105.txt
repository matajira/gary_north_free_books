Change of Dispensation or Covenant 89

it by making it a standard of behavior today.

The only other passage to which Zens makes exegetical ap-
peal in order to justify his view of the passing away in foto of the
Mosaic law (due to the passing away of the Mosaic Covenant) is
1 Corinthians 9:20-21, There Paul indicates that he was not with-
out the law of God, however, for he was “in-lawed” to Christ.
Now, Zens might initially think that the law of Christ (and hence
of God) which Paul kept was divorced from the Old Testament
law’s authority, for Paul says in the same passage that he operated
as someone under the law at some times and without the law at
other times. It is obvious, however, that Paul was not a moral
schizophrenic, living arbitrarily under the requirements of the law
but just as arbitrarily rejecting the law’s demands at times as
well. Nor was Paul speaking of trying to gain salvation “under the
law” at some times and living as an antinomian at other times.
Whatever interpretation one gives to these various attitudes (“under
law,” “without law,” “not without law,” “in-lawed to Christ”),
they must be consistent with each other and be in agreement with
other Pauline teaching. If Zens interprets the passage as saying
that Paul lived without the Old Testament law and simply under
the law of Christ, then it is difficult to understand or justify Paul’s
other statement that he indeed lived under this law as well.

The problem can be resolved, though, when we take notice
of the context. Paul was speaking of the differences in his lifestyle
or mode of ministry when he was among Jews and when he was
among Gentiles. Certain Old Testament precepts erected a wall
of separation between the Jews and Gentiles (cf. Eph. 2:15), such
as dietary restrictions, the Passover, and the ceremonial system
of ordinances in general. When he ministered to Jews, Paul lived
in conformity with their ceremonial customs; but when he was
among Gentiles, he functioned without such laws. Nevertheless,
this variation (made possible by the fulfillment of the ceremonies
in Christ and the removal of the wall of separation between Jews
and Gentiles thereby) between being “under” the law and being
“without” the law did not nullify the general truth that Paul was
still with the law of God in his life (i.c., not without it) because
he had submitted to the law of Christ. This passage, then, does
