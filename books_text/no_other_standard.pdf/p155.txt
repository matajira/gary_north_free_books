Separation of Church and State 139

will be helpful in determining whether such a line of attack can
hope to be successful as a rebuttal of the theonomic perspective.

In his lectures Dr. Robertson affirms that the first or funda-
mental thesis of theonomic ethics — its “most important” conten-
tion —is that the relationship of cultic and civic aspects of the old
covenant order is the same as the relationship of church to state
today. He claims that this thesis underlies the distinctive conclu-
sions of theonomic ethics. In his third lecture, he goes on to argue
that this alleged thesis is the primary problem with my out-
look —its most critical defect. Sometimes Robertson claims that
Theonomy sees the church-state relation (or the relation between.
cultic and civic aspects of life) in the old covenant as “essentially
the same” as the relation in the new covenant; at other times, he
claims that Theonomy sees them as “precisely the same.” Although
there is an enormous difference between these two claims, F will
not dwell on the inconsistency in answering the criticism con-
tained in the claims,

(1) It turns out that I make zeither one of these claims in my
book. Indeed, I would not be willing to profess either one of these
claims at present. Between two objects or concepts there can exist
a large number of similarities and differences; to say that they are
the same in some respects is nof to say that they are identical
(“essentially” or “precisely”}. Theonomic ethics does not claim
that the church-state relation in the New Testament is the same
as that in the Old Testament, but rather that “a parallel can be
found.”® Finding one parallel between tanks and corvettes (e.g.,
they roll on wheels) does not make a corvette into a tank! Robertson.
has really overstated my position—just as we would overstate
Paul if we said that he saw Christian ministers as identical to
Jewish priests (on the ground that he taught both should receive
their sustenance from God’s people in I Cor. 9:13-14). Conse-
quently, what he attributes to Theonomy as its most important
thesis is not its thesis at all, and this invalidates all of his major
criticism of the book (what he calls the “primary problem”). Since
Theonomy does not deny differences between church-state relation
in the Old Testament and the New Testament, when Robertson

9. Theonomy, p. 414, clarifying emphasis added.
