God's Law and Civil Government Today 161

just punishment should be endorsed by all nations on the earth
{under the King of kings) — not “replicating” the “exact details”
of the Mosaic code, but expressing its moral requirements in a
way applicable and appropriate to each nation’s cultural setting.
Dr. Kaiser, I believe, wishes to teach the same thing.

What Impression Does Scripture Give
When the Theonomic Evidence is Removed?

Sometimes the argumentation against the theonomic view of
civil government becomes noticeably strained. Critics reluctantly
admit the apparent strength of the Biblical and theological case
which is made for the theonomic position and then go to strange
lengths to overcome it. We can take as an example the booklet
written by Lewis Neilson.!°

Neilson is not very confident of the strength of his argument
against a narrow aspect of theonomy’s view of the civil magistrate
(viz., enforcing the Mosaic penal sanctions about “religious”
crimes). His own admitted feeling is that the issue which he raises
is difficult to decide; the question is not easily answered ~is “not
of easy determination.” He allows that “we cannot be certain” of
his perspective from the verses studied, and it is his opinion that
there és no specific and clear reference in Scripture which could
be absolutely definitive in this matter. Accordingly he openly says
that he “is not purporting to rest too much” on his lines of
argumentation. He hopes that, “although no one aspect of what
has been said is conclusive,” there might be a tendency in his
combined remarks to show his point. And in contrast to this
tentativeness Neilson concedes the apparent strength of the
theonomic argument. It is, he says, “meticulously argued” with
“constant appeal to Scripture.” The underlying premises of the
theonomic argument have “a seeming propriety.” If Theonomy is
correct about Matthew 5:17-19, then “the remainder of [the]
argument flows with a seeming relentless logic.” Neilson further
says, “I appreciate the force of all this and admit the seeming

10. God's Law in Christian Ethics: A Reply to Bakasen and Ruskdoony (Cherry Hill, New
Jersey: Mack Publishing Co., 1979).
