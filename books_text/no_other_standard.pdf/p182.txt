166 NO OTHER STANDARD

Refinement Rather Than Refutation

Only a few critics who have written against the political ethic
advanced by theonomy have gone beyond general theological
argument to pay attention to particular details in the argument(s)
used in Theonomy. The value of such efforts is that they have
attempted to be exegetically based, which is crucial to acceptable
theological conclusions. The drawback has been that the criti-
cisms offered have not been broad enough to disprove or under-
mine the basic theonomic position. Before ending this chapter we
can pay attention to the kind of exegetical considerations which
are aimed at subordinate points in the case for a theonomic view
of the civil magistrate.

The first point in limited rebuttal to the theonomic view of
the civil magistrate which Laird Harris!! wants to make is that
the anointing of theocratic kings in Isracl was not “on a par”
with the providential control of pagan thrones. With this, how-
ever, I fully agree. The point made in Theonomy was that God.
appointed both kinds of rulers (noting the wording of Romans
13:1), although anointing only one kind publicly. Harris next
contends that such appointment of Gentile rulers did not mean
that they ruled according to God’s precepts, but only under His
providential control. As a remark about their de facto govern-
ments, this is probably true enough, even though it is irrelevant
to an argument about what they ought to have done as their moral
obligation (cf. Lev. 18:24-27; 2 Sam. 23:3; Ps, 2:10-12). Harris
makes the theocratic unigueness of Israel’s rulers precisely their
obligation to follow God’s laws—which begs the question be-
tween us (cf. Ps. 119:46).

Harris also wants to argue that Old Testament political offi-
cers were not given the religious title of “priest,” and he claims
that “too much” is made of the religious titles which were given
to pagan kings in the Old Testament. Harris does not disprove
the examples which I have forwarded, but simply suggests what
“may” be another way to interpret them. And his indication that

11, *Theonomy in Christian Ethics: A Review of Greg L. Bahnsen’s Book,” Presty-
terion, vol. 5 (July, 1979), pp. 1-15.
