Separation of Church and State 143

the governors, the general concerns of civil rule, ete. Which of
these things does Robertson claim to have been in a covenant
relation with the Lord in a uniquely redemptive sense? The Mo-
saic covenant was made with people; it touched on matters of
territory, political rulers, and civil laws. Where it spoke of civil
justice, it was not uniquely “redemptive” in its content, for it was
proclaiming the righteous standards of the Creator as well (which
explains the judgment on the Canaanite tribes, the prophetic
words against the nations, and Paul’s words in Rom. 1:32; 2:14-
15). So again, I do not know whether to agree or disagree that
the Old Testament state as well as the church was in a redemptive
covenant relation with the Lord.

Not knowing whether to affirm Robertson’s statement of the
church-state relation in the Old Testament, let me move on any-
way to consider his application of the view stated.

(2) Robertson says that, becausc the Old Testament state
was a redemptive community, circumcision was required for citi-
zenship. Membership in the civic community required member-
ship in the church as denoted by the external sign of covenantal
relation with God. Nowhere, says Robertson, does the Old Testa-
ment suggest that someone could be a citizen of the state of Israel
without cultic obligations. Therefore, he concludes, a consistent
application of theonomic ethics would mean that the state today
should require baptism for citizenship — which is clearly unac-
ceptable.

In response, F would contend that Robertson’s remark is
inaccurate. Citizenship in the old covenant order did not require
circumcision, for in that case no woman was a citizen of Is-
rael — which is clearly an unacceptable implication of Robertson’s
premise. We cannot brush away this counter-evidence with a
passing remark about human physiology, a fact well known to
God when He instituted the old covenant sign. We cannot get
away from the truth that circumcision was primarily a sign of
religious significance and not civil membership; as a civil badge it
would have been extraordinarily inappropriate, unless one wishes
to contend that women were not citizens.

At this point, nothing will be rescucd in Robertson’s argument
