230 NO OTHER STANDARD

changed, rather than the law of God. The justice of God’s penal
code (Heb. 2:2) should remain our moral ideal, totally apart from
the rebellious and sinful evaluations of our society. The fact that
unbelieving philosophers today deem the gospel “foolish” does
not stop my apologetical efforts, and the fact that people today
deem God’s penal code harsh, rigid, or impossible should not stop
my spiritual and ethical efforts to convert them to a Biblical
outlook. Let God be true though every man is a liar (Rom. 3:4),
