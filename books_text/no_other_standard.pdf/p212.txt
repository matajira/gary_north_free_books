196 NO OTHER STANDARD

is nothing peculiar here at all. Nor should it be thought that
theonomists attempt to find an exact mirror of modern institu-
tional structures or cultural conditions in the Old Testament (or
New, for that matter). The only relevant distinction here is one
that is moral in character, and that moral principle is —just
because it is moral—universal in its application. This moral
principle is that mo individual has the right to deprive another individual
of his life (murder), freedom (kidnapping), or property (theft) apart from
divine authorization. God has given this authorization and the speci-
fied terms under which it is to be used to civil leaders, who alone
may execute, imprison, or fine people as God has directed for the
protection and well being of those governed by the ruler. Neither
private citizens nor leaders of the religious community (much less
any other kind of leader) have the right to judge people in this
particular way. Civil leaders do not have the right to depart from
these directives, nor do they have the right to add to them on their
own authority.

Given this moral principle — which is readily proved in both
Old and New Testaments — there is nothing whatsoever “pecu-
liar” about theonomists arguing for a limited civil government (its
use of compulsion limited to what God has authorized), a separate
church (which has not been authorized to do what civil leaders
do), and a basically free market (since neither the state nor other
individuals have been authorized to interfere with property rights
except as God has permitted). In nothing which has been said
here is there any dependence upon a particular degree or configu-
ration of societal or institutional “differentiation.” All sorts of
political administrations and all sorts of cultural variations are
readily accommodated to this universal moral principle ~in the
saine way that they readily comport with other revealed principles
of morality (e.g., the prohibition of homosexuality or murder or
theft, etc.).

Theonomists have exposed major departures from Biblical
teaching in the pluralist approach to politics—and Skillen has
not interacted with them or rebutted them by Biblical exegesis (or
relevant historical argumentation, either). Moreover, pluralism
provides no positive Biblical substantiation for its particular out-
