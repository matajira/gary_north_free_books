The Exegesis of Matthew 5 289

law, Jesus took away the law’s curse, Jesus culminates the re-
vealed truth, Jesus fulfilled Jewish history, Jesus satisfied man’s
aspirations and hopes, and Jesus replaced the Old Covenant with
the New! Needless to say, one word cannot simultaneously intend
to communicate all of these things. Long thinks of bringing some-
thing to full realization as its occurrence as an event, but else-
where as setting down its legal status (sometimes abrogating it,
sometimes preserving it). This interpretive zig-zag is hard to fol-
low, being less the result of controlled textual exegesis than the
result of wanting to say everything at once. If plaroo is given
completely diverse and even contradictory senses all at one time,
it has actually been rendered senseless.

Long’s treatment of Matthew 5:18 makes some of the same
mistakes as did Fowler's (providing an antecedent for pana, treat-
ing genatai as if it were plarosai, etc.), but the final and decisive
problem with Long’s interpretation of Matthew 5:18 is this: He
says that the verse assures us that before the earth passes away,
not the least aspect of the Old Testament will be unaccomplished.
The various parts of the Old Testament will individually, in their
own appropriate time, become gradually accomplished, dated and
will pass away, in the course of history, but the entire Old Testa-
ment will not become dated as a whole until the end of the
universe. There are a host of real problems with this unusual
suggestion, but the most salient point is that this runs counter to
the precise language of Matthew 5:18. What Jesus said there is
that not even the slightest detail (“jot or tittle”) — much less the Old
Testament as a whole — will pass away until heaven and earth
pass away. It is not merely the complete contents of the Old
Testament that will not expire prior to the universe passing away,
but not even the smallest part of the Old Testament. Jesus is
dealing with minute parts, but Long’s interpretation assumes that
Jesus spoke simply of the Old Testament as a collective whole,
Any treatment of Matthew 5:18 which renders the last clause as
invalidating the law prior to the end of history (which Long
intends to do} makes the verse self-contradictory,

Conclusion
Other critics have given attention to the exegesis of Matthew
