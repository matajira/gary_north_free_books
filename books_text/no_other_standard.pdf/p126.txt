110 NO OTHER STANDARD

mands that we see Paul’s “tutor” (schoolmaster, guardian} — whom
we are no longer under in our Christ-given maturity—as the
Mosaic ceremonial law in particular, or more generally the Mo-
saic administration of the covenant of grace. Anybody who wishes
to include more than this in Paul’s designation will need strong
textual and theological argumentation such as the critics of
theonomic ethics have not supplied.
