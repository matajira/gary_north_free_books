Flexibility Regarding the Penal Code 259

fifteen cases the judges could commute the crimes deserving of
capital punishment.”’ This reasoning involves at least two errors
which need correcting:

(1) It involves a fallacious argument from silence: viz., since
the law did not forbid commuting the capital penalty for crimes
other than murder, the penalty for those crimes may be com-
muted. This is analogous to the following line of reasoning: Christ
applies to certain specific sins the explicit censure of hell-fire (e.g.,
Matt. 5:22, 29-30; 23:15, 33; 25:41), but does not explicitly men-
tion this in connection with eery particular sin; therefore the pun-
ishment of hell applies only to a few particular sins. We would
surely reply to such logic that (a) the particular sins mentioned
are but an example of how every sin will be treated by God, and
(b) there are texts in Scripture which more generally teach that
God’s eternal wrath will be visited upon all sins in general. Like-
wise, Kaiser has not dealt with the very real possibility that the
prohibition on taking a substitute (and thus commuting the pen-
alty) in the case of murder was iniended to teach us how every capital
crime should be treated — that is, should be treated as illustrative
for the rest of the class. Moreover, Kaiser has not made any
response whatsoever to the texts in Scripture which generally
teach that the penalties of the law were all precisely just, precisely
what the crime deserved, and (apparently) not to be commuted.
Thus judges were ordered by God: “Your eye shall not pity: life
for life, eye for eye, tooth for tooth, hand for hand, foot for foot”
(Deut. 19:21; cf. 25:12); Hebrews tells us that those who broke
the relevant portions of the Mosaic law “died without compassion
upon the word of two or three witnesses” (10:28). Kaiser's inter-
pretation of Numbers 35:31 might be correct, but until he answers
Biblical considerations such as these, we have no adequate Bibli-

7, Kaiser, p, 293, The only confirmatory evidence offered by Kaiser for this
interpretation is the fact that Paul did not prescribe capital punishment in the case
of the incestuous fornicator in 1 Corinthians 5. This kind of argument from silence
ig refuted elsewhere. Are we to believe that Paul was “commuting” the sentence of
the person guilty of incest—or rather that Paul was not attempting to pass cil
sentence whatsoever? The answer should be obvious. Excommunication is not a civil
penalty whatsoever, but in Kaiscr’s view it would have been the commuted sentencet
