192 NO OTHER STANDARD

the state authorizes its actions and by which the state, as a
pluralist institution, is guided.

Pluralists rarely offer (or even think it is necessary to offer)
exegetical warrant for their key moral premises about political
order and guidance, nor do they effectively counter the Biblical
case which can be made for the theonomic perspective? Recent
Calvinistic pluralists regularly rely upon the philosophical outlook
of Herman Dooyeweerd and the language of sphere sovereignty,
assessing anctent Israel as an “undifferentiated” society. Eschew-
ing the use of specific Biblical legislation in contemporary politics
as “biblicism,” pluralists apply broad and ambiguous principles
of “justice” to the state (usually in the sense of “equality” and
tending toward a socialist or welfare state).

In brief synopsis, I believe that pluralism is neither faithful to
Scripture, or even logically cogent. (1) Contrary to the Biblical
demand that all the kings and judges of the earth “serve Jehovah”
specifically (Ps. 2:10-11), pluralism instead calls upon the rulers
of the state to honor and protect all religious positions, regardless
of their negative attitudes toward Jehovah. (2) By subtracting the
civil commandments from God’s law without relevant and specific
Biblical warrant, pluralists come under the condemnation of the
law itself (Deut. 4:2; 17:20} and under the censure of our Lord
(Matt. 5:19). (3) But not only is pluralism morally wrong, it is
logically impossible. When one religious philosophy requires the
death penalty for murder, and another religious philosophy forbids
the death penalty for murder, the state cannot conceivably give
“equal protection” to both viewpoints; whether it executes the
murderer or not, the state will have violated one of the competing
religious convictions, thus not honoring both equally. The “King
of kings,” Jesus Christ, requires certain things to be done by the
kings of the earth, and about those requirements we may not
(morally) and cannot (logically) be “pluralists.” As Jesus de-
clared, “he who is not for Me is against Me” (Matt. 12:30). As
faithful disciples of the Lord, we must urge the state to base its
actions and policies upon the one and only sound moral perspec-

3. For example, see the exchange between Schrotenboer and myself in God and
Politics: Four Views,
