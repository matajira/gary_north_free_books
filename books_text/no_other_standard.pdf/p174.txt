158 NO OTHER STANDARD

to obey the standards of civil justice revealed to Israel, Walter
Chantry observes that no political body today is a nation identi-
fied with God’s people.> Likewise, Richard Lewis contends that
the law of Moses “cannot be applied wholesale and without modi-
fication” to any civil entity today, for “no state today can be
identified with the Kingdom of God.”® But since the Mosaic law
was applied in the Old Testament itself to states which were not
“identified” with the Kingdom of God, it is evident that this
reasoning is not cogent or biblical in character. The states of the
New Testament era are surely not to be “identified with the
Kingdom of God” — even though the King of kings has only one
universal moral standard to which He holds all kings responsible.

Were the Mosaic Civil Laws Not
(“Directly”) for the New Covenant?

House and Ice agree with theonomists that in Romans 13
Paul states that rulers have the function of being avengers of His
wrath, They immediately add, however: “but he never ties it into
the law of Moses.”? Never? In 1 Timothy 1:8-10, Paul clearly ties
the restraint of evil men to “the law” which would have been the
well-known law of Moses (the subject of the disputes to which
Paul there alludes). In Romans 13:4,9-10, Paul defined the evil
which the magistrate is charged to punish with violations of “the
law,” where it cannot be denied that Paul quotes from the Mosaic
code.

Jim Bibza likewise argues that Paul nowhere “tries to apply
the social and political laws of Israel! directly to the New Covenant
situation.”® What is strange about this, however, is that in his
paper, Bibza had just admitted that the principles prescribed by

5, God's Righteous Kingdom (Edinburgh: Banner of Truth Trust, 1980), p. 112,

6, Richard Lewis, personal essay on “The Applicability of the Penal Code of the
Old Testament” (privately produced and distributed for a study committee of che
Presbytery of Northern California, O.P.G,, 1982), p. 3.

7. H. Wayne House and Thomas Ice, Dominion Theology: Blessing or Curse? (Port-
land, Oregon: Multnomah Press, 1988), p. 135.

8. Jim Bibza, “An Evaluation of Theonomy” (privately published and distrib-
uted from Grove City College, Spring, 1982), p. 6.
