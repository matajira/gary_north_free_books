The Exegesis of Matthew 5 287

is, when Jesus declared “I did not come to disannul,” he meant “I
did not come primarily [comparatively] to disannul the law.” What
Long really wants to argue (to strengthen his case) is that the
words of Jesus, “I came not to disannul but to fulfill,” are a case
of relative negation. This is readily countenanced by Greek gram-
marians. The Greek construction “ou . . . alla” which is usually
translated by the formula “not A but B,” can sometimes be taken
in the sense of “not so much this, but that.”!? This formula is
sometimes used in Greek to tone down a first element in order to
bring into the picture an added element, thus having an ascensive
thrust — viz., “not only . . . but even more” (cf. Matt. 4:4; 21:21).
For example: “the one believing on me does aot believe (so much)
on me, but (even more) on the One who sent me” (John 12:44).
Now then, is this what Jesus was doing in Matthew 5:17?
Despite Long’s theological preconceptions which lead him to an-
swer yes, there are strong exegetical grounds for denying the idea
that Jesus was using relative negation in this verse. Ordinarily
“ou... alla” clauses communicate sharp contrast, rather than
relative negation. This is especially the case in Matthew’s writing
(see, e.g., 5:15; 7:21; 8:8; 9:12, 13, 24; 10:24; 13:21; 15:11; 16:12,
17, 23; 17:12; 18:30; 19:11; 20:23, 26, 28; 22:32). If Matthew was
using relative negative in Matthew 5:17, it is most certainly the
exception to his general rule! Furthermore, the accepted instances
of this linguistic phenomenon of relative negation all show a dis-
cernible pattern of paradox in their introductory clauses. The
dialectical pattern of communication is this: affirm A, deny A,
but (even more) assert B. The paradox of affirming and denying
A is resolved by the statement of B. Notice these examples. “Who-
ever receives me does not receive me, but (even more) the One
who sent me” (Mark 9:37; cf. John 12:44), “My teaching is not
mine, but (even more) His who sent me” (John 7:16). This is
precisely what we find in one undisputed instance of relative
negation in Matthew's gospel: “it will be given what you may
say, for you are not the ones speaking, but (even more) the Spirit
of your Father who speaks in you” (Matt. 10:19-20). What is

  

12. For instance, see Blass and DeBrunner, A Greek Grammar of the New Testament
(Chicago: University Press, 1961), p. 233.
