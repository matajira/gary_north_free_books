270 NO OTHER STANDARD

things. At the same time, the opponents of theonomic ethics have
unwittingly turned us over to the worst kind of tyranny imagin-
able — political power which is not restrained by any objective,
publicly accessible, written standard of transcendent justice by
which the state’s actions and prerogatives may be judged. Having
no inscripturated morality from God as our socio-political stan-
dard (or nothing specific enough to be helpful), the critics of
theonomy have no principled basis or guidance for a judicial
system by which to protect us from those who defy God (crimi-
nals) or those who wish to play God (the state).

The most persuasive refutation — and biggest indictment — of
those who have criticized theonomic ethics is not the detailed
rebuttals found in the preceding pages. It is that they have no other
standard to offer which can deal with the problems theonomy
addresses. My experience has been, to be honest, that the critics
show very little concern for those practical problems of men and
society; their lifestyles insulate them from criminal and political
injustice. And those who do show a concern for these practical
problems would, amazingly, prefer any other solution to them than
what is revealed in God’s holy Jaw. I hope the reader will step
away from the trees just long enough to get an overview of the
forest. If we are not supposed to be governed by God’s law, then
what other standard do the critics seriously set forth? 1 cannot see
that they have offered any credible, Biblical alternative whatso-
ever.

If Christianity makes a difference — and Christ the King does
indeed have an answer to this world’s concrete socio-political
problems — then the critics of theonomy must eschew reference
to the Old Testament law and, nevertheless, promote a system of
political and criminal justice which can be proven by Biblical
exegesis of the New Covenant scriptures. They have yet to do so.
T do not believe they can do so. Their alternatives end up being
warmed-over left-overs from their own (humanistic) political tra-
ditions and personal preconceptions. It would be more honorabie
simply to admit that Christianity has nothing specifically to say
to politics, privatizing the faith as a narrow and inward matter of
piety. I praise God that He is bigger than that — that the reign
