The Penal Code as an Instrument of the Covenant Community 245

here is that “the law” is taken in one sense (the one intended by
the author of Hebrews) as the Mosaic administration of the old
covenant order, but “the law” is taken in another sense in Johnson’s
argument against theonomy (viz. the moral stipulations revealed
by Moses). The exegetical problem is that Johnson has chosen to
take the Greek word efi (Heb. 7:11) in the sense of “upon the legal
basis of.” While efi may take the sense of “upon the basis,” as in
Hebrews 10:28, it is there used with the dative case; any importa-
tion of the specific sense of legal basis comes from the context (not
the construction). In no case of which I am aware in Hebrews
does epi with the genitive (as we find in the Heb. 7:11) take the
sense of “upon the basis.” God has not spoken to us “on the
[legal] basis of these last days” (Heb. 1:2), and God does not
write His law “upon the [legal] basis of their hearts” (8:10)! The
author of Hebrews did not believe that the moral stipulations of
Moses were legally predicated upon the Aaronic priesthood — or
even that the Mosaic administration of the covenant was legally
grounded upon that priesthood (whatever legal grounding could
mean in that statement). In Hebrews 7:11 he says, rather, that
the law was given “in association with” (or even “at the time of”)
the giving of the Aaronic priesthood; they coincided.

Once we correct the erroneous interpretation given to the
preposition in Hebrews 7:11 by Johnson’s argument, it is evident
that the question he poses in terms of it— “how sweeping is this
change of law?” (v. 12)—is not the open door to the possibility
of sweeping change that he anticipates. The change of law is a
change regarding precisely that priesthood which was instituted in
association with it. The author of Hebrews himself explicitly tells
us that the change of law about which he speaks is “the law of
fleshly requirement” — that priests come from the tribe of Levi
(Heb. 7:13-16). When Johnson goes on to note, quite correctly,
that there have also been changes in terms of sacrifices and cleans-
ing available in the law, he attempts — without evidence — to tie
them conceptually and/or logically to that particular “change of
law” referred to in Hebrews 7:11. This alleged entailment is
misleading since the “imperfection” of the Old Covenant sacrifices
