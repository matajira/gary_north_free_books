258 NO OTHER STANDARD

to match a much more urbane and cultured day such as ours is.
Bahnsen appropriately notes that in Heb. 2:2 ‘every violation and
disobedience received its just [or appropriate] punishment.’”
Theonomy teaches that only the Lawgiver has the prerogative to
modify or revoke His laws; Kaiser again says the same thing:
“only God could say which crimes might have their sanctions
ransomed.”

Therefore, it is important to note that what Kaiser is do-
ing—and the theological framework within which he is work-
ing — is just as theonomic in character as my own teaching and
publications. Furthermore, what Kaiser is arguing is not that there
has been a change in the penal sanctions from the Old to the New
Testament, but rather that even within the Old Testament itself the
law of God did not necessarily or absolutely require the death
penalty for all the crimes where that penalty is mentioned. Ac-
cordingly, what Kaiser is championing today is simply the same
thing that (he believes) the Old Testament law taught for the Old
Testament. This is very significant because it demonstrates that
Kaiser and theonomists, if they disagree after all, do not disagree
over the principles of theonomic ethics—but simply over the
interpretation of what the law originally meant. If the law taught
what Kaiser claims, then a theonomist is committed to advocating
what Kaiser’s interpretation teaches us as the moral standard for
civil governments in our day.®

The Key Verse

This, then, leaves us with the question of whether Dr. Kaiser
is correct in his interpretation of the Old Testament law. He
might be. I am not convinced as yet, however. According to
Kaiser, “the key verse in this discussion is Num. 35:31... .
Only in the case of premeditated murder did the text say that the
officials in Israel were forbidden to take a ‘ransom’ or ‘substitute.’
This has been widely interpreted to imply that in all the other

6. The reader should also not overlook the fact that Kaiser's interpretation does
not demonstrate that the death penalty is impermissible today in connection with the
crimes specified in the Mosaic law, but only that such a penalty in those cases is not
necessary. They might still be utilized under appropriate circumstances.
