PUBLISHER’S PREFACE
Gary North

In the spring term of 1973, Greg Bahnsen handed in his
Th.M. thesis to his committee at Westminster Theological Semi-
nary in Philadelphia: “The Theonomic Responsibility of the Civil
Magistrate.” The committee accepted it and awarded him his
degree that term. There was no controversy about it at the time.
No protests were filed, no letters sent to faculty members by
outraged presbyters, no protests of reviewers appeared. Westmin-
ster even awarded him a small stipend to go on to graduate
school.

Afier a delay of four years, due to circumstances beyond
Bahnsen’s control, the thesis, with certain additions, was pub-
lished as a book by Presbyterian & Reformed Publishing Gom-
pany: Theonomy in Christian Ethics. Two years after this, Bahnsen
was awarded his Ph.D. in philosophy by the University of South-
ern California.

Five years earlier, in 1974, his ordination to the teaching
eldership (ministry) in the Orthodox Presbyterian Church had
been blocked at the last minute — literally a few minutes before
the actual ordination — by the protest of an OPC ruling elder. It
took a year of procedural activity before his ordination was con-
firmed.

What had caused such intense hostility? It was the ethical
position presented by Bahnsen publicly and defended exegetically
in Theonomy in Christian Ethics, although the book had yet to appear
in print when the elder’s attack was launched. A thesis that had
raised no public protest on campus subsequently raised blood
pressures all over the Reformed world, and even beyond that

ix
