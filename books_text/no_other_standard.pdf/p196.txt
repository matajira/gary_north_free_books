180 NO OTHER STANDARD

asserting that “the word spoken through angels” — that is, the
Mosaic law (cf. Deut. 33:1f£; Ps. 68:17; Acts 7:53; Gal. 3:19) — was
steadfast. The Greek word for this atiribute (bebaios) and its cog-
nates is used both in Biblical and secular literature of the period
for something which does not lapse, which is permanent, which
has secure validity; one ought not to challenge the binding charac-
ter of something which is debates. It is firm and legally guaranteed
(see Moulton & Milligan, and Arndt & Gingrich). The word
connotes the surety of God’s word in the very next verse of
Hebrews (2:3), as well as in Romans 4:16; 15:8; 2 Peter 1:19;
Philippians 1:7; and Hebrews 6:16 (cf. 9:17). The Mosaic law,
according to Hebrews 2:2 then, has a firm and legally guaranteed
character; it is steadfast and permanent. Interestingly, the of
fender spoken of in Hebrews 10:28 is one who “sets aside” the law
of Moses. The Greek word (atheteo, and cognates) speaks of remov-
ing something by annulment, attempting to thwart the validity
of something, or nullifying it—for instance, invalidating a will
(Gal 3:15), breaking a pledge (I Tim. 5:12), or setting aside the
commandment of God by following a contrary tradition (Mark
7:9). While God may annul His particular commandment (Heb.
7:18), men are condemned for treating God's laws as invalid by
breaking them (Ezek. 22:26, LX X). Hebrews 10:28 is something
of a threat to those who would not recognize or keep the laws of
Moses, particularly (in this instance) those laws whose violation
brought the death penalty, The two verses to which Neilson has
gone to show that certain penal sanctions in the Mosaic law have
been repealed begins, therefore, by asserting an entirely contrary
thought - that the law is legally guaranteed or steadfast in its
validity, and that setting aside the Mosaic law or treating it as
nullified is a dangerous thing. From such a platform it is not likely
that the passages will proceed to repeal the law’s provisions!

Has the New Testament Revoked Deuteronomy 18:19?

Neilson also appeals to Acts 3:22-23 and its treatment of
Deuteronomy 18:15-19 as supporting the repeal of the Old Testa-
ment civil sanction for a particular “religious” offense. His conclu-
sion, “that God has particularly reserved for himself the dealing
