Appendix B
POYTHRESS AS A THEONOMIST

After I had sent the manuscript for this book to the printer,
a new book relevant to theonomy appeared in publication, about
which it would be appropriate for me to append a brief response.
I enjoyed and learned much from reading The Shadow of Christ in
the Law of Moses! by my friend and former seminary classmate,
Vern Poythress. I appreciate the kind and thoughtful spirit of his
book, as well as many of its insights. At various times over the
years he has addressed the subject of theonomy, and the transition
from his initial antagonism to virtual agreement with its essential
points is welcome. Since his book was not written in order to refide
theonomy, it would not really require an “answer” in the present
text. Nevertheless, I hope that he will appreciate my efforts here
to sharpen his thinking as much as I have appreciated his own
contribution to the discussion.

1. Fundamental Agreement

Poythress writes: “I affirm what is often regarded as the es-
sence of the theonomic view, Le., the abiding value of the law”
(p. 335). He affirms that “a good many deep, Biblically rooted
concerns make up the common core of theonomy” (p. 312). He
states that he “can affirm virtually all of Bahnsen’s ten main
theses concerning theonomy” which are used by me to summarize
the position (p. 343; cf. above pp. 11-13).

Poythress is right on target when he says, “Many of us reject
theonomy because we think that the Mosaic law is harsh. But the

1, Brentwood, Tennessee: Wolgemuth & Hyatt, Publishers, 1991.

291
