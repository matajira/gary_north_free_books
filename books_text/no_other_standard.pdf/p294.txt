278 NO OTHER STANDARD

note the available senses of the Greek term and choose that transla-
tion which is the best functional equivalent (indicated by the
syntactical use of the term and the English counterparts}.

The distinction between common words for “confirm” and
“fulfill” which Fowler points to in Hebrew and Greek is utterly
irrelevant, for this does nothing to preclude the use of one word
as a precising definition of the other in suitable contexts. In every
language there is more than one way to express the same thing,
and any linguist knows that the combinations and distributions
of senses for verbal tokens do not have a one-for-one correspon-
dence or parallel between different languages. Consequently,
Fowler’s observation that one of the Hebrew words for “confirm”
is never rendered in the (Greck) Septuagint by the Greek word
“fulfill” is greatly beside the point. It is all the more irrelevant,
since Fowler must eventually admit that linguistic authorities and
translators recognize that a common Hebrew equivalent for the
Greek word plaroa appears in the Old Testament with the obvious
sense of “to confirm.”

What other reasons can Fowler offer, then, for rejecting the
linguistic argument set forth in Theonomy that plarosai has the
precise sense of “to confirm in full measure”? Well, Fowler demurs
at the suggestion that a/la implies cxact opposites — contrary to
the testimony of Greek grammarians. He offers counter-instances,
all of which bear testimony to a real failure of understanding on
his part. For instance, Fowler thinks that the exact-opposite un-
derstanding of alfa conflicts with Matthew 6:13 (“Lead us not into
temptation, dui deliver us from evil”) because it would allegedly
require the verse to be translated: “Lead us not into temptation,
but mislead us from evil.” What this reveals, however, is simply
that Dr, Fowler confuses concepts with words. The opposition
expressed in a statement utilizing alla is an opposition in concepts,
not necessarily in single words (much less the main verbs)! Deliv-
ering us from the evil one is indeed the exact opposite (in concep-
tion) to leading us into temptation. This verse in reality substanti-
ates the point made by Greek grammarians about the adversative
alla! Fowler’s critique of my method of determining the precise
sense for “fulfill” in Matthew 5:17 has simply not found any
validity.
