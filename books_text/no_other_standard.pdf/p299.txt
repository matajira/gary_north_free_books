The Exegesis of Matthew 5 283

logue. Nevertheless, his treatment of the Matthew passage does
suffer from certain exegetical and logical errors of which we must
take account, and they are of such a nature as to deprive his
criticism of the theonomic understanding of force.? Fundamen-
tally, what separates Long from theonomists is that he presumes
abrogation, while they presume continuing validity, between the
Mosaic law and Christ. Long’s handling of Matthew 5:17-19
shows how frustrating it can be to try to maintain the presump-
tion of discontinuity with the law. Honest scrutiny will indicate
that even Long cannot support such a discontinuity exegetically
from Matthew 5:17-19, but must turn to theological convictions
he believes to be based on other passages outside of the one in
question here.’

It is Long’s opinion that we are “not under law . . . as a
tule,” and that the covenant law of Moses “is not applicable
today.” He asserts that we are under obligation to the Old Testa-
ment only to the degree that it has been repeated (“only so much
of it as has been taken up and incorporated into Christianity”).
However in the actual text of Matthew 5:17-19 Jesus said that
because the Old Testament law was not abrogated (Matthew
5:17) and will remain valid as long as heaven and earth stand (v.
18}, whoever teaches men the breaking of even the least com-
mandment will be least in the kingdom of heaven (v. 19). This
certainly appears to teach the fundamental principle of theonomic

7 From a logical standpoint, one could prove any conclusion he wishes from
Long’s premises since he contradicts himself chroughout the paper. On the one hand,
he teaches that Christ came to disannu] the Mosaic Covenant’s law (manuscript pp.
11, 17, 19, 24, 25, 27, 29, 33) — and yet elsewhere he teaches that Christ “in no way”
disannulled God's will from the Old Testament (pp. 7, 10). On the one hand, he
teaches that some Old Testament commandmenis have been realized or accom-
plished and are now dated (pp. 34-35) —even though elsewhere he says that “not
one” of the least Old Testament commandments can be violated without penalty {p.
36), On the ane band he says the whole Mosaic law as covenant law has been
abolished and passed away, not being applicable or binding any longer (pp. 14, 15,
16, 22, 28, 32, 38, 43) — but elsewhere he says that Christ came to apply the
unchanging standard of righteousness found in the Old Testament commandments,
that aspect of them which is eternally binding (pp. 4, 7, 10, 22, 25-26, 35, 39, 46).

8. At one point even Long has to candidly admit: “This distinction is implied
but not expressly stated in Matthew 5:17, 19” (manuscript p. 22). He does not
demonstrate that it is even implied, in my view.
