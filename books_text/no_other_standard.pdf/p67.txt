Spurious Targets and Misguided Arrows 51

to be more heavenly-minded, interested in the church rather than
the world.

Despite the mtensc rhetoric that often attends this kind of
criticism, readers must realize that the critics cannot be taken at
face value. In most cases, these same writers will turn around
elsewhere and admit that, well yes, Christians cannot turn away
completely from this world but must live responsibly and righte-
ously with respect to cultural affairs too — thus reintroducing the
relevance of theonomic ethics “through the back door” (as it
were). It is hard to avoid the New Testament witness that holiness
is supposed to characterize not only personal and ecclesiastical
aspects of life, but rather “all manner of living” (1 Pet. 1:15), that
God’s glory is to be pursued not only in home and church but
also in “whatsoever you do” (1 Cor. 10:31). Christ calls His
followers to be the salt “of the earth,” not merely in the church!
In the end, the critics of theonomy do not renounce any and all
Christian involvement in social affairs and political reform after
all. At best, their complaint is with the “wrong emphasis” found
in theonomic ethics, and at best this complaint is slippery and
poorly conceived. We may readily grant that socio-political recon-
struction has less urgency than personal spirituality or the church,
but this does not bear whatsoever upon the truth or error of the
theonomic standard for politics.

Theonomists do not, as theonomists anyway, diminish, under-
value or obscure the surpassing importance of personal salvation,
a pious walk with God, and the life of the church. We would not
for a moment suggest that the New Testament message of the
accomplishment and application of redemption to God’s people
by Jesus Christ — with a view to the individual’s standing before
God and his eternal destiny — is of secondary importance or merely
a means for getting to what is “really” important, namely social
transformation, We cry out with Paul: “God forbid that I should
glory save in the cross of our Lord Jesus Christ, by whom the
world is crucified unto me and I unto the world” (Galatians 6:14).

This-worldly Criticisms
The thesis of theonomic ethics is not logically tied to any
