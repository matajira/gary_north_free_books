262 NO OTHER STANDARD

the Old Testament. . . . We can be grateful to thconomy for
forcing the church to take these issues seriously.”

Misconceptions About Theonomy

Longman appears to think that his differences with theonomic
penclogy are bigger than they really are because he entertains
misperceptions of the outlook and attitude of theonomists. For
instance, he repeatedly says that theonomists are loathe to admit
any kind of “subjectivity” whatsoever in the process of using
God’s law in the punishment of criminals. This just is not so, but
is simply the author's subjective impression. Longman thinks
theonomists are unwilling to consider “the mentally deficient” or
“minors” as ineligible for the death penalty for any capital crime.
But I have taught those very exceptions for years (in carefully
defined cases: e.g., incompetence due to organic brain disorders,
the very young who should have been governcd by their parents).
Longman suggests that perhaps the ius éalionis is “not mandating
in every case” the penalty of death, but teaching the maximum
penalty allowed. I am open to that possibility (in some, not all,
cases of the death penalty), provided it is supported with sound
reasoning and competent exegesis. (That has yct to be donc,
though.)

Longman believes that theonomists fecl that the application
of the Old Testament penal sanctions today is an easy and simple
matter, not difficult at all—just a matter of looking up answers
in a book {as it were). Those who know me and my teaching
know better; I have never considered this an uncomplicated and
simple matter. Far from being “unwilling to admit” the difficulty
(as Longman paints me), I quite freely do so. (What disturbs me,
of course, are those who insist it is an impossible thing to do or
something we morally ought not even try to do.) According to
Longman, in reading theonomic works “one can easily get the
impression” that we believe the application of the penal code is
simple and clear-cut. In 1990 Dr. Gary North published a detailed
study of the case laws of Exodus 21-23, entitled Tools of Dominion.
It ran over 1200 pages! In the same year he published a related
book entitled Victim’s Rights. This hardly gives the impression that
