172 NO OTHER STANDARD

justification for the discrimination which he follows. Without such
a rationale uniting the various particulars into an interrelated
collection, setting that collection apart in principle from the re-
maining Old Testament laws which have an abiding validity, the
list of these particular laws would be admittedly arbitrary in
make-up, in which case nothing logically follows from what might
be true about one member of the set to what is true about other
members of the set—just as nothing follows from what may be
true of the set as a whole to what might be said about other laws
in the Old Testament. Neilson has simply stipulated by fiat what
he will personally denote by the expression “religious” crime.

The very attempt to create a special category of “religious”
crimes is predicated upon a false antithesis. All offenses, including
crimes, are religious in character. Murder and rape are offenses
against the image of God, repudiate God’s revealed authority, and
indicate arrogant self-deification. Neilson recognizes this. The sug-
gestion that the “religious” category of sins is comprised of of
fenses “relating to the divinely revealed means of worshiping the
true God” is also of little utility. Any transgression of God’s law
is a failure to worship and serve God in the way which He
demands, for ail of life is concerned with the glorifying and serving
of God (cf. Rom. 1:21; 12:1-2; e.g., James 1:26-27). Nor does the
common conception of religious worship (as concerned with sa-
cred ritual or practices “directly” related to a supernatural deity)
help in distinguishing sins taken as “religious” from sins which
are not, for Neilson excludes from the category matters such as
Sabbath-keeping and cursing God’s name, which —in the com-
mon conception — would surely count as “religious” sins! We simply
cannot avoid the stipulative character of what Neilson calls “relig-
ious” crimes that should no longer be punished by the civil magis-
trate.

Attempts to define a subset of Biblically delineated crimes as
“religious” — as well as further attempts to qualify some offenses
as “more,” “directly,” or “primarily” religious in contrast to oth-
ers — suffer from a lack of textual control. They are ultimately
fueled by subjective considerations and perceptions. Neilson and
others should stop to consider the way in which the secular mind-
