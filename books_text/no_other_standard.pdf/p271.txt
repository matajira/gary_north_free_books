Flexibility Regarding the Penal Code 255

Testament.? The answer is that any crucial inadequacies in the
theonomic position (apart from occasional expressions of it) have
not yet been pointed out or demonstrated. Theonomic ethics does
indeed see the distinction between the Decalogue (summary and
apodictic) and the judicial laws (case laws).

The report goes on to observe God’s accommodation to sinful
man’s situation as reflected in His law, and to illustrate Biblical
flexibility in the application of the civil law. Theonomic ethics
agrees that God’s law has been accommodated to sinful man’s
situation — perfectly and permanently accommodated to what the
sinner needs in his fallen condition. Until the effects of the fall
have been finally eradicated, there will always be a need for the
justice embodied in God’s precepts. The Old Testament illustra-
tions of flexibility which are offered in the report turn out to be
contextual interpretations, applications, or qualifications of the
laws revealed by God. Since theonomists insist on understanding
the law’s requirements according to all that Scripture dictates and
qualifics with respect to them, such illustrations pose no difficulty
but are quite welcome (provided the detailed exegesis and herme-
neutics are unobjectionable}.

When the report goes on to speak of flexibility with respect
to the civil law in the New Testament, however, it moves from
(Biblically defined) interpretation of the law’s requirements to
alleged elimination or removal of the law’s requirements. This is
altogether another thing. The report says of Christ that “the basic
tenor or mood of His ministry is certainly not characterized by
pressing the details of the civil Jaw.” Such reasoning and the
non-theonomic inferences often based upon it are logically falla-
cious. The fact is that “the tenor” of Jesus’ earthly ministry is not
characterized by a repudiation of the civil statutes of the Old Testa-
ment. At the explicit and relevant points, he supports them. The
examples offered of His alleged departure from them turn out to
be spurious. Missionary endeavors do not require bodily destruc-
tion of unbelievers, and thus the silence of Jesus about destroying
an apostate Samaritan village (Luke 9:51-56) is not at all contrary

3. “Report of the Special Committee to Study ‘Theonomy’,” for Evangel Presby-
tery, PC.A. (submitted June 12, 1979, at Gadsden, Alabama), p. 9.
