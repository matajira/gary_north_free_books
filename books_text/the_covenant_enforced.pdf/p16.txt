xvi The Covenant Enforced

They will not know it through any persuasion of mind or
through any such true understanding of it as we ought to have,
But Moses says that they shall have it proved to their faces; as for
example, we see the wicked grind their teeth when they behold the
faithful prospering, and when they see that God upholds and keeps
them. And how does this come about? Truly they will be aston-
ished at it, and they will not be able to think otherwise but that
God does indeed favor their adversaries — not that they take it to
heart or have a proper attitude about it, but in that they are at
least confounded in their own selves. [Below, pp. 117-18,]

Can Such Things Really Be?

Men who receive the blessings of God, even faithful men, will
have doubts about the relationship between obedience and histori-
cal blessings. Calvin recognized this fact of life and warned against
it. Unfortunately, his warning has not been taken seriously by
those who profess to be his disciples today.

Now Moses repeats again what he had said concerning the
fruit of the womb, of cattle, and of the earth. Surely it would have
been sufficient to have promised once that all bodily blessings
come from God. But on the one hand we see the mistrust that is
in men, how when God speaks to them, they ceaselessly argue and
reply, saying, “Yes, but can I be sure of it?” And therefore to give
us better resolve, God confirms the matter He had previously
spoken of. Again we see our unthankfulness to be such that we
attribute things to “Fortune” or to our own skill and craft, which
are actually done for us by God. Therefore He calls us to Himself,
and shows that it is He who does it.

And on the other hand, He would have us to understand that
if we intend to prosper in all points, we must hearken to Him and
obey Him. For all men, yea even the most wicked in the world,
desire to have issues of their own bodies, increase of cattle, and
great revenues. But what? In the meanwhile we despise God, the
author of all goodness, and seem as though we labored purpose-
fully to thrust His hand far from us, which is as much as if I
should ask a man for an alm and then reach up and box his ear,
or as if he should come to my aid and I should spit in his face;
even so deal we with our God. [Below, p. 119.]
