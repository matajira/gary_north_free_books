Sermon 154: Blessing and Affliction 107

His voice. For as I have been saying, since we are His children,
can we think that He takes pleasure in vexing us? Moreover, it
costs Him nothing. No matter how liberal He is toward us, yet
He is not afraid of having less for Himself as a result, or that He
will feel any lack. For His is a fountain that can never be drawn
dry.

Let us therefore be persuaded that our lives will always be
accursed unless we return to this point whereto Moses leads us,
namely to hearken to the voice of our God, to be thereby moved
and continually confirmed in the fact that He cares for our salva-
tion, and not only for the eternal salvation of our persons, but also
for the maintenance of our state in this earthly life, to make us
taste at present of His love and goodness in such a way as may
content and suffice us, waiting till we may have our fill thereof
and behold face to face that which we are now constrained to look
upon as it were through a glass and in the dark (1 Cor. 13:12).
That is one more thing we ought to remember from this text,
where it is said that we will be blessed if we hearken to the voice
of the Lord our God. .

Specific Blessings of God

This is to be applied to all parts of our lives. For example,
when a man wishes to prosper in his own person — that is, he
desires to employ himself in the service of God and to obtain
some grace so that he may not be unprofitable in this life but that
God may be honored by him—let him think thus to himself:
“Lord, I am Yours. Dispose of me as You will. Here I am, ready
to obey You.” This is the place at which we must begin if we
desire God to guide us and create in us the disposition to serve
Him, so that His blessings may appear and lighten upon us and
upon our persons. So it is concerning every man’s household.

When a man desires to live in peace and concord with his
wife, or to have children in whom to rejoice, let him understand
that all this is in the hand of God, and that it does not lie in our
power or skill to order our households after our heart’s desires.
For they who think they can achieve it by their own power are
