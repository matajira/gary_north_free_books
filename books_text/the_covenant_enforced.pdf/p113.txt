Sermon 152: The Curse and Justification 75

the other side we must trust in the grace Jesus Christ offers us.
For if we should become careless because God has forgiven our
sins, what a thing that would be! We would wind up in the same
mess men get into when they think to render only partial obedi-
ence to God. Now then, it is needful for us to be sorry for all our
sins, and if we detect any vice in ourselves, we must not permit
it to reign.

It is true that we come short. I say not in one part only, but
in all. There is no point in the law wherein we do not fail. That
man who thinks himself free of envy and to be a despiser of
worldly goods, surely he still has some other affection in his heart
that holds him back in the world. He that is chaste and honest
in his body, stili has some vanities that will carry him away. He
who does not foster any hatred or rancor in his heart, is not so
clear of all wicked affection that he lives as perfectly as he ought.
In brief, we shall be found guilty, not in one or two points only
{but in all], so that there is not any part or piece of the law from
which God might not condemn us.

And therefore, as I said, seeing we perceive such imperfection
in ourselves, we must not stand in our own conceits, but mourn
before God. And having mourned, we must endeavor to give
ourselves over to Him, praying Him to increase in us the power
and grace of His Holy Spirit, that we may manfully fight against
our sins so as to subdue them, overcome them, and get the upper
hand of them, to triumph over them once we are clean rid of
them. Thus you see how we ought to proceed in this affair.

And this ought to make the faithful rejoice, that although they
perceive their own imperfections, yet they must not cease on that
account to embrace God’s promises with gladness, assuring them-
selves that they will not be disappointed. And why? Because they
enjoy all those things in our Lord Jesus Christ, by whom and by
whose means the curse that was due to them is done away. You
see then that on the one side it is needful for the faithful to be
cast down utterly, and yet on the other side that they be lifted
up again in our Lord Jesus Christ, because they know that if they
look for what they do not have in themselves, they will find it if
