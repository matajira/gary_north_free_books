202 The Covenant Enforced

Tt is true, as we have noted before, that at first sight it cannot
be easily discerned who they are whom God loves and who they
are whom He hates, because as Solomon says (Eccl. 9:2), both
good and evil are common to all. He that serves God sometimes
prospers and oftentimes is afflicted. And the same is true of the
despiser of God. This is why he says that men become hard-
hearted, for it seems to them that they win nothing by serving
God, or rather that it is but lost labor. Moreover, we often see
that the children of God are sometimes handled more roughly
than the most wicked in the world. From this the carnal-minded
gather that it is much better to despise God. David confessed (Ps.
73:2) that he staggered as if drunk when he beheld the course of
things to be such that the good, and those who endeavor to walk
in all manner of integrity, are constrained to drink the water of
trouble, to eat the bread of heaviness, and to moisten themselves
with tears; but that meanwhile the wicked, who do not cease to
do evil, live at their ease and in pleasure, whereby it should seem
that God loves them. And what kind of dealing would that be?
But our Lord declares in this place that in the end He shall make
it apparent to them that are corrected by His hand that their sins
are the reason why they suffer pain, even if it is not quickly
perceived,

We have seen already among the other curses that Moses
pronounces that they who cast off the word of God are constrained
to borrow and to be always in need, but that the others [the
righteous] who lend to them, have the wherewithal to help them-
selves. But we see how all the children of God fall into need, and
do not find any that will comfort them. They make many turns
before they meet with a man that will use gentleness toward them,
and this seems clean contrary to the word spoken by Moses. But
yet God is exercising His people after this fashion, and in that
respect it is said that when we are afflicted, whether it be with
poverty or sickness or in any other way, we must not fail to enter
into account with God; that is, to examine our lives and to see
whether we have not committed many faults. And then will every
one of us find himself at fault.
