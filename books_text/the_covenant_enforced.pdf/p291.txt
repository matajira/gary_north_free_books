Sermon 162: A Plethora of Plagues 253

the rest of the land of Canaan, He says that we must look unto a
rest that is higher (Heb. 4:9; 11:16). For the children of God are
commonly like wayfarers in this world as St. Paul terms them,
putting himself in the number (2 Cor. 5:6); but let us evermore
direct our course and endeavor to this heritage that is promised
us in heaven, for we shall not be disappointed of that heritage if
we continue in our calling whereunto God has called us. Neverthe-
less, it is said that they who give themselves over unto wickedness
shall be wiped out of the Book of Life (Ex. 32:33). Not that God’s
chosen will ever perish; but it is spoken with respect to such folk
as think it enough for them to bear the name of God’s children,
and to have been baptized, and to receive the Lord’s Supper, and
such like things, and make no further regard. But our Lord tells
us that although it seems for a time that they are written and
enrolled in His register, because they are taken to be of the
company of the faithful, yet will they not fail to be wiped out.

The Greatest Privilege

So then let us advise ourselves, and as long as it pleases God
to keep us in this world, let us walk under His obedience in such
a way that His hand may ever be stretched out to preserve and
maintain us. Let us so stick to Him that we may have freedom
to honor and serve Him. For it is a dreadful desolation that Moses
here speaks of, namely to be no longer of the body of the Church
or to have any longer the appearance or shape of religion; and yet
this is seen throughout the world at this day.

Therefore, let us mark that one of the greatest benefits God
can bestow upon us in this transitory life is to let us have some
little corner in which to assemble ourselves in His name, to call
upon Him, and to profess ourselves to be His people. This ought
to be esteemed among us more than meat or drink or anything
else. Yet very few do think upon it. Notwithstanding, however,
we must confess that even though we have sufficient food, yet if
we forget God meanwhile, then our state is most miserable. And
if we do not think on this, we betray our own beastliness. For God
tells us that the benefit that is spoken of here is far greater than
