140 The Covenant Enforced

we must acknowledge the hand that smites us, for in that is great
wisdom. And for the same reason God complains thus by His
prophet, “This people has not regarded the hand that smote
them” (Is. 9:13).

You see then how we must understand that all the afflictions
and miseries we endure in this world are indeed strokes from
God’s own hand. And along these lines it is said by the prophet
Amos, “Is there any, evil in the city that God has not done?”
(Amos 3:6). That is to say, “Can there happen either war or
pestilence or famine or disease or poverty or any other calamity
whatsoever, that does not come to you from God? Wretched
people, are you so foolish and beastish as to imagine that God,
who created the world, has left it at random and has no care to
watch over His creatures, or to bestow on them what He thinks
fitting for them? Does He not sometimes show His goodness and
sometimes make them feel Him as judge, punishing the sins of
men, and making men know what His office is? Do you think
that He lives idle in heaven, and that He does not set forth His
power, or that the world is not guided and governed by His
providence?”

So then just as God earlier has showed that we cannot prosper
except by His grace and love, which He extends to us, in that
He has chosen us as His children and will also accept our service
{Dt. 8:18; and earlier sermons on Dt. 28), so now He shows in the
same way that if there is any affliction, poverty, or other misery,
they come not by chance, but are the very punishments of God,
sent by Him. And therefore when things do not fall out after our
liking, we must fall to considering and examining our sins. If we
are grieved in any manner of thing, so that one is troubled with
his household, another with the loss of his goods, another with
some disease, another with some vexation of mind, and another
with the loss of something he loved, let us acknowledge it, saying
“Lo, it is our God who has lifted up His hand, and holds it up
still. And why? Because we have offended Him.” The first point,
then, is that men may not deceive themselves when God visits
them, but they must know that by this means they are made to
