Sermon 156: God’s Threats 141

understand their offenses, to the end that they might humble
themselves and bewail their sins. So much for one point.

No Escape From God

The second point is that we must not think we can escape the
scourges of God, no matter how sly we are. We shall always be
caught by the back of the neck if God is against us. And if we
make shields and ramparts, and do whatever we can, yet our
Lord will not fail to find us. He only needs to blow upon our
defenses by which we imagine we can protect ourselves from
Him. It is not like dealing with mere creatures; against such we
might fortify ourselves both behind and before. But God will slap
us down from heaven. We can erect neither shield nor rampart
against Him, hoping thereby to stop His hand from touching us
when it pleases Him to punish us.

Again, what is the end of all the fortifications that we are able
to build against Him, except the overthrow of ourselves? Let us
therefore understand that anything men try to do against God
will simply turn to their own confusion and overthrow. That is
what Moses meant when he said that we shall be cursed in the
town and in the field, in our going forth and in our coming in, if
we do not obey the voice of our God. And just as he earlier said
that God will open His good treasure from heaven to give us rain
in due season, so now he says that God will make our heaven as
brass, and our earth as iron, and that instead of rain He will send
frost; He will send us dust and ashes, and there will be nothing
but barrenness among us.

Here we must cail again to mind the lesson that has been laid
out briefly before, which is that just as every one of us is visited
by the hand of God, we should benefit ourselves thereby and every
man should apply to his own use what is here mentioned. For
God uses many ways to chastise us. One is punished in his own
person with diseases, reproach, and I know not what else; another
has some secret heartache, so that though he is whole in body,
he is continually in torment in mind; another is plagued with his
wife or with his children; and another is troubled with his sub-
