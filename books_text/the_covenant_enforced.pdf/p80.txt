42 The Covenant Enforced

stoned to death, and so such evil was to be taken away. For it is
an unkindly and accursed thing that children should set them-
selves against those who brought them into the world, and have
brought them up, and have taken such pain and care for them.
For we know that a father occupies the place, as it were, of God
toward his own children and offspring. It follows then that he
who lifts himself up against his father or his mother is manifestly
despising God, even as if he were a despiser of all religion. Now
here, by the way, God tells us that although judges and magis-
trates may not do their duty [of executing the rebellious youth],
or if perchance the wrong done to his father and mother is borne
with, yet he has not therefore escaped. Many crimes are buried
in this world, but God reserves His judgment even so, and sooner
or later they must come to account.

Let us mark well, then, that there is no more speaking here
of the execution of justice by means of law [lawcourts], for that
has been spoken of before. Here God declares that those who
have offended through disobedience, even though they are not
punished in this world, and even though their faults are not
known, so that no examination is made of them such as they
deserve, have still gained nothing thereby because there is a heav-
enly Judge, who forgets nothing but keeps all things registered
before Himself, and in the end He shall perform His office. There-
fore, let us think well concerning this teaching, and even if men
find no fault with us, and no man vexes or troubles us, let us not
fall asleep for that reason. Rather, let-every man summon himself,
according to what is told us here, and consider that we must
come before the judgment seat of God.

And therefore let us learn to walk in such a way that He may
accept us when we come there, and so that we do not stand in
fear of the curse uttered here. Not that any of us can perform the
law (as I have declared more fully in earlier sermons), but we are
obliged to tend toward it, and to put our endeavor into it. For
even though we are not completely clean before God, but are on
the contrary guilty of many of the faults that are contained here,
yet He does not lay them to our charge, if we hate them and do
