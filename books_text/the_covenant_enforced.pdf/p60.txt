22 The Covenant Enforced

sacrifice, and second by the erection of a monument, so that it
might be known that this land was not purchased by the hand of
man, but was given by God to that people for an inheritance, But
now we have another commandment, by which God meant to
bind the people unto Himself in another manner. Indeed, He had
done so already, but because men are so difficult to root down,
and because they cannot be bound by too many bands and cords,
to hold them to obedience, God had ample reason to add what is
set down here, in order to keep them better under obedience.

The Usefulness of Blessings and Curses

We have already dealt with the fact that when God gave His
Jaw, it was a mutual covenant, and just as He bound Himself
unto the children of Israel to be their God, so also the people of
Israel bound themselves to be His people. Here, however, an
additional confirmation is given, to ratify that first bond the bet-
ter: God ordained that when they had passed over Jordan, the
people should divide themselves into two companies, and that six
tribes should stand upon Mt. Gerizim, and the other six should
stand upon Mt. Ebal over against them, so that the Ark of the
Covenant and the priests should stand in the midst; and that
these who were on Gerizim should bless, and those who were on
the side of Ebal should curse.

Now we shall look into the content of the curses and blessings
later on, as we deal with the chapter. Note at this point, though,
that God, in order to encourage the people, not only delivered
His will to them and said, “You shall walk thus,” but also added
to it, “You will not serve Me in vain, and your pains will not be
lost, for I will cause you to prosper, and it is for your own welfare
that I would have you to be subject unto Me. I seek after no
profit or advantage by this, but it is for your own benefit and ease
that you should cleave unto Me in keeping My commandments.”
Behold what blessings God gave, intending that the people should
serve Him with a willing mind and not through force or con-
straint. Again, because men are so stiff-necked that they cannot
stoop down without raising a ruckus, and because on the other
