198 The Covenant Enforced

in the light of it.

So then, the matter to which Moses now returns is this: that
having showed us how it is the Lord who withdraws all manner
of blessings from us, how it is He who curses our possessions and
the fruits of the earth, how it is He who sends vermin and storms
and tempests to destroy all, how it is He who gives power to our
enemies, he then shows why all this is done: It is because we have
rebelled against Him, because we have despised His law. That is
the very reason why these plagues of wrath do so pursue us.

Now then, let us bear in mind the teaching that has been
sufficiently set forth about this already, which is that when God
afflicts us, we must shut our mouths from replying or entering
into argument, for we shall win nothing by being contentious with
Him. No, rather we must condemn ourselves, confessing that He
deals justly. Even if it is His purpose to prove our patience and
to try us to the uttermost, yet all the same we must declare
ourselves guilty before Him, and understand that our sins deserve
to be thus roughly handled at His hands. Such an approach
would cause to cease all blasphemies, murmurings, and com-
plaints that are daily heard in the world. When any adversity or
affliction happens to us, it is so that we should think on the sins
we have committed. Mark that for one point.

And since mention is made of the commandments and stat-
utes that God has ordained, the same expressly and purposely
concerns us. For although the Papists have the law, yet it is buried
in their midst. The Jews, as St. Paul says (2 Cor. 3:15), have a
veil before their eyes. The Turks walk in their own ignorance, as
do all the rest of the heathen. Now our Lord enlightens us and
shows us the way. There is, therefore, a more villainous rebellion
in us when we do not do according to what we have been taught.
We deserve that God should use greater severity with us and
pour upon us the plagues of vengeance that are contained here.
Therefore, let us benefit ourselves by the things that are said here
concerning the commandments and statutes that God has or-
dained. And seeing it is the case that He speaks to us daily,
declaring to us His will so familiarly, let every one of us submit
