86 The Covenant Enforced

strengthen himself, and gather all our forces together, and not
think to set forth negligently to the service of our God. For when
we think to march one step forward, we shall go a hundred
backward if we are not very strong and courageous, holding
ourselves in awe and restraining ourselves, and gathering our wits
about us so that we bend our minds wholly thereto, and even, as
one might say, labor for breath in travelling about it.

And moreover, seeing we are laid about with ambushes on
every side and should be soon taken, so that we might fall into
the snares of Satan before we are aware, let us take heed and be
watchful. That is what we are warned about here. And would to
God that we would carry away this lesson with care. For then,
while we are now applying our powers and ail our endeavors to
things not only frivolous but altogether harmful, tending to none
other end than destruction, then every one of us would be vigilant
to walk in the obedience of God and to give himself wholly thereto.
And seeing that we are warned about it, there is no excuse. Shall
we then obey our God, and show that our affection is so inclined,
without deception? Let every one of us incline to what is given him
in commandment, namely to be careful, vigilant, and attentive.

Authority of Office

Now he adds, “to keep all the commandments that I [Moses]
set before you this day,” or that I ordain for you. Although Moses
is the speaker of this, yet he takes the authority and power to
command and to subdue men. This is not anything of his own
invention, as if to say that it ought to be received without gainsay-
ing because he said so. Rather it is from God, because God speaks
by his mouth, and because he himself faithfully delivers the teach-
ing that was committed to him. For that reason he speaks in such
a strong way, saying that they should keep his [Moses’] statutes,

Tt does not lie in the power of a mortal man to bind the
consciences of people. And yet that is the very thing that has
wholly corrupted the Church, in that men have usurped the office
of God in making laws and statutes for the spiritual government
of men’s souls. And it is a point of high treason against God for
