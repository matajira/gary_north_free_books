xxviii The Covenant Enforced

Denis Raguenier. It was learned that he “was a skilful short-hand
writer and that he was taking down Calvin’s sermons verbatim,
for his own spiritual profit.”? The community of French refugees
put up money to support him in this, so that he could devote
himself fully to it. Parker describes Raguenier’s labors: “Raguenier
had to be there in church with his pen and ink and paper, not
only twice on Sunday but also every morning of alternate weeks,
at six o-clock in summer, seven in winter, taking down every word
of Calvin’s sermons, ranging from about three thousand to above
six thousand words in length. He then had either to write them
out in longhand himself or dictate them to another scribe, keep
the sheets carefully in order until the series of sermons was com-
pleted and then get that set bound and delivered to the safekeep-
ing of the deacons.”* Raguenier died in 1560, but his work was
continued by men trained by him.

Parker comments on the accuracy of these transcriptions:
“That no little errors should be made would be asking too much
of men writing short-hand with a quill and ink in an unheated
church. Moreover, the transcripts were not checked by Calvin
himself. But the errors are remarkably few, and are nearly always
such as can be easily corrected by modern authors. Thus the
manuscripts can be taken as faithfully reproducing Calvin’s own
words.”>

Calvin began his Deuteronomy series on 20 March 1555,
immediately after a long series on Job, which had begun 26
February 1554. After Deuteronomy, he went on to Isaiah (July
1556 to September 1559). On Sundays he was preaching 1 and 2
Timothy and Titus, and then 1 Corinthians, while going through
Deuteronomy on weekdays. The fifteen sermons contained in the
present volume were delivered while Calvin was in the middle
chapters of 1 Corinthians on Sundays.6

3, Ibid.
4, bid, pM.
5, Ibid.

6. See T. H. L. Parker, The Oracles of God: An Introduction to the Preaching of John
Calvin (London: Lutterworth Press, 1945), p. 161.
