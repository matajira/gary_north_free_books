30 The Covenant Enforced

care He shows towards us? Yes indeed.

And therefore, being stirred up by the goodness and gentle-
ness of God, of which even now I speak, let us also wake ourselves
up with His threatenings. When we see that our flesh is wanton,
and that it draws us unto evil, let us say, “Alas, should we shake
off His yoke like wild beasts? What has God said?” Let us there-
fore tremble when we hear the threats of our God. For if the anger
of an earthly king is the messenger of death, as Solomon says
(Prov. 16:14), what ought we to think of the anger of God when
it is announced against us?

So then, let us learn to tame ourselves with fear. When the
temptations of Satan start to prevail over us, and our sins act as
baits to deceive us, let it come into our minds to say, “What?
Shall I under the delusion of some pleasure that will soon vanish,
go and provoke the anger of my God and so perish forever?” After
that manner, I say, we ought to call God’s threatenings to our
remembrance, and then answer “Amen” to them, saying, “In-
deed, Lord, it is even so; it is no children’s game. When You
pronounce condemnation upon the wicked, You are ready to
execute it, and when You have once pronounced the word with
Your mouth, it is all the same as if we saw the fire already kindled
to consume us.”

In this way, I say, we ought to receive all the threats God
utters against us, for that is the best means to teach us to observe
the law. I mean, as far as our weakness will allow it; for as ] have
told you it is not possible for us to come to total perfection as long
as we are enclosed in this flesh of ours. All the same, we may well
dedicate ourselves to God, and be held in His fear, if on the one
hand His promises are in force with us, and on the other we give
ear to His threatenings.

Instruction From God
Let us now look at the order here set out. Moses, together
with the priests of the tribe of Levi, commanded the people that
six tribes were to stand on Mt. Ebal, and six on Mt. Gerizim.
And afterwards He said, “Keep My statutes and commandments,
