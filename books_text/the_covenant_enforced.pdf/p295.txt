Sermon 163: Exodus Undone 257

receive us or any nook or corner where we may rest, our state is
then extremely miserable. That is why Moses now (after declaring
that God would drive away and banish the Jews out of the land
He had given them to inherit and to rest in forever) adds that
they shall be as vagabonds and have no certain dwelling place,
but be tossed from pillar to post.

How the Righteous Suffer

Now it is certain, as we have already said, that this often
happens to the faithful; but [first] it is for another purpose;
[second,] God comforts them in it; and [third,] it is fitting that
their patience should be tried in this manner. For our obedience
toward God is displayed when He mingles us among the wicked,
so that we do not appear outwardly to differ from them at all. All
goes to havoc, as they say, and yet notwithstanding we continue
sound in heart and commit ourselves into the hands of our God,
knowing that He has not forgotten us, though He allows us to be
so tossed.

On the other hand all despisers of the law bear their mark as
if they were burned with a searing iron in their hearts. They know
that God is against them and that their sins reprove them. Even
though men do not accuse them, yet they feel a sufficient testi-
mony in themselves to be confounded. And that is what Moses
goes on to say, that concerning those who are hard hearted against
God’s Word, God shall give them a trembling heart, a sorrowful
mind, and failing eyes, so that in the morning they shall say,
“Would that it were night.” And when night is come, “Alas how
shall I get through it? Can I continue until the morning?” Their
lives shall as it were hang before them, and they shall be in
miserable distress. That is the thing wherein the faithful chiefly
differ from the despisers of God, and where their state varies.

For although the faithful suffer much trouble in this world,
yet they have much with which to comfort themselves —just as
our Lord Jesus says in John 16:33, “You shall be vexed in this
world, but yet you shall not fail to have comfort in Me.” We
must consider what is the ground of our comfort; namely that
