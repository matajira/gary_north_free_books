Sermon 154: Blessing and Affliction iil

though they have tremendous advantage at the beginning, and
be full of craft and wiles, yet God will blind their eyes so that they
will cast themselves willfully into the snare and there be taken.
Again, even when they are armed with malice and boldness to set
upon us, God will in the end confound them, no man can tell
how. He will destroy all their devices and attempts, and when
they band themselves against us, and have great multitudes on
their sides, yet will God scatter them, Just as when we see a cloud
threatening rain, and it seems that all should be drowned, God
scatters it and the tempest is gone — even so will He deal with
our enemies. Thus you see in effect what we have to remember
concerning this text, where it is said that we shall be defended
against all such as set themselves against us, and that our God
will make them flee before our faces if we do Him the honor to
acknowledge him to be the Lord of hosts, and that His power is
infinite, and that therefore the whole world can never prevail
against Him in the least.

Conclusion

Finally, what have we now to do but submit ourselves to the
obedience of God? Also, let us understand that all those who do
not believe, and all those who despise God’s majesty, although for
a time their lives may seem happy, yet they are appointed of
perdition, and all their goods shall become a curse to them so
that they will be in a forlorn condition. Just as Psalm 69:22 says,
their very tables will be turned into snares and artifices wherewith
to trap them, and all the benefits of God will become deadly
poison to them. Here is what we must keep in mind.

Moreover, as often as we are afflicted, let us humble ourselves
and acknowledge our sins and bewail them before God. And in
the meantime let us not omit to qualify our griefs, knowing that
amidst the afflictions He sends us, there always appears a certain
testimony of His goodness, and that it is necessary that He should
so hold us in awe. Yet notwithstanding, we must not be cast down
or discouraged when we are vexed and troubled, though we are
encircled with never so many miseries. And why? Because God
