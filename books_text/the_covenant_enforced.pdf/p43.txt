Sermon 149: Altars and Ensigns 5

were in the person of God. And yet it would seem at first blush
that this sentence is not well framed: Moses and the elders say to
the people, “Do what I command you,” the subject of the sentence
being singular rather than plural. As I have mentioned before,
however, it was needful that the Jews should be taught that these
things proceed not from men but from God, who spoke through
their mouths. We see then that Moses and the elders are not
coming here in their own persons as attributing anything to their
own worthiness, nor do they attempt to stand on their own ground
to charge the people with any laws, but they stand as the instru-
ments of God to set forth faithfully whatever is committed to
them.

Now if Moses, who was preeminent among all the prophets
as we shall see (Dt. 34:10), nevertheless restrained himself with
such modesty that he would not usurp to himself the authority to
speak in his own name, what shall we say of those who govern
the Church nowadays? Do they claim to exceed Moses? Let us
note, then, that pastors are not appointed to set forth whatsoever
doctrine seems good to themselves, or to bring men’s souls into
subjection and bondage to them, or to make laws and articles of
faith at their own pleasure; but rather only to bring about the rule
of God, that His Word may be hearkened to. Let that be noted
for one point. We see, then, that ali the traditions of men existing
nowadays in Popedom in the place of the pure word of God, are
but vain things. They must all be beaten down, and the true
government of God must be established again in His Church. And
that government is that men hearken to Him, that they submit
themselves to Him, that both great and small receive what is
delivered in His name, and that men go no further. Let this be
well noted.

But at the same time we must also note that when those who
are appointed ministers of the Word of God perform their office
faithfully, then they may speak with masterly authority. And
indeed we hear how Moses with the rest of the elders says, “I
command you this day, keep my statutes.” It is not for a mortal
creature to advance himself so high. No. But because Moses
