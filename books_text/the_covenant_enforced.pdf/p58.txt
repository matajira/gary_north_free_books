20 The Covenant Enforced

He speaks to us by the mouth of the minister; and when we have
His Holy Scripture, let every man endeavor to be taught by it.
Those who have no skills to read themselves, let them hear it read,
that we may show that since our Lord speaks to us, we are ready
to receive whatever He says, and desire nothing else except to
profit under Him in such a way that His Word is not only
engraved in stone and lime but also imprinted on our hearts, so
that in our whole life we seek to follow it, and give ourselves
wholly to it

Prayer

Now let us kneel down in the presence of our good God, with
acknowledgement of the great number of faults and offenses that
we cease not to commit daily against His majesty, praying Him
to make us feel them better than we have, so that we may en-
deavor to amend them more and more until we are clean rid of
them; and since we obtain pardon for them by our Lord Jesus
Christ, we may also increase and be confirmed in all righteousness
and holiness, that so we may indeed confirm our calling. And let
us pray Him that since He has chosen us for His people, it may
please Him also to withdraw us from all the defilements of the
world, so that we may be to Him a holy people in the name of
our Lord Jesus Christ. And let us pray that it may please Him
to grant this grace not only to us but also to all people and nations
of the earth; etc.
