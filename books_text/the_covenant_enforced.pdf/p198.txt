160 The Covenant Enforced

jest and scoff at it, as it were in spite of God, and while He calls
them to humble themselves in sackcloth and ashes, they razz at
Him with their tongues and are so far from being ashamed of
their deeds that, even as it were in spite, they make a sport of
their own filthiness and miseries. We see, then, how perverse men
can be, seeing that they can make no better profit in God’s school,
I don’t mean in the school of His teaching, where He speaks to
them, but in the school of His smiting them with heavy strokes,
and raising up wars and troubles, which ought to make men
beside themselves with fear. All the same, Moses did not write
this in vain,

Let us, therefore, be the better advised, and when God sends
us any strange diseases, let us understand that our sins are multi-
plied, and that God must on that account be more moved than
He was before. For this reason, let it bring us to repentance, and
let us not double our iniquities; for in the end we shall find out
from experience what we have seen even now, namely that such
evil will stick with us, even in our marrow and bones, until we are
utterly consumed. Besides this, God has other means to punish
us, and when He perceives that the usual methods do not prevail,
He has other rods laid up in His storehouses, as it is said in the
Song [of Moses; Dt. 32:34].

Indeed, we shall see what it means to walk recklessly against
Him; namely that in the end He will overtake those perverse and
crooked persons, who pretend to be innocent fools and do nothing
but scoff and shake their heads when He has punished them one
way or another. Let us not, therefore, tarry until God must use
such extreme measures toward us, but being warned beforehand
by what is here mentioned, let us look to it and consider that
however many diseases He sends us, they are so many witnesses
He sends to prove that we have sinned against Him, that we
might think on it and turn to Him with all lowliness.

Forsaken of God

I said earlier that the very worst of all is what Moses adds
here at the end, that God shall smite us with all astonishment,
