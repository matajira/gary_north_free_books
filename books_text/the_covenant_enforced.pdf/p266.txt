228 The Covenant Enforced

thus we should take occasion to yield ourselves wholly to Him
when the things of this world fail us.

And by the way [secondly], although we have walls and
artillery, money and munitions, and all that we can wish for, yea
and people also, and all things else; yet let us understand that
we must meanwhile so use them that our Lord may be always
our trust, and we give Him always this honor: that it is His office
to defend us, and that we not wade in any deeper than this. It is
certain that we may serve our turn with the things that He puts
into our hands, but our hearts and minds must always be raised
up above all the world, and He alone must be our Sheet-anchor.?

Thirdly, we must not put our trust in the power of the help
that He gives us, for as they say, a thorn of a bramble is enough
to make us believe that we are invincible. “What?” we say, “Is it
possible that they should prevail against us?” All this is nothing;
not even the peel of an onion as they say.? Yet we have to strain
our wits in order to give it price and glory. Rather let us acknowl-
edge our weakness and increpitude, that we may repair to God,
And let nothing hinder us from yielding ourselves wholly unto
Him and unto His protection. Let us not deceive ourselves with
such vain confidence, as the Jews had in their high walls.

Human Shelters, Human Traps

Now with all this he also shows that whatever we may devise
for our own strength and security, it will be impotent in the face
of His power, and we shall continue to be besieged until we are
totally wasted. Indeed, we may gather from this text that it would
be much better for us to be delivered into the hands of our
enemies from the first, than to have the means to resist. [Shut up
in the siege] we are made so to languish that we are not permitted
a simple and clean death. The poor folk who are abroad in the

2. From the Oxfard English Dictionary: A large anchor, formerly always the largest
ofa ship’s anchors, used only in an emergency. Fig.: That on which one places one’s
reliance when everything clse has failed,

3. In other words, our human fortresses are as diaphanous as the outer, paper
like skin of an onion,
