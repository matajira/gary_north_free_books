268 The Covenant Enforced

consideration. As we have noted, we ought to prevent this judg-
ment by fearing God of our own accord. We must bear in mind
that we are all wretched sinners, and there is no one, regardless
of how well and right minded, who ought not to understand that
he is greatly threatened by God. But, it is also true that when we
have our Lord Jesus Christ, who is our peace, and when we can
by His means rest upon the fatherly goodness of God, then we can
be sure that He counts us as His children and that He watches
over us and procures our welfare. And that is why St. Paul says
expressly that when we are once justified by faith, then we are at
peace with God (Rom. 5:1).

He says that we must be justified by faith, which is to say
that we must have embraced the grace offered to us by the gospel,
knowing that God forgives us our sins and is merciful to us in
that He bears with us for our Lord Jesus Christ’s sake. Until we
come to this, we must always be in doubt and perplexity. But
once knowing that God has buried all our sins, so that we put our
trust in the death and passion of our Lord Jesus Christ, then we
are at peace — yea in peace and not in dullness like the wicked.
The unfaithful and despisers of God may have some peace for a
certain time, as we have said before, but since they forget them-
selves and follow continuously on their wicked track, they utterly
besot and bewitch themselves. But St. Paul says that we, acknowl-
edging ourselves undone without Him, may boldly press unto
Him and say: “Seeing God is my Father and He has shown
Himself so gracious to me in this present life as to grant me some
rest here, surely there is a more blissful rest prepared for me when
our Lord Jesus Christ will come to meet me, to guide me, and to
bring me up, that I may seek my God.” And that indeed is the
place we must come to, if we intend to have a place to rest
ourselves in.

The prophets speak with the same effect when, prophesying
the kingdom of our Lord Jesus Christ, they always say that every
man will then sleep under his own fig tree and under his own vine,
and nothing will make him afraid. The prophets were true ex-
pounders of Moses, and as the renewers of the law, bringing it to
