Author's Introduction 9

dow. The place that they go to prove that redemption is for them
is the same Bible that they want to call irrelevant. They can’t have
it both ways. If you want the hope of the Bible, you have to go
with the terms for having hope. If you want an answer that will get
you out of your problems, you wor’t find it in the answers of mod-
ern man; you'll have to go back to the timeless Book of the first
century. So, an “anything goes” answer is no answer, and that is
precisely what this group offers. No answer.
So where's the answer?

A Biblical Blueprint

The answer is in the Word of God, the Biblical blueprint for life,
and especially for divorce and remarriage. “But wait a minute,”
someone is sure to say. “Isn’t the first view above claiming to be
Biblical?” Yes, but only up to a point. God clearly hates divorce
(Malachi 2:16), but Jesus also clearly made an exception that
allowed for divorce when He said, “Whoever divorces his wife for
any reason except sexual immorality causes her to commit adul-
tery” (Matthew 5:32), So the first view is correct up to the point of
what Jesus said.

But what point is that? The point of clear thinking about what
the Bible is all about, the point of the covenant. Marriage, divorce,
and remarriage simply cannot be properly understood until the
Bible is seen as a document about covenant relationships: the
God-to-man covenant and the marriage covenant. The problem is
that people either fail to see the Bible as a Book about these cove-
nants, or they fail to see the ramifications of this covenantal view of
the Bible. When this happens, they will not be able to understand
how the Bible is a blueprint for living.

The Family Covenant

Second Chance offers answers, real answers to the tough ques-
tions because it approaches the issues of divorce and remarriage
from the point of view that marriage is a covenant. What is a cove-
nant? A Biblical covenant has five parts, as I have demonstrated
