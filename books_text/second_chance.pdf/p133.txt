New Covenant, New Spouse i

consequences to his sin, similar to the consequences of cutting off
the arm, Just as the arm will never grow back, certain sins create
consequences that cannot be changed. God has struck him down
with AIDS. That makes him 2 potential murderer. There is no
cure for AIDS. Until there is, and until medical science can dem-
onstrate it clearly, he is cursed in history, even if he has repented,
and even if he has paid the proper restitution. He too would be
able to enter a new covenant, but only after his previous offense is
dealt with, and this would mean his physical restoration as well as
his ethical restoration. A sign of this society’s covenantal restora-
tion will be the removal of AIDS from its midst, or the reduction
of its lethal nature, which probably will first involve the death of
all the present carriers.

Yes, there can be remarriage on the part of the guilty party in
other cases where he repents, pays restitution, and there are no
lasting consequences that would be destructive to the new spouse.
Redemption makes this possible. When God's original covenant
with Adam died, God required a payment for sin so that He could
make a new covenant with mankind. Biblical history illustrates
time and again that the Old Testament payment for sin was
unsuccessful until the Death and Resurrection of Jesus, meaning
that a true New Covenant was not able to be formed until true
restitution had been made for sin, The same Biblical covenant
principle pulls over into the marriage covenant.

Let’s go back to the David and Bathsheba story. Their rela-
tionship was illegitimate until the death of the son. Even though
Bathsheba’s husband had died, and the marriage with that hus-
band had died, she was directly responsible for his death. True,
the covenant was dead, but she had been one of the people who
had killed it. She bore culpability in the matter and restitution
had to be paid. The restitution was the illegitimate son. When her
son died, the payment was made as has been indicated in the
fourth chapter, and her relationship with David became a legiti-
mate marriage. As I have mentioned earlier, this message points
to the true Son Jesus Christ who offers forgiveness to people no
matter what they have done. But it also offers a second chance, if
