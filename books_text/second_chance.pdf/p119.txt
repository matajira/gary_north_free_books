Living Happily Ever After 97

covenantally when she committed adultery.

The third principle of cause and effect argues that the capital
offenses of the Bible are offenses that produce covenantal death,
and they are therefore divorceable offenses of the Bible. It means
for Roger that he has legitimate grounds for divorce.

The fourth principle of protection says that the covenantal life of
the innocent party should be protected by sanctions that essentially
sever the relationship: execution, excommunication, or restitu-
tion. According to this principle, Roger should be encouraged to
protect his own covenant with the Lord and his children’s cove-
nant with the Lord by divorcing Norma, if she does not repent.
He should do so because at present there is no longer a Biblical
system of civil government to apply Biblical law, to uphold the
things that God gets angry at (Romans 13:1ff.).

Finally, the fifth principle of transfer says that the inheritance of
the family should be preserved through a legal transfer of divorce.
Roger should have no reservations about trying to secure all of the
estate, including the children; they should go with him because he
is the one who will raise them in the proper moral environment. I
know that this statement may seem hard to some, but this way of
thinking is the Biblical mind-set. The inheritance goes to the
faithful, not the unfaithful. Norma is the guilty one, and she is the
one who has forfeited her inheritance. She is the one who left her

. husband. She is the one who worried everyone sick by the way she
left. According to the Bible, she has lost everything by her own
choosing!

Roger can make a new life for himself with this inheritance,
because he will have the possibility of making a complete separa-
tion by this approach. I know that he may still love her and that
he may still want to be with her, but if he views her as covenant-
ally dead, he can begin to work through his grief and he can start
all over. I also know that present civil laws make some of my sug-
gestions difficult. He may have to let his children go to be with
her, if the judge so rules in this case.

He should fight for complete custody, and he should try to file
first if she is unrepentant. He must not voluntarily consent to the
