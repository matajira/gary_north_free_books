184 Second Chance

tian lawyer. He should ask them to alert him to possible negative
consequences to the divorce. After he listens for a while, he may
decide that it would be better to remain married, even to someone
who was covenantally dead. For example, what if a woman has no
ability whatsoever to provide for her children, who are being fed,
clothed and educated at a good Christian school? As long as her
husband does not have AIDS or herpes, she might be better off
waiting it out, hoping and praying that her husband will repent and
come to his senses, If she goes ahead with the divorce and has to put
her kids in a public school, her children might be in a much more po-
tentially damaging situation. So, consider the consequences.

How a Family Approaches a Remarriage as an Adoption

Let’s turn to the remarriage question. We can allow the same
Bible story to give us some guidelines about remarriage. Remem-
ber that we have already concluded that remarriage is a process of
adoption. An adoption is a process whereby a person receives the
following:

New Name
New Position
New Law

New Sign

New Inheritance

Keeping these points in mind, let us consider how a family should
approach remarriage.

1. A Name Change is Essentially Religious

The Bible text gives us considerable information about the
backgrounds of Judah and Tamar. Judah’s name meant “praise.”
He was a Jew, a descendent of a long line of patriarchs, the fourth
son of Jacob by Leah. He had special privileges by virtue of his
birth to a patriarch, but he had special responsibilities. He was
also in the Messianic line; he was an extremely important person
in terms of the kingdom of God.

‘Tamar, on the other hand, was more than likely a Canaanite,
