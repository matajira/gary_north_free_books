xX Second Chance

murmuring about the conclusions of Second Chance, is to fail to deal
with the immediate problems at hand. Murmuring is neither a
theoretical solution nor a practical solution; it is simply moral
rebellion. It keeps people wandering in the wilderness and outside
the Promised Land (Exodus 15:24; 16:2; 17:3; Numbers 14:2,
29; 16:41).

To those who take the hock and move on, I say: ignore the mur-
murers. Jast do your work faithfully. The murmurers will eventu-
ally die in the wilderness. They are dying men whose eyes are
turned backward toward the bondage of humanist Egypt. They
jong for the leeks and onions, for the slave world of limited per-
sonal responsibility. They deeply resent the fact that God has
pulled them out of that low-responsibility world. God will not
allow them to go back; neither will AIDS. Ignore them. Let God
deal with them. Your job is to keep your eye focused on Canaan,
which God will deliver into your hands, or into the hands of your
covenantal heirs. The Puritans had such a vision of victory, but it
faded as their covenant theology faded. That lost vision has at last
been recovered in our day. It is our responsibility to make better
use of it, We have been given a second chance. Not just in mar-
riage counseling, but in every area of life.

I fully expect this book to become the most successful of all
those titles in the Biblical Blueprints Series. The series in turn is
designed to lead answer-seeking Christians to the world-and-life
view called Christian Reconstruction, Christian Reconstruction
begins with the five-point covenant model, and Second Chance
shows exactly why this model is Biblical, and therefore why it
works in the real world. For many people, the journey toward the
comprehensive Christian Reconstruction of all society will begin
with Sutton’s highly practical solutions to the vexing questions
surrounding divorce and remarriage.

The critics of Christian Reconstruction had better understand
my strategy in publishing this book. I am bringing hard-pressed
Christians a series of forthrightly, explicitly Bible-based and
workable solutions to problems that our critics cannot successfully
deal with. There is a lot of mumbling about developing a Chris-
