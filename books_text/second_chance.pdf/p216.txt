194 Second Chance

come. In other words, as the Church deals with its own sin, the
society is preserved for the sake of and by means of the Church’s
judgment on itself. If the Church is holy, then it can change soci-
ety, but not until then. If it would deal properly with the divorce
and remarriage question within its own walls, it could change
family life in the world, but not until then. So, how does it per-
form this function?

It must be prepared to process divorce lawsuits and become a su-
perior system of justice to the civil realm. It must lovingly and
righteously prosecute its own offenders. Why am I picking on the
divorce issue? Because divorce is probably the most often violated
Biblical offense in the Church. If it were dealt with in the proper
manner, it would set the stage for dealing with any other offense in the
Church.

One good discipline case in a Church goes a long, long way to
sending a message that the Church is not going to tolerate unre-
pentant behavior any longer. It would drive away the wolves, and
it would encourage the righteous. And then, when the world sees
the Church dealing with its own, it will respect the Church as a
place with people and leaders of integrity. And then, the world
will not only watch its behavior, but it will flock to the Church for
the right answers. So, let us consider how the Church should re-
spond to the covenantal lawsuit of divorce, and then examine how
it should deal with remarriage as an adoption.

The Church and Divorce Lawsuits

As in the previous chapter on the family, I have structured my
discussion in terms of the Bible’s five-point covenant model.

1. Bring it Before the Lord

The very first thing that the Church should realize is that it
represents Christ walking on the earth, because it is the Body of Jesus
Christ. It is the agency which brings people to the Lord, and it is to
the Lord that people in the Church should go when they have a
dispute, especially a divorce lawsuit. The text at the beginning of
the chapter is quite clear about this emphasis. It talks about a
