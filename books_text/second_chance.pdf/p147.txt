The Period of Covenantal Transition 125

effects of having been in sin and spiritual death. He may also have
to wait a considerable amount of time to have old patterns dis-
cipled out of him so that he doesn’t make the same mistakes again.

Solomon speaks of the effect of covenantal death resulting
from adultery when he contrasts the effects of God’s Jaw with the
effects an adulterous relationship. He has already been referred to
in the third chapter, but he is consulted again to illustrate why the
guilty party needs a statute of limitation. Here is what Solomon

says:

My son, keep your father’s command, and do not forsake the
law of your mother, Bind them continually upon your heart; tie
them around your neck. When you roam, they will lead you; when
you sleep, they will keep you; and when you awake, they will speak
with you. For the commandment is a lamp, and the law is light;
reproofs or instruction are the way of life, to keep you from the evil
woman, from the flattering tongue of a seductress. Do not lust
after her beauty in your heart, nor let her allure you with her
eyelids, For by means of a harlot a man is reduced to a crast of bread (Prov-
erbs 6:20-26),

As the Word of God brings a man under its inescapable juris-
diction, so the relationship with a harlot seeks the same. She re-
duces the guilty party to a “crust of bread” (Proverbs 6:26). What
does it mean to be reduced to this? She consumes him and instead
of being the consumer, he becomes the consumed. She dominates
him, destroying his ability to dominate and making him into the
passive one. Ironically, adulterers and fornicators never think of
themselves this way. They generally perceive themselves as being
“in control,” but they're not. They are literally dough in the hands
of sin. Thus, it is this passivity, vulnerability and weakness that
has to be overcome. It takes time, what I have called a statute of
imitation.

I believe that it is the effect of the loss of the jurisdiction of
one’s spouse that the surviving partner has to defeat. I think that
the death of this hierarchy affects the judgment and everything
about a person. I am convinced that it takes a statute of limitation
