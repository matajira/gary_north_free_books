Author's Introduction 15

from committing a divorceable offense just won't work! Indeed,
the Bible teaches that the wicked are provoked by the righteous,
because of their righteousness (Matthew 5:10-12). So, I think is is high
time to consider the innocents, because they do exist, and ironic-
ally, the subjects of divorce and remarriage are seldom addressed
from their point of view. I don’t think that this consideration means
that we should take a soft view on divorce; anyone who seriously
weighs my position should not come away with such a conclusion;
Lam definitely not giving an easy way out, Rather, I am trying to
protect the innocent, and I am attempting to present a whole-
Bible view of the problem in such a way that even the guilty can
be properly restored. In the end, I believe anyone will be helped
who reads Second Chance.

If you know someone, maybe a relative or other loved one,
who has gotten a divorce, you need Second Chance.

If you are a pastor or some other kind of religious leader, you
have undoubtedly been stumped by all kinds of problems related
to divorce and remarriage. You must read on.

If you're contemplating a divorce, you must be absolutely cer-
tain that your marriage has died and that it is unable to be resur-
rected. You must not stop here; you should read on.

If you've already gotten a divorce, you too must be absolutely
certain that your ‘divorce was legitimate. You must read on for
this information.

And if you’ve divorced and remarried, you of all people must
make certain that your previous divorce or divorces were legiti-
mate in the eyes of God. You absolutely must read on,

For all wanting a second chance, there is hope on the pages
ahead!

A Caution About Reading Second Chance

First, please realize that I have already written a book about
the sacred, permanent character of marriage in Who Owns the
Family: God or ihe State?, published also by Dominion Press. So I
believe that marriage is intended to be as long as “they both shall
live.” But Second Chance deals with an awful reality: sin constantly
