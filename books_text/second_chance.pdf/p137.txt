I, _Hierarchy/Authority

7
THE PERIOD OF COVENANTAL TRANSITION

And being assembled together with them, He commanded
them not to depart from Jerusalem, but to wait for the Promise of
the Father, “which,” He said, “you have heard from Me; for John
truly baptized with water, but you shail be baptized with the Holy
Spirit not many days from now” (Acts 1:4-5).

One of the most serious problems facing divorced people is the
problem of remarrying too quickly, what has been called marry-
ing on the rebound. Because this is a serious problem, we know
that it must be related in some way to a Biblical principle. We
need to search for a Biblical norm that is being violated.

Jesus had told His followers that the Holy Spirit could not
come until He had gone to His Father in heaven. Christ’s lan-
guage is that of a man warning His friends of His coming death.

But now I go away to Him who sent Me, and none of you
asks Me, “Where are You going?” But because I have said these
things to you, sorrow has filled your heart. Nevertheless I tell you
the truth. It is to your advantage that I go away; for if I do not go
away, the Helper will not come to you; but if I depart, I will send
Him to you. And when He has come, He will convict the world
of sin, and of judgment” ( John 16:5-8).

So the disciples did what He told them to do. After His ascen-
sion, which took place forty days after His resurrection (Acts 1:3),
they returned to Jerusalem. They waited. They waited for another

15
