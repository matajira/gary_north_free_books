Marriages Are Made in Heaven 19

gestion, or so Doug thought. At first, she listened pensively as he
broached his idea. Then she started to cry, her head hanging in
her hands. After a moment, without raising her head, she screamed
in a burst of anger, “What makes you think the problem is with
me? Why do you have this primitive notion that fertility is always
a problem with the woman? Did you ever consider that you are the
impotent one?” (She really hadn’t meant “impotent”; she had
meant “infertile,” but in the minds of many men, both of these
physical conditions are considered signs of a loss of masculine
status.) She halted her rapid-succession of questions just in time
to notice the sound of Doug's feet walking out of the house.

Doug jogged away from his street and then he slowed to a
walk. He knew that Sally had made a valid point, but he was not
willing, nor was he ready, to admit it. After several hours, how-
ever, he returned. He made plans with Sally for both of them to be
examined by a doctor.

The news was not good. Neither Doug nor Sally could have children,
and there was nothing that could medically be done. Both were infertile.
Both were devastated at the news. Both had planned and both had
waited for the best possible time in their lives to have children,
only to discover that they could never have a family, except by
adoption. But they weren't interested in adoption. They wanted
their own children, They wanted their own flesh and blood. They
didn’t want somebody else’s kid. So, they decided to put the idea
of children out of their minds, because after all, they still had each
other, Or did they?

Doug’s pride was wounded. He could not accept that he had a
physical problem. He persisted in hanging on to the myth that
another woman would be able to bear his children. He decided to
divorce Sally. He wanted out after six years of marriage: four of
which were utopian, and two of which were a nightmare. Before
he went through with it, however, he reluctantly accompanied
Sally to see the Christian counselor of this story.

After Doug and Sally described their situation—mostly Doug
talked because he was the one who wanted a divorce — the counse-
lor said to Doug, “You don’t have Biblical grounds for a divorce.
