28 Second Chance

to commit adultery; and whoever marries a woman who is divorced
commits adultery” (Matthew 5:31-32).

Do you see what Jesus clarified? He said that sexual immoral-
ity, a specific act of unfaithfulness, is the basis for legally dissolv-
ing the covenant. He could only reason this way if He was argu-
ing covenantally, that is, according to the doctrine of imputation. He
presumed that God designates a certain status to a relationship on
the basis of performance. If the moral performance of one of the
marriage partners fails, the injured partner can lawfully seek
God’s formal announcement of the new moral and legal status of
what has become a morally broken marriage. The sinful act in
essence morally destroyed the marriage covenant, and this be-
comes the legal basis for God’s issuing of a divorce certificate. One
certificate of the marriage’s covenantal death is issued in heaven,
and parallel certificates are supposed to be issued by the church
and civil government. If the covenant is broken by a specified act
of moral rebellion, the covenant dies, just as we saw with Adam
when he broke God’s specified provisions governing the covenant.

A certificate of divorce can only degitimately declare what has
already been imputed on the basis covenant unfaithfulness. It is a
statement of death, not the cause of death, and being involved in
divorce and remarriage or being involved in granting a certificate
of divorce is not necessarily a violation of what Jesus meant when
He said, “let not man separate” (Matthew 19:6). A legal declaration
of divorce is not valid without some kind of covenant-breaking that
has already rendered the marriage covenant dead.

So, Jesus reaffirmed the Biblical principle of imputation as the
basis of the marriage covenant. He cautioned that people cannot
get a legitimate divorce just because they go to the State or to the
Church and pay for a document that says they are no longer mar-
ried. Money does not make a divorce certificate valid in God's court. Unless
the covenant has been broken— unless there has been a party truly
at fault, whose act has rendered the covenant dead—the two peo-
ple are considered married by God. It is both immoral and illegal
for any human court to issue a formal death certificate for a still-
valid marriage. The human courts’ divorce documents must re-
