Covenantal Execution Protects the Innocent 83

Summary

Always, the Bible demanded that the innocent be protected.

1. We started with the story of David and Bathsheba to estab-
lish that the death penalty was demanded for violating the mar-
riage covenant. But why?

2. Nathan's parable led to the principle behind the death
penalty: the protection of the innocent.

3. The principle is built into the way the covenant is estab-
lished, the fourth part of the Biblical covenant. A covenant is
formed by parties making promises under the condition of death if
the promises are broken. These promises are called seif-maledictory
oaths, requiring the death penalty on the party that breaks the cov-
enant.

4. This manner of forming a covenant was set up in the gar-
den, where God said, “The day you eat of it, you will surely die”
(Genesis 2:17). It was the means for cutting the Abrahamic and
Deuteronomic covenants,

5. The death penalty requirement protected against a rival cov-
enant and rival sanctions. The garden is an example. Satan attempted
a rival covenant with Adam and Eve to God’s covenant with the
same. He actually tried to sanction God into a covenant under him
through the rival covenant. But God stopped Satan with the death
penalty sanction. He was the innocent, and He was protected from
Satan’s rival sanctions.

6. The same principle of protection applies to marriage. A
marriage is formed when both parties give promises under the con-
dition of death.

7. The “one fiesh” promise of Adam was referred to as an ex-
ample of the marital promise.

8. The condition of death attached to the covenantal promise
of “one flesh” is seen in Paul's warnings against a rival “one flesh”
covenant with a harlot (1 Corinthians 6).

9. From the context of 1 Corinthians 5-6, Paul teaches that any
covenant with a “fornicator” is dead, and it should be dissolved. He
reasons, “A little leaven leavens the whole lump.”

10. The death penalty sanction is applied in some unusual
‘ways.

11. First, execution is a means of the death penalty. Although
