What the Church Should Do 205

the cause/effect of knowing Christ delivers him from his past.
Christ’s death frees him from the death of his previous life. The
only exception to this principle is if the new convert is still married
to an unbeliever. He (she) must remain with the unbeliever in this
case (I Corinthians 7:12-16),

4. Lawsuit against Remarriage

Adams has raised some questions about the remarriage of a
new convert. But what should the Church do about an unlawful
remarriage between two believers? Does it just turn its head the
other way, and does it just let matters go? Or, does it bring a law-
suit against the second marriage? And if it brings a negative judg-
ment, what should it do? Should it split up the second marriage,
or what? And horror of horrors, what if there are children in-
volved in the second, unlawful marriage?

To answer these questions, the principle of the @fect on marriage
of the shift from wrath to grace in history must be understood. What is
it? In the Old Testament, there is an interesting story of what hap-
pened to Israel after they returned from being exiled. They almost
immediately began to commit the same sins that had previously
caused them to be expelled. One of the sins they committed was
that they married unbelievers. Significantly, Nehemiah and Ezra
told them to “put away their foreign wives” (Ezra 10:2-3). Yet, in
the New Testament, the Apostle Paul tells believers to remain
married to the unbeliever if he was married to him when he con-
verted (1 Corinthians 7:12-16), Why the difference?

In the Old Covenant, death spread to death, meaning there
was no way to stop the spread of the curse. For example, if a per-
son touched some unclean thing, the contamination extended to
him. There was no way of stopping it. In the New Testament,
however, this principle seems to have been significantly altered.
The clearest example is the unclean woman who touched the gar-
ment of Jesus. The moment she did, she was healed. Her death
did not spread to Him, rather, His life spread to her. The explana-
tion is that Christ’s presence reverses the effect of the curse. The
shift from wrath to grace in history changes the effect of the curse.
