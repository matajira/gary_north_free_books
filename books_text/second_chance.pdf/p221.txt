What the Church Should Do 199

your belly swell, and may this water that causes the curse go into
your stomach, and make your belly swell and your thigh rot
(Numbers 5:19-22).

Then her response was, “Amen, so be it” (Numbers 5:22), So the
trial began with an oath before the Lord. Again, is this just an
Old Testament practice? Consider what happened with Ananias
and Sapphira.

But a certain man named Ananias with Sapphira his wife, sold
a possession. And he kept back part of the proceeds, his wife also
being aware of it, and brought a certain part and laid it at the apos-
tle’s feet. But Peter said, “‘Ananias, why has Satan filled your heart
to lie to the Holy Spirit and keep back part of the price of the land
for yourself? While it remained, was it not your own? And after it
was sold, was it not in your own control? Why have you conceived.
this thing in your heart? You have not lied to men but to God.”
Then Ananias, hearing these words, fell down and breathed his
last. . . . Now it was about three hours later when his wife came
in, not knowing what had happened. And Peter answered her,
“Tell me whether you sold the land for so much?” And she said,
*Yes, for so much.” Then Peter said to her, “How is it that you have
agreed together to test the Spirit of the Lord? Look, the feet of
those who have buried your husband are at the door, and they will
carry you out.” Then immediately she fell down at his feet and
breathed her last (Acts 5:1-11),

Notice that Peter asked for a statement under oath, meaning God
was the witness. And then he proceeded with the trial. The key to
the trial is the oath.

The trial itself should be an opportunity to hear witnesses, evi-
dence and both sides of the story. A church court should always
remember that there are always two sides to every account. It
should also keep in mind an important principle in Proverbs:
“The first one to plead his cause seems right, until his neighbor
comes and examines him” (Proverbs 18:15). The first person to
present his story will always seem right, but a church court should
be patient and it should hear both sides.
