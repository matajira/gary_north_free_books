50 Second Chance

The Terms of Covenant-Keeping

What are the terms of the covenant? The third section of Deu-
teronomy summarizes the Ten Commandments in great detail,
meaning these commandments represented the essence of man’s
covenant with God; they were the terms; they form a double wit-
ness to the covenant itself; they are arranged according to the cov-
enant structure in two groups of five.

Dr. Gary North and I have given extended treatments of the
covenantal structure of the Ten Commandments, so I won't
belabor the point here.? But a quick overview will help us to see
the commandments as terms. Keep in mind that there are basically
five terms repeated twice. Think of the covenant as a coin with two
sides, The same five terms are repeated twice, but from two differ-
ent perspectives: priestly and kingly. The first five command-
ments are priestly, in that they primarily focus on responsibilities
before the throne of God. The second five commandments are kingly,
in that they concentrate on responsibilities in society. So, the cove-
nant basically has five terms with two sides to those terms, equal-
ing ten commandments. But what if the terms were broken?
What was the effect? The effect was death.

The Effect of Covenant-Breaking

In the garden Adam was told, “The day you eat of it [the tree
of the knowledge of good and evil] you will surely die” (Genesis
2:17). In Deuteronomy Moses said, “If you do not carefully ob-

 

faithfulness. Moses taught that man was to live by faith, because he was the same
Moses who recorded, “Then he [Abraham] believed the Lord; and He [God]
reckoned it to him as righteousness” (Genesis 15:6), But Moses understood faith
as faithfulness. Faith ig not just some sort nodding of the head with a “yes” to God,
or some sort of mental assent. It is a dependence with one’s whole life that is
faithful to the Lord, as in the words of one writer in the New Testament, “Faith,
if it has no works, is dead” ( James 2:17), Faith, in other words, encompasses
faithfulness to the Lord’s commandments: “Now by this we know that we know
Him, if we keep His commandments’ (1 John 2:3).

3. Gary North, The Sinai Strategy: Economics and the Ten Commandments (Tyler,
Texas: Institute For Christian Economics, 1986), pp. ix-xavi. See also Sutton,
That You May Prosper, pp. 214-24.
