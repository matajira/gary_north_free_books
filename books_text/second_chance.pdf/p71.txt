Playing with Fire Burns Out a Marriage 49

was really saved, or whether or not there is a doctrine called “once
saved always saved.” I’m sure these are important theological
questions that Tom’s case raises; I'm sure they need to be an-
swered and the true Biblical doctrine of “once saved always saved”
defended. I’m also sure that many diligent Christians will read
this book who are on both sides of the “Can a person lose his salva-
tion?” debate. But I tell this story for another purpose.

Tom definitely lost everything, including his faith. Solomon
says it is possible, when he tells us, “Whoever commits adultery
. . « destroys his own soul” (Proverbs 6:32). If you consider the
principle of the last chapter—a person can die covenantally, thereby
dissolving the marriage covenant~—Solomon’s statement makes
sense. But how can this be? How can adultery “destroy the soul’?
The task of the present chapter is to answer these questions, and
to understand how people and marriages die covenantally, Let us
first examine the basic principle in the Biblical covenant, and then
we will be able to move to the marital covenant to discover the
same process.

Cause/Effect in the Biblical Covenant

The third section of the Biblical covenant is called the ethics
segment (Deuteronomy 5-26), because it teaches an ethical rela-
tionship between cause and effect.! It explains that there are cer-
tain terms (cause) of the covenant, and if those terms are broken,
certain éffects will result, namely death to the covenantal arrange-
ment, just as God had said in the garden, “Of the tree of the
knowledge of good and evil you shall not eat [term], for in the day
you eat of it you shall surely die [effect]” (Genesis 2:17). It is like,
but not the same as, a contract. Two parties enter into a legal rela-
tionship with certain specified terms. If those terms are violated,
the relationship is jeopardized, and parties usually end up suing
each other out of the arrangement.?

1. Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas:
Institute for Christian Economics, 1987), ch. 3.

2. Does this mean that the covenantal relationship between God and man was
based on some sort of works salvation? Of course not. The terms are terms of
