Fools Rush in Without Counsel 153

him, He also perished, and all who obeyed him were dispersed,
And now I say to you, keep away from these men and let them
alone; for if this plan or this work is of men, it will come to noth-
ing; but if it is of God, you cannot overthrow it—lest you even be
found to fight against God (Acts 5:33-39).

Gamaliel’s counsel is perhaps the most famous counsel ever
given to the Jews. He gave it in the context of John’s and Peter's
being imprisoned for preaching in Jerusalem, after which they
were brought to a trial where he gave this counsel. He very simply
reasoned that a man named Theudas had claimed to be the
Messiah, and his movement had never amounted to anything. He
concluded that it was not of God. But, he was really trying to say
that something which is of God lasts, to his own people’s utter con-
demnation after two thousand years of expanding Christianity.

The principle of counseling for those entering a second mar-
riage is that a marriage founded in the Lord “cannot be overcome”
(Acts 5:39), to use Gamaliel’s words. It will not be temporary, but
it will last as long as two people are physically alive. For this rea-
son, it should not be approached hurriedly or carelessly. It is like
building a house, which if built haphazardly will not be a perma-
nent structure. So, any counsel given to them should be to this
end. Two people should not enter the marriage thinking “tempor-
ary,” or that they can get out of the marriage if this one doesn’t
work, rather they should think permanent! And they should be
counseled by being told the principles of marriage that maintain it
over the long haul.

Summary

1. [began with a story about John who lost his wife and almost
made the biggest mistake of his life. He was prevented from mak-
ing it by good Biblical counsel.

2. Counseling is grounded in the fourth point of the covenant:
judgment. In this section of the covenant, Israel allows itself to be
judged up front in their new walk with the Lord. They allowed
Moses to be their judge and counselor, leading them into this judg-
ment. Thus, counseling is a process of judgment, as I verified this
