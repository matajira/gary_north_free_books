Subject Index 237

Protestant Reformation, 14

rape, 51
red heifer, 121
reformability, 216
Reformed Theological Seminary, 5
religion, 185
remarriage
adoption, 165
ceremony, 188
covenant Adoption, 169
customs, 187
debt, 139
evaluation of in-laws, 134
guilty party, 111, 124
inheritance, 188, 206
innocent, 110, 123
name change, 184
of Civil Magistrates, 218
overcoming vulnerability, 126
period of adjustment, 122
qualification, 112
State and, 217
structure, 186
time, 126
timing, 120
to a Christian, 133
too quickly, 115.
repentance, 95
restitution, 77, 66n.1, 95ff., 120
Resurrection, 106-107
Revelation, 186
Roe» Wade, 218
Rushdoony, R. J., 2, 61
Rutherford, Samuel, 59

sabbath, 122
sabbath-breaking, 51, 57
Sadism, ix

salt, 193

Samson, 24n3.
sanctions, 142, 167
sanctions principle, 215

second marriage, 108
sex
illicit, 46
sacrament, 53
sins, 58-59
Sodom, 193
Solomon, 49
spiritism, 57
spiritual death, 125
staff, 189
State
appeals for unbeliever, 211
Biblical Law and, 214
blueprint, 210
ceremony and, 221
court system, 212-13
covenantal adoption, 209#f.
covenantal divorce lawsuit, 210
divorce and, 208f.
divorce lawsuit, 209ff.
ethics & remarriage, 219-20
granting divorce and, 215
inheritance, 216, 221
Lordship, 210-11
power, 210-11
remarriage, 217
remarriage of magistrates, 218
State’s laws, 215
sword, 213
transfer of name, 217-18
war and, 219
statute of limitation, 120, 203
stepchildren, 155
how to deal with, 164f.
Stormer, John A., 62n.13
sword, 201

Ten Commandments, 51
equal yoke and, 132
theocentric, xi
Theodosius I, 13
transcendence, 21, 167
declaration, 133
