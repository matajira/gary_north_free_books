12

4,

5.

A Study Guide to Liberating Planst Earth

How do you love God? (John 1415) (p. 18)
QA. By taking up the cross in the world. Joshua 22:5

OB. By keeping His commandments. Psalm 119:9-16
QC. Through cooperating with all men ! John 2:3-5
of good will. °
Q D. By walking after the flesh and not
after the Spirit.

The apostle Paul says that love does no harm to a neighbor,
therefore: (p. 18) —————_—__

*] A. Love is superior to the law. Leviticus 19:17-18
*1 B. Love fulfills the law. Matthew 7:7-12
OC. Love replaces the law. Luke 10:25-37
QD. Love transforms the law. Galatians 5:14

. Karl Marx wanted to see workers throw off their chains. When

they finally did, he taught that they would be happy and the world
would enjoy peace and harmony. Marx’s program suffers because
he failed to reckon with the fact that: (p. 19)

OA. All men need an anchor and chain | Hebrews 6:19, 20
of some kind (men need a principle
to give direction to their lives).

*] B. His followers would divide into competing camps of so-
calists and communists.

OC. It will take a long time to persuade the proletariat to coop-
erate with the peasantry.

QD. The affluence of Western nations makes bourgeois de-
mocracies seem attractive to workers.

 

 

 

 

Since men need an anchor and chain for security and orientation,

they will serve: (p. 19)

OA. Every new fashion that happens to | Matthew 6:24
come along. Luke 16:13

*1 B. Their own true conscience.

1 C. Either God or Mammon.

Q D. The people they feel most comfortable with.

 

 

 

 
