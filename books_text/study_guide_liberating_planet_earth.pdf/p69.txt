66

A Study Guide to Liberating Plaint Earth

3. Why is the Eighth Commandment (“Thou shah not steal”) impor-
tant for political and economic discussions? (p. 117)

el A. It is not important, because owner- | 1 Kings 21

Oc.

«ID.

 

 

 

 

ship of private property is a sin. All
property should be held in common, and the state should
decide how it should be used.

. It is important because Biblical law prohibits people from

forcibly taking the fruits of other people’s labor, or their
inheritances. The state is required by the Bible to defend the
property rights of all citizens.

It is not important because it is not spiritual to be concerned
about worldly possessions.

It is important because the state needs to be protected from
the selfish desires of citizens who have no concern for the
common good.

4, Some wicked men prosper, and some righteous men meet adver-
sity. What are we to conclude about the accuracy of the blessings
and curses in God’s law? (p. 119)

ela

*1 B.

ele

Op.

The promises of the gospel are spiritual, not physical. God’s
law is irrelevant here.

God’s favored way of working with His people is purifying
them through trials. The most important thing is not that
they get blessings but that they remain faithful to Him.

The promises of prosperity do not really apply until we get
to the kingdom age.

In general, there is a significant correlation between cove-
nantal faithfulness and external prosperity.

5. Theological arguments against free market economics are: (p. 121)

OA.
1B.
elec.
oD.

Borrowed from envious humanists.
Gaining respect from Marxist theoreticians in Europe.
Outgrowths of the Protestant Reformation.

Better supported in the New Testament than in the Old
Testament.
