CHAPTER 8
“The Liberation of the State” pp. 100-115
Memory verses: Exodus 18:19b-23

1, An important thing to realize about the human administration of

justice is that: (pp. 100-101)

OA. It is a necessary evil.

OB. Even though we may lose our particular case in court, the
will of the people has prevailed.

QO GC. It is impossible to have an uncomplicated court system in
an advanced society.

Q D. It will always be imperfect, even when carefully admini-

stered in terms of God’s word, because men are finite and
sinful. t

2, The requirements for an elder in the Mosaic system are close to
those in the New Testament. The primary requirement is: (p. 101)

2 A. Wealth. Exodus 18:
*1 B. Renown. 1 Timothy 3.
*1 C. Character.

QD. Managerial experience in the business world.

 

 

3. The administration of justice that Jethro suggested to Moses did.
not provide perfect justice, but it did provide: (p. 101)
*] A. Regular and predictable justice.
*] B. Much less paperwork.
*1C. For appealing cases to Moses much more quickly.
OD. Equality before the law.

58
