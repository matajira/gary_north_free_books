CHAPTER 5
“The Liberation of the Individual” pp. 62-72
Memory verses: John 8:34-36; Remans 8:2

1, How did Adam lose his status as a son of God? (p. 62)

QO A. He revolted against God’s rule and Genesis 3:6

chose to live under the rule of Satan. 1 Corinthians 15:22
*1 B. By the process of evolution. Remans 5:12-19
OC. By making property rights more

important than human rights.
QO D. By sexual contact with Eve.

2. How does Jesus Christ fulfill the role of a Second Adam? (p. 62)

OA. By dying. 1 Corinthians

OB. He lived in Jerusalem. 15:45-58

OC. By recreating the Garden of Eden.

QD. He came to earth in order to restore this forfeited sonship
to His people.

 

 

 

 

3. Is there a universal Fatherhood of God? (p. 62)

*]1 A. No, but it is possible to achieve a | Genesis 1:26-28
common religious creed among the | Malachi 2:10
leading world religions.

*] B. No, He did not create all races at the same time and some
are more religiously developed than others.

*1 c. Yes, God stands as Creator over all men and over the
human race as a unit.

*] D, Yes, there are many ways but one God.

 

 

 

 

34
