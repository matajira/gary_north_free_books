“The Liberation of the Church” 533

14, When the church begins to fearlessly declare God’s standard, the
state: (p. 91)
*] A. Is happy because it sees the church as a fellow servant
seeking to promote peace and unity.
*1 B. Is generally tolerant because it recognizes that God has
given authority to the church.
*1 C. Is outraged, because rulers do not want outspoken critics.

15. When the church begins to fearlessly declare God’s standard, es-

capists are: (p. 91)

OA. Pleased to realize that Jesus wants His people to awaken to
the task of making disciples of the nations.

*] B, Outraged because they do not want to face unpleasant prob-
lems.

OC. Uncomfortable, but eventually are won over to the truth.

OD. Indifferent because they know that despite all false preach-
ing Jesus will return soon and resolve all the problems.

16. God’s plan of comprehensive redemption means simply that:
(p. 92) ee
el A. Everything will be made right at the 1 Corinthians
Second Coming. 15:20-28

«] B. All of the souls of men are important, Philippians 2:5-11
and we must preach the gospel to all.

*1c. Everything is to be brought under the dominion of Jesus,
through His people.

OD. Jesus will rule from His seat in Jerusalem and subject all of
His enemies to Himself for a thousand years.

17, One implication of the comprehensive redemption of Jesus Christ

is that each pastor should: (p. 92)

OA. Expect that church members will ac- Acts 18:1-3
cept unquestioningly his every word. 2 Thessalonians

OB. Inspire the church to conquer the state. 3:8-9

*1(C. Be licensed by the government.

QO D. Should be knowledgeable in a couple of “secular” fields so
that he can demonstrate to his people how to apply Scrip-
ture outside of the church.
