72

A Study Guide to Liberating Planet Earth

10. In the following selections, match the correct category from the

four topics immediately below with the corresponding selections
below them by putting the letter “A”, “B”, “C”, or “D” in the
space provided. There will be five correct matches for each of “A”
through “D”.

*] A. Five Doctrines of Dominion on Earth. (p. 142)

O/B. The Communist imitation of these five doctrines.

(pp. 142-143)

OC. The radical Islamic imitation of these five doctrines. (p. 143)
*1 D. The humanistic-scientific imitation of these five doctrines.

(pp. 143-144),

(1) Elite academic community

(2) Allah has predestined victory for his followers

(3) Eschatological optimism

(4) Confidence that revolutionary violence will conquer all

(5) Inviolability of the scientific method

(6) Allah predestines everything

(7) The irresistible forces of dialectical history

(8) The tool of the covenant, Biblical law

(9) Militant religious organization

(10) The absolute sovereignty of the Creator God.

(11 ) Islamic world-view is the only truth

(12) Membership in the Party

(13) Scientific method judges all rival forms of knowledge

(14) No authority maybe appealed to other than the Koran

(15) Biblical presuppositionalism: the self-attesting,
infallible Bible

(16) Marxist-Leninist philosophy

(17) Immutable physical laws (such as cause-and-effect)

(18) God’s covenant that governs all men

(19) Confidence that applied science will answer all
questions and solve all problems

(20) Sovereignty of man (e.g., “Vanguard of the
proletariat”)
