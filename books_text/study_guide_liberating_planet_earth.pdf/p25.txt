22 A Study Guide to Liberating Planet Earth

4. God should be worshiped because He is powerful; but He should
also be worshiped for other reasons such as: (pp. 39-40)
«| A. Many other people worship Him.
OB. It is socially acceptable.
(1 C. He is righteous.
CD. It seems better to worship Him than the gods of other great
religions.

5. Why do Satanists do everything that they can to focus men’s eyes
on earthly power? (p. 40)

 

1 A. Because they do not really have any | Matthew 10:28
power. Revelation 20:10

*1 B. Because they only have power in hell.

OC. Because earthly power can overcome God.

*] D. Because their power counts only in this life.

 

 

 

6. What is the proper attitude towards power? (p. 40)

*] 4, We should try to gain as much power Deuteronomy
as we possibly can, since God, who 8:17-19
made us in His image, has all power. _ Daniel 3, 4

OB. We should always take power
seriously, but never worship it.

Q ¢. We should not really be concerned about the powers that
operate in this world, since they are merely apparitions of
spiritual powers.

0 D. We should look forward to the millennial kingdom, when
the Lord Jesus will come and truly exercise His power.

7. What does the author believe are the three major outlooks regard-
ing power today? (p. 41)

OA. Socialism, communism and capitalism.

*] B. Military, diplomatic and commercial.

*1 C. Power religion, escapist religion, dominion religion.
QO D. Euro-American, Marxist-Leninist and Third World.
