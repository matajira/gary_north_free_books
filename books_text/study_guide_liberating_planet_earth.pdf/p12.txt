I.

16.

17.

18.

Introduction 9

God put man into the world with work to do from the very begin-
ning. God made the world for man to live in, and to find meaning
in. In Genesis 1:28-29 God authorized man to: (p. 10)

*] A, Devote all of his time to “spiritual” 1 Corinthians

 

pursuits. 15:24-27
OB. Train circus animals. compared with
*1 c, Become a farmer or a fisherman. Ephesians 1:19-23
*] D. Exercise dominion over all other Romans 16:20
creatures. Revelation 1:6;
5:10; 20:6

Why is the Bible a “this-worldly” book? (pp. 11-14)

*1 A. It tells us about the Creator God who made this world.
OB. Because God is part of everything.

OC. It was written by men who lived in the world.

OOD. It tells us the origins of one of the world’s great religions.

Why is the Bible an “other-worldly” book? (p. 11)

OA. Because God is not concerned with this world, but with the
spiritual world.

QO B. Because this world is not our home; we are just passing
through.

OC. This same God who made the world is not subject to it, but
is in full command over it.

QD. It does not give us any helpful advice on living in this
world.

Where can liberation be found? (pp. 14-15)

C1 A. Liberation comes through covenan- | Psalm
tal faithfulness to the God who liber- | Psalm 37:
ates the righteous and unrighteous
alike.

*] B, Liberation comes through covenantal faithfulness to the God
who liberates the righteous.

Q ¢. Liberation comes through a mystic union with God through
meditation.

1 D. Liberation can be found in a democratic popular consensus.

 

 
