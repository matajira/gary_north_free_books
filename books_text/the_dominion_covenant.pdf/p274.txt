230 THE DOMINION COVENANT: GENESIS

By monopolizing the entrepreneurial function, the State creates a
planning structure that is far too rigid, far less sensitive to shifts in
consumer demand and resource supplies, than the decentralized
planning of profit-seeking entrepreneurs. This inflexibility in the face of
ceaseless change drastically increases the risk of devastating, centralized,
universal failure. And even when the State’s bureaucrats turn out to be
successful forecasters, as Joseph was in Egypt, the citizens who ben-
efit from this accurate forecasting run the risk of becoming increas-
ingly dependent on the State. As those in Egypt learned during the
reign of the Pharaoh of Moses’ day, successful State planning in one
period in no way guarantecs the continued success of central plan-
ners in subsequent periods. But successful State planning does in-
crease the share of capital assets controlled by the State and its
bureaucratic functionaries, thereby insulating them in subsequent
decisions from private competition in the total decision-making
process. The Egyptians learned this lesson the hard way.

 

Conclusion

It must be recognized that Joseph was in Egypt. No system of cen-
tralized economic planning was created at Mt. Sinai. God did not tell
His people to imitate the experience of Egypt. He told them to avoid
all contact with the “leaven” of Egyptian culture. Joseph brought the
theological slaves of Egypt under bondage to their false god, the
Pharaoh. God does not want His people to turn to the legacy of
Egypt's bureaucratic tyranny as a model for a godly social order.

This exegesis of Joseph in Egypt outrages your typical State-
promoting evangclical, especially college professors. They have tied
their classroom Iccture notes to the State-worshipping worldview of
the tax-supported, humanist-accredited universities that awarded
them their Ph.1.’s. They are the Pharaoh-worshippers of this era.
Had they been in Egypt in Moses’ day, they would have been the
Hebrew foremen working under the authority of Pharaoh’s Egyptian
taskmasters. They are the people who would have come to Moses
and Aaron and told them to go away, because they were making
Pharaoh angry (Ex. 5:20-21). Their high position in the slave system
was dependent on the continuing bondage of their people. So it is
with humanism’s chaplains in the Christian college classroom and
the pulpit today. Freedom would require them to revise their notes
and begin to promote economic freedom in the name of Christ
rather than bondage to the would-be savior State.
