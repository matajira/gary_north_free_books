486 THE DOMINION GOVENANT: GENESIS

naming, 37, 67, 85
sexuality, 90
subordinate, 145
wild, 165

Animism, 251

Anshen, Ruth, 253

Anthropology, 132

Antinomies, 55, 238

Appleman, Philip, 285f

Apprentice, 67, 75

Aquinas, Thomas, 17

Aristotle, 360f, 369n, 370, 390, 418,

429, 431

Ark, 146

Asa, 80

Asceticism, 128, 364, 366

Asitnov, Isaac, 283f

Assumptions, 52

Astrology, 121

Astronaut, 381

Atheism, 434

Atomism, 345, 347

Attitudes, 131, 170

Auction, 220, 223

Auction process, 41

Augustine, St., 367f

Austrian School, 23, 55,58
planning, 25
purposeful action, 338, 342

Authority (see stewardship;
subordination; sovereignty)

Autonomy, 436, 443
Adam, 31, 73ff, 77, 86, 103
causation, 19
creation, 84
law, 32
man’s, 7, 20, 86, 107
market and, 10, 26
name, 150f
nature, 147
omnipotence, 107
rebellion, 86f
sabbath and, 69, 71, 72
testing, 109
universe's, 2
urbanization, 136

Babel, 136, 150, 153, 296

Bacon, Francis, 401
Bahnsen, Greg, 27n, 31
Banfield, Fdward, 126n
Banking, 82
Baptism, 452
Bargaining, 177
Barnacles, 403
Barth, Karl 431
Basil, Sc., 154
Bastiat, Frederic, 94
Batten, C. R., 116n, 209
Bauer, P. T., 13in, 159
Becker, Gary, 346
Being, 449, 453
in general, 446
scale of, 431
Belly, 189
Benhadad, 80
Bentham, Jeremy, 48, 49
Bevan, Edwyn, 13n
Bible, 361, 415f
cortective data, 7, 9
free market, 324
rewriting of, 142
sovereignty of, 229
theocentric world, 19
Bible-pornography paradox, 41£
Bible Presbyterian Church, 28, 147f
Biblical law
blessings of, 75
capital asset, 109
constant, 25
cost-cutting, 106
gold standard, 82
Great Commission, 31
higher spirituality vs., 170
Kingdom of God, 174
normative, 32
overcoming cycles, 114, 116f
power, 35
private property, 195
prosperity, 156f, 174
tool of dominion, 32, 36, 74,
106, 116
Bird, Herbert, 141
Birthright, 178ff, 182, 190f
Black market, 229
