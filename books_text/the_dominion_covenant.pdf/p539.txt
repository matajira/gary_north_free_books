Index

Impersonalism, 279, 344
Impersonal market, 215f
Imputation
God's values, 37, 42, 591, 64, 1004,
107
gold’s value, 83
man’s, 38, 42, 59f, 107
markel’s, 42
(see also values)
Incarnation, 201, 202
Incommensurability, 51
Indeterminacy, 405f
Individualism, 24, 57, 58, 292,
330 (see also anarchism)
Industrialism, 34
Industrialization, 166
Infinity, 433
Inilation, 81f, 123, 129, 243
information, 106, 108
Intellectuals, 294
Interest rate, 126, 128, 182, 207
Interpersonal comparisons, 46ff, 55
Interpretations, 60
Interventionism, 195
Intuition, 238
Investment, 127
Treland, 166
Isaac
commentators and, 203
fought God, 188, 189
interventionist, 195
present-oriented, 187
stew, 187
word af, 191
Islam, 77
Israel (Jacob), 201, 202
Tsrael of God, 454

Jabin, 185
Jablow, Eric, 121
Jacob, 193, 451
(Israel), 201f
limp, 202
old man, 198f, 208
Jael, 185, 194
Jaki, Stanley, 114m
James, Will, 135n

495

Jastrow, Robert, 374
Jeremiah, 82
Jesus
parables, 192f
power, 173
restored week, 70
sabbath and, 76
temptation of, 8
throne of David, 173
uncertainty of, 221
Jericho, 184
Jevons, W. S., 41, 49
Jewels, 78
Jews, 138, 216
Job, 164
Jordan, James, 189n
Joseph, 189n
authority of, 213f
buys Egypt, 234
competence, 219
Joshua, 108
Judgment, 119, 124, 142, 175f, 221,
267, 382, 417, Appendix E

Kant, 324, 351, 373, 375, 420,
evolutionist, 425
god of, 4

Karate, 137

Karina, 365

Keepers, 1394

Kelvin, Lord, 409

Kennedy, John F., 30

Kepler, 371

Kerouae, Jack, 134n

Kidner, Derek, 13n

Klimister, Clive, 14n

Kline, Meredith, 452n

Kingdom of God, xxxiii

King, Martin Luther, Jr., 19n

Kirzner, Israel
aggregates, 348
aggregation, 55
Becker and, 347
equilibrium, 350f
holism, 57
individualism, 347, 348
per capita capital, 61
