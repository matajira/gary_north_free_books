From Cosmic Purposelessness to Humanistic Sovereignty 269

biological evolution, which also continues.”®? In other words, “The
evolutionary process is not moral—the word is simply irrelevant in
that connection — but it has finally produced a moral animal. Con-
spicuous among his moral attributes is a-sense of responsibility. . . .
In the post-Darwinian world another answer seems fairly clear: man

  

is responsible to himself and for himself. ‘Himself’ here means the
whole human species, not only the individual and certainly not just
those of a certain color of hair or cast of features.”6° Man, meaning
collective man or species man, is sovercign. Individuals are responsible
to. this collective entity.

Simpson makes his position crystal clear. “Man is a glorious and
unique species of animal. The species originated by evolution, it is
still actively evolving, and it will continue to evolve. Future evolu-
tion could raise man to superb heights as yet hardly glimpsed, but it
will not automatically do so. As far as can now be foreseen, evolu-
tionary degeneration is at least as likely in our future as is further
progress. The only way to cnsure a progressive evolutionary future
for mankind is for man himsclf to take a hand in the proccss.
Although much further knowledge is needed, it.is unquestionably
possible for man to guide his own evolution (within limits) along
desirable lines. But the great weight of the most widespread current
beliefs and institutions is against even attempting such guidance. If
there is any hope, it is this: that there may be an increasing number
of people who face this dilemma squarely and honestly seek a way
out.”6! With these words, Simpson ends his book.

Are Simpson and Dobzhansky representative of post-Darwinian
evolutionism? They are. It is difficult to find biologists who do not
take this approach when they address themselves to these problems.
Many, of course, remain silent, content to perform the most prosaic
tasks of what Thomas Kuhn has called “normal science.” When
they speak out on the great questions of cosmology, however, thcir
words are basically the same as Simpson’s.

59. Tid, p. 24.

60. Thid., p. 25.

61. Lid, p. 285.

62. Thomas Kuhn, The Structure of Scientific Revolutions (2nd ed.; Chicago:
University of Chicago Press, 1970), coined this phrase. For an extended discussion
of Kubn’s important distinction between “normal science” und “revolutionary
seience,” see Imre Lakatos [LakaTOSH] and A. E. Musgrave (eds.), Criticism and
the Growth of Knowledge (Cambridge University Press, 1970).
