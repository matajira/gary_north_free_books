12
PRIMITIVE NOMADS

And Abel was a keeper of sheep, but Cain was a tiller of the ground (Gen.
4:2).

There has been no myth cherished more in twentieth-century
anthropological scholarship than the mid-nineteenth-century
hypothesis concerning primitive man, the nomad. According to
widely varying but ever-lengthening estimates, man—or some
animal quite close to man genetically —first appeared on the earth
about three million years ago. No doubt this estimate will be woe-
fully conservative in future years, given the fact that this estimate
grew from 500,000 years at the absolute outside (and probably closer
to 250,000 years)—the standard account in the late 1950’s!—
to-1.5 million years in 1961 (Louis Leakey’s African discoveries) to
2.5 million years in 1974 (Richard Leakey)? to as much as 3.5
million years in 1976 (Richard Leakey)? It startled archeologists
when 150 bones from a group of “manlike individuals” who lived
together in a family or troop were discovered in Ethiopia in the
mid-1970’s, for they seemed to have perished about three million
years ago, Anthropologists had always believed that human social
groups were relatively recent, dating back only 60,000 years.*

However long ago it may have been, these creatures supposedly
roamed alone or in packs, eating wild berries or other plants that
grew wild, hunting animals, and drifting with the productivity of
nature. Then, about 10,000 years ago, men in the fertile crescent
region of the Near East discovered the skills of animal husbandry
and agriculture. Shepard Clough’s evaluation is representative:
“Here was one of the major technological revolutions of all time,

. Loren Eiseley, The Immense Journey (New York: Vintage, 1957), pp. 115-18.
, Newsweek (July 15, 1974).
. Newsweek (March 22, 1976)
Idem.

we ue po re

132
