General Introduction to The Dominion Covenant xiii

pline, it becomes necessary for the nation to go through the painful process
of testing and maturation. God must teach His people the consequences of
irresponsible decisions. The forty years of wilderness experience transforms
them from a rabble of ex-slaves into a nation ready to take the Promised
Land. Numbers begins with the old generation (1:1-10:10), moves through a
tragic transitional period (10:11-25:18), and ends with the new generation
(26-36) at the doorway to the land of Canaan.®

Deuteronomy is the book of the inheritance, point five of the bib-
lical covenant model. “It is addressed to the new generation destined
to possess the land of promise — those who survived the forty years of
wilderness wandering.” The children of the generation of the ex-
odus renew their covenant with God and inherit Ganaan on this
basis. Moses blesses the tribes (Deut. 33), a traditional sign of inher-
itance in the Old Testament (Gen. 27; 49). Moses dies outside the
land, but before he dies, God allows him to look from Mt. Nebo into
the promised land (Deut. 34:4). He sees the inheritance. The book
closes with the elevation of Joshua to leadership, the transitional
event (Deut. 34:9-12).

Thus, the Pentateuch is itself revclatory of the structure of God’s
covenant. This cconomic commentary on the Pentateuch is there-
fore a commentary on a covenant. I call it the dominion covenant,
for it is the God-given, God-required assignment to mankind to ex-
ercise dominion and subdue the carth that defines mankind’s task as
the only creature who images God the Creator.

Covenant theology is inescapably dominion theology. God has
placed on His people the moral requirement of transforming the
world through the preaching of the gospel. He has also given man-
kind the tools of dominion, His laws.'5 This thought upsets all those
Calvinist amillennialists who reject as impossible and utopian the
postmillennial vision of the progressive manifestation of the king-
dom of God on earth. Dominion theology is inescapably covenant
theology. This thought upsets all these Arminian “positive confes-
sion” preachers who reject covenant theology and its call to compre-
hensive social transformation, and who prefer to limit the trans-

13. Tbid., p. 128.

14, Tbid., p. 171.

15, Gary North, ‘Tools of Dominion: The Case Laws of Exodus (Tyler, Texas: Institute
for Christian Reconstruction, 1987).

16. Gary North, ds the World Running Down? Crisis in the Christian Worldview (Tyler,
‘Yexas: Institute for Christian Economics, 1987), Appendix C: “Comprehensive
Redemption; A Theology for Social Action”
