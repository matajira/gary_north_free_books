The God-Designed Harmony of Interests 99

harmony of intcrests shall be restored. It is man’s task to oxtend the
kingdom of God on earth, and to begin to reduce the effects of the
sin-based disharmony of interests. It is this extension of God's
kingdom which scrves as the down payment (earncst) of that future
and final restoration of the full harmony of interests. Until then, all
that we can hope to accomplish is to minimize the disharmony of
interests by extending the rule of biblical law, which provides the
social framework of the harmony of interests. 3

The free market is, in the realm of economics, the most impor-
tant institutional arrangement that has resulted from the application
of biblical law to society. This is why we must affirm that free market
economics is biblical economies, and why all forms of socialism or collec-
tivism are the products of anti-biblical economics. This is why the
free market order is an important institutional means of reducing the
disharmony of interests by encouraging people voluntarily to mesh
their individual plans by means of private property, freely fluctuating
prices, and profit-and-loss statements.

 

13. It is important to understand that the division of labor within the family was
designed lo extznd men’s dominion over nature, The family unit was to be broken
with each generation, even before the Fall of man. Speaking of marriage, Adam
said: “Therefore shal! a man leave his father and his mother, and shall cleave unto
his wife: and they shall be one flesh” (Gen. 2:24). The harmony of the family before
the Fall was never to be intended to keep sons and daughters in the same immediate
houschold. They werc to leave, to bring the whole carth under dominion. After the
Fall this pattern became even more important for the preservation of both harmony
and dominion. In the mid-seventcenth century, the Massachusetts town of Sudbury
was split between the older gencration, which wanted to control access lo common
lands in the town, and younger men, who wanted freedom. Kventually, the younger
men simply walked out of town, moved a few miles away, and established the town
of Marlborough. ‘Lhis was the dominion aspect of the nuclear family in action. (See
Suroner Chilton Powell, Puritan Village: The Formation of a New England Town {Mid-
dietown, Conneciicut: Wesleyan University Press, 1963]). Isaac did not live wich
Abraham; Jacob did not live with Isaac, The so-called nuclear family of the Chris-
tian West is the biblical standard. The hierarchical patriarchy of Central European
cultures, where sons remain under the immediate jurisdiction of the father, or
grandfather, oven to the point of dwelling under the same roof, is a non-biblical
altemative to the nuclear family —an alternative which reduces haxshony and geo-
graphical dominion. It is the nuclear family, not the clan order of classical civiliza-
tion and other cultures, which is sociologically normative. It is also interesting to
note that when immigrants from Central European cultures settle in Western
Europe or North America, the patriarchial family orders are abandoned within a
generation or two. They simply cannot compete with the biblical family pattern.
Young men who are not compelled to put up with patriarchal authoritarianism
choose the nuclear famity. And on this point, their wives are in total agreement.
They prefer to be subordinate to one man, not two, plus another wornan. It is difficult
to serve two (or more) muasicrs, The nuclear family provides maximum harmony.

 

 

 

  

 
