16
INVESTMENT AND CHARACTER

Now the Lorb had said unto Abram, Get thee out of thy country, and
from thy kindred, and from thy father’s house, unto a land that I will
shew thee: And I witl make of thee a great nation, and I will bless thee,
and make thy name great; and thou shalt be a blessing. . . (Gen.
12:1-2).

It was not to some poor man that God came with His command;
it was to Abram, a wealthy man who was “very rich in cattle, in
silver, and in gold” (13:2). These three commodities were basic signs
of wealth throughout Old Testament times, and all three served as
money, especially the two precious metals. Abram’s wealth was
mobile, which is understandable, given the fact that he had already
been uprooted once before, when his father left Ur of the Chaldees,
heading for the land of Canaan, stopping in Haran and settling
there (11:31), Now he was being called upon to move again, to con-
tinue the journey begun by his father.

Abram’s nephew Lot, who was also wealthy, decided to accom-
pany Abram. The two families held their wealth in the form of cat-
tle, and so great were the herds that the land in any particular area
of Canaan was not capable of sustaining all of them (13:6), The
result was conflict between herdsmen of the two families (13:7). The
original patriarchs — Abram of the Israelites and Lot of the Moabites
and Ammonites (Gen. 19:37)—were men possessing great capital
resources, God in no way questioned the legitimacy of their wealth.
He did not call them to redistribute it to the people of Ur of the
Chaldees, of Haran, or of Canaan.

Each man had a capital base to work with. The history of the two
men illustrates a fundamental aspect of biblical economics, namely,
the strong relationship in the long run between character and wealth
(Prov, 13:22}. More precisely, there is a relationship between the

156
