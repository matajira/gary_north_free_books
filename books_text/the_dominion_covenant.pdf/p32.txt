XxXii THE DOMINION COVENANT: GENESIS

years ago. Ironically, it was in the late-seventeenth century that
Christian casuistry—the application of biblical principles to daily
life ~ began to decline, It was also the century in which hypotheti-
cally neutral economics began to be promulgated, an intellectual in-
novation described by William Letwin in his book, Origins of Scientific
Economics (1963, 1965). The fact that it has taken until the final
decades of the twentieth century to get into print an economic com-
mentary on one book of the Bible is a testimony to the systematic,
conscious retreat from the world of scholarship and practical wisdom
on the part of those who call themsclves Christians. Why haven’t
Christian economists written numerous economic commentaries on
the Bible, at least one each century, and preferably one each genera-
tion? Has it been that Christian scholars have been suffering from an
intellectual illusion, namely, that there is @ zone of neutral scholarship
which provides Christians with all the data and logic they need, even
though the work is being produced by men who believe that there is
no God, and if there were, it could not be the God which the Bible
presents?

Consider the implications of the statement, “There is no such
thing as a distinctly Christian economics [psychology, political
theory, education, etc.].” First, God has not spoken to His peuple
with respect to how they should think and live. He remains silent,
providing them with no ethical guidelines. He does not answer His
people when, they ask Him, “How shall we then live?” Second, the
Bible is not a comprehensive book. The “whole counsel of God” is
simply the call to repentance. But in specific terms, the Bible does
not tcll us, “Repentance from what?” The Bible is a book appealing
to the heart of man, but the heart has no communication with the
mind in areas outside of church policy, evangelism, and—at the
most—family life. Third, the Bible gives the world over to Satan and
his rebellious hordes. Not that they have stolen something from
God, but that God gave this world to them, At the very least, they
possess it by default, since God has not established guidelines. He
does not really own the world, even though He says that He does
(Ps, 50:10-12). God has not established rules for lawful stewardship
and administration of His property, Satan and his followers have
broken no laws of economics, for there are no laws of economics. Or
if there are such laws, they are common to every culture, and we do
not need the Bible to tell us what they are. Again, we arc back to the
premise of neutrality. Fourth, there are no specifically biblical stand-
