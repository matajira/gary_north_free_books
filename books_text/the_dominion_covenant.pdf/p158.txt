114 THE DOMINION COVENANT: GENESIS

numbers would be limited by the thorns and thistles that clogged up
the formerly abundant productivity of the land. The fulfillment
would of necessity be linear, but the new law of nature was cyclical.
To overcome this cyclical restraint, covenantally faithful men must
apply the principles of biblical law. Linearity of economic growth, of
the growth of both human and animal populations, is now a product
of cthically faithful societies (Ex. 23:26). Linear development is not
natural in the post-Fall world. Linear development is the product of a phi-
losophy of life, a religious outlook, and few cultures in history have maintained
anything like i. Paganism promotes a cyclical view of life, using the
regularities of the cursed, post-Fall agricultural world as its standard
of human development. Cursed cyclical nature has become norma-
tive for pagan social thought.?

Common Curse, Common Grace

We generally focus our attention on Adam’s plight and the
ground’s curse. We see mostly wrath in both. Nevertheless, there
was also gracc in both curses, since we define grace as an unearned
gift of God to man or the crcation. As in all manifestations of God’s
common wrath, there was also common grace. This grace-curse pro-
duced special curses for the rebellious and special benefits for God’s
elect.

Adam, by rebelling, deformed the nature of man. Men would no
longer naturally cooperate with each other in the tasks of dominion.
Because of the murder in their hearts, they would search for ways of
stealing from their fellow men and killing them. Man had rebelled
against God; man’s descendents would normally seek to destroy all
those made in God’s image. Mankind therefore needed external and
internal restraints in order to survive. Men were now alienated from
each other because they were alienated from God. Something was

 

2. Stanley Jaki, the historian of science, contrasts the cyclical views held by the
Chinese, Hindus, Grecks, Babylonians, Mayans, and Arabs with the linear view of
orthodox Christianity. Why did science develop only within the intellectual frame-
work of the Christian West? As he writes: “Needless to say, many factors—geo-
graphical, social, economical, and political— played a part in the stillbireh of the
scientific enterprise in the various ancient cultures. The only common factor in all
cases seems, however, to be the commilment to the cyclic world view” Jaki, “I'he
History of Science and the Idea of an Oscillating Universe,’ in Wolfgang Yourgrau
and Allen D. Breck (eds.), Cosmalogy, Hisiory, and Theology (New York: Plenum Press,
1977), p. 140n. He develops this idea at considerable length in his book, Science and
Creation: From Eternal Cycles to an Oscillating Universe (Edinburgh: Scottish Academic
Press, [1974] 1980).

  
