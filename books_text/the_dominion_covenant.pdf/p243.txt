Contingency Planning 199

even though Benjamin was born after Joseph. No events are
recorded between the death of Rachel at the birth of Benjamin
(35:19) and Jacob's visit to his father (35:26) and his father’s death at
age 180 (35:27). Jacob was then about 120 years old, since he was
born twenty years after Isaac’s marriage, when Isaac was 60 (25:26),
Jacob was 104 when he met Esau’s forces. Jacob, therefore, was tak-
ing great care to preserve half of his capital for the sake of his family,
since at his advanced age, there was no guarantee that he would be
able to recoup his losses if Esau took everything Jacob owned. He
was very probably running short of economically productive years,
so capital preservation was far more important than it would have
been had he been younger. The economic strategy of an older man is
understandably different from that which might appeal to a younger
man who has time to recover from mistakes.

Only after he had taken what he regarded as effective contingency
planning did he go to God in forthright prayer (32:9-12). He then
pleaded with God to uphold His promises to him, though admitting
freely that “I am not worthy of the least of all these mercies, and of all
the truth, which thou has shewed unto thy servant . . .” (32:10). He
reminded God of God’s own covenant with him, to uphold him and
bless him, but he did not assume that God was in any way bound to
honor Jacob’s temporary interpretation of the meaning of the terms
of the covenant in that particular situation. He did not sit by idly,
waiting for God’s automatic seal of approval on his own self-
confident decisions. Jacob had already taken prudent steps to
preserve a portion of his capital before coming to God. He acted sen-
sibly, and he did so almost automatically, knowing from experience
that God is in no way morally compelled to honor foolishness or
lethargy.

Jacob did not leave off at this point. He adopted a further tactic
to use against his brother. He decided to buy him off. In this case,
however, he did not assume that Esau could be pacified with a mess
of pottage. It would be very expensive, but well worth it if he could
stay the hand of Esau without conflict. He separated numerous
animals from the main flock and divided them into smaller.groups.
He then commanded his servants to go in small droves, one at a
time, delivering multiple peace offerings to Esau (32:13-21). Esau

1. We have already seen thar Jacob was 98 when Joseph was born: footnote #3 in
the previous chapter. He left Laban’s service six years later (Gen. 31:38).
