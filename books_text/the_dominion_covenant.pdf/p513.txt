Witnesses and Judges 469

man’s law. As Cornelius Van Til has said, each side makes its living
by taking in the other side’s laundry.

Both natural law theory and positive law theory are apostate,
Both cry out together against the universally binding nature of God’s
revealed law. Both sides define justice in terms of what man can dis-
cover and enforce, not in terms of what God has declared, has en-
forced, and will bring to final judgment.

It is more common for self-styled Christian social, political, and
legal theorists to declare the doctrines of natural law. Natural law
seems at first glance to be closer to a concept of eternal law made by
God. Natural law theorists can also appeal to the fatherhood of God
{Acts 17:26) as the foundation of their universal valid categories of
law. But the fatherhood of God is a doctrine that condemns man, for
it points to fallen man’s position as a disinkerited covenant-breaker, not
an ethical son. How can a disinherited son agree with an adopted
son about the nature of their mutual responsibilities to themsclves
and to the Father, let alone agree about the final distribution of the
inheritance? Did Isaac and Ishmael agree? Did Jacob and Esau
agree? Did Cain and Abel?

What was the “natural law” aspect of Ged’s prohibition against
eating of the tree of the knowledge of good and evil? Satan at first
tried to lure Eve into eating by an appeal to what appeared to be a
universal law. Hadn’t God said that they could eat of every tree in
the garden? In other words, why not eat of this one tree? Eve replied
appropriately; God has forbidden us to eat of this particular tree.
This was a specific revelation to her husband. If she had stuck with
her initial resistance, Satan would have been thwarted in his plans.
If man had relied on natural law theory to guide his actions, he would not have
offered even this token reststance to the temptation.

It is not surprising to find that those Christian scholars who have
been most.open in their denial of the continuing applicability of re-
vealed Old Testament law have also been vociferous promoters of
some version of natural law theory. Natural law theory offers them a
time-honored, man-made covering for their shame, for they fear be-
ing exposed as unfashionably dressed in the eyes of their humanist
colleagues. Natural law theory is the conservative antinomian Christian's
fashion preference in the world of fig leaf coverings. The “bloody skins of
God-slaughtered animals’—the forthrightly biblical morality of
God-revealed Iaw—are just not adequate for him.
