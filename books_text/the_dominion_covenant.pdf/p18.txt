xviii THE DOMINION COVENANT: GENESIS

humanist social order. With so few pessimillennial authors devot-
ing themselves to such detailed intellectual work, the intellectual
leadership of such practical efforts necessarily and steadily falls by
default to theonomic (God’s law) postmillennialists. Simultaneously,
postmillennialist scholars, because they do believe that such compre-
hensive social transformation is not only possible but inevitable,
work hard to achieve dominion in history.

There is a secondary psychological motivation in all this. Be-
cause the premillennialist expects defeat in history, he does not want
his name associated with some futile social project. He does not want
to be publicly embarrassed retroactively for his naive efforts to build
a successful long-term institution devoted to social transformation in
history. Pessimillennialists expect either 1) nearly total progressive
defeat for the church in history (amillennialism) or else 2) victory for
Christians only on the basis of Christ’s discontinuous, miraculous,
physical intervention into history (premillennialism) to change the
course of the church’s futile efforts to transform society through
preaching the gospel and cbheying God’s law. They have no confi-
dence in the patient, day-by-day work of Christians to make this
world a bettcr. place to live in. They have no confidence in the con-
tinuity of history and the continuity of God’s covenant promises.
They see the fulfillment of God’s covenant promises only through the
discontinuous overcoming of history, either when Christ returns in
final judgment (amillennialism) or when He comes to set up a cen-
tralized, top-down, bureaucratic, one-world State (premillennial-
ism). ‘hus, pessimillennialists do not expect Christians ever to be in
positions of leadership in colleges and universities, nor do they ex-
pect future Christian historians to be in a position to write histories
explaining how the efforts of earlier Christians led to the present
triumph of the gospel,

Postmillennialists, on the other hand, believe that such efforts to
transform society will eventually be successful, and they would like
to be the people who personally lay the foundations for this future re-
construction of society. They want to leave a visible mark in history.
They want to be footnoted as the key transitional figures in transfor-
mation of humanist civilization into Christian civilization. They
want to be treated favorably in the history books of the future. They

19. See the multi-valume set, the Biblical Blueprints Series, published by Domin-
ion Press, Ft. Worth, Texas, which began to appear in late 1986.
