8
THE GOD-DESIGNED HARMONY OF INTERESTS

And Adam gave names to all cattle, and to the fowl of the air, and to every
beast of the field; but for Adam there was not found an help meet for him.
And the Lorp God caused a deep steep to fall upon Adam, and he slepi:
and he took one of his ribs, and closed up the flesh instead thereof; and the
rib which the Lonp God had taken from man, made he a woman, and
brought her unto the man. And Adam said, This is now bone of my
bones, and flesh of my flesh: she shall be called Woman, because she was
taken out of Man (Gen. 2:20-23).

Adam had demonstrated his competence by naming the animals,
his first completed assignment. He had begun to work out the com-
mandments of God. By engaging in specific labor, he had begun to
extend his control over the creation, thereby beginning the historical
fulfillment of his own nature, He was asserting his legitimate, subor-
dinate sovereignty over the creation. Only after he had demonstrated
skills in his calling was he provided with a wife. The husband’s calf-
ing is therefore basic to marriage. It is supposed to be antecedent to
marriage.

This point cannot be overstressed. The animals were simultane-
ously created male and female from the beginning. Sexual reproduc-
tion and the multiplication of each kind’s numbers were the product
of the male-female division. But Adam was created before the
woman, The assignment of cultural dominion was given to a
representative head of the family of man, even before there was an
historically existing family. The heart of man’s being is not his sexuality, it
is his calling before God. He is fundamentally different from the
animals. Where sexuality is made the foundation of marriage, rather
than the calling, cultural development will be retarded. The male-
female relationship, in the case of mankind, is not based on the fact
of biological reproduction; it is not, in some evolutionary sense, the

90
