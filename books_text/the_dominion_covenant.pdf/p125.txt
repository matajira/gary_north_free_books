The Value of Gold 81

is the righteous judgment of God! This same comparison is used
repeatedly by biblical writers (Ps. 119:72, 127; Pr. 3:14; 8:10, 19;
16:16; etc.). Even the New Jerusalem, God’s final and most glorious
physical gift to redeemed mankind, is referred to as pure gold (Rev.
21:18). From the garden of Eden to the New Jerusalem, gold is
wealth.

Monopoly and Dross

It is when men as citizens or government officials tamper with
the gold and silver content of the currency that disaster results. When
men’s hearts are dross, they risk the production of dross currency
and dross consumer goods (Isa. 1:22). The boom-bust business cycle
is one of the disastrous consequences of currency debasement. Kings
and central bankers have practiced this monetary deception for as
long as there have been kings and central bankers. They pour less
expensive (base) metals into the silver or gold used to cast ingots, or
cains; they substitute paper notes or checks or computer entries on
magnetic tapes for the precious metals, and then they multiply the
notes, checks, or computer entries. Money multiplies, prices rise,
and the redistribution of wealth through deception increases. The
civil government fosters fraud, either directly (debasement, printing
press money) or indirectly (central and commercial banking). When
the authorities of the civil government stamp a coin or bill with a seal
testifying that a particular quantity and fineness of a precious metal
is contained in a coin (or a specific quantity of this metal is on re-
serve for immediate exchange of the paper note), and subsequently
they debase the coinage or print more bills than there is metal on re-
serve, they thereby act fraudulently. They first create a monopoly of
money issue, and then they misuse this government monopoly. They
spend the fat money into circulation, buying up the market's scarce
economic resources, The State thereby increases its consumption by
levying the “invisible tax” of monetary inflation.

The monopoly of money is fraught with danger for all but the
most alert private citizens and the beneficiaries of State favors. The
authorities cannot long resist the temptation of levying the invisible
tax of price inflation. It is true that Byzantium was blessed with a
gold coinage for 800 years, but this was unique in man’s history.”

2. Charles Weber, “A Closer Look at Gold,” The Freeman (Sept., 1972), pp.
337-38.
