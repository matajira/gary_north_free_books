396 THE DOMINION COVENANT: GENESIS

autonomy was born, It was an auspicious beginning. !'9

Wallace had been thinking about the problem for almost a dec-
ade. He had wondered why some men live and some men die. “And
the answer was clearly, that on the whole the best fitted live.” He
might have said simply, those who survive do, in fact, survive. But
that would never have satisfied a scientist like Wallace. “From the
effects of disease the most healthy escaped” — you can’t fault his logic
here, certainly—“from enemies, the strongest, the swiftest, or the
most cunning; from famine, the best hunters or those with the best
digestion; and so on.” A skeptic might not be very impressed so far,
but you have to remember that the man was suffering from a fever.
“Then it suddenly flashed upon me that this self-acting process would
necessarily improve the race, because in every generation the inferior
would inevitably be killed and the superior would remain — that is, the
Jitest would survive.”'?© This is the Darwinian theory of evolution, with-
out its footnotes, intricate arguments, flank-covering, and graphs.

There are two answers to this perspective. First, the absolute sover-
eigniy of God: “So then it is not of him that willeth nor of him that run-
neth, but of God that sheweth mercy” (Rom, 9:16). The other is that
of the philosophy of pure contingency, described so wonderfully in Ec-
clesiastes: “I returned, and saw under the sun, that the race is not to
the swift, nor the battle to the strong, neither yet bread to the wise,
nor yet riches to men of understanding, nor yet favour to men of
skill, but time and chance happeneth to them all. For man also
knoweth not his time: as the fishes that are taken in an evil net, and as
the birds that are caught in the snare; so are the sons of men snared in
an evil time, when it falleth suddenly upon them” (Ecc. 9:11-12),

Pure contingency or God’s sovereignty: neither satisfied Alfred
Russel Wallace, Charles Darwin, or the myriad of their monograph-
writing followers. Somewhere in the randomness that overtakes the
individual, the evolutionists believe, there has to be some stability —
impersonal, laws-of-probability-obeying stability. Thomas Huxley,
Darwin's unofficial hatchet-man and progenitor of that remarkable
family of professional skeptics —skeptics except where evolution was
concerned — stated his faith quite eloquently: chance is really quite
orderly, all things considered, and totally sovereign in any case. This
is the testament of modern evolutionary thought: “It is said that he

119. Alfred Russel Wallace, My Life (New York: Dodd, Mead, 1905), 1, p. 361.
120. fbid., 1, p, 362.
