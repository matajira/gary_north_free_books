394 THE DOMINIGN COVENANT! GENESIS

defend his researches? First, he defended the Mosaic record as being
most in conformity with his views. Then he said that it was God’s ex-
pressions of will, not His direct activities, that brought forth the
creation. (He ignored, of course, the orthodox doctrine of the verbal
creation, that is, the response out of nothing to the command of
God.) God created all life; he stated that he took this for granted. “In
what way was the creation of animated beings effected? The or-
dinary notion [that is, the debased doctrine of successive creations
over endless ages—G.N.] may, I think, be described as this, — that
the Almighty Author produced the progenitors of all existing species
by some sort of personal or immediate exertion.” So he allowed God
to create life. But he then proceeded to ridicule the “orthodox” crea-
tionism of his day, that disastrous fusion of geologic time, unifor-
mitarian change, and successive creations: “How can we suppose an
immediate exertion of this creative power at one time to produce
zoophytes, another time to add a few marine mollusks, another to
bring in one or two crustacea, again to produce crustaceous fishes,
again perfect fishes, and so on to the end? This would surely be to
take a very mean view of the Creative Power... . And yet this
would be unavoidable; for that the organic creation was thus pro-
ive through a long space of time, rests on evidence which
nothing can overturn or gainsay. Some other idea must then be come
to with regard to the made in which the Divine Author proceeded in
the organic creation,”116

It should be obvious that the progression described by Chambers
is correct: given the idea of vast geological time, fossils distributed in
layers, uniformitarian change —and it was, by 1840, a single idea—
God’s creative interventions do look foolish. So a new mode of crea-
tion is offered: organic evolution. In two sentences, Chambers takes us
from Newton’s cosmic impersonalism for the heavens (not that
Newion intended such a conclusion) into a hypothetically imper-
sonal world of biological law: “We have seen powerful evidence, that
the construction of this globe and its associates, and inferentially that
of all the other globes of space, was the result, not of any immediate
or personal exertion on the part of the Deity, but of natural laws
which are expressions of his will. What is to hinder our supposing
that the organic creation is also a result of natural laws, which

 

116. [Robert Chambers}, Vistiges of the Natural History of Creation (4th ed.; Soho,
London: John Churchill, 1845), pp. 157-58. Te sold 24,000 copics, 1844-60: Darwin's
Century, p. 133.
