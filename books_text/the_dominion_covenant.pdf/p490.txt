446 THE DOMINION GOVENAN':! GENESIS

James 1:17). Thus, when the devil offered Christ the world in return
for Christ’s worship of him, he was making an impossible offer
(Matt. 4:9). It was not his to give.

‘Lhe cosmic personalism of the Bible’s universe is obviously in
total opposition to the autonomous multiverse of modern man. This
is God’s universe. He brings blessings and curses as He sees fit ( Job
38-41), but He has covenanted Himself to bring earthly blessings
and troubles to communities (though not necessarily to individuals)
in terms of their covenantal responses to Him. Deuteronomy 8 and
28 outline this relationship: blessings for obedience; curses for
rebellion. All human sovereignties are derivative. All attempts to
escape the limitations set by God on the exercise of property rights
are therefore self-defeating.

The Good Creation

“And God saw every thing that he had made, and, behold, it was
very good. And the evening and the morning were the sixth day”
(Gen. 1:31). The creation was originally good. ‘This included even
Satan himself. At a point in time (presumably after the seventh day
of creation-rest), he rebelled. His own pride was his downfall (I:
14:12-15). He then led Adam and Eve into this same path of destruc-
tion (Gen. 3). As Van Til has pointed out so well, our parents in
Eden were tempted to think of themselves as determiners of reality.
They would test God's word to see if it would hold true. They placed
their own logic and interpretation of the universe on a level with
God’s interpretation. Thus, they viewed the universe as prob-
lematical and therefore God's word as problematical. They denied
the absolule sovereignty of God’s word over history and nature, It
was this that constituted the Fall —knowing (determining) not only
good and evil, but also knowing (determining) the possible and im-
possible.®

Through Adam, sin entered the world (Rom. 5:12), Man’s rebel-
lion, like Satan’s, was therefore ethical, not metaphysical, It was not
some flaw in man’s being, but a willful rejection of God’s sover-
eignty. It was an attctmpt to play God. It was a matter of purpose
and will, not a defect in creation. Man did not slide into a lower
realm of “Being in general”; he simply rebelled. Sin, therefore, is not
a built-in eternal aspect of the creation. The fault was in the will of

 

 

18. Van Til, Survey of Christian Epistemology, pp. 19-20.
