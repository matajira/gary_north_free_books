The Entrepreneurial Function 223

tion of the good or service are underpriced in relation to what they
would be if all entrepreneurs recognized the true state of future con-
sumer demand. The entrepreneurs are middlemen for consumers, actual
surrogates for them. They enter the markets for production goods
and compete among each other in order to buy them, but always
because they intend to sell the results of production to consumers. If
an entrepreneur sees that certain factors of production are presently
underpriced in relation to what consumers in the future really will be
willing and able to pay for them in the form of final consumer goods,
then he has a profit opportunity. (Of course, he has to pay a rate of
interest, since future goods are always discounted in comparison to
what people will pay for present goods, and he has to tie up the use
of the searce resources until the time he can get the finished products
to market.) The entrepreneur enters the market and begins to buy
up production goods—land, labor, capital —in order to manufacture
the consumer goods. Or he may simply rent these factors of produc-
tion. In any case, he removes them fram the marketplace for a specific pertod
of time. When he brings the final products to market as finished con-
sumer goods, he raises their price to the level determined by the
competitive auction bids of consumers. In short, he makes his profit
by estimating in advance what future consumers, in bidding against
each other, will be willing to pay in a free market for the output of his
production process,

The entrepreneur does not compete against consumers, except in
so far as there are zones of ignorance in the minds of both consumers
and his competitors, other potential sellers, concerning the market
price of the goods or services. In a highly competitive market, these
zones of ignorance are drastically reduced. People know pretty well
what items sell for in the marketplace. The entrepreneur is alwaps
competing against other entrepreneurs—the middlemen who act for the
benefit of consumers — who also produce in order to meet future con-
sumer demand. When the finished consumer goods or services are
offered for sale, the “auctioneer”—the seller of goods—is guided by
the competitive bids of competing consumers, He is unable to set
any price he wants to set, though of course he prefers to receive a
high price. He sets his price in response to consumer bids, so as to
“clear the market.” He wants to sell every item scheduled for sale in
the particular time period. He cannot squeeze any more money out
of the consumers than they are willing and able to pay. Other sellers
