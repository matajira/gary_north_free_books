Appendix A

FROM COSMIC PURPOSELESSNESS
TO HUMANISTIC SOVEREIGNTY

Through billions of years of blind mutation, pressing against the shifting
walls of their environment, microbes finally emerged as man. We are no
longer blind, at least we are beginning to be conscious of what has hap-
pened and of what may happen. From now on, evolution is what we
make tt...

So writes Dr. Hermann J. Muller, the 1946 Nobel Prize winner
in physiology.! Muller has stated his position quite clearly, His state-
ment of faith is almost universally believed within scientific and in-
tellectual circles in the final years of the twentieth century. The idea
is commonplace, part of the “conventional wisdom” of the age.
Theodosius Dobzhansky, a zoologist at Columbia University and an
influential scholar in the United States from the 1930's through the
1970's, concluded his essay, “The Present Evolution of Man,” which
appeared in the widely read Scientific American (September, 1960),
with these words: “Yet man is the only product of biological evolution
who knows that he has evolved-and is evolving farther. He should be
able to replace the blind force of natural selection by conscious direc-
tion, based on his knowledge of his own nature and on his values. It
is as certain that such direction will be needed as it is questionable
whether man is ready to provide it. He is unready because his
knowledge of his own nature and its evolution is insufficient; because

1. Hermann J. Muller, “One Hundred Years Without Darwinism Are Enough,”
The Humanist, XIX (1959); reprinied in Philip Appleman (ed.), Darwin: A Norton
Critical Edition (New York: Norton, 1970), p. 570. 1 first saw an incomplete version of
this quotation in an article by Elisabeth Mann Borghese, “Human Nature Is Still
Evolving,” in ‘The Center Magazine (March/April, 1973), a publication of the now-
defunct Center for the Study of Democratic Institutions, a Santa Barbara humanist
think-tank which was influential in the 1950's and 1960's, It was founded by Robert
Maynard Hutchins. My point: this is a standard idea arnong liberals and humanists.

245
