420 THE DOMINION GOVENANT: GENESIS:

recognizes this. Instead of the fiat word of God—a discontinuous
event which created time and the universe—we are expected to
believe in the creativity of impersonal process. As Rushdoony
argues, “the moment creativity is transferred or to any degree ascribed
to the process ‘of being, to the inner powers of nature, to that extent
sovereignty and power are transferred from God to nature. Nature
having developed as a result of its creative process has within itself
inherently the laws of its bemg. God is an outsider to Nature, able to
give inspiration to men within Nature but unable to govern them
because He is not their Creator and hence not their source of law.”195
Is it any wonder, then, that the first modern cosmological evolu-
tionist, Immanuel Kant, was also the philosopher of the modern
world? Is it any wonder that his theory of the two realms—autono-
mous external and random “noumena” ys. scientific, mathematically
law-governed “phenomena” — is the foundation of the modern neo-
orthodox theology which has eroded both Protestantism and Cathol-
icism? Is it any wonder that Kant’s “god” is the lord of the noumenal
realm, without power to influence the external realm of science,
without even the power to speak to men directly, in terms of a ver-
bal, cognitive, creedal revelation? This is the god of process theol-
ogy, of evolution, of the modern world. It is the only god rebellious
men allow to exist. The God of Deuteronomy 8 and 28, who controls
famines, plagues, and pestilences in terms of the ethical response of
men to His law-word, is not the God of modern, apostate evolution-
ary science, He is not the god of process theology. The Christian
with the Ph.D. in geology who says that he just cannot see what proc-
ess has to do with the sovereignty of God is telling the truth: fe can-
not see. Had he been able to see, no “respectable” university would
ever have granted him a Ph.D. in geology, at least not in historical
geology. 196

The Bible does not teach the theology of process. It does not tell
us that an original chaos evolved into today’s order, and will become
even more orderly later. That is the theology of the Greeks, of the
East, and the modern evolutionist. It is not a part of the biblical
heritage. Even the so-called “chaos” of Genesis 1:2—“And the earth
was without form and void”—does not teach a “chaos into order”
scheme. Prof. Edward J. Young has offered considcrable proof of

195. Rushdoony, The Mythology of Science (Fairfax, Virginia: Thoburn Press,
11967] 1978), pp. 38-39.
196. Davis Young, Westminster Theological Journal (Spring, 1973), p. 272.
