Introduction xli

ary movements that swept over late-medieval and early modern
Europe, century after century, the sects’ leaders initially preached
total equality. Then, step by inevitable step, they imposed radical to-
talitarian hierarchy, usually with plurality of wives, but only for the
leaders.? They began in the name of radical individualism, and
ended in radical hierarchy.

There is no escape from hierarchy. Hierarchy is a covenantal
reality. It is never a question of “hierarchy vs. no hierarchy.” It is
always a question of whose hierarchy, Those who preach a world
without hierarchy, a world without dominion by covenant, are either
secking to confuse their victims, or else they are incredibly naive
accomplices of the power-seekers who do not want God's covenantal
hierarchy. Follow their doctrine of “no hierarchy” at your own risk.

3. Ethics/Dominion

Rushdoeny is correct when he writes: “The third characteristic of
the Biblical law or covenant is that it constitutes a plan for dominion
under God. God called Adam to exercise dominion in terms of God’s
revelation, God's law (Gen, 1:26ff.; 2:15-17). This same calling, after
the fall, was required of the godly line, and in Noah it was formally
renewed (Gen. 9:1-17)."8 Chapter Three of The Dominion Covenant:
Genesis is called, appropriately, “The Dominion Covenant.” It out-
lines the basics of God’s command to mankind, “subdue the earth.”
This is a command from God. It is not some after-thought on God's
part. It is basic to man’s very being. Men are commanded by Gad ta
subdue the earth. Thus, obedience requires dominion. Dominion
also requires obedience: God's work done in God's way. If man
rebels against God, he becomes a destroyer rather than a subduer.
The difference between subduing nature and exploiting nature is
ethics: conformity to God’s law. Thus, I write at the end of the chap-
ter: “, .. man’s fundamental tool of dominion is the moral law of
God” (p. 36). The connection between ethics and dominion cannot
be broken,

J. Norman Cohn, The Pursuit of the Millennium: Revolutionary messianism in medieval
and Reformation Europe and its bearing on modem totalitarian movements (2nd ed,; New
York: Harper Torchbook, [1957] 1961); Iyor Shafarevich, The Socialist Phenomenon
(New York: Harper & Row, [1975] 1980).

8. Institutes, p. 8.
