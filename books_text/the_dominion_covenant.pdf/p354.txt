310 THE DOMINION COVENANT: GENESIS

recognize the fundamental truth that it is not to apotheosize a few
exceptional intellects, but to render the great proletariat comfort-
able, that true civilization should aim.”?0? Tt has been the self-
imposed task of the believers in statist planning by elites to buy off
the proletariat by making proletarians comfortable —or promising to
make them comfortable soon, just as soon as the evolutionary leap of social
being takes place—throughout the twentieth century.

Ward, like all evolutionists, believed in the covenant of domin-
ion, or rather ¢ covenant of dominion, That covenant of dominion is
knowledge. Man elevates himself through knowledge. Man ts therefore
saved by knowledge. This is Satan’s temptation: ye shall be as gods, if
ye eat of the tree of the knowledge of good and evil. Ward wrote: “We
sce in this brief sketch what a dominion man exercises aver all
departments of nature, and we may safely conclude that he has not
yet reached the maximum limit of his power in this direction. But
that power is wholly due to his intellectual faculty, which has guided
his act in devising indirect means of accomplishing ends otherwise
unattainable.”203 Men are not innately evil. “Mankind, as a whole,
are honest.”#0t Man’s problem is not sin; it is ignorance. “If all the
people knew what course of action was for their best interest, they
would certainly pursue that course.”°5 It would be possible, through
education, to eliminate crime, “The inmates of our prisons are but
the victims of untoward circumstances, The murderer has but acted
out his education. Would you change his conduct, change his educa-
tion.”206

What we must do, then, is to raise soctety’s consciousness, Con-
sciousness, not conscience, is the problem. “After dynamic opinions
of the universe, of life, and of man have been formed, it is easy to
rise to the position from which society can be contemplated as pro-
gressive and subject to a central control. The duties of society toward
itself are manifest enough so soon as its true character can be under-
stood. , . . The great problem remains how to bring society to con-
sciousness, Assuming it to have been brought to consciousness, the
dynamic truths with which it must deal are. comparatively. plain. The

202. Ibid, U1, p. 368.
203. Ibid., TI, p. 385.
204. Tbid., U, p. 508.
205. Ibid, U, p. 238.
. Ibid., TL, p. 241.

 
