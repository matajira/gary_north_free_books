422 THE DOMINION COVENANT: GENESIS

Darwin’s purposeless evolution. They accepted each new scientific
“breakthrough” with glee. At best, each resistance attempt was a
three-stepped process: 1) it is not true; 2) it is not relevant, anyway;
3) we always knew it was true, and Christianity teaches it, and
teaches it better than any other system, No wonder Darwin w:
tated; a good, purposeless universe could not be left in peace by
these silly people!

The battle lines should be clear: Christianity or error, the-six-day

irri-

 

creation or chaos, purpose and meaning or cosmic impersonalism
and randomness. It is not hard to understand why the religion of
modernism clings to Darwinian thought. It is also not surprising
why occultist Max Hindel could write The Rosicructan Cosmo-
Conception or Mystic Christianity: An Elementary Treatise upon Man’s Past
Evolution, Present Constitution and Future Development (4909). It is not
surprising that the book has been reprinted in a cheap paperback as
recently as 1971, and that it is used in college classrooms. (I bought
my copy in a bookstore of an ostensibly conservative private col-
lege —not on a rack, but in the class section. One hopes it was used
simply for the purposes of criticism.) But why Christians should give
one second’s consideration of the possibility of evolution — ancient or
modern, occultist or scientific—is a mystery.

The compromise with uniformitarian principles has been a steady,
almost. uniformitarian process within Christian circles. Gillispie,
describing the steady capitulation of early nincteenth-century Chris-
tian naturalists, shows how disastrous the retreat was for orthodoxy.
At each stage, the Christians, copying the mythical act of King
Canute, shouted “thus far and no farther” to uniformitarianism.
“And at every stage except the last, progressives admitted that a fur-
ther step, the possibility of which they disavowed while they unwit-
tingly prepared it, would indeed have had serious implications for
orthodox religious fidelity.” But each new uniformitarian “dis-
covery” was assimilated into the supposedly orthodox framework
nonetheless, despite the fact that at every preceding capitulation, the
proponents of that compromise admitted that the next step (now
greeted passively or even enthusiastically} would be unnecessary,
impossible, and utterly wrong. (Any similarity between nineteenth-
century Christian progressives and today’s Christian progressives is
hardly coincidental.) The progressivists of the 1840's, like the com-

   

198, Gillispie, Genesis and Geology, p. 221.
