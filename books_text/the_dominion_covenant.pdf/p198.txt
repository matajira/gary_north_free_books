154 THE DOMINION COVENANT: GENESIS

imacy of international trade. In his Oratéones, he wrote: “God did not
bestow all products upon all parts of the earth, but distributed His
gifts over different regions, to the end that men might cultivate a so-
cial relationship because one would have necd of the help of the
other, And so he called commerce into being, that all men might be
able to have common enjoyment of the fruits of earth, no matter
where produced.”5 Basil and Chrysostom picked up this idea and
placed it within a Christian framework.6 Theodorct, the fifth-
century Bishop of Cyrus, a town about two days’ journey west of An-
tioch, held this view. We know he was influenced by the writings of
Chrysostom.’ Finally, St. Ambrose took up the idea, whose Hexam-
eron was an adaptation in Latin of Basil’s Greek title of the same
name (six days).® Not all the church fathers were equally favorable
to trade, but at least a tradition was established, one which found
adherents throughout the middle ages. As Jacob Viner stated in his
lecture before the American Philosophical Society in 1966—which
sadly he did not live to put into final, fully documented form as a
full-length book, as he had planned to do—“I have the impression
that there are few ideas of comparable age, subtlety, and prevalence
with the idea whose history I have been commenting on, which have
so often been received by modern scholars who encounter them in a
text as being both important and novel. The origin of the idca of the
interest of providence in commerce has been attributed by scholars
to Bodin, to Calvin, to an English scholastic of the fourteenth cen-
tury, Richard of Middleton, to an Italian Renaissance writer, L. B.
Alberti, to Grotius, and to any number of others.” For all we know,
it may be found in the writings of someone even earlier than Liban-
ius, but Viner had not discovered it earlier.

While it is not universally true that “where goods do not cross
borders, armies will’—that old ninctcenth-century slogan! —it is

5, Gited by Jacob Viner, The Role of Providence in the Soctal Order: An Essay in Intel-
lectual History (Phitadelphia: American Philosophical Society, 1972), pp. 36-37.

6. Ibid, p. 37.

7. “Theodorel,” in John McClintock and James Strong (eds.), Gpelopacdia of Bib-
tical, ‘Theological, and Ecclesiastical Literature (New York: Harper & Bros., 1894), X, p.
320.

8. Viner, Providence, p. 37.

9, Ibid, pp. 37-38.

10, “If men and commodities are prevented {rom crossing the borderlines, why
should not armies try to pave the way for them?” Lardwig von Mises, Human Action
(3rd ed.; Chicago: Regnery, 1966), p. 832.
