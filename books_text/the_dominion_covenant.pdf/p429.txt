Casmologies in Conflict: Creation vs. Evolution 385

read: “We believe that Scripture does not permit the interpretation of
the theistic evolutionist, We do believe that the data of Scripture per-
mit, although they do not require, the view that the days of Genesis
one were periods of time of indefinite length. Hence we believe that
the products of creation of the various days one through six were not
necessarily instantaneously produced in a mature state but were
formed over a long period of time. This view does have the advan-
tage of permitting the Christian geologist to interpret intelligibly the
actual data of geology.” It has the advantage of allowing a geologist
who is a Christian to interpret the Bible in terms of the geology and.
theology of 1840, when some men could still believe in numerous
special creations. The geology of 1859 or later, devoid of final causes,
purpose, interventions by God, or the need of reconciliation with the
Bible, has no space for God’s activity in between the autonomous
strata of the earth.

Galileo had begun the steady removal by autonomous men of
God from His universe, By the 1840’s, God’s last place of refuge
among scientists was in the realm of biology. Uniformitarianism
after 1830 had finally removed Him from the rocks. He was allowed
His various “special creations” from time to time among living be-
ings. “And while all these miraculous interpositions were taking
place in order to keep the organic kingdom in a going condition, the
Creator was not for a moment allowed, by most of these geologists
(including, as we shall see, Lyell and his followers) to interfere in a
similar manner in their own particular province of the inorganic proc-
esses. . . . So, in the opinion of most naturalists the only officially
licensed area in which miracles might be performed by the Creator
was the domain of organic phenomena.”®° Charles Darwin’s On the
Origin of Species repcaled the license even here. Thus, it is a sign of
the demoralization and naiveté of modern uniformitarian geologists
who claim to be Christian in their scholarship, that they expect the

98. Davis A. Young, “Some Practical Geological Problems in the Application of
the Mature Creation Doctrine,” Westminster Theological Journal, XXXV (Spring,
1973), p. 269. He is the son of Edward J. Young, author of Studies in Genesis One. A
reply to Young’s article appeared in the subsequent issue: John C. Whitcomb, Jr.,
“The Science of Historical Geology in the Light of the Biblical Doctrine of a Mature
Creation,” ibid., XXXVI (Fall, 1973). Young’s doctorate is in geology; Whitcomb’s
is in theology. Whitcomb is co-author of The Genesis Flood (1961), the most important
book in the revival of the six-day creation view of Genesis, for it helped a develop
the market for numerous additional studies along these lines in the 1960's.

99. Lovejoy, in Forerunners, p. 365.
