Introduction xxx

have set for myself, after they have finished this volume. There
should be no question in anyone’s mind that the Rible has a lot to say
about economics. They still may not be convinced that there is a
uniquely Christian economics, but there should be no question of the
large quantity of data for economics which the Bible presents to us,

This is a commentary. It should be useful for those biblical
scholars who are simply trying to exegete a passage for its inherent
meaning, and not just for thase who arc seeking strictly economic in-
formation. I have discovered over the last few ycars that conven-
tional commentaries are almost devoid of economic insight, and for
some verses, the economic ignorance of the writers has proved a
stumbling block, They have missed the point entirely in a few
cases—not simply the cconomic aspects of a particular verse, but the
major point of the verse (when the point happens to be primarily
economic).

There is no doubt that | am breaking new ground exegetically
and intellectually with this commentary. Pathbreakers are always
going to make mistakes. I will undoubtedly skip over some verse or
some aspect of a verse that relates to economics. I will undoubtedly
misinterpret some verses, or overemphasize the economic implica-
tions of some passage. Nevertheless, I think it is better to publish
and wait for the responses, if any, of the critics, and then revise a
later version of the commentary. By publishing it in sections, I have
made it possible to improve the final version. This gets the intellec-
tual division of labor working for me, and at low monetary cost,
since antagonistic crilics and nit-picking scholars are content to
point out my gricvous crrors free of charge, just so long as they think
their comments will make me look stupid and/or make them look
brilliant. [ am deliberately using this psychological characteristic of
reviewers and critics to my advantage and the advantage of the
kingdom, which will eventually receive an improved final version of
this work. My philosophy is that it is better to publish something
80% correct, especially when there is no comparable book available,
than to wait years to publish a book that is 90% correct, especially
when the price of printing keeps rising.

It is a sad commentary on Christian intellectual life that no com-
mentary like this has ever been attempted (as far as I have been able
to determine). In fact, it is sad that key men in every academic disci-
pline have not long been writing commentaries in their fields. These
should have becn begun at least 400 years ago, and certainly 300

 
