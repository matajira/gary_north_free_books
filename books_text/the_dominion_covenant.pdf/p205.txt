Envestment and Character 161

Conclusion

Godly men are instructed not to put their faith in earthly treasures
(capital), where thieves break in and rust corrupts (Matt. 6:19-21).
Men are to build in terms of Christian character and biblical law.
Their decisions are not to be guided primarily by the land in front of
them but by the human capital at hand. An investment in terms of
character may not reap immediate rewards. After all, Lot settled
down in temporary comfort, while Abram wandered. But Abram
became Abraham — “father of nations” — and his children and grand-
children were buried with him (Gen. 49:28-31; 50:13), while the bur-
ial location for Lot and his daughters is not mentioned. Abraham’s
commitment to character, and his reliance upon the covenantal
promises, brought him visible blessings and rest in his old age. Lot,
though a just man (II Pet. 2:7), dwelt where his spirit was endlessly
vexed (tormented) (II Pet. 2:8). He had left Haran with great
wealth; he would leave Sodom with only the items he could carry
away in an emergency retreat. He had traded internal peace for the
seeming promise of external blessings, and he ended his life with
neither peace nor external blessings.

God may, for a time, preserve the wealth of a rebellious culture
for His own purposes. He may preserve it for the sake of a few godly
men who dwell within the culture (Gen. 18:23-33). Nevertheless,
when He brings down His wrath upon a culture, the faithful may
have to make a grim and hasty retreat. (Forms of mobile capital,
such as gold, silver, and precious stones, are sensible investments for
Christians in times of social disintegration for this very reason. We
should not look back, but it is wise to take something for the future
along with us as we make our escape.)

 

 

book was written and published for the U.S. Labor Party, a strange splinter group
with populist-Marxist leanings. Some of its claims, especially concerning the total
economic profits derived from the drug trade, are preposterous. The documentation
is not always reliable. Nevertheless, it provides a needed revision and exposé of
dozens of negleeted topics in recent British and Asian history. It is unlikely that con-
ventional, tenured historians will follow through on the many leads provided by this
book, which is why such books have to be published by obscure, subsidized presses,
and written by innovative, but erratic, researchers. Paths are broken by energetic,
enthusiastic, innovative, and sometimes crackpot people who have no academic
reputations to risk, and few fears about getting some of the facts mixed up. The
tenured scholars came in later to lay down the asphalt and keep the sides of the road
trimmed neatly, and even beautifully, But without the pathbreakers, the gardeners
would scldom expand their intellecrual horizons.

 
