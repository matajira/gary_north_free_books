312 THE DOMINION COVENANT: GENESIS

at {to cite President Woodrow Wilson’s unheeded principle of
diplomacy)? It should be obvious. When Ward said that he wanted
unanimity, he really meant sctentific planning without opposition. “The
legislature must, therefore, as before maintained, be compared with
the workshop of the inventor.”?!° There is no opposition to the inven-
tor in his workshop, it should be pointed out.

Scientists must lead the legislators. Men of informed opinion
must tell them what needs to be done. Then the legislators can pass
laws that will compel the masses to follow the lead of the scientists in-
to a new realm of “comfort.” Ward was quite explicit about this: “The
problem is a difficult and complicated one, While legislators as a
class are far behind the few progressive individuals by whose
dynamic actions social progress is securcd, it is also true that, as a
general rule, they are somewhat in advance of the average consti-
tuent, sometimes considerably so. This is seen in many quasi-
scientific enterprises that they quietly continue, which their consti-
tucnts, could they know of them, would promptly condemn. The
question, therefore, arises whether the legislators may not find
means, as a work of supererogation, to place their constituents upon
the highway to a condition of intelligence which, when attained, will
in turn work out the problem of inaugurating a scientific legislature
and a system of scientific legislation.”2"' With these words, he ended
chapter XI, “Action.” (The Oxford English Dictionary defines “super-
erogation” as “The performance of good works beyond what God
commands or requires, which are held to constitute a state of merit
which the Church may dispense to others to make up for their defi-
ciencies.” Ward may have known what he was writing; the State, as
the dispenser of salvation, needs saints to build up merit to pass
along to the proletariat, who can do nothing by themselves, Scien-
tists and legislators are the saints.)

When Ward wrote “society,” he meant the State. “When we speak
of society, therefore, we must, for all practical purposes, con-
fine the conception to some single automatic nation or state or, at the
widest, to those few leading nations whose commercial relations
have to a considerable extent cemented their material interests and
unified their habits of thought and modes of life.” Yet even this is too
loose a definition, he wrote. “Only where actual legislation is con-

20. foid., Il, p. 396.
211. Hid., IL, p. 399.
