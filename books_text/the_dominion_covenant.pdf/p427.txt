Cosmologies in. Conflict: Creation vs. Evolution 383

definitive book. He had been a catastrophist until 1827; three years
later he was the premier uniformitarian in the English-speaking
world.

It is not easy to summarize Lyell’s work. He opposed the theory
of biological evolution until the late 1860's, yei it was sometime
around 1860 that the evangelical Christianity of his youth returned
to him.®? His commitment to uniformitarian principles of interpreta-
tion led him to view geological processes as if they were part of a
huge mechanism. He was familiar with the young science of paleon-
tology; he was aware of the fact that Jower strata (“older”) often con-
tained species that did not appear in the higher (“younger”) strata.
This seemed to point to both extinct species and completely new
(“recent”) species, indicating biological development, given the “fact”
of eons of time in between the geological strata. Yet Lyell resisted
this conclusion until 1867 — nine years after Darwin and Wallace had
published their first essays on natural selection and biological evolu-
tion. Lyel]’s opposition to evolution had long vexcd Darwin; he
could not understand why Lyell resisted the obvious conclusion of
the uniformitarian position, As recently as 1958, scholars were still
as confused over this as Darwin had been. Lyell’s correspondence in-
dicates that he was committed to the idea of final causation —teleal-
ogy—like most other scientists of his day. He spoke of a “Presiding
Mind” in an 1836 letter to Sir John Herschel.%* This divine in-
telligence directed any extinctions or new appearances of species that
might have taken place in the past. He called these “intermediate
causes,” and Ict it go at that. But such interventions by God, direct
or indirect, violated the principle of uniformitarian change, since no
such intervention is visible today. Thus, concludes the meticulous
scholar, A. O. Lovejoy, “once uniformitarianism was accepted,
evolutionism became the most natural and most probable hypothesis
concerning the origin of the species.”°> But Lyell insisted (in the
1830's through 1863) on the recent origin of man and the validity,
respecting mankind, of the Mosaic record. “He simply did not see,”
writes Lovejoy, “that a uniformitarian could not consistently accept
special-creationism, and must therefore accept some form of evolu-

94, William Irvine, Apes, Angels, and Victorians: The Story of Darwin, Huxley, and
Evolution (New York: McGraw-Hill, 1955), p. 139.

94. Quoved by Greene, Adam, p. 373, note #6.

95. Lovejoy, "The Argument for Organic Evolution Before the Origin of the
Species, 1830-1858,” in Glass (ed.), Horerunners to Darwin, p. 367.
