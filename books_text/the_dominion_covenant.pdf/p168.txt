124 THE DOMINION COVENANT: GENESIS

words of the passage speak of “an hundred years old” as a child’s age.
Second, taday’s high rates of economic growth have not been the
product of spiritual renewal. What we have seen is an inverse rela-
tionship between Christian orthodoxy and economic growth: the
worse their spiritual condition, the more material possessions
modern men receive. We are viewing conditions analogous to those
described in Deuteronomy 8:12-17, where men attribute their wealth
to their own autonomous efforts. Today’s wealth appears to be a prelude to
God's judgment. Per capita wealth is rising in the West, but population
growth is declining, A major blessing of God is being withheld:
children,

Today’s rates of economic growth cannot be sustained for cen-
turies. The compounding process at 2% per annum, let alone 6%,
creates astronomically high per capita wealth in a few centuries. We
will run into the limits of growth eventually. Humanism may be
nearing the end of its rapid economic growth rates, at least in terms
of industrial growth. This century has been a radical historical aber-
ration: large-scale mass production, financed by monetary inflation,
accompanied by mass pollution, compounding annually, decade
after decade. This is not the culmination of Christian orthodoxy but
of arrogant secular humanism which is steadily consuming its moral
foundation, namely, the cultural veneer of Christian orthodoxy. If
anything, modern industrialism is a demonic imitation of Isaiah
65:20, the substitution of historically unprecedented economic and
cultural change for long-term social progress and increased life ex-
pectancy through the application of biblical law to society, It is “eat,
drink, and be merry, for tomorrow we die.” What a biblical social
order offers is longer life spans and slower, less radical social change
which can be sustained by the environment—social, ecological, and
psychological — over centuries.

The emergence of modern science and technology came in
response to the establishment of godly rule on a far wider basis than
ever before. Prof, Lynn White, Jr, has chronicled the important
technological developments of the Middle Ages. Medieval Catholic
culture was far more productive than the pagan cultures that it
replaced. But it was the Protestant Reformation which unleashed
the forces of modern science.!2 Loren Eiseley, the anthropologist-

11, Lynn White, Jr., Medieval Technology and Social Change (New York: Oxford
University Press, 1962).
12. For an introduction to this question, see the two articles that appeared in The
