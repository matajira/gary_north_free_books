Economie Value: Objective and Subjectice 54

the validity of the postulate, it can be imported and used as the
epistemological foundation of applied economics.

The problem with this strategy is that the specialists in ethics are
faced with precisely the same philosophical paradoxes, and -they
have not come to any agreement about the resolution of the problem
of making interpersonal comparisons of subjective utility. This is the
incommensurability problem in hedonism and utilitarianism, Pleasures
and pains cannot be quantified, even by the individual. There is an
ordinal scale (this is more pleasurable than had), but no cardinal
scale (zhis is exactly this much more pleasurable than that). McIntyre’s
comment on John Stuart Mill's utilitarianism applics cqually well to
Robbins: “Mill’s whole tenor of thought is that of a utilitarian who
cannot avoid any of the difficulties which this doctrine raises, but
who cannot conceive of abandoning his doctrine eithcr.”2) What was
Mill's philosophical difficulty? Writes McIntyre: *. . . trying ‘to
bring all the objects and goals of human desire under a single con-
cept, that of pleasure, and trying to show them as all commensurable
with each other in a single scale of evaluation.”?* Modern economists
do not solve this commensurability problem by substituting the word
“utility” for “pleasure.”

Robbins was not some amateur philosopher wha could legiti-
mately call upon the ethical theorists to solve his problem. His prob-
Jem was the same one which had baffled ethical theorists for many
years. Richard Brendt’s article in the Encyclopedia of Philosophy on
“Hedonism” even turns to the economists as examples of the contin-
uing debate over whether “we can know nothing about the mental
states of other persons, since there is no way of observing them
directly; . . .”2* Professor Smart has put the matter quite well: “The
fact that the ordinary man thinks that he can weigh up probabilities
in making prudential decisions does not mean that there is really no
sense in what he is doing. What utilitarianism badly needs, in order
to make its theoretical foundations secure, is some method according
to which numerical probabilities, even approximate ones, could in
theory, though not necessarily always in practice, be assigned to any
imagined future event. . . . But until we have an adequate theory of

21. Alasdair McIntyre, A Short History of Ethier (New York: Macmillan, 1966),
p. 235

22. Dbid., p. 236.

23, Richard B. Brendt, “Hedonism,” in The Encyclopedia of Philosophy, edited by
Paul Edwards (New York: Macmillan, 1967), IIL, p. 434.
