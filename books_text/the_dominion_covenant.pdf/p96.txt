52 THE DOMINION COVENANT: GENESIS

objective probability, utilitarianism is not on a secure theoretical
basis.”2* Keynes’ teacher and fellow pervert, the philosopher G. E.
Moore, put it more graphically when he wrote, concerning the sum-
ming up of individual pleasures in a social aggregate: “It involves
our saying that, for instance, the state of mind of a drunkard, when
he is intensely pleased with breaking crockery, is just as valuable in
itself—just as well worth having—as that of a man who is fully real-
izing all that is exquisite in the tragedy of King Lear, provided only
the mere quantity of pleasure in both cases is the same. Such in-
stances might be multiplied indefinitely, and it seems to me that they
constitute a reductio ad absurdum of the view that intrinsic value is
always in proportion to quantity of pleasure. Of course, here again,
the question is quite incapable of proof either way.” But if it is quite
incapable of proof for the ethicists, then there is nothing for the econ-
omists to import from this source which can serve as the foundation
for the necessary assumption of the postulate of psychological equal-
ity among men. The economics of secular humanism must make un-
provable assumptions about mankind in order to operate—assump-
tions that cannot legitimately be made, according to the logic of
secular humanism, but must and will be made by policy-makers.
Mark A. Lutz, an economist, and Kenneth Lux, a psychologist,
have altacked methodological individualism and laissez-faire eco-
nomics by challenging the presuppositions of the individualists in the
field of epistemology, They are methodological collectivists, and they
believe that the State can and should reorder economic priorities in
terms of collective needs. They have grasped the fact that it is il-
legitimate to use Robbins’ arguments against welfare economics to
criticize only collectivists’ policies, if Rebbins’ arguments are not
simultaneously used to criticize all policy decisions, and indeed, all
economic aggregates. They write: “In the absence of any way to
measure utility directly, the most reasonable thing to do is to assume
cqual utility scales across people, which in effect means equal capac-
ity for satisfaction. In fact, it is hard to see how any other assump-
tion makes sense. And this assumption is precisely what economics

24. J. J. C. Smart, in Smart and Williams (eds.), Utilitarianism: for and against
(Cambridge University Press, 1973), pp. 40-41

25, G. E, Moore, “Multiple Intrinsic Goods,” (editor's title) in Wilfrid Sellers and
John Hospers (eds.), Readings in Ethical Themy (2nd ed.,; New York: Appleton-
Century-Crofts, 1970), p. 387, The selection is taken from Moore's book, Ethics
(Oxford: Clarendon Press, 1912).
