8 THE DOMINION COVENANT: GENFSIS

asserting the sovereignty of man). We find modern psychologists,
especially those in the behaviorist camp, grimly and fanatically
depersonalizing even man, the chooser, making him just another
product in a strictly impersonal, cause-and-effect universe. But the
effort is in vain. Men will spend eternity with Satan or God, in the
lake of fire (specifically prepared for Satan and his angels: Matt.
25:41) or in the new heavens and new carth (Rev. 21).

Cosmic impersonalism is a myth. We never choose between
cosmic personalism and cosmic impersonalism; it is merely a ques-
tion of whose cosmic personalism: God's or Satan’s. Eve was tempted
by a person, Jesus was tempted in the wilderness by a person (Matt.
4). Cosmic impersonalism is a satanic delusion, a convenient way to
mystify men. Men choose to believe in something other than God,
and from Satan’s viewpoint, anything will do just fine. The result is
the same: man’s destruction, the alienation of man from God, in
whose image he was created. Satan is content to stay in the back-
ground, when necessary. He is content to be devilish; publicity for
publicity’s sake is not his style. The darkness suits him fine.

Perhaps the most perceptive analysis of this aspect of Satan’s
temptation is found in C. 8. Lewis’ fictional account of a senior
devil’s advice to a junior devil. Screwtape, the senior devil, gives this
advice in The Screwtape Letters, Chapter 7 (written in World War II).
“Our policy, for the moment, is to conceal ourselves. Of course this
has not always been so. We are really faced with a cruel dilemma.
When the humans disbelieve in our existence we lose all the pleasing
results of direct terrorism, and we make no magicians. On the other
hand, when they believe in us, we cannot make them miaterialists
and skeptics. At least, not yet. 1 have great hopes that we shall learn
in due time how to emotionalise and mythologise their science to
such an extent that what is, in effect, a belief in us (though not under
that name) will creep in while the human mind remains closed to
belief in the Enemy. The ‘Life Force,’ the worship of sex, and some
aspects of Psychoanalysis may here prove useful, If once we can pro-
duce our perfect work—the Materialist Magician, the man, not
using, but veritably worshipping, what he vaguely calls ‘Forces’
while denying the existence of ‘spirits’—then the cnd of the war will
be in sight.”

Providence and Economics

The justification for an economic commentary on the Bible is
based on the opening lines of Genesis. God created the world. It is
