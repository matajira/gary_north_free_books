From Cosmic Purposelessness to Humanistic Sovereignty 289

Dr, Young taught) and other six-day creationists in order to indoc-
trinate students with uniformitarianism, And then he writes an in-
tellectual defense of his uniformitarian faith, so that other Christians
might be convinced! Confiscated tax dollars were promoting Dr.
Young’s professional religion, uniformitarianism. (His professed
religion has been compromised by his professional, academic
religion. Today, he teaches at Calvin College.)

We must not be naive. The uniformitarian interpretation of geo-
logical processes is a religion, It has led to a more consistent religion,
that of evolution through natural sclection. The god of uniformitar-
ian, meaningless, directionless process was created by nineteenth-
century humanists and compromising Christian geologists — whose
intellectual and spiritual heirs are still publishing books—to provide
an explanation of this world which did not require full allegiance to
the plain teaching of Genesis 1. The god of uniformitarian geology,
whose high priest was Charles Lyell, metamorphosed (evolved?) into
a far stronger deity, the god of evolution through natural selection.
Charles Darwin became the founder and high priest of this new god,
whose kingdom is the whole academic and scientific world in the
final decades of the twentieth century. Finally, Darwin’s god of
meaningless process has developed into the modern god, mankind,
who will take over the operations of evolutionary process. Anyone
who fails to recognize the satanic nature of uniformitarianism’s
process divinity is hopelessly naive, for it is this divinity who has tom
the eternal decree of God from the presuppositions of modern man,
leaving man with only random process, or man-directed tyrannical
process, to comfort him. Christians cannot afford to be hopelessly
naive, even if that self-imposed naiveté is their justification for re-
maining on the faculties of state university geology or biology de-
partments. The price of such naiveté is still too high, for them and
for their equally naive Christian readers, who do not recognize a
theological battle when they sce it.

The Predestinating State

The social philosophers of the late nineteenth century grappled
with the same fundamental intellectual problems that faced the
biologists. What is the nature of evolution? Is the species Home sa-
piens governed by the same laws as those governing other species? Is
“survival of the fittest” a law applying to mankind? If so, in what
ways? Is competition primarily individualistic— man vs. man, man
