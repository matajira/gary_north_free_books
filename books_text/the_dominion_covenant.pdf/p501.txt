Witnesses and Judges 457

Eve saw that the tree was good for food, for aesthetic pleasure
(“pleasant to the eycs”), and for wisdom. She did not seek to confirm
her new understanding of God’s word with her husband. She sought
autonomy of interpretation. She would test God’s word for herself.
She ate, and she gave her husband fruit to eat, The subordinate in
the family took control of the situation. The results were predictable
for those governed by God’s word, Adam and Eve did not predict
them,

Why did Satan begin by calling God’s word into question?
Because this was the essence of the temptation. The fruit was only
the symbol, the reliability of God’s word, and His authority to bring
that word to pass, were the ultimate issues. Satan was challenging
both. He was calling God a liar. He also was saying that God is not
omnipotent: He cannot bring His word to pass. In short, Satan was
saying that he was telling the truth, and that God was a false
witness. Man had to decide. He had to make a judgment: Who was
the false witness?

Two Witnesses

What modern commentators fail to emphasize, or even to recog-
nize, is this: the temptation in the garden was fundamentally a judicial pro-
ceeding. Satan was bringing charges against God. The charge was
false witness. Yet il was more than false witness; it was the charge of
false witness concerning God almighty. Satan charged that God was
not telling the truth about the “real” God. It is a capital crime to
teach men to worship a false god (Deut. 13:6-11). God was therefore
deserving of death. But who would listen? To bring a charge of this
magnitude against anyone, the accuser needs two witnesses (Num.
35:30). To begin his rebellion, Satan needed two witnesses to testify
against God. (This is why Satan’s rebellion probably began in the
garden, not in heaven days before or even before time began.)

Furthermore, who had the right to execute the death sentence?
Not the accuser. The witnesses have this responsibility: “The hands
of the witnesses shall be first upon him to put him to death, and
afterward the hands of all the people. So thou shalt put away evil
from among you” (Deut. 17:7). Satan needed at least two witnesses
who had knowledge of the actual words of Ged before he could see
his goal achieved, namely, the death of God.

Adam had been given God’s instructions concerning the forbid-
den tree. He was the witness whose word was fundamental to the
