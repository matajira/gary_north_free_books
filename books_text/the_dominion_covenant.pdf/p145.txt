Costs, Choices, and Tests 101

creation, is sovereign. His word is determinative.

Man, as the image-bcarer of God, also imputes value to the crea-
tion. He can impute value to the word of God itself. Man determines
for himself the value of the choices he must make. This does not
make his choices objectively correct. He can and does deviate from
God’s standards of value. God, being omniscient, knows exactly how
much a person should give up in order to gain some item or achieve
some goal. Men, being rebellious and unwilling to adhere to the law
of God, frequently pay too much or try to pay too little for the things
in life they pursue. They are unable to impute value according to the
warning given by Jesus: “For what shall it profit a man, if he shall
gain the whole world, and lose his own soul?” (Mark 8:36). If they
cannot correctly yaluate this key transaction in terms of its cost-
effectiveness, how can they make accurate judgments concerning the
(rue value of any other transaction? Yet they are required by God to
do so.

Man imputes value to anything in terms of a hierarchy of values.
He makes choices in terms of this set of priorities. Is it worth giving
up this in order to attain that? It depends upon one’s value scale. This
value scale is constantly shifting, since tastes change, external condi-
tions change, and men’s first principles sometimes change. Every
value scale is connected to some concept of authority, This is preferred
to that because of the perceived correctness of one’s value scale. The
very idea of correctness implies the concept of authority. So man makes
his choices within the framework of some sort of authority structure.
Choice requires basic standards of preference, and standards imply
authority, meaning a source of ultimate sovercignty. Man never
finds himself in a position of choosing in terms of one authority or no
authority; it is only a question of which authority. Rushdoony has
stated this forcefully: “For a man to live successfully, he must have
an ultimate standing ground; every philosophy is authoritarian, in that,
while it may attack savagely all other doctrines of authority, it does
so from the vantage point of a new authority, This new authority is a
basic pretheoretical presupposition which is in totality religious and
which rests on a particular concept of infallibility. Every man has his
platform from which he speaks. To affirm that foundation without
qualification is an inescapable requirement of human thought.”!

 

1. R. J. Rushdoony, Infallibility: An Inescapable Concept (Vallecito, Calif.: Ross
House Books, 1978), p. 4
