300 THE DOMINION COVENANT: GENESIS

having seen that results in the organic world are produced through
rhythmic differentiations, they infer that results in the superorganic
world should be left to the same influences. Nothing could be more
false or more pernicious. Scientists of this school, from the weight
which their opinions must have, are really doing more to counteract
the true tendencies of social progress than those who openly oppose
them. All social progress is artificial. It is the consequence of teleo-
logical foresight, design, and intellectual labor, which are. processes
diametrically opposed in principle to the processes of nature. If in
learning the law of evolution we must apply it to society, it would
have been better to have remained ignorant of that law.”!5? Since the
chief opponents of Social Darwinism were orthodox Christians, this
statement indicates that Ward hated Social Darwinists’ ideas more
than he hated orthodoxy. Who was he challenging? Spencer and
Sumner. He was attacking Sumner’s whole methodology of in-
vestigating the conflicts found in nature and then transferring this
conflict principle to human society. After all, it was Sumner who
wrote in What Social Classes Owe to Each Other that “We cannot get a
revision of the laws of human life. We are absolutely shut up to the
need and duty, if we would learn how to live happily, of investigating
the laws of Nature, and deducing the rules of right living in the
world as it is.”45¢ Not so, announced Ward. “Civilization consists in
the wholesale and ruthless trampling down of natural laws, the com-
plete subordination of the cosmical point of view to the human point
of view, Man revolutionizes the universe. . . . The essential func-
tion of Knowledge is to aid him in accomplishing this revolution.”155
Man must exercise dominion.

Ward set forth the basic conflict between the two forms of evolu-
tionary thought. It is a question of properly interpreting the concept
of adaptation, the central idea in Darwinian evolution. No one has
made the issues any clearer. “All progress is brought about by adapta-
tion, Whatever view we may take of the cause of progress, it must be
the result of correspondence between the organism and the changed
environment. This, in its widest sense, is adaptation. But adaptation
is of two kinds: One form of adaptation is passive or consensual, the

153. Zbid., TL, p. 628.
154, Sumner, Social Classes, p. 14.
155, Ward, Il, p. 473.
