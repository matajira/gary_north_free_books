136 THE DOMINION COVENANT: GENESIS

wagon, the symbol of family responsibility in America, looks at him
and says, “Where are you going?” “Nowhere special,” Bronson
answers. “Boy, I wish I were you,” the man replies. “Well, hang in
there,” says Bronson, and zooms away as the light turns green. Here
was the heart of the message of romantic nomadism: the lure of low-
responsibility existence, the lure of the road, The cowboy nomads of
American fiction brought law and order with. them, though they
themselves may have been on the fringes of the Establishment’s law.
The modern fictional nomad lives on the fringes of culture and
brings existential alienation, The real nomads of the counter-culture
brought {and bring) disease, such as venereal disease and lice.
The counter-culture, shortly after its inception, was literally all
loused up.

There is no question that nomads are primitive. They are not
future-oriented. They do not build for the future. Whether in the
African veldt, the Australian back country, the pre-Columbus
American plains, the post-Civil War American plains, the wastcs of
the Arabian desert, or on Route 66, the nomads cannot build a civi-
lization. God paralyzed the people who attempted to build the tower
of Babel by scattering them..The wanderer is culturally impotent.
But what must be understood is that primitivism is primarily a religious
and ethical outlook, it is not some hypothetical steppingstone in man’s
upward evolution. It is a religion opposed to civilization.

Rootless men are not long creative. It is not surprising that
modern American corporations are finally questioning the practice
of moving executives from city to city every few years. They have
encountered increasing opposition from their employees, who now
understand better the strains that such nomadism creates for the
family. (Differentials in housing costs, city to city, in the late 1970's
also became an important factor encouraging some families to stay
put; they could not afford the mortgage debt in citics like Los
Angeles, San Francisco, and Washington.) In the long run, the large
American corporations will find that stronger community and family
tics will benefit the companies, since productivity will increase in
higher management.

The autonomy of modern urban life, with its atomized life styles,
has built-in limits. Unstable neighborhoods, in which few people
know more than one or two families on the block, are easier targets
for burglars and other criminals. The requirements of self-defense,
especially since the racc riots of the mid-1960’s, have prompted some
