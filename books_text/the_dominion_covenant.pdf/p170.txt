126 THE DOMINION GOVENANT: GENESIS

“garden” apart from saving faith and biblical dominion. Men may
wish to find an escape from the bondage of death, thereby allowing
them access to infinite temporal extension for the purpose of indulg-
ing their lusts. Yet their tools of dominion now threaten all of civili-
zation, for the tools of dominion can produce and have produced
mighty weapons, allowing us to turn ploughshares into swords more
efficiently.

Time and Economics

Unquestionably, technology has permitted us to make more
efficient use of our time. Time is the resource; above all, which men
seek to conserve, if only to waste it in unfulfilling leisure activities.
Time is.mankind’s only absolutely irreplaceable environmental resource. It is
the human resource which confounded the attempts of Solomon to
deal with in terms of the logic of autonomous man (Eccl. 1-3). Time’s
limitations led the psalmist to declare: “My days are like a shadow
that declineth; and I am withered like grass” (Ps. 102:11), Time is in
short supply—only one earthly life per customer!

Time is the fundamental component in all economic planning. It is the
foundation of a proper explanation of the phenomenon of the rate
of interest, The interest rate stems from the rational distinction in
each person’s mind between an economic good enjoyed in the pres-
ent and the same good enjoyed in the future. Goods to be used in the
future are less valuable than the same goods used.in the present
(other things being equal, as the cconomist always says), Some men.
value present consumption very highly. They will therefore sacrifice
the use of a presently owned resource only for large quantities of
scarce economic resources in the future. These people will loan their
assets only at high rates of interest. The premium of present goods
over future goods is very high; some cconomists call this “high
time-preference.”5 This present-orientedness is a crucial factor in slum
communities and in underdeveloped (backward, primitive) nations.'*

15. Ludwig von Mises, Human Action (3rd ed.; Chicago; Regnery, 1966), pp-
4837, 4991

16. Edward Banfield, The Unheavenly City (Boston: Little, Brown, 1970), argues
that one’s class position is a function of one’s attitude toward the future, with lower-
class people being present-oriented, In Mises’ terminology, they have high time-
preference. See pp, 47, 62, 72, 163ff. The ghetto suffers from massive present-
orientedness. Sociologist Helmut Schoeck has pointed out that envy in primitive
cultures prevents people from sharing their views of the future, and the economic
