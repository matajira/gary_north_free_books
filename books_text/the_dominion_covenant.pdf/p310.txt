266 THX DOMINION COVENANT: GENESIS

Gcorge Gaylord Simpson, onc of the most prominent paleontolo-
gists of the mid-twentieth century, has offered us this interpretation
of man, the new sovereign: “Man és the highest animal. ‘Che fact that
he alone is capable of making such a judgment is in itsclf part of the
evidence that this decision is correct. . . . He is also a fundamen-
tally new sort of animal and one in which, although organic evolu-
tion continues on its way, fundamentally a new sort of evolution has
also appeared. The basis of this new sort of evolution is a new sort of
heredity, the inheritance of learning.” Simpson contrasts organic
evolution, nature’s non-teleological, random development of non-
human species, with the new, social cvolution of mankind. “Organic
evolution rejects acquired characters in inheritance and adaptively
orients the essentially random, non-environmental interplay of gen-
etical systems. The new evolution peculiar to man operates dircctly
by the inheritance of acquired characters, of knowledge and learned
activities which arise in and are continuously a part of an organ-
ismic-environmental system, that of social organization,”47 A new
Lamarckianism, with its inheritance of acquired characteristics, has
arisen; it has brought with it a legitimate teleology. Man, the prod-
uct of nature, can at last provide what autonomous nature could not:
conscious control, “Through this very basic distinction between the old
evolution and the new, the new evolution becomes subject to con-
scious control. Man, alone among all organisms, knows that he
evolves and he alone is capable of directing his own evolution. For
him evolution is no longer something that happens to the organism
regardless but something in which the organism may and must take
an active hand.”*8 Man’s control over future evolution is limited, of
course. He cannot choose every direction of a new evolution, nor the
rate of change. “In organic evolution he cannot decide what sort of
mutation he would like to have,” but he does have power, and
therefore must make responsible decisions. “Conscious knowledge,
purpose, choice, foresight, and values carry as an inevitable cor-
ollary responsibility."5° Of course, we know that all ethics is relative,
in fact, “highly relative.”5! “The search for an absolute ethic, either

 

 

46. Simpson, The Meaning of Evolution: A Study of the History of Life and of Is Signif-
cance for Man (New Haven, Gonn.: Yale University Press, [1949] 1969), p. 286.
47, Ipid., p. 187.
48, Lid, p. 291.
49. Idem,
50. Tid, p. 310.
51. Lbid., p. 297.
