From Cosmic Purposetessness to Humanistic Sovereignty 253

fortable adhering to fundamentalist creeds. This should cause no
surprise, since a large majority of these believers are as unfamiliar
with scientific findings as were people who lived centuries ago. The
really extraordinary phenomenon is the continued existence of a
small minority of scientifically educated fundamentalists who know
that their beliefs are in utter, flagrant, glaring contradiction with
firmly established scientific findings. . . . Discussions and debates
with such persons is [sic] a waste of time; I suspect that they are un-
happy people, envious of those who are helped to hold similar views
by plain ignorance,”9

What is the heart of the evolutionist’s religion? Dobzhansky
makes himself perfectly clear: “One can study facts without bother-
ing o inquire about their meaning. But there is one stupendous fact
with which people were confronted at all stages of their factual en-
lightenment, the meaning of which they have ceaselessly tried to dis-
cover. This fact is Man.”2° This is the link among all of man’s relig-
ions, he says. Man with a capital *M” is the heart of religion; and on
these terms, evolutionism must certainly be the humanistic world’s
foremost religion. It is not surprising, then, that Dobzhansky’s book
was published as one of a series, edited by Ruth Nanda Anshen:
“Perspectives in Humanism.”

What must be grasped from the very beginning is that evolution-
ism’s cosmology involves an intellectual sleight-of-hand operation, It appears
initially to denigrate man’s position in a universe of infinite (or
almost infinite) space and time, only subsequently to place man on.
the pinnacle of this non-created realm. Man becomes content to be a
child of the meaningless slime, in order that he might claim his
rightful sovereignty in the place once occupied by God. By default—
the disappearance of God the Creator—man achieves his evolving
divinity,

Constants, Chronology, and Purpose
The Bible categorically asserts that the stars, sun, and moon
were created after the earth. Therefore, the Bible categorically re-
jects the doctrine of uniformitananism, namely, that the rates of change
observed today have been the same since the beginning. Differently

19. fhid., pp. 95-96.
20. Ibid, p. 96.
