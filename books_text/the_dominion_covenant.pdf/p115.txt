God's Week and Man's Week 71

He needed no rest as a creature; he could begin as an originally crea-
tive being, The seventh day was his first day of the week, a day of
original, autonomous work. He could then rest at the end of the
week, as God had rested, when his work was finished. He would
complete his own work, announce its perfection, and then rest, as
God had rested. He did not begin with rest, nor did he begin with a
perfect environment provided to him by God, Adam proclaimed. He
began his week by means of his own labor, and to prove his full inde-
pendence, he began with a violation of God's covenant.

God, in effect, “rubbed man’s nose” in his own rebellion, God es-
tablished the six-and-one pattern as a requirement for man, until the
day of redemption came in history, Covenant-keeping man in the Old
Testament era would begin work on the seventh day, and his rest
could come only at the end of his labors. Man’s life would be a life of
labor, not beginning with a day of rest, but promising rest only at the
end of man’s days. Man's rest, even for a covenanted man, would
come only at the end. The six days of labor symbolized man’s rebel-
lious week, a week begun autonomously, denying the reality of that
first full day of rest which prepared man for his week of service. Man
turned his back on that first sabbath; God then did the same for man.
“Your rest will come at the end of your days, after death has cut you
off in the midst of your days.” The six-and-one framework was a bless-
ing, for it promised covenant man eventual rest, but it was also a
curse: it delayed man’s day of rest. Man wanted to be as God, resting
at the end of his week of labor. God allowed him to achieve his goal,
but only through grace: rest at the end of man’s week (life).

Man announced that he, autonomously, would begin his crea-
tion week on the seventh day. God’s curse on Adam was that his
work would henceforth be burdened. Man wanted to demonstrate
his own creativity. God showed him how limited he was as a
creature, making him struggle with the creation. Man had received
a completed, perfect creation as God’s gift. It awaited him for the
eighth day of history, his second day of the week. Adam spurned the
gift, choosing to regard himself as the creator. The cursed earth now
serves as a testimony to man of the difficulties of creation, even in an
environment that was completed by God.

Adams first full day of life was also his first day of sin and judg-
ment. What he failed to see was that his /ife and his rest were linked.
By denying the validity of his rest, he denied the foundation of his
life. God cursed man. Every man who is not given life is also not
