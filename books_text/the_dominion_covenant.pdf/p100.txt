56 THE DOMINION COVENANT: GENESIS

concerning capital per head in various nations, He also provides
some unique answers. He writes: “Is it really without meaning to say
that the capital per head in country A is greater than in country B? Is
it meaningless to attempt to explain the higher productivity of labor
in country A by reference to the larger quantity of capital combined
with cach man-hour of labor? It is indeed difficult to deny that we, in
fact, use aggregate concepts of capital in this manner; what is the
meaning to be attached to such concepts, and how do they relate to
the ‘individualistic’ concept of capital that has been adopted for the
purpose of this essay?”? The consistency with which Kirzner
answers his questions is unprecedented: “Careful reflection on the
matter will, it is believed, reveal that the aggregate concept of
capital, the ‘quantity of capital available to an economy as a whole,’
is, for a market economy, a wholly artificial construct useful for mak-
ing certain judgments concerning the progress and performance of
the economy. When using this construct one is in fact viewimg the
economy in its entirely [entirety] as i it were not a market economy
but instead a completely centralized economy over which the
observer himself has absolute control and responsibility. . . . One is
thus oé merging the plans of all the individual capital owners who
participate in the market economy, one is conceptually replacing these
plans by a single master plan that one imagines to be relevant to the
economy as a whole, and against which one gauges the performance
of the economy as a whole.”*

We must ask Kirzner, how is it that such a “wholly artificial con-
struct” which imagines that the economy is one vast outworking of a
single economic plan—in contrast to the operations of the free
market, with its multiple plans—should be “useful for making cer-
tain judgments concerning the progress and performance of the
economy”? Why should such an artificial construct be deemed in-
tellectually defensible? Why should it be useful? Why should
defenders of the logic of the market be forced to rely on a wholly
artificial construct in order to make judgments in the area of applied
economics? Is applied economics really applied economics? Is it not
rather applied common sense? But must common sense be our only
source of such judgments, when common sense apparently relies on
the holism or collectivism of such a mental construct? Isn’t this

 

29. Israel Kirzner, An Essay on Capital (New York: Augustus Kelley, 1966), p. 120.
30. Ibid., pp. 120-21.
