360 THE DOMINION GOVENANT: GENESIS

larly striking: here, even more than elsewhere, one may justly speak
of their theories as ‘radical myths’ ”®

Hecateus of Miletos, an historian of the mid-sixth century, B.c.,
attempted to link human history with natural history. His conclu-
sions were still being quoted by Diodorus of Sicily five centuries
later, in the latter’s Historical Library: “When in the beginning, as
their account runs, the universe was being formed, both heaven and
earth were indistinguishable in appearance, since their elements
were intermingled: then, when their bodies separated from one
another, the universe took on in all its parts the ordered form in
which it is now seen. . . .”9 Life sprang from “the wet” by reason of
the warmth from the sun; all the various forms were created at once.
The creation of the elements was therefore impersonal. ‘he creation
of life was spontaneous, instantancous, and fixed for all time. It was
a purely autonomous development.

The philosopher Plato was caught in the tension between order
and chaos. ‘Iwo of the pre-Socratic philosophers, Heraclitus and
Parmenides, had set forth the case for each. Heraclitus had argued
that all is lux, change, and process; Parmenides had argued that all
is rational, static, and universal. This so-called dialectic between
structure and change, order and chaos, was expressed in terms of the
Form (Idea)-Matter dualism. Plato, in the Timaeus dialogue,
begins with a contrast betwcen exact, eternal mathematical concepts
and the temporal flux of history. As "oulmin and Goodfield com-
ment: “The Creation of the cosmos was the process by which the
eternal mathematical principles were given material embodiment,
imposing an order on the formless raw materials of the world, and
setting them working according to ideal specificalions.”"! It is the vi-
sion of a Divine Craftsman. Plato was non-committal about the tim-
ing of this creation or the order of the creation; it was, at the
minimum, 9000 years earlier. In response to Aristotle’s attack on this
theory, Plato’s pupils argued that it was only an intellectual con-

8 Stephen Toulinin und Jane Goodfield, ‘The Discovery of Time (New York:
Harper Torchbook, 1965), p. 33; of. p. 37.
9. Ibid., p. 35.

10. Rushdoony, ‘he Oxe and the Many, ch, 4; Herman Dooyoweerd, In the Twilight
of Western Thought (Philadelphia: Presbyterian and Reformed, 1960), pp. 39-42; Gor-
nelius Van Til, A Survey of Christian Epistemology, Volume IL of In Defense of the Faith
(Den Dulk Foundation, 1969), ch. 3

11. Discovery of Time, p. 42.
