430 THE DOMINION COVENANT: GENESIS

ens shall vanish away like smoke, and the earth shall wax old like a
garment, and they that dwell therein shall die in like manner: but
my salvation shall be for ever, and my righteousness shall not be
abolished” (Isa. 51:6), He who dares to tamper with the doctrine of
creation compromises the revelation of the Creator concerning His
own activity. If the latest finding of science — based, as it is, on the
oldest antitheistic philosophy of creation—should be permitted to
undermine the explicit revelation of God concerning one aspect of
His relationship to His creation, there is no logical reason to draw
back in horror when science also undermines the doctrine of salva-
tion, Without the doctrine of creation there can be no doctrine of sal-
vation — not, at least, an orthodox doctrine.

God is eternal and unchanging (Mal. 3:6). His words shall not
pass away (Matt. 24:35); His counsel is immutable (Heb. 6:17).
“The Lorp by wisdom hath founded the earth; by understanding
hath he established the heavens” (Prov. 3:19). God’s wisdom founded
the world; the fallen world’s wisdom cannot accept this. God's
wisdom is foolishness to the world (I Cor. 1:20), and God warns His
people not to be beguiled by the vanity of apostate philosophies (Col.
2:4-9). God is the standard of reference, the unchanging measure of
all truth. Thus, the Bible rejects the pagan idea of creation through
self-generated process, and it affirms the fiaf creation by the word of
God, Creation was a discontinuous event—éhe discontinuous event
prior to Christ’s incarnation. Process theology is the remnant of
Adam’s thought; by stressing the continuity between man’s truth and
God’s truth, it relativizes God’s truth. The shifting opinions of scien-
tists replace the verbal revelation of God. Time, not God, becomes
the framework of creation; chance, not God's eternal word, becomes
the creative force in history. Evolution, the most consistent and most
dangerous form of process theology, cannot be made to fit the cate-
gories of Christian faith,

Providence

The definition of creation goes beyond the concept of the original
creation which ended on the sixth day, It simultaneously affirms the
sustaining hand of God in time. It is Christ, “who being the brightness
of his glory, and the express image of his person, and upholding all
things by the word of his power” (Heb. 1:3), maintains the earth and
the stars, “He hath made the earth by his power, he hath established
the world by his wisdom, and hath stretched out the heaven by his
