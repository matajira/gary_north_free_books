xliv THE DOMINION COVENANT: GENESIS

Note to the reader: throughout this book, I have avoided em-
phasizing any material in direct citations. If an italicized word or
phrase appears inside the quotation marks, then the original author
made this decision. The only changes I have made are the very occa-
sional use of brackets to define an author’s use of an obscure word.

Additional note: I capitalize the word “State”— civil government
in general —in order to distinguish it from those regional entities in
the United States, “states.”
