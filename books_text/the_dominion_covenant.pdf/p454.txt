410 THE DOMINION COVENANT: GENESIS

At last he was free from Paley: “The old argument from design in
Nature, as given by Paley, which formerly seemed to me so con-
clusive, fails, now that the law of natural selection has been discov-
ered.”176 What little cosmic personalism that still remained in Paley’s
rationalistic universe was now officially rejected.

When challenged by Asa Gray to defend his anti-teleological atti-
tude, Darwin did not call forth his notes on barnacles or some new
theory of coral recf formation, He replied from his heart, and his
heart was exceedingly religious. What he really hated was the Christian
doctrine of a totally sovereign God. He hated this God more than he
feared a random universe: “With respect to the theological view of
the question. This is always painful to me. I am bewildered. I had
no intention to write atheistically. But I own that I cannot sce as
plainiy as others do, and as I should wish to do, evidence of design
and beneficence on all sides of us. There seems to me too much
misery in the world, I cannot persuade myself that a beneficent and
omnipotent God would have designedly created the Ichneumonidae
with the express intention of their feeding within the living bodies of
Caterpillars, or that a cat should play with mice, Not believing this,
I see no necessity in the belief that the eye was expressly designed.
On the other hand, I cannot anyhow be contented to view this
wonderful universe, and especially the nature of man, and to con-
clude that everything is the result of brute force. I am inclined to
look at everything as resulting frorn designed laws, with the details,
whether good or bad, left to the working out of what we may call
chance. Not that this notion at aif satisfies me. I feel most deeply that
the whole subject is too profound for the human intellect, A dog
might as well speculate on the mind of Newton,”!”7

He could not believe that the eye was designed, despite the in-
escapable difficulty that it is a totally complex element of the body
that needs to be complete before it can function at all. How could
this organ have evolved? What good was it during the countless
millennia before it was an eye? Darwin was familiar with this objec-
tion, but he could not believe in specific design. However, in order to
save his hypothetical universe from the burden of total randomness
—from “brute force”— he was willing to admit that natural laws had
been designed, a conclusion wholly at odds with his own theoretical
methodology. But he was not satisfied with this conclusion, either.

176, Ibid., I, p. 278.
177, Darwin to Gray (May 22, 1980), ibid, TI, p. 105.
