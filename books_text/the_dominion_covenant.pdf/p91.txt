Economie Value: Objective and Subjective ”

may be urged that the economist hereby goes outside his proper
‘scientific’ field. This point is strongly urged by Professor Robbins.
Whether the nth unit of X has greater or less utility than the mth of Y
to a given individual may be made the subject of a test. He can be
given the choice. But there are no scientific means of deciding
whether the nth of X has greater utility to individual P than the mth
of Y has to another individual Q, The choice can never be put, This
implies that we cannot in fact decide whether two pence have more
utility to a millionaire than a beggar, We may have a shrewd suspi-
cion: But this, we are told, is ‘unscientific, for lack of a test.” But
what answer can Harrod provide? Only that economics really isn’t
very much of a science afier all. “This objection would be very
weighty if economics itself were a mature and exact science. Yet in
fact its achievements outside a limited field are so beset on cvery side
by matters which only admit of conjecture thal it is possibly rather
ridiculous for an economist to take such a high linc.”® He then aban-
dons the whole idea of scientific logic, of a scientific epistemology.
He appeals to “common sense” in order to justify the scientific econ-
omist in making value judgments and policy decisions in the name of
scientific rigor. “Can we afford to reject this very clear finding of
common sensc? Of course, great caution must be exercised in not
pushing the matter too far. Since the evidence is vague, we must not
go farther than a very clear mandate from common sense allows.”!°
This, however, docs not answer the problem. Whose common sense
is he talking about? The socialist’s? The Keynesian’s? (Keynes was
the editor of the Economie -Journal when Harrod’s article was pub-
lished,. and Harrod was Keynes’ biographer after Keynes died in
1946.) Harrod’s “common sense” is simply an admission of intellec-
tual and epistemological bankruptcy.

Harrod understood the threat Robbins’ book posed and will con-
tinue to pose to applied cconomics. “If the incomparability of utility
to different individuals is strictly presscd, not only are the prescrip-
tions of the welfare school ruled out, but all prescriptions whatever.
The cconomist as an adviser is completely stultified, and, unless his
speculations be regarded as of paramount aesthetic value, he had
better be suppressed completely. No; some sort of postulate of
equality has to be assumed.” This postulate of psychological equality

9. RK. & Harrod, “Scope and Method of Economics,” Economic fournal, XLVIIL
(1938), p. 396

10, fdem.

il. Tbid., p. 397.
