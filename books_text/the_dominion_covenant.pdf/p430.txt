386 THE DOMINION COVENANT: GENESIS

methodology of uniformitarianism to be easily restrained. It is sup-
posedly fine for geologists to assume as valid this uniformitarian
methodology (as it was in 1840), but biologists nevertheless have to
be anti-evolutionists, denying therefore Darwin’s overwhelmingly
successful—pragmatically speaking—fusion of uniformitarianism
and biology. But Darwinianism is not to be denied by compromising
Christian biologists in the 1980’s, any more than he could be denied
by uniformitarian scholarship in the 1870's, Uniformitarian concepts
of time are far too potent for half-measurcs.

The important humanist study, Forerunners of Darwin (1959), pub-
lished on the centenary of the publication of Origin of Species, opens
with a crucial quotation from the uniformitarian geologist, George
Scrope, who wrote in 1858 these memorable words: “The leading
idea which is present in all our researches, and which accompanies
every fresh observation, the sound which to the ear of the student of
Nature seems continually echoed from every part of her works, is—
Time! Time! Time!”!.

Biological Evolution: Pre-Darwin

The seventeenth century had seen the reappearance of postmil-
lennial eschatology—out of favor since the fifth century—which
offered Christians new hope. The preaching of the gospel and the es-
tablishment of Christian institutions would eventually transform (he
world cthically, and this cthical transformation would eventually be
accompanied by external personal and cultural blessings. This had
been the vision of many English Puritans and most of the American
colonial Puritans (until the pessimism of the 1660’s, symbolized by the
poetry of Michael Wigglesworth, set in), This vision was to have a
revival, unfortunately in more antinomian, “spiritual” forms, through
the influence of Jonathan Edwards in the cighteenth century.’

Paralleling this biblical optimism was the secular idea of progress

100, Gited by Francis C. Haber, “Fossils and Early Cosmology,” ibid., p. 3.

101. On the Puritans’ postmillennial impulse, see the articles by James Payton,
Aletha Gilscorf, and Gary North in The, fournal of Christian Reconstruction, V1 (Sum-
mer, 1979); Iain Murray, The Puritan Hope (London: Banner of Truth, 1971); Ernest
Lee Tuveson, Redeemer Nation: The Idea of America’s Millennial Rote (University of
Chicago Press, 1967); Alan Heimert, Religion and the American Mind (Cambridge,
arvard University Press, 1966). One of the representative documents of the
colonial American period is Edward Johnson’s Wonder-Working Providence, edited by
J. Franklin Jameson (New York: Barnes & Noble, 1952). Until quite recently, post-
millennial thought was a neglected —incecd, completely misunderstood — factor in
American history,

  
