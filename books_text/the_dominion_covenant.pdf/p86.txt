42 THE DOMINION COVENANT: GENESIS

comparative price of a specific Bible and a specific pornographic book
or magazine. Furthermore, the culture may be made up of rebellious
people who are determined to work out their own damnations with-
out fear or trembling. They impute value to pornographic books,
and little or no value to Bibles. The market will reflect this phenome-
non in an objective manner. It will reflect it in the profit-and-loss
statements of publishers. Those who meet market demand will pros-
per, while those who do not meet it will falter or go bankrupt. The
profits and losses will be a result of the subjective valuations of acting
men, who make decisions in terms of their values. Christian litera-
ture must be subsidized, while pornography produces income.

The humanistic, relativistic economist looks at these facts and can
conclude that in a specific market, pornography is more valuable at
the margin than Bibles are, He says ihat he is making no ethical value
judgment when he says this; he is only reporting the objective results
of multiple subjective valuations on the market. But since he allows
no concept of objective valuc to enter his economic analysis—not
consciously, at least — he is unable to take a sland against the market
except by means of stating his fersonal opinion that Bibles are better
than pornographic magazines. However, the market supposedly
must be left alone to have its way, since one man’s opinion must not
be allowed to thwart the operations of the market process. His
relativism leads to an objective result: the spread of pornography
through price competition, thereby lowering the costs of achieving
damnation and cultural disintegration,

The biblical explanation is different. The Bible affirms that men
do have the power to impute economic value, It also affirms that
there are absolute, objective standards of value. In fact, it is because
of these standards that all coherence in the universe can be said to
cxist. The creation reflects these standards, revealing the Gad who
created all things (Rom. 1:19). The Bible reveals these standards ver-
bally. “Vherefore, all human imputation goes on within a framework of God's
absolute, objective standards. God imputes good and evil in terms of His
own standards, and this imputation provides the only reliable stand-
ard of evaluation. The facts are what God determines and imputes,
not what the market determines and imputes, or some socialist plan-
ning board determines and imputes. The accuracy of each man’s in-
dividual: act of imputation stands or falls in terms of its cor-
respondence to God’s act of imputation. We live in a universe of
cosmic personalism.

 
