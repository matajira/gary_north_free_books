xxxvili TIE DOMINION COVENANT: GENESIS

the day that they sinned. He executed judgment, but they did not
die physically. He imputed physical life to them because of Christ's
future sacrificial offering at Calvary. To inherit God’s blessings, men
must be adopted by grace back into God’s family ( John 1:12). To
refuse the offer of ethical adoption is to remain in the family of God’s
disinherited son, Adam (Acts 17:26), and to be cut out of God’s inherit-
ance in eternity.

Four Covenants

There are four, and only four covenants in the Bible: personal,
family, church, and civil. Each corresponds to an agency of govern-
ment. Each has an oath attached to it, either implicit or explicit.
Each has all five points of the biblical covenant.

Personal oaths are called vows. They are referred to in Numbers
30. Women are allowed to take them, and are required by God to
adhere to them, but only if the male head of household approves
within 24 hours: father or husband (vv. 3-8). Widows and divorced
women, as heads of their households, may take vows without per-
mission of a man (v. 9). Even though this is a personal oath, we see
hierarchy illustrated by this requirement of hierarchy. The individ-
ual is under God, and is held accountable directly by God. Most
women have to get permission, but are then held directly accounta-
ble by God.

Second, there is the family covenant, It, too, has all five points of
the covenant.* God is sovereign over it. There is hierarchy: hus-
bands over wives, parents over children. It has specific laws govern-
ing it. It is sealed with a public oath (marriage vow). It involves
inheritance and continuity.

Third, there is the church covenant. God is sovereign over it,
and specially present in the sacraments. It has a system of hierar-
chical authority. Specific laws govern it. There is a baptismal oath,
cither explicit (adults) or representative (parents in the name of in-
fants), There is continuity: membership and ordination of officers.

Finally, there is the civil government. God ordains it and governs
it. There is hierarchy: a court system, There are civil laws revealed
by God. There are oaths: implicit (citizenship) and explicit (magis-
trates). There is continuity: clections, constitutional amending proc-
ess, judicial precedents, etc.

 

 

 

4. Ray R, Sutton, Who Oumns ihe Kamily? God Or the State? (Ft, Worth, Texas:
Dominion Press, 1986).
