The Growth of Human Capital 165

Children are basic to the covenant and a sign of God’s unmerited
favor to man: “Lo, children are an heritage of the Lorn: and the fruit
of the womb is his reward” (Ps, 127:3). Children are blessings — not
blessings in disguise, but blessings—within the framework of the
covenant of grace. The broader covenant of dominion also implies
that children are a blessing in time and on earth, since men and
women are told to reproduce, and obedience involves blessings. The
all-inclusive nature of the covenant of dominion does not mean that
the element of cursing, on the day of judgment, has been overcome
merely by the willingness of people to have large families. But in
time and on earth, children are a blessing.

The growth of his family, resulting in millions of descendants,
was unquestionably basic to the Abrahamic covenant. As far as
Abraham was concerned, the modern ideal of zero population
growth would have been an acceptable one . . . for the Canaanites.
The promise of ultimate victory in Canaan necessitated the exter-
mination and expulsion of the enemies of God from the land (Gen.
15:16, 18-20; Fx. 23:31; Josh. 21:44). When the Israclites had left
Egypt, they were given a special promise by God: their covenantal
faithfulness would result in a society without miscarriages, either of
animals or humans. In the same breath, God promised them long
life: “, . . the number of thy days I will fulfill” (Ex. 23:26b). The “old
folks” would be allowed to get even older. The carth would therefore
be filled and subdued by covenantally faithful people far sooner.
When you lower the death rate of infants by eliminating miscar-
riages, and you simultaneously lower the death rate of adults, you
create ideal conditions for an historically unprecedented “population
explosion,” Yet this was the promise of God to a people who had just
undergone the most rapid expansion of population in recorded
human history.

The fulfillment of the covenant was inescapably linked to the
decline of influence in Canaan of the ungodly. As the numbers of the
faithful increased, the ungodly would decrease. This was basic to the
Abrahamic covenant, as well as to the revelation presented to
Abraham’s descendants immediately prior to the military invasion of
Canaan (Deut. 1:10). God preferred the expansion of man’s numbers
and man’s dominion in comparison to the dominion over the land by
the wild animals (Ex. 23:29-30), but He much preferred the expan-
sion of His special people and their dominion over the land instcad
of continued dominion by the Ganaanites.

 

 
