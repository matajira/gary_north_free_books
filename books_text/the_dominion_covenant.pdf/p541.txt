Index

Magic, 130, 216, 370, 449
Magicians, 8
Malthus, Thomas, 21, 301, 314, 402
Mammon, xxiii
Mammoths, 152
Man
animal, 267
as God, 252
authority of, 147
autonomy, 436
autonomous, 20, 73M, 77, 316f
central planner, 330
collective vs. individual, 290
developing being, 69, 70
development, 113
evolution-directing, 20, 263,
266, 268f, 275, 352, 414
Fall, 69tf, 734, 86, 250
“god,” 20
image of God, 27f, 120
imputing, 38, 235f
interpreter, 4
meaning, 280
minister, 146
naming, 150f
not static, 69
omniscience, 225, 227f, 352
pilot, 252
purpose, 20, 290
recreative, 6B
self-aware, 252, 276
self-transcendence, 252, 276
service, 147
sovereign, 20, 263
sovereignty of, 20, 22
Manchen-Helfin, Otto, 134n
Manicheanism, 28
Manser, A. R., 258
Maps, 369
Mardi Gras, 128, 441, 453
Marginalism, 38
Market (see free market)
Marketplace of ideas, 337f, 341
Marsden, George, 149
Martyr, Justin; 69n, 239
Marx, Karl, 18, 39, 98, 335, 339
Darwin and, 18

497

Marxism, 58f, 98, 281
Masses, 246, 281, 295, 305, 306,
309, 314, 341
Masters, 216f
Materialists, 8
Mathematies, 125, 438
Matthew, Patrick, 398
Maya, 362
McIntyre, Alasdair, 51
Melntyre, Carl, 148
Meaning, 51, 440
God's decree, 317
man-determined, 249, 280
planning and, 310
process vs., 287, 368
science vs., 372
Zen vs., 366
Meat, 113n
Medawar, Peter, 3, 20, 257f
Medes and Persians, 310
Meckness, 436f
Mencken, H; L., 388
Mendel, Gregor, 403n, 408
Menger, Karl, 41
Mephistopheles, 4
Mercy, 70, 181, 182, 183
Mesopotamia, 3n
Messiah, 151, 188
Metallurgy, 378
Methodological covenantalism, 24
Methodological individualism, 58
Middle Ages, 124
Middleman, 223, 225f
Migration, 232
Military, 194
Miller, Eugene, 333
Mill, John Stuart, 38, 51
Ministers, 389, 402, 413f, 415
Miracles, 328, 257
Miscarriages, 165
Mises, Ludwig von
aggregates, 63
a priorist, 238
career, 326
comparing wealth, 64
dualism, 324
economic calculation, 62
