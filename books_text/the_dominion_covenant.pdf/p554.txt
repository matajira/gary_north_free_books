510 THE DOMINION COVENANT: GENESIS

scholars have ignored economic theory for generations. This is why
the ICE devotes so much time, moncy, and effort to studying what
the Bible teaches about cconomic affairs,

There will always be some disagrecments, since men are not
perfect, and (heir minds are imperfect. But when men agree about
the basic issue of the starting point of the debate, they have a far bet-
ter opportunity to discuss and learn than if they offer only “reason,
rightly understood” as their standard.

    

Services

The ICE exists in order to serve Christians and other pcople who
are vitally interested in finding moral solutions to the economic crisis
of our day. The organization is a support ministry to other Christian
Mministrics, [t is non-sectarian, non-derniominational, and dedicated
to the proposition that a moral economy is a truly practical, produc-
tive economy.

The ICE produces several newsletters. These are aimed at in-
telligent laymen, church officers, and pastors. The reports arc non-
technical in nature. Included in our publication schedule are these
monthly and bi-monthly publications:

Biblical Economics Today (6 times a year)
Christian Reconstruction (6 timcs a year)
Covenant Renewal (12 times a year)

Biblical Economics Today is a four-page report that covers eco-
nomic theory from a specifically Christian point of view. It also deals
with questions of economic policy. Christian Reconstruction is
more action-oriented, but it also covers various aspects of Christian
social theory. Covenant Renewal explains the Biblical cavenant and
works out its implications for the three social institutions of culture:
family, church and state.

The purpose of the ICE is to relate biblical cthics to Christian
activities in the field of cconomics. To cite the title of Francis
Schaeffer's book, “How should we then live?” How should we apply
biblical wisdom in the field of economics to our lives, our culture,
our civil government, and our businesses and callings?

If God calls men to responsible decision-making, then He must
have standards of righteousness that guide men in their decision-
making. It is the work of the ICE to discover, illuminate, explain,
