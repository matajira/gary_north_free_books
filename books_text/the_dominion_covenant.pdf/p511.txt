Witnesses and Judges 467

tion. He brings His judgment to pass in history. This is His progres-
sive judgment. He will declare and execute judgment at the end of
time. This is His finaf judgment. Men can therefore render provi-
sional judgment because God has declared His judgment and His
standards of enforcement in His law. He declared Himself defini-
tively to Moses, as He had to Adam. Men are therefore called to
render earthly, provisional judgment in God’s name, as His lawful
subordinates. But they must render honest judgment in terms of His
law.

The temptation in the garden was in the form of a judicial pro-
ceeding. So is all of life. We. are to render provisional, subordinate
judgment in every area of life. We are to master God's law so that we
can render honest judgment, just as Adam and Eve should have ren-
dered provisional judgment against Satan in the garden by avoiding
him and the forbidden tree before God returned physically to render
final judgment.

Man wanted to be able to render autonomous, instant judgment.
He ate of the forbidden tree. What he found was that final judgment
is delayed. It is delayed against him, but it is also delayed against
Satan. Satan remains man’s enemy, bruising man’s heel. God threw
Adam and Eve out of the garden and banned their return to it physi-
cally, in time and on earth. But he offered them grace and a promise:
man will eventually crush the head of the serpent. Redeemed men
will witness against him formally, in the court of life, and then ex-
ecute judgment against him. But now the delay in God’s physical
return would be more than one afternoon, Rebellious man declared
instant judgment against God and for Satan; redeemed man must
now struggle against Satan and the works of Satan’s people, develop-
ing his good judgment over time.

Man must serve as a judge. He must declare judgment progress-
ively in terms of God’s definitive judgment and the promised final judg-
ment. Man is now outside the garden, which was to have served as
his training ground before he entered the world at large. Now the
garden is closed to him, and the earth is cursed. This cursing of the
ground also delays man’s judgment, under God. It takes longer to
render judgment as he works under God to build up the kingdom of
God, in time and on earth. He struggles ethically against Satan and
physically against the thorns. Adam had hoped to be an instant
judge, but only Satan was willing to promisc him that option, and
then only if he testified against God.
