218 RAPTURE FEVER

Church to fulfill the Great Commission during the so-called
Church Age. The primary self-inflicted wound of dispensation-
alism is its antinomianism, best expressed in the slogan: “No
creed but Christ, no law but love.” This slogan is in fact a creed
-a creed designed for and approved by adulterers: “no law but
love.” This creed has produced a stream of adulterers since
1980: men who have publicly affirmed their commitment to
dispensationalism, several of them doing so on their cable tele-
vision shows. Dispensational theologians may choose to shrug
off the antics of these adulterers, as well as that serial polyga-
mist who marries and divorces as if he were playing musical
chairs. They may choose to say (as always, in private), “There is
_Simply no relationship between dispensationalism’s rejection of
biblical law and the unending stream of sexual scandals that
afflict our movement.” But there és a relationship between
sanctions and behavior. These adulterers are not immediately
defrocked permanently by their churches. No law but love!
Dispensationalism is nearing the end of the road. Its aca-
demic defenders have departed to a better world. Its present
academic representatives no longer write systematic theologies.
They refuse to write scholarly monographs that show precisely
how recent suggestions for revisions to their theological system
actually fit together, and how these revisions will not under-
mine the received system. In the 1990's, all but one (Rev. Ice)
have refused to respond in print to the intellectual challenges
from those theonomic theologians who have been published by
one or more of the publishing firms I control. Since at least
1965, they have played “let’s pretend” and “the silence of the
lambs.” What they are really playing is blind man’s bluff.
On January 5, 1993, a Dallas Seminary faculty member sent
me a letter. It said, among other things:

Lhave not paid much attention to your writings because you
principally have been occupied with attacking us and misrepre-
senting our point of view. If you expect any scholarly response
