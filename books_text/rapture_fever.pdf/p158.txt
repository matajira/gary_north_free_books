124 RAPTURE FEVER

such practical efforts necessarily and steadily falls by default to
theonomic (God’s law) postmillennialists. Simultaneously, post-
millennialist scholars, because they do believe that such com-
prehensive social transformation is not only possible but inevita-
ble, work hard to achieve dominion in history.

Pessimillennialists self-consciously preach the progressive
future failure of the gospel and therefore the inability or un-
willingness of the Holy Spirit to transform the world positively
in terms of kingdom standards. Dave Hunt goes so far as to say
that God Himself is incapable of establishing His kingdom on
earth: “In fact, dominion ~ taking dominion and setting up the
kingdom for Christ - is an impossibility, even for God. The mil-
lennial reign of Christ, far from being the kingdom, is actually
the final proof of the incorrigible nature of the human heart,
because Christ Himself can’t do what these people say they are
going to do... .”8

Whether premillennialist scholars like it or not, Dave Hunt
has become the spokesman for premillennial social philosophy
in this decade. He is the best-selling premillennialist author.
Silence by premillennialist leaders regarding Hunt’s books and
his kingdom-denying conclusion is an admission that he in fact
speaks for premillennialism today. Traditional kingdom-affirm-
ing premillennialists lose theologically to Hunt by default.

And once they lose theological leadership to Hunt, they lose
intellectual leadership to the Christian Reconstructionists.

Intellectual Leadership: Losing by Default

This does not mean that non-postmillennialists will never
produce works in the field of applied Christian theology. Dutch
amillennialists have done so. Premillennialists have done so,
especially in the field of natural science.® Nevertheless, it is not

8. Dave Hunt, Tape Two, “Dominion and the Cross,” in Dominion: The Word And
New World Order.

9. Almost always, however, from the point of view of historic pessimism: an
