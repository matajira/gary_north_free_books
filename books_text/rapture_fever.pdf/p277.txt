natural, xxvii, 95, 97, 113,
141, 210
rejection of, 83
victory, 206
victory &, 202
“wisdom,” 140-41
leaven, 139, 150-51
lecture notes, 154-55
Lewis, David Allen, 40-42
Liberty University, 194, 199-
201
Lindsey, Hal
anti-semitism, xxxiii
Dallas Seminary, 159
fading, 59
ghostwriter, 1-2
inside dope, 3
lack of confidence, 149
Late Great Planet Earth, 1
newspapers, 22, 216
prophet, 18
publisher (666), 189
pusher, 3
response to, 159
Lord's Prayer, xiv, 76, 94
Lord’s Supper, 80-81
losers, 202

MacArthug John, 172, 175-78
Macdonald, Margaret, 105, 137
MacPherson, Dave, 137-38, 153
Maginot Line, 211

Man of Sin, 45

McClain, Alva, 139, 98

McGee, J. Vernon, 100

meek, xiii

Migne, J.P, 115

milk-drinkers, 202

243

Milton, John, 140

Montezumas’ two-step, 191

Moody Press, xxix, 168

Moral Majority, 183, 184, 199-
200

Morris, Henry, xxix, 73, 167

motivation, 66

murder, 142

Murray, John, 146

National Affairs Briefing Con-
ference, 180-87

Nato, 30

Natural law, xxvii, 95, 97, 113,
141, 210

Nero, 52

Neuhaus, Richard, 42

neutrality, 42, 95, 151, 181

New Age, 73-74, 74, 80

New World Order, 194

newspaper exegesis, 22-23

newspapers, 216

night shift, 78

Noah, 142

Nobel, David, xxv

obedience, xviii
occultism, 39
Owen, John, 152

Packer, J. 1, 173
padded cell, 119
Pamona College, xxvii
paradigm shift
defensive mentality, 6
delayed deliverance, xxxv
evidence, 15-16
Grace Seminary, 197
