Conclusion 221

surely the situation today) or else they have assumed that their
followers are not readers of theological books, and. therefore
book-length criticisms by other theologians are dismissed as
institutionally irrelevant. The dispensational movement at best
throws up one book per half generation to defend the system.
In the case of Dominion Theology: Blessing or Curse?, its academic
co-author immediately began to retreat, both geographically
and theologically. Dr, House may no longer be a dispensation-
alist; surely he no longer defends the traditional system with
the fanatic though incoherent determination that Rev. Ice does.
Ice is the dispensational movement's last visible defender; his
newsletters reply to the system’s major critics, namely, Christian
Reconstructionists. No one else bothers to defend the system.

Without either a long-term strategy of cultural replacement
or a strategy of rapid and comprehensive intellectual defense,
a movement can recruit and retain only the less bright and less
dedicated members of the next generation. This is the situation
in which dispensationalists find themselves today.

My conclusion: we are witnessing dispensationalism’s terminal
generation. Just wait.

BERRA E AE REE

For those of you who have been persuaded by my argu-
ments, and also for those who are at least curious, I invite you
to request a free six-month subscription to the monthly newslet-
ter, Dispensationalism in Transition, written by Dr. Ken Gentry,
the author of He Shall Have Dominion, The Beast of Revelation,
and Before Jerusalem Fell: Dating the Book of Revelation. Since
1988, this newsletter has covered the issues that dispensational
theologians refuse to discuss. To subscribe, write to:

Dispensationalism in Transition
P.O. Box 8000
Tyler, TX 75711
