Introduction 17

Christian Reconstruction. The dispensationalists of 1970 had no
problem identifying their theology and its social implications.
Today’s dispensationalists.do. This is evidence of their quiet aban-
donment of traditional dispensationalism.

Conclusion.

First, I ask six very simple questions regarding the world’s
premier dispensational institution of theology; Dallas Theologi-
cal Seminary: (1) What is Dallas Theological Seminary’s position
on abortion? (2) What is its position on the legitimacy of public
education? (3) Where is its textbook on New Testament social
ethics? (4) Where is its systematic theology? (5) Why did the
seminary in 1988 refuse to republish Lewis Sperry Chafer’s
Systematic Theology (1948)? (6) Why has no one on the faculty
written a point-by-point refutation of O. T. Allis’ Prophecy and
the Church? (I omit Charles Ryrie, whose attempted refutation in
Dispensationalism Today in 1965 was both partial and brief, and
who subsequently disappeared from the faculty.)

Second, 1 ask five questions regarding dispensationalism in
general: (1) Where is a dispensational, Ph.D. degree-granting
university (other than family-operated Bob Jones University)?
(2) Where is a dispensational college whose faculty members in
every department place the Bible as the foundation and final
court of appeal for the actual content of their courses? (3)
Where are college-level textbooks that present a dispensational
view of philosophy, education, psychology, economics, civil
government, architecture, the arts, mathematics, biology, geolo-
gy, and paleontology? (4) Where is a non-charismatic dispen-
sational law school? Medical school? (5) Why have charismatic
dispensationalists dominated cable television rather than tradi-
tional dispensationalists?

Here is my main question: Is the absence of dispensational
leadership in every area of life related to dispensationalism’s
theology? J think it is. Some readers may not. What other ex-
planation makes sense except the theology of dispensationalism
