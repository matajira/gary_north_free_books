52 RAPTURE FEVER

Israel and the destruction of the temple in A.D. 70 - then who
was the Beast? After all, if New Testament prophecies regard-
ing the Beast were not fulfilled during the lifetime of John, but
refer to some individual still in the Church’s future, there
would seem to be no reason to believe that the other prophe-
cies regarding “the last days” were also fulfilled in his day.
These prophecies must be taken as a unit. It is clear that the
Beast is a figure who is said to be alive in the last days. This is
why it is imperative that we discover who the Beast is or was. If
he has not yet appeared, then the last days must also be ahead
of us, unless we have actually entered into them. If he has
already appeared, then the last days are over.

Gentry’s studies prove beyond much doubt that the prophe-
sied Beast was in fact the emperor Nero. (So, for that matter,
does David Chilton’s commentary on the Book of Revelation,
The Days of Vengeance). Gentry’s books are not filled with pro-
phecies about brain-implanted computer chips, tatoos with
identification numbers, cobra helicopters, nuclear war, and New
Age conspiracies. This is why most fundamentalists are not
interested in his books. Customers of most Christian bookstores
too often prefer to be excited by the misinformation provided
by a string of paperback false prophecies than to be comforted
by the knowledge that the so-called Great Tribulation is long
behind us, and that it was Israel’s tribulation, not the Church's.
(For biblical proof, see David Chilton’s book, The Great Tribula-
tion.)"* They want thrills and chills, not accurate Bible exposi-
tion; they want a string of “secret insights,” not historical knowl-
edge. Like legions of imaginative children sitting in front of the
family radio back in the 1930’s and 1940’s who faithfully
bought their Ovaltine, tore off the wrapper, and sent it in to
receive an official “Little Orphan Annie secret decoder,” funda-

22. David Chilton, The Days of Vengeance: An Expasition of the Book of Revelation (Ft.
Worth, Texas: Dominion Press, 1987).

28. David Chilton, The Great Tribulation (Ft. Worth, Texas: Dominion Press,
1987).
