Dispensationalism Remaves Earthly Hope 81

cises covenantal judgment in the midst of the congregation
during the Lord's Supper, which is why seff-judgment in advance
is required.

Wherefore whosoever shall eat this bread, and drink this cup
of the Lord, unworthily, shall be guilty of the body and blood of
the Lord. But let a man examine himself, and so let him eat of
that bread, and drink of that cup. For he that eateth and drink-
eth unworthily, eateth and drinketh damnation to himself, not
discerning the Lord’s body. For this cause many are weak and
sickly among you, and many sleep. For if we would judge our-
selves, we should not be judged. But when we are judged, we
are chastened of the Lord, that we should not be condemned
with the world. Wherefore, my brethren, when ye come together
to eat, tarry one for another (I Cor. 11:27-33).

I suspect that it is dispensationalism’s lack of emphasis on
the sacrament of Holy Communion that has led them to adopt
the strange belief that Satan’s kingdom rule is real even though
he is not physically present on earth, yet Jesus’ kingdom reign
cannot become real until He is physically present on earth. In
each case, the two supernatural rulers rule representatively. In
neither case does the Bible teach that the supernatural ruler
needs to be bodily present with his people in order for him to
exercise dominion through them.

Obvious, isn’t it? But when have you héard a sermon or read
a book that mentions this?

No Earthly Hope

If the Church is just about out of time, as dispensational
authors keep insisting, decade after decade, then what legiti-
mate hope can Christians have that they can leave the world a

Supper that involves anything more than a memorial: ibid, p. 302.
