A Commitment to Cultural Irrelevance 107

book. It also appeared on the surface to be a scholarly book.
Therefore, it sank without a trace; fundamentalist readers are
not interested in scholarly books. House Divided buried that ill-
conceived effort, and in so doing, buried the last vestiges of
dispensational theology.'®

In 1989 the Berlin Wall came down. In 1990, Iraq invaded.
Kuwait, and Dallas Seminary professor Charles Dyer rushed
into print with his paperback sensation, The Rise of Babylon: Sign
of the End Times. Not to be outdone, John Walvoord resurrected
his 1974 potboiler, Armageddon, Oil, and the Middle East Crisis.
Book sales soared, only to crash in flames in February, 1991,
when the U.S. military smashed Iraq’s army. Later that year,
the attempted coup by hard-line Soviet bureaucrats failed to dis-
lodge Boris Yeltsin, but it did ruin Gorbachev's career. So
much for Robert Faid’s 1988 potboiler, Gorbacheo! Has the Real
Antichrist Come?

This feeding frenzy of “ticking clock” prophecy books cooled
after the Soviet Union began falling apart. Israel’s predicted
invader from the north no longer has any viable candidates.
The State of Israel no longer faces any nation that conceivably
can assemble an army of millions. The paperback experts in
Bible. prophecy again look like fools and charlatans. The dis-
pensational movement has once again been publicly embar-
rassed by its book royalty-seeking representatives. The world
howls in derisive laughter, for good reason. The paperback
prophecy experts conveniently forget about Nathan's accusation
against David: “... by this deed thou hast given great occasion
to the enemies of the Lorp to blaspheme. . .” (II Sam. 12:14).
They, too, have given great cause to the many enemies of God
to ridicule Christianity. But, unlike David, who repented of his
sin, these people keep repeating theirs, updating nonsense.

18, Greg L. Bahnsen and Kenneth L. Gentry, Jn, House Divided: The Break-Up of
Dispensational Theology (Tyler, Texas: Institute for Christian Economics, 1989).
