Dispensationalism Removes Earthly Hope 79

be the dispensationalists’ least favorite Bible passage, for good
reason.

‘The Lorn said unto my Lord, Sit thou at my right hand, until
I make thine enemies thy footstool. The Lorp shail send the rod
of thy strength out of Zion: rule thou in the midst of thine ene-
mies (Psa. 110:1-2).

This passage makes it clear that a legitimate goal of God's
people is the extension in history and on earth of God’s king-
dom, to rule in the midst of our spiritual enemies and oppo-
nents, But more to the point, the Lord speaks to Jesus Christ
and informs Him that He will sit at God’s right hand until His
enemies are conquered. Obviously, God's throne is in heaven.
This is where Jesus will remain until He comes again in final
judgment. Jesus sits tight while His people extend His rule.

This is also what is taught by the New Testament’s major
eschatological passage, I Corinthians 15. It provides the context
of the fulfillment of Psalm 110. It speaks of the resurrection of
every person's body at the last judgment. Jesus’ body was resur-
rected first in time in order to demonstrate to the world that
the bodily resurrection. is real. (This is why liberals hate the
doctrine of the bodily resurrection of Christ, and why they will
go to such lengths in order to deny it.)* This passage tells us
when all the rest of us will experience this bodily resurrection,
What it describes has to be the final judgment.

For as in Adam all die, even so in Christ shall all be made alive.
But every man in his own order: Christ the firstfruits; afterward

lennialists. Boyd, “A Dispensational Premillennial Analysis of the Eschatology of the
Post-Apostolic Fathers (Until the Death of Justin Martyr).” Gary DeMar summarizes
Boyd's findings in his book, The Debate Over Christian Reconstruction (Ft. Worth, Texas
Dominion Press, 1988), pp. 96-98, 180n.

6. A notorious example of such literature is Hugh J. Schonfield, The Passover
Plat: New Light on the History of Jesus (New York: Bantam, [1966] 1971). It had gone
through seven hardback printings and 14 paperback printings by 1971.
