INTRODUCTION

|
¥ are the salt of the earth: but if the salt have lost his savour, where-
with shall it be salted? it is thenceforth good for nothing, but to be cast
out, and to be trodden under foot of men (Matt. 5:13).

In 1970, Hal Lindsey and ghostwriter C. GC. Carlson wrote a
hook, The Late Great Planet Earth. It was eventually to sell over
35 million copies. It became the best-selling nonfiction book of
the 1970's. Prior to the publication of this book, Lindsey had
been known, if at all, only as a successful southern California
college-age youth pastor in the UCLA area. After its publica-
tion, he became the premier international spokesman for dis-
pensationalism.

This placed dispensationalism in a dilemma. Its best-known
representative was not a theologian. He had to employ an
assistant to write his books.’ The basis of his reputation was a
sensational paperback book that made a series of predictions
regarding the nation of Israel and the imminent return of
Christ in secret to pull Christians into heaven: the doctrine of
the pre-tribulation Rapture. The book dealt with contemporary
prophecy, not permanent theology. It made Lindsey a fortune.
(If Lindsey is an honest man, C. C. Carlson made one, too.)

1. This is not inherently a bad idea. There is a division of labor in life (I Cor.
19). A lot of authors could dearly use an openly acknowledged ghost writer. But
employing one has never been regarded as academically acceptable.
