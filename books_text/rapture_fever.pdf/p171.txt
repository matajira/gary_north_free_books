House of Seven Garbles 137

As I said of Dave Hunt's view, which is identical to Ice’s, “Ir
sounds great, but I think he makes these things up as he goes
along.” So does Ice. These people rewrite a whole system of
eschatology in order to appeal to uninformed laymen in their
movement, and then they pretend that this is the original ver-
sion. This is not what I would call honest dealing with one’s
overly trusting followers.

Garble #3: The Old Historical Shell Game

In their chapter, “Is Premillennialism a Heresy?” they attack
David Chilton for the latter’s accusation that premillennialism
was first invented by Cerinthus, a second-century heretic. They
acknowledge that nobody else in the Reconstructionist camp
has sided with Chilton on this, and they quote me in saying
that many of the early Church fathers were premillennial. Chil-
ton’s gift is exegesis, not historiography, so I will not run to his
defense at this point.

What is important to understand is that by spending a chap-
ter defending the early Church origins of premillennialism, the
authors are playing a game that has been basic to Dallas Semin-
ary’s creaky defense of its faith: dead silence regarding the 1830
origin of the pre-tribulation Rapture doctrine. Post-trib dispensation-
alist Dave MacPherson has inflicted a devastating wound on the
pre-trib camp by showing that a teenage Scottish girl named
Margaret Macdonald, a disciple of a mystic named Edward
Irving, came up with this doctrine during a private “revelation
from God."* The traditional Dallas Seminary-taught view has
always been that John Nelson Darby discovered the doctrine in
1830. In either case, traditional pre-tribulational dispensationalism
cannot trace its origins back te anyone prior to 1830.

If Mr. MacPherson is categorically wrong, as Mr. Ice insisted
that he is in a letter to me, then why hasn’t Dallas Seminary’s

9, MacPherson, The Unbelievable Pretrib Origin, The Great Cover-up, and The Great
Rapture Hoax,
