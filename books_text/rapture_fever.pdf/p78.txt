44 RAPTURE FEVER

gogue system over the temple. So central was the destruction of
the temple to the future of both Christianity and Judaism that
Jesus linked it symbolically to His death and resurrection:

Then answered the Jews and said unto him, What sign shew-
est thou unto us, seeing that thou doest these things? Jesus
answered and said unto them, Destroy this temple, and in three
days I will raise it up. Then said the Jews, Forty and six years
was this temple in building, and wilt thou rear it up in three
days? But he spake of the temple of his body (John 2:18-21).

Dating The Book of Revelation

“But,” you may be thinking to yourself, “John wrote the
Book of Revelation (the Apocalypse) in A.D. 96. Everyone
agrees on this. Thus, John could not have been prophesying
events associated with the fall of Jerusalem, an event that had
taken place a quarter of a century earlier.” This is the argu-
ment of Dallas Theological Seminary professor Wayne House
and Pastor Tommy Ice in their theologically creative but highly
precarious revision of traditional dispensationalism.” It is also
the intellectual strategy taken by best-selling dispensational
author Dave Hunt, who writes in his recent defense of Chris-

Jerusalem (which lives on as Orthodox Judaism): “Until the destruction of the
Second Temple in A.D.-70 they had counted as one only among the schools of
thought which played a part in jewish national and religious life: after the Destruc-
tion they took the position, naturally and almost immediately, of sole and undisputed
leaders of such Jewish life as survived. Judaism as it has continued since is, if not
their creation, at least a faith and a religious institution largely of their fashioning;
and the Mishnah is the authoritative record of their labour Thus it comes about that
while Judaism and Christianity alike venerate the Old Testament as canonical Scrip-
ture, the Mishnah marks the passage to Judaism as definitely as the New Testament
amarks the passage to Christianity.” Herbert Danby, “Introduction,” The Mishnah (New
York: Oxford University Press, [1933] 1987), p. xiii. The Mishnah is the written
version of the Jews’ oral tradition, while the rabbis’ comments on it are called Gem-
ara, The Talmud contains both Mishnah and Gemara. See also R. Travers Herford,
The Pharisees (London: George Allen & Unwin, 1924).

12, H. Wayne House and Thomas D. ice, Dominion Theology: Blessing or Curse?
(Portland, Oregon: Multnomah, 1988), pp. 249-60.

 

 
