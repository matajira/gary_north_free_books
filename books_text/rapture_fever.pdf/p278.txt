244 RAPTURE FEVER

intellectual, xxxi
moral, xxxv
process of, 6-8
questions, 11-14
Parenthesis, 23
(see also Church Age)
Passover, XX
Pentecost, 149-51
Pentecost, J. Dwight, 136
perfection, 178
pessimillennialism, 62, 126
pessimism, 67-71, 82-83, 99,
182
Pharisees, 43, 112
pietism, 102, 185
pluralism, 95, 102, 117
polishing brass, 100
politics, 4, 8, 13, 40-43, 95,
117, 181, 182
positive feedback, xxi, 62
postmillennialism, 71, 73-74,
119, 124, 126, 138
power, xvii, xix
power religion, 42
premillennialism, 98, 153
progress, 61, 62, 73,

prophecies, 19-20, 123-24, 189-

90
prophecy, 43-44
prophecy clock

{see clock of prophecy)
Puritanism, 138
Puritans, 62

Rapture
any moment, 23-25, 58
date, 210
delayed 1,950 years, 89

deliverance, 206

fading, 102

false hope, 105

helicopter escape, 87-88

impossible, 85

missing word, 183

passivity, 4

politics &, 182-83

postponed, 29-31, 191-92

responsibility vs., 60

Russia &, 27-29, Chap. 12

shelved, 1980, 4
Rapture fever

blessed hope, 75

contagious, 2

Falwell, 1992, 200

ghetto, 10

heart, 22

inside dope, 2-3

newspaper exegesis, 22

older generation, 88

played out?, 18

rationalism, 92

Reagan, 54, 180

scrambled theology, 90

subsided 1991, 189

responsibility
blessings &, ix
clock of, 39
dispensationalism vs., 109,
128, 192-93
escaping, ix, xxiii, 60
resisting Caesar, 111
victory, xx-xxi, 86
wisdom, xviii
representation, xix, 178
rescue mission, 67
