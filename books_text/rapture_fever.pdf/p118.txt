84 RAPTURE FEVER

Legitimizing Cultural Retreat

Because he has no faith in the long-term efforts of Christians
to transform this world through obedience to God, the consis-
tent dispensationalist retreats from the hard conflicts of society
that rage around him, just as the Russian Orthodox Church did
during the Russian Revolution of 1917. The existence of this
dispensationalist attitude of retreat is openly admitted by dis-
pensational pastor David Schnittger:

North and other postmillennial Christian Reconstructionists
label those who hold the pretribulational rapture position pietists
and cultural retreatists. One reason these criticisms are so painful
is because I find them to be substantially rue. Many in our camp
have an all-pervasive negativism regarding the course of society
and the impotence of God’s people to do anything about it. They
will heartily affirm that Satan is Alive and Well on Planet Earth,
and that this must indeed be The Terminal Generation; there-
fore, ary attempt to influence society is ultimately hopeless. They
adopt the pictistic platitude: “You don’t polish brass on @ sinking
ship.” Many pessimistic pretribbers cling to the humanists’ ver-
sion of religious freedom; namely Christian social and political
impotence, self-imposed, as drowning men cling to a life preserv-

ent

Removing Illegitimate Fears

David Chilton shows in The Great Tribulation that Christians’
fears regarding some inevitable Great Tribulation for the
Church are not grounded in Scripture. Kenneth Gentry shows
in his books on Bible prophecy that the Beast of Revelation is
not Jurking around the corner. Neither is the Rapture. Thus,
Christians can have legitimate hope in the positive earthly
outcome of their prayers and labors. Their sacrifices today will
make a difference in the long run. There is continuity between

14. David Schnittger, Christian Reconstruction from a Pretriblational Perspective
(Oklahoma City: Southwest Radio Church, 1986), p. 7.
