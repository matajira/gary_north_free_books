Flouse of Seven Garbles 139

This shell game has been going on at Dallas for decades.
“Keep your eye on the pre-trib, premillennial pea, my friends.
See how it goes here, under-this 1830 pretribulation Rapture
shell. Now, with just a few deft shuffles . . . presto: we now find
it under the historic premillennial shell of the early Church!
Thus, we can see that C. I. Scofield was a defender of eschatol-
ogical orthodoxy. Now, take this. postmillennial pea. We place
it under the seventeenth-century Puritan shell. A few deft
shuffles . . . presto: we now find it under Daniel Whitby’s eigh-
teenth-century unitarian shell. Thus, we can see that postmil-
lennialism has very questionable ancestry!” Ice and House
continue to push around these mostly empty shells. Bad habits
picked up in one’s youth are difficult to break.

Garble #4: An Inner Kingdom Only

Matthew 13 is filled with parables about God’s kingdom in
history. Several proclaim the Church’s continuity in history, also
called the leaven principle (Matt. 13:33-34): the influence of the
gospel continues to transform history without a break until the
final judgment. These parables are the most difficult passages
in the Bible for premillennialists, The authors cite Grace Sem-
inary’s Alva J. McClain, who referred to them as “these difficult
parables” (p. 226). They are not difficult for postmillennialists!
On the contrary, they are foundational.

To escape the Reconstructionists’ accusation that dispensa-
tionalism is socially paralyzing, they say: “Dispensationalists
agree that it is wrong to limit God to only the spiritual or inner
realm. This is why we so strongly believe in a literal kingdom of
Christ, which will encompass his rule over every area of life” (p.
247, note 65). They have now given the game away. The external
kingdom is supposedly exclusively millennial, i.e. future. They say
that insofar as we are speaking of the so-called Church Age —
the here and now — God has limited His kingdom to the inner
or spiritual realm. This is what author Dave Hunt says repeat-
edly in his Seduction books.
