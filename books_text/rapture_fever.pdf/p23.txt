Preface xRV

It is that the children of dispensationalists are being sent to
state universities by their parents, where they abandon their
parents’ religion. From the 1870’s until the 1970's, dispensa-
tionalists self-consciously withdrew from the world into a kind
of cultural and emotional ghetto. But, beginning in the years
following World War II, they began sending their children off
to college, which generally meant tax-funded humanist colleges.
They want their children to climb the ladder of upward eco-
nomic mobility, and this means college. There is a heavy price
to pay for this mobility — the risk of an eternal tuition payment.
Christian parents vaguely recognize this, but they think, “My
child is ready for this challenge.” It is a safe guess to say that
half of them are not ready for it, and this may be much too low
an estimate.

Surviving College

To survive the gauntlet of the secular college, an intelligent
student needs defenses: emotional, institutional, and intellectu-
al. He is not provided with these defenses in his high school
years unless he has been subjected to a Christian curriculum.
Few fundamentalists send their children to Christian high
schools. Fewer still home school their children. In his excellent
two-week summer seminars, David Nobel asks each group of
150 students how many attend or attended public high schools.
At least 80% of the students raise their hands. Nobel says two of
his sessions have a much lower percentage: the first one, held
before schools normally get out, since this one is attended by
home schoolers; and the last one, on six-day creationism.’

This information reinforces my main point: dispensation-
alism is losing the war to humanism. There must be a systemat-
ic effort on the part of Christian parents to train up their chil-
dren in the way they should go, but dispensational parents are
unwilling to do this. They voluntarily turn their children over

L, Summit Ministries, 935 Osage Ave., Manitou Springs, Colorado 80829.
