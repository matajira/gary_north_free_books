126 RAPTURE FEVER

Eschatology unquestionably matters in the life of a Christian
scholar who regards his life’s work as anything more important
than going through a series of academically acceptable intellec-
tual exercises. Posumillennialism is an important motivation to
those scholars who are self-consciously dedicated to long-term
Christian Reconstruction. I devote ten hours a week, fifty weeks
per year, to writing my economic commentary on the Bible.
Anyone who holds a different eschatology is unlikely to sit
down for ten hours or more per week, for thirty or forty years,
to discover exactly what the Bible teaches about a real-world
subject, and how its principles might be applied by people in
the New Testament era. J win by their default.

Time is on the Reconstructionists’ side, not the side of our
many critics. I believe that Christians have plenty of time to
work toward the transformation of this world, so I work long
and hard to publish the intellectual foundations of this trans-
formation. In contrast, pessimillennialists believe that, Jesus is
coming soon. They waste little time on such “utopian” intel-
lectual projects. I see hope in long-term scholarship; pessimil-
lennialists see little hope in long-term anything.

Time is also on our side in another sense. Christian Recon-
structionist authors have built up a large body of published
materials. The more we write, the more difficult it is for anti-
Reconstruction scholars to refute us: too much material to
refute easily. We can also respond to them within thirty days:
newsletters. To put it bluntly, we Reconstructionists have mail-
ing lists, non-profit foundations with some money in the bank,
and at least a small and dedicated market of book buyers.

Christian Reconstruction in general is winning the war of
ideas through our critics’ default. They have not done their
academic homework. Literate Christians recognize this.

Our Christian critics really do believe that they can fight
something (a growing body of Reconstructionist literature) with
nothing (snide remarks, an occasional book review in some
unread academic periodical, unpublished grumbling, and above
