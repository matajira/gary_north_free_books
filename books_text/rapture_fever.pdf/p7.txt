FOREWORD
(to be read)

For which of you, intending to build a tower sitteth not down first,
and counteth the cost, whether he have sufficient to finish it? Lest haply
fit happen}, after he hath laid the foundation, and is not able to finish it,
all that behold it begin to mock him, Saying, This man began to build,
and was not able to finish (Luke 14:28-30).

Count the costs, Jesus said. But we must also remember to
count the benefits. In this Foreword I present three major gifts
that are available to all Christians through their membership in
God's New Testament Church. The existence of these stupen-
dous gifts has been denied by many theologians. Millions of
Christian laymen have therefore been unwilling to accept these
benefits from the hand of God. I concluded that it was time for
a Christian layman to protest. I can read the Bible, too, and I
am convinced that all three of these gifts are not only available
to God’s people, but also that He is highly displeased when we
deny their existence and thereby reject them.

There is a reason for Christians’ hesitancy to admit the avail-
ability of these gifts. They know that with all blessings inevitably
come duties and obligations. We never get something for noth-
ing. Even salvation requires men to live new lives that break
with their evil past (I Cor. 6:9-10; Eph. 5:1-5). Millions of
Christians think they can evade many duties and obligations if
they just refuse to accept God’s blessings. This is a terrible
