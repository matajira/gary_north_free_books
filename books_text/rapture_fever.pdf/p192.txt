158 RAPTURE FEVER

ly defend the system, they should say so. They never do, of
course, but they should.

The faculties of Talbot Seminary, Grace Seminary, and Dal-
las Seminary have been unwilling for many decades to reply to
O. T. Allis’ book, Prophecy and the Church (1945). This refusal
was entirely self-serving. Allis was the most prominent defender
of the integrity of the Old Testament’s text in his generation,
the author of The Five Books of Moses (1943). He could not be
dismissed as some crackpot or theological amateur. He was in
fact a master theologian. His comprehensive criticism of dis-
pensationalism’s eschatology remains the most powerful ever
offered. Yet almost half a century later, no dispensational schol-
ar has written a book of equal length and detail to refute Allis.
Charles Ryrie’s thin book, Dispensationalism Today (1965), was
devoted only in part to Allis.

Their failure to respond indicates an inability of dispensa-
tionalism’s academic defenders to defend the. system. Ef they
were willing to announce publicly that they are incapable of
answering a particular critic, this would be honest, but to do so
would be a kind of intellectual suicide. The fact is, a failure to
respond is intellectual suicide, but it is death by slow poison in
private rather than a quick end to one’s misery in public.

When I decided to challenge dispensationalism publicly,
beginning with my book, 75 Bible Questions Your Professors Pray
You Won't Ask (1984), I committed myself to respond immedi-
ately to any counter-attack. 1 stand ready to publish a rapid
reply to any academic critic who writes a book, and also popu-
lar critics who have a large readership. Thus, when Dave Hunt
devoted a few pages to Christian Reconstruction in his Seduction
of Christianity (1987), I hired Gary DeMar and Peter Leithart to
write The Reduction of Christianity (1988). That book appeared
within 12 months of Hunt’s effort. do my best to reduce to a
minimum the time elapsed between the criticism and our res-
ponse. When Hunt and Tommy Ice took on Gary DeMar and
me in April of 1988, I had DeMar’s The Debate Over Christian
