Fear of Men Produces Paralysis Al

by Christians to speak to political issues as people — or worse,
as a people — who possess an explicitly biblical agenda will
invite “unnecessary persecution.” He recommends silence.

We see once again dispensationalism’s concept of evangelism
as tract-passing, a narrowly defined kingdom program of exclu-
sively personal evangelism that has one primary message to
every generation, decade after decade: flee the imminent wrath to
come, whether the Antichrist’s (the Great Tribulation) or the
State’s- (“unnecessary persecution”). This is a denial of the
greatness of the Great Commission,’ but in the name of the
Great Commission: “Our vision is to obey and fulfill the com-
mand of the Great Commission.”*

Mr. Lewis says that we can legitimately participate in politics
as individuals, since our government is democratic: “. . . we en-
courage Christians to get involved on an individual basis, in all
realms of society, including the political arena.” Should our
goal be to change society fundamentally? Hardly. This is an
impossible goal. Our goal is to gain new contacts in order to
share the gospel with them. “This is partly to insure that Chris-
tians are in place in every strata of society for the purpose of
sharing the gospel. message.”* The purpose of political and
social involvement is not to reform the world; it is to tell people
about the imminent end of this pre-millennium world. We are
apparently not supposed to say anything explicitly Christian or
vote as an organized bloc (the way that all other special-interest
groups expect to gain’ political influence).’ “To be involved in

2. Kenneth L. Gentry, Jn, The Greatness of the Great Commission: The Christian
Enterprise in a Falien World (Tyler, Texas: Institute for Christian Economics, 1990).

3. Lewis, Prophecy 2000, p. 282.

4. Idem.

5, This is traditional democratic theory, but it has never really come to grips with
the reality of political power. The Counci! on Foreign Relations and the Trilateral
Commission do not organize voters into blocs. They simply make sure that they
control who gets appointed to the highest seats of power and what policies are
enacted. This raises other questions, which, being political, are not the focus of my
concern here. See Gary North, Conspiracy: A Biblical View (Ft. Worth, Texas: Domin-
