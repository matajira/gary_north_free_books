58 RAPTURE FEVER

Question: If the pre-tribulation Rapture can come “at any
moment,” then how can there be any fulfilled prophecies to
write about that take place in between the New Testament
documents and the future Rapture? How can there be any
“prophetic signs of the times”? How can anyone who believes in
the “any moment coming” of Jesus also believe some self-de-
clared prophecy expert who announces that specific Bible pro-
phecies are being fulfilled in our day? If any event is said to be
a fulfilled Bible prophecy today — an event that absolutely had
to take place, as all true Bible prophecies obviously must - then
the Rapture surely was not an “any moment Rapture” prior to
the fulfillment of the allegedly fulfilled prophecy, Some proph-
esied event therefore had to happen before the Rapture could
occur. This, obviously, is a denial of the doctrine of the “any
moment coming” of Christ. This fact does not seem to deter
any particular decade's reigning paperback prophets or their
gullible disciples.

The Paralysis Factor

Once a particular prophecy expert's predictions begin to be
perceived as being embarrassingly inaccurate, another expert
appears with a new set of prophecies. Christians who become
temporary followers of these false prophets become ominously
similar to the misled women described by Paul: “For of this sort
are they which creep into houses, and lead captive silly women
laden with sins, led away with divers lusts, Ever learning, and
never able to come to the knowledge of the truth” (IJ Tim. 3:6-
7). Eventually, these frantic (or thrill-seeking) victims become
unsure about what they should believe concerning the future.
Everything sounds so terrifying. Christians become persuaded
that personal forces beyond their control or the Church’s con-
trol ~ evil, demonic forces — are about to overwhelm all remain-
ing traces of righteousness. How, after all, can the average
Christian protect himself against mind control and memory
transfer, let alone head transplants, assuming such things are
