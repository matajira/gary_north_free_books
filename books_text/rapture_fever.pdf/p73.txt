Endless Unfulfilled Prophecies Produce Paralysis 39

visible. It will do no good to remain silent, either. But dispensa-
tionalists will remain silent. This is the only strategy they know.

‘The sad thing is that fringe Rapture scenarios are becoming
wilder and wilder, mixed with pyramidology, UFO’s, and other
occult materials.” As the year 2000 approaches, this “invasion
by the fringe” will escalate. This escalation of expectation of the
Rapture will tend to paralyze the Church as an institution of
salt and light as the 1990’s unfold. When this expectation is also
fueled by occultism, it cannot have anything but negative conse-
quences for dispensationalism.

A clock is indeed ticking. It is the clock of responsibility. We
have all been given assignments by God and enough time to
complete them in life (Eph. 2:10). Christian institutions have
been given assignments by God through their officers. This is
why eschatology matters. This is why the Institute for Christian
Economics sometimes publishes books on eschatology. A per-
son’s time perspective is important for the tasks he begins, the
capital he invests, and the rate of return he seeks. The shorter
the time remaining, the more capital we need when we begin
our tasks and the higher the rate of return we need to com-
plete them. This is also true of God’s Church. Each person,
each church, each family, each civil government, and each
organization must decide how much available time seems to
remain. Our goals and plans, both personal and institutional,
should reflect this assessment. False prophecies, decade after
decade, regarding an inevitably imminent Rapture distort this
assessment.

Christianity has lots of time remaining. Dispensationalism
doesn’t. This is the message of Rapture Fever.

29. William M. Alnor, Seothsayers of the Second Advent (Old Tappan, New Jersey:
Revell, 1989), Part IV.
