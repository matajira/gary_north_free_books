248 The Debate over Christian Reconstruction

haps the continental Reformer who had the most direct influence
on English Puritanism, wrote in a 1568 commentary on Romans
that Paul prophesied a future conversion of the Jewish people.
Peter Martyr, Bucer’s associate in Strasbourg, agreed.®

In England, the place of the Jews in prophecy was a promi-
nent issue already in the seventeenth century, and, significantly,
this was most true among the generally postmillennial English
Puritans and Scottish Presbyterians. Iain Murray summarizes the
seventeenth-century concern for Israel in this way:

The future of the Jews had decisive significance for them because
they believed that, though little is clearly revealed of the future
purposes of God in history, enough has been given us in Scrip-
ture to warrant the expectation that with the calling of the Jews
there will come far-reaching blessing for the world. Puritan Eng-
land and Covenanting Scotland knew much of spiritual blessing
and it was the prayerful longing for wider blessing, not a mere in-
terest in unfulfilled prophecy, which led them to give such place
to Israel.9

This emphasis fits neatly into the postmilfennialist position:
The latter-day glory of the Church will be inaugurated by the
conversion of the Jews to Christ; this is what Paul meant when he
said that the conversion of the Jews would be “life from the dead”
(Romans 11:15). There were other views of Paul’s prophecy in
seventeenth-century England. One school of interpretation claimed
that Romans 11:26 (“all Israel shall be saved”) referred not to a
future dramatic conversion of the Jews but to the gradual conver-
sion of the Jews throughout history. It is significant that this view
was “almost uniformly rejected by English and Scottish exegetes
of the Puritan school.” They favored the postmillennial view de-
scribed above.

8. Quotations from J. A. DeJong, As the Waters Cover the Sea: Millennial Expec-
tations in the Rise of Anglo-America Missions, 1640-1810 (Kampen: J. H. Kok, 1970),
p. 9.

9. Murray, Puritan Hope, pp. 59-60.

10, Ibid., p. 64.
