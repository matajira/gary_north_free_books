200 The Debate over Christian Reconstruction

5. Gary North, Moses and Pharaoh: Dominion Religion Versus Power
Religion (Tyler, ‘I'X: Institute for Christian Economics, 1985).

6. Gary North, The Sinai Strategy: Economics and the Ten Command-
ments (Tyler, TX: Institute for Christian Economics, 1986).

7. Gary North, Tools of Dominion: The Case Laws of Exodus (Tyler,
TX: Institute for Christian Economics, 1988).

Tommy Ice begins by emphasizing motivation for ethical behav-
ior again. This has already been answered at numerous points in
this chapter, Suffice it to say, whatever one’s motivation, it does
not answer the question of what standard we should live by. Ice
continues by stating:

We [Dave Hunt and Tommy Ice or dispensationalists in gen-
eral] believe in applying the Old Testament Jaw to today, but we
do not believe we are directly under the Old Testament law be-
cause of Christ having released us from this.

There’s not too much to disagree with here. Still, 1 do believe that
Tommy Ice is confused over one point. While the law no longer
condemns the Christian, Christians have not been freed from fol-
lowing the demands of the law. Consequences do follow from dis-
obedience, as even the New Testament shows. Obviously, we
have been freed from the sacrificial system and all matters relating
to the shedding of blood. But nowhere in the New Testament does
it say that we are not to keep the law as summarized in the Ten
Commandments. Tommy Ice continues:

We believe just as Deuteronomy, chapter four, says: The nations
even in the Old Testament times would observe Israel's law and
realize that it was wise and understanding [for them to follow it].

If the law of God is good ~—it is, in fact, God’s law—then why not
promote it as the best option for the world to follow? Whether you
say it’s obligatury for the nations or the best law among all law
systems for the nations, Christians should be promoting God’s law
as a standard for personal, family, ecclesiastical, and civil right-
