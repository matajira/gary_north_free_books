104 The Debate over Christian Reconstruction

Dispensationalism, however, was not readily accepted in the
nineteenth century. In fact, even premillennialism was considered
aberrational. J. Gresham Machen, a staunch opponent of mod-
ernism, wrote:

The recrudescence of “Chiliasm” or “premillennialism” in the
modern Church causes us serious concern; it is coupled, we
think, with a false method of interpreting Scripture which in the
long run will be productive of harm.‘

David Bogue, an cighteenth-century minister in the Church of
Scotland and a great supporter of foreign missions, wrote of pre-
millennialism:

How wise and pious men could ever suppose that the saints,
whose souls are now in heaven, should, after the resurrection of
the body from the grave, descend to live on earth again; and that
Jesus Ghrist should quit the throne of his glory above, and descend
and reign personally over them here below, in distinguished
splendour, for a thousand years, may justly excite our astonish-
ment, since it is in direct opposition to the whole tenor of the dac-
trinal parts of the sacred volume. Such, however, have been the
opinions of some great men. Happy will it be if we take warning
from their aberrations.

R. B. Kuiper of the newly organized Westminster Theological
Seminary considered “Arminianism and the Dispensationalism of
the Scofield Bible” to be “two errors which are so extremely preva-
lent among American fundamentalists.” He went on to describe
them as “anti-reformed heresies.” John Murray, professor of West-
minster Theological Seminary, regarded dispensationalism as
“palpably inconsistent with the system of truth embodied in” the
Westminster Confession of Faith and its Larger and Shorter Cate-
chisms. He describes it as “heterodox from the standpoint of the

47. Machen, Christianity and Liberalism (Grand Rapids, MI: Eerdmans, [1923]
1981), p. 49.

48, Bogue, Discourses on the Millennium (1818), p. 17. Quoted in Murray, The
Puritan Hope, p. 187.
