136 The Debate over Christian Reconstruction

dicted by the dispensationalists for the distant future? I don’t be-
lieve so. The text tells us that God “will overthrow the chariots and
their riders, and the horses and their riders will go down, everyone by the
sword of another” (2:22). This is a description of pre-modern armies.
Now, unless we say that the writer had no way of describing
future events, some might conclude that chariots and swords are
nothing more than a description of implements of war for any age.””
But doesn’t this severely damage the “literal hermeneutic” espoused
by dispensationalists?

What about stars falling from the sky? In the Bible, leaders
and nations are described as stars, Their fall is an indicator of
judgment. Hal Lindsey makes this point for us:

The “star” of Revelation 9:1 has to be a person rather than a
literal star, since “he” is given a key with which he opens the bottom-
less pit. I believe this falien star is none other than Satan himself,
described in Isaiah 14:12 as “Lucifer” or “Star of the Morning.”

But some dispensationalists insist that there must be a future ful-
fillment that includes the falling of stars, Here’s one example:

Rev. 6:12 describes the sixth seal; a great earthquake, the sun
darkened, the moon becoming like blood and the stars falling. To
relate this symbolically ignores the context; it isn't poctic as some
of the OT passages are, and the response of the men is to hide in
caves. Rev. 8:12 says a third of the sun and moon and stars will be

27. The extremes of a forced literalism are found in a number of dispensa-
tional tracts that try to be “consistent” with the literal hermeneutic. In describing
the battle of Gog and Magog of Ezekie] 38 and 39, one author tries to force his
futuristic interpretation on the chariots that are burned for fuel in a future battle
between Israel and Russia (Rosh). He asks this question: “Why should nations in
the future give up guns, tanks, airplanes, cannon and weapons of steel for a
reversion to implements of wood?” (p. 49). Future “warring nations will have to
turn to weapons of wood—or rubber!” to nullify the effects of a type of weapon
that only reacts to metal. Harry Rimmer, The Coming War and the Rise of Russia
(Grand Rapids, MI; Eerdmans, 1940), p. 50. Maybe Howard Hughes's “Spruce
Goose” is a fultillment of Bible prophecy!

28. Hal Lindsey, There’s a New World Coming: ‘A Prophetic Odyssey” (New York:
Bantam Books, [1973] 1984), p. 121.

    
