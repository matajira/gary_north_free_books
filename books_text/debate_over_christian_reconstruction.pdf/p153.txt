Tommy Ice: A Response— Part Hf 135

fall from heaven? Now, with God all things are possible. There is
the possibility of a solar eclipse, but I do not believe that text man-
dates such an interpretation. The dispute over what this passage
means does not center on God’s ability to cause these things to
happen. Rather, the issue is, What does Jesus mean by the use of
this type of language? Again, the Bible is our guide. Keep in mind
that all these events, including those in verse 29, are to happen in
the generation prior to A.D. 70: “This generation will not pass
away until aif these things take place” (v. 34).

The Old Testament is filled with lunar and stellar language
depicting great political and social upheavals. In fact, Jesus quotes
Isaiah 13:10; 24:23; Ezekiel 32:7; Amos 5:20; 8:9, Zephaniah 1:15,
and He has in mind many more verses that use language that
describes the darkening of the sun and the moon, the rolling up of
the heavens like a scroll, and the falling of heavenly bodies.** In
each case, these verses describe a judgment upon contemporary
nations: Babylon (Isaiah 13:10), Egypt (Ezekiel 32:7), Israel
(Amos 5:20; 8:9), Judah (Zephaniah 1:15), The judgment of na-
tions is like the “shaking of the heavens and the earth,” since when
governments undergo judgment (invasions from other nations),
the entire world order is in upheaval (Haggai 2:6, 21; cf. Acts
17:6: “These men who have upset the world have come here also.”).
Haggai says that this shaking will happen in “a little while” (2:6).
If this is a prediction of something that’s to occur 2500 years in the
future, then “a little while” cannot be taken literally. But verse 21
makes it clear that the judgment is to happen during or soon after
the reign of “Zerubbabel, governor of Judah,” and the shaking of
“the heavens and the earth” refers to the judgment of kingdoms
and nations.

How do we know that this is the correct interpretation? The
text immediately moves to kingdoms: “And I will overthrow the
thrones of kingdoms and destroy the power of the kingdoms of the nations.”
But couldn’t this be a description of the “Great Tribulation” pre-

26. Symbolic language and Israel: Ecclesiastes 12:1-2; Amos 5:18-20; 8:2-9;
Zephaniah 1:4, 15; Jeremiah 4:23; Joel 2:28-32; Symbolic language and the nations:
Ezekiel 30; 32:7-15; Isaiah 34:4-10; 13:9-20.
