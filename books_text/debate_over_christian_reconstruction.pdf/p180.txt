162 The Debate over Christian Reconstruction

should not be stopped from recruiting young boys for the “homo-
sexual lifestyle”?3° Should Christians ignore dominion in these
areas? Hopefully Dave Hunt would say no. Pm sure he would say
no. This is dominion.

Hunt continues by stating that Christian Reconstructionists
want to “set up kingdoms and governments.” Again, Hunt offers
no proof for this charge. What does it mean? First, Reconstruc-
tions (and a lot of other Christians) believe that we are already liv-
ing in God’s kingdom. Jesus tells us that God’s kingdom has come
upon us (Luke it:20). Scripture informs us that God “delivered us
from the domain of darkness, and transferred us to the kingdom
of His beloved Son” (Colossians 1:13). So then, we are not working
to “set up kingdoms.” Second, civil governments are presently in
existence. Reconstructionists, as well as all Christians, should be
working—“ministering as priests the gospel of God”? (Romans
15:16; cf. 1 Peter 2:9; Revelation 1:6)—to help “ministers of God”
in the area of civil government (Romans 13:4; cf. Deuteronomy
17:18-20), rule in terms of God’s law so they can promote “good
behavior” (Romans 13:3), bring “wrath upon the one who prac-
tices evil” (v. 5), and make conditions favorable for the preaching
of the gospel (1 Timothy 2:2-4).

Dave Hunt also has a problem with “taking dominion over
cultures.” Would he rather leave dominion to the humanists?
Hitler had “dominion” in Germany and terminated the lives of
millions of people. The same is true of Mao Tse-tung in Red
China and Josef Stalin in Russia.

Dominion in the sense of exercising authority is inevitable and
inescapable. As was noted above, there must be some domination
of other men to restrain their evil. Paul tells individuals to “leave
room for the wrath of God” (Romans 12:19). As individuals, we
are not to “dominate other men” (Matthew 5:38-42; cf. Exodus
21:22-24), nor are we to use the State as a means to push a social
agenda. But God has not left evil without some checks in this
world. The civil magistrate is ordained by God to exercise His

30. Frank York, “Does Your Public School Promote Homosexuatity?,” Focus
on the Family: Citizen ( June 1988), pp. 6-8.
