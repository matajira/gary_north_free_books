The Place of Israel in Historic Postmillennialism 251

Declaration (1658) included the conversion of the Jews in its sum-
mary of the Church’s future hope:

We expect that in the latter days, Antichrist being destroyed, the
Jews called, and the adversaries of the kingdom of his dear Son
broken, the churches of Christ being enlarged and edified
through a free and plentiful communication of light and grace,
shall enjoy in this world a more quiet, peaceful, and glorious con-
dition than they have enjoyed.#

Clearly, the conversion of the Jews was part of a postmillennial
view of prophecy.

Prayer for Israel’s Conversion

Because they believed that the Jews would be converted, Puri-
tan and Presbyterian churches earnestly prayed that Paul’s proph-
ecies would be fulfilled, Murray notes that “A number of years be-
fore {the Larger Catechism and Westminster Directory for Public
Worship] were drawn up, the call for prayer for the conversion of
the Jews and for the success of the gospel throughout the world
was already a feature of Puritan congregations.”!9 Also, among
Scottish Presbyterian churches during this period, special days of
prayer were set aside partly in order that “the promised conver-
sion of [God’s] ancient people of the Jews may be hastened,”2°

In 1679, Scottish minister Walter Smith drew up some guide-
lines for prayer meetings:

As it is the undoubted duty of all to pray for the coming of
Christ's kingdom, so all that love our Lord Jesus Christ in sincer-
ity, and know what it is to bow a knee in good earnest, will long
and pray for the out-making of the gospel-promises to his Church
in the latter days, that King Christ would go out upon the white
horse of the gospel, conquering and to conquer, and make a con-
quest of the travail of his soul, that it may be sounded that the

18. Quoted in DeJong, As the Waters Cover the Sea, p. 38.
19. Murray, Puritan Hope, p. 99.
20. Quoted in ibid., p. 100.
