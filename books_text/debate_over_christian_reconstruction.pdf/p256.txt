238 The Debate over Christian Reconstruction

of Joel]; even to the end there will be war [the Jewish War of
66-70 a.p.]; desolations are determined.

27a. And He [Messiah the Prince] will confirm a covenant
[by fulfilling the Old Covenant as the New Covenant] with the
many [the church] during one week [the 70th week]. But in the
middle of the week He will put a stop to sacrifice and grain offering
(by dying on the cross, and thereby ending the sacrificial system],

Now we come to the statement that “on the wing of detestable
things, or abominations, comes one who makes desolate, even un-
til a complete destruction, one that is decreed, is poured out on
the one who makes desolate” (v. 27b). In the past, I have taken the
wing as a reference to the eagle, and thus to invading armies
(e.g., Is. 10:5, 12, 24-27; Hab, 1:8). Rome’s imperial standard was
the eagle, but the Bible also symbolizes Edom with the eagle (Jer.
49:16; Obad. 4; Dan. 11:41). The Romans and Idumeans together
managed to destroy the Temple. The Idumeans (Edomites) in-
vaded the Temple and filled it with human blood. The Romans
sacked it. I understood the last phrases of the verse to be saying
thac in time the Romans would also be destroyed.

There is a problem with this view. These who ignore the Idu-
mean invasion of the Temple cannot deal with Jesus’ statement in
Matthew 24 that the abomination of desolation stood in the holy
place. Luke’s parallel statement that Jerusalem would be sur-
rounded by armies (actually a reference to the Idumean-Zealot
conspiracy that let the Edomites into the Temple) is not cquiva-
lent: surrounding Jerusalem is not the same as standing in the
Temple. Only the Idumeans stood in the Temple.

But is this enough? The other passages in Daniel to which
Jesus alludes indicate that counterfeit worship was set up in the
Temple, and that this was the abomination of desolation.
Prophesying of Antiochus Epiphanes, Gabriel (?) tells Daniel that
he will “desecrate the sanctuary fortress, and do away with the
regular sacrifice, and they will set up the abomination of desola-
tion” (Daniel 9:31; 1 Maccabees 1:41-61). At the end of Daniel, the
preincarnate Christ (?) tells him that “from the time that the regu-
Jar sacrifice is abolished and the abomination of desolation is set
