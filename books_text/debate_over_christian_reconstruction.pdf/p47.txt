The Neutrality Myth 29

A law system that is formulated on the basis of the “principles
of reason common to all men” fails to account for man’s fallen
nature, especially the noetic effects of sin, that is, sin as it affects
the mind. Fallen man “suppresses the truth in unrighteousness”
(Romans 1:18). Are we to trust fallen man with determining what
“natural laws” we are to follow? This is the height of subjectivism.

There are two areas where the “principles of reason” cannot
match biblical laws. First, the Bible has them all written in one
place. The “laws of nature” must be hunted down by finite, falli-
ble, and fallen creatures. While it is true that these same finite,
fallible, and fallen creatures must interpret the Bible, at least the
hunting process is taken care of. The laws are there for ail to see.
Second, the “principles of reason” are not specific enough. The
Bible is a detailed ethical blueprint.

The Bible or the Bayonet

When the world is crying out for answers, why is it that some
leaders in the Christian community are saying the Bible is a book
that was only designed to show you how to get to heaven? We're
often told that the Bible is not a blueprint for life beyond the fam-
ily and church. There is tyranny in the Soviet Union, Cuba, El
Salvador, South Africa, Chile, and numerous other countries
around the world. They all have two things in common: First,
Jesus Christ is not seen as the only mediator between God and re-
bellious sinners. In fact, the problems of tyranny and oppression
are not seen as fundamentally religious. Rather, it is believed that
reconciliation must come between man and man without any
need of Jesus’ redemptive work.

Second, the Bible is rejected as a blueprint for living. The
Bible is ridiculed. Even many Christians do not take the Bible ser-
iously. They reject its solutions for the homeless, the rising in-
cidence of sodomy and AIDS, the poor, teenage promiscuity, for-
eign affairs, and the threat of a nuclear holocaust. It’s no wonder
that tyranny is replacing freedom around the world.

On May 28th, 1849, Robert C. Winthrop addressed the An-
nual Meeting of the Massachusetts Bible Society in Boston. His
words are no less true today:
