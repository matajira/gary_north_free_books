5
THE NEUTRALITY MYTH

One Christian writer maintains that “Government is not
based on special revelation, such as the Bible. It is based on God’s
general revelation to all men.”! Nations, whether Christian or
non-Christian, establish governments. Does this mean that na-
tions are free to establish the standard by which they will rule?
What are the limits of power? How much tax should be collected?
Should the State control education? Is homosexuality a crime? If
it is, what should the punishment be if two men are caught in the
act, are tried, and are found to be guilty? Is bestiality wrong?
How about abortion? It’s convenient to say that “government is
not based on special revelation,” but it is not much help when you
must deal in particulars. General revelation does not give answers
to specific ethical dilemmas.

But what if the Bible is available to a nation as an ethical stan-
dard for civil legislation? Is it inappropriate to use it as a blueprint
for governance? Should those who rule trust the fallen “light of
reason” or the Word of God that “is a lamp to my feet, and a Jight
to my path”? (Psalm 119:105). The assumption of those who
choose the “light of reason” over “special revelation” is that man’s
sense of justice is greater than God’s revealed will relating to jus-
tice. Israel’s obedience to the law was to be an enticement to fol-
low its directives: “So keep and do them, for that is your wisdom
and your understanding in the sight of the peoples who will hear

1, Norman L. Geisler, ‘A Premillennial View of Law and Government,” The
Best in Theology, gen. ed., J. I. Packer (Carol Stream, UL: Christianity Today/
Word, 1986), vol. 1, p. 259.

27
