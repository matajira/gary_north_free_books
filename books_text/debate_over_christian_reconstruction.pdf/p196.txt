178 The Debate over Christian Reconstruction

3. Manifest Sons of God, based on the teachings of Franklin
Hall’s teachings concerning immortalization, taught that the
“manifestation of the sons of God” spoken of in Romans 8:19 was
to occur as a result of the final shower of the Latter Rain just prior
to Christ’s return, These “sons of God” would be drawn from a
remnant of the church, and would be individual extensions of the
Incarnation or replicas of Christ, who was regarded as the “Pat-
tern Son.” Do Reconstructionist believe this? No! Does Gentry’s
article even suggest this outrageous teaching? No!

Dave Hunt probably objects to strains of dominion in their
theology. But there are strains of premillennialism in numerous
cults, including the Manifest Sons of God. Should we then con-
clude that premillennialism is deviant? As has been shown repeat-
edly, dominion has been a part of the church for centuries. This
has been pointed out in The Reduction of Christianity. Even some
dispensationalists support the dominion mandate, defining it in
terms similar to that of Reconstructionists.

Conclusion

Dave Hunt has majored on the minors of theology. He has
then taken these minor doctrinal differences and has made them
tests of the orthodox faith. Further, he leads his audiences astray
by rarely defining terms or quoting Reconstructionists in context
or even interacting with the detailed analysis of Christian Recon-
structionist beliefs set forth in The Reduction of Christianity. Dave
Hunt, therefore, is unreliable as a critic of Christian Reconstruction.

60. One critic of the excesses of the Manifest Sons of God theology laments
the fact that some were teaching that they would “call the shots” in the tribula-
tion, a distinctive of premillennialism. George Hawtin, “Mystery Babylon,” The
Page (Battleford, Sask.: n.d.), twelfth printing, p. 10, Quoted in Richard Michael
Riss, “Latter Rain Movement of 1948,” p. 135.
