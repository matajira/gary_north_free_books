Other books by Gary DeMar

God and Government:
A Biblical and Historical Study, 1982

God and Government;
Issues in Biblical Perspective, 1984

God and Government:
The Restoration of the Republic, 1986

Ruler of the Nations:
Biblical Blueprinis for Government, 1987

The Reduction of Christianity:
A Biblical Response to Dave Hunt, 1988
(with Peter J. Leithart)

Surviving College Successfully: A Complete
Manual for the Rigors of Academic Combat, 1988

Something Greater is Heres Christian
Reconstruction in Biblical Perspective, 1988
