Tommy Ice: A Response — Part IT 127

a rebuilt temple. The temple of God in the New Testament writ-
ings is quite obviously the Church of Christ or Jesus Himself (Jolin
2:19-21). The Old Testament passages that mention a rebuilt tem-
ple were fulfilled in the post-exilic period and in the first coming of
Christ. Those verses that mention a future temple, specifically
Ezekiel 40-48, have reference to the church.!§ We, the church, are
living stones (1 Peter 2:5) being joined together in a living temple
(Ephesians 2:19-21; 1 Corinthians 3:16; 6:19; 2 Corinthians 6:16).

Fourth, a variety of interpretations has been given on the correct
meaning of what or who the abomination of desolation/the man of
sin is. Tommy Ice prejudices his audience against Christian Recon-
struction by implying that those who have written on the subject,
and assert that the abomination of desolation is the invading
Roman armies or an Edomite rebellion,”” are somehow out of ac-
cord with the history of interpretation of this passage. Yet, Des-
mond Ford in The Abomination of Desolation in Biblical Eschatology
lists six possible interpretations, all held by Bible-believing Chris-
tians throughout church history: The statue of Titus erected on
the side of the desolated temple; statues erected by Pilate and
Hadrian; the atrocities of the Zealots; Caligula’s attempted profa-
nation; the Antichrist as the abomination of desolation; the abom-
ination of desolation as the invading Roman armies.

16. William Hendriksen, Israel in Prophecy (Grand Rapids, MI: Baker Book
House, 1968). Many expositors believe, however, that Ezekiel’s temple was a vi-
sionary expression of the post-exilic community and its temple, first of all, and
only by extension a picture of the New Covenant. This position is maintained by
James B. Jordan, Through New Eyes (Nashville: Wolgemuth and Hyatt, 1988),
chapter 17. In defense of this Jordan cites Matthew Henry, and especially E. W.
Hengstenberg, The Prophecies of Ezekiel (Minneapotis: James Reprints, [1869]).
“With the exception of the Messianic section in ch, 47:1-12, the fulfilment of all
the rest of the prophecy belongs to the times immediately after the return from
the Chaldean exile. So must every one of its first hearers and readers have under-
stood it.” Hengstenberg, p. 348.

17. Chilton, The Great Tribulation, pp. 11-13.

18. (Washington, D.C: University Press of America, 1979), pp. 158-168. Ford,
who is not a Reconsiructionist, believes the abomination of desolation is the in-
vading Roman armies: “This viewpoint gives weight to both profanation and
devastation, and certainly the Roman invasion brought both. This understand-
ing, and this understanding alone, rings true to the demands of the literary, philologi-
cal, and historical evidence of Mk. 13.” (p, 169).
