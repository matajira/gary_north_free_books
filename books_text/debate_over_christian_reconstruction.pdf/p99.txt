Putting Eschatology into Perspective 81

T want to make it clear that I do not believe that dispensationalism
is a cult. But a preoccupation with an end times scenario is cultish
when it leads the church to establish timetables that assure us as
to the timing of the Lord’s return and when it turns the church
into 4 retreatist institution.

The Test of Orthodoxy

In order to be considered an “orthodox Christian,” one has to
believe in verbal inspiration, the deity of Christ, the substitutionary
atonement, the bodily resurrection, and other doctrines of the
biblical faith. But when we come to the millennial issue, we learn
that Christians are categorized as holding four views of the nature
and timing of the millennium: amillennialism, premillennialism,‘®
dispensational premillennialism, and postmillennialism.

In the debate, Tommy Ice and Dave Hunt focused their atten-
tion on eschatology. For them, dispensational premillennialism is
the litrnus test for establishing the bounds of orthodoxy. This is one
of dispensationalism’s major flaws.!? But the dispensationalist is

16. There is a great amount of disagreement within the premillennial camp
over the timing of the rapture, an eschatological distinctive that separates historic
premillennialists from dispensationalists (for the most part), Does the “rapture”
occur before (pre), during (mid), or after (post) the “Great Tribulation”? See
Gleason Archer, Jr., Paul D. Feinberg, Douglas J. Moo, and Richard R. Reiter,
The Rapture: Pre, Mid-, or Post-Tributational? (Grand Rapids, MI: Zondervan/
Academie Books, 1984). If “moderation” and “a call for unity that allows for di-
versity and promotes toleration” on the timing of the rapture is promoted among
premillennialists, then why can’t the church do the same with millennial posi-
tions in general? (p. 44).

17. George Eldon Ladd, an historic premillennialist, in a response to Herman
Hoyt's article on dispensationalism, makes the following observation:

Hoyt’s essay reflects the major problem in the discussion of the mil-
lennium, Several times he contrasts nondispensational views with his
own, which he labels “the biblical view” (pp. 69-70, 84). If he is correct,
then the other views, including my own, are “unbiblical’ or even
heretical. This is the reason that over the years there has been little crea-
tive dialogue between dispensationalists and other schools of prophetic
interpretation. George Eldon Ladd, “An Historic Premillennial Response,”
The Meaning of the Millennium: Four Views, ed. Robert G, Glouse (Down-
ers Grove, IL: InterVarsity Press, 1977), p. 93.
