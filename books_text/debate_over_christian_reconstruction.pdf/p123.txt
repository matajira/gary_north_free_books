Tommy Tce: A Response — Part I 105

Reformed Faith.” Their attack, however, was not on historic or
classical premillennialism.

Dispensational Beginnings

Tommy Ice attributes the preterist view of eschatology to a
Jesuit Catholic by the name of Lacunza who Ice says originated it
in 1614. Supposedly, Lacunza fabricated the preterist interpretation
of the Book of Revelation to prove that the Pope was not the anti-
christ. On this point, I believe, Tommy Ice is confused. (He must
have meant Daniel Whitby [1638-1726].) Manuel De Lacunza
(1731-1801), writing under the penname Rabbi Ben-Ezra—to give
the illusion that he was a converted Jew—wrote The Coming of the
Messiah in Glory and Majesty. It was published in 1812, eleven years
after his death. A complete Spanish edition was published in
London in 1816.

Now, if this is the same Lacunza mentioned by Ice in the
debate, then his ties are with dispensational premillennialism and
not postmillennialism. Edward Irving translated Lacunza’s work
into English and had it published in 1827 with an added “prelimi-

nary discourse of two hundred pages. . . . The prefatory material
supplied by Irving contends for the premillennial advent with
great persuasiveness. . . .”50

The prophetic speculation of Dave Hunt and modern dispen-
sationalism can be found in the writings of Edward Irving, who
was influenced by Lacunza, over 150 years ago. Irving believed
that the end of the world was near, that the church was in apos-
tasy, and that Jesus would return soon. He published a small
prophetic work with the title Babylon and Infidelity Foredoomed.
“Babylon was his term for all Christendom, and he declared that
because of its infidelity it was doomed, judgment was soon to fall,
and the coming of Christ was very near.”5!

49. Edwin H. Rian, The Presbyierian Conflict (Grand Rapids, MI: Eerdmans,
1940), pp. 235-36.

50. Murray, Puritan Hope, pp. 189, 190.

51, Arnold Dailimore, Forerunner of the Charismatic Movement: The Life Of Edward
Irving (Chicago, 1L: Moody Press, 1983), p. 76.
