190 The Debate over Christian Reconstruction

world.” He described these beliefs to be based upon “mere legends
and myths.” Shinto priests were still permitted to practice their re-
ligion “so long as church and state were separated.” This is how
MacArthur sums up Japan’s transformation:

Whenever possible, I told visiting Christian ministers of the
need for their work in Japan. “The more missionaries we can
bring out here, and the more occupation troops we can send
home, the better.” The Pocket Testament League, at my request,
distributed 10,000,000 Bibles translated into Japanese, Gradually,
a spiritual regeneration in Japan began to grow.'?

So then, it was the rejection of Shintoism and Buddhism and the
adoption of a Western worldview that was at least vaguely Chris-
tian that has made Japan what it is today.

Dave Hunt continues by denying that there is any progress in
history. This is an astounding admission. Let me quote him in full:

DeMar says Hunt has no philosophy of historical progress
rooted in the sovereign operation of the Spirit of God. No, I don’t
have a biblical basis for a philosophy of gradual progress. There’s
not one example in the Bible. There’s not one example in history.
They give examples like the cessation of slavery in England. But,
look at England today. They are not selling literal slaves. They're
selling slaves to Satan—the souls of men to Satan, Lock at the
church today. Look at Holland where Abraham Kuyper was and
so forth. But, take a look at it today. He did tremendous things
there, but look at it today. We have been going— we have had ups
and downs—but we have been going down in history.

Hunt claims that there’s not one example of gradual progress in
the Bible. What does he think of the advance of God’s kingdom as
it’s depicted in Isaiah 9, 11; Daniel 2, 7 and the “growth parables”
in Matthew 13? Isaiah says that at the time when “a child will be
born to us” there will be “no end to the increase of His government

12. Alll of these quotations are taken from Douglas MacArthur, Reminiscences
(New York: McGraw-Hill, 1964), pp. 310-11.
