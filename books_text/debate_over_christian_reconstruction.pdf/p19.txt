INTRODUCTION

Controversy for controversy’s sake is sin, but controversy for
truth’s sake is biblical and vital to the church.

The debate over Christian Reconstruction held in Dallas,
Texas, on April 14, 1988, between Tommy Ice and Dave Hunt
(representing dispensational premillennialism) and Gary North
and Gary DeMar (representing Christian Reconstruction) was
historic. This public debate pitted dispensational? premillennial-
ists, representing a recent school of biblical interpretation, against
Christian Reconstructionists, fully in the tradition of the historic
Protestant faith.

Who won? You must decide.

1. Walter Martin, noted cult expert and author of Kingdom of the Cults, quoted.
in Christian Research Journal (May 1988), p. 3.

2. For evaluations and critiques of dispensationalism see the following books:
Millard J. Erickson, Contemporary Options in Eschatology: A Study of the Millennium
(Grand Rapids, MI: Baker Book House, 1977), pp. 109-181; Robert G. Clouse,
ed. The Meaning of the Millennium: Four Views (Downers Grove, IL: InterVarsity
Press, 1977); Oswald T. Allis, Prophecy and the Church: An Examination of the Claim of
Dispensationalists that the Christian Church is a Mystery Paventhesis which Interrupts the
Fulfillment to Israel of the Kingdom Prophecies of the Old Testament (Philadelphia, PA:
Presbyterian and Reformed, 1945); Vern 8. Poythress, Understanding Dispensation-
alists (Grand Rapids, M1: Zondervan/Academie Books, 1987); Clarence B. Bass,
Backgrounds to Dispensationalism: Its Historical Genesis and Ecclesiastical Implications
(Grand Rapids, MI: Baker Book House, [1960] 1977); John H. Gerstner, A
Primer on Dispensationalism (Phillipsburg, NJ: Presbyterian and Reformed, 1982);
William E. Gox, Why I Left Scofieldism (Phillipsburg, NJ: Presbyterian and
Reformed, n.d.); An Examination of Dispensationalism (Philadelphia, PA: Presby-
terian and Reformed, 1963). For a critique of dispensationalism by two former
dispensationalists who attended Dallas Theological Seminary, see Curtis I.
Crenshaw and Grover E. Gunn, II], Dispensationalism Today, Yesterday, and Tomor-
row (Memphis, TN: Footstool Publications, 1985).

1
