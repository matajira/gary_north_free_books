4
HEAVENLY AND EARTHLY REWARDS

When we obey God’s commandments, He will bless us. This
was true under the Old Covenant, as Deuteronomy 28 and Levit-
icus 26 show. Obedience to God brings blessing, and disobedience
to God brings curses. Ultimately, of course, none of us can obey
God perfectly, We receive God's blessing only because Jesus
Christ has obeyed perfectly and shares His blessings with us. But
even in the New Testament, we are taught that God blesses His
church as it is faithful to Him.

Paul encouraged children to obey parents with the promise
that “you shall live long on the earth” (Ephesians 6:3). Elsewhere,
Paul told the Corinthians that in Christ they possess not only
“things to come,” but “the world” and “things present” (1 Corinthi-
ans 3:22). As the true children of Abraham, we are with him heirs
of the world (Romans 4:13).

Jesus promised that those who seek His kingdom above all will
receive all the earthly things that they need (Matthew 6:33), and
claimed that the meek would inherit the earth (Matthew 5:5). On
the other hand, He threatens to punish rebels against Him with
earthly punishments (Psalm 2:10-12). Putting these two con-
siderations together, we conclude that, in general, God’s people
receive earthly blessing and success, and the unrighteous receive
earthly judgment.

Several qualifications are needed to avoid a misunderstanding
of what has just been said. First, this does not mean that every in-
dividual Christian will be successful in his lifetime, or that spiri-
tuality is measured in financial or material terms, But faithful

23
