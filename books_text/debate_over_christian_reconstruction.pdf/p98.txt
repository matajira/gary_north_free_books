80 The Debate over Christian Reconstruction

lators continue to use these prophecies to lead more and more
people into inaction. Some have taken the end-times scenario
altogether too seriously and sold their possessions. There is often
a “cultish” feature among those obsessed with the rapture and the
supposed threatening end of the world. This is not to minimize
the evil and hardships we are facing. Christians should seek solu-
tions, not a timetable for lift-off.

Some cults entice prospective members by spinning a scenario
that includes the approaching end of the world and the return of
Christ in judgment. The Jehovah's Witnesses have made this a part
of their “evangelistic” strategy since 1914. Converts are attracted to
cults that maintain that Jesus is coming back on a certain day, and
that by joining with the bearers of the only true religion, they can
avoid the impending judgment.'* Here’s an extreme example:

The group known as “The Lighthouse Gospel Tract Founda-
tion,” led by Bill Maupin, was located in Tucson. He originally
calculated that the Rapture would take place on June 28, 1981, Some
members of the group quit their jobs and/or sold their houses. When
that date passed, Maupin said that he had miscalculated by forty
days, and predicted that the Rapture would take place on August
7, 1981. The Return of Christ is to occur May 14, 1988. Maupin
calculated his dates on the basis of Daniel’s seventy “weeks,” and
the founding of the State of Israel on May 15, 1948.15

14, In the July 1988 issue of Charisma magazine, an advertisement appeared
with this headline: “8 Reasons Why the Rapture Could ‘ake Place in the 3-day
Period from September 11-13, 1988.” ft goes on to say: “At no time in the past or
fature will the Bible dates of Daniel, Ezekiel and Revelation fit except 1988-1995.”
15. William Sanford LaSor, The Truth About Armageddon: What the Bible Says
About the End Times (Grand Rapids, MI: Baker Book House, 1982), p. 103, note a.
Parents are constantly fighting this cultish dimension of eschatology. A group
of parents from Delaware County, Pennsylvania, have watched their children
move to South Carolina at the invitation of a self-styled evangelist in anticipation
of “the end.”

They suy the children, most in their 20s, have “been brainwashed
inta believing that the end of the world is at hand and are selling their
possessions, often at cut-rate prices, to move to the evangelist’s Sauth
Carolina farm.” “Pa. Parents Fear Children Lost ta Cult,” The Atlanta
Journal/Constitution (April 24, 1988), p. 10-A.

For a balanced treatment of what Christians should expect in terms of the ‘end times,”

the reader is encouraged to study DeMar and Leithart, Reduction of Christianity.
