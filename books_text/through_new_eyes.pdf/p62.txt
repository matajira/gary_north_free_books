50 ‘TuHrouch New Eyes

visionary Temple (Ezekiel 40-48) was more glorious than Solo-
mon’s Temple. The New Jerusalem-is more glorious yet. The
study of how each of these models is transformed into the next,
and the parallels between them, is part of typology.

Because al] men are made in the image of God, all men bear
His imprint. Every man is, thus, in one sense a type of every
other man. More importantly, church leaders arc to be types or
models for kingdom citizens (Philippians 1:7; 1 Thessalonians
1:7; 1 Timothy 4:12; Titus 2:7; 1 Peter 5:3). In terms of a typo-
logical view of history, the kingdom of men in the Old Covenant
was a type of the New Covenant (1 Corinthians 10:6, 11), and the
first Adam was a type of the Last (Romans 5:14).

A great deal of nonsense has been published under the banner
of typology; but in spite of this, the fact remains that typology is
the fundamental Biblical philosophy of history.!? Typology
means that history is under God’s control, not man’s. It means
that the successive stages of world history have meaning, a
meaning related to the heavenly pattern and God’s purpose to
glorify man and the world progressively.

In an important study, Jean Daniélou has shown that the
early Church Fathers regarded typology as central to their
understanding of the Scriptures. It enabled them to answer both
their Jewish and their Gnostic critics. Against the Jews, typology
showed the superiority of the New Covenant over the old;
against the Gnostics, typology showed that the Old and New
Covenants both revealed the same truths.13 The symbolic and
typological approach of the Church Fathers is often confused
with allegory, but Daniélou shows conclusively that the Fathers
were well aware of the difference. The Fathers did indeed use the
Bible allegorically to express what they intended to be a Chris-
tian philosophy, but

this trend, strictly philosophical, is something quite different
from typology. It goes back to Philo.‘In his Treatise on Paradise,
Ambrose, who was much influenced by Philo, writes as fol-
lows: “Philo confined his attention to the moral sense, because
his Judaic outlook prevented him from a more spiritual under-
standing” (IV, 25; C.S.E.L. 281, 21). Spiritual here denotes the
Christological or typological sense, while moral implies philoso-
