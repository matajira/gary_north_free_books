The New World 261

Jesus had accomplished His work, the law was changed, for
“when the priesthood is changed, of necessity there takes place
a change of law also” (Hebrews 7:12). The old heavens and
earth passed away in the first century a.p.; and at that time,
many of the jots and tittles also passed away, their purpose ful-
filled at last.3

‘Whoever then annuls one of the least of these commandments,
and so teaches others, shall be called least in the kingdom of
heaven; but whoever does them and teaches them, he shall be
called great in the kingdom of heaven (v. 19).

The idea here is this: Anyone who presently ignores the Old
Testament Law will find disgrace and condemnation in the new
kingdom that will come; but anyone who scrupulously keeps all
the Old Testament Law at the present time, will be great in the
kingdom when it comes.

The Pharisees in their teaching were setting aside the com-
mandment of God in order to keep their traditions (Mark 7:9).
Such men were annulling some of the commandments, and they
would find condemnation. Thus, Jesus concludes by saying:

For I say to you, that unless your righteousness surpasses that
of the scribes and Pharisees, you shall not enter the kingdom of
heaven (v. 20).

We are used to thinking of the scribes-and Pharisees as
meticulous men who carefully observed the jots and tittles. This
is not the: portrait found in the Gospels. The scribes and
Pharisees that Jesus encountered were grossly, obviously, and
flagrantly breaking thé Mosaic law, while keeping all kinds of
man-made traditions. Jesus’ condemnation of them in Matthew
23 certainly makes this clear, as does a famous story in John 8.
There we read that the scribes and Pharisees brought to Jesus a
woman taken “in the very act” of adultery (John 8:1-11). How did
they know where to find her? Where was the man who was
caught with her? Apparently he was one of their cronies. Also,
when Jesus asked for anyone “without sin” (that is, not guilty of
the same crime) to cast the first stone, they all went away, be-
cause they were all adulterers.
