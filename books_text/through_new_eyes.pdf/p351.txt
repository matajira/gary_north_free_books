AUTHOR INDEX

 

Addison, Joseph, 52

Albright, W, F., 302
Alexander, Cecil Frances, 94
Allen, R. H., 61, 295

Ambrose of Milan, 50, 51
Augustine of Hippo, 10, 22, 291

Bahnsen, Greg L., 305, 312, 313

Baltzer, Klaus, 299

Barnouin, M., 58-59, 294

Basil the Great, 10, 291

Bavinck, Herman, 22-25, 32, 108, 292,
295, 298, 315

Berkhof, Louis, 109, 293, 298, 315

Berman, Harold J., 313, 315

Boettner, Loraine, 313

Boorstin, Daniel J., 301

Brenner, Athalya, 296

Brinsmead, Robert D., 315

Brown, John, 3i1

Bullinger, Ethelbert W., 295

Galvin, John, tif, 112, 298, 309
Campbell, Roderick, 14, 292, 316
Cansdale, George S., 297
Carmichael, Calurn M., 297

Carr, G. Lloyd, 309, 316

Cassuto, Umberto, 303, 304, 312, 316
Chadboume, Robert L., 296
Chilton, David, 292-313 passim, 316
Clarke, Adam, 310

Clowney, Edmund P., 316
Cornwall, Judson, 316

Corsini, Eugenio, 313

 

Daniélou, Jean, 50, 292, 294, 300, 303,
316

Darr, Katheryn P., 310

Daube, David, 304

Davidson, Richard M., 292, 294, 306, 316

De Graaf, 8. G., 294, 316
De Jong, J. A., 313
DeMar, Gary, 299, 313
Dillow, Joseph C., 303
Dix, Gregory, 130, 299, 316
Donne, Joha, 116

Edersheim, Allred, 317
Eliade, Mircea, 302

Fairbairn, Patrick, 294, 301, 306, 317
Farrer, Austin, 295, 317

Feinberg, Charles Lee, 302

Filson, Floyd V., 301

Fortunatus, Venantius, 80

Foulkes, Francis, 294

Frame, John M., 22, 292, 293, 299, 317
France, R, T., 294

 

Gaffin, Richard B., Jr., 312
Goldman, Bernard, 295, 406
Grant, George, 299
Greydanus, §., 312

Hachlili, Rachel, 295

Haran, Menahem, 298, 306, 317
Hengstenberg, F. W., 302, 310
Henry,. Matthew, 310

Herbert, George, 142

Heschel, Abraham J., 138, 140, 300
Hough, R, E., 294, 317

Jaki, Stanley, 291, 295, 317

‘Jordan, James B., 291-313 passim,
317-318, 319

Josephus, 295

Keel, Othmar, 292, 318
Keil, C. F., 302
Kik, J. Marcellus, 295, 318

339
