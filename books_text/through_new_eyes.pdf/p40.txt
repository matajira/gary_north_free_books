 

Unto this catholick visible church Christ hath given the
tninistry, oracles, and ordinances of God, for the gather-
ing and perfecting of the saints in this life, to the end of
the world; and doth by his presence and Spirit, accord-
ing to his promise, make them effectual thereunto.

The Westminster Confession of Faith (1643),
Chapter 25:2

 
