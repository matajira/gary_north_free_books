298 Noves To Pacgs 108-123

2, Herman Bavinck, Our Reasonable Faith, trans, Henry Zylstra (Grand Rapids:
Baker Book House, [1956] 1977), p. 178.

3. Auguste Lecerf, An Introduction to Reformed. Dogmatics, trans. André Schlemmer
(Grand Rapids: Baker Book House, [1949] 1981), p. 147.

4. Vern S. Poythress, Symphonic Theology: The Validity of Multiple Perspectives in
Theology (Grand Rapids: Zondervan, 1987), p. 106.

5. Louis Berkhot, Systematic Theology, 4th ed. (Grand Rapids: Eerdmans, 1949),
pp. 171-172. Berkhof sets out three errors to be avoided in connection with the
doctrine of concurrence: 1. That it consists merely in a general communication
of power, without determining the specific action in any way. 2. That it is of
such a nature that man does part of the work and God a part. 3. That the work
of God and that of the creature in concurrence are co-ordinate; rather, the work
of God always has priority, Berkhof discusses the mystery of the relationship
between Divine concurrence and sin. See pp. 172-175.

6. This implies that they did indeed cease during the Flood, which means that the
Flood year was an extended miraculous event, and casts some doubt on the
ability of creation scientists to account for its every detail in terms of uniformity
of causation,

7. John Calvin, Commentaries on the First Twenty Chapters of the Book of the Prophet
Ezekiel, trans, Thomas Myers (Grand Rapids: Baker Book House, [1849] 1979),
pp. 334-335. Italics added. Calvin's lectures on Ezekiel were his last work—he
died before finishing the book—and reflect his maturest thought.

8. These twenty-four elders are sometimes identified as the twenty-four chief
priests of the Temple service. In light of the progression of thought in Revela-
tion 4 and 5, however, I believe we have to see the twenty-four elders as the
angelic archetypes of the twenty-four chief priests on earth.

9, See Menahem Haran, Temples and Temple-Service in Ancient Israel (London:
Oxford University Press, 1978), p. 247.

10. On the Levite guards, see James B. Jordan, “The Death Penalty in the Mosaic
Law: Five Exploratory Essays” (available from Biblical Horizons, P.O. Box
132011, Tyler, TX 75713), chap. 3.

Chapter 10—Breaking Bread: The Rite of Transformation

1, Henry Van Til, The Galvinistic Concept of Gulture (Grand Rapids: Baker, 1959), p. 27.

2. The material on the “six-fold rite” in the remainder of this chapter was origi-
nally published in a slightly different form as part of James B. Jordan, “Chris-
tian Piety: Deformed and Reformed,” The Geneva Papers 2:1 (September 1985). It
is also found, again with some changes in cmphasis, in James B. Jordan,
Primeval Saints: Studies in the Patriarchs of Genesis, chaps. 1 and 2. Both are ayail-
able from Biblical Horizons, P.O. Box 132011, Tyler, TX 75713.

3. Biblical names are descriptive. Thus, Abraham means “Father of a Multitude,”
and Isaac basically means “Laughter.” When Adam named the animals, he did
not simply assign random sounds to each. Rather, he gave each an appropriate
descriptive name. He had the wisdom to interpret the world rightly, and to rec-
ognize the character God had placed in each creature.

4. This is an act of primordial oblation, offering God’s world to Him, apart from
human works. Itis, thus, an act of faith apart from works. It corresponds to the
sabbath as the first day of man’s week.

5. This is an act of eschatological oblation, offering God’s world to Him as trans-
formed by human works. It is, then, an act of faith that embraces human
works. It corresponds to the sabbath as the last day of the week,
