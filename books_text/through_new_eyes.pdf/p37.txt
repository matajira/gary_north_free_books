The Purpose of the World 25

Genesis 1:27 gives us both perspectives: “And God created man
in His own image, in the image of God He created him [in-
dividual}, male and female He created them [society].” For this
reason, there is a symbolic relationship between the parts of
the individual human person and the parts of the corporate
“body” politic.

For even as the body is one, and has-‘many members, and all
the members of the body, though they are many, are one body,
so also is Christ. For by one Spirit we were all baptized into one
body, whether Jews or Greeks, whether slaves or free, and we
were all made to drink of one Spirit. For the body is not one
member, but many. If the foot should say, “Because I am not a
hand; I am not of the body,” it is not for this reason any less of
the body. And if the ear should say, “Because I am not an eye, I
am not of the body,” it is not for this reason any. less of the body.
If the whole body were an eye, where would the hearing be? If
the whole were hearing, where would the sense of smell bc? But
now God has placed the members, each one of them, in the
body, just as He desired. . , . Now you are Christ’s body, and
individually members of it. And God has appointed in the
church, first apostles, second prophets, third teachers, then
miracles, then gifts of healing, helps, administrations, various
kinds of tongues . . . (1 Corinthians 12:12-28),

This social diversity also symbolizes various aspects of God’s
infinite person. Since Bavinck has done the homework for us, let
us quote him once more:

Furthermore, God is often called by names which indicate a
certain office, ‘profession, or relation among men. Hence, he is
called bridegroom, Isaiah 61:10; husband, Isaiah 54:5; father,
Deuteronomy 32:6; judge, king, lawgiver, Isaiah 33:22; man of
war, Exodus 15:3; hero, Psalm 78:65; Zephaniah 3:17; builder
(architect) and maker, Hebrews 11:10; husbandman, John 15:1;
shepherd, Psalm 23:1; physician, Exodus 15:26; etc.!?

The World Reveals Man
Bavinck states,

Whereas the universe is God's creation, it follows that it also re-
veals and manifests him, There is “not one atom of the uni-
