Man: The Agent of Transformation 137

Ezekiel 40 and following, where a man with a measuring rod
measures off the new Temple of God. For the most part, how-
ever, the Bible shows the priests guarding God’s ‘holy bound-
aries. They determined who was clean and who was unclean,
and thus who could approach God’s Tabernacle (Leviticus 11-15).
They challenged sinners who dared to transgress His threshold,
as Azariah confronted King Uzziah (2 Chronicles 26:16-23).4
Primarily, though, the priest guarded God's throne by leading
the people in worship. People who truly worship God will not
disobey Him. Thus, the systematic theologians are right to say
that the priest’s primary task, in one sense, was to offer sacrifice
to God by leading men to become living sacrifices.

Just as God brought animals to Adam to teach him about so-
ciety and his kingly task, so also He brought an animal to Adam
to teach him about holiness and his priestly task. The serpent or
dragon was the most. beautiful and wise of all the beasts of the
field. He was doubtless one of the “great monsters” created on
the fifth day. Indeed, the use of the word “create” in Genesis 1:21
points to'an especially wondrous work.5 With God’s permission
(cf. Job 1, 2), Satan used the dragon to challenge Adam and Eve.

The assault was directly against the woman. Since it was
Adam’s task to guard the Garden and all within it, he should
have guarded her. Instead he stood by and let her fall. (Genesis
3:6 says that he was “with her” during the discussion.) He failed
to guard the Garden, and admitted the enemy.

We noted above that the priest’s primary means of guarding
is through worship. What should Adam have done? He should
have led Eve away from the serpent to the Tree of Life.6 There
he would have led her in rendering obeisance to the Lord of Life,
admitting his own need for life from God. He would have taken
the fruit and given it to her (as Jesus, the New Adam, feeds His
Bride). In this way, Satan’s designs would have been thwarted.

Instead, however, Satan was given access to the Garden. By
receiving food from Satan, Adam and Eve acknowledged him
as their priest. They were disqualified from guarding the Gar-
den, and new cherubic guardians were set up in their stead
(Genesis 3:24).

During the Old Covenant, God set aside men to fulfill the
office of priest in a special way. These men led others to the door
