The World of the Temple 295

In Deuteronomy 17:16-17 Moses provided the people with
three laws of kingship. The king was not to multiply horses: or
gold, or take more than one wife. In 1 Kings 10:14—11:8 we find
Solomon breaking all three of these laws. First, Solomon took in
666 talents of gold per year (1 Kings 10:14).24 According to the
Open Bible, a talent of gold in 1985 money would be worth U.S.
$5.76 million.22 This means that Sclomon was taking in over
$3.84 billion each year. This was hefty revenue for a country
the size of New Jersey, Second, Solomon multiplied horses and
chariots (1 Kings 10:26). Adding a few horses and chariots for
the small professional army would not have been wrong, but
Solomon went overboard. Finally, of course, Solomon commit-
ted polygamy, and his wives turned his heart away from God
(1 Kings 11:1-8).

This disruption in the spiritual environment of the cove-
nant manifested itself right away in a rending of the social pol-
ity, as the ten northern tribes, with God’s blessing, seceded
from the nation (1 Kings 12). An equivalent disruption in the
symbolic cosmos occurred when Pharaoh removed the gold
from the Temple and Palace, and these were replaced -with
bronze (1 Kings 14:25-27). ,

These were only preliminary judgments, however. There
were good times as well as bad times during the Kingdom estab-
lishment. Good kings listened to God’s ambassadors, the proph-
ets, and being blessed by God, were able to restore much of the
gold to the Temple.

The kingdom of (Northern) Israel was separated from the
Temple in Jerusalem. The people were supposed to go into
Judah and worship God at the Temple, but then return to their
homes in the separate kingdom of Israel. Of course, the apos-
tate kings of Israel resisted this, and sometimes even closed the
borders to prevent the people from leaving the country to wor-
ship (2 Chronicles 16:1). Thus, for much of their history the
people in Israel were left only with synagogue worship.

On one occasion God provided them with proper sacrificial
worship. When Elijah challenged the prophets of Baal on Mount
Carmel—Carmel means “fruitful place’—he built an altar and
God honored it. God would not accept such non-Temple altars,
because He would not accept strange fire, fire that He Himself
