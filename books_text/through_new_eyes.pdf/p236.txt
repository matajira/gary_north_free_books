224 Turoucn New Eves

about as far away from the center of national life as you could
get. Moreover, their offense was bribe-taking, which was no-
where near as serious as the offense of Eli’s sons a generation ear-
her (1 Samuel 2:12-17, 22-25). In addition, ail the elders of Israel
needed to do was ask that the men be removed from office. In-
stead they demanded a king (1 Samuel 8).

They wanted a king like the kings of the other nations, “that
we also may be like all the nations” (1 Samuel 8:20). This was
not the kind of king God had in mind for them. God’s king
would be a shepherd, like David. He would rule by service, as
the servant of the people. The kind of king they wanted, though,
would rule through fear and domination, and would tax the peo-
ple to death. Jesus summed up the difference this way:

You know that those who are recognized as rulers of the Gentiles
lord it over them; and their great men exercise authority over
them. But it is nat so among you, but whoever wishes to become
great among you shall be your servant; and whoever wishes to
be first among you shall be slave of all (Mark 10:42-43).

The crucible of Philistine enslavement also tore up the sym-
bolic polity of Israel. When the Ark returned to the land, it was
not placed back in the Tabernacle, but was enshrined at Kiriath-
jearim. We are not told why this was, but Samuel was the leader
in Israel at this point, and it must have been under his guidance
that it was done. Not until the Temple was built, a century later,
was the Ark restored to a House of God. During this century,
when there was no central sanctuary, the people were permitted
to offer sacrifices at “high places,” local “holy mountains” analo-
gous to the oasis-sanctuaries of the Patriarchs (1 Kings 3:2; and
cf. 1 Samuel 6:15; 7:16-17; 9:12-13; 10:5, 8, 13; 11:14-15; 16:3-5;
20:6, 29; 2 Samuel 15:12). Later, when the Temple was set up,
this kind of high place worship was no longer acceptable (Deu-
teronomy 12:13-14; 1 Kings 15:14; 22:43; 2 Kings 12:3; etc.), For
now, however, the nation was clearly torn apart, with no center.

T believe that the rending of the Tabernacle should be associ-
ated with the rending of the animal in the sacrificial system. It is
a picture of death, and the building of the Temple is a picture of
resurrection. Nereparampil has shown that in John’s Gospel,
Jesus is pictured as “tabernacling” with His people before His
