 

Let us, the Cherubim mystically representing,
And unto the Life-giving Trinity

the thrice-holy chant intoning,
All cares terrestrial now lay aside,
That we may raise on high the King of all,
Like Conqueror on shield and spears,
By the angelic hosts invisibly upborne.
Alleluia!

The Cherubimic Hymn!

 
