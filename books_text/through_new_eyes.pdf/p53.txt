FOUR

THE WORLD AS
GOD’S HOUSE

 

Suppose we were going to build a house. What would be our
first step? Imagine that we bought some land, and then went out
and bought some materials. We all came out to the land one day
and started putting it together with no pre-arranged plan. We
started nailing boards together, pouring concrete, laying pipes,
and all the rest, according to our whims. What kind of building
would we erect, if we could get anything up at all?

Clearly something else is needed: a blueprint. We need to go
to an architect and have him draw up a blueprint, a model, for
us to work from. We also need to come up with a schedule of
what is done first, and what is done later. Then we can get
together and build the house ‘properly. The Bible tells us that
man is God’s image and workman, taking the raw materials of
the world and building civilizations from it- As a worker, man
needs a blueprint and a schedule; that is, he needs a worldview
and a philosophy of history. In general, these are provided by
the Scriptures: The Bible tells us what to-do and how to do it in
the transforming power of the Spirit. In terms of a worldview
niodel, however, the Bible shows heaven as the blueprint for
earth. Heaven as a model for the eatth is presented to us in
Genesis 1.

The Heavenly Blueprint
“In the beginning, God créated the heavens and the earth,”
says Genesis 1:1.1 This heaven is the “highest” or “third” heaven.
In Genesis 1:8, God created the “firmament” within the earth,
and called it “heaven.” The stars were placed in. this “firmament
heaven,” and birds are said to fly in it (Genesis 1:17, 20). Thus,

4l
