62 TuroucH New Eves

In conclusion, when Joseph saw the sun, moon, and eleven
stars bowing down to him (Genesis 37:5-10), what do you sup-
pose the “stars” were? It seems miost likely that they were the
twelve signs of the zodiac. It would be interesting to take the
twelve tribes of Israel, and the preeminent symbols associated
with each by Jacob and Moses, and study them as “humanity in
twelve dimensions,” both as revelations of sinful Adam and as
adumbrations of Christ. Such a study might shed light on the re-
lationship between the twelve tribes and the zodiac.

Prophetic Stars

Let us now briefly survey the passages where sun, moon,
and stars are used in a prophetic-symbolic sense. A failure to
understand the symbolic nature of these passages has led a few
popular writers to assume that such expressions as “the sun turned
to sackcloth and the moon to blood” can only be understood as
referring to the collapse of the physical cosmos. Nobody takes
these verses literally, after all. The question is, to what kind of
event does this symbolic language refer? For modern man, it
seems that it can only be speaking of the end of the natural
world. For ancient man, it was indeed the end of the “world” that
such language indicated, but not the “world” in our modern
scientific sense. Rather, it was the end of the “world” in a socio-
political sense.

For instance, Isaiah 13:9-10 says that “the day of the Lorp is
coming,” and when it comes, “the stars of heaven and their con-
stellations will not flash forth their light; the sun will be dark
when it rises, and the moon will not shed its light.” It goes on to
say in verse 13, “I shall make the heavens tremble, and the earth
will be shaken from its place at the fury of the Loxp of hosts in
the day of His burning anger.” Well, this certainly does sound
like the end of the world! But, if we read these verses in context,
we have to change our initial impression. Verse 1 says, “The
oracle concerning Babylon which Isaiah the son of Amoz saw,”
and if we read on, we find nothing to indicate any change in sub-
ject. It is the end of Babylon, not the end of the world, that is
spoken of. In fact, in verse 17, God says that he will “stir up the
Medes against them,” so that the entire chapter is clearly con-
cerned only with Babylon’s destruction.
