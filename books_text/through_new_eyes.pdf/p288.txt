Diagram 18.2
Types of the Church in the Letters to the Seven Churches of Asia

Ephesus: Eden and the Fall

2:1 the Lord walking among tree-like
tampstands {Ex.37:19-21)

2:4-5 falling from the first love

Reward:

2:7 — tree of life; paradise of God

Smyrna: The Patriarchs, especially Joseph
2:8 death and resurrection

2:9 poverty and riches

2:9 counterfeit Jews

Reward:

2:10 _ prison & elevation tothe crown

Pergamum: The Exadus and Wilderness
2:13. Satan'senvironment

2:14 Balaam

2:14 Balak

2:16 sword against fornicators

Reward:

2:17 hidden manna

2:17 whitestone

Thyatira: The Davidic Monarchy
2:18 bronze legs

2:20 Jezebel

2:22 greattribulation

2:23 God searches the heart
Reward:

2:26f. Kingly rule

2:28 themorning star

the Lard watking in Eden (Gen. 3:8)

Adam’s fall

Genesis 2:9

Rom. 4:19; Gen, 25:21; 1;
Gen. 22:1-14; Heb. 11:17-19
Heb. 14:9
counterfeit Isaac, Gen. 21:9; Gal, 4:22-31

Gen. 39,41

wilderness, Matt. 4:1; 12:43

Num, 22-24; 3116

Num. 25:1-3

Phineas, Num. 22:31; 25:7-8; 31:8

 

{mvanina hidden in ark}
9-12; Zech. 3:5-9*

   

Jachin & Boaz, I Kings 7213-22
T Kings 16, 21

1 Kings 17; Rev. 12:6} 13:5
Psalmsof David, esp. Ps. 51, 139

the Davidic Son, Psalm 2
Root of David, Rev. 22:16

Sardis: Jeremiah and the Later Prophetic Period

3:1 almostdead
3:3. invasion like a thief

3:4 soiled garments

Reward:
3:5 white garments, Christdefending us
against Satan beiore the Father

Philadelphia: The Return from Exile
3:7 restored David
3:8 little power

open door

38
3:9 false Jews

judgment andexile

sack of Jerusalem by Nebuchadnezzar (cp.
Luke 12:39; Rev. 16:15; the sack of
Jerusalem by Titus)

the garments of Joshua, Zech. 3

Zech, 3*

Zerubbabel, Zech. 4*

the post-exilic community (Ezra,
Nehemiah)

world influence (Esther)

Ezra 4; Neh. 4, 6, 13
