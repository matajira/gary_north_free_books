END NOTES

 

Introduction

1, Vincent Rossi, “Understanding History from a Christian Perspective,” Tree of
Life 4:1 (1986), p. 14.

Chapter 1— Interpreting the World Design

1. A translation of Basil’s Hexameron is found in The Nicene and Post-Nicene Fathers,
series 2, vol, 8 (Grand Rapids: Eerdmans, n.d.). A translation of Augustine's
work by John H, Taylor is published as vols. 41 & 42 of the Ancient Christian
Writers series (New York: Newman Press, 1982).

. On Biblical chronology, see James B. Jordan, “The Biblical Chronology Ques-
tion: An Analysis,” Creation Social Sciences and Humanities Quarterly 11:2 (Winter

15, If:3 (Spring, 1980):17-26. A copy of this essay can be obtained from
Biblical-Horizons, P.O, Box 1320, Tyler, TX 75713.

3. On the shifting sands of science, see Thomas S, Kuhn, The Structure of Scientific
Revolutions, 2d ed. (Chicago: University of Chicago Press, 1970); and Stanley
L. Jaki, The Road of Science and the Ways to God (Chicago: University of Chicago
Press, 1978). These two works are complementary. In my opinion, Kuhn sees
too much discontinuity, but Jaki sees 100 much continuity.

4, On the scale of being, sce Arthur O. Lovejoy, The Great Chain of Being (Gam-
bridge: Harvard University Press, 1936); and Rousas J. Rushdoony, The One
and the Many (Tyler, TX: Thoburn Press, [1971] 1978).

5. Those wha wish to read further in the area of six-day creationism should con-
sult the following studies: Gary North, The Dominion Covenant: Genesis, 2d ed.
(Tyler, TX: Institute for Christian Economics, 1987); Gary North, ed., The
Journal of Christian Reconstruction 1:4, “Symposium on Creation” (1974); Edward
‘Jo Young, Stidias in Genesis One (Phillipsburg, NJ: Presbyterian and Reformed
Pub. Go., 1964); John G. Whitcomb, The Early Earth (Grand Rapids: Baker,
1972); and R. J. Rushdoony, The Mythology of Science (Nutley, NJ: Craig Press,
1967).

6. See James B. Jordan, Revelation Made Practical (tape set with syllabus available
from Biblical Horizons, P.O. Box 132011, Tyler, TX 75713).

7, George E. Mendenhall, The Yanth Generation: The Origins of the Biblical Tradition
(Baltimore: Johns Hopkins Press, 1973), p. 39.

8. The recent trend in “libera!* scholarship is to grant a bit more intelligence to the
“final redactor.” Of course, Divine authorship continues to be denied.

2Ht

n

 
