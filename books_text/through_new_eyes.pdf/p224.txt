212 Trroucu New Eyes

with stars, as Revelation 1:20 makes explicit. It can be sug-
gested, though not proved, that the seven stars are to be associ-
ated with the seven planets of the ancient world. The planets
were the “stars” that moved, and the Tabernacle was portable.
Another suggestion is to associate the seven lamps with the seven
sisters of the Pleiades (Job 9:9; 38:31; Amos 5:8).18

The twelve loaves of showbread on the table of showbread
should be correlated to the manna that rained upon Israel from
heaven during the wilderness sojourn (Nehemiah 9:15; John
6:31). As the people ate this heavenly bread, they were symboli-
cally transformed into a heavenly people. They became the
“stars” of the Abrahamic promise.

Finally, the cloud of smoke arising from the altar of incense is
to be associated with God's glory-cloud, as it appeared in the fir-
mament-heavens (Exodus 19:18). Incense has to do with prayer,
and the glory-cloud environment is an environment of ceaseless
angelic prayer (Isaiah 6:3-4; Revelation 5:8).

It is very interesting to note that synagogue buildings
dating from the early Christian era very often have three sec-
tions of mosaics on their floors, stretching from the door to the
front. The first mosaic is generally nondescript; but the one at
the Beth Alpha synagogue is a picture of the sacrifice of Isaac,
clearly a courtyard, holy-mountain theme. The second mosaic,
occupying the center of the hall, is almost invariably a zodiac.
The third mosaic, at the front, is a “sacred portal,” filled with
imagery of the highest heavens. Obviously the zodiac has to do
with the firmament-heavens, in this sequence derived from the
Tabernacle and Temple. Similar zodiacs are found in early
Christian churches. 19

The Tabernacle as Holy Mountain

While the altar in the Tabernacle complex was a holy moun-
tain, leading toward heaven, in a wider sense the entire Taber-
nacle complex was a holy mountain, or extended ladder to
heaven, What makes this clear is the connection between the
Tabernacle and Mount Sinai. We have already noted that the
three-storied Ark of Noah was a world model, a model that
transferred itself to the three-storied configuration of the world
after the Flood. We see the same thing here: Mount Sinai was a
