Subject Index

food Jaws, 201

footstanl, 90, 207

fortress, 72

four corners of the earth, 147£.

frarmework hypothesis, 11

future, cannot be envisioned, 198, 222,
949, 2736, 286

Garden-sanctuary, 152fF., 175, 190, 228,
251, 269
garment, universe as, 197
gehenna, 159, 230, 308
gems, 74ff., 158
see bdellium, gold, onyx, pearls,
sapphire, silver
Gentiles, 211, 303f.
see sea, Gentile
geography, 2
Gideon, 56
Gihon, 150f.
glory (heavy), 74
maturation in, 106f.
glory-cloud, 421f., 54, 85, 203, 216, 274,
293, 294
gorification, U7, 123f., 135
God Ahnighty, 188
God Most High, 176
gods, defeated, 186
Gog and Magog, 245
gold, 74, 85, 91, 158
good, eschatological character of, 123
gopher wood, 92, 172
Goshen, 192, 305
gourd, Jonah's; 83
gravity, 109, 141
grove, 90ff., 190
see oasis
guard, guarding, 113, 183, 136ff., 214, 300
gutter, 302

Hannah, 71
Ham, 174, 177
Havilah, 73, 75, 18, 150f., 158, 295
heaven, 54, 293
and earth, 167, 197, 207, 243, 260f.
as model or pattern, 41ff., 153f., 293
heaviness, 74
hearth, 1588, 301
Hebrew, meaning of term, 188
hermeneutics, 15ff.
history, philosophy of, 2, 41
stages of, 181f.

343

high ground, 155
high places, 224
High Priest, 75
Holy Communion, 125f.
Holy of Holies
see Most Holy
holy ground, 85
Holy Place, 206¢f., 231, 309
holy places, 271
Holy Spirit, 42f., 192
hooves, split, 101f,
Horeb, 85
host of God, 55f,, 58, 85, 106, 161, 204f.
house, world as, 44#f.
houschuilding, 186f., 205, 245
hyssop, 86

idolauy, 51
imagery, 13

Imperial order, 241
incense, 206, 212, 2166.
incest, 198

infancy, 156
interpretations, rules, 1547.
Isaac, 65, 183

Ishmael, Jews as, 274
Ichamar, 225

Jacob, 158, 183
“Jacob's ladder, 87
Jealousy inspection, 234
Jericho, 9
Jeroboam, 1874.
“Jerusalem, 227
destruction of, 65, 66, 245
heavenly, 157
see New Jerusalem
Jethro, 202, 205
John the Forerunner, 140; 259, 267
Joktan, 304
Jenah, 83, 100
Jordan River, 295
Joseph, 59H, 135, 192, 296, 304, 305
Joshua, 202
Josquin des Prez, 21
jubilee, 211
Judah, 22
judges, 202f,, 221
judgment, 168
justification, and symbolism, 33

Kant, Immanuel, 29
