Symbotism and Worldview 37

The second problem with trying to revivify old symbols is
that such an attempt can become idolatrous, if it says that the
mainspring of human society lies in man-made symbols, instead
of in God’s symbols. It is idolatrous'to say that restoring the U.S.
Constitution is more necessary to social renewal than restoring
the sacraments. The key to social renewal, to cultural reforma-
tion, to Christian reclamation, then, is this: We must restore the
primary special symbols: Word, Sacrament, Persons. That is be~
cause only the primary special symbols transcend history, and
thus only they can form the wellspring of historical progress.

Conclusion

Practically speaking, does this mean to stop singing in
church any hymn more than a hundred or so years old? Stop
reciting the Apostles’ Creed? Stop using the Westrninster Con-
fession (or whatever)? Not at all. The principle of growth means
we have to move on, but it also means we cannot move on until
we understand our heritage. To try to generate good church
music out of the. meager vocabulary of American popular music
is like trying to generate good theology out of the ideas heard an
Christian radio and television. Christian theologians need to ac-
quire familiarity with the whole of the Christian past, in con-
stant contact with the primary special.symbols, in order to move
forward into new man-made theologies. Christian musicians
must know all the music of the Christian past, in constant con-
tact with the primary special symbols, in order to produce good
contemporary Christian music.?

Thus, earlier man-generated symbols have great value to us,
if we do not commit idolatry with them. The preacher must
preach his man-generated sermons from the primary symbol
(the Word) with an eye to the man-generated symbols of the past
as well as the needs of the present. The liturgist must organize
his man-generated expression of worship from the primary sym-
bol (the sacraments) with an eye to the man-generated liturgies
of the past as well as the needs of the present. The Christian
must. live his man-generated life out of the example of the pri-
mary Symbol (Jesus Christ), with an eye to the godly men of the
past and present.
