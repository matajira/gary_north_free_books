182 TuroucH New Eves

made a man to rule it, so also, after the priests of Israel fell into
sin (1 Samuel 1-3), God took the opportunity to rc-create the
(Adamic) king with the Davidic covenant, With the collapse of
the Davidic covenant and the exile, God took the opportunity to
inaugurate the imperial stage of history, and placed Israel under
the protection of world emperors. With the collapse of the im-
perial stage of history, seen in Rome's crucifixion of the Son of
God, God enthroned Jesus Christ to be the True Noahic Gen-
tile, the True Abrahamic Hebrew, the True (Mosaic) Aaronic
Priest, the True Davidic King, and the True World Emperor.

Each of these covenants is built on the previous one, by way
of being added to it. Each one, however, transforms the previous
one as well. Once Abram’s family had been set aside as priests, it
was no longer enough for the Gentiles to obey the Noahic cove-
nant. They were also required to bless Abram. Once the Taber-
nacle was set up, it was no longer proper for the Hebrews to
have altars in many places.? The only altar permitted was at the
Tabernacle. Once the Temple was set up, there was no more
moving around of the Tabernacle from place to place. Once the
Imperial stage of history was inaugurated, God’s people were re-
quired to “render to Caesar.” Of course, with the coming of the
New Covenant, there were radical transformations of the entire
Old Covenant series.

With this as background and context, let us look at the age of
the patriarchs. We have already noticed God’s laying hold” on
the situation in His call of Abram out of Ur of the Chaldees. We
find that in Ur, Terah had sons, but his son Haran died (Genesis
11:27-28). We find that in Ur, Abram took a wife, but his wife
was barren (Genesis 11:29-30). The message was clear: If you
have sons in Babylon, they will die; and if you take wives in
Babylon, they will be barren. An exodus is clearly needed.?

Exodus
What follows is the second exodus in the Bible, the first being
the Flood. The exodus is the second step in the typological pat-
tern of history, the transition from the old world to the new. It is
the act of breaking down and restructuring, as when I remove a
glass from the. cabinet and. water from the pipe, and put them
together into a new thing: a glass of water; or as when Jesus,
