Breaking Bread: The Rite of Transformation 429

calling, “Bless we the Lord!” and the congregation shouting,
“Thanks be to God.”

In this way, worship keys the believer into the proper frame
of mind for all of life. Since men continually and unceasingly are
engaged in acts of restructuring, distributing, and evaluating, it
would be impossible to try to sort out every action in life and
engage in a particular act of thanksgiving at the appropriate spot
in the sequence. We do not ordinarily stop to give thanks, for in-
stance, when we get a glass from the cabinet, to return to the ex-
ample used above. All the same, there are certain specific times
in the day when, according to the consensus of Christian wis-
dom of all ages, it is appropriate to stop and give thanks. The
most obvious of these is mealtime. After the food has been set on
the table (so that we visually “take hold” of it), we offer thanks,
and then get to work eating it (restructuring, appreciating, etc.).
Similarly, first thing in the morning, as we lay hold on the day’s
chores and events, we give thanks. Public meetings used to
begin with prayer, before getting down to work. In this way, the
simple six-fold rite is applied constantly in daily life, and in this
way the Kingdom comes.

The stress on thanksgiving in liturgical piety is thus the key
to practical or vocational piety. In the early Church, all life was
thus worship, either the special worship of the rite, or the gen-
eral worship of thanksgiving in all of life (1 Thessalonians 5:18).
This worship-centered piety was the characteristic of the earliest
Church. It must become ours today.

Conclusion and a Qualifying Addendum

While it would be interesting and valuable to trace out many
more examples of how men are to transfigure God’s world
through the six-fold pattern, our main concern in the present
book is with God’s own actions. At each stage of Biblical history,
God lays hold of an existing deteriorating situation, breaks His
people down through a death-resurrection transition, and re-
establishes them with a renewed covenant. Each time God does
this, He brings in a new covenant, a new stage of history, a new
world model. We shall trace this pattern in Chapters 12 through
18. By becoming familiar with how God acts, we shall become
