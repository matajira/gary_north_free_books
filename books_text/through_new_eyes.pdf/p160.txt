148 TuroucH New Eves

pillars, the falling of its ceiling-stars, etc. In terms of the world as
altar, judgment means fire falling upon the altar. Positively, the
world is a house to be adorned and in which man worships, and
also an altar on which man grows and upon which he offers him-
self and his faith-filled good works to God.

Not only is the Biblical, cosmic world four-cornered in a
symbolic sense, so is the Biblical social world. Israelite society
under the judges and kings had four corners or cornerstones at its
top. These men were the supreme judge or king, and his three
top advisors. This word occurs in Judges 20:2 and 1 Samuel
14:38, where it is translated “chiefs” (cf. also Isaiah 19:13; 28:16;
Zephaniah 1:16; 3:6; Zechariah 10:4). Thus David had three
mighty men, he being the chief corner (2 Samuel 23; 1 Chroni-
cles 11). Similarly, Jesus had Peter, James, and John, who were
the three corner pillars of the Apostolic Church (Galatians 2:9),
remembering that Jesus was Himself the fourth and chief cor-
nerstone of the new temple (Ephesians 2:20; 1 Peter 2:6).

This image of the four-corncred world takes its rise from the
fact that four rivers flowed out of the Garden of Eden to water
the whole world. They doubtless did not actually flow in four op-
posite directions; indeed, they all seem to have flowed south, as
we shall see. Symbolically, however, they carried the Edenic pat-
tern to the four corners of the earth. The task of Adam’s descen-
dants would be to follow the four rivers and carry with them the
Kingdom pattern, extending it over the whole earth and bring-
ing the world from primordial to eschatological glory.

The four rivers going to the four corners can and should he
associated with the four faces of the cherubim (Ezekiel 1:10), the
four sides of the camp of Israel (Numbers 2), and the four limbs
of the cross.* It is a fundamental symbol of the world structure.
(See Diagrams 12.2 and 12.3.)

North and South
Ancient man knew that the world was a sphere, and the Bible
affirms, in Job 26:7, that God hung “the earth on nothing.” It is
a fundamental mistake to assume that ancient or medieval world
diagrams with four corners, or riding on the backs of elephants
or bulls, were taken by educated people as literal pictures of the
world, They were always understood to be symbolic. For in-
