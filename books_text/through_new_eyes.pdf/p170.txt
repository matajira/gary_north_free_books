158 ‘THroucu New Eyes

Thus, mountains and pyramids arc ladders to heaven. (See
Diagram 7.1, p. 89.) At the Tower of Babel, sinful man tried to
build such a pyramid-ladder from the ground up, but God for-
bade it. The ground had been defiled by Adam’s sin, since
Adam was made of earth and his sin corrupted the earth, Thus,
if there were to be a new ladder to heaven it would have to pro-
ceed from above to below. Jacob saw such a ladder in prophetic
vision (Genesis 28:12). In fulfillment, the New Jerusalem came
from heaven to earth, not vice versa, on the Day of Pentecost.

The New Jerusalem is a gem-studded pyramid overlaid on a
mountain (Revelation 21:2, 10ff.). Unlike the holy mountains of
the Old Covenant, the New Jerusalem is definitely (symboli-
cally} on the highest of all mountains, because the apex of the
pyramid reaches into heaven itself and the throne of God (Reve-
lation 22:1); and it is from here that the restored rivers flow to
bring life to the world.

Whence came these gems and gold for Jerusalem? From. the
outlying lands. Neither Eden nor Israel, it secms, were rich in
precious stones. The Jews got the raw materials for the Taber-
nacle and the gems for the High Priest’s ephod from Egyptian
spoil (Exodus 35) and from their travels in Havilah (Arabia).
Solomon’s Temple was adorned with gold and gems from other
lands (2 Samuel 8:11; 1 Kings 7:51; 9:28; 10:11). The message is
that God’s house cannot be fully built until all nations are con-
verted and cooperate in its spiritual development. As the rivers
of spiritual blessing go out from Jerusalem (the Church), so the
nations return their tithes for her adornment.

It remains to note that altars were also holy mountains, lad-
ders to heaven. We have just mentioned the contrast between
Jacob's ladder and the Tower of Babel. More broadly speaking,
there is a contrast between the Tower of Babel and the altars of
worship set up by Abraham. Abraham’s altars were probably
just pillars made up of stone and earth, but what they symbol-
ized is sct out for us in an important vision in Ezekiel 43.

Ezekiel describes an altar in the form of a stepped pyramid.
The top section is called “the Mountain of God,” and the plat-
form on top for the fire is called the “hearth.” A literal translation
of Ezekiel 43:15:is: “And the Mountain of God: four cubits
(high); and from the hearth four horns extend upwards.”!*
