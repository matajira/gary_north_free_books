188 TuroucH New Eves

Establishment

Now that Abram has made his exodus from Babylon and
come into the land, what is the nature of the covenant estab-
lished with him, and with the succeeding patriarchs Isaac,
Jacob, and the sons of Israel? First, the name of God given in
connection with the new covenant is, as we have mentioned,
God Almighty. By this name, God assured the patriarchs that
He was fully capable of performing what He promised.

Second, God gave new names to His restructured people.
God changed Abram to Abraham and Jacob to Israel. Jacob
means Supplanter, and pointed to his being the younger son who
replaces the older. The older son is often a type of Adam, and
the younger of the Second Adam, Thus, Seth replaced Cain,
Shem replaced Japheth (Genesis 5:32; 9:24; 11:10), Isaac re-
placed Ishmael, Jacob replaced Esau, Joseph replaced the older
brothers, Ephraim replaced Manasseh (Genesis 48:18), Eleazar
and Ithamar replaced Nadab and Abihu (Exodus 6:23; 24:1;
Leviticus 10:1-6), David replaced his older brothers, and Jesus
replaced Adam. Israel, however, means God’s Prince. When we
get to Exodus and the Mosaic covenant, we find that the priestly
nation is called “children of Israel,” a race of princes and
princesses. During the patriarchal period, however, they were
known as Hebrews, descendants of the Shemite Eber (Genesis
11:16; 14:13; 43:32; Exodus 1:15; 2:6; 3:18; 5:3; 21:2).12

The grant made to Abram and his descendants was the land
of Canaan (Genesis 15:18-21), The stipulations that came along
with this grant were to obey all of God’s law (Genesis 26:5) and
in the area of sacraments, circumcision (Genesis 17).

The new world polity that came into being meant that the
Hebrews were a nation of priests to evangelize and guide the
Gentiles. This is what it meant for Abraham to be a “father” to
other nations (Genesis 45:8; Romans 4:1t), The evangelistic
ministry of the patriarchs is symbolized by their altars and wells,
as we shall see.

The internal polity of the people of Abraham was a simple
patriarchal or clan order: The family head was also the spiritual
leader. Since they were not yet a nation, and did not govern any
territory, they did not exercise the sword of civil authority. Thus,
“separation of church and state” was not an issue during the pa-
