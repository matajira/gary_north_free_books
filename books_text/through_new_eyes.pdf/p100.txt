88 THroucu New Eyes

I need to mention something briefly here that we shall take
up in more detail later. Altars and pillar stones are also ladders
to heaven. This is because they are miniature holy mountains,
and the holy mountain is a ladder to heaven. The Tower of
Babel, a ziggurat, and the pyramids of Egypt were counterfeit
holy mountains. The holy mountain has God's glory at the top,
the glory-heaven. The altar and pillar represent this (see Diagram
7.1). We shall come back to this in detail later. I mention it now
to point up the association of altars and trees in the patriarchal
era, Abram built a worship altar, a ladder to heaven, at Shechem,
and this is associated with a tree (Genesis 12:6; 35:4; Joshua
24:25; Judges 9:6). The same is true of Abram at Mamre (Gen-
esis 13:18; 14:13; 18:1), at Beersheba (Genesis 21:33), and of Jacob
at Bethel (Genesis 28:18, 22; 35:7, 8, 14). These were terebinth
oaks, trees that had massive trunks and huge cloud-like canopies.
(Also note that God fed Elijah under a broom tree, and from
there led him to the holy mountain Horeb, 1 Kings 19:4, 5, 7.)

The gate, or entry court, of the city was the place where the
law courts were held (Deuteronomy 17:5; 22:15, 24; Ruth 4:1, etc.).
The gate or forecourt of the Tabernacle was where God executed
judgment upon the animal substitutes in the sacrificial system
(Leviticus 1:3, etc.); and as we shall see later on, this area was
the foot of the symbolic holy mountain, and thus the bottom of
the ladder, the gate of heaven. The association of the gate, then,
is with judgment.

We find the same association with trees. As a ladder to
heaven, the base of the tree is the gate. Thus, Deborah set up
her chair of judgment at the Palm Tree of Deborah (Judges
4:4-5). Joash the Abiezrite held court and conducted false wor-
ship at an oak (Judges 6:11, 12, 19, 21, 30-32). Saul held court
at a pomegranate tree (1 Samuel 14:2) and at a tamarisk tree
(1 Samuel 22:6),

Of course, there were plenty of counterfeit ladder trees, and
the Bible condemns false worship when conducted. under leafy
trees (Deuteronomy 12:2; Isaiah 57:5-7; Jeremiah 2:20; 3:6;
17:2). But these are only counterfeits of the truth.

In this connection it remains only to note that since both
altars and trees are ladders to heaven, a tree can be an altar, The
