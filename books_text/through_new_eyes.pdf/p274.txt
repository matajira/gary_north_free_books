262 ‘THroucnH New Eyes

Remember the point we made in Chapter 15; The Mosaic
Law was an easy yoke. It was not hard to keep. The parents of
John the Baptist kept it perfectly (Luke 1:6). They obeyed the
law; and when they fell into sin, they did what the law said to do
about it. Thus, when Jesus called the people back to the law and
warned them to do a better job than the Pharisees, He was not
laying some heavy burden on them. Actually, He was lightening
their load.

Each time the covenant changed in the Old Testament there
was a change in law. In one sense, each time the change was
total in that the form of the law changed and the historic circum-
stances of its phrasing and application changed. Yet, since the law
reveals God’s character, its fundamental conient can never
change. At the same time, God only reveals His law to man in
specific forms and circumstances. Even the form of the Ten
Commandments changed between Exodus 20 and Deuteron-
omy 5. It is because the fundamental content of the law never
changes that. the prophets called men back to the older law each
time; but it is because the circumstances of history change and
mature that the new covenant, when it comes, is always different
in form. The changes in law during the Old Testament were rel-
atively minor compared to the change from the Old Adamic
Covenant to the New Covenant, as we shall see.

Law, Wisdom, and Paradox

In calling the people back to the Old Covenant Law and
Prophets, Jesus simultaneously advanced the standards of the
kingdom a step further. We have seen that God gave the people a
written law for the Mosaic establishment, but that in the Davidic
establishment, the focus is on wisdom based on the law. The
people were to take the principles of the Mosaic Law and apply
them to new and changing circumstances. In the Restoration cs-
tablishment, wisdom was taxed further since under imperial rule
the Jews were unable to keep much of the law in its original
form. Jesus takes us one step further, from law and wisdom to
what I shall call paradox. A paradox is an apparent contradiction
that forces us to meditate on deeper meanings.*

There is a great deal of paradox in Jesus’ teaching and in the
teaching of the New Testament as a whole. In the Sermon on the
