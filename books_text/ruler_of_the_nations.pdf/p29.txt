Author's Introduction 9

If Christians are to change things at the higher levels of civil
government, things must first change at the bottom, first in the hearts of
men, second in the realm of self-government, third in church gov-
ernment, fourth in family government, fifth in local civil govern-
ment, and only then in distant central government. Christian recon-
struction is an inside-out process, a bottom-up process. Nevertheless, this
process can take place nearly simultaneously during a major re-
vival, as it did in Nineveh under the preaching of Jonah. Changes
eventually must occur at both ends of the domino spectrum.
Those who rule at the highest levels of civil government must
cease to see themselves as independent of God’s government over
them, At the same time, the citizenry must stop looking to the
State for salvation and get their own households in order. The
government at the top (civil government or the State) is a re-
flection of the government at the bottom (the type of self-govern-
ment supporting the family and church). In effect, you get what
you vote for.

Israel’s Example

Israel’s history is the scene for the Biblical “domino effect.” The
apostle Paul wrote;

For I do not want you to be unaware, brethren, that our
fathers were all under the cloud, and all passed through the sea;
and all were baptized into Moses in the cloud and in the sea; and
all ate the same spiritual food; and all drank the same spiritual
drink, for they were drinking from a spiritual rock which fol-
lowed them; and the rock was Christ. Nevertheless, with most of
them God was not well-pleased; for they were laid low in the wil-
derness. Now these things happened as examples for us, that we should not
crave evil things, as they also craved. And do not be idolaters, as some
of them were; as it is written, “The people sat down to eat and
drink, and stood up to play.” Nor let us act immorally, as some of
them did, and twenty-three thousand fell in one day. Nor let us
try the Lord, as some of them did, and were destroyed by the
serpents. Nor grumble, as some of them did, and were destroyed
by the destroyer. Now these things happened to them as an example, and
they were written for our instruction, upon whom the ends of the ages have
come {1 Corinthians 10:1-11),
