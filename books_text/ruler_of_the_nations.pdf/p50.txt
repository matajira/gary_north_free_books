30 Ruler of the Nations

Babel wanted to rule the world. Their centralized kingdom would
be located in the “land of Shinar.” Man, through the agency of
statist power, would become God as he ascended the Tower of
Power, grabbing for himself more and more control over God’s
created order, centralizing his domain and subjugating the people
to do his bidding.

Decentralization

In contrast to the pyramid system, God’s system of political
power is decentralized. No single institution has been established by
God to bring about social order. Freedom and order are realized
when men throughout a society strive to follow the blueprint God
has given for the reconstruction of all family, ecclesiastical, social,
and political institutions. For example, Genesis 10 is a list of many
families that represent a decentralized social order. The builders
of Babel wanted to eliminate the many governments and consolid-
ate family, ecclesiastical, and political power in the one State. God
would have none of it. God “scattered them abroad from there
over the face of the whole earth; and they stopped building the
city” (Genesis 11:8).

This language is very similar to what God says of anyone who
works to substitute God’s blueprint for an orderly society with
man’s blueprint for tyranny and oppression: “Nevertheless you
will be thrust down to Sheol, to the recesses of the pit” (Isaiah
14:15). All the kingdoms in Nebuchadnezzar’s dream, a reflection
of the Tower of Power, “were crushed all at the same time, and be-
came like chaff from the summer threshing floors; and the wind
carried them away so that not a trace of them was found” (Daniel
2:35). Man’s Towers of Power will be “thrust down,” “crushed,”
and “scattered.” God’s kingdom—God’s government—will be-
come “a great mountain” that will fill “the whole earth . . . and
will itself endure forever” (Daniel 2:35). While centralized poli-
tical regimes will be “scattered,” the government of Jesus Christ
will have “no end” (Isaiah 9:7).

Bearing in mind that God ordains authority, what should a
Biblical political structure look like? Should the pyramid and the
