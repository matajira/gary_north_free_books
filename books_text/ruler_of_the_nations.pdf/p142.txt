122 Ruler of the Nations

(Matthew 5:13-16). Salt is a preservative,* keeping the culture
from experiencing social entropy, the inevitable decline of society
as sin works out its rotting effects. The Christian also is light,
pointing out the way to the spiritually blind.

As Christian cultures develop toward [the final days], aren’t we
serving as lights to all nations in every case where the gospel becomes
a shaping influence in any one nation? Shouldn’t we strive to cre-
ate a society which will shine ethically before other nations, just as we
try to do in our families, our churches, and our businesses?

Obviously, this passage applies both to individuals and to
groups of Christians. Yet many Bible teachers draw an arbitrary
line at politics and say, “Thus far, and no farther, Jesus. Your
word does not speak to this area of life. Yes, Israel was supposed
to shine, and Israel had less light then we do, for you have come.
But Christians have less responsibility!”

A godly society before pagans was an important aspect of
evangelism in the Old Testament. Why not today?

Many Christians contend that if enough people are saved, the
broader culture will change automatically. On the surface this
might seem reasonable. But it misses a vital element. While the
regenerate person certainly has a new disposition to do right, he is
often left without knowing what to do. The specific ethic of God’s
revealed laws has been reduced to the single ethic of “love.” Of
course, the Bible does command us to love, but love without spe-
cific guidelines is nothing more than sentimentality.

The Earth Is the Lord’s

Restoration begins by realizing that we live in the midst of
God’s kingdom. God's pattern for godly living is established in

4. Salt can also season food. “Christians are ta improve the world, in the same
way that salt improves flavor in otherwise bland, tasteless foods. How are they to
do this? With their good works, which is the focus of this passage [Matthew 5:16].
Remove a Christians good works, and he is no better than tasteless salt in the
eyes of the world”: Gary North, 75 Bible Questions Your Professors Pray You Won't Ask
(Tyler, Texas: Spurgeon Press, 1984), p. 145. Both metaphors teach similar
truths: Our good works preserve society and our good works add savor to society.

5. Ibid., p. 148.
