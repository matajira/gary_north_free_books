Call No Man Your Father A

God has led you in the wilderness these forty years, that He
might humble you, testing you, to know what was in your heart,
whether you would keep His commandments or not. And He
humbled you and let you be hungry, and fed you with manna
which you did not know, nor did your fathers know, that He
might make you understand that man does not live by bread
alone, but man lives by everything that proceeds out of the
mouth of the Lorb. Your clothing did not wear out on you, nor
did your foot swell these forty years. Thus you are to know in your
heart that the Lorp your God was disciplining you just as a man disci-
plines his son (Deuteronomy 8:1-6).

Nearly every time Israel was in need of provisions, they
wanted to go back to Egypt, back to the imagined security of a
fatherly State. When the freed Israelite slaves were hungry, in-
stead of turning to God their Father for provision, they turned to
the supposed security of tyrannical Egypt: “Would that we had
died by the Loro’s hand in the land of Egypt, when we sat by the
pots of meat, when we ate bread to the full; for you have brought
us out into this wilderness to kill this whole assembly with hunger”
(Exodus 16:3). Their god was their appetite (Philippians 3:19):
“For such men are slaves not of our Lord Jesus Christ but of their
own appetites” (Romans 16:19).

Finally, God is our Father in that He redeems us. In one
sense, God is the Father of all. But in a very special sense, God is
Father only to His adopted children. Jesus called the Pharisees
children of their father the devil because they repudiated His re-
deeming work (John 8:31-47). We are adopted children who can
now cry out, “Abba! Father!” (Romans 7:15).

God Feeds Us

God fed the Israelites in the wilderness, and yet they sought
the supposed security of Egypt instead of the freedom and security
they had with God as their Father. Even after God provided them
the food they needed, they still grumbled. Egypt still seemed at-
tractive. “And the rabble who were among them had greedy
desires; and also the sons of Israel wept again and said, ‘Who will
