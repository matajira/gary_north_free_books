108 Ruler of the Nations

elimination of poverty, the establishment of a one-world human-
istic government, or the military threat of Mutual Assured
Destruction (MAD). (On the last point, see the book in the Bibli-
cal Blueprints Series, The Chariots of God.) To pray for peace, as we
are instructed to do, can be no substitute for the preaching of the
gospel so that the nations are discipled according to the Word of
Jesus Christ (Matthew 28:20), Wars do not come because of en-
vironmental factors. Rather, they are the result of man’s inherent
sinfulness: “What is the source of quarrels and conflicts among
you? Is not the source your pleasures that wage war in your mem-
bers? You lust and do not have; so you commit murder. And you
are envious and cannot obtain; so you fight and quarrel...”
(James 4:1-2). If this is true of the Christian community, should
we expect anything less among non-Christians?

Humanist institutions deny the reality of sin; therefore,. they
believe that man can save himself given enough time, money,
technology, and education. The following is an excerpt from The
Preamble to the Charter of the United Nations. It is an example
of man’s attempt at peace without Christ: “We the peoples of the
United Nations determined to save succeeding generations from
the scourge of war . . . to unite our strength to maintain interna-
tional peace and security, and to ensure, by the acceptance of
principles and the institution of methods, that armed force shall
not be used, save in the common interest . . . have resolved to
combine our efforts to accomplish these aims.”

Peace then comes by and through the efforts of man. Of
course “peace” is defined in man’s terms. The peace that the Lord
wants us to pray for and promote is His peace, not peace as men
define it. Jesus said that He came not to bring peace, but a sword.
The Word of God does not place the absence of physical conflict or
war as the highest value, but rather recognizes legitimate reasons
for men engaging in war—just grounds for defending family
members, neighbors, property, and the nation.
