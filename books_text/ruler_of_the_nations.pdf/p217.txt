Reconstructing Family Government 197

cal sanctions. For example, supervise the use of the automobile. It
should become an earned privilege, not an automatic benefit. Set
rules as to who will pay for gas, automobile insurance, and gen-
eral maintenance. Be sure all family members wear a seat belt at
all times. This is an aspect of self-government.

Supervise dating. Dating is another earned privilege. You
should always know where your children are. If plans change,
then they should call. Stiff discipline should be imposed when the
rules are broken. Make the punishment fit the crime.

Inheritance/Legitimacy/Continuity

Start a family business. This will help you work together as a
family, diversify your income, bring in extra income, help with
taxes, and teach your children a trade that could turn into quite a
business enterprise.

Encourage your children to set daily, weekly, monthly, and
yearly goals. They must become future-oriented. This is the
essence of upper-class thinking. It is the foundation of upper-class
income and responsibility.

Take your children to libraries, museums, historical sights,
anti-abortion rallies. Encourage them to visit the sick and help the
poor.

Give them an appreciation for good art, music, literature, and
poetry. Keep the radios in the house tuned to classical stations.
The upper-class cultural achievements of Western civilization
should be in front of them on a regular basis. This means, of
course, that parents must learn to appreciate and understand this
heritage.

Apprentice your older children during the summer months, if
possible. The experience and skills they will pick up will be their
pay. Let them try their. hands at carpentry, plumbing, farming,
computer programming, auto maintenance, journalism (neigh-
borhood newspapers, church newsletters, etc.), small appliance
repair, etc. Every child should learn how to type and operate a
computer.

Teach your children how to spend money and the principle of
