God Judges the Nations 67

judgment. Their citizens may pretend that they do not recognize
God's claim on them, but they cannot play pretend forever. Even-
tually, judgment comes.

Individualism denies the covenant. It denies that God judges
collectives in the midst of time. Without covenant sanctions by
God against nations, there can be no doctrine of covenant law over
nations. Thus, individualism results in social antinomianism.

There must be sanctification of individuals: definitive, pro-
gressive, and final. There must also be sanctification of collec-
tives: definitive and progressive, though not final (day of final
judgment). This is the basis of all history, which includes individ-
uals and collectives.

There is no such thing as a victimless crime. A crime is a
crime against God primarily, and man secondarily. Thus, the
State should enforce God’s laws, even if the violators are “consent-
ing adults.”

A God who does not bring judgments against nations in his-
tory is not the ruler of the nations. If He is the ruler of the nations,
then He does bring judgment against individuals and collectives.
He does so in terms of His laws that govern individuals and
collectives.

In summary:

1. God judged Babel, Sodom, and Gomorrah.

2. God therefore judged nations other than Israel.

3. Responsibility is collective.

4. God has promised in the past to protect a whole society for
the sake of a few righteous people.

5. He spared Judah for the sake of one righteous king.

6. The prophets warned’ pagan nations that they should
repent.

7. Individualism denies the covenant.

8. God judges only individuals on judgment day (Revelation
20:11-15).

9. He judges nations and groups within history.

10. A denial of collective judgment usually is accompanied by
a denial of God's law for collectives (blueprints).
