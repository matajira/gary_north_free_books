TABLE OF CONTENTS

Editor's Introduction ..........
Foreword by John Whitehead .....

 

Part I: BLUEPRINTS

  
   
  
   
  
 

Author's Introduction .............-.
1. God Is Sovereign, Not Man
2. A Bottom-Up Hierarchy .....
3. Plural Law Systems, Plural Gods 39
4. God Judges the Nations ..... 54
5. Call No Man Your Father .. 69
6. Jesus Alone Possesses Divine Rig ts - 85
7, We Must Render Appropriate Service . 101
8. Neutrality Isa Myth ...........- 0s eee eee e eee 116
9. Judges Need Jurisdiction ......

10. Rebuilding Takes Time. .

Conclusion......... 66. e eee eee ee

Part II: RECONSTRUCTION

 

11. Reconstructing Church Government........... 169
12. Reconstructing Family Government . 185
13. Reconstructing Civil Government .............. 203
Appendix: The Christian Origins of
American Civil Government ............. 225
Bibliography .........

  
 

Scripture Index .
Subject Index... 0.6... ccc cece cee eee cece cence eens 253
What Are Biblical Blueprints? ..........00 06000 cece 257

vii
