I._Transcendence/Presence

1
GOD IS SOVEREIGN, NOT MAN

For the Lorp is our judge, the Lorn is our lawgiver, the Lorp
is our king: He will save us (Isaiah 33:22).

“The voice of a god and not of a man!” (Acts 12:22). Herod be-
lieved the words of his subjects. He fell for the grand delusion: the
belief that those who rule are self-styled gods, independent of
God’s government over them. God did not take long to remind
King Herod and the people that He rules in heaven and earth, and
all rulers are subject to His sovereignty and law. For his denial of
God’s sovereignty over him, Herod became a diet for worms:
“And immediately an angel of the Lord struck him because he did
not give God the glory, and he was eaten by worms and died”
{Acts 12:23).

Today, the United States finds herself in the midst of making a
similar choice. It’s true that our coins have “In God We Trust”
stamped on them. But it’s probably equally true that our nation
puts more trust in the money where the acknowledgment appears
than in the God who supplies all wealth. This nation is being
warned, just as Israel was warned. Ged is reminding the United
States that her prosperity and security do not come by way of the
State. As Christians, we must heed God’s warning that a forgetful
nation is a doomed nation. Have we come to the place where we
now believe that “My power and the strength of my hand made
me this wealth” (Deuteronomy 8:17)? God’s assessment of such
presumption is not easy to take for an unrepentant nation:

it
