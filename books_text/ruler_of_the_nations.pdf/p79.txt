God Judges the Nations 59

tivism, planning, and the denial of personal responsibility before
God and man. On the other hand, there has been a tendency
among Bible-believing Christians to overemphasize the case for
individualism because of their commitment to personal salvation
at the expense of social and institutional transformation and heal-
ing. They want to restrict the limits of God’s salvation because
they do not want the responsibility of applying God’s law to soci-
ety. Thus, they have denied the rule of Gods law outside Chris-
tian churches and Christian families.

They have been consistent in this anti-covenantal position by
also denying that God blesses and curses nations, and other col-
lectives, with the exception (inconsistently) of churches and fami-
lies. To some degree, they still recognize the covenant in churches
and families, and so they recognize God’s sovereignty, His institu-
tional authority and hierarchy, His law, His judgment, and His
continuity of relationships over time. But by denying that God
rules over national and cultural collectives, modern Christians
have denied the covenant. This has left humanists in control of
politics, education, the media, and just about everything else.

There is no doubt that each person stands alone on judgment
day. He is not judged by a committee (unless we mean the Trin-
ity), nor do other human committee members stand at his side
and take some of the blame. This accurate vision of final judg-
ment has led Christians to a false conclusion: Since God judges
individuals as individuals outside of history, He therefore judges
only individuals inside history. But the testimony of the Old Tes-
tament is very different: It describes the judgments of God against
nations and collectives far more often than His judgments against
individuals.

New Testament Judgments

It could be argued that in the New Testament, God’s relation-
ship to men has changed. Now He judges only individuals. But
then how can we make sense out of the fall of the Roman Empire?
Daniel prophesied that God’s kingdom would crush the fourth
empire, Rome.
