Neutrality Is a Myth 119

When Jesus kept silent regarding Pilate’s question concerning
His origin (John 19:9), Pilate grew indignant: “You do not speak
to me? Do you not know that I have authority to release You, and
I have authority to crucify You?” (v. 10). Jesus’ answer settled the
matter about the operation of God’s kingdom. Unless the king-
dom of God operated i and over this world, what Jesus next said
would be false: “You would have no authority over Me, unless zt
had been given you from above. . .” (v. 11).

Looking for Political Solutions

Confusion over Jesus’ words develops from a false notion that
the answer to man’s problems is solely political. There were nu-
merous occasions when the crowds wanted to make Him King
(e.g., John 6:15). While there are political implications to Jesus’
kingship, just as there are personal, familial, economic, business,
ecclesiastical, and judicial implications, the kingdom of God cannot be
brought about politically. Good laws do not make good people. They
can at best prepare people to become good people by restraining out-
ward evil. Only the sovereign work of the Holy Spirit in regenera-
tion makes people good. The State has a God-imposed jurisdic-
tion to perform kingdom activities related to civil affairs according
to the specifics of God’s Word.

The people in Jesus’ day saw the kingdom of God in externals
only. They visualized the kingdom of God as coming, not through
regeneration, but revolution. Jesus said of His followers: “Truly,
truly, I say to you, you seek Me, not because you saw signs, but
because you ate of the loaves, and were filled” ( John 6:26). It was
Jesus’ message about mankind’s need for salvation and about Him
as the Savior, the Messiah of God, that caused the religious and
political establishments of the day to seek His death.

The kingdom of God never advances through political in-
trigue, backed by military power, Though power-directed, its
power comes from above and works on and in the heart of man: “I
will give you a new heart and put a new spirit within you; and I
will remove the heart of stone from your flesh and give you a heart
of flesh. And I will put My Spirit within you and cause you to
