We Must Render Appropriate Service 105

The apostles’ obedience to God conflicted with the laws of the
State. This resulted in the first apostolic death: “Now about that
time Herod the king [Agrippa I] laid hands on some who belonged
to the church, in order to mistreat them. And he had James the
brother of John put to death” (Acts 12:1-2). Peter was later ar-
rested for similar “crimes” against the State (v. 3). God, at least,
does not show His disapproval of rebellion against tyrants in these
specific cases. He even sent one of His angels to release Peter from
prison (vv. 6-8). There are several such cases where divine assis-
tance released outspoken Christians from the hands of the State.

Thus, there can be no question of the legality of resistance to
evil civil magistrates. But the Bible always specifies that such re-
sistance is not to be autonomous (self-law), but rather based on
God’s call through another lawful authority, such as a local
church, a local civil magistrate, or parents. This is a now unfamil-
iar doctrine of the Protestant Reformation called “the doctrine of
interposition.” John Calvin articulated it in his Institutes of the
Christian Religion (Book IV, Chapter 20, Sections 22-32).! It also is
one of the legal justifications for the American Revolution.?

Praying for Civil Servants

Our rulers need the prayers of Christians. First, to give them
support for the difficult tasks that surely burden them. The work
of the civil magistrate is multi-faceted. There are constant pres-
sures that weigh heavily on the office of each civil representative.
A minister in the civil sphere must keep his own house in order as
well as the house of State. Family responsibilities are often
neglected for the supposed urgency of civil affairs. There is the
constant barrage of special interest groups wanting to turn the
civil sphere of government into a vehicle to engineer society
through power and coercion. The temptation to appease these
groups is great.

1. Michael Gilstrap, “John Calvin's Theology of Resistance,” Christianity and
Civilization 2 (1983): Symposium on “The Theology of Christian Resistance,” pp.
180-217.

2. Tom Rose, “On Reconstruction and the American Republic,” idid., pp-
285-310.
