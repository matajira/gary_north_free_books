14 Ruler of the Nations

people have in mind, obliterating by definition the legitimate gov-
ernments of family, church, and the various local civil govern-
ments like the city, county, and the many states.

Noah Webster

Let’s briefly look at some older definitions of the word to see
that government meant more than the State, that is, civil govern-
ment at the national level. The dictionary is always a good place
to begin. Noah Webster’s An American Dictionary of the English Lan-
guage (first published in 1828) defines government this way:

GOVERNMENT, «. Direction; regulation. “These precepts will
serve for the government of our conduct.”

2. Control; restraint. “Men are apt to neglect the government of
their temper and passions.”

3. The exercise of authority; direction and restraint exercised
over the actions of men in communities, societies or states; the
administration of public affairs, according to the established con-
stitution, Jaws and usages, or by arbitrary edict. “Prussia rose to
importance under the government of Frederick II.”

4. The exercise of authority by a parent or householder. “Chil-
dren are often ruined by a neglect of government in parents. Let
family government be like that of our heavenly Father, mild, gentle
and affectionate.”

Did you notice the emphasis here? All human government
begins with the individual. We've been calling this self-government
or self-control (Galatians 6:23; Acts 24:25). Self-government un-
dergirds all institutional governments, including the mothers and
fathers in family government, elders in church government, and civil
servants at all jurisdictional levels in civil government. Webster
does not define government as the State and only the State.

Where did these early definers of government get these funda-
mental ideas? From the Bible. When the Bible speaks of “govern-
ment” in the singular, it refers to the government of Jesus Christ
that encompasses all other governments:

For a child will be born to us, a son will be given to us; and the
government will rest on His shoulders; and His name will be
