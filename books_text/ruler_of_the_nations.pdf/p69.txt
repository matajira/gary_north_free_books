Plural Law Systems, Plural Gods 49

Those who advocate natural law see it as equal to Biblical law.
We are told that we need “both” standards. Mrs. Marshner
continues:

If order is to be restored to society, and the underpinnings:of
freedom preserved, America must return to non-consequential
ethics. What would be the basis for such ethics? Some say the sole
possible basis is revealed religion. This is a mistake. First of all, the
Bible and other revealed documents do not answer explicitly all the ethical
questions that arise... >

Tf revealed religion (the Bible) is not the “sole possible basis”
for decision-making, then what is? The Bible says “no one can
serve two masters” (Matthew 6:24). The apostle Paul writes that
Christians are not to be unequally yoked with unbelievers (2
Corinthians 6:14). How then can Christians be unequally yoked
with unbelieving ethical systems? “For what partnership have
righteousness and lawlessness, or what fellowship has light with
darkness? Or what harmony has Christ with Belial, or what has a
believer in common with an unbeliever?” (vv. 14, 15)

The premise of these natural law advocates is that the Bible
cannot be used as an ethical handbook to deal with issues like
“genetic screening, nuclear weapons systems, or in vitro fertiliza-
tion” because “the scriptures speak only by interpretation.”* But
can’t the same thing be said about every ethical system? There’s
always interpretation. At least with the Bible, we know that our
starting point is reliable.

Natural law is flawed from its inception because it assumes
that “nature” (creation) is not fallen, that man’s reasoning abilities
are not distorted due to the Fall, and that ethics is based on “phi-
losophy” and not “religious precepts.” Again, Mrs. Marshner has
little use for the Bible: “Citizens must defend their rights, and
they must defend them intellectually, by employing and invoking
an objective system of right and wrong. This new traditional sys-
tem of ethics is based on philosophical precepts, not on religious precepts,

3. Ibid., p. 128. Emphasis added.
4. Ibid., p. 128.
