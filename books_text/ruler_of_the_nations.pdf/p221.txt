Reconstructing Family Government 201

alternatives on a $70,000, 13 percent fixed-rate mortgage:?

LOAN TYPE/ REGULAR EFFECTIVE TOTAL
PAYMENT PAYMENTS TERM INTEREST
FREQUENCY DUE PAID
30-year/monthly $774 30 years $208,762
30-year/biweekly? $387 18 years 111,970
15-year/monthly $886 15 years 89,421

Here are some advantages to a “quick-pay loan”:

1, A young couple with, say, a 15-year loan will create
enough value to refinance the house, when their children reach
18, and pay for college.

2. Homeowners who quick-pay will find it easier to trade up
to a better house, because they'll get more cash when the old
house is sold.

3. Only a quick-pay loan can guarantee that a middle-aged
couple will have a paid-up house when they retire.

4, Extra savings put into your house instead of the bank will
build up tax-deferred.

Be aware of legislation directed at the family. There are laws
on the books where your underage daughter can get an abortion
without your consent. Know the law and work for its defeat. Read
your state’s laws concerning parents’ rights. A number of parents,
or a group of parents, could hire a lawyer or ask someone to speak
to you about what the law says. Under no circumstances should
you “volunteer” information to state or local officials who are sent
to your home to “investigate.” Refer them to your lawyer, either
retained by you or a group of concerned parents. Lobby your leg-
islators on crucial family issues. Read John W. Whitehead’s Par-

9. Jane Bryant Quinn, “The Rise of Quick-Pay Mortgages” Newsweek
(December, 10, 1984), p. 72.

10, “Your payments are figured as if the Ioan were going to last for 30 years.
But instead of paying once a month, you make one-half of the monthly payment
every two weeks. Over the year, that equals 13 monthly payments instead of 12, a
small change that brings surprisingly big savings in interest costs” ( Jane Bryant
Quinn, “The Rise of Quick-Pay Mortgages,” Newsweek, December 10, 1984, p. 72).

Ui, ibid.
