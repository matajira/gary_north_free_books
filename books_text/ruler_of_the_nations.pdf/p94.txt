74 Ruler of the Nations

in [Samuel’s ways], but turned aside after dishonest gain and took
bribes and perverted justice” (f Samuel 8:3). Even the church, the
priesthood, was corrupt: “Now the sons of Eli [who were priests]
were worthless men; they did not know the Lorp and the custom
of the priests with the people” (1 Samuel 2:12, 13).

The corruption of the individual resulted in the corruption of
family, church, and State. The nation rejected God’s prescription
for such wholesale corruption repentance! Instead, they turned
to Samuel and demanded a “king to judge us like all the nations”
(i Samuel 8:5). In making this demand, God told Samuel: “Listen
to the voice of the people in regard to all that they say to you, for
they have not rejected you, but they have rejected Me from being
king over them” (1 Samuel 8:7). Their choice of security by re-
penting and turning to God as their Savior, Lord, and Provider
was rejected. Instead, they turned to the false security offered to
them by a civil government that would subject them to slavery in
the name of security.

God told Samuel to warn them of the judgment they could ex-
pect. Each of the following kingly provisions is an idolatrous sub-
stitute for God's faithful provision to His people:

1. The king would raise an army for his purposes in opposi-
tion to the law (Deuteronomy 20). Samuel told the people that
war would be a way of life in Israel: “He will take your sons and
place them for himself in his chariots and among his horsemen
and they will run before his chariots” (1 Samuel 8:11). Instead of
appearing before the Lorn “three times a year” as part of God’s
army (Exodus 23:17), the men would appear before this king “like
ali the nations” (1 Samuel 8:5).

2. The king would use this army for personal profit: “He will
appoint for himself commanders of thousands and of fifties, and
some to do his plowing and to reap his harvest and to make his
weapons of war and equipment for his chariots” (1 Samuel 8:12).
What was designed to benefit the nation in the organization of
God’s host (Exodus 18:21}, would alone benefit Saul.

3. The young women of Israel would be subject to the whims
and fancies of the king: “He will take your daughters for per-
