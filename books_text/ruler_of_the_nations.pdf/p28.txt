8 Ruler of the Nations

If we see these characteristics in civil government, we can
most assuredly conclude that they first manifested themselves in
self-government. The all-embracing State feeds on poor self-gov-
ernment. Statists are assured that those who lack self-government
will elect leaders who will promise them relief from their own sin.
Of course, relief never comes, only tyranny.

Moral Criteria for Leadership

Those who cannot govern themselves cannot be expected to
govern others. There is a direct relationship between poor self-
government and family and church governments. For example, a
ruler in the church

must be above reproach, the husband of one wife, temperate,
prudent, respectable, hospitable, able to teach, not addicted to
wine or pugnacious, but gentle, uncontentious, free from the
love of money. He must be one who manages his own household
well, keeping his children under control with all dignity (but if a
man does not know how to manage his own household, how will
he take care of the church of God?); and not a new convert, lest
he become conceited and fall into the condemnation incurred
by the devil. And he must have a good reputation with those out-
side the church, so that he may not fall into reproach and the
snare of the devil (vv. 2-7).

When moral self-government under God’s law is not operating
because the sovereign government of God is denied, and His law
is repudiated, we can expect the family and church to suffer the
inevitable wicked effects, The one human government that seems
always to be ready to pick up the pieces is civil government, meaning
the State. Once all the dominoes fall against that last domino of
civil government, those in power scoop up all the dominoes and go
home, obliterating the multiple governments under God and deny-
ing the individual and the God-ordained governments of the family
and the church. The limited, delegated sovereignty given to them by
God is denied. The new rulers assume that there is no need for self-
government undergirding the family, church, and State. In fact,
even God's sovereign rule is denied. The State rules and overrules.
