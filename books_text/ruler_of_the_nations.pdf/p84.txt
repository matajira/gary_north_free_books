64 Ruler of the Nations

for certain kinds of sinful actions. Other nations get rewards, such
as military spoils or special trading rights. Historians write well of
the victors. History therefore has meaning. It makes a difference
in history how collectives live. This difference is reflected in the
judgments of men and God in history.

Thus, we have to conclude that in some sense, God imputes
Christ’s righteousness to collectives. He certainly does this to the
church. He presumably does this with nations, If he didn’t, then
why has He judged them in history the way He judged Sodom
and Gomorrah? The blessings and cursings of Deuteronomy 28
include military victories (v. 7) or defeats (v. 25). Military events
take place to collectives. A bad general makes decisions that affect
his troops. Satan is just such a bad general, and his troops have
suffered and will suffer again. The point is, collective judgments
come in the midst of history: blessings and cursings.

Victimless Crimes

A popular argument today is that the State should not enforce
laws against pornography, prostitution, homosexuality, gambl-
ing, and even drug abuse because such acts are voluntary acts be-
tween (or among) consenting adults. So long as they do not affect
anyone who is not a party to the transaction, there should be no
laws against such acts.

What is the Biblical response? That God judges sin. Sexual sins
are seen in the Bible as sins of idolatry. The whole Book of Hosea
is built around this theme. There is no such thing as a victimless
crime. If society allows rampant public sin to go on without sanc-
tions against it, it (a collective) has thereby sanctioned these sins. Its
members have collectively covenantally declared: “We are not con-
cerned about God’s righteous requirements for us as individuals;
we will continue in our sins.”

The result, as venereal disease testifies throughout history, is
the judgment of God in history. In our day, AIDS threatens to be-
come God’s judgment against a society that has neglected His
laws regarding homosexuality. Because God sees and hears, and
because sins “cry out to God” in history, there can be no such thing as a
