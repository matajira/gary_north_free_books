50 Ruler of the Nations

and can be understood and accepted by anyone willing to master the intellec-
tual rigors of it."> Who will determine what an “objective system of
right and wrong” will be? What if the precepts of the Bible are re-
jected by those “willing to master the intellectual rigors” of these
“philosophical precepts”? If history is any indicator, the Bible will
be rejected in favor of self-serving, humanistic natural law.
Religion is an enemy to natural law. Stalin’s words should silence
all who appeal to the supposed inherent reasonableness of natural
law: “We guarantee the right of every citizen to combat by argu-
ment, propaganda and agitation, any and all religion. The Com-
munist Party cannot be neutral toward religion. It stands for
science, and all religion is opposed to science.”6 So much for ob-
jectivity, “non-consequential ethics,” and “philosophical precepts.”

One last nail needs to be driven into the coffin of natural law.
If natural law is adequate as a basis for ethical standards, why
were the Israelites required to hear the revealed law read at least
once every seven years? Gary North writes:

Human reason, unaided by God’s revelation untwisted by
God's grace—cannot be expected to devise a universal law code
based on any presumed universal human logic. What mankind's
universal reason can be expected to do is to rebel against God and
His law. Ethical rebels are not logically faithful te God.

Biblical law had to be read to everyone in Israel at least once
every seven years (Deut. 31:10-13). God presumed that men
would not understand His law unless they heard it (Deut. 31:13).
Every resident of the land had to listen to this law. If the whole of
biblical law had been ingrained into the consciences of all men
from the beginning of time, or if the terms of the law, including
the law's explicit penalties, were always available to all men
through the exercise of man’s universal reason, then the law
would not have required the priests to read the Mosaic law before
the congregation of Israel every seventh year.”

5. Ibid., p. 182. Emphasis added.

6. “Declaration to American Labor Delegation,” Moscow, September 7, 1927.

7. Moses and Pharaoh: Dominion Religion Versus Power Religion (Tyler, Texas:
Institute for Christian Economics, 1985), p. 236.
