XX Ruler of the Nations

obligations. Christians should read this section with a mind to-
ward application.

The creeping statism of our society will never be stopped until
Christians are ready to obey God’s mandate and take responsibil-
ity for the duties He has assigned. This book could prod many in
the right direction.
