Reconstructing Civil Government 209

laws which directly address the civil magistrate should be enacted
into law: protecting the unborn; the orphan, widow and stranger;
maintaining just weights and measures (no fiat paper money); en-
forcing judicial laws regarding restitution and capital punish-
ment; protecting the nation’s borders; and establishing a tax rate
that does not exceed 10 per cent for any citizen.

The purpose of Biblical civil government is not to create a perfect soctety
but rather to create legal conditions for the voluntary establishment of a mar-
ketplace, free of tyranny. This is one of the reasons Paul urges us to
pray “for kings and ail who are in authority, in order that we may
lead a tranquil and quiet life in all godliness and dignity”
(1 Timothy 2:1, 2).

What is left if we cannot go to the Bible for our laws? By what
other standard should governments be ruled? Will we be ruled by
God or men? This is always the choice facing men. As Elijah said
on Mt. Carmel to the people, choose this day whom you will
serve, Baal or God. As usual, the people answered not a word
(1 Kings 18:21) until after they saw whose God was more powerful,
Elijah’s God or the god of the false prophets.

Gad or Man?

The NBC television network in 1986 presented a drama about
the gripping and courageous story of Raoul Wallenberg and his
attempts to save European Jews from their Nazi tormentor, Col-
onel Adolf Eichmann. Wallenberg’s efforts may have made the
difference between life and death for nearly 120,000 Hungarian
Jews.

During the course of the story, when the viewer is confronted
by a scene of Jews being loaded into trucks for shipment to a con-
centration camp, a Jewish teenager turns to a rabbi and confronts
him with what he perceives to be an unanswerable question:
“How can you still believe in God after all of this?” The rabbi does
not take long to respond: “How can you still believe in man?”

This is our dilemma today. If we do not believe in God for our
laws, then man is all that is left. Such a foundation leads either to
anarchy {as in Beirut, Lebanon) or totalitarianism (the Soviet
