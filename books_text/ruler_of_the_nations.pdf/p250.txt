230 Ruler of the Nations

form to the decree of Congress. Because of these fears, many
states petitioned the First Congress to include a Constitutional
Amendment prohibiting the national government from funding a
single Christian denomination and favoring it with legal action.
This is why, historically, “the real object of the [Flirst Amendment
was not to countenance, much less to advance Mohammedanism
[Islara], or Judaism, or infidelity, by prostrating Christianity, but
to exclude all rivalry among Christian sects [denominations] and
to prevent any national ecclesiastical establishment which would
give to an hierarchy the exclusive patronage of the national gov-
ernment.” Such was the opinion of Chief Justice Joseph Story in
the mid-19tb century.

When the First Amendment was drafted, nine of the thirteen
states had established churches, The First Amendment was a guar-
antee to the states that the states would be able to continue what-
ever church-State relationship existed in 1791, the year the Bill of
Rights was ratified and made part of the Constitution. Maryland,
Virginia, North Carolina, South Carolina, and Georgia all
shared Anglicanism as the established religion. Congregational-+
ism was the established religion in Massachusetts, New Hamp-
shire, and Connecticut. New York, while not having an estab-
lished church, allowed for the establishment of Protestant relig-
ions, Only in Rhode Island and Virginia were all religious sects
disestablished. But the Christian religion was the foundation of all
of the states. Their social, civil, and political institutions were
based on the Bible. Not even Rhode Island and Virginia renounced
Christianity, and both states continued to respect and acknowl-
edge the Christian religion in their systems of law.

Congressman James Madison, the chief author of the First
Amendment, informed his Congressional colleagues that he was
responding to the desires of the various state Conventions to pro-
hibit establishment of a national religion where one religious “sect
might obtain a pre-eminence” over others.

As legal scholars point out, the critical word in the First
Amendment's religion clauses is “respecting.” “Congress shall
make no law respecting an establishment . . . .” “Respecting” is
