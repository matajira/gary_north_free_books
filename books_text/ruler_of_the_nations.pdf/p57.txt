A Bottom-Up Hierarchy 37

Summary

The second basic principle in the Biblical blueprint for gov-
ernment is that God has established a hierarchical yet decentral-
ized political order where neither the individual nor the group is
ultimately sovereign. There are checks and balances in order to
preserve liberty, authority, and stability. No single human institu-
tion or branch of civil government is absolutely sovereign. Only
God is absolutely sovereign.

Authority and power to govern are delegated by God, thus, those
who govern are obligated to govern according to God’s law, for
they are ministers of God (Romans 13:1-7). There really is no other
option, When men cease to believe in God, they do not end up be-
lieving in nothing, but what is worse, they believe in anything,
however absurd or sinister. They substitute another god to fill the
vacuum. Some political system will prevail, and it will be directed
by some god.

Throughout history, as a nation moves away from God as
Governor of all of life, the State claims for itself more and more
power. All competing governments are removed—usually by
force, always by intimidation. In time, the people suffer under the
weight of unbridled oppression. What was once a promise of se-
curity by those centralizing authority and power, becomes a chok-
ing tyranny. The oppression continues and turns the people either
into slaves or rebels. The solution to the plight of tyranny is not to
place authority and power in the hands of the individual. Nor is
the solution found in the ruling abilities of a self-appointed elite.

Centralization was the prevailing political structure in the his-
tory of the world. Egypt used the people to construct a society, us-
ing the energies of the masses to implement the goals of the State,
governed by the Pharaohs. The pyramid, the tower, and
Nebuchdnezzar’s colossus are visible manifestations of centralized
political planning. God works to disestablish centralized political
regimes. His blueprint for life is decentralization, with the indi-
vidual working and having freedom in the family, church, busi-
ness, and civil government. No one man or institution is to rule
over all other aspects of society. The way to a just order is not
