42 Ruler of the Nations

appointed by Moses to share in the task of settling disputes also were
required to judge according to the law of God (wv. 19-20). The people
as a whole must be careful not to add to the law of God or take away
from it (Deuteronomy 4:2). This indicates that God’s law must be
used as it is and not mixed with the changing laws of men. Men
often are tempted to adapt God’s commandments to fit the times.
Thus the judgment of man is deemed superior to that of God, and
the commandments of God often are nullified (cf. Mark 7:13).

The Levitical priests as well as the judges of Israel were bound
to follow the law. The people of Israel also were instructed to heed
the judges’ verdict because the decision was based upon the un-
changing law of God and not upon the decision of men: “Accord-
ing to the terms of the law which they teach you, and according to
the verdict which they tell you, you shall do; you shall not turn
aside from the word which they declare to you, to the right or the
left” (Deuteronomy 17:11).

Reforming Society

When Jehoshaphat initiated reforms, one of the first areas of
reformation was the judicial system: “And he appointed judges in
the land in all the fortified cities of Judah, city by city. And he said
to the judges, ‘Consider what you are doing, for you do not judge
for man but for the Lorp who is with you when you render judg-
ment, Now then let the fear of the Lorp be upon you; be very
careful what you do, for the Lorp our God will have no part in
unrighteousness, or partiality, or the taking of a bribe’” (2 Chron-
icles 19:5-7). Reformation came from the top and bottom. Judges’
decisions must not issue from political or economic pressures or
calculations, or any merely human purposes, but from obedience
to the Lord of heaven, earth, and history, and to the righteous
laws He has revealed to us in Scripture.

The prophets of Israel continued to warn the nation that only
the law of God serves as a legal system’s foundation: “To the law
and to the testimony! If they do not speak according to this word,
it is because they have no dawn” (Isaiah 8:20). Any attempt to
repudiate the law of God as a standard for righteousness estab-
