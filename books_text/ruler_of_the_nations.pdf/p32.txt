12 Ruler of the Nations

But you shall remember the Lorp your God, for it is He who
is giving you power to make wealth, that He may confirm His
covenant which He swore to your fathers, as it is this day. And it
shall come about if you ever forget the Lorp your God, and go
after other gods and serve them and worship them, I testify
against you today that you shall surely perish. Like the nations
that the Lorn makes to perish before you, so you shall perish;
because you would not listen to the voice of the Lorp your God
(Deuteronomy 8:18-20).

Herod saw himself as a god. But what is even more frighten-
ing, is that the people accepted him as a god. It is no less true to-
day that people reject the God of the Bible and His faithful provi-
sions of life, liberty, and property and turn to the State for suste-
nance and security. Where we are told to pray, “Our Father who
art in heaven. . . . Give us this day our daily bread” (Matthew
6:9, 11), we too often turn to the State for our daily bread. Where
the Bible tells us that God is our Father, more often than not the
people make the State their father, because the State can provide
them all the financial aid they need. (See Chapter Five for more
details.)

This is why the first commandment must be our starting point
for the proper ordering of ourselves, our families, our churches,
and our nation. We must never put any of man’s laws before the
first commandment: “I am the Lorp your God. . . you shall
have no other gods before Me” (Exodus 20:2, 3), Adherence to the
first commandment protects us from those who would rewrite it to
read “I, the State, am your God. You shall have no other gods be-
fore me.” This is the first point in the Biblical covenant structure:
the absolute sovereignty of a transcendent God who is always present (imma-
nent) with His people. He sets up a hierarchy (point two), lays down
the law (point three), judges men continually and also at the end
of time (point four), and preserves His kingdom (point five).

When a ruler decrees either by words or deeds that he is inde-
pendent of God’s government, or that justice is defined according
to his self-made laws, or that man looks to the State for salvation,
then God responds with judgment. A nation might not see God’s
