34 Ruler of the Nations

gated and limited. The delegation of authority is purposely
multiplied. This hampers the sinful tendency toward tyranny of
any one governmental institution. For example, the authority of
the State is not the same as that of a father who exercises authority
as head of his household.

While each government should follow guidelines laid down in
Scripture for its particular area of jurisdiction, not all Biblical
laws apply to each in the same way. For example, the symbol of
family authority is the rod (Proverbs 13:24), while the symbol of
church authority is the keys (Matthew 16:19), and for the State,
the sword (Romans 13:4).

Laws found in Scripture give parents the authority to exercise
discipline over a child with a rod, but not over an erring brother
in Christ. This authority belongs to the church. The church can
discipline a member, but cannot use the rod or wield the sword as
punishment. The church has the power of the “keys” to bar unre-
pentant members from the Lord’s table and finally to excommuni-
cate them if they remain unrepentant (Matthew 18:15-20).

The State has authority to wield the sword as punishment for
capital offenses, but it cannot use its authority to influence the in-
ner workings of the family or church. These “institutions” are out-
side its jurisdiction. If, however, a family member commits a
crime, the State may have jurisdiction unless voluntary restitution
is made.

These various institutions must often defer judgment to the
more legitimate area of jurisdiction. Jesus’ words in Luke
20:22-25 establish the authority of the State and also set the
boundaries for its civil jurisdiction. Men asked Him, “‘Is it lawful
for us to pay taxes to Caesar, or not?” But He detected their
trickery and said to them, ‘Show me a denarius. Whose head and
inscription does it have?’ And they said, ‘Caesar’s. And He said to
them, ‘Then render to Caesar the things that are Caesar’s, and to
God the things that are God’s”’”

When the church disciplines a local church member over an
ecclesiastical matter, the State cannot rightly be approached to use
its authority to override what the disciplined member might con-
