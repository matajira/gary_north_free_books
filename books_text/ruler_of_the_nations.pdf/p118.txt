98 Ruler of the Nations

because the heads of foreign powers are His servants. For exam-
ple, Pharaoh (Romans 9:17), Herod, and Pilate (Acts 4:25) were
raised up by God to do God's will. The psalmist says that God
“puts down one, and exalts another” (Psalm 75:7). God’s dealings
with Nebuchadnezzar surely are the most revealing actions of sov-
ereignty brought upon an earthly ruler. Daniel acknowledges the
sovereignty of God in the appointment and removal of kings by
stating that God “changes the times and the epochs; He removes
kings and establishes kings” (Daniel 2:21a).

The rule and authority that men in power enjoy come from
the gracious hand of God: “The Most High is ruler over the realm
of mankind, and bestows it on whom He wishes, and sets over it
the lowliest of men” (Daniel 4:17). Nebuchadnezzar was reminded
of his rightful position as a ruler under God (4:25, 32). When the
king comes to his senses, God returns the kingdom to Nebuchad-
nezzar: “So I was reestablished in my sovereignty, and surpassing
greatness was added to me” (4:36). This great lesson was not re-
membered, however. Some years later Belshazzar’s mockery of
God’s rule (cf. 5:2-4) brought sudden destruction, but not before
Daniel reminded him of the nature of his sovereignty: “O king,
the Most High God granted sovereignty, grandeur, glory, and majesty
to Nebuchadnezzar your father” (5:16). Belshazzar’s kingdom was
“numbered,” “weighed,” “divided,” and “given” by Ged (vv. 25-28).

Summary

The sixth basic principle in the Biblical blueprint for civil gov-
ernment is that the lordship of Jesus Christ is universal. There are
no exemptions from God’s service. There is no “King’s X” for
human kings.

One of the greatest lies ever fostered in the church is that Jesus
is King of the church but not of the State. The law of God is valid
for individual believers, but non-Christians are supposedly not re-
quired to keep it. The nations are supposedly under their own jur-
isdiction—an assertion of their autonomy (self-law). The nations
supposedly do not have to keep the law of God unless they wish
to. God supposedly does not hold them accountable. Suppose,
