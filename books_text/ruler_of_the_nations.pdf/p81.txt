God Judges the Nations 61

groups under His laws. It is an argument in favor of God’s law
over the actions of individuals, but not over collectives. It is an ar-
gument against Biblical law: social (institutional), economic, cul-
tural, and civil.

This sort of argument is not simply implicit in our day; it has
become explicit. Christian intellectuals, especially in Christian
colleges, repeat endlessly that the Bible offers no blueprints for so-
ciety. Christians are regenerated, they say, but God has given His
people no revealed standards or appropriate sanctions in the
Bible. But what about the Old Testament standards and sanc-
tions? These are no longer valid, the antinomians say.

Modern fundamentalists until very recently have argued the
same way. Because of this, they long ago abdicated their role as
God's representatives and ambassadors, and turned over the
Christian colleges, Christian magazines, and other institutions to
liberal intellectuals who have spent their academic lives defending
the legal autonomy of today’s covenant-breaking pagan antino-
tmians, who are in fact tyrants. The pagans have set the terms of
debate in every field, and Christian antinomians have agreed with
the pagans’ starting point: The God of the Bible is irrelevant to
the terms of discourse. A God who does not judge in history ac-
cording to His law is irrelevant to history.

Sanctification

Moral regeneration is a fact of conversion. But how is it to be
understood? We know that Christ died for the sins of all mankind,
because only His death satisfied God’s covenantal stipulations
governing man. History went forward after Adam’s sin only
because God looked forward in time to the cross, and then He im-
puted this perfection of Christ’s humanity, as well as His death
and resurrection, back to all mankind. He does not bring all men
to saving grace, but He certainly gives all men gifts that they do
not deserve on their own merits. He brings rain and sunshine on
all mankind (Matthew 5:45). He does this only because His wrath
has been placated by Christ's work on the cross. Thus, God is “the
