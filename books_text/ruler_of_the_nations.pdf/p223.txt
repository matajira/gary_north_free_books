13
RECONSTRUCTING CIVIL GOVERNMENT

What will it take to reconstruct our society? What rival faith
offers us a legitimate hope in long-term dominion? None.

How about the political faith of the two major political parties
in the United States? Should we join with them? The liberal end
of the political spectrum promises a just society but only with
more of your tax dollars, fewer freedoms, mediocrity in nearly
everything, and more political control. The conservatives have
their own brand of false hopes, believing that justice finds its wis-
dom in common sense, that natural law is a handy substitute for
Biblical law, and that progress in a free market society is inevita-
ble. For all of them, politics is the answer. “Vote for me and I'll see
to it that all will be right with you and the world,” goes the poli-
tical promise. This is the political faith.

The Bible calls us to faith, but a faith that takes us to the God
who created heaven and earth, to the God who sent His Son Jesus
Christ to make the restoration of a fallen world a reality. This
same faith will be a beacon to the nations, a light to the world that
lies in darkness. This is God’s purpose for His people, that they
might be a “city on a hill.” Not a light for the political faith, but a
light that shines bright on the redemptive work of Jesus Christ, the only
hope for the world. Where is the source of the light found? In the
Bible.

The reconstruction of civil government begins with the Bible.
Jesus wants us to return to the standards of Gad’s law so the whole
world will marvel and follow. First, to show men everywhere that
they are sinners and are in need of redemption. Second, to set

203
