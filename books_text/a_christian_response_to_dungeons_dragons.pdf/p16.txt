12 A Christian Response to Dungeons and Dragons

simply that leisure, “time out” from work, is an important and God-
ordained part of the life of God’s people.

By pushing this theme back to the beginning of Biblical history,
we come to a fuller understanding of the nature of the Sabbath rest.
While Deuteronomy roots the Sabbath in the Exodus, the book of Exodus
(Ex. 20:4), roots it in creation: Israel is commanded to keep the
Sabbath because the Lord Himself rested on the seventh day of crea-
tion week (Ex. 20.4). Thus, the leisure of God’s people is to image the
“leisure” of God. Significantly, God’s rest follows His evaluation of
creation as “very good” (Gen. 1:31). From this we can infer that God's
seventh-day rest was not a mere cessation from labor, but a positive
enjoyment of the “very good” things He had made. This is confirmed
by Prowrbs (Prov. 8:30-31), where God’s Wisdom is pictured as a
craftsman delighting in the world He has made, and especially in man
(Prov. 8:30-31). Man’s rest, in imitation of God, is to include this
aspect of enjoyment of creation. Again, this is emphasized in Genesis
(Gen. 2:9), where we are told that the trees were “pleasing to the eye
and good for food (Gen. 2:9).” Man was made not only to rule
creation, but also to enjoy God’s bounty in it (Eccles. 2:24-26; 3:12-14;
5:18-20). Indeed, the Bible considers it a curse for a man to work
without being able to enjoy the fruits of his labor (cf. Deut. 28:30; Is.
65:22). Freed from the crushing burden of guilt and sin, the believer
is able fully to enjoy God’s world.

Partying, laughter, and play are also included in the Biblical
understanding of leisure. After the Exodus, Moses and Miriam led
Israel in singing and dancing; David danced on the return of the ark
to Jerusalem; Jeremiah compared Israel’s return from exile to a virgin
dancing and playing a tambourine and to a lavish banquet (Jer. 31:4,
10-14), These are all “religious” celebrations that nonetheless involve
what older translations call “merrymaking.” “Non-religious” play is
also presented in Scripture as a blessing. The prophets, for example,
predicted that in the Messianic age, children would play in the streets
and at the nest of vipers (Zech. 8:5; Is. 11:7). The sight and sound of
children safely playing in the streets is a sign of a well-ordered city.

But of course, our leisure must be subject to God’s Law. Play is
good, but the “play” of the Israelites at Sinai brought God’s curse
upon them (Ex. 32:6). Laughter is God’s gift, but the laughter of Sarah
arose from unbelief (Gen. 18:12ff). Parties are good, but the merriment
of the rich man was foolish (Lk. 12:13-21). Moreover, leisure is never
to dominate our lives; there is a time for everything under the sun. The
modern idea that one works in order to have the wherewithal to play
