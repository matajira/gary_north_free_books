The Catechism of the New Age ll

power apart from God’s ordained means, and thus involve a thorough
rejection of God’s authority, Witchcraft is associated in the Scripture
with rebellion, idolatry, and adultery.

In defending FRPs, many people have pointed out the obvious fact
that most fairy tales (The Brothers Grimm, J.R.R. Tolkien, or C.S.
Lewis for example) are full of witches, goblins, and sorcer-
ers. But the heroes of these stories aren’t the witches and sorcer-
ers, Only the evil witch can change the prince into a frog. The prince
himself uses entirely mortal means. Sorcery is always a temptation in
fairy tales. As Lewis’ and Tolkien’s mentor, G.K. Chesterton, pointed
out, the genius of fairy tales lies in the fact that the hero is a normal
person in an abnormal world, an innocent among ravaging nether
beasts. Make the hero abnormal and you destroy the tension and
interest (not to mention the moral focus) of the entire narrative.
There’s all the difference in the world between Hansel and a dwarf
cleric who casts spells. Both may meet a witch, but they react differ-
ently. The dwarf covets the witch's power; Hansel just wants a chance
to shove her into the oven.

How Should We Then Play?

We know, we know. This whole thing is just another way that
Christians have found to spoil everyone’s fun. We can’t sleep at night
if we know someone is having a good time. Because this caricature of
Christianity is so commonplace, it is necessary for us to present the
outlines of a Biblical approach to play in general and role-playing in
particular.

Leisure is onc of the chief blessings of God’s covenant mercy. God
delivers His people that they may rest. This is a central theme of Exodus.
The book begins with Israel in slavery. Moses appears before Pharaoh
to demand that the Israclites be permitted to celebrate a festal sacrifice
(5:3). Instead, Pharaoh charges the Israelites with laziness and in-
creases their burden (5:6-9, 17-18). After the Exodus, Israel observes
a weckly Sabbath (16:23). Thus, in the recitation of the law in Deuter-
onomy, the Sabbath is a reminder of Israel’s slavery (Deut. 5:15).
Israel cannot deny rest to aliens because to do so would be to imitate
Pharaoh and, as it were, to turn back the clock on redemptive history.
It is for this reason that the Sabbath takes on such central importance
in later Israelite history. ‘Io neglect the Sabbath is to revert to a
bondage where rest and leisure are denied. The Sabbath, of course, is
centrally a religious rest and a time of worship. The point here is

 
