A CHRISTIAN RESPONSE TO
DUNGEONS AND DRAGONS
The Catechism of the New Age

Parents are concerned.

And well we should be. Our children are growing up in a very
hazardous world. Not only are they forced to pick their way through
a complex maze of conflicting values at school, in the neighborhood,
and out in the marketplace, but they are even being assaulted in the
“safety” of their own homes.

Across the airwaves has come a barrage of violent, irreverent,
fantastic, and occultic Saturday morning cartoons. Out of the toy box
has come a haunting phalanx of magical and monstrous macho images!
Off the bookshelf has come a frightening parade of pulp and pap:
comics and paperback books that exalt the basest of vices and disdain
the highest of virtues. And then, of course, there is MTV: sex, drugs,
violence, rebellion, and defilement on tap twenty-four hours a day, all
at the flick of a switch and the turn of a dial.

The seductive siren’s song of the world has begun to seep into even
the most protected of environments, And as a result, the hearts and
minds and souls of our children have become a battleground.

Amazingly though, the chief weapon used in this spiritual raid on
our children is a game—just a simple little game. It is called Dungeons
and Dragons. Even more than all the cartoons, toys, comics, books,
videos, and music, this simple little game has served to make our
children a “generation at risk.”

What is Dungeons and Dragons?

It’s Christmas morning. Brimming with excitement Junior opens
his first gift, a Basic Dungeons and Dragons Set. Inside he finds a rule
book, some graph paper, and an odd set of dice. No game board. No
little “men” to move around, He checks the wrapping paper for miss-
