30

ARGUMENTS AGAINST THE
LAW'S POLITICAL USE

“‘Theonomists’ preach and promote biblical
law's authority and wisdom, praying that citi-
Zens will be persuaded willingly to adopt God's
standards as the law of the land.”

Even when they grant that the law of God has a
general validity in the New Testament age, some
Christians nevertheless believe that it is wrong to
maintain that this validity and use of the law extend
to the political realm, They say: “The law of God
may be generally binding in personal, ecclesiastical,
and interpersonal social affairs, but it should not be
the standard for political justice and practice in the
modern world,” Since this attitude confficts directly
with the conclusions to which we have been brought
by our study of Biblical teaching regarding the law,
we need to listen to the reasons which are offered for
a negative attitude toward the political use of Gad’s
