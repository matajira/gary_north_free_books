58 By THIS STANDARD

Speaking of the moral teaching of Christ, Her-
man Ridderbos says,

It is the ‘ethics’ of obedience in the full sense of the
word... . If, therefore, the question is asked
by what Jesus’ commandments are regulated,
the ultimate answer is only this: by God's will as it
is revealed in his law. . . . Jesus’ ethical preaching
does not have a deeper ground than the law as
the revelation of God’s will to Israel, the people
of the covenant. Again and again it is the law,
and only the law, the meaning and purpose of
which is also the meaning and purpose of Jesus’
commandments.!

In the light of these things, we recall how Jesus
severely warned His followers not even to begin to
think that His coming had the effect of abrogating
even the slightest letter of the law; teaching that even
the least commandment had been annulled would
eventuate in one’s demotion in the kingdom of God
(Matt. 5:17-19). Throughout His life and teaching,
as we have seen, Jesus upheld the law’s demands in
the most exacting degree.

Moreover, Christ submitted to the law of God
even to the very point of suffering its prescribed
penalty for sin, He died the death of a criminal (Phil.
2:8), taking upon Himself the curse of the law (Gal.
3:13) and cancelling thereby the handwriting which
was against us because of the law (Col. 2:14). “He

1. The Coming of the Kingdom (Philadelphia: Presbyterian and
Reformed, 1962), pp. 290-91.
