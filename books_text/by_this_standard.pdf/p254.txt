224 BY THIS STANDARD

chosen by God in a special way, being a foreshadow-
ing of the person of Christ, etc. the law by which
this magistrate was to govern society must also have
been unique, meant only for Israel to follow. In
short, there was an extraordinary doctrine of the
office of civil magistrate in the Old Testament revela-
tion for Israel, and thus what was the moral duty for
Old Testament Jewish rulers should not be taken as
the standard for political ethics today.

‘The fallacy embodied in this line of thought is the
assumption that if two entities are in some ways
different, then they are in aif ways different. What
has been overlooked is the distinct possibility of sim:-
larity — not total identity and not complete difference,
but elements which are the sane between two things
and elements which are distinct. A tank and a sports
car are similar with respect to their running on
wheels, but they are different in their speed, power,
and appearance. Likewise, it may very well be that
Old Testament Jewish magistrates were different
from Gentile magistrates in some respects, and yet
very much like the others in further respects.

The Civil Magistrate

The Bible appears to teach that one way in which
all civil magistrates are alike — whether they are Jew-
ish or Gentile, Old Testament or New Testament—
is in the standards of justice which are laid upon them
by the Creator. God does not have a double-stand-
ard of justice. Thus, the laws which He stipulated
for Old Testament Jewish magistrates to follow are
just as applicable to pre-consummation issues of
