208 BY THIS STANDARD

are not aligned with their Reformation forefathers.
Luther and Calvin were fully in agreement that
God’s law was an instrument of civil government,
functioning to restrain crime and to promote thereby
civil order. Luther taught that

the first use of the law is to bridle the wicked.
This civil restraint is very necessary, and ap-
pointed of God, as well for public peace, as for
the preservation of all things, but especially lest
the cause of the Gospel should be hindered by
the tumult and seditions of wicked, outrageous
and proud men (Commentary at Gal. 3:19).

Calvin concurs:

The first use of the law is, by means of its fearful
denunciations and the consequent dread of pun-
ishment, to curb those who, unless forced, have
no regard for rectitude and justice. Such per-
sons are curbed, not because their mind is in-
wardly moved and affected, but because, as if a
bridle were laid upon them, they refrain their
hands from external acts, and internally check
the depravity which would otherwise petulantly
burst forth (Institutes, 2.7.10).

This continued to be the view of Reformed
thinkers through the centuries. At the time of the
Westminster Assembly, Samuel Bolton wrote:

First of all, then, my work is to show the chief
and principal ends for which the law was prom-
ulgated or given. There are two main ends to be
observed, one was political, the other theologi-
