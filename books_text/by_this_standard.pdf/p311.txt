‘CRIME AND PUNISHMENT 281

our children forever, that we may do all the words of
this law” (Deut. 29:29).

Others appeal to emotion, saying that the penal
sanctions of the Old Testament would lead to a
bloodbath in modern society. Such a consideration is
by its nature a pragmatic concern, rather than a con-
sideration for truth and justice. But more impor-
tantly, it directly contradicts the Bible's own teaching
as to what the effect would be of following God’s
penal code. Far from leading to numerous more ex-
ecutions, such a practice would make others “hear
and fear” (for example, Deut. 17:13) so that few will
commit such crimes and need to be punished. God’s
sanctions bring safety, protection, integrity, and life
to a community —not a blood bath.

Some teachers have likened the Old Testament
penal sanctions to the ceremonial laws of the Old
Testament, no longer followed in the same way as
they were previously because of the work of Christ.
However, such penalties were not ceremonial in
character, foreshadowing the person and work of the
Redeemer (for example, like the sacrificial system);
they were not redemptive in purpose or religious in
character. While the New Testament shows that the
sacrifices, temple, etc. have been laid aside, the New
Testament endorses the continuing use and author-
ity of the penal sanctions. They simply are not in the
same theological category as the ceremonial laws.

The social penalties prescribed by the Old Testa-
ment law cannot be seen as fulfilled in the death of
Christ, the excommunicating discipline of the
church, or the final judgment--for none of these
