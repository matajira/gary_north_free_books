‘THE POLITICAL IMPLICATIONS OF THE COMPREHENSIVE GOSPEL 213

He rules the world with truth and grace,
And makes the nations prove

The glories of his righteousness

And wonders of his love.

The church has sung of the “political” implica-
tions of the gospel for years now! It has sung that
earth must receive her King —a reigning Savior who
rules the world, making the nations prove His right-
eousness. And this King is interested in more than
the inward souls of men and their heavenly existence
in the future. As a Savior from sin, Christ is interested
in every aspect of life infected by sin at man’s fall. “He
comes to make his blessings flow, Far as the curse is
found.” Just because man’s social existence and his
political efforts have been cursed by sin, Christ the
King proves His righteousness in the realm of
human politics, even as he reigns over every other
department of man’s thoughts, life, and behavior.

‘The early church was well aware of the political
implications of being a Christian. To be a
“Christian’—a disciple or follower of Christ (Acts
11:26)— meant to confess Jesus Christ as Savior,
Messiah, and Lord. Christians declared that Jesus
was their Savior or soter (Greek), as we see in Acts
5:31 and 1 John 4:14 (“We have beheld and bear wit-
ness that the Father has sent the Son to be Savior of
the world”). Despite the fact that Roman coins of the
day often depicted the Emperor’s face with the in-
scription of soter (or “only Savior” in some cases), the
earliest Christians declared the name of Jesus was the
one and only name given among men whereby we
