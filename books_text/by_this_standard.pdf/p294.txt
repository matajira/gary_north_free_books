264 BY THIS STANDARD

rulers in Israel, as well as the consequence of the Old
Testament perspective on civil rulers outside of
Israel. Since civil rulers are appointed by God, since
they bear religious titles, since they are sent to be
avengers of God’s wrath, since they must punish
those who are genuine evildoers, the only proper
standard for their rule in society ~ the only proper cri-
terion of public justice — would have to be the law of
God, Those who are ordained by God must obey
His dictates, not their own. Those who are called
“ministers of God” must live up to such a title by
serving the will of God. Those who are to avenge
God’s wrath must be directed by God Himself as to
what warrants such wrath and how it should be ex-
pressed. Those who are to punish evildoers must
have a reliable standard by which to judge who is,
and who is not, an evildoer in the eyes of God.

So everything points to the obvious conclusion
that the civil magistrate, according to Romans 13:1-7
(even as in the Old Testament), is under obligation
to obey the stipulations of God’s law as they bear on
civil leadership and public justice. Within its own
literary context (especially 12:19 and 13:10), Romans
13:4 specifically teaches that God's law ought to be
the guide for the magistrate who is not to bear his
sword in vain, The law of God defines those who are
truly evildoers, and it indicates those upon whom
God's wrath must come.

What Better Standard?
Those who do not favor taking God’s law as the
ultimate standard for civil morality and public
