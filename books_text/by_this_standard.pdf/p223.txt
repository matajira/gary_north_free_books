WHAT THE LAW CAN AND SHOULD DO 193.

with a special reference to the law, and cannot
be otherwise discerned. We see the perfection
and excellence of the law in his life. God was
glorified by his obedience as a man. What a per-
fect character did he exhibit! yet it is no other
than a transcript of the law.”!

(2) The law displays the demand of God upon our
lives as men. By revealing the character of God, the
law quite naturally expresses what is required of
men if they are going to imitate their Creator. The
law's commands show how we are to be like God by
propounding the will of God for us. Before deliver-
ing the summation of the law in the Decalogue, God
spoke to Israel with these words: “Now therefore, if
you will obey my voice indeed, and keep my cove-
nant, then you shall be my own possession from
among all peoples, for all the earth is mine; and you
shall be unto me a kingdom of priests and a holy na-
tion” (Ex. 19:5-6). Obedience to the law is obedience
to the voice of the King, the Lord of the covenant,
and as such it shows us what it means to be His sub-
jects and servants. For us to pray “Thy kingdom
come,” is likewise to pray “Thy will be done on earth”
(Matt. 6:10), And God’s will is communicated by
His commandments, telling us what His holiness
means on a creaturely level (Lev. 20:7-8).

(3) The law pronounces blessing upon adherence to

L. Letters of John Newton (London: Banner of Truth Trust,
1960), p. 47.
