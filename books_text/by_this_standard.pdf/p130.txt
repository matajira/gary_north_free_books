100 BY THIS STANDARD

with a standard of moral perfection, the latter lays
stress on utter separation from ail moral impurity.
However, the norm for both is the same in Scripture.
An unrighteous man cannot be deemed holy, and an
unholy person will not be seen as righteous.

Above all God is “the Holy One” (1 John 2:20; as
applied to Christ, Mark 1:24; John 6:69; Acts 3:14;
Rev. 3:7). When He saves us and draws us to Him-
self, He makes us holy—that is, “sanctifies” us—as
well. We were chosen in Christ before the founda-
tion of the world “in order that we should be holy
and without blemish” (Eph. 1:4); from the beginning
God chose us to be saved in believing the truth and
in holiness (sanctification) produced by the Holy
Spirit (2 Thes. 2:13). By His own sacrifice and the
work of reconciliation accomplished by his death
(Heb. 10:14; Col, 1:22), Christ sanctifies the church,
aiming to present it as holy and without blemish be-
fore God (Eph. 5:26-27). It is God who makes us ho-
ly (i Thes. 5:23), especiaily through the ministry of
the Holy Spirit in us (1 Peter 1:2).

Holiness is thus an important ethical theme in
the New Testament. Believers are called by God pre-
cisely to be holy ones—that is “saints” (Rom. 1:7; 1
Cor. 1:2). Christians in a particular locality or
church are customarily designated as God’s “saints”
(Acts 9:13, 32; Rom. 15:25; 2 Cor. 1:1; Phil. 4:22);
these holy ones are those for whom the Holy Spirit
makes intercession (Rom, 8:27), to whom God
makes known His mysteries (Col. 1:26), and for
whom we are to show acts of love (Col. 1:4; Rom.
12:13; Heb. 6:10; 1 Tim. 5:10). They have been
