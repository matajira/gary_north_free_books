A CONSEQUENTIAL ETHIC ENDORSES THE LAW 81

well in saying that we would have to be deceived to
think God could be mocked. Evil living will not
bring about happiness and blessing, for then the jus-
tice and holiness of our God would be a mockery.
Rather, says Paul, “whatsoever a man soweth, that
shall he also reap” (Gal. 6:7). Those who live accord-
ing to their rebellious nature will suffer corruption,
while those who live by God’s Spirit will gain eternal
life (v. 8), And on that basis Paul exhorts believers,
‘let us not be weary in well-doing.” Why? Because
“in due season we will reap, if we faint not” (v. 9).

It is noteworthy here that Paul focuses on the
benefits which will accrue to us if we engage in well-
doing. It is not —contrary to modern-day versions of
Christian asceticism—somehow ignoble or sub-ethi-
cal for a Christian to be motivated by the thought of
reward for righteous living. God often sets before us
the prospect of divinely granted benefits as an incen-
tive for moral living.

For instance, Jesus said, “Seek ye first the
kingdom of God and its righteousness, and all these
things (daily provisions of life) shall be added unto
you” (Matt. 6:33), Paul taught that “Godliness is
profitable for all things, since it holds promise for the
present life and also for the life to come” (1 Tim.
4:8). The Old Testament prophet Malachi exhorted
God's people that if they would obey Him (here, by
bringing in their tithes), God would open the win-
dows of heaven and pour out a blessing for which
there would not be enough room to take in (Mal.
3:10), Even earlier, the great leader of the Israelites,
Moses, had written that obedience to the Lord
