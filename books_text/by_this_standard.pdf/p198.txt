168 BY THIS STANDARD.

bility for obedience.

With the giving of new light and new power in
the New Covenant, the responsibility of men to obey
the voice of God is increased. To whom much is
given much is required (Luke 12:48). God no longer
overlooks any people’s disobedience but requires all
people everywhere to repent because of His ap-
pointed Judge and Day (Acts 17:30-31). The revela-
tion of the New Covenant is even more inescapable
than that of the Old Covenant (Heb. 12:25), and to it
we should give “the more earnest heed” (Heb. 2:1-4).

Conclusion

Our study of the New Covenant scriptures has
shown us, in summary, that there are definite
discontinuities between the New Covenant relation
to the law and that of the Old Covenant. The New
Covenant surpasses the Old in glory, power, realiza-
tion, and finality. There is zo textual indication, how-
ever, that the New Covenant brings a new standard of
moral conduct, and there is no textuad indication that
the Old Covenant standard has been categorically
laid aside. The Covenantal administrations are
dramatically different~in glory, power, realization,
and finality~but not as codes defining right and
wrong behavior or attitudes.
