68 BY THIS STANDARD

us to understand the guidance given by Ged’s word,
and who makes us to grow by God’s grace into peo-
ple who better obey the Lord’s commands.

The precise reason that Paul asserts that we are
under grace and therefore not under the condemnation or
curse of the law is to explain how it is that sin does not
have dominion over us—to explain, that is, why we
have become slaves to obedience and now have lives
characterized by conformity to God’s law (Rom.
6:13-18). It is God’s grace that makes us Spiritual
men who honor the commandments of our Lord.

Spiritual Powers

The answer to legalism is not to portray the law
of God as contrary to His promise (Gal. 3:21) but to
realize that, just as the Christian life began by the
Spirit, this life must be nurtured and perfected in the
power of the Spirit as well (Gal. 3:3). The dynamic
for righteous living is found, not in the believer's
own strength, but in the enabling might of the Spirit
of God, We are naturally the slaves of sin who live
under its power (Rom. 6:16-20; 7:23); indeed, Paul
declares that we are dead in sin (Eph. 2:1), However,
if we are united to Christ in virtue of His death and
resurrection we have become dead to sin (Rom. 6:3-4)
and thus no longer live in it (v. 2).

Just as Christ was raised to newness.of life by the
Spirit (1 Tim. 3:16; ! Pet. 3:18; Rom. 1:4; 6:4, 9), so
also we wha have His resurrected power indwelling
us by the life-giving Spirit (Eph. 1:19-20; Phil. 3:10;
Rom. 8:11) have the power to live new lives which
are freed from sin (Rom, 6:4-11). The result of the
