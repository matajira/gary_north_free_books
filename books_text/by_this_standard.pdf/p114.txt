84 BY THIS STANDARD

peace, and joyous children.

In short, we see that a consequential approach to
ethics cannot be functional without the normative ap-
proach as well; the two work together because the
way of blessing is diligent obedience to the law of
God. Seeking first the righteousness of Christ’s king-
dom requires heart-felt obedience to the dictates of
the King, and in response to that He grants us every
blessing for this life and the next. We see again why
the validity or authority of God’s law cannot be dis-
missed today. Without that law we would be lost
when it comes to pursuing the beneficial conse-
quences for ourselves, others, and our society in all
of our moral actions and attitudes. As God clearly
says, He has revealed His law to us for our good
(Deut. 10:13). Opponents of God’s law, therefore,
cannot have our good genuinely in mind; they wit-
tingly and unwittingly mislead us into personal and
social frustration, distress, and judgment (Prov,
14:12).
