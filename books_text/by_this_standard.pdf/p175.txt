CONTINUITY BETWEEN THE COVENANTS ON THE LAW 145,

The same holds true for saints in the New Testa-
ment, Paul says that we have not been saved by good
works, but we have been saved for good works — that
is, in order to live obediently before God (Eph. 2:10),
God’s grace teaches us to renounce lawless deeds
(Titus 2:11-14), and by faith we actually establish —
rather than nullify—what was taught in the law of
God (Rom. 3:31).

IV. Ged’s law is central to His one covenant of grace.

(A) The law can epitomize or stand for the cove-
nant itself. We read in Genesis 17:10, 14 that circum-
cision could represent the very covenant itself that
God made with Abraham. In like manner, the stipu-
lations of the Mosaic law could be used to stand for
the covenant itself, as in Exodus 24:3-8 (cf. Heb.
9:49-20). Just as circumcision is the covenant, so also
is the law God’s covenant. This is why the tables of
Jaw and commandments which God gave to Moses
on Mount Sinai (Ex, 24:12) can actually be called
“the tables of the covenant” (Deut. 9:9, 11, 15), Accord-
ingly, when Jeremiah speaks of the New Covenant
which is to come, he indicates that the law of God is
central to its provisions: “I will put my laws into their
mind, and on their heart will I write them” (Jer.
31:33). This is quoted when the New Testament re-
flects upon the character of the New Covenant (Heb.
8:10), using these words as a summary for the whole
(Heb. 10:16). Concern for the covenant, then, entails
concern for the law of God in both Old and New
‘Testaments.
