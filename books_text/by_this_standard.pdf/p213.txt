NEW TESTAMENT OPPOSITION TO THE ABUSE OF GOD'S LAW 183

Conclusion

As we have seen, passages in Paul's writings
which seem to take a negative attitude toward the law
of God can be correctly harmonized with Paul’s
equally strong endorsements of the law by distinguish-
ing at least two (among many) uses of the word “law”
in Paul’s epistles.1 The revelatory use of “law” is its
declaration of the righteous standards of God; in this
the law is good. The degalistic use of “law” refers to the
attempt to utilize the works of the law as a basis for
saving merit; this is an unlawful use of the law and
receives Paul’s strongest condemnations. Paraphras-
ing 1 Timothy 1:8, Paul says that we know the law—
as a revelation of God’s unchanging will—is good, as
long as one uses it “lawfully” (as it is meant to be
used) instead of legalistically.

1, Gf. Daniel P. Fuller, “Paul and the Works of the Law,” Wést-
minster Theological Journal, XX XVIII (Fail 1975), pp. 28-42. For a
modem statement of the covenantal position that the Old Testa~
ment did not teach justification by law-works (legalism), see
Fuller's fine exegetical study, Gospel and Law: Contrast or Contin-
uum (Grand Rapids, Michigan: Eerdmans, 1980).
