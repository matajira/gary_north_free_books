PROLOGUE xbt

begun to recognize that the church is not culturally
impotent, and God’s law does not lead to impotence.

Unlike the comic book advertisement for Charles
Atlas’s “dynamic tension” program, where the
200-pound bully kicks sand in the face of the
98-pound weakling, Christians in the twentieth cen-
tury have been the 200-pound weaklings who have
been pushed around by 98-pound bullies. Like Sam-
son without his hair, Christians without God’s law
are impotent, and have been regarded by Philistines.
throughout the ages as drudges to be misused and
humiliated publicly, if the opportunity presents it-
self. What Dr. Bahnsen is proposing is that we fiex
our muscles and knock the. pillars out from under
humanism’s temple. But this time, we should push

from the outside of the arena, not pull from the in-
side. When it comes to social collapse, let the
Philistines of our day be inside. Let us pick up the
Pieces.

The much-abused traditional slogan, “we're
under grace, not law,” is increasingly recognized by
intelligent Christians as an ill-informed and even
perverse theological defense of a perverse cultural
situation: “We’re under a God-hating humanist legal
structure, not God’s law, and there’s nothing we can
do about it.” But there s something Christians can
do about it: they can start studying, preaching, and
rallying behind biblical law.

It is unlikely that antinomian critics of biblical
law can be successful much longer in withstanding
the pressures of our era. A growing minority of
Christian leaders now recognize that they must
