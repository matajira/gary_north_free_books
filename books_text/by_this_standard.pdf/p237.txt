‘THE TRADITIONAL “THREE USES" OF THE LAW 207

An ironic situation has arisen in our day. Evan-
gelical Christians who might be considered to lean
toward a more “liberal” position in politics, and
Evangelical Christians who might be thought to
favor a more “conservative” position in politics, have
at least this one unwitting area of significant agree-
ment: they both wish to make principled and author-
itative use of the Old Testament law for social jus-
tice. Recent publications which have promoted an
active involvement by the believer in relieving the
needs of impoverished people around the world have
made noteworthy appeal to the law of Jubilee, while
many books and articles written to protest the toler-
ance of homosexuality and/or abortion in our day
have made clear and unapologetic reference to the
Old Testament prohibitions against them.

The law is recognized has having a continued
political significance by present-day believers, even
when they do not systematically work out a
theological foundation for the appeals which are
made to the law’s authority in contemporary society,
and even when they might elsewhere unwittingly
contradict that assumed foundation. That founda-
tion is the continuing validity of God’s law, even in
its social or political relevance. Strangely enough, it
is often those who are heirs to the Reformation tradi-
tion of maintaining the political use of the law that
raise objection to that notion today.

In resisting the political use of God’s law, in
detracting from its political relevance, and in en-
couraging either indifference to questions of social
justice or else alternative standards for it, such men
