278 BY THIS STANDARD

dorse, then indeed even our attitude toward the per-
version itself has varied from that prescribed by
God's law!

Someone might convincingly argue that the
method of execution (for example, stoning) is a varia-
ble cultural detail, but the text simply will not sup-
port the thesis that the law’s penal sanctions are cultur-
ally variable. It will not support teaching an open-
ended approach to penology— that is, teaching sim-
ply that criminals should be punished, without say-
ing what the punishment must be. The principle
taught in such case laws is that the relevant crimes
are worthy of this or that specified treatment.

The various alternatives for treatment may not
be changed around—as though a murderer could be
fined, and a thief could be executed. It is precisely
the equity of God’s penal sanctions which precludes
any shifting of them around; yet this shifting of pen-
alties is what the suggestion before us would allow
(by saying that the case law teaches no set sanction but
only that there should be some kind of sanction).
Such shifting violates the principle of an eye for an
eye, a tooth for a tooth, a life for a life, etc. We have
already seen above that equity characterizes the
penal sanctions of God’s law. Crimes have meted out
to them precisely what justice says they deserve. This
is the Biblical approach to penology, and to depart
from it is to welcome (in principle) arbitrariness,
tyranny, and injustice into one’s society.

No More, No Less
Biblical penalties, we are observing, are never
too lenient and never too stringent for the cases
