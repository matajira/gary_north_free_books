288 BY THIS STANDARD

matter of fact, there were indeed citizens of Israel
(members of the state) who were nof circumcised
(bearing the mark of belonging to the covenant com-
munity), namely the women. But even more impor-
tantly, there were men in Israel who enjoyed the
privileges and protections of citizenship, and yet
who were not members of the “church”— who were
not circumcised and did not partake of the redemp-
tive meal of passover. These were the “sojourners” in
Israel. They had the same law (Lev. 24:22) and same
privileges (Lev. 19:33-34) as the native Israelite, but
unless they were willing to undergo circumcision
and join the religious community, they did not take
passover (Ex. 12:43, 45, 48).

In many ways this parallels the situation today.
All men live under the same laws and privileges in
our state, but only those who assume the covenant
sign (baptism in the New Testament) would be
members of the church and free to take the Lord’s
Supper (the redemptive meal). Even at this level we
do not find a situation in ancient Israel that is
altogether different from our own. Church and state
were not merged in any obvious way in Old Testa-
ment times.

Of course there were many unique aspects to the
situation enjoyed by the Old Testament Israelites. In
many ways their social arrangement was not what
ours is today. And the extraordinary character of
Old Testament Israel may very well have pertained
to some aspect of the relation between religious cult
and civil rule in the Old Testament. Nevertheless,
we will search in vain to find any indication in the
