112 BY THIS STANDARD

nounce every sin against God’s commandments and
every hindrance to complete obedience to them
(Matt. 19:21), Accordingly, we learn that God’s law
is our standard of moral perfection today. James in-
structs believers that the man who is blessed of God
is the one who is a doer of the word, having “looked
into the perfect law” (Jas. 1:25).

Summary

We may return now to Romans 12:2, where
Paul’s ethical guidance to the New Testament be-
liever is to follow the will of God, that which is good,
well-pleasing, and perfect. We have seen that the
New Testament consistently assumes as common
knowledge (and explicitly applies the truth) that she
commandments of God's law in the Old Testament are a
sufficient and valid standard of God's will, of the good, of the
well-pleasing to the Lord, and of perfection. Whenever
these themes appear in the New Testament scrip-
tures the authority of God’s law is repeatedly being
applied. Our obligation to that law is reinforced
many times over when Paul summarizes the ethical
standard for New Testament morality as “the good,
well-pleasing, and perfect will of God.” God himself
is to receive the glory for bringing our lives into con-
formity with this unchallengeable norm for Chris-
tian conduct. He is the One who, through the minis-
try of His Son, makes us “perfect in every good thing
to do His will, working in us that which is well-
pleasing in His sight” (Heb. 13:20-21).

Every attempt to reject the law of God in the
New Testament era meets with embarrassment be~
