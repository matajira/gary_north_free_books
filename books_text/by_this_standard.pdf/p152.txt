122) By THs STANDARD

native rules demonstrates the current authority and val-
idity of the particular rule, For this reason a driver who
is stopped by a highway patrolman for travelling
sixty-five miles per hour will not avoid a ticket by ap-
pealing to the former law which set the maximum
speed at sixty-five. The use of the fifty-five mile per
hour speed law by the courts and the police estab-
lishes the validity of this law over against the older
one. We do not use expired rules if we are informed
and honest. Looking at library cards and credit
cards, and reflecting on civil rules and sports rules,
we have seen that the use of them assumes their val-
idity. Invalid cards and rules are unauthoritative.

We can now apply this reasonable insight to the
practice of the New Testament speakers and writers.
Like policemen and umpires, the inspired speakers
and writers of the New Testament were called upon
to make decisions on the basis of rules; they needed
to draw moral judgments in particular situations.
When that time came, which rules did they utilize?
Did they —being infallibly informed in their utter-
ances—ignore the moral rules (commandments) of
the Old Testament as though they were expired, in-
applicable, or invalid? What does New Testament
usage of the Old Testament law tell us about that
law’s authority today?

Antinomian Doctrines

The current validity of the standing rules of Old
‘Testament morality is either challenged or drastically
reduced by many within the Christian church today.
We find some who teach that the New Testament
