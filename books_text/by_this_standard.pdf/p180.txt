150 BY THIS STANDARD:

according to 1 Timothy 1:8-10 (cf. 6:3). God gave us
His law for our good, and for that reason Old and
New Testament writers rejoice in it. It is to our
shame if we do not emulate their attitude.

VI. God's law is eternal and is not to be altered.

In a day when many view the law of the Lord as
arbitrary, expendable, or temporary in its authority
for the life of man, it is highly valuable to observe the
outlook of the inspired writers. Moses wrote that for-
ever it would go well with God’s people to observe the
commandments which He revealed (Deut. 12:28).
David exclaimed that “All his precepts are sure; they
are established forever and ever” (Ps. 111:7-8; cf.
119:152). Indeed, the eternal authority of God’s com-
mands characterizes each and every one of them:
“Every one of thy righteous ordinances endureth for-
ever” (Ps. 119:160). Looking unto the fearful day of
the Lord when the wicked will be consumed with fire
(Mal. 4:1), the prophet Malachi pronounces as one
of the final words of the Old Testament, “Remember
the law of Moses my servant” (4:4),

However, in the pages of the New Testament we
hear the words of one who is far greater than Moses,
David, or any prophet of old, Their testimony to the
eternal authority of God's law ts pale in comparison
to the absolutely clear and utterly unchallengeable
declaration of Jésus Christ that God’s command-
ments—each and every one—is everlastingly valid:
“Truly I say unto you, until heaven and earth pass
away, until everything has come about, one letter or
one stroke shall by no means pass away from the
