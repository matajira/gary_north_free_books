CRIME AND PUNISHMENT 283.

magistrate’s response). Does his silence challenge or
support the validity of the sanctions? Neither, really,
for a consideration of silence is logically fallacious.
What is important is the presumption of continuing
validity taught elsewhere by Christ (Matt. 5:19) and
Paul (Acts 25:11; Rom. 13:4; 1 Tim. 1:8-10; cf. Heb.
2:2). Silence cannot defeat that presumption, for the
presumption can be turned back only by a definite
word of abrogation.

Conclusion

There is no general repudiation of the penal sanc-
tions in the New Testament. And #f there were, there
would be no textually legitimate way to salvage the
penalty for murder. The attempt to limit our moral
obligation to the Noahic covenant (Gen. 9:6) is mis-
conceived, not only because the New Testament rec-
ognizes no such arbitrary limitation (see Matt.
5:17-19), but also because the Mosaic law is neces-
sary to understand and apply fairly the Noahic stip-
ulation about murderers {for example, the distinc-
tion between manslaughter and murder is not drawn
in Genesis 9), That Paul in Romans 13 was not
limiting the power of the sword to the guidance of
Genesis 9 is clear from the fact that Paul recognizes
the right of taxation, which is unmentioned in
Genesis 9. If the Old Testament sanctions have been
abrogated (and we have no reason to think they have
been), then there appears to be no way to salvage the
death penalty for murder either. Yet very few
evangelicals will be content to accept that conclu-
sion, especially since it leaves Paul’s words about the
