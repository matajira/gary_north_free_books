LAW AND POLITICS IN OLD TESTAMENT ISRAEL 223

have seen that the whole Bible is our standard of
morality today, for God does not have a double-
standard of justice. Instead, the law reflects the
Lord’s unchanging holiness, being perfectly obeyed
by Christ (our example) and enforced within the be-
liever by the Holy Spirit (our power). We have seen
that the Old and New Covenants have a uniform
view of the law of God, and that Christ Himself
declared that every stroke of the Old Testament con-
tinued to have validity after His coming to earth to
save sinners. Repeatedly the New Testament
authors assume the standard of the law in their
ethical themes and make application of the law in
their moral judgments, Every scripture, every point,
every word, and indeed every letter of the Old Testa~
ment law is upheld in the New Testament.

Therefore, it would seem obvious that the socio-
political aspects of the Old Testament law would re-
tain their validity today ~ that they are authoritative
for civil magistrates of all ages and cultures. Just as
parents, farmers, merchants, and others have moral
duties laid upon them in the Old Testament law, so
also civil rulers have duties enjoined for their official
business in the law of the Lord,

Yet not everyone is willing to endorse the current
applicability of the Old Testament law in the partic-
ular domain of civil politics. The whole law may be
endorsed in the Old Testament, it it thought, but
there has come about in the New Testament a differ-
ent attitude toward the civil magistrate. The view
taken seems to be that because the magistrate in Old
Testament Israel was in various ways unique — being
