CONTINUITY BETWEEN THE COVENANTS ON THE LAW 151

law” (Matt. 5:18). The Old and New Testaments
unite in this doctrine.

The voice of the two Testaments is further united
in saying that God’s law is not to be altered. David
recognized that God commands only what is just
and right, and thus to depart from His commands is
to deviate from moral integrity, “I esteem all thy pre-
cepts concerning all things to be right, and I hate
every false way... . All thy commandments are
righteousness” (Ps. 119:128, 172). To change or ig-
nore any of God’s commands is necessarily to create
an unrighteous or unjust pattern for behavior,
Therefore the law itself guards against alterations
within itself: “You shall not add unto the word which
Icommand you, neither shall you diminish from it,
in order that you may keep the commandments of
Jehovah your God” (Deut. 4:2; cf. 12:32), No man
has the prerogative to tamper with the requirements
laid down by God. Only God himself, the Law-giver,
has the authority to abrogate or alter His command-
ments, Yet the testimony of God incarnate in the New
Testament is that the law is not to be changed, even
with the momentous event of His coming: “Do not
think that I came to abrogate the law or the proph-
ets... . Therefore whoever shall break one of the
least of these commandments and shall teach men so
shall be called least in the kingdom of heaven”
(Matt. 5:17, 19). God’s eternal and righteous law is
unalterable, according to the joint teaching of the
Old and New Testaments.

VIL. Therefore, we are obligated to keep the whole law today.
