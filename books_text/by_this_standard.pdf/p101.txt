C. MOTIVATIONAL AND CONSE-
QUENTIAL PERSPECTIVES

9

A MOTIVATIONAL ETHIC
ENDORSES THE LAW

“All of God's people, throughout both testa-
ments, have a heart which longs to obey the
commandments of the Lord, for the faw is
established against the background of God's
mercy toward His people.”

Those who are genuine believers in Christ know
very well that their salvation cannot be grounded in
their own works of the law: “... not by works of
righteousness which we did ourselves, but according
to His mercy He saved us, . . . that being justified by
His grace we might be made heirs according to the
hope of eternal life” (Titus 3:5-7). The believer's justi-
fication before God is grounded instead in the perfect
obedience of Jesus Christ (Gal. 3:11; Rom. 5:19); it is
His imputed righteousness that makes us right before the
judgment seat of God (2 Cor. 5:21). “A man is justified
by faith without the deeds of the law” (Rom. 3:28).
