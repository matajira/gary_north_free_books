LAW AND POLITICS IN THE NEW TESTAMENT 249

Romans 13 does not stress practical submission on
the part of the believer; it rather stands in evaluative
judgment over all magistrates, showing the Chris-
tian which ones are deserving of their submission
and obedience. Both of these interpretations of
Romans 13 have tended toward practical conse-
quences which are pretty clearly unacceptable, given
the rest of what Scripture says to Christians about
morality and politics. The descriptive view of Rom-
ans 13 has led many believers in past history to be in-
different to concrete political wrongs and even to
comply passively with the injustices of political
tyrants, like Hitler. On the other hand, the prescrip-
tive view of Romans 13 has often encouraged a rebel-
lious spirit toward the civil magistrate, leading
believers to take lightly the Biblical injunctions
against revolution or civil disobedience.

It can be said in defense of each approach that
these practical consequences are in fact abuses of the
respective views—abuses that do not take into ac-
count other Biblical teaching, qualifications made,
and the full context. This may be, but if one keeps in
mind the Old Testament background to Paul's in-
struction about the civil magistrate in Romans 13, it
is possible to interpret the passage in a way which
does justice both to the Christian’s need to resist
political injustice and to the Christian’s obligation to
be in submission to the powers that be.

When Paul says that the ruling powers are
ministers of God who avenge wrath against
evildoers, he is explaining what civil magistrates
ought to be and simultaneously explaining why
