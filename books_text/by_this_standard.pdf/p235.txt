‘THE TRADITIONAL “THREE USES" OF THE LAW 205

the inevitable consequence. The present pre-
vailing theology has not been able to elevate so-
ciety and halt its moral decline, and unques-
tionably, one explanation of this is its misunder-
standing of the place of the law and its useful-
ness in the service of the covenant of grace.”3

When men fail to see that God’s law is meant to
operate as external discipline within society, when
they doubt and oppose the “political use” of the law,
their societies inevitably suffer the accursed conse-
quences. Carl F. H. Henry puts the matter this way:

Even where there is no saving faith, the Law
serves to restrain sin and to preserve the order
of creation by proclaiming the will of God. . . .
By its judgments and its threats of condemna-
tion and punishment, the written law along
with the law of conscience hinders sin among
the unregenerate. It has the role of a magistrate
who is a terror to evildoers It fulfills a pol-
itical function, therefore, by its constraining in-
fluence in the unregenerate world.*

 

Biblical Law and Civil Government
This political function of the law is undeniable in
the Old Testament, where God delivered statutes
pertaining to civil matters for His people. These

3. Samuel Bolton, The True Bounds of Christian Freedom (Lon-
don: Banner of Truth Trust, 1964), pp. 10-11,

4. Christian Personal Ethics (Grand Rapids, Michigan: Eerd-
mans, 1957), p. 355.
