Fractional Reserve Banking 81

Obviously, it is useful to him. He gets cold at night, so, he
comes to get it. It is a lot of trouble for him (and a bit humiliating)
to have to cored to my place every evening to get back his cloak,
He wants to get out of debt as soon as possible. So it does serve as ,

an incentive for him to repay, which also means that it is an asset
‘to me.

There is another aspect of this sort of collateral which most
people never think of. What if the borrower is corrupt in his
heart? What if he went out and borrowed money from a dozen
people, with the cloak as collateral? He promised each lender:
“Look, if I default, you may have my cloak. I want my cloak, so I
surely won't default .“ But if he has borrowed against the cloak
twelve times over, he may be perfectly willing to default on that
cloak. Let the lenders decide who gets the collateral.

The corrupt debtor shouts, “Tough luck, suckers. Sort it out
among yourselves, The money is gone. All I have left is the cloak. I'll
be cold without it, but I had fun with the money. It was worth it!

What the Bible teaches is that it is immoral to secure multiple
loans with the same piece of collateral. To reduce the possibility of
someone indebting himself several times over, the Bible allows the
lender to take physical possession of the collateral daily. Since only
one lender can do this per day, the debtor is not able to indebt him-
self many times over on the basis of one piece of collateral.

Just because a piece of collateral is physically useless to the
lender does not mean that it is economically useless to him. It may be
very useful to him economically, first, to motivate the debtor to
repay the loan, and second, to prohibit the borrower from indebt-
ing himself several times over.

Multiple Indebtedness

In Chapter Three I discussed the creation of a warehouse
receipt for storing gold or silver. A person brings in ten ounces of
gold to the warehouse for safekeeping, and the warehouse issues a
receipt for ten ounces of gold. The owner pays a fee for storing the
money, but he presumably increases the safety of his holdings.
The warehouse specializes in protecting money metals from bur-
