2
THE ORIGINS OF MONEY

And the gold of that land [Havilah] is good. Bdellium and the
onyx stone are there (Genesis 2:12).

In the second chapter of the Book of Genesis, God, speaking
through Moses, saw fit to mention this aspect of the land of
Havilah. It was a place where valuable minerals were present.
One of these minerals was gold.

We cannot legitimately build a case for a gold standard from
this verse. We could as easily build a case for the onyx standard,
or a bdellium standard (whatever it was: possibly a white
mineral), What we can argue is that Moses knew that people
would recognize the importance of the land of Havilah because
they would recognize the value of these minerals. One of these
minerals was gold.

Why do I stress gold? Historically, gold has served men as the
longest-lived form of money on record. Silver, too, has been a
popular money metal, but gold is historically king of the money
metals. There is no doubt that Moses expected people to recog-
nize the value of gold. We read his words 3,500 years later, and we
recognize the importance of the land of Havilah. If we could
locate it on a map, there would be as wild a gold rush today as
there would have been in Moses’ day. No one thinks to himself, “I
wonder what gold was?”

Money: Past, Present, and Future

You may remember from the previous chapter that money
appears in a society when individuals begin to recognize that a
particular commodity is becoming widely accepted in exchange.

19
