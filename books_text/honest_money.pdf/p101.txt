Protecting Licensed Counterfeiters 95

The Federal Reserve Bank is a privately owned corporation
whose shares of ownership are held by. the member banks. It is
quasi-public, in that the President of the United States appoints
the members of the Board of Governors of the Fed, but the direct-
ors of the 12 regional Fed banks, and especially the powerful New
York Federal Reserve Bank, are not appointed by any political
body. There are nine directors of each regional Federal Reserve
Bank; six are appointed by local bankers, and three by the Board
of Governors of the Federal Reserve System.

Can the government tell the Fed what to do? If Congress and.
the President are agreed about what to do, yes. If there is dis-
agreement over monetary policy — and there usually is — then the
Fed does pretty much what it wants. What the origin of the Fed
indicates is that the Fed does what the major multinational banks
want. What the House and Senate committees on bank regulation ©
want is usually unclear, and a majority of the members barely

* know what a central bank is, let alone how it functions or— won-
der of wonders — who actually owns it. They don’t even ask. It’s
considered “bad form,” a breach of etiquette. I know from experi-
ence, I served as a research assistant for a Congressman who was
a member of the House Banking Committee.

The Monetization of Debt

This is an invention of the modern world. A government
needs money. It fears a tax revolt if it raises taxes. It cannot afford
to pay more interest, so it can’t borrow money from the general
public. It therefore goes to the central bank and says, “Buy our
Treasury debt certificates.”

The Treasury creates the debt certificates (usually on a com-
puter entry: liability). The central bank buys them by creating
another entry: money. The computer blips are swapped.

The government has just monetized some of its debt. It pays a
lower rate of interest inztially to the central bank than it would
have to pay if it went into the free market to compete for borrowed.
money.

What’s wrong with this? Who gets hurt? Holders of money
