A Program of Monetary Reform 127

agency. Furthermore, all past financial records of the Fed should
be audited, with the money for this task being collected from the
Fed’s interest-rate returns on the Federal debt it now holds.

Third, all meetings of the Board of Governors of the Federal
Reserve System must be opened to representatives of the press, to
members of the Cabinet, and to members and staff economists of
the House and Senate committees that are required by law to
supervise the U.S. banking system..

In the case of “emergency meetings” that require “closed-door
hearings,” the President of the United States, the Secretary of the
Treasury, the Speaker of the House, the House minority leader,
any member of the U.S. Supreme Court, and the majority and
minority leaders of the Senate must be invited to attend.

All decisions of the Board of Governors with respect to mone-
tary policy must be made public immediately after each meeting,
not 45 days later.

Fourth, the Federal Reserve System must cease buying or selling
any further debt certificates or assets of any kind. A permanent
moratorium on such purchases and sales must be imposed. The
Fed will not be allowed to inflate or deflate the nation into a crisis,
and then demand that we turn everything back over to them.

Fifih,the assets and liabilities of the Federal Reserve System
will be transferred to the U.S. Treasury, and the Fed will cease
operations entirely. This includes the pension fund of the Board of
Governors of the Fed, probably the only fully funded pension sys-
tem in Washington.

There may be some steps that I have missed. Nevertheless, I
would settle for any program of Fed-removal. But the program
must lead to Step Five: the abolition of the Fed.

Increased Reserve Requirements

Once the assets of the Fed are back in the hands of the Treas-
ury, control over banking must also reside in the Treasury. The
Treasury's task at that point will be to bring the banking system
into conformity to the principle of zero fractional reserves, or
100% reserve banking.
