A Biblical Monetary System 105

into effect, alternately driving out of circulation either silver or
gold, depending on which one was artificially undervalued by the
Federal government at any point in U.S. history.

The problem is, people think there has to be only one
“supreme” money, defined in value by the government. Yet we
voluntarily use paper money, checks, credit cards, and token
coins: pennies, nickels, dimes, etc. We used to use silver coins,
and before that, gold coins. We once used private banknotes, be-
fore the Federal government started taxing them, and the banks
switched to checks (untaxed).

Why do we think we need one “supreme” form of State-dgfined
money? The only State-defined form of money that is legitimate is
tax money. The government has the authority to determine what
it will accept as payment from among the various types of privately pro-
duced moneys that become established through market competition. But the
State cannot be trusted to establish its own money. It always
betrays this trust. It counterfeits its own currency. It inflates.

The U.S. Constitution specifies that gold and silver alone may
be issued by the state governments as legal tender currency (Arti-
cle I, Section 10). The Founding Fathers clearly recognized the
limits that metal moneys place on governments. Unfortunately, '
they neglected to place the U.S. government under a similar re-
striction. The first great political battle of the Federal government
after the Constitution was over the establishment of a privately
owned central bank, which Alexander Hamilton wanted and
Jefferson opposed. Hamilton won, and the U.S. began its long,
though intermittent, history of fractional reserve central banking.

The important point is that the State must not be allowed to
establish any fixed price between any two forms of money. I am
not speaking here of warehouse receipts that function as a substi-
tute for metal money. If a warehouse receipt promises to pay one
ounce of gold, it must have one ounce of gold in reserve. I am
speaking here of the exchange price between two market-created
moneys: gold vs. silver, copper vs. silver, dollars vs. yen, etc. The ,
government must not enforce price controls on anything, includ-
ing money.
