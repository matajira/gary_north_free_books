Fractional Reserve Banking 85

money) because the economy seemed to be booming, and interest
rates were nice and low.

The reason interest rates were so low is that the banks were
counterfeiting money and lending it out. They didn’t have to pay de-
positors any interest, and they were taking in interest. It was so easy

The day of economic judgment arrives. Businesses go bank-
rupt. Others lay off employees. Everyone has to adjust to the new
conditions of supply and demand. The inflation is over; deflation
has come. Some bank notes (warehouse receipts) are worthless.
They aren’t money any more. People who held them have lost
their money. They stop spending as much as before.

‘Does this sound familiar? It should. It’s called a depression.
And there is one cause, and only one cause, of depressions: prior wn-
flations. The good days looked so good; the bad days look so bad.
People were lied to. The counterfeit warehouse receipts were
promissory notes, and these promises were lies. The reality of the
post-lying era is like a hangover after a night of reveling. But it is
reality. The drunk, like the businessman, should be thankful for
it. They seldom are.

Look, depressions are hard to explain. Why should virtually
every businessman in the country.-even in the world (1930's) - all
make the same mistakes at about the same time. Sure, business-
men make mistakes. Some buy when they ought to be selling.
Some win, and others lose. But not in a depression. Why do
almost all of them make the same mistake at about the same time?

The answer is the money system. All businessmen are tied to
money and interest rates. If we want to explain why almost all of
them think a boom is going to continue when a bust is about to oc-
cur, we need to look at money and interest rates. The business-
men make the same mistakes because interest rates are giving them in-
correct signals,

Borrowing rates are low because bankers are creating counter-
feit money— egal counterfeit money — and loaning it out. Then
inflation hits, the economy booms, and then craters when the
bankers slow down the printing of money in self-defense against
bank runs: too many receipts for too few reserves. Money shrinks
(or even just slows down), and the depression hits.

We've seen it before: the boom of 1964-69 turned into the bust
