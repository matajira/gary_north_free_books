Biblical Banking 7

fear your God, that your brother may live with you. You shall not
lend him your money for usury, nor lend him your food at a
profit” (Leviticus 25:35a, 36-37). Notice: it speaks of the poor
brother. This is not a prohibition against business loans. ‘

The warning against profiting from charitable loans from
those whoyshare the faith is clear: “One who increases his posses-
sions by usury and extortion gathers it for him who will pity the
poor” (Proverbs 28:8). In other words, the evil man lays up
treasure unjustly, but the righteous man will eventually earn it
back. This is in line with another promise of Proverbs, “the wealth
of the sinner is stored up for the righteous” (13:22b).

Yes, the lender who lends money to a poor fellow believer can
legitimately ask only for a return of the principal. He must not ask
for anything extra. This means that he forfeits the interest that
might otherwise have been earned in some sort of business loan.
The lender suffers a loss, for he forfeits the use of his capital over
time, and bears the risk that the loan will never be repaid. But
God will reward the generous lender, Proverbs says. In effect, God
pays the interest payment to the righteous lender. God becomes a
kind of heavenly co-signer of the poor man’s note. Specifically, the
generous lender will prosper at the expense of the unrighteous ex-
ploiter in a society which is governed by the law of God.

Summary
Lending money at interest isn’t immoral and shouldn't be
made illegal. It shouldn’t be controlled by the State in any way.
The Bible teaches that loans at interest to poor fellow believers
should not be made, but the Bible is equally emphatic that it is
God who punishes this type of loan. There is no mention of any
civil penalties. It is a religious matter. Someone has to define
“fellow believer” and “poor.” This is not something the civil author.
ities should concern themselves with. At most, church authorities

might penalize usurers, not the State.
But most loans in a society are business loans or loans made to
people who have credit references and collateral. These are not
poor people. They come with credit worthiness. This is a true capital

e
