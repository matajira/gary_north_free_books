Fractional Reserve Banking 87

explanation of how the system works. It was published by the
Federal Reserve System itself in 1963, on the 50th anniversary of
the creation of the Fed (in 1918), a book called The Federal Reserve
System: Purposes and Functions. The numbers are different because
the example uses a more conservative 20% reserve requirement
instead of today’s 10% (or lower). But you can see how it works,

according to the people who run this country’s banking and mone-

tary system. With a 20% reserve requirement, a $100 deposit mul-

tiplies by 5: the original $100 plus $400 in “phantom” money —

which is real, legal money.

Murnetyinc Caracrry oF RESERVE MONEY
TurouGH Bank TRANSACTIONS

 

 

 

(in dollars)
Deposited Set aside
Transactions in checking Lent as
, accounts reserves
Bank 1. 80.00 20.00
2 64.00 16.0
3 51.20 12,80
4, 40.96 10,24
5 32.77 8.19
6 26.22 6.55
7 20.98 5,24
8 16.78 4,20
9 13.42 3.36
0, ” 10.74 2.68
Total for 10 banks .,....... «446.83 357.07 89.26
Additional banks .........444 63.67 242.93 . 210.74
Grand total, all banks...... 600.00 400.00 100.00

 

Assuming an average member bank reserve requirement of
20 per cent of demand deposits. |
2Adyusted to offset rounding in previous figures.
