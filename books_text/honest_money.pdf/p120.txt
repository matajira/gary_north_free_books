14 Honest Money

placed on the rate of monetary expansion. Geological limits were
therefore placed on monetary fraud. Governments and banks
tampered with monetary weights and measures on a minor scale
only.

What does the average person need to remember in order to
understand the fundamental principles of Biblical money? Not
much.

1, We shouldn’t expect something for nothing, such as the de-
positor’s withdrawal on demarid of loaned-out funds, or counterfeit
money making everyone richer.

2, The State shouldn’t interfere with private non-coercive
decisions (contracts).

3, It is cheaper to print paper money than it is to mine metals.

4, Money isn’t money unless people expect other people to ac-
cept it m trade later on, meaning:

-5. Money requires continusty of acceptance over time.

6, Debasing money is a form of tampering with weights and
measures,

7, Debasing money reduces its value in trade.

8. Debasing money therefore reduces the wealth of people who
hold money.

9, Warehouse receipts should be backed 100% at all times by
whatever is promised by the receipt.

There are other subtle distinctions that are useful, but these
are the basics. Any society which enforces civil laws against any
violation of fixed, defined weights and measures, and any viola-
tion of the rule against multiple indebtedness (unbacked ware-
house receipts) will have honest money.

The problem comes when the State, as the enforcer, gets into
the business of stamping its mark on “certified money.” This proc-
ess soon becomes money creation, then a monopoly of money cre-
ation, then debasing the money, and finally elitist private control
over “government” money by central bankers. We have seen this
again and again. The control over “government” money by pri-
vate central bankers is a universal feature of modern economies.
So is unstable money.
