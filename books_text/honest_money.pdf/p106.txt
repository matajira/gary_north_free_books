100 Honest Money

bankrupt, either the Fed or Congress would almost certainly act
to bail it out. The Fed would print the money, just as it did when
the Continental Illinois Bank almost went under in 1983, and it
had to pump about $4.5 billion into it. The bankers don’t want a
bank run.

Could the FDIC bail out the banks in a panic? Of course not.
It has about $1 on reserve for every $100 in deposits. This “reserve”
is in fact nothing except U.S. Treasury bills: government bonds,
in other words. To get the cash, the FDIC has to cash in these
bonds and get the U.S. Treasury to pay cash. Two bankruptcies
the size of Continental Illinois would deplete the FDIC's reserves
to zero, or close to it.

The FDIC is an illusion whose purpose is to calm down
depositors who might otherwise make runs on weak banks and
crash the economy into a depression. The FDIC was created to
reduce risks for bankers, so that at least the biggest banks don’t face
such crises. Then the bankers can go out and loan hundreds

‘millions of the depositors’ dollars to “Third World” nations that
never intend to pay back any of the money.

In a gold-standard country— none exists any more— the peo-
ple can put the pressure on banks and the government to stop
inflating the currency, simply by going down to the bank or the
Treasury and buying gold at the fixed, government-defined price.
Pretty soon the government has to stop inflating. Pretty soon, a
bank which has issued too many phony warehouse receipts gets
threatened by a panic run.

Then one of two things happens:

1, The bank (or Treasury) stops creating unbacked paper
money, or loans, or checks, (Recession usually follows.)

2, The bank (or Treasury) closes the withdrawal window. No
more gold on demand. (Inflation usually follows.)

The second event happens at the beginning of every major
war. It did in the United States in December of 1861, when the
North invaded the South. It did during World War I when the
U.S. entered the war that President Wilson had promised to keep
