46 Honest Money

nearest store and buy goods with it.

Those who are hurt are those who hold onto these debased in-
gots too long. As more and more of them flood the market—
remember, the only way for the cheaters to collect their profits is
to spend the extra, silver—one of two things happens. First, if the
dross in the new ingots is undetectable, the market price of all
silver ingots will fall: more supply, lower price per ingot. Second,
if the dross-filled ingots are detectable (inexpensively), then the
price of the phony silver ingots will drop in relation to pure silver
ingots. This means that there will be two separate price-quote sys-
tems in the economy: a pure silver price per good or service, and a
dross-filled silver price per good or service. “

In either case, the person who is stuck with a pile of
dross-filled ingots will lose when prices rise. He sold goods and
services at yesterday’s lower price level, but he will buy his goods
and services at today’s higher price level, or perhaps at tomorrow's
even higher price level.

Thus, the winners are those who get access to the phony
money early, and spend it fast. The losers are those who get access
to the phony money later, after prices in general have risen.
Worse, what about the people on fixed money incomes, who don’t
see their incomes rise at all, but who now face higher prices?

Who are these people likely to be? Pensioners. Small
businesses that are barely making money. In short, widows: the
very people that Isaiah said were being harmed by false judg-
ment. They were to be protected, and to fail to do so was a sign of
sin within the nation, but especially among the rulers: “You shall
not afflict any widow or fatherless child. If you afflict them in any
way, and they cry at all unto Me, I will surely hear their cry; and
My wrath will become hot, and I will kill you with the sword;
your wives shall be widows, and your children fatherless” (Exodus
22;22-24),

Isaiah was threatening them with just such military judgment
by God. This is why it is ridiculous to argue that Isaiah was not
talking about the specific sin of monetary debasement, but only of
a strictly “spiritual analogy.” He was talking about corrupt metal
