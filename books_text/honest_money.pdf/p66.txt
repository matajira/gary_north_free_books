60 Honest Mong

been imposed by a foreign ruler. If they profited from the system,
why shouldn’t they pay taxes to support the system? End of argu-
ment.

We can learn a lot by a study of Roman coinage. The Roman
Empire was a religious organization— all ancient societies were.
(So are all modern societies, but most of them disguise this fact.)
Increasingly, the emperors were regarded as gods, especially in
the eastern half of the Roman Empire. The coins were used as
political devices. In an illiterate world, the pictures on the coins
announced religious messages, and therefore political messages.

Tiberius Caesar’? picture was on the denarius that they handed
to Jesus. Tiberius issued only three types of denarii during his
reign, and by far the most widely circulated had his face on one
side, adorned with a laurel wreath, a sign of his divinity. The in-
scription read, “Emperor Tiberius august Son of the august God,”
referring to Caesar Augustus, the father who had adopted him.

On the back of the coin, his mother appears, seated on a
throne of the gods. In her right hand she holds an Olympian
scepter, and in her left hand is an olive branch, a symbol of peace.
As Professor Stauffer comments concerning the coin: “It is a sym-
bol of power. For it is the instrument of Roman imperial policy.”

Roman coins from Augustus on, announced divine emperors,
saviors of the world. Yet by the year 300, the coins were worth-
less, price controls had been imposed, and the empire was an eco-
nomic catastrophe. The more the coins promised deliverance, the
worse they became. The silver was taken out of them, and cheap
copper was substituted. Professor Stauffer’s book, Christ and the
Caesars (1965), tells the story of the collapse of the pagan Roman
Empire through a study of its progressively debased coinage. As
the Empire collapsed, so did its coinage.

A Sign of Sovereignty
Political rulers learned very early just how powerful coins
could be in serving as symbols of political and religious authority.
They could serve as unification devices; just as flags serve modern
men. The users were reminded constantly of the source of the
