232 BRINGING IN THE SHEAVES

_____. Bad News for Modern Man (Westchester, IL:
Crossway Books, 1984). Perhaps the most helpful aspect of this
gripping and controversial book is its unmasking of the all-too-
predominant evangelical humanism. Irreverent wit and scintillat-
ing investigative journalism combine to make this work a profound
indictment of passivity and pietism.

—____, with Harold Fickett. A Modest Proposal
(Nashville, TN: Thomas Nelson Publishers, 1985). In this work of
speculative fiction, the authors vividly portray the brutality and
inhumanity of modern humanism.

Schlossberg, Herbert. /dols for Destruction (Nashville, TN: Thomas
Nelson Publishers, 1983). Clearly one of the most important books
of the last decade, this work simply cannot afford to be passed over.
Schlossberg maintains that humanism is nothing more than a sleek
and updated form of idol worship. His thesis is borne out as he
takes his readers from one undeniable cultic activity to another in
the sacred groves of cosmopolitan American culture. With judg-
ment inevitable for all idolatrous nations, he then issues forth with
an urgent plea for repentance and reconstruction.

The Church In Ministry

Dallimore, Amold. C. H. Spurgeon (Chicago, {L: Moody Press,
1984). This is one of the best popular biographies written about the
remarkable Victorian preacher who was able to institute one of the
most comprehensive local church ministries in modern times.

Fletcher, William M. The Second Greatest Commandment (Colorado
Springs, CO: NavPress, 1983). This book is a profound and Bibli-
cal call to a personal and corporate life of caring.

Jordan, James B., ed. Christianity and Civilization: The Reconstruc-
tion of the Church (Tyler, TX: Geneva Ministries, 1985). This
insightful symposium examines a whole host of important areas of
concern to church ministry.

Kuiper, R. B. The Glorious Body of Christ (Grand Rapids, MI:
