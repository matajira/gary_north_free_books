212 BRINGING IN THE SHEAVES

The army of Christ is invincible: we are not fighting in
mere human power, but with weapons that are “mighty in
God” (Ephesians 6:10-18), divinely powerful, more than
adequate to accomplish the job. With these weapons at our
disposal, we are able to destroy everything that the enemy
raises up in opposition to the Lord Jesus Christ. “We are
taking every thought captive to the obedience of Christ’:
Christ is to be acknowledged as Lord everywhere, in every
sphere of human activity. We are to “think God’s thoughts
after Him” at every point, following His authoritative
Word, the Law book of the Kingdom . . . The goal of our
warfare is total victory, complete dominion for the King-
dom of Christ. We will not settle for anything less than the
entire world.+

Our commission demands that we bring the Gospel of grace to
men and nations, making disciples and “teaching them to obey
everything” that Christ has commanded (Matthew 28:19-20).

Thus, our commission has not been fulfilled, nay, it has yet to be
undertaken, if we do not bring the message and the practice of charity
first to the church, and then beyond to “the uttermost parts of the
earth” (Acts 1:8).

Though ! speak with the tongues of men and of angels, and
have not charity, I am become as sounding brass. or a
tinkling cymbal. And though I have the gift of prophecy,
and understand all mysteries, and all knowledge; and
though I have all faith, so that I could remove mountains,
and have not charity, I am nothing. And though I bestow
all my goods to feed the poor, and though I give my body
to be burned, and have not charity, it profiteth me nothing.
Charity suffereth long, and is kind; charity envieth not;
charity vaunteth not itself, is not puffed up, doth not
