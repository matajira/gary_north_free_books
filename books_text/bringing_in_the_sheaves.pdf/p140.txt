T have six faithful serving men
Who taught me all I know.
Their names are what and where and when
And how and why and who.
Rudyard Kipling

 

Unwilled observation is soon satiated and goes to sleep. Willed
observation, vision with executive force behind it, is full
of discernment and is continually making discoveries which keep
the mind alert and interested. Get a will behind the eye and
the eye becomes a searchlight, the familiar is made to disclose
undreamed treasure.
Robert Traina

 

Peering into the mists of gray
That shroud the surface of the bay
Nathing I see except a veil
Of fog surrounding every sail.
Then suddenly against a cape
A vast and silent form takes shape.
A great ship lies against the shore
Where nothing has appeared before.
Who sees a truth must often gaze
Into a fog for many days;

Tt may seem very sure to him
Nothing is there but mist-clouds dim.
Then suddenly his eyes will see
A shape where nothing used to be.
Discoveries are missed each day
By men who turn too soon away.

Clarence Edward Flynn

 
