44 BRINGING IN THE SHEAVES

poverty, including Michael Harrington, author of the influential
book, The Other America. and Joseph Califano, later a chief aid to
President Jimmy Carter, forged a new and invincible consensus.

This new consensus decried the old consensus as “harsh,”
“unrealistic,” “insensitive,” and “discriminatory.” Rejecting the
notion that poverty was in any way connected with individual or
familial irresponsibility, the new consensus adamantly asserted that
poverty was the fault of the system. Environment was the problem.
Society was to blame.

Thus, society must be made to do penance.

One day, Califano called reporters into his office ut the White
House to explain the President’s legislative initiative increasing social
welfare spending. He told them that, contrary to conservative rhetoric
by then-Governor Ronald Reagan, a government analysis had shown
that only 50,000 people, or 1% of the 7.300.000 people on permanent
welfare were capable of being given skills and training to make them
self-sufficient. Of the other 12,000,000 people on temporary welfare
programs, only about half were trainable, he said.?> Quite a dismal
situation for this, the Land of Opportunity.

He went on to suggest that since society is to blame for creating
such a mess, programs must be developed that go beyond equality of
Opportunity. Programs must be developed that will insure equality of
outcome.

This was the new voguc, the new consensus. Any and all other
persuasions were quickly labeled “greedy,” “racist,” “unChris-
tian,” and “unjust.” Any public officials or political candidates
daring to pass judgment on the effectiveness of the massive federal
giveaways forged by the new consensus were then, and even are now,
bludgeoned with the so-called “fairness issue.” If they persisted in
their obstinate nonconformity, they were made to drink the wrath of
near universal rejection. They were made out to be the enemies of the
elderly, the dispossessed, and the sacrosanct dolations of Social
Security and Medicare.
