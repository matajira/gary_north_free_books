48) BRINGING IN THE SHEAVFS

their children can reccive benefits simultaneously from as many as
seventeen different programs:
1. The child nutrition program
The food stamps program
. The special supplemental food program
. The special milk program

 

The lower-income housing as
. The rent supplements program

. The public health services program
. The Medicaid program

ance program

. The public assistance grants program

. The work incentive program

. The employment services program

The financial assistance program for clementary and
secondary education

13. The public assistance services program

14. The human development services program

15. The action domestic care program

16. The legal services program

17. The community services program

FHS Oeaeneaubkwn

a=

Overlap is practically universal among the forty-odd welfare
assistance programs, since only five of them limit eligibility on the
basis of participation in other programs.4* But even then, when
overlap is considered. recipients are usually not turned away. In fact,
many of the programs, including the basic cash subsidy programs like
Aid for Families with Dependent Children (AFDC). Supplemental
Security Income (SSD, Social Security, and Unemployment Insur-
ance Compensation, actually will encourage applicants to multiply
their benefits by applying for any und all overlapping programs.

Lachetle’s mother always used to say, “If the government’s

 

gonna be givin’ it away, we might as well be in on the gettin’.
But, by the time she was 26, Lachelle had made the startling
discovery that life on the dole was not all it was cracked up to be. Her
