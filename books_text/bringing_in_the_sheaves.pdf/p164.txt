164 BRINGING IN THE SHEAVES

mapping out strategies, tactics, goals, and agendas; and for mobiliz~
ing families, coordinating businesses, and informing the media. In
short, the local church task force would spearhead the full implemen-
tation of the Good Samaritan faith.

The next step in organizing for Biblical charity is the formation
of acommunity coalition. Once a church has established a task force,
and a functioning model of compassion has been implemented, other
interested churches and organizations can be coordinated to share the
burdens of charity, extending the societal impact far beyond what any
single church could accomplish. Joint projects, like food banks,
emergency shelters, social service centers, and computer networks,
could then be effectual.

Finally, in organizing for Biblical charity, provision will need to
be made for those uniquely complex cases where special facilities or
resources are required. A system or network of referrals will enable
each ministry to focus on its own peculiar area of expertise without
fear that needs will go unmet.

Of course, there is always the risk that, with organization,
impersonalization will reduce our compassion to a cold bureaucratic
dotation. So, although haphazardness is to be avoided at all costs, we
must be no less wary of over-organization.

Charity must be organized in order to be effective. But it must be
personal in order to be Biblical.
