128 BRINGING IN THE SHEAVES

ers. They had the expertise, the space, and the resources.” The
Samaritan Food Project is a prime example of how churches can
motivate families who can then mobilize community forces to help
the poor Scripturally.

In Minneapolis, Minnesota, a once elegant and bustling Francis
Drake Hotel is alive again, this time as a shelter for recovering
alcoholics and homeless people. Dick Danielowski and John Treiber,
both former alcoholics, decided to pool the resources of their family
businesses so that they could better implement the Scriptural mandate
to help the down and out rebuild their lives. “If it hadn’t been for the
Godly concern of others, I probably wouldn’t be alive today,” says
Treiber. “Dick and I and our families just wanted to comfort others
with the very comfort we’d received. So we kind of mixed and
matched our business assets and our family holdings with the con-
stant encouragement of our churches and, well, this is what we came
up with.”

When it first opened in 1926, the old hotel was a glittering 200-
room showcase with a basement supper club. Now, it is a showcase of
a different sort. It, too, is a prime example of how churches motivat-
ing families mobilizing businesses can offer a viable alternative to the
federal welfare system.

In Houston, Texas, executives of the Igloo Corporation teamed
up with a local church charity to provide homeless families with ice
chests, thermos jugs, and pure water containers. According to Igloo
executive, Joe Decker, “Most manufacturers of consumer goods have
large supplies of unsaleable inventory. The items may be seconds,
they may be outdated, or they may just be the wrong color. But,
regardless, the stock must be disposed of somehow.” Most manufac-
turers destroy such inventory. “If churches and other charities could
organize a reputable and efficient distribution system,” he said,
“then I’m sure most of the waste would cease immediately and the
goods could be had by those who probably need them the most.”

Manufactured goods are not the only commodities that would be
