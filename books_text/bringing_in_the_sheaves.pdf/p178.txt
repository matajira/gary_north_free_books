178 BRINGING IN THE SHEAVES

occupation, that trade converts into capital development for both the
family and the community. As a secondary occupation, that trade
forms an especially effectual employment insurance. Second, family
businesses establish habits of discipline and diligence, so crucial for
any successful endeavor in the free marketplace. Third, family busi-
nesses promote self-motivation and incentive for achievement. Fam-
ily involvement in enterprise does not create an artificial or makeshift
togetherness, but community, based on shared goals, shared pri-
orities, and shared labor. Fourth, family businesses affirm and con-
firm the primary economic values of productivity and efficiency. The
child in a family business learns more, plans better, works harder, and
buys cheaper. The child in a family business is an asset to society. He
is prepared for the future. He is creating a legacy. And, last but not
least, he is making money!

If we would only teach, equip, and facilitate the poor so that they
can begin to establish small, efficient, cost-effective, and labor
intensive family businesses, we would do more for our society's
economic outlook than any number of corporate shakedowns, tariff
restrictions, union confederations, or governmental regulations. Out
of the backroom, from a comer of the garage, off of the living room
floor, or out of the trunk of the car, those tiny family operations could
very well be, in fact, most certainly would be, the most functional and
effective structures of charity in our society.

In Wilmington, Clayton Cooper has instituted a program of re-
education that takes each of these elements into account. Working out
of the Delaware Community Center, he takes the poor off the streets
and helps them find jobs, but only after teaching them everything
from basic reading skills to Scriptural principles. “You can’t just take
a prostitute off the corner and say, ‘Listen, God says you can’t do
this,’ and expect to make much headway,” says Cooper. “You have to
have alternatives. You have to do some re-educating ... .”

Biblical charity is not mere philanthropy. It is doing anything
and everything necessary to enable the poor to stand on their own, to
