130 BRINGING IN THE SHEAVES

as dock hands, or perhaps even serving in the merchant marine. But
jobs were few and far between. The city was deeply depressed. The
corruption and decay of Reconstruction had left the city treasury
thoroughly depleted, so the streets, the docks, the harbor, the sewers,
and the financial district had fallen into near ruin. That, of course,
inhibited business growth and reinvestment. For a time, it looked as if
Charleston, once “the emerald of the South,” would go the way of
Carthage and Troy, and slowly die. But Langdon Lowe had other
ideas.

The former Confederate colonel had been converted to faith in
Christ during the great revival among the Southern armies in 1863.
Over the next eighteen years, the dashing Southern gentleman
devoted his energies to church, family, business, and politics, rising
to a place of moderate prominence in the community. But a chance
encounter one evening along the Charleston strand with a destitute
family of nine, forced him to re-evaluate his life completely. Not only
did that disturbing encounter revitalize his Christian devotion, but it
lit a fire of compassion in his heart that would ultimately spark
Charleston's revival.

Lowe began by organizing work crews to repair the streets and
docks. In exchange for a day’s food and shelter, plus a few coppers,
unemployed and homeless workers would gather rubbish, clear away
debris, cut down overgrowth, and do light repairs. Lowe solicited
financial and material assistance from the various benefiting busi-
nesses. Before long, not only were many of the city’s poor working
again, but a full-scale revitalization had begun. Several other groups
similar in scope to Lowe’s sprang up, and suddenly Charleston was a
hive of industry, activity, and prosperity.

Just three years after beginning his ambitious private initiative
program of Biblical charity, Langdon Lowe died. But the legacy he
left Charleston and all Christendom lives on, “he, being dead, still
speaks” (Hebrews 11:4). His diary, first published in 1896, is not only
a classic glimpse into the spiritual vitality of this Southern Pres-
