224 BRINGING IN THE SHEAVES

land. With nearly 150 “care committees,” and a staff of over 30 state
and regional dircctors in federal and state institutions throughout the
country, Prison Fellowship has made a dramatic impact on the Bibli-
cal charity movement.

Voice of Calvary Ministries (VOC)
P.O. Box 10562
Jackson, Mississippi 39209

Voice of Calvary Ministries was founded in 1960 by John Perkins
to reach the needy black people in the South with the Gospel of hope.
Starting from the base of a local church, the ministry went on to
establish businesses, co-ops, clinics, schools, day-care facilities, and
legal service agencies. Today, there are a number of innovative
Biblical charity projects that VOC has spawned. Among them are
“People’s Development, Inc.,” a housing cooperative that buys and
renovates deteriorating homes, which are then rented or resold to the
poor; “Thrifteo” is a developing network of thrift stores located in
poor areas; and, “The John Perkins International Study Center” has
recently been established to train Christians for work in community
development. Heralded far and wide as one of the best examples of
Christian social action, VOC has demonstrated, in the tough realm of
daily obedience, the masterful and majestic efficacy of Biblical
charity.
