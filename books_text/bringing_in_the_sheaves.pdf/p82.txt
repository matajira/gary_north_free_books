82 BRINGING IN THE SHEAVES

Other gleaners groups, like Goodwill Industries, the Salvation
Army, and Light and Life Resale Shops, collect discarded com-
modities and then repair them for sale by using unemployed and
handicapped workers. And groups like the Humble Evangelicals to
Limit Poverty (HELP) in Texas and the St. Vincent de Paul Society in
New Hampshire have put unemployed workers out on the city streets
cleaning up litter, rubbish, and overgrowth in exchange for groceries.
All without federal subsidies. All without bureaucratic interference.
As R. J. Rushdoony has pointed out, “The rise of welfarism has
limited the growth of urban gleaning, but its potentialities are very
teal and deserving greater development.”"?3

The third principle that emerges from the story of Ruth is that
Biblical charity is discriminatory (Ruth 2:7). Biblical charity knows
nothing of promiscuous handouts to sluggards. “The lazy and
improvident,” David Chilton has said, “could expect no saving
intervention by a benevolent bureaucrat.”’24 If he worked, he ate. If
he chose to laze about, then he and his family went hungry. Biblical
charity discriminates.

Discrimination. Just mention the word and suddenly visions of
bigotry, pogroms, and stiff-necked lovelessness dance in our heads.
But Scripture teaches that discrimination, far from being a villainous
vice, is very often a venerable virtue. Our confusion comes when we
automatically associate discrimination with racism, unfairness, and
oppression. But whereas the Bible explicitly condemns racism,
unfairness, and oppression, it condones discrimination.

The dictionary defines discrimination as “making a clear dis-
tinction, to differentiate.” Thus, to discriminate Biblically is to make
distinctions and to differentiate utilizing God’s unchanging Law as
the standard. As such, discrimination is nothing more than the fruit of
discernment. While the racist may abuse discrimination by judging
the world around him in accord with his vile prejudices, the Christian
is called to exercise spiritual discernment, godly discrimination,
judging the world around him in accord with God’s unerring Truth.
