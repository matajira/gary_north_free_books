AGENCIES AND ORGANIZATIONS 223

For years, ICE has been the pioneer in producing books, news-
letters, and practical manuals on the Scriptural application of eco-
nomic principles. Both Gary North and David Chilton have
contributed significandy and constructively to the theology of Bibli-
cal charity as espoused in this book.

Operation Blessing
10000 Old Katy
Houston, TX 77055

Growing out of the nationwide media ministry of Pat Robertson
and the CBN’s 700 Club, Operation Blessing is an extraordinary
network of Biblical charity outreaches. Utilizing local church
resources and community coordination, volunteers distribute emet-
gency food, clothing, and medical aid to the needy both at home and
abroad. Many branches also provide financial aid in crisis situations
to the homeless and the unemployed. With offices in virtually every
section of the nation (wherever the 700 Club is aired), Operation
Blessing has a tremendous potential to impact the shape of charity
work over the next century.

Prison Fellowship
P. O. Box 40562
Washington, D.C. 20016

Chuck Colson, following his own prison term in the aftermath of
the Watergate break-in, founded Prison Fellowship in 1976, to pro-
vide Biblical fellowship and compassion to inmates all over the
United States. The ministry is active in the prison reform movement
and, thus, is intimately involved in the reconstruction of Biblical
standards in both the penal codes and the judicial structures of our
