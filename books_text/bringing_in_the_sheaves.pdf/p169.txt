THE BOOTSTRAP ETHIC 169

layoffs and permanent plant closings in basic U. S. industries as well.

First, in many industries such as steel, rubber, and automobiles,
skyrocketing labor costs and a lack of effective domestic competition
have pushed prices ever upward, In fact, many such industries, steel
in particular, have priced themselves out of the world, and even the
domestic market.

Second, the industries have often failed to invest in more effi-
cient manufacturing methods. For example, even in the face of stiff
Japanese competition in the late ’60s and after the imposition, in
1969, of “‘voluntary” restrictions on steel imports, U. S. steel indus-
try capital expenditures through 1974 remained befow 1968 levels.>
Meanwhile, Japanese stcel producers were increasing capital invest-
ment by more than 23% per year.

Part of the problem has been that management, held hostage by
labor, has been unable to modernize. Jurisdictional battles between
craft unions during times of slow- or no-growth have led to unin-
formed resistance to changes designed to improve productivity, and
thus to lower unnecessarily high construction costs. But then, part of
the problem has simply been poor management. Those in the higher
echelons have plainly not been responsive enough to the changing
climate of the world market.

Third, the “protectionist” policies of the federal government
have contributed to industrial decline by failing to tie subsidies, tax
allowances, and trade barriers to enforceable commitments by indus-
try to upgrade facilities, and improve efficiency and productivity.

Most subsidies to “declining” industries have been based on
emotional appeals by industry and labor to save jobs. However, most
of the firms only perpetuated or, in some cases, intensified the
downward spiral of layoffs and plant closures.

Tronically, the import controls, tax breaks, and regulatory relief
measures that the government has instituted. at industry’s behest,
have actually pushed countries like Japan and West Germany more
quickly into higher technology products and into higher cost con-
