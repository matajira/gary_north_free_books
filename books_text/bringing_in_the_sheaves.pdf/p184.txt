184 BRINGING IN THE SHEAVES

environmental impact statement on pilgrim removal. In triplicate, of
course.

Upon receipt of EPA approval, Help then would have been
required to conduct a sectional opinion survey or, perhaps, call for a
community-wide referendum, thus securing permission from the
citizenry to undertake such a bold course of action.

Next, he would have had to add to his retainer a lawyer, to protect
him from criminal and/or civil liabilities, a press secretary, to sched-
ule all future media appearances, and a literary agent, to find the best
market for his “life story,” tentatively entitled Slough, Great Thou
Art.

Finally, since he was a devout man, he would have had to return
to his prayer closet in order to ascertain rightfully “God’s will” in the
matter.

Meanwhile, of course, Christian would have expired in the
slough, thus writing a premature and an entirely unsatisfactory end-
ing to the tale.

Somehow we have complicated even the simplest of human
transactions. Deals are no longer sealed with a handshake; they are
dependent upon clause after clause of legalese. Marriages are no
longer bound by vows; they are consummated by bi-lateral property
contracts. Helping is no longer a matter of neighborly concern; it is
stipulated, conditioned, and administered by legislation and litiga-
tion.

Humanism’s grand scheme has backfired and, as a result, our
society is less human than ever before.

Biblical charity acts as an immutable humanizing force in the
midst of such modern inhumanity. Biblical charity reaches across all
barriers and defies all odds to rescue, without any delay, those caught
in the sloughs of despond and deprivation. Biblical charity extends a
steady, ready hand in times of need.

Now is one of those times. Need grips our land. Job oppor
tunities and re-education programs offer long-term solutions for that
