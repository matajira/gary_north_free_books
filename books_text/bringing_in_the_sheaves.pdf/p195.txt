CHAPTER 12

 

No Room at the Inn

viction. It’s not a pretty sight.

A helter-skelter of chairs, tables, clothes, dishes, and toys
heaped irreverently on a sidewalk as an angry landlord and distraught
constables confront a former tenant, even angrier and more distraught
than they: this is eviction. And, sadly, though not a pretty sight, it is
an all too common sight.

According to a recent congressional study of homelessness,
evictions have increased a hundred-fold over the last five years. Even
the recovery-enhanced 1984 saw a 28% jump in the number of tenant
expulsions.!

Theresa Walden, a regional manager for Harold Farb properties,
the nation’s largest apartment developer, reported, ‘In the past, most
of our evictions have involved irresponsible tenants. They were
either loud, or destructive, or chronically late with rent payments.
But the recent spate of evictions has involved families that have
always been responsible before, they’ve just fallen on hard times:

195
