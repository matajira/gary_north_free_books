POOLING RESOURCES 163

Memphis that accepts only “black men from the ages of 45-65 who
suffer from alcoholic debilitation and homelessness.’ Another shel-
ter in Lincoln limits its aid to “‘battered women, with children,
between the ages of 25 and 40.” One halfway house in Spokane
stipulates, ‘‘trucking industry personnel and former Viet Nam War
veterans with serious chemical dependencies.” Obviously, any out-
reach to the poor or afflicted needs to have a narrow focus so that
effective care and counsel can be provided. But the trend toward over-
specialization has fragmented the social service coverage in the
United States into an abominably complex jigsaw puzzle. “It has
almost gotten to the point,” says George Getschow of The Wall Street
Journal, “that you have to be poor plus something to get help from a
private charity: poor plus addicted to drugs, or poor plus mentally ill,
or poor plus a member of the pipefitters union.’’8 Specialization is
fine as long as it does not work to the exclusion of those we have been
called to help.

Task forces, community coalitions, and referral lists are invalu-
able aids to implementing Biblical charity. But, we must keep them in
perspective. We must be organized, but never so organized that we
lose the sensitivity, the accountability, and the individuality so central
to the Good Samaritan faith.

Summary

Organization is essential. If our charity outreaches are to be truly
effective in relieving poverty, we must plan. We must mediate,
motivate, and mobilize. We must establish priorities, set goals,
formulate tactics, coordinate resources, and build networks of coop-
eration.

The first step in organizing for Biblical charity is to establish a
local church task force. This core group, committed to living out the
full implications of the Good Samaritan faith, would be responsible
for initiating a demographic operation; for educating, motivating, and
training the congregation to fulfill its Scriptural obligations; for
