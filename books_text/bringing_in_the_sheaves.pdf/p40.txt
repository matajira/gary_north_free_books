40 BRINGING JN THE SHEAVES

Army, the Navy, and the Air Force.4 In fact, its budget had grown to
be the third largest in the world, excceded only by the entire budget of
the United Siates government and that of the Soviet Union.> The
Department came to supervise a gargantuan empire reaching every
community in the nation, touching every life.

The ‘“‘war™ strategy developed by the HEW involved the crea-
tion or expansion of well over 100 social welfare agencies.© Their
grab-bag efforts included major programs like Social Security, unem-
ployment insurance, Medicare, Medicaid, Aid to Families with
Dependent Children (AFDC), Supplemental Security Income (SSI),
food stamps, and a myriad of minor ones, including special supple-
mental feeding for Women, Infants, and Children (WIC), the Inten-
sive Infant Care Project (LCP), rent supplements, urban rat control,
and travelers’ aid.

By the time President Johnson relinquished the reins of power to
Richard Nixon in 1969, the war on poverty had assumed an immuta-
ble, untouchable position in the federal agenda, consuming a full
25% of the Gross National Product? and employing one out of every
100 Americans in one way or another.* Even when the new president
appointed conservative ideologues like Howard Phillips and William
Simon to positions of authority, liberal crusaders had little to fear. The
poverty programs had acquired such political clout that they not only
survived, they thrived. In fact, the post-Johnson war on poverty saw
the greatest increase in social welfare services since the Great Depres-
sion.?

The food stamp program, for instance, began in 1965 with less
than a half million beneficiaries.'!0 By 1968, the number in the
program had quadrupled.!' Under Nixon, that number was again
quadrupled.!? And, by 1980, the number of beneficiaries had grown
to 21.1 million, !> fifty times the coverage of Johnson's original war on
poverty legislative package.

Though the socio-economic dogma of the HEW’s legions gained
consensus in Washington’s corridors of power, it was not without its
