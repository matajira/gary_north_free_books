EQUIPPING FAMILIES 121

ate. They would coordinate. They would network. As a people
bonded together by the covenant of God, they would see alt their
work, all their ministries, and all their responsibilities in terms of the
whole, in terms of the many. As a priesthood, and not just a collection
of isolated priests, each going separately to God, they would be a
community of priests. They would be priests to each other. Mono-
lithic problems like hunger and homelessness would not be teft to a
catch-as-you-can haphazardness, but would be confronted by the
unified resources and the coordinated faithfulness of all the families
of the covenant.

Third, Christians would begin to look outward with optimism
once again. They would comprehend that priesthood is not just for the
internal life of the church: it is for the world. They would see
themselves as ambassadors. They would move out to claim the earth
for their Master and King. They would become Kingdom-conscious
instead of program-conscious. They would begin to measure success
by the standard of Scripture instead of the standard of the world, and
thus would willingly commit to difficult, multi-generational tasks
like the total reconstruction of social welfare. Knowing that a
priesthood has the responsibility of both representing God to men and
men to God, they would begin to minimize petty differences and
move with confidence toward dominion.

The One and the Many

The practice of the priesthood of believers is a practical reflection of
the Biblical balance between the one and the many.!? It is the
application of both individual responsibility and corporate life. What
this means for Scriptural compassion should be obvious: families are
the primary agents of charity in society, but anarchy need not reign.
Families are responsible and accountable to God to live out the Good
Samaritan faith, but they need not tackle poverty alone, isolated and
uncoordinated. God’s people are called together in the New Covenant
(Hebrews 7:22). They are to act covenantally (2 Corinthians 3:6).
