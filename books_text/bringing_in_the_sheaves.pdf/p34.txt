34 BRINGING IN THE SHEAVES

were hard-working people.

These new poor are now crowding into public shelters and soup
kitchens to the point where they often outnumber the bums and
shopping-bag ladies, who, for years, have had the charities mostly to
themselves.

“Yn Tulsa,” says Roland Chambless, the Salvation Army com-
mander there, ‘most of the people we fed a year ago were derelicts
and alcoholics, but today it’s mothers and small children.””

Sergeant E. D. Aldridge of the Houston Police Department’s
Special Operations Division, has said, “It used to be that most of the
homeless on the streets were alcoholics and things like that. Now, if
you talk to them, most seem quite intelligent, middle-class types.
They’re just fiat out and down on their fuck.”

A recent New York City survey of those staying in shelters there
found an extremely high percentage of families and first-time appli-
cants. Half of the men were high school graduates and 20% attended
at least some college. They were primarily middle-aged secretaries
unable to find work, young construction workers who hadn’t worked
in months, and laid-off department store clerks who had never been
unemployed before.27

Gary Cuvillier, who operates a family shelter in New Orleans,
says, “Most of the folks we deal with day in and day out are from the
fringe of the middle class. Many owned homes before the big lay-
affs. None had ever known real want before. What we’re seeing is a
change in the structure of American society, so fundamental that no
one will remain unaffected.”

“Lots of fong-time indigents are landing in the streets,” says
Michael Elias, who administrates a shelter near Los Angeles. “But so
are a whole new class of people . . . families from Michigan and
Ohio . . . middle-class people . . . it’s a tragedy.”

The Invisibility of the Poor
So, where are they? If there really is starving in the shadow of plenty,
