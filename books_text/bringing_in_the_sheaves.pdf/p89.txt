PART III:

 

THE STRATEGY

The batile plans were laid before Joshua, long before he
took to the field of Jericho. The plans were enscribed upon
time’s scroll by the holy predetermination of the Sovereign

Lord. Joshua had but to follow, to obey. Indeed. no less could
be said of the plans taid before us. We have but to follow, to obey.
Such is the strategy.

Langdon Lowe (1882)

 
