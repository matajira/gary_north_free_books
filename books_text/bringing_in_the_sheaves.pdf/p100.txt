100 BRINGING IN THE SHEAVES

(Romans 12:2).

God’s way is excellent: “As for the Lord, His way is perfect,”
and, “The Word of the Lord is tried” (2 Samuel 22:31).

And so the story goes, on and on throughout the Scriptures, a
never-ending hymn of praise to the excellence of the Living Lord.

Now, if we are to follow after Him, and we are (Matthew 4:19); if
we are to be of the same mind as He, and we are (Philippians 2:5), if
we are to walk in His footsteps, and we are (1 Peter 2:21); if we are to
emulate His very attributes, and we are (1 Peter 1:16), then it only
stands to reason that excellence must be a universal distinguishing
characteristic of disciples of the Lord.

The fact is that the same God who demanded excellent sacrifices
(Malachi 1:8-10); the same God who demanded artistic excellence
(Exodus 28:2); the same God who demanded cultural excellence
(Genesis 1:28); the same God who demanded evangelistic excellence
(Matthew 28:18-20): the same God who demanded economic excel-
lence (Matthew 25:14-30), demands that you and I manifest some-
thing significantly more than the current status quo of mediocrity.

Elton Trueblood, the esteemed Quaker scholar, has noted that
“Holy shoddy is still shoddy.” And there is no room for any shoddy
in the glorious Kingdom of our God and King. Isn’t it about time we
acknowledged as much by striving for excellence in our churches? In
our preaching? In our thinking? In our work on behalf of the poor? We
must motivate our congregations, not just to action, but to effective
action, excellent action.

A Missions Strategy

Obviously, a full-scale frontal assault on complacency and reticence
in our congregations will be required if Biblical charity is to be
implemented with any real success, if optimistic, militant Biblical
worldviews are to be ingrained to any degree. The Good Samaritan
faith is not easily nurtured. Congregations are not easily catalyzed.
Nothing less than a missions strategy that encompasses every aspect
