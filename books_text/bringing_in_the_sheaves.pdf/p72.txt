72 BRINGING IN THE SHEAVES

disciplines, then they included them in their regular daily routine. If
the Bible advocated a particular approach to relief of the poor, then
they faithfully followed suit. Being mere men, they lacked perfect
understanding, of course, so they made mistakes and they exercised
poor judgment at times. But on the whole, they stuck to their convic-
tion that in Scripture, and in Scripture alone, could the blucprint and
safeguards for a wholesome society be found. Thus, undergirding all
their efforts was the willingness to obey carefully the dictates of
Scripture (Joshua 1:8), and the restraint never to go beyond its bounds
(1 Corinthians 4:6). Such was the faith of our Good Samaritan
forebears. And such must ours be if the Great Commission is to be
fulfitled in any measure.

To many Christian liberals, ail talk of applying Scripture in
blueprint fashion to the problem of the poor is scorned as woefully
antiquated. They much prefer the present insanity of government
welfare. Similarly, tenured humanists dismiss the Biblical plan of
telief as dangerously demented. So, well might we at this point adopt
as our own the words of R. L. Dabney, first uttered over acentury ago,

A discussion of a social order now totally overthrown...
will appear as completely out of date . . . as the ribs of
Noah’s ark, bleaching amidst the eternal snows of Ararat,
to his posterity, when engaged in building the Tower of
Babel. Let me distinctly premise that I do not drearn of
affecting the perverted judgements of the great . . . party
which now rules the hour. Of course, a set of people who
make success the test of truth, as they avowedly do in this
matter, and who have been busily and triumphantly
engaged for so many years in perfecting a plain injustice,
to which they had deliberately made up their minds, are
not within the reach of reasoning. Nothing but the hand of
retributive Providence can avail to reach them, The few
among them who do not pass me by with silent neglect, I
