BIBLICAL CHARITY 75

may be fairly reduced to four points:

First, the Bible teaches that all honorable work is holy, “A man
can do nothing better than find satisfaction in his work" (Ecclesiastes
2:24; 3:22). Far from being a bitter consequence of the Fall, work isa ©
vital aspect of God’s overall purpose for man in space and time. For
that reason, He has typically used workmen, ordinary laborers, in the
enactment of that purpose. He has used shepherds like Jacob and
David. He has used farmers like Amos and Gideon. He has used
merchants like Abraham and Lydia. He has used craftsmen like
Aquilla and Paul. He has used artists like Solomon and Bezalel. And
the men He chose to revolutionize the Roman Empire in the first
century were a motley band of fishermen and tax collectors. The great
Puritan, Hugh Latimer, best captured the Biblical emphasis on the
holiness of man’s work when he wrote,

Our Saviour, Christ Jesus, was a carpenter and got his
living with great labor. Therefore, let no man disdain . . .
to follow Him ina . . . common calling and occupation.”

The Fourth Commandment, though commonly and correctly
understood as prohibition against working on the Sabbath, has
another all-too-often neglected injunction: “Six days you shall labor
and do all your work” (Exodus 20:9). And so Richard Steele, another
of the great Puritans, could confidently write that it is in the shop
“where you may most confidently expect the presence and blessing of
God. Work is holy unto the Lord, ordained by His immutable Way.””®

Everyone, even the partially disabled, reaps honor from indus-
trious, productive work.

Second, the Bible teaches that God calls each person to his or her
work. “‘There are different kinds of gifts, but the same Spirit. There
are different kinds of service, but the same Lord. There are different
kinds of working, but the same God works all of them in ali men” (1
Corinthians 12:4-6). The doctrine of calling was once the cornerstone
