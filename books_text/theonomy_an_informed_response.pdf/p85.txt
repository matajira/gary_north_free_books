Fear of Flying: Clipping Theonomy's Wings 65
A Plane With Wings

The odyssey did not stop with Schaeffer. Schaeffer asked the
question of how should we then live; it was left to others in the
Reformed tradition to answer it.

A Pair of Calvinists

Gary North came to the RTS campus in 1978 to address the
topic of cconomics in an informal debate with Richard Mouw of
Calvin Seminary. (He is now at Fuller Seminary.) The differ-
ences could not have been more striking. Dr. North stayed with
the Bible. One thing I do remember about Dr. Mouw’s address
is that he said that when he gets to heaven, he will finally have
time to read the works of Karl Marx. Sounds like hell to me.

One of Dr. North’s messages had a singular impact on me.
North was demonstrating the reformed methodology as it relat-
ed to economics. His text was Isaiah 1:21-23, and the topic was
“A Biblical Critique of Inflation.” Keep in mind that this was
the era of dollar inflation and double-digit interest rates. The
economy was in “stagflation.” This double economic whammy
was affecting the economy with not much hope for a solution.
Gold and silver prices were rising because of inflation fears. We
were warmed by Dr. North of what would happen if God’s laws
were rejected, Sure enough, the “predictions” came to pass. By
1980, silver was selling for $50.00 per ounce while gold was
selling for more than $800.00 per ounce. Interest rates were
nearing 20%. Does the Bible have anything to say about any of
this? Dr. North said it does. Little was said by the faculty.

The passage in Isaiah 1 is an application of the case laws
regarding just weights and measures (Lev. 19:36; Prov. 11:1;
16:11). The people and rulers alike resembled the debased
silver that was being passed off as pure and the diluted wine
that was being sold as uncut. Of course, under such economic
conditions the poorest members of society, orphans and widows
(v, 23), suffer the most. In just a few verses was found a specific
