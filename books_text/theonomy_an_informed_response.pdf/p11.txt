Editor’s Preface xi

Actually, they started returning in force after 1964, as I
indicated in my books, Nene Dare Call It Witcheraft (1976) and its
update, Unholy Spirits (1986). The rise of occultism in the West
has been spectacular since 1965. It has accompanied the advent
of a far more consistent humanism than existed before Presi-
dent Kennedy was killed. The can-do technocratic liberalism of
the New Deal era did not survive the rise of radical relativism,
occultism, the counter-culture, and New Age theology. It did
not survive the Vietnam War.

It is remarkable that the modern humanist is more ready to
acknowledge the mythical status of the neutrality doctrine than
Christians are. The educated humanist may know a little about
what quantum physics did to the Newtonian worldview after
1924. He knows, if only second hand, something about the soci-
ology of knowledge. He may have read Thomas Kuhn's Struc-
ture of Scientific Revolutions (1962), a book dealing with the histo-
ry of post-Newtonian natural science, which became a kind of
epistemological Bible for younger humanistic social scientists
after 1965. In contrast, the Christian apologist is still a wide-
eyed tourist in the epistemological equivalent of Walt Disney
World’s Newtonland, where all rational men know that two plus
two equals four, and numerical relationships govern the exter-
nal world for no apparent reason.

And just like Disney World, everything is swept clean daily.

Westminster Seminary in 1963

I entered Westminster Theological Seminary in the fall of
1963, about ten weeks before the assassination of President
Kennedy. I was a hyper-dispensationalist who believed in pre-
destination. So, why did I choose Westminster? For two rea-
sons. First, because it had the reputation of eloquently defend-
ing the inerrency of Scripture. Second, because it had the repu-
tation of being the most academically rigorous Bible-believing
theological seminary in the English-speaking world. In short, it
was Westminster's ability to deal with texts: the Bible’s and
