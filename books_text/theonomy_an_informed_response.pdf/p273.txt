Whose Conditions for Charity? 253

answer to the crucial question: Who pays? It is extremely naive
to think that this benevolence will be given without expecta-
tions, t.e., votes.

What about the taxpayers who are paying for it? For the
state to operate as a modern welfare state, it will have to tax
more than the tithe of the church, as every model socialist
society has demonstrated, including our own. The middle class
will have to be robbed to pay for the poor and their Civil Ser-
vice-protected handlers. Conditions are placed on someone.
The history of the welfare state reveals that its mythological
“unconditional benevolence” is far more conditional than judi-
cially conditional Christianity is.

The Task of the Church

This brings me to the possibility of unconditional church
benevolence. Let’s think this situation through. Someone always
gets sanctioned or ends up with conditions being placed on him
or her, If the church is giving away money in a non-discrimi-
natory fashion, this means less money for the widows and the
orphans in the church. It’s a matter of simple mathematics.

Dr. Keller has created a system whereby those who are under
grace are negatively sanctioned more than those who are outside of
grace. What kind of message does this send to the members of
the church? The elect are really not the elect because there is
no preference given to them. Being in the church means being
equal to an unbeliever. Worse, being in the church means
being less than an unbeliever because at least the unbelicver gets
the “unconditional” benevolence. It is presumably better to be
outside of the church where the “gettin’ is free.” A widow or or-
phan is better off economically not being part of the church.
What kind of message does Mr. Keller's system send to the
world? In an attempt to communicate grace, he has destroyed
a major incentive for coming under grace, The incentive is
gone.
