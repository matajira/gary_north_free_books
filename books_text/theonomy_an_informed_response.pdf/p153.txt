Westminster Seminary on Penology 133

by Johnson before he would begin to have constructed a suc-
cessful argument for the conclusion which he wished to draw.

A Final Fallacy

Before Johnson ends his essay, he exhorts his readers about
Christian involvement in politics and engages in one last notori-
ous fallacy: arguing from what was the case to what should always
be the case.

Johnson makes note of the “politically powerless” situation of
the early church and of the “minimal direction” given in the
New Testament to political rulers (since believers would have
had little opportunity to carry out such directions anyway) ~
stating that both of these things were part of God’s timing and
providence. Johnson tells us “this situation is itself significant”
(p. 191). Significant of what, though?

Johnson claims this situation reflects the sovereign and wise
“design of his [God's] Word, the standard for our faith and life”
— remarkably shifting from the written word as our doctrinal
and ethical standard, to the metaphysical word by which all
events are providentially controlled. And then Johnson boldly
commits the naturalistic fallacy, arguing from what is the case to
what ought to be the case. He makes the “design of [God’s]
Word” - meaning the providential design of God to reveal the
New Testament to a politically powerless church — “the stan-
dard of our faith and life”!

Thus he concludes that the “New Testament’s minimal direc-
tion to governmental officials does not support the view that
the Mosaic penaltics should be enforced by a non-covenantal
government structure. . . .” This kind of thinking is startling
and disappointing. Upon reflection, Johnson would surely
recognize that numerous counter-examples are available to
reduce this kind of theological thinking to absurdity.’® For

19. Indeed, Johason’s co-author, Clair Davis, offers just such a refutation in the
same volume where Johnson's essay appears, Dr. Davis writes: “. . . while this age is
