credentialism, 333-41
cryptic, 58
decline, 329
dispensationalism &, 38
dualism &, 84
ethics, 5
fig leaves, 22
five points, 29
judicial theology, 20-22
Kuyper, 39-44
law, 54-55
muddled, 1788-, 344
non-theonomic, 23
“plane” (see “plane”)
Politics &, 47-49
preaching, 52
Princeton Seminary, 30
rigorous, 4, 29
Rushdoony, 15
scholarship, 327-28
seminary, 334-36
system, 29
theocracy, 86
theonomy &, 23, 26-27,
27-28, 322
“wings,” 39-56
worldview, 29, 39, 41,
50-52, 55-56, 66

Canaan, 276

cannibalism, 316-17

capital, xiv-xv

capital punishment, 114, 127

capitalism, 261

Carter, Jimmy, 67

case law, 5

casuistry, 326

cease-fire, 10

chaos, 72

Index 369

charismatics, 16

charity
application, 246
conditional, 243-45
drug-addict, 231-32
guilt &, 264
State, 249-51

Charles TI, 7

Chilton, David, 16, 234-35,
238, 240

Christ (see Jesus)

Christendom
Calvinism, 86
civilization, 9-10
covenants, 85
denial, 207-8
early church, 324
hermeneutics &, 205
ideal of, 198-99
inevitable, 344
pluralism vs., 198-99
Westminster vs., 9-10,

255

Christianity
America &, 35
pluralism &, 33-34
pluralism vs., 35
special favors?, 34
world &, 91-92

church
centrality of, 204, 206
Christendom, 324
confrontation, 200
continuity, 202
covenant, 8
covenant people, 125
discipline, 297-98
doctrine, 195
