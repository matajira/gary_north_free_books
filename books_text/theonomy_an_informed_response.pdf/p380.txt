360 THEONOMY: AN INFORMED RESPONSE

North, Gary. The Dominion Covenant: Genesis. Tyler, Texas:
Institute for Christian Economics, (1982) 1987. A study of the
economic laws of the Book of Genesis.

North, Gary. Moses and Pharach: Dominion Religion vs. Power
Religion. Tyler, Texas: Institute for Christian Economics, 1985.
A study of the economic issues governing the Exodus.

North, Gary. Political Polytheism: The Myth of Pluralism. Tyler,
Texas: Institute for Christian Economics, 1989. A 700-page cri-
tique of the myth of neutrality: in cthics, social criticism, U.S.
history, and the U.S. Constitution.

North, Gary. The Sinai Strategy: Economics and the Ten Com-
mandments. Tyler, Texas: Institute for Christian Economics,
1986. A study of the five-point covenantal structure (1-5, 6-10)
of the Ten Commandments. Includes a detailed study of why

the Old Covenant’s capital sanction no longer aplies to sabbath-
breaking.

North, Gary. Tools of Dominion: The Case Laws of Exodus. Ty-
ler, Texas: Institute for Christian Economics, 1990. A 1,300-
page examination of the economics of Exodus 21-23.

North, Gary. Victim’s Rights: The Biblical View of Civil Justice.
Tyler, Texas: Institute for Christian Economics, 1990. An ex-
tract from Tools of Dominion.

Rushdoony, Rousas John. The Institutes of Biblical Law. Nut-
ley, New Jersey: Presbyterian and Reformed, 1973. The foun-
dational work of the Christian Reconstruction movement. It
subsumes all of biblical law under the Ten Commandments. It
includes three appendixes by Gary North,
