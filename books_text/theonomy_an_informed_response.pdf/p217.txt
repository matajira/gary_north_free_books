Editor’s Introduction to Part LIT 197

as High Priest. This exclusively ecclesiastical focus has obscured
His continuing office of King of kings in history. His resurrec-
tion and ascension for some reason supposedly have annulled
His status as a King with sanctions in New Covenant history.
Today, He is supposedly a King without any need for interme-
diary, oath-bound civil sanctions. Ile imposes only random
sanctions today, we are assured; He does not impose predict-
able negative sanctions, as He did under the Old Covenant. So,
there is no reason for anointed civil magistrates to take a self-
maledictory oath to Him. They no longer serve as His interme-
diaries, imposing the negative sanctions of civil government so
that God does not impose His own negative sanctions directly.

Once freed from explicitly biblical covenantal restraints, the
state can then impose positive sanctions as an agency of coven-
antally neutral healing. This may not be the opinion of all the
Westminster faculty, but it was surely the opinion of Edmund
Clowney, and it is openly the opinion of his disciples, most
notably Timothy Keller."

Westminster’s faculty members have accentuated the nega-
tive covenantal sanction of excommunication and the positive
covenantal sanction of spiritual healing, but they have cither
downplayed or denied the legitimacy of the negative covenantal
sanction of the sword. They have publicly stripped Jesus Christ of
His kingdom in church history. This is necessarily what all amillen-
nial and all premillennial schemes do, and members of the
faculty are all amillennialists. Jesus is seen by them as King only
on the day of final judgment: “King of dead kings, and Lord of
dead lords.” This theological position is expressed most forth-
rightly in Dennis Johnson’s essay, but it is the underlying judi-
cial presupposition of every essay in the book except the essays
by Frame and Poythress. (Frame and Poythress believe that
Jesus is kind of a King in history and sort of a Lord, from a

1. Timothy J. Keller, “Theonomy and the Poor: Some Reflections,” Theonomy: A
Reformed Critique, ch. 12.
