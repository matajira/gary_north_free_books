Hermeneutics and Leviticus 19:19 263

and more the responsibility of the king and the nation rather
than the local lord of the manor. Loyalties shifted accordingly,
especially in the cities. Lampard writes: “The result was a new
social division of labor in which property rights played a more
decisive role than personal obligations in determining the divi-
sion of the social product. Property rights as a claim on the
material means of existence provided the institutional founda-
tion, if not the psychological mainspring, for a commercial,
acquisitive society.”* This institutional transformation was not
confined to Great Britain. Because of this, once England had
shown the way, the industrial revolution spread within two
generations throughout Northern and Western Europe, and
also to North Amcrica. By 1830, it was a common Northern
European phenomenon.

Population Growth

The most statistically relevant aspect of the era of the indus-
trial revolution in England was the growth of population. In
the year 1700, there were about five and a half million people
in England and Wales. By 1750, it was six and a half million.
By 1801, it was about nine million, an unprecedented increase
of 60 percent. By 1831, population had reached fourteen mil-
lion. This was not duc to an increase in the birth rate.®> It was
also not due to immigration. During the eighteenth century, as
many as a million people left Great Britain for the colonies.**
The cause of the increase in population, 1750-1800, was an
unprecedented reduction in the death rate.

The question is: Was it the industrial revolution that pro-
duced this increase? This seems not to have been the case. A

22, Eric E. Lampard, The Industrial Revolution: Interpretations and Perspectives
(Washington, D.C. Service Center for Teachers of History, American Historical
Association, 1957), p. 12.

28. T.S. Ashton, The Industrial Revolution, 1760-1830 (New York: Oxford Univer-
sity Press, 1948), p. 4.

24. Mhid., p. 5.
