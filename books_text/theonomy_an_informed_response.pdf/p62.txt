42 THEONOMY: AN INFORMED RESPONSE

“divine norms,” the people will “experience peace, justice, and
righteousness in their fullness.”

But exactly how should the Christian define justice and right-
eousness? Is it just and right to tax the citizenry in order to fulfill
the general demands of justice and righteousness, say, in caring
for the poor and educating the people through an educational
system controlled by the state because it is financed by the
state? Liberals and conservatives espouse justice and righteous-
ness, Whose definition is correct? Whose solution should Chris-
tians follow if the pluralist is correct when he maintains that the
Bible cannot be appealed to for specifics, since the “tares” must
be tolerated until the time of the “final harvest”? By what stan-
dard are Christians required by Gad to decide these issues?

Where does the Christian pluralist go for his specific norms?
They are few and far between in the pluralist’s world. For
example, in Gordon J. Spykman’s defense of principled plural-
ism, there is little appeal to the Bible, even under the heading
“Biblical Foundations.” He mentions general norms, but there is
no worked-out judicial system.

Our view of society should not be derived from isolated pas-
sages scattered throughout the Bible. Such a piecemeal approach
assumes that the Bible is a collection of timeless truths with built-
in, ready-made applications for every situation. Rather, the
Scriptures present principles and directives that hold for life as
a whole in every age. We must therefore rely on the comprehen-
sive meaning of the biblical message. Though couched in ancient

forms, the Scriptures carry with them universal norms that should
direct the lives of Christians and shape societies they live in.’

This is doubletalk. Let's rephrase the first sentence in this
quotation: “Our view of the Trinity should not be derived from
isolated passages scattered throughout the Bible.” How about

8, Ibid.
9, Gordon J. Spykman, “The Principled Pluralist Position,” ibid., p. 80.
