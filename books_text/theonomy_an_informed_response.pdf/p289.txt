Hermeneutics and Leviticus 19:19 269

industrial revolution, and this remained a fixture of the English
aristocracy.*” Large debt accompanied large landed estates.

Case Laws and Underlying Principles

A single case law governing agriculture, animal husbandry,
and textile production had to be taken very seriously under the
Old Covenant. The expositor’s presumption is that these three
laws constitute a judicial unit. If they are a unit, there has to be
some underlying judicial principle common to all three. All
three prohibitions deal with mixing. The first question we need
to ask is the crucial one: What was the covenantal meaning of
these laws? The second question is: What was their economic
effect?

I argue here that the fundamental judicial principle under-
girding the passage is the requirement of separation. Two kinds
of separation were involved. The first two clauses were agricul-
tural applications of the mandatory segregation of the tribes inside
Israel until a unique prophesied seed would appear in history:
the messiah. We know who the seed is: Jesus Christ. Paul
wrote: “Now unto Abraham and his seed were the promises
made. He saith not, And to seeds, as of many; but as of one,
and to thy seed, which is Christ” (Gal. 3:16). The context of
Paul’s discussion is inheritance. Inheritance is by promise, he
said (Gal. 3:18). The Mosaic law was given, Paul said, “till the
seed should come to whom the promise was made” (Gal. 3:18).
Fwo-thirds of Leviticus 19:19 relates to the inheritance laws of
national Israel, as we shall see. When the Levitical inheritance
laws. (Lev. 25) ended with the establishment of a new priest-
hood, so did the authority of Leviticus 19:19.

‘What was Paul attempting to prove? This: eternal life (the
ultimate inheritance) is obtained by God’s promise, not by
God’s law. God’s law cannot impart life.* That is to say, the

37. Mathias, First Industrial Nation; pp. 55-57.
38. Moises Silva, “Is the Law Agaitist the Promises? The Significance of Galatians
