Editor’s Introduction 9

Christendom vs. Anti-Christendom

Here is their problem. Covenants are inescapable concepts, as
Van Til would say. It is never a question of “covenant vs. no
covenant.” It is always a question of whose covenant. If a Chris-
tian says that there is no legitimate Christian civil covenant in
New Testament times, he is necessarily saying that Christendom
is not a biblically valid goal in history. Christendom is a civiliza-
tion - the kingdom of God in history — that is governed in
every area, every nook and cranny, by God: a society whose
lawfully anointed rulers govern in terms of God’s revealed law.
In this view, God is not in retirement or on vacation; He is a
King who has delegated to Ilis officers the authority to exercise
command. There are three covenantal institutions: church,
state, and family. To deny that God’s covenant law applies to
civil government in New ‘Testament times is necessarily to aban-
don the ideal of Christendom. Westminster Seminary has done
this. So have all Christians who defend political pluralism.”

The problem is, God’s covenantal enemies understand what
modern Christian theologians fail to see, namely, that there are
three legs supporting civilization’s stool: church, stale, and family.
Modern humanism has. identified its church and priesthood:
the public school system. It has identified its state: centralized
power. Finally, it has identified its family: two adults (frequently
of different genders) living together by law. The humanists sec
that these three institutions must be governed by a comprehen-
sive, consistent legal order. They understand what the West-
minster faculty has long chosen to ignore specifically and deny
implicitly: civilization is a package deal. A civilization is not built
on the basis of smorgasbord religion: “a litte of this, a little of
that,” and all on the basis of personal taste. A civilization is a
system of integrated covenants. Westminster’s faculty is not inter-
ested in building a Christian civilization because its members

12. The standard rhetorical ploy of those Galvinistic pluralists who deny the ideat
of Christendom is to substitute the supposedly pejorative word, “Constantinianism.”
