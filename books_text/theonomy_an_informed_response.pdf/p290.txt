270 ‘THEONOMY: AN INFORMED RESPONSE

means of eternal life is not obedience to God's revealed law.
Paul was not, contrary to the argument of the Judaizers, at-
tempting to set biblical law in opposition to the principle of
inheritance by promise.** He was arguing that there is only
one pathway lo eternal life: by God’s promise. It is this promise
of new life, which is a new inheritance, that is central to Leviticus
19:19. Seed and inheritance are inescapably joined together.

The second form of scparation is more familiar: covenantal
separation. The final clause of Leviticus 19:19 deals with pro-
hibited clothing. This prohibition related not to separation
among the tribes of Israel - separation within a national coven-
ant - but rather the separation of national Israel from other
nations. This is the familiar application of the separation laws.

Because their frame of reference is not intuitively recog-
nized, the first two clauses must occupy our initial attention. We
must begin with an understanding of the ultimate boundary in
ancient Israel: the covenantal boundary of blood.

Boundary of Blood: Seed and Land

The preservation of Israel’s unique covenantal status was re-
quired by biblical law. The physical manifestation of this separa-
tion was the sign of circumcision. A boundary of blood was
imposed on the male organ of reproduction. It was a sign that
covenantal life is not obtained by cither physical birth or
through one’s heirs. As Rushdoony says, “Circumcision witness-
es to the fact that man’s hope is not in generation but in regen-
eration. . . .”*° Unlike the ancient Greeks, who believed that a
decent life after death could be obtained only through an un-

3:21 for Covenant Continuity,” Fheonomy: A Reformed Critique, p. 158.
39. Meredith G. Kline argues that this was Paul’s contention: By Oath Consigned:
A Reinterpretation of the Covenant Signs of Circumcision and Baptism (Grand Rapids:
Eerdmans, 1968), p. 23. Moises Silva says that Kline is incorrect on this point.
Theonomy, p. 160. In fact, Silva says, Kline's interpretation - the radical contrast
between law and promise - is the same as the Judaizers’ argument. Jbid,, p. 163.
40. Rushdoony, Institutes, p. 43.
