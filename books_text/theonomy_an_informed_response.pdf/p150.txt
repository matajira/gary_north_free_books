130 THEONOMY: AN INFORMED RESPONSE

explicitly tells us that the change of law about which he speaks
is the “the law of fleshly requirement” — that priests come from
the tribe of Levi (Heb. 7:13-16).

When Johnson goes on to note, quite correctly, that there
have also been changes in terms of sacrifices and cleansing
available in the law, he attempts - without evidence - to tie
them conceptually and/or logically to that particular “change of
law” referred to in Hebrews 7:11. This alleged entailment is
misleading since the “imperfection” of the Old Covenant sacrifi-
ces and cleansing is argued by the author of Hebrews on other
grounds than the priestly prerequisite of coming from the Leviti-
cal tribe (e.g., Heb. 9:10-12; 10:1-2).

Johnson comes to the crux of his article by asking whether a
change in the application of the Mosaic penal sanctions has “also”
been introduced by the change in priesthood (pp. 185-86). It
must be borne in mind that nothing that has been said up to this
point is either logically or theologically relevant to answering
that particular question. To ask whether the penal sanctions
have “also” been changed is to ask, therefore, whether we have
a textually grounded basis for believing such about them - as
we “also” have such biblical warrant regarding the other changes
(in priestly requirement, sacrifices, and cleansing efficacy).
Johnson offers no textual proof (or anchor) for that opinion at
all - not even one clear case that he then could use for an
argument from analogy. Rather, his argument rests upon a
misreading of the a fortiori logic of Hebrews 2:2 and 10:29 (pp.
186-89).

Johnson is entirely correct that these two passages in Heb-
rews prove (among other ways) how much more important and
significant is the New Covenant order than the Old. The
“greater the grace revealed in [God’s] words to his people, the
greater their liability should they disregard his voice.” Precise-
ly.® But then listen to the way in which Johnson, without justi-

15, I said in By This Standard: The Authority of God's Law Today (Tyler, Texas:
