Theonomy and Calvinism’s Judicial Theology 27

application that is the essence of theonomy. By way of analogy,
two U.S. constitutional theorists who agree on the doctrine of
original intent,’ but who disagree on application, are still cons-
litutionalist theorists who believe in original intent.

As those who study the arguments for theonomic ethics
soon learn, far from being “a new kid on the block,” as Muet-
her intimates it is, theonomic ethics has always been a part of
Reformed theology.’ It’s a Reformed theology attraction that has
led many Calvinists like myself to embrace the distinctives of
theonomy. The belief that the Bible in its particulars can and
should be applied to every area of life is a major theological
distinctive that sets Reformed theology apart from all other
orthodox Trinitarian traditions. Furthermore, a growing num-
ber of non-Reformed Christians have adopted much of the
ethical system outlined by theonomists because of its “biblicist
hermeneutic.”® Contrary to Muether’s views, John Monsma, an
early advocate of world-and-life view Calvinism, stated that

Calvinism is nothing but Biblicism. Ifa government acts in accor~
dance with the Bible, it will always be doing the right thing. If it
transgresses the bounds that the Bible has placed around it, it
becomes tyrannical. The New England governments, taken on
the whole, were so exemplary because they were — not theocra-

7. Meaning the original intent of the Framers in 1787,

8, Mcredith G. Kline, a Calvinistic critic of Christian Reconstruction, is honest
enough to state that theonomic ethics “is in fact 2 revival of certain teachings con-
tained in the Westminster Confession of Faith — at least in the Confessions original
formulations.” Kline, “Comments on an Old-New Error,” The Westminster Theological
Journal 41:1 (Fall 1978), p. 174.

As Greg Bahnsen points out, there was no amendation to “the declaration about
the law of God or its use in catechisms (i.e., the strictly cheonomic elements of the
Confessional Standards).” Revision was made to “a subsection of the chapter on the
civil magistrate, aiming to reinforce disestablishment and the rejection of Erastianism
(see Theonomy, pp. 527-37, 54]-43).” Bahnsen, “M.G. Kline on Theonomic Politics:
An Evaluation of His Reply,” Joumal of Christian Reconstruction, VII (Winter 1979-80),
p- 201.

9. Ina faculty discussion at RTS on July 17, 1978, Greg Bahnsen had to answer
the charge of being a “biblicist hermeneutic” several times. Biblicist? Too biblical?

 
