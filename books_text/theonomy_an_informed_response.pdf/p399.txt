50-proof
legal orders, 33-34, 46
litmus test, 32
Madison & Rushdoony,
332
no defense of, 322
polygamy, 33
principled, 41-44
Reformed Seminary, 36
Satanism, 33
smorgasbord ethic, 31-32
standards, 42
standards?, 35
toleration, 98
values &, 35-36
Van Til, Henry, 46
politically correct, 352
politics, 133-34, 198
polygamy, 33, 259
population, 263-64
postmillennialism
gradualism, 216
Rushdoony, 17
salvation, 215
sin, 228
transforming history,
203
victory, 207, 215
‘Westminster Seminary,
xvi
poverty, 234, 241-42, 266
Poythress, Vern
affliction of, 205
blueprints, 205
book, 81
challenge, 256
exegetical imperative,
258

Index

379

hermeneutics, 256
Kline & theonomy, 205
shadows, 258
statutes and functions,
258
preaching, 52
predestination, 18, 22, 29, 84-
85, 331-33
premillennialism, 202
Presbyterians, xiii, xvi
presuppositions, 58
preterism, 287n
priesthood
change in, 259, 286
Christ’s, 275
citizenship &, 280
clothing, 284, 285, 289,
292-93
Israel, 285
law, 178
law &, 128-29
laws 8, 275-76
sacraments &, 259
sanctions, 130
Princeton Seminary, xiii, 30
prison, 225
prisons, 115
progress, 264
promise, 286
promise (seed laws), 279
property rights, 261-62, 268
psychology, xiv
Puritans, 6-7, 28, 86

Qumran, 173-74

ransom, 114
redemption, 172
