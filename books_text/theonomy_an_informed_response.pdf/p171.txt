Civil Sanctions in the New Testament 151

ties), while passing over other applications (e.g., the more obvi-
ous infractions of civil law). There are numerous examples of
this practice. In Matthew 5:16, Jesus encourages good works
before men to glorify God, but in Matthew 6:1, He discourages
them. In Matthew 7:1, He warns against judging others, but in
Matthew 7:6, He urges His disciples not to cast their pearls
before swine (a judgment call if ever there was one!). In Mat-
thew 5:39ff Jesus is employing hyperbole, rather than setting
down universally abiding commands. For instance, notice that
He forbids oaths (Matt. 5:34), yet He himself takes one (Matt.
26:64), as does Paul (Rom. 1:9; 2 Cor. 1:23; Phil. 1:8). He urges
turning the other cheek when slapped (Matt. 5:39), yet He
rebukes one who slaps Him (John 18:23).

2. Other Gospel References

The second class of texts Johnson points to as referring to
penal sanctions are those found elsewhere regarding Jesus’
ministry, He summarizes these briefly in two paragraphs, intro-
ducing them with what seems to be a theonomic admission:
“Other accounts of Jesus’ earthly ministry reaffirm the appro-
priateness of the penal sanctions as expressions of God’s disfa-
vor toward sin” (p. 179). He cites Matthew 15:4, wherein Jesus
mentions the capital law for the rebellious child (Exo. 21:17;
Lev. 20:9). He refers also to Zaccheus’ determination to offer
fourfold restitution (cf. Exo. 22:1) to those whom he defrauded
(Luke 19:8), which action is put in an approbative light by the
gospel writers,

Johnson is correct when he states that the appropriateness of
these penal sanctions is reaffirmed as expressions of God’s
disfavor toward sin. These are also cited with the apparent
assumption of their continuing validity, i.e. as civil sanctions.

He then turns to the familiar pericope of the woman caught
in adultery (John 7:53-8:11). He doubts its textual authenticity,
but says that “if the text is an authentic part of God’s Word, it
gives us no clear guidance on the question of the continuing
