Editor’s Introduction to Part I 17

This led Rushdoony to the question of ethics, including
social ethics ~ dangerous territory for Christian scholars, filled
with ruthless savages who hold Ph.D. degrees from accredited
universities. Van Til’s technical developments in apologetics led
Rushdoony to the idea of Christian Reconstruction. Rushdoony
concluded that if the individual can be regenerated — from
covenant-breaking to covenant-keeping - then so can all human
institutions. Van Til had to admit this possibility in principle,
but his amillennial eschatology denied its progressive attain-
ment in history. Rushdoony’s postmillennialism figuratively
forced Van Til’s hypothetical idea of comprehensive covenant-
keeping out of the tenured safety of the seminary classroom
and into the streets. Rushdoony began working on The Institutes
of Biblical Law in the late 1960's, in the midst of a world-wide
social upheaval: the counter-culture. Rushdoony was offering
an explicitly biblical counter-counter-culture in a crisis period.

Van Til was not a theonomist. He was a philosopher — by
training, calling, emotional preference, and self-imposed intel-
lectual boundaries. Rushdcony pioneered the theonomic posi-
tion. What was the necessary connection between the two men?
The connection was judicial. Van Til destroyed all claims of
autonomy in any area of life. God’s Word, said Van Til, pro-
vides man with the only theoretically possible standard. All
natural law theory, he said, is a covenant-breaking myth unless
based squarely on the doctrine of man as a fallen creature who
is in need of redemption and written revelation, But if this is
true, then all legal systems that arc not derived directly from
the Bible have to be wrong. By dynamiting the dikes of natural
law theory, Van Til had left Christianity without any intellectu-
ally valid alternative except the Bible. Van Til’s was a career
based on demolition. Rushdoony saw that this exclusively nega-
tive critique was necessary but insufficient. There are no judicial
vacuums; there is no judicial neutrality. Something has to take the
place of the collapsed edifice of natural law theory, not just in
philosophy but also in social theory. This means theonomy.
