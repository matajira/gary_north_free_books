Whase Conditions for Charity? 237

Despite his close association through the publishing houses,
Grant is not a reconstructionist. Certainly, there are many defini-
tions of that term, but by virtually any criterion he does not meet
the requirements. Grant does not share the reconstructionists’
approach to the application of the Old Testament law .. . He is
not a thoroughgoing postmillennialist, nor docs he partake of
many other common reconstructionist perspectives.*

Keller refers to, but does not quote from, “personal correspon-
dence” with George Grant to prove his claims. I have not seen
any of these personal letters. For all I know, Dr. Keller has
misrepresented his personal correspondence with Grant; it
would certainly be consistent with how he operates: selective
quotation. On the other hand, perhaps this is what George
believes today. Even so, the point is not where my friend
George is in his thinking today, but what he held at the time of the
writing of the major corpus of his writings, during the period of the
80's.

Grant in the 1980's

What was George Grant’s position on poverty in the 80's?
Two themes appear. First, he was against state welfare because
he thought that it had been an utter failure in its attempts to
help the poor. It was part of the problem and not the solution,
even creating a monstrous class of welfare state people. Con-
sider this one statement of Grant’s, so characteristic of a recon-
structionist mind-set that pervades all of his writings on pover-

ty:

The war on poverty actually halted in its tracks the ongoing
improvement in the lot of America’s poor. Writers as diverse as
Charles Murray, George Gilder, Warren Brookes, Thomas Sow-
ell, and Murray Rothbard have shown conclusively that instead

4, Keller, Theonomy, p. 201.
