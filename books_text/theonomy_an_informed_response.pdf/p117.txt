Westminster Seminary on Pluralism 97

In the Westminster volume which he edited, Barker under-
takes to present a biblical defense of political pluralism,’* and
this makes his work especially worthy of analytical scrutiny.
Much of what Barker advocates in his paper is set forth as
though it confticts with a theonomic understanding of the civil
magistrate, when in fact there are large areas of agreement
which Barker overlooked, apparently due to misconceptions
about theonomic cthics.

Barker says “If we are indeed zealous for the application of
God's Jaw in society, our first question must be, what is our
King’s intention?” He answers: “his intention is for the civil
authority to apply God’s law in the area of human relations in
which God has ordained him to serve.” Given Barker’s concep-
tion of how this application would take place, it is inappropriate
for the state to propagate God’s saving truth or promote per-
sonal faith. “Civil authority” should not be used -“to enforce the
true religion” or “enforce the true faith and worship,” for in-
stance by “destroying” other religions than Christianity. The
state may not “in any way coerce belief or worship,” nor is it
responsible “to exterminate false religion.” We must, rather,
“protect the liberty of conscience and belief of unbelievers
under a Christian government.” “It is not Caesar’s to enforce
the true religion.” Accordingly, there ought not to be an “estab-
lished church.” We should “oppose the requirement of prayer
or acts of worship in the public schools.” The true religion
ought not to be supported by taxation, and taxcs ought to be
paid even when the government follows a blasphemous reli-
gion. Victory for King Jesus “comes not through civil govern-
ments, but through his witnesses.”"”

Theoriomists agree heartily with beliefs such as these, and I
have promoted such viewpoints zealously in my public lectures
because 1 believe that they are required by a proper reading of

16. “Theonomy, Pluralism, and the Bible,” Theonomy, ch. 10.
17. Barker, pp. 230, 232, 233, 236, 237, 238, 239, 241, 242.
