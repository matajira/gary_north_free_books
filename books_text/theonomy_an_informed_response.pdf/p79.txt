Fear of Flying: Clipping Theonomy’s Wings 59

that followed dealt with and extended areas of Christ’s total
Lordship in all of life. . . ."* In this; Schaeffer worked in the
shadow of Kuyper.

Third, late in his career, Schaeffer saw extended implications
to the worldview he put in motion in his early works. [le ex-
panded the areas over which He believed Jesus is Lord with the
publication of How Should We Then Live, Whatever Happened to the
Human Race, and A Christian Manifesto. “That led to the demand
of the next logical step: What is the Christian’s relationship to
government, law, and civil disobedience?”*

It was here that Schaeffer saw where his initial flight plan
was about to take him: Christian Reconstruction. A reading of
A Christian Manifesto alerts the reader that Schaeffer moved
from being a critic of culture, his main contribution to world-
view Calvinism, to advocating civil disobedience. The missing
link was Reconstruction. To advocate civil disobedience was an
admission that no constructive alternative to the humanistic
system existed except the one advocated by Christian Recons-
tructionists. Schaeffer wanted his readers to understand that he
in no way wanted what Reconstructionists were offering.® His
earlier works influenced many future Reconstructionists be-
cause of his insistence that the whole Bible was applicable to the
whole of life, the law of God included.

Schaeffer’s View of God’s Law

While he refused to discuss the particulars of the law of God
as the “base” for authority, he knew something had to be done
to confront a bold humanistic law system. Schaeffer turned to
Samuel Rutherford’s doctrine of Christian resistance while
ignoring Rutherford’s biblical approach to the application of the

4. Schaeffer, “Preface” (1981), A Christian Manifesto (1981), Complete Works, V:A17.

5. Did.

6. See Gary North, Political Polytheism: The Myth of Pluralism (Tyler, Texas: Tnsti-
tute for Christian Economics, 1989), pp. 165-220.
