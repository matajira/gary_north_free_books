TABLE OF CONTENTS

Part I: Calvinism and Theonomy
Editor’s Introduction to Part I
1. Theonomy as an Extension of Calvinism’s

Judicial Theology, Gary DeMar - 1... c ee eee eee ee 24
2. Some Wings for Calvinism’s Modern Plane, Gary DeMar 39
3. Fear of Flying: Clipping Theonomy’s Wings, Gary DeMar 57

 

Part JI: Covenantal Sanctions

Editor’s Introduction to Part 6... eee eee eee eee 81
4, Westminster Seminary on Pluralism, Greg L. Bahnsen .. 89
5. Westminster Seminary on Penology, Greg L. Bahnsen .. 112
6. Civil Sanctions in the New

 

 

Testament, Kenneth L. Gentry 66.00 es 135
7. Church Sanctions in the Epistle .
to the Hebrews, Kenneth L. Gentry... 0.00 cee 164
Part II: The Church
Editor’s Introduction to Part ITI] .........2. 0 eee eee 195
8. Whose Victory in History?, Kenneth L. Geniry +++ 207
9. Whose Conditions for Charity?, Ray R. Sution ...... 231
10. The Hermeneutics of Leviticus 19:19 —
Passing Dr. Poythress’ Test, Gary North .......... 255
11. A Pastor’s Response, John Maphet ..........40-5 295
Editor’s Conclusion «2.0... 0.00 cece eee eee eee 315
Appendix, Gary DeMar 1.2.1... eee eee eee eee 346
Bibliography : .. 359
Index .....- : .. 367
Scripture Index... 1.6... eee eee eee eee 385

About the Authors 0.0... 0.6 eee eee 394
