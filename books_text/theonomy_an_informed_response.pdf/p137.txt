Westminster Seminary on Penology 117

“guided by the principles of Scripture rather than by the ex-
plicit statements of the Old Testament” because the explicit
statements sometimes involve flexibility or difficulty in interpre-
tation (pp. 50-51). Such considerations provide no support
whatsoever for turning away from the “explicit statements” of
Scripture and substituting more “general” principles! (Besides,
are not “general principles” aise flexible and difficult to inter-
pret?) The fact is that Jesus did not permit us the option of
dismissing the explicit statements of Scripture — not even a jot
or a tittle of the least commandment in the law (when properly
interpreted of course, Matt. 5:17-19).

There are only two other, very brief theological arguments
{both on p. 48) which Longman introduces into the dialogue
over the penal sanctions, and they fare no better than the oth-
ers, really. Longman suggests that the penal sanctions for vio-
lating God’s law regarding divine-human relations were depen-
dent upon God’s special, holy presence in the midst of Israel. I
have already thoroughly analyzed and rebutted this kind of
argument elsewhere in response to Meredith Kline,® but Long-
man fails to interact with that discussion so as to rescue his line
of reasoning. Indeed, the weaknesses in the argument from
Israel’s special holiness are also exposed by two of Longman’s
own co-authors in the Westminster book.? Longman does not
explain why the penal sanctions regarding other kinds of sins
(e.g., kidnaping, rape, even theft) are not likewise relativized by
God’s special presence in the land of Israel, to whom He re-
vealed His law. Longman leaves unexplained why not all sins in
Israel called for capital punishment, given the same holy pres-
ence of God which allegedly required the death penalty for
blasphemy, idolatry, etc. Longman does not account for the
biblical declarations that the laws given Israel carried a univer-

8. Greg L. Bahnsen, “M. G. Kline on Theonomic Politics: An Evaluation of His
Reply,” Journal of Christian Reconstruction, VII (Winter 1979-80), pp. 195-221.

9, See John Frame, “The One, the Many, and Theonomy,” pp. 92-97, as well as
Vern Poythress, “Effects of Interpretive Frameworks,” passim.
