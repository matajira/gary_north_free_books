 

 

Some Wings for Modern Calvinism’s Plane 43

our view of the deity of Christ, the resurrection from the dead,
and justification by faith alone? Could the same be said for “the
family” and “the church”? It was an isolated passage in Romans
that brought on the Reformation of the sixteenth century.
Luther’s cry was that “The just shall live by faith” (Rom. 1:17).
Are the doctrines of justification and sanctification different
from the doctrines of law and the civil magistrate? The West-
minster divines did not think so. Biblical passages are cited
throughout the Shorter and Larger Catechisms.

Spykman tells us that when “the Reformers spoke of sola
Scriptura, they did not mean that Scripture is God’s only revela-
tion. God also reveals His will in creation and providence. In
fact, the creational word remains His fundamental and abiding
revelation.” Then why did God give us the Bible? Adam and
Eve, prior to the fall, were given special revelation regarding
the maintenance of the created order. Spykman continues:
“God gave the Scripture to correct and reinforce His original
revelation upon our minds, redirecting our attention to its
meaning, refocusing the intent and purpose of creation. God’s
message is always the same, but it comes in different modes. Its
author does not contradict Himself. Though revelation comes in
various forms, its norms are constant. The word holds, even when men
do not discern or obey it.”

Like theonomists, Spykman agrees that general and special
revelation present the same message. If this is true, then we
should expect to find the same laws in the creation order as we
find in the Bible. For example, not only should we find prohi-
bitions regarding what.a society should do with men practicing
sodomy, but we should also be able to find the same sanctions.
Since both general and specific norms are found in the Bible,
general and specific norms can be found in creation. They are
one and the same! If the Bible was given “to correct and rein-
force” God’s “original revelation,” then why not begin with the

10. ibid., pp. 82-83. Emphasis added.
