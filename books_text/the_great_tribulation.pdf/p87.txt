THE FOUR HORSEMEN 71

which he was sanctified, and has insulted the Spirit
of Grace? For we know Him who said: “Vengeance is
Mine, I will repay.’ And again: ‘The Lord will judge
His people.’ It is a terrifying thing to fall into the
hands of the living God” (Hebrews 10:26-31). It was
thus necessary that the first Rider should be seen
carrying the Bow of God's vengeance, to signify the
unleashing of the Curse upon Isract’s ground; for
these apostates, the Noachic covenant is undone.

St. John’s first readers would immediately have
understood his reference to this Rider with the Bow
as speaking of Jesus Christ, on the basis of what we
have already seen. But, third, there is the fact that the
Rider is given a crown, and this too agrees with what we
know about Christ from Revelation (14:14; 19:11-13).
This Greek word for crown (stephanos) is used seven
times in Revelation with reference to Christ and His
people (2:10; 3:11; 4:4, 10; 6:2; 12:1; 14:14).

The fourth and final point, however, should ren-
der this interpretation completely secure: the Rider
goes out conquering. This is the very same word in the
Greek as that used in the letters to the seven
churches for overcoming or conquering (see Revelation
2:7, 11, 17, 26; 3:5, 12, 21), Consider how the Revela-
tion has used this word up to this point:

He who conguers, I will grant to him to sit
down with Me on My Throne, as I also con-
quered and sat down with My Father on His
Throne (3:21).

The Lion that is from the tribe of Judah,
the Root of David, has conquered so as to open
the Book (5:5).
