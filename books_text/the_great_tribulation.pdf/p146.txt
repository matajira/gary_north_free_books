130) THE GREAT TRIBULATION

esting statement: by the apostates’ shedding of
blood, “they are worthy!” This is a deliberate parallel
to the message of the New Song in Revelation 5:9:
“Worthy art Thou to take the Book, and to break its
seals; for Thou wast slain, and didst purchase us for
God with Thy blood.” Just as the Lamb received His
reward on the basis of the blood He shed, so these
persecutors have now received the just recompense
for their bloodshed.

God had once promised the oppressed of Israel
that He would render to their enemies according to
their evil works:

I will feed your oppressors with their own flesh,

And they will become drunk with their own
blood as with sweet wine;

And all flesh will know that I, the Lozp, am
your Savior,

And your Redeemer, the Mighty One of Jacob

(Isaiah 49:26).

Israel’s apostasy has reversed this: now it is she,
the Persecutor par excellence, who will be forced to
drink her own blood and devour her own flesh. This
was true in much more than a figurative sense: as
God had foretold through Moses (Deuteronomy
28:53-57), during the seige of Jerusalem the Israel-
ites actually became cannibals; mothers literally ate
their own children. Because they shed the blood of
the saints, God gives them their own blood to drink
(cf. Revelation 17:6; 18:24).

Joining the angel in praise comes the voice of the
