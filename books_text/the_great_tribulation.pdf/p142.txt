126 THE GREAT TRIBULATION

The First Chalice

As the first angel pours out his Chalice onto the
Land (Revelation 16:2), it becomes “a loathsome and
malignant sore upon the men who had the mark of
the Beast and who worshiped his image.” The sores
are a fitting retribution for apostasy, God placing
His stamp of wrath upon those who wear the Beast’s
mark. Just as God had poured out boils on the un-
godly, state-worshiping Egyptians who persecuted
His people (Exodus 9:8-11), so he is plaguing these
worshipers of the Beast in the Land of Israel—the
covenant people who have now become Egypt-like
persecutors of the Church. This plague is specifically
mentioned by Moses in his list of the curses of the
covenant for idolatry and apostasy: “The Lorp will
smite you with the boils of Egypt and with tumors
and with the scab and with the itch, from which you
cannot be healed. . . . The Lorp will strike you on
the knees and legs with sore boils, from which you
cannot be healed, from the sole of your foot to the
crown of your head” (Deuteronomy 28:27, 35).

The Second Chalice

The second angel pours out his Chalice into the
sea (Revelation 16:3), and it becomes blood, as in the
first Egyptian plague (Exodus 7:17-21) and the Sec-
ond Trumpet (Revelation 8:8-9). This time, how-
ever, the blood is not running in streams, but instead
is like that of a dead man: clotted, coagulated, and
putrefying. Blood is mentioned four times in this
chapter; it covers the face of Israel, spilling over the
four corners of the Land.
