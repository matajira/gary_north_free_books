THE BOOK IS OPENED 99

in prayer, believing, you shall receive” (Matthew
21:21-22). Was Jesus being flippant? Did He really
expect His disciples to go around praying about
moving literal mountains? Of course not. More im-
portantly, Jesus was not changing the subject. He
was still giving them a lesson about the fall of Israel.
What was the lesson? Jesus was instructing His dis-
ciples to pray imprecatory prayers, beseeching God
to destroy Israel, to wither the fig tree, to cast the
apostate mountain into the sea.

And that is exactly what happened. The perse-
cuted Church, under oppression from the apostate
Jews, began praying for God’s vengeance upon
Israel (Revelation 6:9-11), calling for the mountain
of Israel to “be taken up and cast into the sea.” Their
offerings were received at God’s heavenly altar, and
in response God directed His angels to throw down
His judgments to the Land (Revelation 8:3-5), Israel
was destroyed. We should note that St. John is writ-
ing this before the destruction, for the imstruction and
encouragement of the saints, so that they will con-
tinue to pray in faith. As he had told them in the be-
ginning, “Blessed is he who reads and those who hear
the words of the prophecy, and keep the things that are
written in it; for the time is near” (Revelation 1:3).

The Third Trumpet
Like the preceding symbol, the vision of the third
Trumpet (Revelation 8:10-11) combines Biblical im-
agery from the falls of both Egypt and Babylon. The
effect of this plague—the waters being made bitter
—is similar to the first plague on Egypt, in which the
