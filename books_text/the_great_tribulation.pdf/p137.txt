10

ALL CREATION
TAKES VENGEANCE

The Seventh Trumpet was the sign that “there
shail be no more delay” (cf. Revelation 10:6-7). Time
had run out; wrath to the utmost had now come
upon Israel. From this point on, St. John abandons
the language and imagery of mere warning. Jeru-
salem’s destruction is certain, and so the prophet
concentrates wholly on the message of her impend-
ing doom. As he describes the City’s fate, he extends
and intensifies the Exodus imagery that has already
been so pervasive throughout the prophecy. He
speaks of “the Great City” (16:19), reminding his
readers of a previous reference: “the Great City,
which Spiritually is called Sodom and Egypt, where
also their Lord was crucified” (11:8). Jerusalem is
called Sodom because of its sensual, luxurious apos-
tasy (cf. Ezekiel 16:49-50), and because it is devoted
to total destruction as a whole burnt sacrifice (Gen-
esis 19:24-28; Deuteronomy 13:12-18). But St. John’s
more usual metaphors for the Great City are taken
from the Exodus pattern: Jerusalem is not only
