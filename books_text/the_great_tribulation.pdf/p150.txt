11

IT IS FINISHED!

The symbolic targets of the first four Chalices
were the elements of the physical creation: Land,
sea, waters, and the sun. With the last three
Chalices, the consequences of the angelic attack are
more “political” in nature: the disruption of the
Beast’s kingdom; the War of the great Day of God;
and the Fall of “Babylon.”

The Fifth Chalice

Although most of the judgments throughout
Revelation are aimed specifically at apostate Israel,
the heathen who join Israel against the Church come
under condemnation as well. Indeed, the Great
Tribulation itself would prove to be “the hour of test-
ing, that hour which is to come upon the whole world,
to test those who dwell upon he Land” (3:10). The
fifth angel (Revelation 16:10-11) therefore pours out
his Chalice “upon the throne of the Beast”; and, even
as the sun’s heat is scorching those who worship the
Beast, the lights are turned out on his kingdom, and
