192 THE GREAT TRIBULATION

earth.” Yet to refuse to work to bring heaven on earth
by teaching people to obey heaven’s righteous princi-
ples on earth is to turn history over to the devil. His
disciples are working hard to bring hell on earth by
teaching people to obey hell’s rebellious principles on
earth.

There is a war going on. It is a war between God
and Satan, righteousness and evil, covenant-keepers
and. covenant-breakers, heaven and hell. This war is
going on in history. It is an earthly war primarily. The
ultimate issue over which the war is being fought is
the issue of sovereignty. Who is sovereign, God or
Satan? The historical issue is also being fought over
the issue of sovereignty: Whose human forces will
triumph in history, God’s or Satan’s? Whose New
World Order will be victorious in history, Christ’s or
Satan’s? In short, the war is being fought over this
question: Heaven on earth or hell on earth?

There is no possibility of any other kingdom on
earth. There is no possibility of neutral man’s king-
dom on earth, operated by hypothetical neutral nat-
ural law. Men are never neutral, and there is no
such thing as natural law. There is God’s law, and
there are Satan’s numerous alternatives, including
“neutral” natural law. There is no neutrality. Therefore,
we face the question: Will it be heaven on earth or
hell on earth? Will it be God’s covenant law as.the
law of nations, or one or more of Satan’s counterfeit
law systems? Any attempt to substitute a third
choice, such as natural law, is simply another at-
tempt to replace God’s covenant law with Satan’s. It
is simply another attempt to build hell on earth.
