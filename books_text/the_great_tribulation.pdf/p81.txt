‘THE FOUR HORSEMEN 65

logical order. For example, in the Fifth Seal—after
all the havoc wreaked by the Four Horsemen —the
martyrs calling for judgment are told to wait. But
the judgment is immediately poured out in the Sixth
Seal, the entire creation “unseam’d from the nave to
the chaps.” Yet, after all this, God commands the
angels to withhold judgment until the servants of
God are protected (7:3). Obviously, the Seals are not
meant to represent a progressive chronology. It is
more likely that they reveal the main ideas of the
book’s contents, the major themes of the judgments
that came upon Israel during the Last Days, be-
tween A.D. 30-70.

Several commentators have observed the close
structural similarity between the six Seals of this
chapter and the events of the so-called Little Apoca-
&pse—Jesus’ discourse recorded in Matthew 24,
Mark 13, and Luke 2i—which, as we have already
seen, foretells the fall of Jerusalem in a.p. 70 (see
Chapters 1 and 2, above). As the outlines below
demonstrate, all these passages essentially deal with
the same basic subjects:

Revelation 6
1. War (vv. 1-2)
. International strife (vv. 3-4)
. Famine (vv. 5-6)
. Pestilence (vv. 7-8}
. Persecution (vv. 9-11)
. Earthquake; De-creation (vv. 12-17)

Aaron
