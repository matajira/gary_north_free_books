‘THE FOUR HORSEMEN 69

prayer of every Church worship service for decades
prior to the fall of Jerusalem.)

As the first living creature calls, St. John sees a
white horse, its Rider armed for battle, carrying a
Bow. The Rider is already victorious, for a crown was
given to Him. Having achieved victory, He rides on
to further victories, going out “conquering, and to
conquer.” Amazingly, an interpretation popular in
some circles claims that this Rider on the white horse
is the Antichrist. Showing where his faith lies, one
writer goes all the way and declares that the Anti-
christ is “the only person who could accomplish all of
these feats”!

But there are several points about this Rider that
demonstrate conclusively that He can be none other
than the Lord Jesus Christ. First, He is riding a white
horse, as Jesus does in Revelation 19:11-16. Second,
He carries a Bow. As we have seen, the passage from
Habakkuk that forms the basis for Revelation 6
shows the Lord as the Warrior-King carrying a Bow
(Habakkuk 3:9, 1f). St. John is also appealing here
to Psalm 45, one of the great prophecies of Christ’s
victory over His enemies, in which the psalmist joy-
ously calls to Him as He rides forth conquering, and
to conquer:

Gird Thy sword on Thy thigh, O Mighty One,

In Thy splendor and Thy majesty!

And in Thy majesty ride on victoriously,

For the cause of truth and meekness and
righteousness;

Let Thy right hand teach Thee awesome things.
