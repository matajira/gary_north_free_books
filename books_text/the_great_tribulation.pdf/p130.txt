114 THE GREAT TRIBULATION

‘Trumpet, apostate Israel foolishly became confirmed
in her rebellion.

Therefore, St. John records in verses 20-21 that
“the rest of the men, who were not killed by these
plagues, did not repent... so as not to worship
demons and the idols.” The Jews had so completely
given themselves over to apostasy that neither God’s
goodness nor His wrath could turn them from their
error. Instead, as Josephus reports, even up to the
very end—after the famine, the mass murders, the
cannibalism, the crucifixion of their fellow Jews at
the rate of 500 per day—the Jews went on heeding
the insane ravings of false prophets who assured
them of deliverance and victory. Josephus com-
ments: “Thus were the miserable people beguiled by
these charlatans and false messengers of God, while
they disregarded and disbelieved the unmistakable
portents that foreshadowed the coming desolation;
but, as though thunderstruck, blind, senseless, paid
no heed to the clear warnings of God” (The Jewish
War, vi. v. 3).

Warnings of Jerusalem’s Fail

What “clear warnings” had God given them?
Apart from the apostolic preaching, which was all
they really needed (cf. Luke 16:27-31), God had sent
miraculous signs and wonders to testify of the com-
ing judgment; Jesus had warned that, preceding the
Fall of Jerusalem, “there will be terrors and great
signs from heaven” (Luke 21:11), This was especially
true during the festival seasons of the year 66.
Josephus continues in his report: “While the people
