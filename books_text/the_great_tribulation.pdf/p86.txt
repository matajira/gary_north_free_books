70 THE GREAT TRIBULATION

Thine arrows are sharp;

The peoples fall under Thee;

Thine arrows are in the heart of the King’s
enemies (Psalm 45:3-5).

We should ask a rather obvious question at this
point—so obvious that we are apt to miss it alto-
gether: Where did Christ get the Bow? The answer (as is
usually the case) begins in Genesis. When God
made the covenant with Noah, He declared that He
was no longer at war with the earth, because of the
“soothing aroma’ of the sacrifice (Genesis 8:20-21);
and as evidence of this He unstrung His Bow and
hung it up “in the cloud” for all to see (Genesis
9:13-17). Later, when Ezekiel was “raptured” up to
the Throneroom at the top of the Glory-Cloud, he
saw the Bow hanging above the Throne (Ezekiel
1:26-28); and it was still there when St. John ascended
to heaven (Revelation 4:3). But when the Lamb
stepped forward to receive the Book from His
Father’s hand, He also reached up and took down
the Bow, to use it in judgment against the apostates
of Israel. For those who “go on sinning willfully after
receiving the knowledge of the truth, there no longer
remains a sacrifice for sins, but a certain terrifying
expectation of judgment, and the fury of a fire that
will consume the adversaries. Anyone who has set
aside the Law of Moses dies without mercy on the
testimony of two or three witnesses. How much
severer punishment do you think he will deserve who
has trampled under foot the Son of God, and has re-
garded as unclean the blood of the covenant by
