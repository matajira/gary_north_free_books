140) THE GREAT TRIBULATION

This is echoed in St. Paul’s prophecy to the Thes-
salonians:

For the mystery of lawlessness is already at
work; only he who now restrains will do so un-
til he is taken out of the way. And then that
jawless one will be revealed whom the Lord will
slay with the breath of his mouth and bring to
an end by the appearance of His Coming; that
is, the one whose coming is in accordance with
the activity of Satan, with all power and signs
and false wonders, and with all the deception of
wickedness among those who perish, because
they did not receive the love of the truth so as to
be saved.

And for this reason God will send upon
them a work of error so that they might believe
the lic, in order that they all may be con-
demned who did not believe the truth, but took
pleasure in wickedness (I Thessalonians 2:7-12).

Ultimately, the “work of error” performed by
these lying spirits is sent by God in order to bring
about the destruction of His enemies in the War of
“that great Day of God,” a Biblical term for a Day of
Judgment, of calamity for the wicked (cf. Isaiah
13:6, 9; Joel 2:1-2, it, 31; Amos 5:18-20; Zephaniah
14:14-18). Specifically, this is to be the Day of Israel’s
condemnation and execution; the Day, as Jesus fore-
told in His parable, when the King would send His
armies to destroy the murderers and set their City on
