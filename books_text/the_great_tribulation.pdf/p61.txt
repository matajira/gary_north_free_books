THELAST DAYS 43

Who rule this people who are in Jerusalem,

Because you have said, “We have made a
covenant with death,

And with Sheol we have made a pact.

When the overwhelming scourge passes through,

It will not reach us,

For we have made falsehood our refuge

And we have concealed ourselves with
deception.”

Therefore, thus says the Lord Gon:

“Behold, I am laying in Zion a stone, a tested
stone,

A costly cornerstone for the foundation, firmly
placed;

He who believes in it will not be in a hurry.

And I will make justice the measuring line,

And righteousness the level;

Then hail shall sweep away the refuge of lies,

And the waters shall overflow the secret place.

And your covenant with death shall be canceled,

And your pact with Sheol shall not stand;

When the overwhelming scourge passes through,

Then you become its trampling place.

As often as it passes through, it will seize you.

For morning after morning it will pass through,

And by day and by night.

And it will be sheer terror to understand what it
means” (Isaiah 28:11-19).

The miracle of Pentecost was a shocking message
to Israel. They knew what this meant. It was the
sign from God that the Chief Cornerstone had come,
