PUBLISHER'S EPILOGUE 163

Salt is also used as a destroyer in history. It not
only adds flavor, it also kills, and kills Forever.” It was
used in the ancient world as a means of destroying
an enemy city, for salting over a city’s agricultural
area destroyed its future productivity. “And Abim-
elech fought against the city all that day, and slew
the people that was therein, and beat down the city,
and sowed it with salt” (Jud. 9:45). God salted over
Sodom and Gomorrah, and later other cities. Why?
To preserve His covenant. Chilton reproduces this
passage in its entirety in The Days of Vengeance in rela-
tion to the Temple’s sacrifices. He does so in his in-
troductory remarks to the book's section on God’s
covenant sanctions (p. 226):

And the Lorn shall separate him unto evil
out of all the tribes of Israel, according to alll the
curses of the covenant that are written in this
book of the law: So the generation to come of
your children that shall rise up after you, and
the stranger that shall come from a far land,
shall say, when they see the plagues of that
land, and the sickness which the Lorp hath laid
upon it; And the whole land thereof is brim-
stone, and salt, and burning, that it is not
sown, nor beareth, nor any grass groweth
therein, like the overthrow of Sodom, and
Gomorrah, Admah, and Zeboim, which the
Lorp overthrew in his anger, and in his wrath:
Even ali nations shall say, Wherefore hath the
Lorp done thus unto this land? What meaneth
