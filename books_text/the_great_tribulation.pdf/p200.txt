‘164 THE GREAT TRIBULATION

the heat of this great anger? Then men shall
say, Because they have forsaken the covenant of
the Lorp God of their fathers, which he made
with them when he brought them forth out of
the land of Egypt: For they went and served
other gods, and worshipped them, gods whom
they knew not, and whom he had not given
unto them. And the anger of the Lorp was kin-
dled against this land, to bring upon it all the
curses that are written in this book (Deut.
99:21-97),

The phrases of cursing are temperature-oriented:
“the whole land thereof is brimstone, and salt, and
burning”; “the heat of this great anger”; “the anger of
the Lorn was kindled against this land.” It is totally
misleading to speak of God’s judgments in history
apart from the language of fire. But it is also mis-
leading to speak of God’s judgmental fire without
salt. Salt is the savor of judgment. Thus the presence
of the Church in history is the savor of judgment in
history. God’s covenant sanctions are two-fold: bless-
ing and cursing.

What is true of God’s covenant cursings in his-
tory is equally true of His covenant cursings in eter-
nity. The lake of fire is the place “where their worm
dieth not, and the fire is not quenched. For every
one shall be salted with fire, and every sacrifice shall
be salted with salt.” The New Heaven and the New
Earth is as assured of its eternal status as the lake of
fire is, and vice versa. God’s covenant sanctions never
end.
