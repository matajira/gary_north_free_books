VERUSALEM UNDER SIEGE 119

St. John’s description of Israel’s idolatry is in line
with the usual prophetic stance; but his accusation is
an even more direct reference to Daniel’s condemna-
tion of Babylon, specifically regarding its worship of
false gods with the holy utensils from the Temple. Daniel
said to king Belshazzar: “You have exalted yourself
against the Lord of heaven; and they have brought
the vessels of His House before you, and you and
your nobles, your wives and your concubines have
been drinking wine from them; and you have praised
the gods of silver and gold, of bronze, iron, wood,
and stone, which do not sce, hear, or understand.
But the God in whose hand are your life-breath and
your ways, you have not glorified” (Daniel 5:23).

St. John’s implication is clear: Israel has become a
Babylon, committing sacrilege by worshiping false
gods with the Temple treasures; like Babylon, she
has been “weighed im the balance and found
wanting”; like Babylon, she will be conquered and
her kingdom will be possessed by the heathen (cf.
Daniel 5:25-21).

Finally, St. John summarizes Israel’s crimes, all
stemming from her idolatry (cf. Romans 1:18-32).
This led to her murders of Christ and the saints (Acts
2:23, 36; 3:14-15; 4:26; 7:51-52, 58-60); her sorceries
(Acts 8:9, 11; 13:6-11; 19:13-15; cf. Revelation 18:23;
21:8; 22:15); her fornication, a word St. John uses
twelve times with reference to Israel’s apostasy (Rev-
elation 2:14; 2:20; 2:21; 9:21; 14:8; 17:2 [twice]; 17:4;
18:3 [ewice]; 18:9; 19:2); and her thefts, a crime often
associated in the Bible with apostasy and the result-
ant oppression and persecution of the righteous (cf.
