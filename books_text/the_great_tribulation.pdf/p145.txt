ALL GREATION TAKES VENGEANCE 129

There is no such thing as ‘natural law”, we might do bet-
ter to speak of God’s “covenantal habits,” or the habit-
ual order which God imposes on Fits creation through the ac-
tions of His angels. Our sciences are nothing more
than the study of the habitual patterns of the per-
sonal activity of God and His heavenly messengers.

This is, in fact, precisely what guarantees the
validity and reliability of both scientific investigation
and prayer. On the one hand, God’s angels have habits
a cosmic dance, a liturgy involving every aspect of
the whole universe, that can be depended upon in all
of man’s technological labors as he exercises domin-
ion under God over the world. On the other hand,
God’s angels are personal beings, constantly carry-
ing out His commands; in response to our petitions,
He can and does order the angels to change the
dance.

There is, therefore, an “Angel of the Waters”; and
he, along with all of God’s personal creation, rejoices
in God’s righteous government of the world. God's
strict justice, summarized in the principle of an eye for
an eye (Exodus 21:23-25), is evidenced in this judg-
ment, for the punishment fits the crime: “They
poured out the blood of saints and prophets,” cries
the Angel of the Waters, “and Thou hast given them
blood to drink!” As we have seen, the characteristic
crime of [srae! was always the murder of the proph-
ets (cf. Il Chronicles 36:15-16; Luke 13:33-34; Acts
7:52): Jesus named this fact as the specific reason
why the blood of the righteous would be poured out in
judgment upon that generation (Matthew 23:31-36).

The Angel of the Waters concludes with an inter-
