186 THE GREAT TRIBULATION

manence of God’s covenant sanctions. God does not
permit such an attack on His sovereignty in time and
eternity. His sanctions never end, for His covenant
never ends.

Kline’s Exposition of the Ritual Sanctions

These covenant sanctions are two-fold sanctions:
cursings and blessings. This two-fold nature of the
covenant sanctions is spelled out in great detail by
Meredith G. Kline in his book, By Oath Consigned
(Eerdmans, 1968). Kline refers to John the Baptizer’s
summary of Christ's ministry: “I indeed baptize you
with water unto repentance: but he that cometh after
me is mightier than I, whose shoes I am not worthy
to bear: he shall baptize you with the Holy Ghost,
and with fire” (Matt. 3:11). What did John have in
mind, “baptizing with fire”? Kline cites Malachi 4:1:
“For behold, the day cometh, that shall burn as an
oven; and all the proud, yea, and all that do wickedly,
shall be stubble: and the day that cometh shall burn
them up, saith the Lorp of hosts, it shall leave them
neither root nor branch.” Stubble cannot grow. It
cannot send roots into the soil for nourishment, nor
grow leaves on branches to absorb sunlight. Without
root and branches, stubble dies, dries, and is easily
set afire.

But there is another source of light than the
burning of stubble, as Malachi 4:2-3 says: “But unto
you that fear my name shall the Sun of righteousness
arise with healing in his wings: and ye shall go forth,
and grow up as calves of the stall. And ye shall tread
down the wicked; for they shall be ashes under your
