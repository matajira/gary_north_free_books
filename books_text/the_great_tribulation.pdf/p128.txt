112 THE GREAT TRIBULATION

be “myriads of myriads,” an expression taken from
Psalm 68:17, which reads: “The chariots of Gad are
double myriads, thousands of thousands” —in other words,
an incalculable number, one that cannot be com-
puted. Attempts to turn this into an exact figure (as
in the supposed size of the Chinese army, or the armed
forces of Western Europe, and so on) are doomed to
frustration. The term simply means many thousands,
and indicates a vast host that is to be thought of in
connection with the Lord’s angelic army of thou-
sands upon thousands of chariots.

Avoiding the dazzling technological speculations
advanced by some commentators on Revelation
9:17-19, we note simply that while the number of the
army is meant to remind us of God’s army, the char-
acteristics of the horses—the fire and the smoke and
the brimstone which proceeded out of their mouths
—remind us of the Dragon, the fire-breathing Levia-
than ( Job 41:18-21), and of hell itself (Revelation 9:2;
19:20; 21:8).

Thus, to sum up the idea: An innumerable army
is advancing upon Jerusalem from the Euphrates,
the origin of Israel’s traditional enemies; it is a fierce,
hostile, demonic force sent by. God in answer to His
people’s prayers for vengeance. In short, this army is
the fulfillment of all the warnings in the law and the
prophets of an avenging horde sent to punish the
covenant breakers. The horrors described in Deuter-
onomy 28 were to be visited upon this evil genera-
tion (see especially verses 49-68). Moses had de-
clared: You shall be driven mad by the sight of what you see
(Deuteronomy 28:34).
