32 THE GREAT TRIBULATION

regard its usage in Scripture. In the first place, con-
sider a fact which will undoubtedly shock some peo-
ple: the word Antichrist” never occurs in the Book of Revela-
tion. Not once. Yet the term is routinely used by
Christian teachers as a synonym for “the Beast” of
Revelation 13. Obviously, there is no question that
the Beast is an enemy of Christ, and is thus “anti”
Christ in that sense; my point, however, is that the
term Antichrist is used in a very specific sense, and is
essentially unrelated to the figure known as “the
Beast” and “666.”

A further error teaches that “the Antichrist” is a
specific individual; connected to this is the notion
that “he” is someone who will make his appearance
toward the end of the world. Both of these ideas, like
the first, are contradicted by the New Testament.

In fact, the only occurrences of the term Antichrist
are in the following verses from the letters of the

Apostle John:

Children, it is the last hour; and just as you
heard that Antichrist is coming, even now
many Antichrists have arisen; from this we
know that it is the last hour.

They went out from us, but they were not
really of us; for if they had been of us, they
would have remained with us; but they went
out, in order that it may be shown that they all
are not of us... .

Who is the liar but the one who denies that
Jesus is the Christ? This is the Antichrist, the
one who denies the Father and the Son.
