The Reconstructionist Option a3

What differences with the Old Covenant era have been intro-
duced? Only the King, the Lord of the Covenant, who speaks by
means of the Holy Spirit is in a position to answer that question
with authority, and thus we look, not to sinful speculation or cul-
tural tradition, but to the inspired word of Christ to guide our
thoughts regarding it. There we are taught that the New Covenant
surpasses the Old Covenant in (1) power, (2) glory, (3) finality,
and (4) realization. Such discontinuities must not be overlooked,
and yet, in the nature of the case, they presuppose an underlying
unity in God’s covenantal dealings. The historical changes in out-
ward administration and circumstance grow out of a common and
unchanging divine intention.

The Old Covenant law as written on external tablets of stone
accused man of sin, but could not grant the internal ability to
comply with those demands. By contrast, the New Covenant writ-
ten by the Holy Spirit on the internal tables of the human heart
communicates life and righteousness, giving the fower to obey
God’s commandments (Jer. 31:33; Eze. 11:19-20; 2 Cor. 3:3, 6-9;
Rem, 7:12-16; 8:4; Heb. 10:14-18; 13:20-21). Although the Old
Covenant had its glory, the sin-laden Jews requested Moses to veil
his face when revealing its stipulations, for it was fundamentally a
ministration of condemnation. But the New Covenant redemp-
tively brings life and confidence before God (2 Cor. 3:7-4:6; Rem.
8:3; Heb. 4:15-16; 6:18-20; 7:19; 9:8; 10:19-20), thus exceeding in
unfading glory (2 Cor. 3:9, 18; 4:4-6; Heb. 3:3). Moreover, unlike
God’s word to Old Covenant believers, special revelation will not
be augmented further for New Covenant Christians; it has reached
its finalized form until the return of Christ. This New Testament
word brings greater moral clarity (removing Pharisaical dis-
tortions of the law, Matt. 5:21-48; 23:3-28, and unmistakably
demonstrating the meaning of love, John 13:34-35; 15:12-13) and
greater personal responsibility for obedience (Luke 12:48; Heb.
Q:1-4; 12:25).

Finally, the New Covenant surpasses the Old in realization.
To understand this, we must take account of the fact that the laws
of the Old Covenant served two different purposes. Some laws de-
