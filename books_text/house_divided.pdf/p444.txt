Subject Index

Present Kingdom, 175-77, 187
Preterism, 257-82
puritans, 46
Reconstruction, 45-50, 287-88
Standard of Truth, 46-50
Sweeping Assertions, 297-98
Theonomy, 68-83, 85-101, 103-16,
126-28

Wisdom, 132-34

Domitianic Persecution, 262

Ebionites, 311, 315
Ecclesiocracy, 79, 293-94, 312
88 Reasons Why the Rapture Is in 1988,

wo, 361, 374
Eschatology, 139-40

History, 233-54

Social Action, 21

See also Premillennialism;

Postmillennialism

Eschatology of Victory, 306
Ethics, 13-19

Social Action, 21
Evangelism, 193-96, 313

Fall of Man, 150-51
Fringe Law, 304-5
Fundamentalism, xii-xix
The Fundamentals, xiv
Futurism, 280-82

Garden of Eden, 149-51

General Revelation, 118

Gifts to Church, 223-25

The Gospel According to Jesus, 368-66

Gospel Victory Theme, 140, 347-48
Biblical Basis, 149-58, 200-1,

213417

Gifts, 222-25

Grace Theological Seminary, xliv,
xlvii, xviii, xlix

Gradualism, 217-22

Great Commission, 194-210

409

Greek grammar, 370-72
Guilt by Association, 58-59

Hermeneutics, 265, 267-68, 275-76,
314

Holy Spirit, 154

Home Schools, xli

Howard Payne College, xxvi

Inference, 76, 141-42

‘The Institutes of Biblical Law, xxxviii-
xxxix, xlv, 287, 305

Israel, Old Testament, 76-77

Jehovah’s Witnesses, 59, 326-40
Jews
Dispensationalism, 164-71
Postmillennialism, 216-17
Journal of Christian Reconstruction, 258
Judicial Law, 96-101

Kingdom of God, 83, 140-41
Consummation, 188-89
Establishment, 175-91
Expectation, 149-58
Gospel, 193-96
Gradualism, 218-21
Heaven, 189-90
Mission, 196-210
Nations, 164-71
Outcome, 213-25
Presence of Christ, 162-64
Preterism, 257-82
Spiritual Character, 159-74, 183

Kingdoms in Conflict, 356-57, 358

Last Daya, 155

‘The Late Great Planet Earth, 361, 363,
364, 365, 376

Law of God, 14-15, 68-69, 85-101,
351-59
Character of God, 85-101
Civil Law, 125-34
