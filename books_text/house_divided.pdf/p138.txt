The Theological Concept of Law 89

which he speaks it... . Scripture shows God revealing different
expressions of the law appropriate for different times .“

We can add to this analysis the observation that House and Ice
appear to state their disagreement with theonomic thinking by
using the words that the Gentile nations “are not under the provi-
sions of” the Mosaic manifestation of God’s law. ‘ It is in that
sense they later speak of “varying laws [which] flow from God’s
eternal purposes .“ 15

Bringing together all of the above, it would seem that what
House and Ice are trying to say amounts to this claim:

It does not follow from God’s unchanging moral character that
His moral demands - i. e., the “provisions” or principles which
are given different, specific “expressions” at different times and.
places — are required in all ages and cultures.

On that interpretation, however, House and Ice have rather
obviously contradicted themselves. The presumption of continuing
and universal validity for the moral Provisions (underlying de-
mands, not specific cultural details) of God’s law does indeed “fol-
low” from their reflection of His essential and unchangeable char-
acter. They may be “expressed” in different ways, but God’s moral
requirements (e. g., self-giving love to one’s” neighbor) are the
same everywhere.

13, Ibid., p. 88.

14, Ibid., p. 86.

15, Ibid., p. 88. Actually, at this point House and Ice have slipped from one
theological concept (God’s essential ‘character”) into a logically different one
(God’s “eternal purposes”). The second denotes God’s good pleasure which is not
constrained or necessary (and thus could have been otherwise), while the first de-
notes what is always and necessarily true of God. (It is conceivable that their in-
exact vacillating between different theological concepts contributes to the
obscurity of their reasoning and ambiguity in what they are trying to say.) The
prohibition of stealing stems from God’s unchanging character (it is not an open
question whether God would choose to condemn or to commend stealing),
whereas the provision of atoning sacrifice stems from God’s eternal purpose (ne-
cessity did not constrain, but God graciously chose it in His good pleasure). That
is why God could purpose to change the way of atonement (Christ's cross fulfills
the anticipatory animal sacrifices), but could not purpose to violate His character
and now commend stealing to us.
