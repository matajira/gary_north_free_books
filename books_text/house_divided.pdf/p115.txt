66 House Divided

Should a Christian decide on the basis of the reasoning or the kind
of reasoning which is found throughout Dominion Theology? Upon
examination and reflection, we cannot draw anything other than
a negative answer. The book is rebutted by its own doctrinal and
logical failures:

1. House and Ice have in practice resorted at points to
false standards for judging the theological acceptability of Recon-
structionism, betraying a commitment to sola Scriptura,

2, Beyond that, a large portion of their book resorts to
venomous speculation (“potential dangers”) and notorious logical
fallacies in trying to discredit their opponents — inconsistencies,
short-sightedness, guilt by association, irrelevant personal
chiding, etc.

3. In evangelical scholars, these kind of failures in theological
reasoning are disappointing.

4, If House and Ice fell into one such line of fallacious reason-
ing, it would not commend their book. But because they repeat-
edl y fall into these fallacious lines of thought, their polemic
against Reconstructionism has lost its credibility and value.
