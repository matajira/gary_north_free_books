368

Lindsey:

“But David Chilton uses a
typical debater’s tactic to cast
doubt on the reliability of the
source. There is no legitimate
reason to doubt the veracity of
the source. This is why Chilton
resorts to the weak statement,
U |. he [Irenaeus] may have
meant, .. .”

Lindsey:

“Chilton concludes his ar-
gument by making a totally
unfounded, unsupported, and
speculative statement: ‘Cer-
tainly, there are other early
writers whose statements indi-
cate that St, John wrote the
Revelation much earlier, under
Nero's persecution.’ But then
he doesn’t give us even one of
these phantom ‘other early
writers’ to support his confi-
dent boast .“*

Lindsey:

“If the Apostle John were
exiled to Patmos and wrote
the Book of Revelation during
the reign of Nero (4.. 54-68),

House Divided

House and Ice:

“Chilton’s approach is noth-
ing more than a debater’s tech-
nique. When you do not have
strong reasons against some-
thing then you try to cast doubt
upon the reliability of the source.
But no reagon exists to doubt
the veracity of the source.
Otherwise, Chilton would have
given some specific reasons
rather than resorting to the
we of the word ‘may.”3

House and Ice:

“Chilton concludes his cri-
tique of the early church tradi-
tion by making a totally un-
founded, unsupported, and
speculative statement: ‘Cer-
tainly, there are other early
writers whose statements indi-
cate that St. John wrote the
Revelation much earlier, under
Nero’s persecution ” But he
does not produce those other
early writers.”5

House and Ice:

“It would be strange, if
the book really was produced
at the end of Nero's reign, that
so strong a tradition arose as-

2, Hal Lindsey, The Road to Holocaust (New Yorke Bantam Books, 1985), p. 245.
3, House and Ice, Dominion Theology, pp. 252-53.

4, Lindsey, Road to Holocaust, p. 245.

5, House and Ice, Dominion Theology, p. 253,
