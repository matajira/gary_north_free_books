XXViil House Divided

forth this, which ye now see and hear. For David is not ascended
into the heavens: but he saith himself, The Lord said unto my
Lord, Sit thou on [at] my right hand, Until I make thy foes thy
footstool” (Acts 2:32-35). And when will this be? When death is
conquered at the end of history: “For he must reign, till he bath
put all enemies under his feet. The last enemy that shall be de-
stroyed is death” (1 Cor. 15:25-26).

There is no question that this is a postmillennial passage. Let
me also assure the reader that there is no possible way for a pre-
millennialist to explain this passage in terms of his system, let
alone a dispensationalist. And so premillennialists rarely com-
ment on it. It is one of those favorite neglected passages in the pre-
millennial camp.

Rev. Ice is a very confident man. Consider, however, the justi-
fication for Rev. Ice’s self-confidence in light of the fact that traces
of postmillennial theology can be found in the writings of John
Calvin. 24 Some historians would trace this eschatology back at
least to the fourth century church historian, Eusebius. The post-
millennial system was subsequently developed by the Puritans in
the seventeenth century.” In contrast, Rev. ‘Ice’s own theological
system — premillennial, pre-tribulational dispensationalism — was
developed at the earliest in 1830."Now, when someone tells you
that there is not a single Bible verse that teaches a doctrine that
has been held by a significant segment of the Puritans, plus Mat-
thew Henry, Jonathan Edwards, Charles Hodge, A. A. Hodge,
Benjamin B. Warfield, W. G. T. Shedd, and O. T. Allis, you are
entitled to take such statements with more than a grain of salt.
Such universal negative assertions — “Not a single verse!”— are
made only by theological amateurs who are utterly unfamiliar

24, Greg L, Bahnsen, “The Prima Facie Acceptability of Postmillennialism,”
Journal of Christian Reconstitution, (Winter 1976-77), pp. 69-76.

25. Ibid, pp. 77-88.

26. Clarence B. Bass, Background tor Diy sationalism: ie fisaren! Genesis and
Ecclesiastical Implication (Grand Rapids, MI: Eerdmans, 1960), Chapter 2. Se
also Dave McPherson, The Great Rapture Hoax Fletcher, Nee New Puritan
Library, 1983),
