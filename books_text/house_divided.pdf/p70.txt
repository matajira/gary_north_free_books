20 House Divided

nationalists to be this: “How and in what ways are we commis-
sioned to be involved in this world?””° The answer to this question
leads us to see another aspect or tendency in the lifestyle fostered
by dispensationalism (in addition to the three we have seen above)
— namely, (4) withdrawal from social involvement and from re-
form of education, economics, and politics. This attitude is some-
times termed “pietism.”21 A Dave Hunt, for instance, castigates
evangelical “during the Reagan years” because they subordinated
belief in an imminent (any-moment) rapture of believers to
heaven. The deplorable result, he feels, is that “the Church suc-
cumbed once again to the unbiblical hope that, by exerting godly
influence upon government, society could be transformed.” He
condemns “the false dream of Christianizing secular society,” and
holds that it maybe as important as the Reformation itself for dis-
pensationalists to divide from and oppose those “who believe it is
our duty to Christianize society.””

Dispensationalism leans toward pietism for a couple of obvi-
ous reasons. The Old Testament revelation of God’s moral will
took into account numerous details of socio-political behavior,
while the New Testament does not repeat the same emphasis.
There is much more to be found in the Old Testament about the
larger concerns of civil society than in the New. Reconstruction-
ists find the explanation for this in the fact that God’s Old Testa-
ment revelation was an expression of His perfect will, and once
God has spoken to a subject, He does not need to repeat Himself.
The New Testament focuses (though not exclusively) upon the

20, House and Ice, Dominion Theology, p. 9.

21, Herbert Schlossberg and Marvin Olasky write: We need to understand
the crucial difference between a vital component of authentic Christianity, piety,
and a false ideology that we call pietism.” They define ‘piety’ as “a reverence for
God, as evidenced in prayer, Scripture reading, and doing mercy to others,”
while ‘pictism’ is taken as “2 belief that the practice of piety is all the Christian has
to do, and that it is alright to ignore larger concerns of the society.” See Turning
Point: A Christian Worldview Declaration (Westchester, IL: Crossway Books, 1987),
pp. 25, 152; Chapter 2 is entitled “Piety vs, Pietism.”

22, Dave Hunt, Whatever Happened to Heaven? (Eugene, OR: Harvest House,
1988), pp. 8-9,
