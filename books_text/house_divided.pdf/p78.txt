3
THE RECONSTRUCTIONIST OPTION

Continuation of the comparison with dispensationalism.

When readers turn to Dominion Theology for a summary of the
Reconstructionist view of ethics (theonomy), they do not get a
balanced, refined or adequate explanation of the position. Per-
haps this is not surprising. But without a better picture of what
theonomic ethics actually maintains (and why it maintains it), the
reader cannot fairly evaluate the position for himself or herself. * It
would be more satisfactory to rehearse the Reconstructionist out-
look in the following way,* beginning with the question of whether
it is legitimate from a biblical standpoint to make contemporary
use of the Old Testament revelation of God’s law for human conduct.

On the one hand, to deny that dictates revealed in the Old
Testament are unchanging moral absolutes is implicitly to en-

1 House and Ice gratuitously assert at one point that “Bahnsen has so modified
some of his views” that the theonomic position is dying the death of a thousand
qualifications (EL, Wayne House and Thomas Ice, Dominion Theology: Blessing or
Curse? [Portland, OR: Multnomah, 1988], p. 20), Absolutely no examples or
substantiation is given, and I (Bzhnsen) have no idea what they imagine has
been modified, As far as I know. my publications and lectures subsequent to the
appearance of Theonomy in Chrisiian Ethics present nothing of any significance
which contradicts the position taken earlier, Moreover, fuller explanation and
consistent refinement of a thesis is not usually deemed a fault (‘dying’), buta vir-
tue, If Tsay “There is a cat on the mat.’ the truth of my thesis is not challenged if
I qualify it further by saying, “There is a black cat on the mat,’

2, The following synopsis, with slight changes and a few additional notes, is
taken from Bahnsen’s paper, “The Theonomic Position,” found in God and Politics:
Four Views on the Reformation of Civil Government, ed. Gary Soott Smith (Phillips-
burg, NJ: Presbyterian and Reformed, 1989). The article goes on to apply what
is rehearsed here to socio-political ethics, interacting with the opposing viewpoint
of pluralism.

29
