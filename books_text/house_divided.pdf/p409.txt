Hal Lindsey’s “The Road to Holocaust” 373

ment is horribly superficial. Besides, Lindsey himself clearly
teaches that the Seven Churches of Revelation represent seven
eras of Church history right up to our own time! 28 Does this not
deny an imminent coming before our era?

Elsewhere he warns against “eisegesis” and suggests that such
describes “the Dominionist method of interpretation.” But he is
quite adept at eisegesis! For instance, as we have noted, he reads
a Greek word into Irenaeus’ text and builds an argumentative re-
buttal on the basis of it! 30 He reads a “genitive construction” into
Matthew 28:19, where there is none. *! Ironically, in attempting to
prove a literalistic hermeneutic, he attempts to rebut Chilton’s
statement that there will be no personal Antichrist. He does so by
citing 1 Thessalonians 2:1-12 — despite the fact the word “Antichrist”
does not even appear in the text! It ony occurs in the epistles of
John and there indicates that “Antichrist” is not an individual!
How could this use of 1 Thessalonians prove a literal Antichrist
when it does not even mention him? Is this not eisegesis?

And what of his dealing with a modern scholar’s writing?
Somehow a quotation by J. L. Neve is supposed to prove the
Apostolic Fathers believed in a future Jewish supremacy in the
Millennium, Lindsey quotes three paragraphs from Neve and
then writes: “Note carefully the following crucial facts from this quote.
... [T]he early church . . . firmly believed that Israel was yet to
be redeemed as a Nation and given her unconditionally promised
Messianic Kingdom.”*? I have read and re-read the Neve quota-
tion given in Lindsey’s work and it simply does not make a2) men-
tion of the Jews or of their becoming a redeemed nation ai ail!
Read it yourself. Is this not eisegesis of a modern text?

After analyzing what Lindsey thinks he is reading from Neve
he states: “These six prophetic views caused the early Christians
to recognize the Jews as a chosen people with whom God will yet

28, Hal Lindsey, There's A New World Coming, Chapter 1.
29, Lindsey, Road to Holocaust, p-53.

30. Ibid., p. 240.

31. Ibid, p. 277.

32, Ibid, p. 10 (emphasis mine),
