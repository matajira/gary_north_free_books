152 House Divided

great enemy — undoubtedly Satan, the head of a nefarious king-
dom. This passage portrays a mighty struggle between the
woman’s seed (Christ and His kingdom, cp. Rem. 16:20; cf. 1
Cor, 12:12-27; John 15:1-7; Matt. 25:40, 45) and the serpent’s seed.
(Satan and his kingdom). This, then, is the theological explana-
tion of struggle in history. It must not be overlooked that the point
of this poetic proteevangelium is victory. A victory won by Christ.
Later revelation in the New Testament shows that this prophecy
began its fulfillment at the death-resurrection-ascension of Christ
(1 John 3:8; Heb. 2:14; Col. 2:14,15; ep. Rev. 20:1-3), it is not
awaiting some distant beginning of its fulfillment.

Thus, here we have at the very inception of prophecy in Scrip-
ture the certainty of victory. Just as the Fall of Adam has a world-
wide negative effect, so is God’s salvation, on the basis of the res-
urrection of Christ, to have a world-wide positive effect. "The
crushing of Satan (Gen. 3:15) is not awaiting a consummative,
catastrophic victory of Christ over Satan at the penultimate mo-
ment of present history. The idea (as will be more fully seen later)
is that Satan the Destroyer, his nefarious kingdom, and its evil
effects will be overwhelmed by the superior strength and glory of
Christ the Lord, Who has already come (i. e., His First Advent).
The specific means of its fulfillment must await later revelation.

The Patriarchal and Early Mosaic Expectation of Victory

As the redemptive cord grows stronger and the scarlet thread.
is woven more distinctively into the fabric of Scriptural revelation
and history, the eschatological hope of redemptive victory trails
right along, becoming itself more evident and more specific. The
post-Adamic era of the patriarchs and the early Mosaic era are de-
monstrative of this fact. A list of a few of the more significant ref-
erences in these eras will illustrate this truth.

Genesis 12:2-3; 13:14-16; 15:5; 16:10; 22:17-18; 26:4 promise
that “all the families of the earth” will be blessed through the out-

7, Gary North, Is the World Running Down?: Crisis in the Christian Worldview
(Ft. Worth: Dominion, 1988),
