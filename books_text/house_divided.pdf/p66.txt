16 House Divided

there is a natural, logical pull toward relativism which can be
resisted only by an effort of will. Dispensationalism itself as a the-
ology (or worldview) does not have the internal resources to avoid
a situationally variable interpretation of God’s moral command-
ments since God Himself (on this theory) changes the rules. What
was immoral before Christ is not as suck immoral after Christ; what
was immoral in Israel is not as such immoral among the Gentiles.

Dispensationalist Charles Ryrie categorically dismisses the
validity of Old Testament commands for non-Jews. Why? He
writes: “The law was never given to Gentiles and is expressly done
away for the Christian. ... Neither are the words of Malachi 3
for the Christian” since the passage is addressed to the sons of
Jacob.’

Dispensationalism becomes, ironically, a Christianized ver-
sion of cultural relativism, ° particularly in its view of fluctuating
ethical standards throughout history:

The Scriptures divide time . . , into seven unequal periods,
usually called “Dispensations”. .. . These periods are marked
off in Scripture by some change in God’s method of dealing with
mankind, in respect of the two questions: of sin, and of man’s re-
sponsibility, 7

A dispensation is a period of time during which man is tested in
respect of obedience to some specific revelation of the will of
God.

5, Charles Caldwell Ryrie, Balancing the Christian Lyfe (Chicago, IL: Moody
Press, 1969), p. 88.

6. That is, the validity of moral standards is relative to the culture in which
they are promulgated: “the very definition of what is normal or abnormal is rela-
tive to the cultural frame of reference” (M. J. Herskovits,Cultural Relativism
iNew York: Random House, 1973], p. 15). Dispensationalists would not agree,
of course, that cultural acceptance (human agreement) constitutes the sole au-
thority for the changing moral codes in different dispensations.

7.C, 1, Scofield, Rightly Dividing the Word of Troth (Neptune, NJ: Loizeaux
Brothers, 1896), p. 12 (emphasis mine),

8, The Scofield Reference Bible, ed.C, I, Scofield (New York: Oxford University
Press, 1909), p. 5 (at Gen, 1:28).
