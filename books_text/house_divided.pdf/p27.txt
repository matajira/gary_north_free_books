Publisher’s Foreword xxvii

nialism. 7! He repeats this accusation in the Preface to Dominion
Theology:

My challenge is simply this: Since postmillennialism is on
every page of the Bible, show me one passage that requires a
postmillennial interpretation and should not be taken in a pre-
millennial sense. After fourteen years of study it is my belief that
there is not one passage anywhere in Scripture that would lead to
the postmillennial system. The best postmillennialism can come
up with is a position built on an inference. 22

All right, Tommy, how about Psalm 110, verses 1 and 2? This
psalm is quoted in the New Testament more than any other Old
Testament passage. It was quoted during the first century of the
early church more than any other passage. 23 Consider its words:

A Psalm of David. The Lorn said unto my Lord, Sit thou at my
right hand, until I make thine enemies thy footstool. The Lozp
shall send the rod of thy strength out of Zion: rule thou in the
midst of thine enemies (Psalm 110:1-2).

Who is ruling? Jesus. Where does Jesus sit? At God’s right
hand. Where is this located? In heaven. Stephen announced: “Him
bath God exalted with his right hand to be a Prince and a Saviour,
for to give repentance to Israel, and forgiveness of sins. . . . But he,
being full of the Holy Ghost, looked up steadfastly into heaven,
and saw the glory of God, and Jesus standing on [at] the right
hand of God, And said, Behold, I see the heavens opened, and the
Son of man standing on [at] the right hand of God” (Acts 5:31;
7:55-56). How long will Jesus remain in heaven? Until God has
made a footstool of all Christ’s enemies. Peter announced at Pente-
cost: “This Jesus bath God raised up, whereof we all are witnesses.
Therefore being by the right hand of God exalted, and having re-
ceived of the Father the promise of the Holy Ghost, he bath shed

21, Audiotapes and a videotape are available of this debate: Institute for
Christian Economica, P.O. Box 8000, Tyler, Texas 75711,

22. Ice, “Preface,” Dominion Theology, p. 9.

23, David M. Hay, Glory at the Right Hand: Psalm 110 in Early Christianity (Nagh-
ville, TN: Abingdon Preas, 1973), p. 15.
