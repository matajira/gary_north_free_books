The Reconsiructionist Option 36

and reality, acknowledging that ‘apart from the shedding of blood
there is no remission” from the guilt of sin (Heb. 9:22).

The New Covenant People of God

In connection with the superseding of the Old Covenant shad-
ows, the redemption secured by the New Covenant also redefines
the people of God. The kingdom which was once focused on the
nation of Israel has been taken away from the Jews (Matt. 8:11-12;
21:41-43; 23:37-38) and -given to an international body, the Church
of Jesus Christ. New Testament theology describes the Church as
the “restoration of Israel” (Acts 15:15-20), “the commonwealth of
Israel” (Eph. 2:12), the “seed of Abraham” (Gal. 3:7, 29), and “the
Israel of God” (6:16). What God was doing with the nation of
Israel was but a type looking ahead to the international Church of
Christ. The details of the old order have passed away, giving place
to the true kingdom of God established by the Messiah, in which
both Jew and Gentile have become “fellow-citizens” on an equal
footing (Eph. 2:11-20; 3:3-6).

It is important for biblical interpretation to bear this in mind
because certain stipulations of the Old Covenant were enacted for
the purpose of distinguishing Israel as the people of God from the
pagan Gentile world. Such stipulations were not essentially moral
in function (forbidding what was intrinsically contrary to the
righteousness of God), but rather symbolic. This accounts for the
fact that they allowed Gentiles to do the very thing which was for-
bidden to the Jews (eg., Deut. 14:21). Accordingly, given the
redefinition of the people of God in the New Covenant, certain
aspects of the Old Covenant order have been altered: (a) the New
Covenant does not require political loyalty to Israel (Phil. 3:20) or
defending God’s kingdom by the sword (John 18:36; 2 Cor. 10:4).
(b) The land of Canaan foreshadowed the kingdom of God (Heb.
11:8-10; Eph. 1:14; 1 Peter 1:4) which is fulfilled in Christ (Gal. 3:16;
ef, Gen. 13:15), thus rendering inapplicable Old Covenant provi-
sions tied to the land (such as family divisions, location of cities of
refuge, the levirate). (c) The laws which symbolically taught
Israel to be separate from the Gentile world, such as the dietary
provisions (Lev. 20:22-26), need no longer be observed in their
pedagogical form (Acts 10, esp. v. 15; Mark 7 :19; Rem. 14:17), even
