358 House Divided

make the Old Testament commandments “suggestions” since they
are not obligatory. Colson seems to believe that Exodus 22 is more
than just suggestive. He tells us that the case laws regarding resti-
tution are “the only answer to the crime problem.”

(2)

[Tihe citizens of the kingdom of God living in the midst of
the kingdoms of the world provide a respect for the Law that
stands beyond human law, It means the presence of a commu-
nity of people whose values are established by eternal truths,
There is no other place that a culture can find those values.

a

“How about the revealed propositional truth of Scripture,
because that is the Law that is beyond law?” The Bible provides
a basis for absolute truth, for true right and wrong, It is only the
citizens of the kingdom in the midst of the kingdoms of man that
make that discovery possible. ”!

Comment: What is the Law that stands beyond human law?
Colson tells us that “the Bible provides a basis for absolute truth,
for true right and wrong.” Colson does not point us to natural law
since he describes this law as “the revealed propositional truth of
Scripture.” This is what Reconstructionists have been saying: The
Bible is the standard. 22 Colson, like Reconstructionists, believes
the whole Bible (with some reservations) as the standard.

@)

In his Kingdom in Conflict, Colson decries a “utopianism” that he
says “is often articulated today in contemporary Christian circles .” He
tells us that “such preoccupation with the political diverts the church
from its primary mission” for the salvation of man’s soul. But there is
another risk, particularly among “those on the political right where
many want to impose Christian values on society by force of law.”

21, Colson, “The Kingdom of God and Human Kingdoms,” p. 151.

22, Greg L, Bahnsen, By This Standard: The Authority of God's Law for Today
(Tyler, TX: Institute for Christian Economics, 1985),

23, Charles Colson, Kingdoms in. Conflict (Grand Rapids, MI; Zondervan,
1987), p. 117.
