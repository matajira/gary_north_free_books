How Should We Then Decide? 55

there must be a ‘tendency’ or ‘potential danger’ that the position of
our opponents ‘could possibly’ be ‘in effect’ the same as MUD. (f)
Therefore, readers should reject the position of our opponents,
watching out for its MUD .“ We can designate this line of reason-
ing the mud-slinging fallacy.

Let us apply the form of this fallacious thinking to the authors
of Dominion Theology. They are not theological liberals and would
actually oppose liberalism. However, dispensationalism is not a
personal prophylactic against moving into liberalism; some peo-
ple have done it. In fact, in tune with the liberals, House and Ice
have been known to urge extra-biblical reasons for rejecting some
position which is presented as biblical. Bahnsen has pointed this
out at the beginning of this chapter. Moreover, liberals are notori-
ous for rejecting the authority of the Old Testament (ridiculing
the ethical code that allowed for holy war or capital punishment or
slavery, etc. ) — just as dispensationalists are against the ethical
perspective of the Old Testament (cf. the “antinomianism” in dis-
pensationalism which Bahnsen observed in Chapter 2 above). It is
possible that dispensationalism is not free of liberalism, then. The
tendency in dispensationalism is to produce liberal thinking and.
attitudes. In effect, dispensationalism is liberalism. Therefore,
readers, if you oppose liberalism, you will want to reject dispensa-
tionalism too!

But then, this same line of thinking can be applied to the “po-
tential danger” of Romanism in dispensationalism. At the begin-
ning of this chapter Bahnsen has shown that House and Ice have,
in practice and at points, done what a Romanist would do: urge
against a theological position that it is out of accord with tradition
or with the agreement of their religious group. Looks like “possi-
ble” Romanism to me! And if it might be, then it probably is. At
least, we have to say that there is a tendency toward Romanism in
dispensationalism. Oh, but that’s not all. No, no. In Chapter 2
Bahnsen has observed and documented the general tendency for
dispensationalists to be pietists who deny that the Christian is
called to engage in reforming society, politics, education, etc.
Pietists emphasize personal holiness, but withdraw from cultural
