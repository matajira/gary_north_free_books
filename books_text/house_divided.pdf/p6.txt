Ye are the salt of the earth: but if the salt have lost his savour,
wherewith shall it be salted? it is thenceforth good for nothing, but
to be cast out, and to be trodden under foot of men. Ye are the
light of the world. A city that is set on an hill cannot be hid.
Neither do men light a candle, and put it under a bushel, but on a
candlestick; and it giveth light unto all that are in the house. Let
your light so shine before men, that they may see your good.
works, and glorify your Father which is in heaven. Think not that
Iam come to destroy the law, or the prophets: I am not come to
destroy, but to fulfill. For verily I say unto you, Till heaven and
earth pass, one jot or one tittle shall in no wise pass from the law,
till all be fulfilled. Whosoever therefore shall break one of these
least commandments, and shall teach men so, he shall be called
the least in the kingdom of heaven: but whosoever shall do and
teach them, the same shall be called great in the kingdom of
heaven (Matthew 5:13-19).

At the heart of the problem of legalism is pride, a pride that
refuses to admit spiritual bankruptcy. That is why the doctrines of
grace stir up so much animosity. Donald Grey Barnhouse, a giant
of a man in free grace, wrote: “It was a tragic hour when the Ref-
ormation churches wrote the Ten Commandments into their
creeds and catechisms and sought to bring Gentile believers into
bondage to Jewish law, which was never intended either for the
Gentile nations or for the church.”! He was right, too.

8. Lewis Johnson (1963)"

1, He cites Barnhouse, Gods Fredo, p. 134.
2, S, Lewis Johngon, “The Paralysis of Legalism,” Bibliotheca Sacre, Vol, 120
(April/June, 1963), p. 109,
