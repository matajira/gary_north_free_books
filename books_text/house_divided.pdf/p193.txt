148 House Divided

Summary

1, Eschatology is a deep and involved aspect of Christian the-
ology that may not be approached in a simplistic manner. No one
passage may be expected to present an entire eschatological system.

2, The Gospel Victory Theme dominates the entire prophetic
Scriptures, from Genesis 3:15 on.

3. The substitution of a defeatist scheme regarding Christian
endeavor for the Gospel Victory Theme has paralyzed Christian
endeavor in this century.

4, Postmillennialism holds that the prophesied kingdom of
Christ was established in the first century and will victoriously
spread throughout the earth by means of the propagation of the
Gospel of the saving mercies of Jesus Christ.

5. There is coming a time in earth history, continuous with
the present and resultant from currently operating spiritual
forces, in which the overwhelming majority of men and nations
will in salvation voluntarily bow to the Lordship of Jesus Christ,
thus ushering in an era of widespread righteousness, peace, and.
prosperity.

6. Christ will not return to the earth until after His Spirit-
blessed Gospel has won the victory in history (His coming is post-
millennial),

7. Postmillennialism has been held by a great number of stal-
wart, evangelical and Reformed scholars in history.
