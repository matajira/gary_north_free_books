46 House Divided

that are in primary deviance from contemporary evangelicalism
and Protestant Christianity in general — theonomy and postmil-
lennialism. . ..2

These views, they claim, are “shocking to some and ques-
tioned by most of the evangelical theological community.” But
this kind of criticism, of course, is:

. unsubstantiated (House and Ice are not acquainted with most
evangelical, even contemporary ones),

eself-serving (who determines who will count as a “mainstream”
evangelical?),

. contrary to historical fact (simply forgetting two centuries of
Puritan influence and even nineteenth-century evangelicalism),*

eand above all, anything $ut Protestant in character.

2, Ibid, pp. 16-17.

3, Ibid, .D. 20,

4, Although the evidence is rather clear and has been presented a number of
times, House and Ice waver regarding it (pp. 90-98), Actually, they stumble into
downright self-contradiction in expressing their own view about historical prece-
dence: “it is true the Puritans were generally theonomistic in outlook” (House
and Ice, Dominion Theology, p. 94) — but on the next page: “the movement as a
whole was never in wholehearted agreement” [with theonomy] (p. 95). Well,
which is it? (The reader may pursue this subject in the “Symposium on Puritan-
ism and Law,” The Journel of Christian Reconstruction [Winter, 1978-79], passim. )

House and Ice likewise betray how unstudied they are in this subject when they
categorically state “Calvin opposed implantation of the Mosaic law into civil life”
(p. 93), apparently basing this opinion on a remark in the Institutes about a cer-
tain “perilous and seditious” view — when in fact Calvin was not referring to en-
dorsement of the Mosaic civil law, but the using of it as a pretext for revolu-
tionary repudiation of the powers that be by Anabaptist radicals (cf. Jack W.
Sawyer, Jr., Moses and the Magistrate: Aspects of Calvin’s Political Theory in
Contemporary Focus,” unpublished masters thesis, Westminster Theological
Seminary, 1986), Calvin wrote “Absurd is the cleverness which some persons but
little versed in Scripture pretend to, who assert that. . . the obligations under
which Moses laid his countrymen are now dissolved” (commentary at Lev. 23:6).
‘One needs to read Calvin's sermons on Deuteronomy or consult the biblical
eters of his conduct in Servetus affair (where he calls the Mosaic judicial law “a

erpetual rule”), etc. Because of such indications as these, House and Ice are
Forced to call the evidence on Calvin “mixed,” and they accuse Calvin’s thinking
of being “confused” (pp. 91, 93). But T(Bahnsen) find the Genevan scholar far
more clear and consistent in his thinking than his current detractors.
