The Reconstructiontst Option 4t

rape, steal, gossip, or envy! Christ did not come to change our
evaluation of God’s laws from that of holy to unholy, obligatory to
optional, or perfect to flawed. Listen to His own testimony:

Do not begin to think that I came to abrogate the Law or the
Prophets; I came not to abrogate but to fulfill, For truly I say to
you, until heaven and earth pass away, until all things have hap-
pened, not one jot or tittle shall by any means pass away from
the law, Therefore, whoever shall break one of these least com-
mandments and teach men 80 shall be called the least in the
kingdom of heaven (Matthew 5:17-19).

Several points about the interpretation of this passage should be
rather clear. (1) Christ twice denied that His advent had the pur-
pose of abrogating the Old Testament commandments. (2) Until
the expiration of the physical universe, not even a letter or stroke
of the law will pass away. And (3) therefore God’s disapprobation
rests upon anyone who teaches that even the least of the Old Testa-
ment laws may be broken. s The underlying ethical principles or
duties which are communicated in the minute details (jot and
tittle) of the law of God, down to its least significant provision,
should be reckoned to have an abiding validity — until and unless
the Lawgiver reveals otherwise.

Of course, nothing which has been said above means that the
work of Christian ethics is a pat and easy job. Even though the
details of God’s law are available to us as moral absolutes; they
still need to be properly interpreted and applied to the modern
world. It should constantly be borne in mind that no school of
thought, least of all the theonomist outlook, “has all the answers.”
Nobody should get the impression that clear, simple, or uncon-
testable “solutions” to the moral problems of our day can be just

8. Attempts are sometimes made to evade the thrust of this text by editing out
its reference to the moral demands of the Old Testament — contrary to what is ob-
vious from its context (5:16, 20, 21-48; 6:1, 10, 33; 7:12, 20-21, 26) and semantics
(the law” in v. 18, “commandment” in v. 19). Other attempts are made to extract
an abrogating of the law’s moral demands from the word “fulfill” (v. 17) or the
phrase “until all things have happened” (v. 18). This, however, renders the verses
self-contradictory in what they assert,
