350 House Divided

Let us reformulate the verse quoted above: “Any theological
position divided against itself is laid waste; and dispensationalism
divided against itself shall not stand.”

Dispensationalism cannot stand since: (1) There are few able
defenses of dispensationalism being published since the metamor-
phosis of the position. Dispensationalism has gone through such a
transformation process that it needs a new scholarly defense. (2)
Dispensationalism is being questioned by the more orthodox
charismatic. ? Dr. Joseph Kikasola, professor of international
studies and Hebrew at CBN University believes that there has
been au ‘diminishing of dispensationalism,’ especially among charis-
matic, who, he says, are coming to see that ‘charismatic dispensa-
tionalist’ is ‘a contradiction in terms. ”° (3) The date-setting element
of dispensationalism is losing its fascination with many of its ad-
herents since the fortieth anniversary of Israel’s nationhood (1948-
88) has passed without a rapture. Dave Hunt, a proponent of the
national regathering of Israel as the time text for future prophetic
events, writes: “Needless to say, January 1, 1982, saw the defection
of large numbers from the pretrib position. . .. Many who were
once excited about the prospects of being caught up to heaven at
any moment have become confused and disillusioned by the
apparent failure of a generally accepted biblical interpretation
they once relied upon.”* He goes on later to assert: Gary “North’s
reference to specific dates is an attack upon the most persuasive
factor supporting Lindsey’s rapture scenario: the rebirth of na-
tional Israel. This historic event, which is pivotal to dispensation-
alism’s timing of the rapture, as John F. Walvoord has pointed.
out, was long anticipated and when it at last occurred seemed to
validate that prophetic interpretation .”°(4) The schizophrenia
within dispensationalism and evangelicalism over the application

2, Traditionally, pentecostalism has been dispensational,

3, Randy Frame, “The Theonomic Urge,” Christianity Today, (April 21, 1989),
p. 38,
4, Dave Hunt, Whatever Happended to Heaven? (Eugene, OR: Harvest House,
1988), p. 68,

5, bid, p. 64,

 
