The Preterist Interpretation of the Kingdom 269

Another endorser of House and Ice, Dave Hunt, has written
similarly regarding certain prophecies, which are related to Reve-
lation: “For at least 200 years, prophecy students had identified
Russia, long before it became a world military power, as the
leader of a biblically prophesied confederacy of nations that would
attack Israel in the last days .“” What about the original readers
1900 years ago? This is a major difference between their dispensa-
tional and our Reformed hermeneutic.

The text-bracketing temporal indicators as pointed to by pret-
erists cannot lightly be dismissed, however. John is writing to
seven historical churches (Rev. 1:4, 11; 22: 16), which are expecting
troublesome times (2-3). He testifies to being with them in “the
tribulation” (1: 9). And despite Lindsey, Hunt, and other dispensa-
tionalists, he expects those very churches to hear and understand
(1:3; 22:10) the “revelation”"(1:1) and to heed the things in it (1:3;
22:7), because of the nearness of the events (1:1, 3; 22:6, 10).

Original relevance, then, is the lock and the time-texts the key
to opening the door of Revelation. And think, What terms could
John have used to speak of contemporary expectation other than
those that are, in fact, found in Revelation 1:1, 3; 22:6, 10 and other
places? 5?

Objections to Preterism

Tn that the charges against preterism are sufficiently answered
in print by evangelical authors,® our consideration of a few of
their objections will be quite brief and merely illustrative of the
precariousness of their argument.

57, Hunt, Whatever Happened?, p. 65.

58. “Revelation” means “uncovering, opening up”- not “obscuring, conceal-
ing.” See John’s intention in Revelation 1:3; 22:7.

59, For more references see Gentry, Before Jerusalem Fell, pp. 133-45,

60. See J, Marcellus Kik, An Eschatology of Victory (nup.: Presbyterian and Re-
formed, 1971), passim; Cornelis Vanderward, Search the Scriptures, (Ontario,
Canada: Paideia Press, 1979), vol. 10, pp. 82ff; Gary DeMar, The Debate over
Christian Reconstruction (Ft, Worth, TX: Dominion Press, 1986); and Gentry,
Before Jerusalem Fell, passim,
