The Presence of the Kingdom 187

covenant” (literally) to Gentiles in the Church (2:10-12). He em-
phasizes the removal of the distinction of the Jew and the Gentile
(2:12-19). He refers to the building up of the Church as being the
building of the temple (2:20-22).*° The New Testament phase of
the Church is declared to have been taught in the Old Testament,
though not with the same fullness and clarity (3:1-6). Christ’s
kingly enthronement is celebrated by the pouring out of gifts upon
Hig Church/kingdom (4:8-11) with the expectation of the historical
maturation of the Church (4:12-14). Paul mentions the kingdom
in such a way as indicative of its spiritual, rather than political,
nature (5: 5).

In 1 Corinthians 3:21-22 Christians are shown their noble status:
“For all things are yours; whether Paul, or Apollos, or Cephas, or
the world, or life, or death, or things present, or things to come;
all are yours.” Elsewhere the present kingly status of Christians is
evidenced (e. g., Rem. 5:17; Col. 3:3; 1 Tim, 2:11-12).

Objections

Although enough material has been generated to point to the
presence of the kingdom since the days of Christ, a brief sampling
of some of the verses put forth by House and Ice in opposition to
the Reformed or Reconstructionist view would be in order.

One passage they use is 2 Timothy 4:1: “I charge thee there-
fore before God, and the Lord Jesus Christ, who shall judge the
quick and the dead at his appearing and his kingdom.” They
write: “Paul puts in the future both the ‘appearing’ of our Lord.
and ‘His kingdom’ in his charge to Timothy to preach the word.
(2 Timothy 4:1).”3°

There are three live possibilities beyond the dispensational in-
terpretation (which has been shown to be inadequate on other

35, Cp. also 1 Peter 2:4-5; 1 Corinthians 3:16-17; 6:
Revelation 3:12,

36. House and Ice, Dominion Theology, p. 224, In their next paragraph they
point to 2 Timothy 4:18 ag a proof of a future kingdom. To uze that verae thus re-
quires that we give credence to their mere agsertion without argumentation that
it does not refer to heaven, so we will not deal with that reference.

; 2 Corinthians 6:16;

 
