140 House Divided

pose. It is involved in that it incorporates both personal and cos-
mic eschatology and a number of redemptive, spiritual, ethical,
and cultural matters flowing from them. Furthermore, it draws
upon revelational language and concepts from all eras of revela-
tion: the pre-Mosaic, Mosaic, Prophetic, and Christie-Apostolic
eras,

This leads to a practical necessity for limiting our inquiry due
to the designedly compressed nature of our study. We will focus
on just a few themes related to cosmic eschatology that have been
woefully misunderstood by House and Ice and neglected by many
contemporary evangelical. One particular theme — the Gospel
Victory Theme —is quite dominant in the entire prophetic Scrip-
tures; its omission in much modem eschatology is to be lamented.
Its replacement with a defeatist scheme for Christian activity has
paralyzed the Christian cultural enterprise, emptied the Christian
worldview of practical significance, and given the Christian a sin-
ful comfort in lethargy. It has left the earth (which “is the Lord’s,”
Psalm 24:1) to a conquered foe and the enemy of our Lord and
Savior. This paralysis is all the more lamentable in that it has
caused the forfeiture of great gains made by the tireless and costly
labors of our Christian forefathers, particularly from the Refor-
mation era through the earl y 1900s.

Furthermore, this Gospel Victory Theme of postmillennialism
— an eschatology thought moribund for much of the present cen-
tury — is receiving renewed attention and debate in our era, as
House and Ice’s work so clearly demonstrates. The topic is quite
relevant since the mid-1970s. Let us begin with a definition of the
system to which the writers of this book adhere.

Definition of Postmillennialism

Postmillennialism is that system of eschatology which under-
stands the Messianic kingdom to have been founded upon the
earth during the earthly ministry and through the redemptive
labors of the Lord Jesus Christ in fulfillment of Old Testament
prophetic expectation. The fundamental nature of that kingdom
is essentially redemptive and spiritual, rather than political and
corporeal. Because of the intrinsic power and design of Christ’s re-
