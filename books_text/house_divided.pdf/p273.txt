230 House Divided

Summary

1. Biblical prophecy expects that there is coming a time when
the majority of the world’s population will have been converted to
Christ by means of the Gospel.

2. Christ is presently ruling and reigning from heaven (1 Cor.
15:25 a).

3. He will not return in His Second Advent until “the end” of
history (1 Cor. 15:24), when He turns His rule over to the Father
(1 Cor, 15:28).

4, At Christ’s Second Advent, He will have already con-
quered His enemies (1 Cor. 15:24) — the last one, death, being
conquered at His Return, when we are resurrected (1 Cor. 15:26).

5. The falling away of the Jews allowed for mass conversions
among the Gentiles (Rem. 11:12).

&. Eventually the vast majority of Jews and Gentiles alike will
be converted, leading to the “reconciliation of the world” (Rem.
11:15).

7. God normally works in history in a gradualistic manner, as
evidenced in His gradual unfolding of His plan of redemption and
His revelation of Himself in Scripture.

8. The kingdom comes gradualistically, as well, growing and
ebbing ever stronger over the long run (Dan. 2: 35ff,; Eze.
17:22-24; 47:1-9; Matt, 13:31-33; Mark 4:26-29).

9. The imminence doctrine of Christ’s return, which is held
by dispensationalism, is meaningless in that it may mean either
very soon or thousands of years distant.

10. The imminence doctrine is also unbiblical in that Scrip-
ture anticipates a long time delay in Christ’s return (Matt. 24: 48;
25:5, 19; Luke 19:11-27).

11. Dispensationalism denigrates the gifts that God has given
the Church.

12. Christ’s gifts to the Church well equip it for its task of win-
ning the world to Christ through its members.

13. The Church has the very presence of Christ (Matt, 28: 20)
and the Holy Spirit (1 Cor. 3:16).
