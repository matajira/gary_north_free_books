The Reconstructionist Opiion a7

ter of His redemptive plan. However, regarding God’s Jaw, one
nowhere reads in Scripture that God’s moral stipulations share the
same historical variation or anything like it. The Bible never
speaks of the New Covenant instituting “better commandments”
than those of the Old Covenant. Far from it.

Instead, Paul declared that “the [Old Testament] law is holy,
and the commandment is holy, and righteous, and good” (Rem.
7:12). He took the validity of the law’s moral demands as a theo-
logical truth which should be obvious and presupposed by all,
stating without equivocation: We know that the law is good”
(1 Tim, 1:8). That should be axiomatic for Christian ethics accord-
ing to the Apostle. Contrary to those today who are prone to criti-
cize the Old Testament moral precepts, there must be no question
whatsoever about the moral propriety and validity of what they
revealed. It should be our starting point — the standard by which
we judge all other opinions — that the law’s moral provisions are
correct. “I esteem all Thy precepts concerning ail things to be right”
(Psalm 119:128).

Accordingly, James reminds us that we have no prerogative to
become “judges of the law,” but are rather called to be doers of the
Jaw (4:11). And when Paul posed the hypothetical question of
whether the law is sin, his immediate outburst was “May it never
be!” (Rem. 7:7). God’s holy and good law is never wrong in what
it demands, It is “perfect” (Deut. 32:4; Psalm 19:7; Jas. 1:25), just
like the Lawgiver Himself (Matt. 5:48). It is a transcript of His
moral character. It so perfectly reflects God’s own holiness (Rem.
7:12; 1 Peter 1:14-16) that the Apostle John categorically dismissed
anyone as a liar who claimed to “know God” and yet did not keep
His commandments (1 John 2:3-4). God’s law is a very personal
matter — so much so that Jesus said “If you dove Me, you will keep
My commandments” Wohn 14:15; cf. vv. 21, 28; 15:10, 14). It is
characteristic of the true believer to have the law written upon his
heart and delight inwardly in it (Jer. 31:33; Rem. 7:22; Psalm 1:1-2)
—just because he so intimately loves God, his Redeemer.

The Universality of the Law
Paul teaches elsewhere that a// men — even pagans who do not
love God and do not have the advantage of the written oracles of
