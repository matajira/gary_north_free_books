The Expectation of the Kingdom 151

dominion impulse operated at a remarkably rapid rate, contrary
to the primitive view of man held by modern anthropologists (Gen.
4:17-22)."Any primitiveness that may be found in earth cultures
is a record of the developmental consequence of sin and of estrange-
ment from God, not of original creational status. In the second place,
the Creation Mandate is specifically repeated in Scripture in both
testaments (Gen.9:1ff.; Psalm 8; Heb. 2:5-8).

As will be demonstrated, the Gospel Victory Theme of post-
millennialism comports well with God’s creational purpose. It
highlights the divine expectation of the true, created nature of
man qua man. It expects the world as a system (or kosmos)® to be
brought to submission to God’s rule under the active, sanctified.
agency of redeemed man, who has been renewed in the image of God
(Col. 3:10; Eph. 4:24). Posimillennial eschatology expects what God
originally intended. It sees His plan as maintained and moving to-
ward its original fruition.

The Post-Fall Expectation of Victory

The first genuinely eschatological statement in Scripture oc-
curs very early: in Genesis 3:15. In keeping with the progressively
unfolding nature of revelation, this eschatological datum lacks a
specificity of the order of later revelation. At this stage of revela-
tion the identity of the coming Redeemer is not sharply exhibited;
it will take later revelation to fill out the picture, a picture not per-
fectly full until Christ actually comes at his First Advent. Yet the
broad outlines drawn by this original eschatological statement are
clear enough, particularly in light of the fuller New Testament
revelation.

Orthodox Christians recognize the Genesis 3:15 reference as
pointing to the coming and the redemptive labor of Christ as the
Promised Redeemer. He is promised as One coming to crush His

5, That apes, lemurs, and monkeys are called “primates” (from the Latin
primus, “first is indicative of the evolutionary view of man,

6. Kosmos (*world” is the Greek word (used in the New Testament) which is
expressive af the orderly system af the worlds; it is contrary to chao, See pp. 201-7.
