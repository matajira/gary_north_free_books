94 House Divided

is a different kind of thing from the divinely authorized regulations
and precepts (see the distinctions drawn above). House and Ice
mean more than this, however. (2) They also mean that the regula-
tions and precepts themselves which were revealed through Moses are
not coextensive with those revealed through Adam, Noah, and Christ.
The moral demands are to some extent different from each other,
and the regulations and precepts revealed through Moses are not
universally obligatory. But then the Mosaic law is “different” from
God’s law in yet another way. (3) The regulations and precepts re-
vealed through Moses were not based upon God’s unchanging
moral Principles. That is just to say that they did xo reflect the
essential moral character of God (even at the level of precept).

This inference is confirmed by the assertion of House and Ice
that the Mosaic law is not equivalent to the law delivered in spe-
cial revelation. The difference between the two laws for dispensa-
tionalists is much more than a difference in “specificity” (the point
mentioned by House and Ice)*°— a claim made without the slight-
est Biblical substantiation for such a contrast, by the way. Even
more, the difference is that the precepts of general revelation re-
flect God’s unchanging moral principles (and thus His essential
character), whereas the precepts of Moses do not.

We are now in a position to see why the dispensational per-
spective of House and Ice is theologically unacceptable. First, it is
now evident that, when the linguistic ambiguity and equivoca-
tions are cleared up, leaving us to see the conceptual scheme of
dispensationalism for its true character, House and Ice have roundly
contradicted themselves. They fully believe that the Mosaic law
was ordained by God, of course; it is from God (‘law* in the sense
of revelation). From the standpoint of conceptual analysis they
have taught that the precepts of the Mosaic revelation are not “law”
in the sense of reflecting the moral principles of God’s unchanging
character (just because they change and are not universally valid).
Yet on the other hand, they elsewhere state that the Mosaic law in its
entirety does reflect the character of God.

30, Ibid. , p. 129,
