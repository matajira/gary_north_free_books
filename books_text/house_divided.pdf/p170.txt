122 House Divided

forth out of Zion” (Isa, 2:3-4). Amos, Nahum, and Habakkuk all
declared Jehovah’s judgment upon Gentile nations for violating
moral standards found in the law of Moses — for example, per-
taining to matters as mundane and specific as slave trafficking
(Amos 1:6; Exodus 21:16; Deut. 24:7), witchcraft (Nahum 3:4;
Exodus 22:18; Lev. 19:21), and loan pledges (Hab. 2:6; Exodus
22:25-27; Deut. 24:6, 10-13). John the Baptist declared the moral
standard of the Mosaic law to Herod, saying “it is not lawful” for
him to have his brother’s wife (Mark 6:18). The moral standards
of the Mosaic law were not unique to Israel, even though the Mosaic
covenantal administration was.

Conclusion

The Bible is decisive on this point at Deuteronomy 4:6-8,
where Moses declared that “all these statutes .. . all this law”*?
which he had delivered to the nation would be the conspicuous
“wisdom” and “righteousness” of Israel in the sight of the sur-
rounding peoples. This passage pointedly contradicts the state-
ment by House and Ice that “the nations surrounding Israel were
never called upon to adopt the law of Moses.”*! To rescue them-
selves they can only try to tone down the implications of the pas-
sage by saying it merely says the nations would be “attracted” to
Israel and deem it wise.“ Such is the contrivance to which dispen-
sationalism is driven. Scripture says that the nations would also
perceive the “righteousness” of the Mosaic precepts (Deut. 4:8).
Yet House and Ice are trying to hold that the nations were not
thereby “called to adopt” those precepts! Such thinking founders
on the presupposition either that righteousness is not something
obligatory in the sight of God but optional (one choice among
many, a matter of preference), or that righteousness is variable
(being different things from culture to culture).

40. Notice how this emphasis contradicts the opinion expressed by House and
Ice that Israel’s status as a “model” to the nations did not require the nations to
follow “all or most of the commands given to Israel” (House and Ice, Dominion
Theology, p. 131).

41, Ihid., p. 128,

42, Ibid.
