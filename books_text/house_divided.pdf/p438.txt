NAME INDEX

Agrippa, 273

Albertine de Casale, 253

Alcasar of Antwerp, 277, 281

Alexander, J. A., 143

Allis, Oswald T., xix, xxiv, x0,
xxviii, xliv, xlviii, xlix, 143, 279,
281-82

Andreas of Cappadocia, 260, 279,
298

Antiobus, 273

Aquinas, Thomas, xi, 360

Arethas, 260, 279, 298

Asnaldus of Villanova, 253

Athanasius, 181, 184, 251-53, 255,
274-75, 283

Augustine, 238, 283, 297

Augustus, 264, 280, 372

Bandstra, A. J., 114

Barnabas, 167, 170-71, 174, 244

Barnes, Albert, 143

Beckwith, Isbon T,, 281

Berkhof, Louis, xv, 239, 244, 316

Bietenhard, Hans, 316

Blaising, Craig A., ix, xii

Blanchard, Jonathan, 14

Boettner, Lorraine, xlix, 143

Boyd, Alan Patrick, 167, 170, 221, 235,
237, 239, 240, 244, 299, 316, 374

Briggs, Charles A., 248

Brightman, Thomas, 248, 255

Brown, David, 143, 224

Brown, Harold 0, J., »xxviiisooix

Bullinger, E, W., 20cv

Bush, George, xix

Caius, 315

Caligula, 264

Calvin, John, xxviii, 248, 255, 303,
307, 319-20

Campbell, xlix

Cerinthus, 315-16

Chafer, Lewis Sperry, xodii, xxiv,
xxv, 80, 175, 235, 366

Chilton, David, 238, 257, 258, 259,
261, 264, 265, 268, 270, 271, 274,
276, 277, 280, 281, 292, 297, 304,
307-8, 314-16, 317, 319, 368-69

Clapp, Rodney, 31

Claudius, 264, 280

Clement of Alexandria, 259, 260,
278, 283

Clement of Rome, 170, 174, 244

Clouse, Robert G., 234, 242, 246,
249, 306

Cocceius, 254, 255

Colson, Charles, 345, 349, 351,
356-59

Cyprian, 167, 278, 283

Cyril of Jerusalem, 242

Dabney, Robert L., 143

Darby, J. N., 20d, 249, 280, 323

David, John J., 202, 246, 306

David, John Jefferson, 143

DeMar, Gary, ix, rorvi, 83, 196, 296,
345, 362, 369

Dield, William E., 353

Dio Cassius, 264

Diognetus, 244

Dionysius of Alexandria, 240, 241

443
