The Preterist Interpretation of the Kingdom 279

of fact, several of the early fathers held a distinctly preteristic inter-
pretation of Daniel 9! 1° By way of further example, Tertullian,
though a premillennialist, does as well: “Vespasion, in the first
year of his empire, subdues the Jews in war; and there are made
lii years, vi months. For he reigned xi years. And thus, in the day
of their storming, the Jews fulfilled the lxx hebdomads predicted in
Daniel .”!°? Though House and Ice adopt the Gap Theory of Dan-
iel’s weeks, which allows them to project the final week into the
distant future, the more standard evangelical interpretative op-
tions regarding Daniel's Seventieth Week can be found in Mere-
dith Kline, Edward J. Young, O. T. Allis, and others. *°*
Andreas of Cappadocia (6th century) wrote: “There are not
wanting those who apply this passage to the siege and destruction
of Jerusalem by Titus.” 1° Later he wrote: “These things are referred

by some to those sufferings which were inflicted by the Remans
upon the Jews.”!° Also Arethas specifically interprets various

passages in Revelation in terms of the destruction of Jerusalem. !°6

101. For a discussion of early interpretive approaches to Daniel 9, see Louis
E, Knowles, “The Interpretation of the Seventy Weeks of Daniel in the Early
Fathers,” Westminster Theological Journal (7 :2), pp. 137-38. Actual preteristic refer-
ences include: The Epistle of Barnabas 16:6; Clement of Alexandria, Miscellanies
1:21; Tertullian, Againat the Jaws 8 (despite being a Montanist premillennialist!);
Origen, Matthew 24:15; Julius Africanus, Chronography (relevant portions pre-
served in Eusebius, Preparation for the Gospel 10:10 and. Demonstrations of the Gospel
8); Eusebius, Demonstrations 8; Athanasius, Incarnation 40:1 (cited above), and
Augustine in his 199th epistle.

102, Tertullian, An Answer to the Jews 8. His entire chapter is given over to
demonstrating the fulfillment of Daniel 9, in order to vindicate Christianity
against Judaism,

103. Meredith G. Kline, “The Covenant of the Seventieth Week” in John H.
Skilton, ed. The Law and the prophets: Old Testament Studies in Honor of Oswald T.
Aitis (Nutley, Nj: Presbyterian and Reformed, 1974), pp. 452ff,; E. J. Young,
‘The Prophecy of Daniel (Grand Rapids, MI: Wm. B, Eerdmans, 1949), pp. 191-221;
0, T, Allis, Prophecy and the Church (Philadelphia; Presbyterian and Reformed,
1945), pp. 111f.; R, Bradley Jones, The Great Tribulation (Grand Rapids, MI:
Baker Book House, 1980), pp. 43-61; Philip Mauro, The Seventy Weeks (Swengel,
PA: Reiner, 1923),

104, Andreas on Revelation 6:12.

105, Andreas on Revelation 7:1,

106. He so interprets Revelation 6 and 7,
