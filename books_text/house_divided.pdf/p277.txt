History of Theology on the Kingdom 235

of the Millennium: Four Views, instead of following House and Ice’s
practice of speaking of “three views.”

Second, some dispensationalists admit the futility of equating
dispensationalism and ancient premillennialism. There is an ex-
cellent Master’s Thesis on this very topic, which was presented to
the faculty of Dallas Theological Seminary eleven years before the
publication of Dominion Theology. 10 In it Alan Patrick Boyd con-
fesses that “he originally undertook the thesis to bolster the
[dispensational] system by patristic research, but the evidence of
the original sources simply disallowed this... . [T]his writer be-
lieves that the Church rapidly fell from New Testament truth, and
this is very evident in the realm of eschatology. Only in modern
times has New Testament eschatological truth been recovered.” '!
He goes onto admit that ‘it would seem wise for the modern [i.e.,
dispensational] system to abandon the claim that it is the histori-
cal faith of the Church .’>lz He points to the error of such dispensa-
tionalist worthies as George N. H. Peters, Lewis Sperry Chafer,
Charles C. Ryrie, J. Dwight Pentecost, and John F, Walvoord in
assuming a basic similarity between ancient premillennialism and
modern dispensationalism. '* Boyd’s conclusion is significant. Of
Ryrie’s bold statement that “Premillennialism is the historic faith
of the Church,” he states: “It is the conclusion of this thesis that
Dr. Ryrie’s statement is historically invalid within the chronologi-
cal framework of this thesis.” “

10. Those interested may borrow this thesis through the inter-library loan pro-
gram of their local library. Alan Patrick Boyd, “A Dispensational Premillennial
Analysis.”

11, Boyd, “Dispensational Premillennial Analysis,” p. 91n.

2, Ibid., p. 92,

13, Ibid., p. 2, note 1: George N. H. Peters, The Theocratic Kingdom of Our Lord
Jesus, the Christ, 3 vcls. (New York: Funk and Wagnalls, 1884), vol. 1, pp. 494-97;
LS, Chafer, Systematic Theology, 5 vols. (Dallas, TX: Dallas Seminary Press,
1947), vol. 4, pp. 271-74; Charles Ryrie, The asis of the Premillennial Faith (Nep-
tune, NJ: Loizeaux Bros., 1953), pp. 20-23; Pentecost, Things To Com, pp,
375-76; John F. Walvoord, The Millennial Kingdom (Grand Rapids, MI: Zonder-
van, 1959), p. 43,

14, ibid, p. 89.
