9
THE CONFLICT OF EXPECTATIONS

Accurately defining postmillennialism in terms of the Gospel Victory
theme,

The following chapters will be given over to a consideration of
the various criticisms that House and Ice have brought against
Reconstructionist eschatology — particularly its dominant post-
millennialism. Unfortunately, our inquiry necessarily will have to
be merely summary, due to space limitations. This is quite disap-
pointing for at least two reasons: (1) With their shotgun attack
upon Reconstructionism, a great number of issues were sprayed.
across their pages, many of them in a superficial manner. '
(2) Contrary to the simplistic impression left by their book, the
eschatological locus of systematic theology is exceedingly broad,
deep and involved. Certainly no “one passage”? should lead to any
eschatological system.

As will be illustrated in survey fashion, the material of escha-
tology begins at the genesis of universal history and extends to the
consummation. * Thus its breadth encompasses the whole of time
and the entirety of the biblical record. It is a deep inquiry in that it
involves God’s unsearchable, infinite, and eternal will and phr-

1 See Part III below where a number of their scholarly lapses are pointed out.

2, H, Wayne House and Thomas Ice, Dominion Theology: Blessing or Curse?
(Portland, OR: Multnomah, 1988), p. 9. See quotation on pp. 141-42 below,

3, House and Ice recognize this: “David Chilton once offered me the following
exegetical support for postmillennialism: That's why my book started in Genesis.
J wanted to demonstrate that the Paradise Restored theme (..e., postmillennial-
ism) is not dependent on any one passage, but is taught throughout Scripture.
. + + The fact is, postmillennialism is on every page of the Bible” (ibid., p. 9). “To
understand Reconstructionist views of the end, we must go back to the begin-
ning” @id., p. 47).

139
