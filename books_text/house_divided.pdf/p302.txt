The Preterist Interpretation of the Kingdom 261

Frenaeus and the Date of Revelation

Second, as we continue through their ar,gument, it becomes
obvious that although they are confident in their employment of
drenacus against early date advocacy, they do not appear to be as
prepared to deal with his evidence as is requisite for their task.
Note that after citing Irenaeus’s passage from Against Heresies, they
employ a debater’s technique by attempting to promote their
point as “clear.” They write: “How does Chilton deal with such a
clear statement 7“ Unfortunately, Irenaeus’s modern translators
have commented on the difficulty of translating and interpreting
him. * In light of such a problem, how could Irenaeus’s debated
statement be deemed “clear” evidence?

Then after citing a particular English translation of Irenaeus
(who wrote in Greek), House and Ice comment: “Chilton ques-
tions whether [Irenaeus’s] ‘that was seen’ refers to ‘the apocalyptic
vision’ or to John himself. Since the impersonal pronoun ‘that’ is
used we can assume that it refers to John’s ‘apocalyptic vision.’ ”?6
This is a serious blunder. The original Greek of Against Heresies
has no “impersonal pronoun ‘that”!?” The “that” which forms the
basis of their argument is an English translator’s interpolation!

Irenaeus’s fa-mous statement reads (with options listed): We
will not, however, incur the risk of pronouncing positively as to
the name of Antichrist; for if it were necessary that his name
should be distinctly revealed in this present time, it would have
been announced by him who beheld the Revelation. For ‘he’ [John?]
or ‘it’ [Revelation?] was seen .. . towards the end of Domitian’s
reign.”2° Actually it is a matter of debate as to what Irenaeus in-
tended by his famous statement: Did he mean to say that John,

24, Ibid, p. 251.

25, See Gentry, Before Jerusalem Fell, pp. 47-57, The first English translation of
Trenaeus's work even notes: “Irenaeus, even in the original Greek, is often « very
obscure writer, . . [U]pon the whole, his style is very involved and prolix”
(A, Cleveland Coxe, The Apostolic Fathers in Alexander Roberts and James
Donaldson, eds., Anie-Nicene Fathers (Grand Rapids, MI: Wm, B, Eerdmans,
1985) vol. 1, p. 312,

26, House and Ice, Dominion Theology, p. 251.

27, See Gentry, Before jerusalem Fell, pp. +68. for the Greek text and comments
on it

28, Irenaeus, Against Heresies 5:30:3.
