18 House Divided

rule of life . And why do they believe this radical premise? “The
fact that God gave the Law to the people of Israel and not to the
Church is the beginning point for dispensationalism’s difference with
theonomy. All other points of disagreement stem from this one .* °
Accordingly, when it comes to ethics, dispensationalists are
prevented by the nature of their theory from being “whole-Bible
Christians.” That portion of the Bible which they find “profitable
for instruction in righteousness” is the New Testament only, ex-
cluding the Old Testament law. This mindset puts dispensational-
ism at odds with the Apostle Paul who spoke of “all scripture” —
referring specifically to what we call the Old Testament — as
profitable for instruction in righteousness and morally authoritative
(2 Tim, 3:16-17). Antagonism to the Old Testament law (“anti-
nomianism”) also pits dispensationalism against the perspective of
our Lord, who said that anyone who teaches the breaking of even
the least commandment from the Law and the Prophets (i. e., the
Old Testament) will be assigned the position of least in the kingdom
of God (Matt. 5:17-19). Where Scripture stresses moral continuity
with the Old Testament, dispensationalism stresses discontinuity.

Legalistic Antinomianism

What we have seen is that the dispensational answer to the
question of how we should then live leans heavily toward (1) cul-
tural relativism and toward (2) antinomianism. That is, dispensa-
tionalism is against the continuing authority of the Old Testament
law, restricting it to the culture of Old Testament Israel. On the
other hand, dispensational ethics also gravitates toward a form of
(3) legalism— that variety of legalism which replaces God’s com-
mands with social traditions, human opinions, and subjective
feelings (or manipulation). For instance, Charles Ryrie repudi-
ates the requirement of tithing because it belongs to the Old Tes-
tament, and then replaces that standard with “giving in obedience
to the still, small voice of the Spirit of God” which “on the basis of

15. Robert P. Lightner, “A Dispensational Response to Theonomy,” Bibliotheca
Sacra 143 Quly, 1986), pp. 235-36,
