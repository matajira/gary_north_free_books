Statism 187

Minimum Wage Laws

Tn one of his most amusing statements, Sider defends minimum.
wage laws against those who stand for the biblical mandate of per-
sonal charity:

Personal charity is too arbitrary and haphazard, It depends on the whim
and feelings of the well off. Many needy people fail to meet those who
can help. Proper institutional change (e.g. a minimum wage) on the
other hand automatically benefits everyone."

Again—to. be repetitious—such statist intervention is com-
pletely unbiblical. God’s law commands personal charity. If we do
not obey God’s word with regard to this command, God promises
to destroy us. Biblical law is the standard of righteousness. But
now a new Prophet has arrived, with a new word. He tells us that
God’s law can’t get the job done. God’s law is not only morally in-
ferior to statism, but it is also “too arbitrary and haphazard” in
practice,

More than this, the specific example of a proper means of charity
is the minimum wage law—‘which automatically benefits
everyone.” Sider is guilty of either ignorance or duplicity, but in
any case, he is not telling us anything about justice or compas-
sion, The minimum wage law is unjust, and serves as a powerful
tool of statist oppression.

‘When the state does not intervene into the market, all those
who wish to work will find jobs. Labor is always scarce. At some
price, producers will hire labor, Unemployment is a limited and
temporary phenomenon, But when the state mandates a
minimum wage, that suddenly changes. Each employer must now
pay his least productive workers this wage, plus the legal fringe
benefits which amount to 25-35%, At the present minimum
(1985) of $3.35, this means that the marginal worker costs his
employer well over four dollars per hour. Thus, no one whose
productivity is lower will be employed. The minimum wage law
creates institutional unemployment, where people who are willing and

11, Sider, “Ambulance Drivers or Tunnel Builders,” pp. 4f.
