276 Productive Christians in an Age of Guilt-Manipulators

ridden), the standard, “liberal” welfare system will have to be
scrapped, If Sider is to be taken seriously, he will have to deal with
Murray. Dr, Sider, I challenge you—I dare you—to respond to
Losing Ground.

Moreover, J submit two things: First, I submit that neither Dr.
Sider nor anyone in his camp (including his “economist” friends)
possesses the sheer competence for the challenge I have posed.
Second, I submit that Dr. Sider is a chicken: He won't even try.

Look Who’s Defending the Privileged Elite

In the new edition of Rich Christians, Dr. Sider has added a sec-
tion on the evils of colonialism. The notion that colonialism is the
major cause of world poverty was popularized by V. I. Lenin, and
his disciples have faithfully repeated it ever since. We might think,
therefore, that Sider has nothing new to say. On the contrary,
however, the professor has provided information in this section
which is not only new; it is startling; it is downright flabbergast-
ing. It is information which we are not likely ever to read any-
where else, To set us up for what is perhaps his most astonishing
passage to date, Sider says this:

One quarter of the world’s people wallow in the mire of deep poverty,
Forty thousand children die each day of malnutrition and related dis-
eases. One billion people have annual incomes of less than $50 a
year...

How did we get into this situation? (p. 124)

Let’s just hold it right there. What on earth can the professor
be babbling about? “How did we get into this situation?” Is Dr.
Sider seriously suggesting that all these millions and millions of
Third World people used to be rich, and that somehow they “got
into” poverty? Does he mean that all those people had annual in-
comes of far more than $50 until somebody took it away from
them? As a matter of fact, that is exactly what he is saying. And
guess who “got” these formerly affluent countries “into” poverty?
The Western nations, of course. (Wait a minute. Since the Third
World was so filthy rich, maybe Ged took it away from them! Isn’t
