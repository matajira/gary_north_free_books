300 Productive Christians in an Age of Guilt-Manipulators

ernments, If this is true, then we should stop calling it aid, and call
it by its true name: extortion payments. Let us, however, ignore that
point for now. Let us also ignore the fact that. the Soviet govern-
ment itself has been able to survive only through the generosity of
American taxpayers. Let us just concentrate on the logical errors
of the proposition. The government-to-government transfer of tax
receipts is not conducive to the development of a market-oriented
society; indeed, it is a denial of it. Moreover, it positively en-
courages the growth of statism and the politicization of life in reci-
Pient countries. Foreign aid simply turns the recipients into “Uittle
Soviets.” It should not be a matter of much surprise that so many of
our clients are hostile toward us. The extortion payments are not
working.

Over the past twenty years some have recognized the inherent
problems of government-to-government foreign aid, and have ad-
vocated a so-called “multinational aid”!9 instead, meaning
government-to-international-development-agency-to- government
foreign aid. This, its proponents hope, will depoliticize aid; Sider
argues that “the political misuse of food aid could be largely avoided
if food assistance went through multilateral channels,”!8° But, as
Bauer points out, the aid still comes out of taxpayers’ pockets, and
thus cannot help being political to some degree. It is still the coer-
cive transfer of funds from one group to another at the command.
of government officers.

More than this, however, multinational aid simply “substi-
tutes political contro] by the international organizations for politi-
cal control by the donor government over national aid.” It still
politicizes life among the recipients, for the money is still, ines-
capably, given to the governments. Multinational aid is nonpolitical
only in the sense that the recipient governments are even less ac-
countable to the donors than they were previously: “Both in public
and private spending the more distant the relationship between

129. Also called “multilateral aid.” See Sider, Rich Christians, second edition,
pp. 43-46.

130. Ibid., p. 212.

131, Bauer, Reality and Rhetoric, p. 64.
