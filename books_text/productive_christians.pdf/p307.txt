Déa Vu—A Romp Through Ronald Sider’s Second Edition 293

cali “the market” is simply the most efficient and reliable means for
those people to express their choices. No one person or committee
can possibly acquire the necessary data to control an economy. No
one can possibly know what “goods” are, or how “valuable” they
are, or how “scarce” they are; no one can know the constantly
changing scales of value (and “need”) of millions of people; and no
one can know, in advance, what some energetic young entrepre-
neur is about to introduce —tomorrow’s version of the ballpoint
pen or the microchip which will immediately throw today’s calcu-
lations into the wastebasket. The only way this information can
become known is through the undictated prices of the market
order. Free competition is, in its essence, a discovery procedure. ‘10
When people are free to express choices, the result is a market
order which no one has (or could have) organized— what FA.
Hayek, following Adam Ferguson, calls “the results of human ac-
tion but not of human design.”#11

One of the most crucial aspects of the market order~- which a
socialist “economy” can never provide~-is its function as @ érans-
mutter of information. When the market is functioning freely, without
interference from gangsters or bureaucrats, its prices accurately
reflect the real wants of the people. This gives entrepreneurs and
producers the information they need to allocate resources in the
most efficient manner, in order to serve people’s needs most
effectively.

On the other hand, when the market is hampered, the infor-
mation is retarded and falsified, And if the market is abolished
altogether, as in socialism, there is no information at ail. The only
way socialist economies can function is by keeping track of the
pricing information made available by the free market order. The
bureaucrats then use this information to set their own prices.
Without the market order, socialism could not work at all. As
Ludwig von Mises brilliantly demonstrated, a major curse of the

20. See Friedrich A. Hayek, Naw Studies in Philosophy, Politics, Economics, and
the History of Ideas (Chicago: University of Chicago Press, 1978), pp. 179-90,
232-46,

111. Friedrich A. Hayek, Studies in Philosophy, Politics, and Economics, pp. 96ff.
