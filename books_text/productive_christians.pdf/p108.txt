94 Productive Christians in an Age of Guilt-Manipulators

their cattle, their land, and their produce to him. Eventually, they
were forced to surrender their own children into bondage to the
state’s war machine, The principle here is that your Savior will be
your Lord as well, and that when you are saved you are also enslaved.
Ungodly cultures invariably become enslaved to the state. The eco-
nomic problem is that a socialist society has no means of economic
calculation, as Ludwig von Mises constantly pointed out: “Where
there is no market there is no price system, and where there is no
price system there can be no economic calculation.”

Thus, without the market mechanism of profit and loss, the
socialist Planner has no way to tell where energy and capital
should be directed. Surpluses and shortages become the norm,
and unanticipated (and thus unplanned-for) events—unusual
weather, for instance—produce catastrophes as a matter of
course. Famine is a commonplace of socialist states. The “con-
trolled” economy is in fact controlled not by the planners, but by
vicissitude, It is at the mercy of its environment —which is to say
God, our ultimate Environment, at whose hands a self-deified
state may expect little mercy.

In a truly Christian culture, the market is free from state con-
trol, and the result is that scarcity does not produce shortages.
The free market adjusts immediately to continually changing con-
ditions, and a shortage does not occur. Shortages have one real
cause: price controls.®° Moreover, God physically blesses the nation
that obeys Him, and natural disasters are considerably lessened—
making it even more certain that goods and services will be
available in abundance.

An important principle is at work in history. It is this: God is
continually at work to destroy unbelieving cultures and to give the world over
to the dominion of His people. (That, by the way, is what is meant by
those verses about God uprooting the rich; see Leviticus 20:22;
Deuteronomy 28; Proverbs 2:21-22; 10:30). God works to over-
throw the ungodly, and increasingly the world will come under

19, Ludwig von Mises, Socialism: An Economic and Sociological Analysis (London:
Jonathan Cape, 1951), p. 131; see pp. 113ff. Reprinted by Liberty Press, 1981.

20. George Reisman, The Government Against the Economy (Ottowa, IL:
Caroline House Publishers, Inc., 1979), pp. 63ff.
