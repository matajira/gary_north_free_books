Introduction b

was certainly no paragon of biblical standards. Many of his ac-
tions were in flagrant contradiction to the actual laws of the
Bible,?? And when God’s holy word informs us that “a good man
Jeaves an inheritance to his children’s children” (Proverbs 13:22),
we are on dangerous ground if we depart from it on the mere basis
of John Wesley’s questionable authority.

Sider goes on to commend. the communal way of life of such
groups as Reba Place Fellowship, which practices “total economic
sharing’? among its members. Just how biblical this practice is
(as a normal lifestyle) will be indicated in Chapter 12. Scripture
stands in terms of community, not communes. Complete economic
equality is never stated as a Christian ideal. It may be a tem-
porary necessity in an emergency; but it is not a “model” for the
usual Christian lifestyle. God has called us to dominion, the
developing of the earth by:men with different gifts and abilities,
increasing the earth’s productivity for the glory of God. The com-
munal ethic is not oriented toward dominion, but toward bare-
minimum survival, Giving money away does not produce any-
thing for the future: its whole function is to provide for émmediate
needs, for present consumption alone. This does not mean we
shouldn’t give our money away—we should. There are valid
needs in the.present. But God’s law is structured so that, usually,
a good portion of income can go toward production. This is the
only way to bring lasting, long-term benefit to all.

That thesis will be developed in the chapters that follow. We
must be sure that in all our thinking and acting, we are operating
according to the clear mandates of Scripture. The Bible is law for
all of life. To depart from it is foolishness —a word we should not
use idly. Biblically, it describes the condition of the man who has
departed from God’s word, who constructs great programs on the
powdery basis of autonomous reason, who self-consciously lives a

27. For details of Wesley’s treacherous appropriation of real estate belonging
to George Whitefield, see Arnold A, Dallimore, George Whitgield: The Life and
Times of the Great Evangelist of the Eighteenth-Century Revival (Westchester, IL: Cor-
nerstone Books, 1980, 2 vols.), 2:36-40, 68-78.

28. Sider, Rich Christians; p. 200 {cf. p. 189].
