74 Productive Christians in an Age of Guilt-Manipulators

Men have always had to choose between two methods of social
change: regeneration and revolution. The Christian secks first to
discipline himself to God’s standard. He then publishes the gospel
and attempts to peacefully implement the laws of God into the life
of his culture, trusting in the Spirit of God for the success of his
efforts. He knows that there is not, and never will be, a perfect so-
ciety in this life. He knows that the Kingdom of God spreads like
leaven in bread—not by massive, disrupting explosions, but by
gradual permeation. He knows that justice, righteousness and
peace result from the outpouring of the Spirit in the hearts of men
(Isaiah 32:15-i8); a nation’s legal structure is, therefore, an in-
dicator, not a cause, of national character. Law does not save.

But the revolutionary believes that a perfect society.is possible,®
and that it must be coercively imposed on men. He seeks to over-
throw everything which threatens to obstruct the coming of his
made-to-order millennium. God’s providence is too slow, His law
too confining. Society must be perfect —tomorrow—or be blasted
to rubble, As the slogan of the French Revolution put it: “Lib-
erty, Equality, Fraternity—or Death.”? The abolitionists, rising
out of the early nineteenth-century religious turmoil, yearned for
such a perfect society, and were willing to slaughter innocent peo-
ple in order to achieve it. The atmosphere in which abolitionism
thrived was produced by such men as the creedless Unitarian
crusader, William Ellery Channing, who called for “guerrilla
war... at every chance.”!° Channing was a major influence on
young Ralph Waldo Emerson, the chief exponent of New
England pantheism and transcendentalism—and. a considerable
warmonger as well, To many, his pacifistic nature-worship seems
harmless: the very mention of Emerson conjures up serene visions
of gurgling brooks, sparkling dew on new-fallen leaves, and
Henry David Thoreau behind bars. The soporific calm is shat-

8. Cf. in this regard Sider’s praise of Jonathan Blanchard’s perfectionism, in
“Words and Deeds,” pp. 155f.

9. Otto Scott, Robespierre: The Voice of Virtue (Vallecito, CA: Ross House
Books, 1974), p. 195.

10. Scott, The Secret Six, p. 15,
