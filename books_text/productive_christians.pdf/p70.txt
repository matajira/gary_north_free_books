56 Productive Christians in an Age of Guilt-Manipulators

call forth more of the very poverty that is supposedly being
alleviated.”9

Rothbard notes further: “Private charity to the poor, on the
other hand, would not have the same vicious-circle effect, since
the poor would not have a continuing compulsory claim on the
rich. This is particularly true where private charity is given only
to the ‘deserving’ poor.”!° And that is exactly the case with the
biblical Poor Laws. They are not enforced by the state, nor does
the state collect or dispense the tithes. The individual is expected
to obey God’s word, and he is responsible to administer the tithes
in a conscientious, faithful manner. Biblical law aids the poor, yet
makes it economically desirable for them to work their way out of
poverty. This fact is even more obvious in the following section.

Gleaning

The primary source of regular charity to the poor was the prac-
tice of gleaning, in which farmers were required to let the poor
gather the fruit that remained after the harvest (Leviticus
19:9-10; 23:22; Deuteronomy 24:19-21). The farmer was prohib-
ited from completely harvesting his crops: he had to leave the cor-
ners of his field untouched, and the fruit which was left on the
trees after they were beaten or shaken had to remain there. The
poor were then allowed to pick the fields clean. Related to this was
the law of the sabbatical year, when the land received its rest; no
real harvesting was allowed, but the poor were allowed to glean
whatever fruit was there (Exodus 23:10-11), A similar law, not
dealing with poverty as such, allowed anyone entering a
neighbor’s field to pick grapes or grain and eat his fill, as long as
he did not carry any food away from the premises (Deuteronomy
23:24-25; see Matthew 12:1).

Two points are of special importance here. First, gleaning was
not indiscriminate. Landowners apparently had the right to specify

9. Murray Rothbard, Man, Economy, and State (New York: New York Univer-
sity Press, [1962] 1975), p. 818.
10. Iid., p. 931.
