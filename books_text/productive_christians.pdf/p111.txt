The Third World 97

the lower castes (i.e., the less “Westernized” Indians) often know
neither their own birthdate nor their age, or the ages of their chil-
dren, Can any amount of aid render such a culture productive?
What can be done for a “timeless” society? As a prominent gov-
ernment leader put it: “'m going to be reincarnated thousands of
times. If I don’t get something done in this life, I have other op-
portunities.”2* Clearly, India’s most pressing need is not more
grain or financial grants. India needs Jesus Christ.

The fact that the poor nations are suffering under the judgment
of God does not mean we should disregard the real misery of these
people. But it does require that we approach them carefilly, with a
biblical, theologically informed mind. Our actions toward them
must be.concerned with éransforming their cultures by the Word of God.
They will not be economically blessed until they obey Him, and we
will be cursed if we seek to help them in ways that are forbidden.

Poor people need the Gospel. The truly liberating message of
the salvation provided in Christ must sink down into their inner-
most beings, changing their perspectives completely. They must
become disciplined, obedient ta God’s law. They must renounce
their state-worship and their envy of those who are better off. They
must seek to become free, responsible men under God, building for
the future, working and investing in every area of life for the glory
of God. And they must keep the state in its place, not allowing it to
take God’s place in controlling the economy. Under God’s blessing
they will then prosper, And they need have no fear of a “tiny,
wealthy elite” of dictators, for the ungodly will have fallen—not by
a revolution, but by the providential judgment of God.

He brought forth His people with joy,

His chosen ones with a joyful shout.

He gave them also the lands of the nations,

That they might take possession of the fruit of the people’s labor,
So that they might keep His statutes,

And observe His laws,

Praise the Lorp! (Psalm 105:43-45)

24, See Tyler Marshall, “Time—It's Timeless for Most Hindus, Los Angeles
Times, 24 January 1981,
