378 Productive Christians in an Age of Guilt-Manipulators

Blessings are bestowals of goods. God’s blessing of His people is
first of all the giving of salvation through His Son. This results in
long-term maierial blessing as the believing culture is obedient to
God’s law (Ephesians 1:3; Deuteronomy 28:1-14; Proverbs 10:22;
28:20; Malachi 3:10-12). God’s people are blessed with increasing
dominion over the earth, and the land yields abundant fruit under
the godly development of its resources.

 

Capital is the net wealth of goods and savings owned by a person
who participates in a market. Capital can be accumulated only by
saving and increasing the supply of capital goods. See Saving.

Capital goods are things such as tools, machines, and buildings
~already produced “factors of production”—which make labor
more productive by reducing the time and energy necessary to
reach the goals of production. (Sider subscribes to the fallacy of
regarding “labor-intensive’—i.e., less efficient~production as
morally superior; see Rich Christians, pp. 54, 230 [pp. 47, 237}. In
reality, increasing the productivity of labor means increasing per
capita wealth and employment opportunities. Money that is saved
by hiring fewer workers will be spent elsewhere, thus employing

the workers in another industry. See Henry Hazlitt’s Economics in
One Lesson, ch. 10.)

Capitalism is the Marxist term for Christian Society, It is the sys-
tem of private ownership and control of the means of production
as well as the fruits of production, and stands against policies of
govenmenital ownership or control. See Free enterprise.

Capitalists are people who either invest in an enterprise or de-
fend and advocate the system of capitalism.

Commodities are things, real goods which people value. For ex-
ample, paper money is not a commodity: you don’t value the
money in your wallet for the actual worth of the paper, but for what
you hope the paper represents (ability to purchase goods). But com-
modity money (gold or silver) is valued because of what it is as a
commodity. See Money and Value.
