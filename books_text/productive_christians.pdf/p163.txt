Cultural Bone Rot 149

The only cure for this malaise is “a sound heart”—an attitude
of contentment with God’s providential government of your life.
He is the one who raises up and puts down, and your advantages
~-or lack of them—aré from Him, Samuel Willard said this in
1706: “We are never the worse in our selves, because another en-
joys a Prosperous Condition; it is of God that his State is such,
and it in no way makes ours other than it is . . . To envy men the
Prosperity which God bestows upon them, is to Hate them with-
out a cause; or when they have offered us no real Affront or Prov-
ocation. It is an Affront offered to the Divine Sovereignty; it is
God who lifts Men up, and puts them down; He is the Supream
disposer of all the Affairs of the Children of Men. It implies a fault
found with His Government of the World, as if He dealt Unjustly,
and did not distribute His Favours, either in Wisdom or Right-
eousness. It Envys God His Glory in the World, in that it is Angry
if He be Glorified by another; because he thinks that in so doing
he out-shines him, and darkens his Light. It despiseth and reflects
upon the Gifts and Favours of God, as if they were Lost, because
they are not Concentred in him.”

Ifyou have needs, the Bible commands you to pray (Philippians
4:6-7), to be content (Philippians 4:5, 8, 12), and to work (I Thessa-
lonians 4:11); and Ged, who hears the cry of the poor, will supply
all your needs (Philippians 4:19). We have a wealthy Father, and
under His care we can be at peace, regardless of our financial
standing. But this requires obedience to Him, seeking Him as the
Source of wealth (Deuteronomy 8:18), and finding our happiness
in obedience to His law.

God’s law does bring physical, material blessing to a culture.
For one. thing, the society’s ethic is not envy, but obedience to
God. This makes for both social stability and economic growth. The
Jand prospers when people are at least externally obedient to
biblical law; when they allow their neighbors to prosper; when
they allow even wicked men—as long as they remain externally
obedient—to develop the earth. God will catch up with the un-

39, Willard, pp. 750, 752.
