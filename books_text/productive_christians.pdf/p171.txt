The Jubilee Principle 157

meaning and significance —is ours forever.

In terms of this, the Jubilee required that the land could not be
permanently alienated from godly heirs. This was a symbol that
God would never leave or forsake His people—that, by His grace,
His people would remain in the land, instead of getting kicked out
as Adam and Eve were, and as were the previous heathen in-
habitants of the land, who were spewed out of the earth (Leviticus
18:24-29). God's people have an atonement in Jesus Christ. The
effects of the Curse, still visited upon those outside of Christ, are
being reversed through the grace of God in the gospel. Therefore
the announcement of the Jubilee was made on the Day of Atone-
ment (Leviticus 25:9), after seven sevens of years, a perfect
fulness (Leviticus 25:8), symbolizing Christ, who came in “the
fulness of the time” (Galatians 4:4). The Jubilee was clearly
ceremonial, pointing to our atonement, liberty and security in
Christ, whose coming marked “the favorable year of the Lord” as
he proclaimed “release to the captives” (Luke 4:18-19; cf. Isaiah
61:1-2). But Israel rejected liberty in Christ, choosing instead to
be enslaved to Satan and the Roman antichrist..In spurning
Christ, they thereby forfeited their right to the land; so the curses
of the law were reinstated, the Jews were driven out, and their
Jand was confiscated. Christ, like the land which spoke of Him,
spews the ungodly out of His mouth (Revelation 3:16).

However, we must examine further the specifics of the Jubilee
laws, in order to see just how much the actual principles conflict
with Sider’s version. In some ways, the Jubilee actually furthered
inequality. For one thing, it did not equalize incomes. The
“unlucky” Israelite who had to sell his land between Jubilee years
to a more successful entrepreneur did not share in what might be
high profits coming from the wise use of the property. The best
man still won. The land was used by those who were best able to
manage it, as demonstrated by their other successes that enabled
them to purchase it in the first place. And when the original
owner received it back, it would not exactly be in prime, income-
producing condition, for it would have lain fallow, with no labor
expended on it, for at least a year. This is obviously vastly
