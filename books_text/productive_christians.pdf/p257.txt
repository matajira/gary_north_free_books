The Conquest of Poverty 243

knew the importance of biblical laws, and that “righteousness ex-
alts-a nation” (Proverbs 14:34). Their adherence to God’s word
was blessed by God, and our land became one of increasing
wealth. But we fell into the snare warned of in Deuteronomy 8.
We looked at our peace and prosperity and convinced ourselves
that our strength had come from ourselves. We began to seek
growth for ourselves, and not for the glory of God. We rejoiced in
the gifts, and ignored the Giver. We used His tools to build idols.
While we boasted of freedom, we became enslaved.

When God’s goodness does not lead to repentance,’ He
chastises. He sent judgments to-our nation, to turn.us from our
sins and as-we felt our power eroding, we turned more and more
toward sin as a means of strength. We allowed our rulers to-lead
us into wars, in order to achieve a supremacy that is denied to all
but the obedient. We increasingly deified the state, ascribing to it
creative powers, abandoning biblical standards in one area after
another. We coveted goods, and got credit expansion; we wanted
business booms, and the state provided them. Our demands in-
creased, and the dominance of our new god was enlarged to keep
up with them. And as our nation became enslaved, the Christians
ran—some to the security of fundamentalist retreat, some to the
comfort of liberal compromise, some to the heretical moderation
of hovering somewhere in between. Every avenue was tried but
the way of obedience.

And everything backfired. Our wars reduced our population;
our foreign aid produced contempt and envy; our foreign policies
generated revolutions abroad and riots at home; our welfare re-
sulted in poverty and dependence; our economic booms termin-
ated in racking depressions; our energy policies caused shortages;
our evangelistic campaigns strewed a generation of “carnal Chris-
tians” across the land. And inflation accompanied it all. The
Curse became a part of everyday life.

So we sought for new solutions, in a fruitless attempt to avoid
the consequences of apostasy without repenting of sin. And our
new solutions have bound us in chains stronger than those we had
before. From national pride ‘we have sunk into national guilt. We
