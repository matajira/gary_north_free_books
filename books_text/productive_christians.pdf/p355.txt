Socialism, the Anabaptist Herésy 341

of socialism. Whether we look at the theories of Plato or those of
the Anabaptists; whether we consider the practices of the ancient
Babylonians or the modern Soviets; the same ideas, even in seem-
ingly unimportant details, are present (for example, one of the
many strange threads running throughout this book is the recur-
ring socialist condemnation of private rooms, of doors and
walls®), Socialism is not—contrary to Marxian dogma-—a “later
phase” in human history. The basic, heretical principles have been
championed for ages: the abolition of authority, of property, of the
family, and of Christianity. On this last point, Shafarevich says:
“The term ‘atheism’ ts inappropriate for the description of people in the grip
of socialist doctrines. It would be more correct to speak here not of ‘atheists’
but of God-haters,’ not of ‘atheism’ but of ‘theophabia’ ">

And this leads to what Shafarevich powerfully argues is the es-
sence of socialism: the yearning for death and destruction, The
closing sections of his book are filled with some of the most star-
tling documentations of socialist hatred for mankind I have ever
seen.*6 Again and again, socialists have made it clear that they de-
sire nothing less than the death of mankind. This, Shafarevich
says, is the basic allure of socialism; this is the secret of its seductive
power and its driving force; it is nothing less than a deeply emo-
tonal, ecstatic urge toward self-destruction. This, 1 submit, is exactly
why neither Ronald Sider nor any of his cohorts, either within or
without the Christian Church, have been able to provide a posi-
tive program for the creation of wealth, for the productive devel-
opment of the earth’s resources to bring about a rising standard of
living. This is why they can speak only of confiscation and de-
struction. They never intended otherwise. The. prospect of the utter an-
nihilation of oneself and of mankind is precisely the attraction of
socialism, and possesses a subliminal motivating power far sur-
passing any rational economic argument. For socialism is the final
religion of the Theophobians, the God-haters; and God has told

64. Ibid., pp. 1986.
65. Mid., p. 235. Italics added.
66. Ibid., pp. 258-300.
