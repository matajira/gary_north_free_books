God's Law and the Poor 37

which of the “deserving poor” could glean on their land, and
special favors would be granted by the owner (Ruth 2:4-16).
Gleaning was not simply a “right” which could be claimed by any
poor person against the field of any landowner. In no sense was
property held in common. God required landowners to allow the
poor to glean, but the owner nevertheless had the right to dispose
of his property as he saw fit, within the boundaries of the law. The
gleaning law cannot be used as a basis for social redistribution of
wealth,

Second, gleaning was hard work—much harder than normal
harvesting. Gleaners had to labor arduously in order to gather
sufficient food. Only a little would be left after the reapers were
finished: a small cluster of grapes here, a sheaf of grain there.
Israel was no Welfare State. Recipients of charity had to be dil-
igent workers. The lazy and improvident could expect no saving
intervention by a benevolent bureaucrat. God’s law commands us
to “bear one another’s burdens” (Galatians 6:2), but not in such a
way as to produce dependence on charity. The result of charitable
activity should be responsibility, so that eventually “each one
shall bear his own load” (Galatians 6:5),

Lending

A third remedy for poverty was that the poor man could take
out a loan. While there is no biblical evidence that it was forbid-
den to charge interest on a dusiness loan to a fellow believer (Mat-
thew 25:27), loans to a believer for charitable purposes had to be
interest-free (Exodus 22:25; Leviticus 25:35-37). Moreover, any
loan to a believer had to be wiped out in the seventh year.

At the end of every seven years you shall grant a remission of debts. And
this is the manner of remission: every creditor shall release what he has
loaned to his neighbor; he shall not exact it of his neighbor and his
brother, because the Lorn’s remission has been proclaimed (Deuteron-
omy 15:1-2).

Thus, no believer could be charged with debt for longer than
six years. Wealthy Israelites were strictly commanded not to with-
