Is God on the Side of the Poor? 83

getting around this text. It stands in the same passage with the
previous one about Christ’s ministry to the poor, to guard against
the ‘false impression (perpetrated by Sider ¢t al.) that He came to
relieve the sufferings of “the poor,” without distinction. God’s
mercy is neither promiscuous nor partial, in terms of econoinic
status; but He hears those who call upon Him in truth (Psalm
145:18), and He hates all those who do iniquity (Psalm 5:4-5),
regardless of the size of their paycheck. It is for this reason that
the Psalmist can exult:

The Lop also will-be a stronghold for the oppressed,

A stronghold in.times of trouble,

And those who know Thy name will put their trust in Thee;
For Thou; O Lorn, hast not forsaken those who seek Thee.
{Psalm 9:9-10)

God does not merely relieve the oppressed or troubled in
general. He graciously relieves the sufferings of those who seek
Him. The poor man who is treated unjustly and has no legal
recourse in’ an ungodly society need not despair. If he seeks the
Lord with his whole heart, he will. find Him. God will arise in
deliverance, breaking in pieces the oppressor, avenging injustice,
and satisfying the needs of His people (Psalm 12:5; 34:6; 68:10;
72:2-14; 113:7-8; 140:12; 146:7). Ifthe poor man commits him-
self to the Lord (Psalm 10:14), he will be delivered. God “will
deliver the needy when he cries for help” (Psalm 72:12). But not be-
fore. And if the needy opts for revolution instead, God will crush
him to powder.

By appealing to class-consciousness, by inciting resentment
against a state which does not dispense enough benefits, by en-
couraging covetousness, envy, and theft against the rich, Ronald
Sider has chosen the way of revolution. This is underscored by his
beliefin chaos as a key to history. Implying that the rich are, ipso
Jacto, oppressors, he turns revolution into an almost metaphysical
principle: “God regularly reverses the good fortunes of the rich.”®

5. Did., p. 73 [p. 65]. Italics added.
