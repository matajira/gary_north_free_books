Ts God on the Side of the Poor? a1

regard the state as their rightful savior; if the ruler does not step in
to bail them out, they will curse him as well. God will not hear the
prayers of those who thus defy. Him and His constituted author-
ity. Emphatically, He is not on their side.

Covetousness and theft are strong temptations to one who is in
want, If a starving man steals food, we can understand his
reasons; nevertheless, Scripture says he must make a seven-fold res-
titution (Proverbs 6:30-31).3 Until he does, he is still a thief— and
God is not on his side, either, regardless of “mitigating circum-
stances.” The disobedient have no claim on God’s mercy or pro-
tection. “He who turns away his ear from listening to the law,
even his prayer is an abomination” (Proverbs 28:9). “Better is the
poor who walks in his integrity, than he who is crooked though he
be rich” (Proverbs 28:6).

The message of Scripture is that God is a refuge to those who
call upon Him. But if the poor man curses the Lord, and breaks
His law, not trusting in Him, God has only condemnation. Socio-
economic status is no guarantee against His wrath:

Therefore the Lorp does not take pleasure in their young men,
Nor does He have pity on ihetr orphans or their widows;

For every one of them is godless and an evildoer,

And every mouth is speaking foolishness. (Isaiah 9:17)

Sider loves to quote from Luke 4:18-19, where Jesus declares:

The Spirit of the Lord is upon Me,

Because He anointed Me to preach the gospel-to the poor.
He has sent Me to proclaim release to the captives,

And recovery of sight to the blind,

To set free those who are downtrodden,

To proclaim the favorable year of the Lord.

Thus, Sider claims, Christ’s mission “was to free the oppressed
and heal the blind. . . . The poor are the only group specifically
singled out as recipients of Jesus’ gospel... . At the supreme
moment of history when God took on human fiesh, the God of
Israel was still liberating the poor and oppressed and summoning

3. See James B. Jordan, The Law of the Covenant: An Exposition of Exodus 21-23
(Tyler, TX: Insticute for Christian Economics, 1984), p. 135.
