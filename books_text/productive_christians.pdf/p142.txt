128 Productive Christians in an Age of Guilt-Manipulators

even international production. Take away the opportunity to
make and keep high profits, and you have turned the power of
making production decisions over to the state’s bureaucrats. The
entrepreneur is a middleman, not a tyrant.

Production decisions have to be made by somebody. Either the
“yntouchables” in the state bureaucracies decide for the con-
sumers, “in the name of the people,” or else profit-seeking, risk-
taking entrepreneurs will decide. An incompetent entrepreneur
will soon be out of business. He will stop wasting the community’s
scarce economic resources in the production of items that the
public prefers to skip. An incompetent, monopolistic, government-
protected bureaucratic planning committee is a lot harder to
remove. So take your pick: production by compulsion or produc-
tion for profit. There is no third choice.

Seeing profits in this light helps us to understand something
else about the high-profit complaint. If we say that someone’s
profits are too high, we mean one of two things. First, that the en-
trepreneur was too efficient. If he had done his job poorly, he would
have directed the factors of production less efficiently, and the
costs of production would have been higher, with the result that
his profit margin would have been slimmer. But no—that wicked
profiteer was able to allocate the scarce factors of production in
the most careful and prudent manner, and thus he was able to
reap high returns. The second thing we might mean by saying his
profits are too high is that he made too many people happy. The only
way to make high profits is by meeting a high demand. If someone
makes a very high profit, it means that many people wanted that
item, and were willing to pay high prices to get it. A more
moderate man would not have tried to satisfy so many people.
But that wicked profiteer is so despicable that he would please
everyone in the world if he could.

God has structured the world so that those who are best at satis-
fying the public have the most control of production. This is the
function of profits. The price system of the market transmits infor-
mation about consumer wants, and the profits go to those who are
most efficient in using capital to supply those wants. Complaints
