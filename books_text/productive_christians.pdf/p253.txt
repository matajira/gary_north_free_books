The Conquest of Poverty 239

gospel. The early Christians did not start a liberation movement
against the “structural injustices” of the Roman Empire, They
converted the Empire instead. Then they changed the structures.

For example, I do think it’s a tragedy that worthy people do not
own land. I think every Christian ought to own property—and,
someday, I believe every Christian wilf own land (as we shall see
further in this chapter). But socialistic “land reform” is not the
answer. Regeneration is the answer. As men become responsible,
they will inherit the earth—meekly, Meekness does not mean
spinelessness. It means obedience to God, and submission to His
providence.

Hilaire Belloc wrote of the “abolition” of slavery that took place
as Europe was Christianized: “In general you will discover no
pronouncement against slavery as an institution, nor any moral
definition attacking it, throughout all those early Christian cen-
turies during which it nonetheless effectively disappears.”3

Slavery disappeared because a majority of men stopped being slaves
to Satan. Christianity works like leaven: from the inside out.
Laws — biblical laws — are important to the security of society. But
if men are not ruled by law internally, the external controls will
break down, Our work to establish God’s laws in society may, and
should, accompany our evangelistic efforts. But apart from those
efforts, we are laboring in vain and striving after wind. Dominion
will come through proclaiming God’s word and obeying it our-
selves, The world will be transformed by the faithful preaching
and living of God’s people. On the basis of His sovereign author-
ity, Jesus commanded: “Go therefore and make disciples of all the
nations” {Matthew 28:19). That command is usually misunder-
stood. Jesus did not say: Witness to the nations. He said: Disciple
the nations. The great commission is not exhausted when we have
brought the gospel to the attention of all nations. That is not even
half the battle, It is only the beginning. We must disciple all na-
tions to the obedience of His commands, and that can come only

3. Hilaire Belloc, The Servite State (Indianapolis: Liberty Classics, [1913], 1977),
p. 72.
