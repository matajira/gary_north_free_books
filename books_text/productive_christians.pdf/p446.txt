432

Tyrrell, R. Emmet, Jr, 143n, 254n,
265n, 298, 302-03

“Underdevelopment,” 278-79

United Farm Workers, 46

United Nations, 154n, 259-60, 294,
354, 359

Utopianism, 291, 307, 336, 337

Value, 40, 186, 292-93, 390,
398

Van den Bruck, Moeller, 325n

Van Til, Cornelius, 312n, 358n.

Van Til, Henry R., 217

Velasco, Gustavo R., 217

Verduin, Leonard, 324n

Vietnam, 297

Violence, 286-97, 334

Voltaire, Francois Marie Arouet de,
338

Voslensky, Michael, 296n

Walton, Robert, 308
Warfield, Benjamin B., 75n, 240
Watergate, 141-42

Watson, Thomas, 95

Productive Christians in an Age of Guilt-Manipulators

‘Wealth
ereation of, 278-79, 280-82, 318, 341

Wealthy vs. free market, 280-282

Weber, James 5., 117, 118

Welfare, 49-51, 108-09, 221-23, 268-76,
318-19

‘Wesley, John, 15

Western civilization, 92-97, 277-78,
301-03, 352

‘Westminster Theological Seminary,
351-52

Wheatcroft, Geoffrey, xiiin

Whitefield, George, 15n, 226

Widows and orphans, 49-50, 180

Willard, Samuel, 139-40, 144,
149

William I, 207

Williams, Walter, 269, 275n

Wisdom, 21

Wittfogel, Karl A., 3381

Wolfe, Bertram D., 29in

Work, 35-38, 48, 50, 56-57, 58-59,
270-271, 273, 274, 307

Young, Edward J., 193
Youth, 271, 272
