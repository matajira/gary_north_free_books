388 Productive Christians in an Age of Guilt-Manipulators

til it ends up in complete socialism (like telling bigger lies to ex-
plain the previous ones—reality keeps catching up with you). See
Socialism.

Justice is defined by God’s law, Any deviation from His com-
mandments—even in the name of “love” or “compassion” —is not
justice, but injustice. Our view of right and wrong must always
come from the Bible. The Bible, rather than Plato, Marx, or
Keynes, defines right and wrong,

Jobn Maynard Keynes [Canes] was an advocate of price infla-
tion as a remedy for unemployment. He reasoned that if there is
more paper money and credit, it will be easier to employ more
people—even though the actual purchasing power of the money
will be lower. Laborers will accept lower real wages (fewer goods
and services), so employers will hire them. He figured that work-
ing people who didn’t have degrees in economics wouldn’t notice
the difference. Lord Keynes died in 1946. He has been receiving
his proper wages ever since. (Keynes was a homosexual who de-
lighted in low-wage Tunisian boys. It was his way to fight unem-
ployment in an underdeveloped nation.)

Labor unions are gangs of legalized thugs. They do not believe in
allowing employers and employees the freedom to make contracts.
They do not allow non-union people to sell their labor at lower
wages; thus they create institutional unemployment for they are
also strong advocates of minimum wage laws. By forcing employers
to raise wages above the free-market price, they raise production
costs, which means two more things: lower production and higher con-
sumer prices. There is nothing wrong with workers bargaining with
employers about wages. What is wrong is when unions are able to
get the government behind them. This means that bargaining is
done at gunpoint. The fact that government backs them also means
that unions can usually threaten, terrorize and even kill those who
oppose them, without facing punishment from the state. Their op-
ponents are not employers but rival workers who are willing to work
for wages lower than union members want for themselves.
