The Missing Blueprints —A Parable 345

Dr. Side sighed heavily. Some of his followers (called the Other
Siders) moved menacingly in the old man’s direction with clubs,
but Dr. Side stopped them. “Now is not the time for violence,” he
whispered, “Now is the time for the Gentle Nudge.” And so, as the
Other Siders gently nudged the old man to the edge of the crowd,
Dr. Side graciously answered his objection. “Yes, it’s true. The
men who wrote the Supplement hated the Manual, and wanted to
replace it. They were very wrong, and I certainly do not mean to
condone any of their actions. Nevertheless, their practical pro-
grams harmonize very nicely with the Manual itself, especially if
we disregard the outdated parts. Has any Anomian come up with
a better plan? And what alternative is there? Surely, none among
us would choose to implement the actual instructions in the Man-
ual! That would be barbaric!”

Everyone nodded. The professor certainly had a point there.
Sensing his advantage, Dr. Side held up a copy of his recent book,
City Builders in an Age of Cave Dwellers, and proclaimed: “The an-
swers are all in this book! The blueprints are no longer missing!”

The crowd went mad. At last, here were answers! Here was a
way to build the City without going by the Manual—and without
seeming to reject the Manual, either. Thousands of Dr. Side’s
books were sold. And while it didn’t quite live up to its reputation
(it didn’t actually have detailed blueprints cither—just a general
theme in terms of the 1848 Supplement), it accomplished a lot. It
made the Anomians feel guilty for the way they had been building
in the past. It showed how those parts of the City that had been
built should be torn down. It demonstrated that the City had been
built at the expense of the Cave-Dwellers (well . . . it didn’t ex-
actly demonstrate that point, but it repeated it so many times that
everyone believed it), And, from the Anomian point of view, it was irre-
Sutable.

The people of Anomia gladly gave Dr. Side and the Other Sid-
ers the power to do whatever they wanted. And he, in turn, pro-
vided everyone with a lifetime supply of Candy Canes and Ger-
man Marks. Some began complaining that the Canes didn’t di-
gest well, and that the Marks had no exchange value; but trouble-
