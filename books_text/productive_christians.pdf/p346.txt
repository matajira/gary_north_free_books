332 Productive Christians in an Age of Guilt-Manipulators

“marriageable age” was forced to be passed around among the
men. Every woman became fair game for an anabaptist’s lust. All
this led, understandably, to rapes, suicides, and severe punish-
ments; mass executions took place almost every day.** (On one
notable occasion, Bokelson himself beheaded a virtuous woman who
had refused his sexual advances. As he ceremoniously chopped her
head off in the public square, a choir of his wives sang “Glory to
God in the Highest.”) This went on for a year and a half, until the
city was captured at last by the orthodox forces, who put Bokelson
and his lieutenants to death for their crimes—crimes committed
in the name of love, equality, and spirituality,

Shafarevich observes another very curious fact about Mintzer
and Bokelson: they became the first “in a long list of revolutionary
leaders” to break completely under defeat.35 When the end came,
both Mantzer and Bokelson ran for cover (Bokelson hid in a
tower, which is mildly amusing in light of the fact that, just before
the city fell, he had ordered all towers to be destroyed, on the
grounds that they were unfairly “superior” to other buildings;*6
identical orders, incidentally, were issued —but not carried out —
during the French Revolution). When they were caught, the social-
ist leaders confessed, informed on their confederates, and begged
for their lives to be spared. “This strange and contradictory figure
will reappear in subsequent historical epochs. He is a man of
seemingly inexhaustible energy when successful, but a pitiful and
terrified nonentity the moment his luck turns against him.”37
Shafarevich explains: “An ideology that is hostile to human per-
sonality cannot serve as a point of suppport for it,758

I have necessarily omitted a great deal of Shafarevich’s mate-
rial on this subject, and he has by no means told the whole story.
Many other groups, with stories just as horrifying, could be men-
tioned, along with the various cults that served as links between

34, Ibid., pp. 63f.

35. Ttid., pp. 58, 66, 79.
36. Ibid., p. 66; cf. p. 50.
37. Bid., p. 79.

38. Ibid., p. 269; cf. p. 294.
