390 Productive Christians in an Age of Guilt-Manipulators

ted to use his beanstalk to trespass on the giant’s property and
steal his possessions. God’s law protects everyone's property, even
rich, unpopular giants,

Marxism is the philosophy of Karl Marx (1818-1883), who wrote
Das Kapital, perhaps the most famous unread book in the world,
except possibly for Darwin’s Origin of Species. Marxism teaches
that everyone who gets rich does so at the expense of the poor.
Marxism teaches that labor is the only source of value, Marxism
teaches that the lower classes are always exploited by the upper
classes. Marxism teaches that landowners should be dispossessed,
and that all property should be held in common, jointly “owned”
by all. Marxism teaches that people should have only what they
need. By now, some of these ideas may sound suspiciously
familiar,

Mercantilism was popular during the 16th and 17th centuries. It
was the theory that if one nation gains, it must be at the expense
of another nation. Mercantilists felt that if any exchange benefit-
ted a person, he must have ripped off the other guy. So they said
the government should intervene to make things fair— by favoring
one group over another. This way, no one would benefit except
the government and those who receive government handouts.
They seldom used out-of-context Bible verses to support their case
for state intervention. Unfortunately, some modern mercantilists
do. This tends to confuse people. Nevertheless, it sells books.

Minimum wage laws prohibit. employers from paying their
workers below a certain amount. Thus, such laws “help” many
poor people by making it illegal for unskilied workers to get jobs.
Anyone who sees the logic in this can qualify as a roving editor for
theOtherSide, and will be invited to give chapel lectures to seminary
students. See Institutional unemployment and Surpluses.

Money is a commodity, a real thing which people value. It is the
most marketable commodity. In the Bible, money is always gold and
silver. Money makes exchange much easier, because people can
