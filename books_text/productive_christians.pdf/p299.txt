Déja Vu—A Romp Through Ronald Sider’s Second Edition 285

laughing.®?

If Sider and Taylor scare you, however, you should realize
that they are just the Marshmallow Corps. Their language is
quite mild, compared with that of some of their associates in the
“liberation theology” movement. Consider the Mexican university
professor José Miranda, author of Communism in the Bible®? — which
is, of necessity, a very thin book, just like Konstantin Chernenko’s
little number on human rights under the Soviets. Not thin
enough, though. Here are some quotes from Miranda:

It is time to drop all these side issues and concentrate on the fundamental
fact: the Bible teaches communism.*+

Communism is obligatory for Christians.®*

The Ananias episode .. . means: pain of death for whoever betrays
communism, Christianity’s indispensable condition.**

No one can take the Bible seriously without concluding that according to
it, the rich, for being rich, should be punsshed.8?

All differentiating wealth is ill-gotten . . . therefore to be rich is to be un-
just.8®

Miranda goes on to argue that aii the “wealthy” —those who have
“differentiating wealth” (any possessions above the lowest com-

82. For information on a real defense system—one which is sensible,
workable, and which will not enslave us to the Soviets—sce General Daniel
Graham, High Frontia: A Strategy for National Survival (New York: Tom Doherty
Associates, 1983); see also Gen. Daniel Graham and Gregory A. Fossedal, A
Defense that Defends (Old Greenwich, CT; Devin-Adair, 1984); and, Brig. General
Albion Knight and David S. Sullivan, eds., The Defense of America: From Assured
Destruction to Assured Survival (Houston: Texas Policy Institute, 1983). “High Fron-
tier” (dubbed “Star Wars” by its opponents) is nof a plan for nuclear weapons in
space; it does not involve offensive warfare; it uses off-the-shelf, already available
technology; and it will reduce the cost of defense. In fact, itis so utterly peaceful
that one wonders why pacifistic Professor Sider has not championed it as his
own. My guess is: “High Frontier” is teo peaceful for Sider, because it will not
result in the destruction of the evil, guilty, capitalistic U.S.A.

83. José Porfirio Miranda, Communism in the Bible, Robert R. Barr, trans,
(Maryknoll, NY: Orbis Books, 1982).

84. Ibid., pp. 6f.

85. Ibid, p. 8.

86. Ibid., p. 1.

87. Ibid., p. 24,

88. Wid., p. 32,
