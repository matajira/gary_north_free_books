Biblical Law and Christian Economics 43

mines the size of the state. As an absolute, outside limit, any tax
of ten percent or more is specifically regarded by Scripture as
Sranny—an attempt by rulers to be like God, extracting a “tithe”
(I Samuel 8:15, 17). See what I mean about limited government?
There’s no way such a tax could possibly support a massive
power-state; and certainly the kind of omnipotent paternalism en-
visioned by Sider would be out of the question.?2

In summary: Ronald Sider, with varying degrees of accuracy,
has pinpointed certain problems regarding poverty in our age.
But. almost without exception, his proposals for solving these
problems are unbiblical to the core. It is not difficult to find Scrip-
tural proof for the assertion that we should “do something” about
the poor. But that alone does not guarantee that our solutions will
be biblical in the slightest. We must follow through with the
Bible’s answers, in concrete applications of biblical law. And that is ex-
actly what Mr. Sider consistently refuses to do. Talk about
“justice” is cheap: the Pharisees did it all the time. They chattered
around the periphery of biblical law, taking the smorgasbord ap-
proach of picking and choosing laws they liked; but the Lord
Jesus condemned them for abandoning “the weightier provisions
of the law: justice and mercy and faithfulness” (Matthew 23:23).
Ronald Sider has made the same deadly error. He has forsaken
the only standard of justice and mercy, the sole blueprint for a just
social order, He has substituted his own outline of social
justice—an outline which more closely resembles Marx’s Com-
munist Manifesto. than it does the book of Deuteronomy. He has
called for dozens of interventionist and socialist programs which
Scripture specifically forbids; he seems to assume that envy is a
virtue; he writes of social problems, not in terms of sin, but of
class war and hatred; he opposes biblical ethical standards; he
specifically teaches others to break God’s commandments with
regard to personal and social moral issues.

And he has done ail this in the name of God.

22. See James B. Jordan, The Low of the Covenant (Tyler, TX: Institute for
Christian Economics, 1984), pp. 225-39.
