The Background of “Productive Christians” 353

of the revolutionary Anabaptists.§ I see myself as a neo-Puritan,
and Sider is forthrightly an Anabaptist, I wrote my doctoral dis-
sertation on the economic thought of the New England Puritans,
and he wrote his on an aspect of Anabaptist history.

‘The Anabaptist (‘baptize again”).movement was an early off-
shoot of the Protestant Reformation. They did not baptize in-
fants, and they required rebaptism to join their congregations.
There are two traditions in the history of the Anabaptist move-
ment. The first is one of revolution, communism, and violence.
The second is pacifist, communalist, and pietist. This second tra-
dition is more familiar to us, since it is represented by the Men-
nonites and the Amish, who seem somehow quaint, posing no
threat to society. But the earlier Anabaptists were anything but
quaint. They were mass murderers. From 1520 to 1535, in what is
now Germany, hordes of them challenged the civil order, and they
began a revolution. Jan Bokelson (John of Leyden), a
26-year-old tailor, and Jan Matthijs, a baker, led a proletarian
revolution in the city of Miinster, in Westphalia, which they cap-
tured in 1533. The intensity of the revolution grew: communal
property was imposed, then a “New Zion” was announced by the
“prophet,” John of Leyden, who then became “king.” He estab-
lished polygamy, selecting several wives for himself, The churches
were burned, Leyden established a reign of terror. He an-
nounced: “Impiety prevails everywhere. It is therefore necessary
that a new family of holy persons should be founded, enjoying,
without distinction of sex, the gift of prophecy, and skill to inter-
pret revelation, Hence they need no learning; for the internal
word is more than the outward expression. No Christian must be
suffered to engage in a legal process, to hold civil office, to take an
oath, or to hold any private property; but all things must be in

8. Martin Luther, “Against the Robbing and Murdering Hordes of Peasants,”
(1525), in Luther's Works (Philadelphia: Fortress Press, 1967), Vol. XLVI. See also
Luther, “An Open Letter on the Harsh Book Against the Peasants,” (1525), ibid.,
pp. 63-85, John Calvin was also hostile to the revolutionary Anabaptists. See
Willer Balke, Calvin and the Anabaptist Radicals (Grand Rapids: Eerdmans, [1973]
1981).
