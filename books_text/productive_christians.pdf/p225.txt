Preparing the Church for Slavery 21

cipalities and powers, as supreme Lord over all who have rule and
authority (Ephesians 1;:20-22). Jesus is the King of all kings, the
Lord of all lords (Revelation 19:16); and we rule with Him, wag-
ing war and overcoming. No state —nene—can successfully lord it
over God’s people. We are kings, and those who oppose us must
be crushed to powder: the nation that will not serve us will perish
(Isaiah 60:12).

For the Lorp Most High is to be feared,

A great King over all the earth.

He subdues people under us,

And nations under our feet. (Psalm 47:2-3)

It is thus of crucial importance to Satan’s plan that he delude
the church into thinking she is powerless. And if the church is
bemused by guilt-manipulators and sapped of her vigor, our na-
tion is lost. Christians alone have the power of dominion over the
Evil One; we alone can provide the moral force to prevail against
the enslaving state, for the principle of liberty in Christ has set us
free from the bondage of men; we alone can preserve our land
from destruction, for we are the salt of the earth. But if the day
comes that we lose our savor, we will be cast out with the heathen,

We would like to tell ourselves that this can never happen to
God’s people, but that is a devilish lie to keep us secure in the very
mouth of destruction. It happened to the churches of France in
1789; of all Europe in 1848; of Russia in 1917; of Germany in
1933, In every case, the churches had been rendered impotent—by guilt,
by fear, by benefits; and always because the church departed from
the word of God as the only standard for every area of life. Do not
say it cannot happen here, That is to say that we can do all things
without Christ. Do not say that we will somehow muddle through
the crisis of the hour: Christ did not call us to muddling, but to
victory. Life is a hattle—no, more: it is a war to the death with the
forces of evil. We cannot merely hold our ground. If we do not
conquer we will be conquered. If we do not gather with the vic-
torious Christ, we are scattering abroad. There is no middle
ground, no possible moderation or compromise. If Ronald Sider
