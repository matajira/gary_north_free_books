324 Productive Christians in an Age of Guilt-Manipulators

society; second, it may well be on the “fringe” of another, organized,
long-term revolution —one which is biding its time and benefiting
from the chaos brought about by the millenarians. Someone has the
blueprints.

One of the first of these millenarian movements of the Middle
Ages was that of the Cathars (“the pure”), a loosely united group of
Manichean sects. Ail these sects possessed a dualistic worldview,
holding that there was an “irreconcilable contradiction between
the physical world, seen as the source of evil, and the spiritual
world.”® This resulted, logically enough, in the denial of the In-
carnation and the Resurrection of Christ, and in the rejection of
the Old Testament. The power of the civil government was held
to be a creation of an evil god; having children was demonic; and
the ultimate goal of the the human race was universal suicide.? The
Church was hated and regarded as the Great Whore of Babylon:
according to Cathar doctrine, the Church had fallen into irretriev-
able apostasy when it became legitimate in the time of Constan-
tine.!9

Because property was considered an aspect of an inherently
sinful, material world, the Cathar leaders demanded that their
followers forsake private ownership and practice communality of
property. “In their sermons, the Cathars preached that a true
Christian life was possible only on the condition that property was
held in common,”"! (It was, of course—as always—the benevolent
leaders who watched over the socialized belongings.) And, since
the evil god of the Old Testament had forbidden adultery, promis-
cuity was encouraged, wives were shared, and faithful marriage
was condemned as sinful: “Marital bonds are contrary to the laws
of nature, since these laws demand that everything be held in
common.”!2

8. Shafarevich, The Socialist Phenomenon, p. 19,
9. Ibid, pp. 19.

10. ibid., pp. 21, 27, 29, 34. CF. the similar teaching of modern anabaptists;
this is the thesis of, for example, Leonard Verduin’s The Anatomy of a Hybrid: A
Study in Church-State Relationships (Grand Rapids: Eerdmans, 1976).

11, Shafarevich, The Socialist Phenomenon, p. 23.

12. Ibid., p. 24.
