xiv Productive Christians in an Age of Guilt-Manipulators

The present book would not have been possible without Lord
Bauer’s pathbreaking and courageous research. It is my firm con-
viction that when—I do not say if—-the currently less-productive
nations of the world ascend out of poverty, they will, in no small
measure, have the writings of P. T. Bauer to thank for it. I have
therefore dedicated this, the jinal edition of Productive Christians, to
hima.
I also wish to thank my wife, Darlene, for her loving en-
couragement and wise counsel throughout the writing of this
book, and for her gracious assistance in its preparation: “Her
worth is far above jewels” (Proverbs 30:10).
