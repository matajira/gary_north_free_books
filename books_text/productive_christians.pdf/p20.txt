6 Productive Christians in an Age of Guilt-Manipulators

or in despotism: in the disintegration of society or in the tyranny of a
state in which all things are levelled without any regard to true liberties
and true rights.”

A man or movement may claim to be Christian, and yet not be;
a man or movement may ée Christian, and yet have unbiblical
ideas. The test is Scripture, and Scripture alone. Not wishes, not
“rights,” not wants or needs; try every word a man speaks at the
bar of God’s inerrant Word. Those who advocate the lawiess over-
throw of society—even if it is technically “legal’—are opposing
God’s commands, The ultimate end of the Revolution is always
unbelief. “The defining feature of the Revolution is its hatred of
the Gospel, its anti-Christian nature. This feature marks the Rev-
olution, not when it ‘deviates from its course’ and ‘lapses into ex-
cesses,’ but, on the contrary, precisely when it holds to its course
and reaches the conclusion of its system, the true end of its logical
development, This mark belongs to the Revolution, The Revolu-
tion can never shake it off. It is inherent in its very principle, and
expresses and reflects its essence. It is the sign of its origin. It is
the mark of hell.”8

The brutality of the French Revolution was not endemic to that
particular situation alone: it is essential to the very nature of the
Revolution itself, All revolutions have begun with sincere pleas for
liberation; all have been carried on by ever-increasing justifications
of infringements on liberty; all have ended in chaos and tyranny.
Revolt against God’s eternal standards can produce nothing else.

One of the most prominent of the new voices in evangelicalism
is Ronald J. Sider, professor of theology at Eastern Baptist Sem-
inary in Philadelphia. He is the president of Evangelicals for
Social Action, a national organization “committed: to the
preaching and practice of biblical justice and peace,” as an ESA
pamphlet puts it. The ESA sponsors a wide variety of activities:

7. Ibid., Lecture IX (1975), pp. 736.

8. [bid.,. Lecture VILE (1975), pp. 296.

9, See Otto Scott's Robespierre: The Voice of Virtue (Ross House Books, P.O.
Box 67, Vallecito, CA 92521). This enlightening and thoroughly terrifying book
is must reading for those who wish to study the rise and progress of revolutions.
