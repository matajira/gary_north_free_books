God's Law and the Poor 65

usually. become wealthy by oppressing the poor.*” (The validity of
this notion will be examined later.) He thus seeks to break this
economic power through granting more power to the state, Are
those greedy, laissez-faire capitalists charging “too much” for
their products? Are those “bourgeois running dogs” paying the
noble worker an “unfair” wage? Let’s bring in some heavy-
handed government clout to take care of the problem. What we
need is an omnipotent state that will enforce price and wage con-
trols on corrupt businessmen, (Never mind that such controls in-
evitably result in shortages and unemployment. Why, that’s a
doctrine of the deist, Adam Smith, His law of supply and demand
has been repealed. Lord Keynes has revolutionized us, ridding us
once and for all of Enlightenment philosophies and secular eco-
nomic theories. *) Of course, various societies for at least the last
4000 years have attempted such measures before. Some men who
implemented price and wage controls are, in fact, quite justly fa-
mous for their actions. One government leader of a generation
ago will be long remembered. For‘a time, he was able to stabilize
prices, wages and employment at a level that moderri bureaucrats
can only dream of. He was so successful that even if you aren't a
history student you.may recall his name: Adolf Hitler,

The point is that economic controls require an omnipotent, en-
slaving state to enforce them. If you aren’t willing to have
totalitarianism, the controls won’t work, Price and wage manage-
ment is impossible without complete oversight of every sector of
society. Halfway measures will not suffice. Hermann Giring,
Hitler’s economic planner, admitted this to an American corre-
spondent in 1946, when he was a prisoner of war. Speaking of
American economic programs which were similar to some of his
own past endeavors, he offered this revealing comment: ‘‘You are
trying to control people’s wages and prices—people’s work. If you
do that you must control people’s lives. And no country can do that part
way.” That was the thrust of Hilaire Belloc’s mocking advice to

17. Sider, Rich Christians, p. 73 fp. 65]; Cry Justice, pp. 31, 203, 210.

18. See Sider, Rich Christians, pp. 114f. (pp. 102 f.]. See the discussion of
Smith below, pp. 179-83.

19. Cited in Schuettinger and Butler, Forty Centuries of Wage and Price Controls,
p. 73. Italics added.
