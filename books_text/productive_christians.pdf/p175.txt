The Jubilee Principle 161

conversions expanded the Hebrew population rapidly, as Pharach
noted.

“Had a rate of 4.17% per annum continued (let alone increased
as a result of zero miscarriages), they would have multiplied from
2.5 million to 10 billion — twice today’s total world population — in
197 years after the Exodus, This gives you some idea of the poten-
tial for growth which God’s promise, coupled with a comparable
rate of growth and conversions, offers, It means that within a cen-
tury, the whole world of their day would have come to the true
faith, (There were not billions of people from which to draw con-
verts, of course.) But to have achieved this, they would have had
to remain covenantally faithful. They didn’t remain faithful.

“The dominion covenant is an ethical covenant. When men con-
form themselves rigorously to God's law, through God's grace, they
are to expect incomparable blessings. The whole earth is to come
under covenant man’s jurisdiction as rapidly as possible. The rule
of God’s law on earth is not to be delayed ‘for old time’s sake.’ God
offered the Hebrews world dominion when they entered Canaan.
Canaan was little more than a point of embarkation.

“They did not respond ethically to the requirements of God’s
law. But if they had, it should be clear how little the Jubilee Year
would have been worth to any given family. With per capita land
of about four acres per person when they. entered Canaan, not
counting any of the mixed multitude who may have covenanted
themselves to Israel (Caleb, for example, was the son of a Kene-
zite: [Josh 14:6]), it was clear to them what large families would
do to the inheritance of each family member. It would shrink to
insignificance. The more faithful the Israelites were to the cove-
nant, the faster the inheritance per capita would shrink.

“What the Jubilee Year represented to a faithful community
was simple: an incentive from God to spread across the face of the earth.
There could be no hope in land ownership in Israel for a covénan-
tally faithful community. God was offering them world dominion;
no family in Israel could hope to remain in the land and prosper,
Each family had to prepare its heirs to make plans to move to dis-
tant lands, to infiltrate and gain control over the kingdoms of the
