A Biblical Theology of Slavery 205

incarceration in a mental institution, to murder again, for humanists
are unwilling to allow the State to turn the murderer over to God as
rapidly as possible, so that God can deal with them eternally, They
regard man as the sovereign judge, not God; so they have invented
the slave-master institution of the modern prison, while they have
stcadily rejected the legitimacy of capital punishment. Better to let
murderers go free, humanists assert, than to acknowledge covenant-
ally and symbolically that the State has a heavenly Judge above it,
and that Ged requires human judges to turn murderers over to Him
for His immediate judgment, once the earthly courts have declared
them guilty as charged.

The nineteenth century’s indiscriminate attack on all forms of
privately owned servitude was ultimately an attack on God's law.
This hostility to indentured servitude rested on a key assumption:
that honoring God's revealed law is a criminal act. The humanist
abolitionist tries to put God in the dock, He trics to put the State on
the judgment throne of God. What he hates is the Bible, not slavery
as such. The question is never slavery vs. no slavery. The question
is: Who will be the stave-master, and who will be the slave? Autonomous
man wants to put God and His law in bondage. On judgment day,
this strategy will be exposed for the covenant-breaking revolution
that it has always been. The abolitionists will then learn what full-
time slavery is all about. It is a lesson that will be taught to them
for eternity.

Pharaoh’s spiritual heirs are with us still. Christians are in spiri-
tual and cultural bondage to the theology of the power religion, and
therefore to the State. They must prepare for another exodus, mean-
ing they should be prepared to experience at least a share of the pre-
liminary plagues, just as the Israelites of Moses’ day went through
the first three out of ten. It is nevertheless time to leave Egypt, leeks
and onions notwithstanding. We must be prepared for numerous ob-
jections from Pharaoh's authorized and subsidized representatives
inside the camp of the faithful. They owe their positions of influence
to Pharaoh and his iaskmasters, and they will not give up their au-
thority without a confrontation. They will complain that their poten-
tial liberators are at fault for the increased burdens that Christians
suffer (Ex. 5:20-21). They will continue to sing the praises of the wel-
fare State. They will continue to sing the praises of tax-supported
