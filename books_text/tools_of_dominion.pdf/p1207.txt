Selling the Birthright: The Ratification of the U.S. Constitution 1199

Harrington explained so Puritan a conflict as the English Civil War
of the 1640's in terms of social forces, not religion, a secular tradition
of historiography to which Marxist historian Christopher Hill ap-
peals.3! The textbook histories of the American Revolution from the
earliest days have been far closer to Harrington’s view of historical
causation than to R. J. Rushdoony’s.

We do not find authoritative references to the Bible or church
history in either The Federalist Papers or the Anti-Federalist tracts.
Adrienne Koch’s compilation of primary source documents, The
American Enlightenment, is not mythological, even though it is self-
consciously selective? There was an American Enlightenment,
though subdued in its hostility to Christianity.* Jefferson, after all,
kept hidden his cut up, re-pasted New Testament, purged of the
miraculous and supernatural; he knew what his constituents would
have thought of such a theology.** He refused to publish this book,
he told his friend, Christian physician Benjamin Rush, because he
was “averse to the communication of my religious tenets to the public,
because it would countenance the presumption of those who have
endeavored to draw them before that tribunal, and to seduce public
opinion to erect itself into that inquest over the rights of conscience,
which the laws have so justly proscribed.” That is, if word got out
to the American voters, who were overwhelmingly Christian in their
views, regarding what he really believed about religion, he and his
party might lose the next election, despite a generation of systematic
planning by him and his Deistic Virginia associates to get Christian-
ity removed from the political arena in both Virginia and in national
elections.° (The book was not made public until 1902. In 1904, the
57th Congress reprinted 9,000 copies, 3,000 for use by Senators and
6,000 for the House.*” It was a very different America in 1904.)

31. Christopher Hill, Puritanism and Revolution: Sturties in Interpretation of the English
Revolution of the 17th Century (New York: Schoken, [1958] 1964), p. 5.

32, Adrienne Koch (ed.), The American Entightenment: The Shaping of the American
Experiment and a Free Society (New York: Braziller, 1965).

33. Henry F. May, The Enlightenment in America (New York: Oxford University
Press, 1976).

34. The Life and Morals of Jesus of Nazareth Extracted textually from the Gospels.
Reprinted as An American Christian Bible Extracted by Thomas Jefferson (Rochester,
Washington: Sovereign Press, 1982).

35. Cited by Russell Kirk, The Roots of American Order (LaSalle, Illinois; Open
Court, 1974), pp, 342-43.

36. Ibid., p. 343.

37, Introduction, American Christian Bible, p. 4.
