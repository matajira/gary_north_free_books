The Economies of the Tabernacle 899

nence. A cathedral is very expensive. If it is constructed in a central
city, it will soon find itself surrounded by very a different economic
and social environment. A cathedral could be constructed in some
distant rural region, but that would not serve the needs of worship-
pers. Any site within a two-hours’ drive or train ride from a central
city could become surrounded by urban decay within two genera-
tions. 8 In this sense, the modern world has become a tabernacle so-
ciety rather than a temple society, Cathedrals are not designed, as
the tabernacle was, as a prefabricated mobile construction project.

Regional Splendor

The church is both local and international. It is bound to local
historical circumstances at any stage in history, yet it is always inter-
national because it is linked to eternity. There is a tendency within
Protestantism to ignore the international and eternal aspects of the
church. Protestant pastors often enjoy building large, fancy places of
worship, for these testify to the influence of the pastor as a builder.
Seldom do these churches reflect long-lasting architectural standards.
Architects display little concern with architectural manifestations of
the church as a force to be reckoned with over long periods of time at
every level of society. Too often the architects selected by churches are
deeply humanistic and governed by aesthetic standards that are
openly rebellious against beauty. They are committed to an architec-
ture of self-conscious ugliness.!9 Beauty is far more objective than
something in the eye of the human beholder; beauty is in the eye of
the Cosmic Beholder. Architects symbolically deny the Cosmic
Beholder by rebelling against all permanent standards of beauty.

Because of the fragmenting of religious denominations, the eco-
nomic resources necessary for constructing great cathedrals have not
appeared in the twentieth century. The large, mainline denomina-
tions that might be able to afford to build them no longer bother.
Central denominational bureaucracies are far more likely to give
money to revolutionary causes or bureaucracy-expanding causes.
Meanwhile, the smaller denominations concentrate on missions or
other spiritual ventures.

There is no architectural representation of the majesty of God
that competes today with the majesty of the State. This statist majesty

18. This is exactly what happened to the most grandiose of all American cathedral
projects, the Episcopalians’ Church of St. John the Divine in New York Gity.
19, Tom Wolfe, From Bauhaus to Qur House (New York: Farrar Straus Giroux, 1981).
