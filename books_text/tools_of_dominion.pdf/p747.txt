The Prohibition Against Usury 739

repay the loan early. Second, since the garment is in the possession
of the lender during the day, it cannot be used as collateral with
another lender. One piece of collateral can be used for only one loan
at a time, if the lender demands collateral. If the borrower kept it,
and simply signed a note saying that it stands as collateral for the
loan, he may sign several such notes for several lenders. If he
defaults, they cannot all collect their collateral. Therefore, by per-
mitting the lender to demand half a day’s collateral, biblical law re-
duces the temptation on the part of borrowers to commit fraud.

Fractional Reserve Banking

Modern banking is based on the flagrant flouting of the prohibition
against multiple indebtedness. For every asset a bank owns, there are
many claims—legal claims— against that asset. The bank keeps fewer
reserves on hand to meet demands of lenders to the bank — depositors —
than the bank has promised to deliver on demand. This is called frac-
tional reserve banking. It is the universal form of banking and has been
since the early modern period. It was an invention of the Renaissance.

Depositors believe that their money is available on demand. The
banks have promised them that it is available on demand. But it
isn’t. Tf every depositor came to the bank one day and began to with-
draw his moncy, the bank would go bankrupt. The bank loaned out
the depositors’ money in order to earn interest on the loans. Part of
this rcturn is paid to depositors as interest on their accounts. The
depositors know this, but they all assume (as do the bank’s man-
agers) that not all depositors will try to get their moncy out on the
same day. They assume that withdrawals will tend to equal deposits
on any given day. Usually, this assumption is correct. The day men
jose faith in the solvency of the bank—in the bank’s ability to repay
those few depositors who demand their money—a bank run ensues.
Everyone wants his money at once. The bank defaults. It has run
out of “raiment.”

Without the protection of state and federal government agencies,
fractional reserve banking would face the prospect of bank runs, as
lenders (depositors) would lose faith in overextended (multipally in-
debted) banks. The most important form of collateral a bank should
have is its reputation for honesty and conservative {minimal frac-
tional reserves) investing policies. In a truly biblical society, banks
would be required to have 100 percent reserves. In the twentieth

64. North, Honest Money, ch. 7.
