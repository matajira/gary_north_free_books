42 TOOLS OF DOMINION

Guilt and Social Paralysis

This should not be a time for pessimism among Christians. Yet it
is. They are missing an opportunity that has not been seen since the
late eighteenth century, and possibly since the resurrection of Christ.
A universal world civilization now exists for the first time since the
Tower of Babel, It is disintegrating morally as it grows wealthy. It is
ripe for the harvest.

A successful harvesting operation requires tools. To take advan-
tage of this unique historical opportunity, Christians need tools of
dominion—blueprints for the reconstruction of the world. But
Christians today do not see that God has given them the tools of do-
minion, His revealed law. They agree with the humanists who in
turn agree among themselves, above all, that the Bible offers society
no specific legal standards for comprehensive reform and reconstruc-
tion. They agree with such statements as the one made by the editor
of The Journal of Law and Religion, who is also a professor of Constitu-
tional law at a Catholic law school:

First, I assume that the Bible is not a detailed historical blueprint for
American society, and that it does not contain much concrete guidance for
the resolution of specific political conflicts or constitutional difficulties such
as slavery and racism, sexism and equal opportunity to participate in soci-
ety. The biblical traditions are not to be viewed as an arsenal of prooftexts
for contemporary disputes, Contextual leaps from the situations in which
the biblical authors wrote to the situations with which we find ourselves
faced are likewise to be avoided.

Notice that he raised the controversial issue of slavery. So does a
professor of Hebrew scriptures at Notre Dame University in Indiana:
“Then there is the larger hermeneutical issue of the Christian ap-
propriation of Old Testament law and the binding nature of biblical
norms and stipulations in general. Who today, for example, would
be prepared to argue that laws concerning the conduct of war or
slavery retain their binding authority for the Christian or for anyone
else?”2° Who would? I would, and so would those who call them-
selves Christian Reconstructionists. This is why Christian Recon-

35. Edward McGlynn Gaffney, Jr. “Of Covenants Ancient and New: The Influ-
ence of Secular Law on Biblical Religion,” Journal of Law and Religion, Th (1984), pp.
117-18.

36. Joseph Blenkinsopp, “Biblical Law and Hermeneutics: A Reply to Professor
Gaffney,” ibid., TV (1986), p. 98.
