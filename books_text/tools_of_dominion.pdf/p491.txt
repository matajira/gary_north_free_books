The Ransom for a Life 483

son of that builder to death.”

This sharp difference from Babylonian law would appear to be
an application of the principle of Deuteronomy 24:16: “The fathers
shall not be put to death for the children, neither shall the children
be put to death for the fathers: every man shall be put to death for
his own sin.”

Conclusion

The Bible establishes the principle of cosmic personalism as the
foundation of the universe.® There is no way that men can escape
their responsibilities before God. Because biblical law recognizes this
principle, it establishes the judicial principle of restitution to victims
by the negligent. The general rule is: an eye for an eye, a life for a life.

The Bible affirms the principle of limited liability before men.
The State is not God. It cannot know every aspect of historical
causation. Neither can men. The State therefore cannot lawfully im-
pose unlimited liability on those convicted of negligence, irrespective
of their knowledge, decisions, and contractual arrangements.

In this unique instance, the case of a dangerous ox that kills a
person, the guilty owner can legitimately escape death, though his
beast cannot, because the victim’s heirs are allowed to impose an
economic restitution payment on the negligent individual. This law
of criminal negligence is much broader than simply oxen and own-
ers; it applies to all cases of death to innocent parties that are the
result of negligence on the part of owners of notorious beasts or noto-
rious machinery —capital that is known to be risky to innocent by-
standers. Automobiles, trucks, certain kinds of occupations, nuclear
power plants, coal mines, and similar examples of dangerous tools
are covered by this general principle of personal liability.

This law should not be understood as applying to workers who
voluntarily work in dangerous callings and who have been warned in
advance of the risks by their employers, nor should it be used as a
Justification for the creation of a messianic State that attempts to dis-
cover criminal negligence in every case of third-party injury, despite
the lack of knowledge of risks by the owners or experts in the field.

52. Code Hammurabi, paragraphs 229-30. Ancient Near Eastern Texts Relating to the
Old testament, edited by James B, Pritchard (rd ed.; Princeton, New Jersey: Prince-
ton University Press, 1969), p. 176.

53. North, The Dominion Covenant: Genesis, ch. 1.
