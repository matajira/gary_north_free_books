724 TOOLS OF DOMINION

aspect of human action; it is therefore inescapable. This explanation
denies the Aristotelian idea that the phenomenon of interest is solely
a function of money.

The prevailing market rate of interest is a component of three
factors, modern economics informs us: 1) time-preference, or the
originary rate of interest (as Mises calls it); 2) a risk premium; and
3) the inflation premium. Few economic textbooks ever explain this,
and no proponents of zero-interest free market loans ever discuss it.

1. Time-Preference

The originary interest rate, or time-preference factor, is the least
understood and yet the most fundamental aspect of the phenomenon
of the market rate of interest.?” Other things remaining equal, a
given quantity and quality of future goods is worth less in the free
market (and in people’s minds) than the same basket of goods today.
This is not because, in the words of an old proverb, “a bird in hand is
worth two in the bush.” I am not speaking here about comparative risks
of obtaining ownership, “in hand” vs. “in the bush,” meaning present
vs. future. I will discuss the risk factor later on. I am speaking here
about interest as a fundamental category of human action.

We live in a universe that is structured by the category of time.
We necessarily live and act in the present. We cannot escape the con-
straints of time. We prefer satisfaction now. A brand-new auto-
mobile (or anything else) is more valuable to me right now than the
delivery of an identical car a year from now (other things — public
tastes, market value, gasoline prices, etc. — being equal). I act in the
present. I choose to do in a sequence of events those things that I am
capable of doing with whatever assets I possess. I plan for the future,
but I am not immediately responsible for the future, for I have no con-
trol over it. I am responsible only in the present. Thus, what happens
in the present is more relevant for me than what I expect in the earthly
future, since I must live in the present in order to get to the future. I
am responsible in the here and now, not in the there and then.

Let us consider all this in more general terms, Biblically speak-
ing, an individual is responsible to God in the present. He cannot
escape this covenantal responsibility. As a person created in God's
image, he must place higher value on action in the present than ac-
tion in the future. He is not yet responsible for what he will do in the

27. Mises, Human Action, ch. 19.
