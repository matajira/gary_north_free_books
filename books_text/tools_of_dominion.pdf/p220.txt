212 TOOLS OF DOMINION

father, thereby indicating his cconomic productivity, or at least his
position as a man possessing inherited capital.© The bridegroom’s
payment of a required bride price is the key to understanding this
case law.’

 

To Give a Wife

Jacob wanted to marry Rachel. He had no visible, transferable
capital, for he was a fugitive, even though he had received Isaac’s
blessing. Without an assured inheritance, he had to pay Laban a
bride price. That bride price was seven years of labor: “And Jacob
served seven years for Rachel” (Gen. 29:20a). His words are signifi-
cant: “Give me my wife, for my days are fulfilled” (Gen. 29:2la). Give
me my wife, he insisted. The father had to give his daughter to the
bridegroom, once he had met the terms of the bride price. Rachel
now belonged to Jacob. He had paid the price.®

Exodus 21:4 reads: “If his master have given him a wife, and she
have born him sons or daughters; the wife and her children shall be
her master’s, and he shall go out by himself.” The language is the
same as Jacob's to Laban: he has given her to him. This raises a sec-
ond crucial question: Where did the master get a woman for his ser-
vant in order to be able to give her to him in marriage? Either she
was a servant already owned by the master, or else she had been pur-
chased by the master for the servant. Perhaps she had been some
other family’s servant. Perhaps she had been the daughter of a free
man. The point is, the master now lawfully controls her as a lawful
father. He can therefore give her to his servant.

6. The bride price would normally have been less than 50 shekels of silver. A man
who seduced an unbetrothed virgin was required by law to pay 50 shekels to her
father and then marry her, with no future right of divorce (Deut. 22:28-29). Addi-
tional evidence of this 50-shekel maximum: the bridegroom who falsely accused a
new bride of not being a virgin at the time of their marriage, and who could not
prove his accusation, had to pay a hundred shekels of silver to her father (Deut.
22:19). This was double restitulion: two times fifty. On these puints, see Chapter 21:
“Seduction and Servitude.”

7. See Chapter 6: “Wives and Concubines.”

8. This is the covenantal basis of Jesus Christ's exclusive lifetime (eternal) owner-
ship of His bride, the church (ph. 3:22-24). The church is a true bride, not a con-
cubine. A concubine in Israel was a wife who possessed no dowry. No bride price
was paid for her, and no dowry was brought into the marriage by her. Legally, had
Christ not died for the church, the church would be a concubine—a second-rate
wife. This is why the church knows that she will never be divorced. This is why Paul
could ask rhetorically: “Who shall separatc us from the love of Christ?” (Rom.
8:35a). Christ paid the required bride price to the Father, The church is not a con-
cubine, even though she brings neither virginity nor dowry into the marriage. The
bride price was paid by Christ at Calvary.

 
