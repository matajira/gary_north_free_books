232 TOOLS OF DOMINION

mained slaves elsewhere, or cven “free men,” worshipping Satan
under the fear of the local shaman. They did learn something of the
Western, Protestant work ethic.

American Negro Slavery

The system of chattel slavery that existed in the antebellum (pre-
Civil War) South had five extremely pernicious features:

The denial of Christ’s jubilee fulfilment
No legal foundation for slave marriages
No legal protection for the slaves

No system of guaranteed redemption
Gang labor was economically productive

The first four of the five great evils of the Southern slave system
were primarily religious and judicial rather than economic, This is
to be expected. Because biblical religion is ethical and judicial, the
institutions that grow out of biblical law are to reflect this legal con-
cern of God. Institutional rebellion will always reflect a denial of the
ethical and judicial character of biblical religion. To focus on the eco-
nomic aspects of life rather than the judicial is itself a manifestation
of man’s rebellion. That modern scholars, influenced by Marx’s eco-
nomic determinism, should redirect our interest away from the judi-
cial and farvilial aspects of slavery, is simply another manifestation
of the distorted presuppositions of medern humanism.

1. Denial of Christ's Jubilee Fulfillment

The South shared the view that had dominated Western civiliza-
tion from the beginning: that Jesus has forever fulfilled Israel's
jubilee land tenure system (Luke 4:16-21). Like the West in general,
the South saw no connection between Jesus’ fulfillment of the jubilee
law regarding permanent chattel slavery (Lev. 25:44-46) and His
abolition of the jubilee laws governing Israel’s land tenure. Thus,
Southerners imported foreign slaves from pagan nations and placed
them and their children in permanent bondage.

As the nineteenth century progressed, people in the West began
to recognize the evil of permanent chattel slavery. The Quakers pio-
neered the anti-slavery impulse, and it was adopted by evangelical
revivalists and Unitarian abolitionists. Southerners continued to ap-
peal to Leviticus 25 in defense of slavery. To do so, they had to ignore
