A Biblical Theology of Slavery 159

on huge construction projects for the State. Mendelsohn is correct
when he says that slavery alone was a suitable labor policy for the
large public works projects of the ancient empires. '°®

Empire Economics: Turning Men Inte Gold

The kings initially would get most of their slaves from the battle-
field. To build an empire, a king had first to be successful on the bat-
tlefield. Then as now, empires are heavily reliant on military con-
quest. Nevertheless, only Greece and Rome became true slave soci-
eties, as far as the presently known records indicate, The empires of
the ancient Near East found ways of integrating the captives into the
conquering society. The Jews who were removed from Israel and
Judah retained their religious nationality during their captivity,
though it took God’s intervention to save them on one occasion
(Esther). What distinguished Greece and Rome was their use of mil-
itary power to create a large subclass of permanent slaves. The State
became part of the process of enslavement by allowing captives to be
sold as slaves by victorious generals.

Soldiers on the march converted booty into gold; they had little
use for slaves. Slave merchants followed the armies of the Greek
city-states, harvesting human crops.!°? The same was true for
Rome’s armies. '* The capitalization of expected future net income,
so technical-sounding a phrase, became an acceptable way for
soldiers and civil governments to convert humanity into capital, and
therefore to make an expanding empire pay a substantial dividend to
its promoters. In fact, the positive feedback relationship between the
profits of slavery and the expansion of classical empire makes it diffi-
cult for the historian to distinguish the primary cause from the sec-
ondary. A plausible case can be made for either the demand for
slaves promoting empire or the price effects of mass enslavement
after a successful military campaign subsidizing those who dreamed
of empire. There is no doubt that in the ancient world, enslavement
and empire were aspects of a closely related reciprocal process.

Rome provides the best example of this process. As the Roman
Republic expanded geographically, it established the economic and
political foundations of the later Empire. The Romans enslaved

106, Mendclsohn, Slavery in the Ancient Near East, pp. 2, 92-96,

107. Westermann, Slave Systems, p. 26.

108. William I. Davisson and Jamex E. Harper, European Evonomic History, vol. 1,
The Ancient World (New York: Appleton-Century-Crofts, 1972), p. 181.
