The Epistemological Problem of Social Cost 1115

to make guesses as to what laws are likely to be worth their costs. Hope-
fully it will use what empirical information is available and seek to
develop empirical information which is not currently available (how
much information is worth tts costs is also a question, however). But
there is no reason to assume that in the absence of conclusive infor-
mation no government action is better than some action.”8”

Please get his argument clear in your mind: welfare economics is
essentially an empirical science, except that empiricism cannot really
solve the issues of welfare economics, so the State will have to decide
what is the appropriate allocation of resources, but economists never-
theless hope that the bureaucrats will use empiricism as the means of
finding solutions to the specific allocation problems, though only an
economically efficient quantity and quality of empiricism should be
purchased. In any case, the State’s decision will necessarily be based
primarily on guesswork. If this explanation resembles a walk through
a hall of mirrors, it is because it is a hall of mirrors. Yet virtually all
essays in welfare economics are little more than guided tours
through this conceptual hall of mirrors. The allocation problem of
welfare economics cannot be solved by humanist economics, for the
economists are overcome by a series of antinomies: the subjective-
objective dualism, the individual-society dualism, the problem of
fixed law and the endless flux of circumstances, and the overwhelm-
ing and unanswered problem of interpersonal comparisons of sub-
jective utility. It is all premised on this formula: dialectics plus intuition
equals cost-effective justice. This formula does not produce anything ex-
cept additional scholarly articles for professors’ vitae — in short, neg-
ative returns.

Third, and far more important for social analysis, there would be
a sense of outrage among the victims of the polluting factory if there
were no State-enforced liability rules. The initial reaction of any one
of the victims, if he knows that the civil law does not protect his own-
ership rights automatically, may be to blow up the factory or murder
its owner. The multiplication of acts of violence would be assured
under such a non-liability legal order. ‘The issue of economic efficiency
therefore cannot be separated from the issue of judicial equity. This is what
Chicago School economists and legal theorists never show any signs
of having understood. When righteous men are thwarted in their
cause by seekers of local “efficiency” who care nothing about the eth-

87. Lbid., pp. 69-70.
