1106 TOOLS OF DOMINION

To begin with, such reasoning is perverse, if accepted as a meth-
odological standard governing economic analysis in all instances in-
volving economic action, It would be just as easy to say of kidnapping
that any restrictions on kidnapping by the State harm the kidnapper,
and that a lack of restrictions harms the victims. If we are going to
build an economic system in terms of the supposedly “reciprocal
nature of harm”—that each economic actor suffers harm when he is
restricted from acting according to his immediate whim—then eco-
nomics becomes positively wicked, not value-free, in its attempt to
sort out just how much harm the courts will allow each party to im-
pose on the other.

There are some areas of life — areas governed by biblical morality —
in which such “cost-benefit analyses” must not even be contemplated.
For example, any attempt to impose cost-benefit analyses on com-
peting techniques of mass genocide, including abortion, is demonic,
not scientifically neutral. Whether a genocidal society should adopt
either gas chambers or lethal injections for adults, or cither saline
solutions or suction devices for unborn infants, cannot be solved in
terms of comparative rates of cost-elficiency, for the economist always
ignores a major “exogenous variable”: the wrath of God. God will
efficiently judge those individuals who promote all such cost-efficient
systems, as well as societies that adopt them, If legal restrictions
against mass genocide harm the potential mass murderers, this is all
to the good. Society faces no “reduction in social benefits” whatso-
ever. Justice does cost something, bul the net economic effect is
positive, whether the economist sees this or not. There is no reduc-
tion in net social benefits as a result of the thwarted goals of the now-
restricted (or previously executed) genocidal technocrats.

Coase offered the following example of reciprocal harm. What
about cattle that stray onto another man’s property and destroy
crops? This, it should be noted, is precisely the issue dealt with by
Exodus 22:5. Coase writes: “If it is inevitable that some cattle will
stray, an increase in the supply of meat can only be obtained at the
expense of a decrease in the supply of crops. The nature of the choice
is clear: meat or crops?”®

This appears to be correct economic analysis, as far as it goes. It
forces us to think about the problem in terms of what members of the
society must give up, meat vs. crops. But his next sentence is the

 

65. idem,
