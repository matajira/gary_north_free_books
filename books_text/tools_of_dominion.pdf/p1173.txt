Pollution in the Soviet Union 1165

This represents an important distinction between the United States and
the U.S.S.R. If, in the United States, private enterprise displays poor con-
servation practices, there are still twa avenues of recourse open for cor-
recting the situation — public vpinion and government regulation, But if the
Sovict central planning mechanism is lax in these regards, there is no effec
tive avenue of recourse. The Party-government supervision of both re-
source exploitation and environmental conservation has strong built-in
conflicts of interest, and brings to mind the analogy directed by some to-
ward our own Atomic Energy Commission of foxes guarding henhouses.’!®

The sheer size of the Soviet planning bureaucracy has inhibited
the implementation of new, pollution-control technologies, he con-
cludes. !9 Furthermore, Marxist ideology sees pollution as a problem
only of capitalist societies. “On the other hand, it is believed that a
socialist economy, faced with the obligation to plan centrally the use
of all its resource wealth, will necessarily do so in the wisest possible
manner,”?? This attitude, coupled with the Marxist emphasis on eco-
nomic growth, has led to a lack of interest in creating institutional
mechanisms—economic, legal, or political—to reduce pollution.

The Poverty Factor

Goldman does not mention it, but by keeping people poor, social-
ist societies create an atmosphere that is more favorable to pollution,
for it is only as men’s wealth increases that they believe that they can
afford the reduction in per capita output that pollution-control
usually involves. Are the Soviets really that poor? Yes. Goldman's
statistics on the availability of running water in homes gives us an
indication of the tremendous discrepancy between the productivity
of the respective economic systems. In 1960, only about 38 percent of
city housing in the Soviet Union had running water, and 35 percent
had sewers.”! By the late 1960’s, only 50 percent of the Soviet Union’s
urban homes had running water that was supplied by a central com-
munity source, as compared to 70-75 percent of U.S. citizens. Most
other Americans had electrically operated water pumps for their
homes’ running water; these are unheard of in the USSR.”

18. Philip R. Pryde, Conservation in the Soviet Union (Cambridge: At the University
Press, 1972), pp. 162-63.

19, Thid., p. 163.

20. Lbid., p. 165

24. Goldman, Spoils, p. 106.

22, [id., p. 107.
