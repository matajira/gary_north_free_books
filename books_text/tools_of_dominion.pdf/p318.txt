310 TOOLS OF DOMINION

Taking a Rebellious Son to Court

At the beginning of this chapter, I raised the question of the par-
ents’ willingness to take a rebellious son to court. Would they do this
if the death penalty were inescapable upon his conviction? Probably
not, The key question then is this: Is the death penalty absolutely re-
quired by the pleonasm of execution? The point I have tried to make
in this exposition is that this pleonasm applies only in cases where the State
is authorized to initiate the prosecution, i.e., in cases where there is no
earthly victim who can bring charges. This is not the situation in
cases involving a rebellious son. Parents can and must bring their
son before the civil authorities and complain about his conduct. God
requires them to bring him to the civil court. The judges would then
enforce a penalty specified by the parents, although they might first
recommend an appropriate penalty. The son would obey his parents
far more readily in the future, since he would know that the parents
could take him back and insist on escalating penalties up to the death
penalty if he committed similar infractions again. This fear would
reinforce the parents’ authority in the home.

What if they refuse to bring a formal charge against their rebel-
lious son? Then they have implicitly subsidized evil behavior. They
have implicitly sanctioned it. They know that they are risking the pos-
sibility that he will become an incorrigible adult. If he does, they will
lose him anyway. Better to bring him before the civil court early.
Better to obey God. Better to avoid God’s sanctions against the fam-
ily for the parents’ refusal to obey. The son may learn fear of the civil
court even though he has no fear of the family court.

If they bring him several times, the court will undoubtedly rec-
ommend increased sanctions. He has been identified as an incorrigi-
ble youth. The day that he commits a crime against someone outside
his family, the court will be able to demonstrate to the victim that le-
niency is no solution, that this man is a habitual criminal, Thus, by
allowing parents to insist on the death penalty, but by also allowing
them to be lenient, God encourages parents to identify rebellious
sons before the latter become incorrigible criminals. The court can
take steps to enforce parentally recommended sanctions before it is
too late.

This law, Rushdoony perceptively argues, is a law against the
development of a professional criminal class. “But the godly exercise
of capital punishment cleanses the land of evil and protects the right-
