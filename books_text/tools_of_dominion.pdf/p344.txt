336 TOOLS OF DOMINION

dent aliens, but the purpose of this practice was primarily cove-
nantal: bringing slaves of demon-possessed cultures mto servitude
under Hebrew families that were in turn under God.

Once the New Testament gospel became an international phe-
nomenon that spread outward from local churches rather than from
a central sanctuary in Jerusalem, there was no longer any need to
bring potential converts into the land through purchase. Jesus com-
pletely fulfilled the terms of the jubilee law, including the kingdom-
oriented goals of the imported slave law. He transferred the kingdom
from the land of Israe} to the church international: “Therefore say I
unto you, The kingdom of God shall be taken from you, and given
to a nation bringing forth the fruits thereof”? (Matt. 21:43).%* He
abolished the jubilee’s land tenure laws, as well as the slave-holding
laws associated with the land of Israel as the exclusive place of tem-
ple sacrifice and worship.

Adoption

Nevertheless, in principle there rernains a modern Christian prac-
tice that resembles the Old Testament jubilee slave law. It is the prac-
tice of adoption, Christians pay lawyers to arrange for the adoption of
infants whose pagan parents do not want them. This is true household
adoption rather than permanent slavery, but biblical law requires
children to support parents in their old age, so the arrangement is not
purely altruistic. ‘The practice of adoption is governed by civil law in
order to reduce the creation of a market for profit,®) therefore discour-
aging the kidnapping of infants, but the economics of modern adop-
tion are similar to the Old Testament practice of buying children from
resident aliens. Adoption is a very good practice. Children are bought
out of slavery inside covenant-breaking households.

Rushdoony refers to kidnapping as “stealing freedom.”5° He

34. Gary North, Healer of the Nations: Biblical Blueprints for International Relations (Ft.
Worth, Texas: Dominion Press, 1987), Introduction.

35. Actually, the adoption laws have created a profitable market for babies, but
only state-licensed lawyers and adoption agencies are legally allowed to reap these
profits. This is a legitimate licensing arrangement, similar in intent and economic
effect as the licensing of physicians: to control a potentially coercive market phenome-
non. Physicians control access to additive drugs, and lawyers and adoption agencies
control access to babies offered for adoption. This reduces the threat of kidnapped
babies. By centralizing access to the flaw of babies offered for adoption, the civil
government can more successfully impose restrictions on the market for babies by
guaranteeing that parents make the decision to supply this market, not kidnappers,

36. Rushdoony, The institutes of Biblical Law (Nutley, New Jersey: Craig Press,
1973), p. 484,
