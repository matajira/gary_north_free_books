Feasts and Citizenship 847

So, universal suffrage is the political demand of those who bear
no marks of ritual subordination to God. Biblically, the right of all
Christian women to vote is clear from the meaning of circumcision
and baptism. The right of all women to vote is denied by the same
law that denies the right of all men to vote: the law that authorized
circumcised men to attend Passover. ‘A foreigner and an hired ser-
vant shall not eat thereof” (Ex. 12:45),

Conclusion.

The Old Testament laws of the feasts specified that the judges of
Israel in the broadest sense had to appear before the Lord in Jeru-
salem three times a year. This reminded them of the magnitude of
their blessings: a court appearance in the presence of the King of
heaven. It also reminded them that they were under this King’s au-
thority judicially. If they disobeyed this law, they were brought
under condemnation: expulsion from the congregation of the Lord.
This meant the removal of the condemned man’s office of judge.

Regular rituals of covenant renewal in the house of God were basic
to the exercise of citizenship in the Old Testament. This is equally
true in the New Testament. The New Testament covenant mark of
baptism and the New Testament feast of the Lord’s Supper have re-
placed the Old Covenant’s mark of circumcision and Passover.

Women now have the mark of the covenant placed directly on
them. Because women receive the mark of the covenant in baptism,
they are required to participate in the ritual meal of covenant re-
newal: the Lord’s Supper. This becomes their legal title to access to
the civil office of judge.*? With respect to civil office, “There is neither
Jew nor Greek, there is neither bond nor free, there is neither male
nor female: for ye are all one in Christ Jesus” (Gal. 3:28). But this
cannot mean that today there is no civil covenant. The civil cove-
nant is an inescapable concept. It is never a question of “civil cove-
nant vs. no civil covenant.” It is always this question: “Which civil
covenant, under which God?”

The Hebrews were required to give the firstfruits to God. He was
the owner of the land. He was entitled to his percentage of the land's

32. Aguin, I am not arguing thal women were not permitted to exercise judicial
authority in Old Covenant Israel. I am making it clear, however, that there is still a
covenant mark of judicial subordination, and this mark must be received by anyone
who claims citizenship, meaning rulership, in a biblical commonwealth. It was received
representatively by women in the Old Covenant through their male relatives

 
