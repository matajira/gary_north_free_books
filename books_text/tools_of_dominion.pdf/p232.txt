224 TOOLS OF DOMINION

of light. Thus even in him is something prophetic of salvation. The
fault of Adam was disobedience to spoken law; but disobedience to
arbitrary spoken decree, to unreasoning command, what is that but
in essence obedience to the unspoken command of intelligence, and
what is that but the soul of goodness?”?> What God was nat allowed
to do in history in His name—bring covenant sanctions — the State
was expected to do in the name of universal humanity.”* The black
slave became a tool in the statist plans of the North’s Republican pol-
iticians. Congressman William D. (“Pig Iron”) Kelley of Pennsyl-
vania announced this messianic humanist vision: “Yes, sneer at or
doubt it as you may, the negro is the ‘coming man’ for whom we have
waited.” Frothingham recalled in 1875 the messianic viewpoint of
his theological peers during the War: “The army of the North was to
them the church militant; the leader of the army was the avenging
Lord; and the reconstruction of a new order, on the basis of freedom
for mankind, was the first installment of the Messianic Kingdom.’
What should have been a biblical moral crusade against illegitimate
lifetime chattel slavery became a humanist moral crusade against all
forms of private, profit-seeking servitude. The result in the twentieth
century has been the advancement of universal servitude to the State.

Protecting the Weak

The wife and children need lawful protection. They retained
their lawful protection, either from the master or from an indus-
trious, now future-oriented former bondservant, whether we are
speaking of voluntary permanent servitude of the ex-bondservant
husband or their purchase by him through the payment of a redemp-
tion price. But the hushand would probably have retained little capi-
tal after having paid 10 buy freedom for his family, Nevertheless, it
was his time orientation and demonstrated industriousness that were para-
mount for the subsequent protection of his family, not his remaining
accumulated savings. This was also true, of course, with the bride
price. A young man would probably give most of his capital to his
father-in-law at the time of the marriage (although the father-in-law

25. Ovlavius Brooks Frothingham, The Religion of Humanity (New York:
Puinam’s, 1875), pp. 299-300; cited in Rushdoony, Nature, p. 89.

26. See especially the book by Unitarian Moncure D. Conway, The Rejected Stone;
or, Insurrection vs, Resurrection in America (Boston: Walker, Wise, 1862).

27. The Old Guard, vol. I, no. IX (Sept. 1863), p. 240; cited in Rushdoony, Nature,

. 83.
a Frothingham, The Religion of Humanity, p. 20; cited in Rushdoony, idem.
