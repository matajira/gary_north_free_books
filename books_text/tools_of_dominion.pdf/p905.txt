The Economies of the Tabernacle 897

An Eschatology of Victory

The people’s economic contributions in constructing the taber-
nacle served as a ritual means for them to testify to an eschatology of
victory. First, their craftsmanship was an affirmation of perma-
nence. Second, their labor on the tabernacle was an affirmation of
history. Each man’s contribution would be seen by later generations
and be appreciated, so long as the community retained its cove-
nantal faithfulness to God. Those who would come later would look
back and be thankful to those who had gone before. Finally, the tab-
ernacle would replace the places of worship in the various cities of
Canaan. The Canaanites would surely be defeated—an affirmation
of the coming military conquest of Canaan. God would bring judg-
ment against their enemies. This pointed to God as cosmic Judge,
the fourth aspect of the biblical covenant.!>

The tabernacle was important in reinforcing the doctrine of the
covenant. This covenant joined the tribes together into one people.
The covenant also extended through time, linking the fathers in the
wilderness with the sons who would occupy the promised land. ‘The
covenant meant continuity over time, point five of the biblical cove-
nant,!° and the tabernacle symbolized this future-orientation.

The importance of symbols for society should not be disre-
garded, Symbols will always exist; the issue is not “symbols vs. no
symbols”; rather it is a question of which symbols and whose symbols.
Symbols are an inescapable concept, whether linguistic, musical, ar-
chitectural, or whatever. Men need to sacrifice something of value in
order to affirm their deeply felt commitments. Men do not choose
wedding rings made of iron or brass to give to their wives. If they are
committed to orthodox worship, they should prefer beautiful build-
ings to churches that resemble large shoe boxes.

Architecture and Culture

Architecture is closely linked to culture. The tabernacle revealed
the centrality of the covenant in Hebrew culture. It was in terms of

 

is typical of someone who has read far more than he can digest intellectually — to the
extent that liberal Old Testament studies can he digested inteHectually at all
Generally, they are fit only for ingestion and rapid regurgitation in doctoral disserta-
tions and journal articles. {t never ceases to amaze me how readily liberal theolog-
jans return to their regurgitations.

15. Sutton, That You May Prosper, ch. 4.

16. Tbid., ch. 5
