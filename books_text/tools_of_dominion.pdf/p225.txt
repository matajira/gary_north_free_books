Servitude, Protection, and Marriage 217

he was therefore less able to fulfill the terms of the dominion cove-
nant on his own initiative.

What about an indentured servant's children? The law did not
permit law-abiding Hebrews to become involuntary lifetime ser-
vants to other Hebrews. A Hebrew could serve another Hebrew or a
resident alien for up to 49 years, and he could become a member of
the household through the pierced ear ritual, but nothing is said
about the bondservant’s children. Nothing needed to be said; the de-
cision to become a servant, or even enforced servitude to repay a
debt or make restitution, did not bind a man’s children beyond the
age of their maturity, for they were not permitted to be enslaved
without their consent. Thus, it should be clear that the children of
the released manservant, upon marriage for daughters or upon
reaching the age of 20 for sons (Ex. 30:14), would have gone free.
Presumably, an unmarried daughter who reached age 20 would have
returned to her father’s house or to her oldest brother’s house, unless
she, too, chose to become a lifetime servant in the master’s house.
Adult children no longer would have been in need of the legal pro-
tection of the master.

The wife, having married in terms of the servant status of her
husband, in effect had already become a voluntary lifetime servant to the
master, unless her husband came and redeemed her, Either she served
her husband or her husband’s former master, who remained her cov-
enantal father until the bride price was paid.

The question arises, does the master own her future productivity,
or does it belong to her husband? Maimonides wrote: “Though the
master must support the wife and the children of his slave he is not
entitled to the proceeds of their work, Rather do the proceeds of the
wife’s work and the things she finds belong to her husband.”* Then
what would be the economic incentive for a master to give the wife to
the bondservant? He docs not escape the legal and economic respon-
sibilities of supporting her, yet he loses her productivity, which is
transferred to the bondservant. Only if the master could escape the
costs of supporting her would such a transaction have made sense.
But the whole justification of this law regarding wives of bondser-
vants is that it was the master’s status as the provider of her protec-
tion that made it mandatory that she and the children remain with
him upon her husband’s departure. Because the responsibilities asso-

15. Maimonides, Acquisition, “Slave Laws,? Chapter Three, Section Two, p. 254.
