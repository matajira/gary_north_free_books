Proportional Restitution 527

Ford’s Attorney General (1974-76), pinpointed the origin of the peni-
tentiary: the ideal of the savior State. “While the existence of jails
dates back to medieval times, the idea of penitentiaries is modern —
indeed, it is American. Largely it is the product of the Quaker no-
tion that if a wrongdoer were separated from his companions, given
a great length of time to think about his misdeeds, and with the help
of prayer, he would mend his ways. This late-18th-century concept
was the beginning of what has come to be known as the rehabilitat-
ive ideal.”"! Here is the great irony: it was Quaker theology that led
both to the freeing of the slaves and the imprisoning of criminals
whose productivity should be put into service of their victims. Prior
to the rise of Quaker jurisprudence, Roger Campbell reports, “Mas-
sachusetts law in 1736 provided that a thief should be whipped or
fined for his first offense. The second time he was apprehended and
proven guilty of that crime he would be required to pay three times
the valuc of the property stolen to the victim and was forced to sit on
the gallows for one hour with a rope around his neck. On the third
offence, the trip to the gallows was for real.”*?

Restitution and Deterrence
We are required by God always to begin our analysis of any
problem with the operating presupposition of the theocentric nature
of all existence. Modern jurisprudence refuscs to begin with Ged. It
begins with man and man’s needs, and generally progresses to the
State and the State’s needs. This is why modern jurisprudence is in
near-chaos. It is also why the court system is in near-chaos.**

 

Deterring God's Wrath in History

Whenever we speak of deterring crime, we must speak first of the
deterrence of God’s wrath against the community because of the
courts’ unwillingness to impose God’s justice within the community.
The civil government is required by God to seek to deter crimes be-

41. Edward H. Levi, speech at the dedication ceremony for the Federal Bureau
of Prisons Detention Center, Chicago, [linais, October 15, 1975; cited in Roger F.
Campbell, Justice Through Restitution: Making Criminals Pay (Milford, Michigan: Mott
Media, 1977), p. 63. By far, the best historical account of this transition frum town
punishments to the state penitentiary system is David J, Rothman’s prize-winning
study, The Discovery of the Asylum, Social Order and Disorder in the New Republic (Bostan:
Litde, Brown, 1971).

42. Ibid., p. 64.

43, Macklin Fleming, The Price of Perfect Justice (Now York: Basic Books, 1974).
