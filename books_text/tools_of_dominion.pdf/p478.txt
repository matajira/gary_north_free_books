470 TOOLS OF DOMINION

ful above all things, and desperately wicked: who can know it?” (Jer.
17:9). His answer was clear: “I the Lorn search the heart, I try the
reins, even to give every man according to his ways, and according
to the fruit of his doings” (Jer. 17:10). The human judge can make
causal connections based on public evidence, but he cannot search
the defendant’s heart. Any assertion to the contrary necessarily in-
volves an attempt to divinize man, and in all likelihood, divinize
man’s major judicial representative, the State.

The Economies of Negligence

We know from the text that the ox’s owner had been warned
about the dangerous ox, yet he did nothing visibly to restrain it.
Why would an owner neglect a warning from someone else regard-
ing the threat of his ox to others? There are several possible reasons.
First, he may not trust the judgment of the person bringing the
warning. The beast may behave quite well in the owner’s presence.
Is he to trust the judgment of a stranger, and not trust his own per-
sonal experience? But once the warning is delivered, he is in jeopardy.
If the beast injures someone, and the informant announces publicly
that he had warned the owner, the owner becomes legally liable for
the victim’s suffering.”

Second, the owner may be a procrastinator. He fully intended to
place restraints on the ox, but he just never got around to it. This
does not absolve him from full personal liability, but it does explain
why he failed to take effective action.

Another reason for not restraining the ox is economics. It takes
extra care and cost to keep an unruly beast under control. For exam-
ple, over and over in colonial America, the town records reveal that
owners of pigs, sheep, and cattle had disobeyed previous legislation
requiring them to pen the beasts in or put rings in their noses.
Apparently, the authorities were unable to gain compliance, for this
complaint was continual and widespread throughout the seven-
teenth century.” The costs of supervising the animals or maintain-
ing fences in good repair were just too high in the opinion of count-
less owners. Even putting a ring in the beasts’ noses, making it easier

29. Because a serious penalty could be imposed on the liable owners, the infor~
mant would have to have proof that he had, in fact, actually warmed the owner of the
beast’s prior misconduct. Otherwise, the perjured testimony of one man could ruin
the owner of a previously safe beast which then injured someone.

30. Carl Bridenbaugh, Cities in the Wilderness: ‘The First Century of Urban Life in
America, 1625-1742 (New York: Capricorn, [1938] 1964), pp. 19, 167, 323.
