1208 TOOLS OF DOMINION

law, law in terms of God’s law, absolute right and wrong. Neither majority
nor minority wishes are of themselves right or wrong; both are subject to
judgment in terms of the absolute law of God, and the largest majority can-
not make valid and true a law contrary to the word of Gad, All man’s law-
making must be in conformity to the higher law of God, or it is false.

A fourth aspect of man under law is that law means true order as justice,
The law is justice, and it is order, godly order, and there can be neither true
order nor true law apart from justice, and justice is defined in terms of
Scripture and its revelation of God’s law and righteousness, The law cannot
be made more than justice. It cannot be made into an instrument of salva-
tion without destruction to justice. Salvation is not by law but by the grace
of God through Jesus Christ.

The issue is justice, not salvation. So, why does he now raise the
spurious issue that the Constitution “can make no man nor nation
good; it is not a moral code”? This is utter nonsense; every law-order is
a moral code, This had been Rushdoony’s refrain for 30 years! As he
wrote in the Institutes, there is “an absolute moral order to which man
must conform.”® He insisted therefore that “there can be no toler-
ance in a law-system for another religion. Toleration is a device used
to introduce a new law-system as a prelude to a new intolerance.”*
In this sentence, he laid the theological foundation for a biblical cri-
tique of the U.S. Constitution as a gigantic religious fraud, a rival
covenant, “a device used to introduce a new law-system as a prelude
to a new intolerance,” which it surely was and has become, But he
has been blinded for 30 years by his love of the Constitution. In a
showdown between his theocratic theology and the U.S. Constitu-
tion, he chose the Constitution.

He says that it will do no good for Christians to appeal to the
Constitution. “The Constitution can restore nothing, nor can it
make the courts or the people just.” The courts are the enforcing
arm. of the Constitution, yet it supposedly cannot make the courts
good. Of course it cannot; but a Constitution can and must prohibit
evil, lawless decisions by lower courts. Tt must reverse all lower court de-
cisions that are not in conformity to the fundamental law of the land.
This is the doctrine of judicial review. This is the whole idea of

62. Ibid., p. 143.

63. Zbid., p. 144,

64. Rushdoony, Znstttutes, p. 18.

65. Tid, p. 5.

66. Rushdoony, “U.S, Constitution,” p. 39.
