344 TOOLS OF DOMINION

standers (Ex. 21:22-25). It can destroy property. When a death or
serious injury is involved, a duel can lead in some societies — espe-
cially those that place family status above civil law—to an escalation
of inter-family feuding and blood vengeance.

The premise of the duel or the brawl is the assertion of the exist-
ence of zones of judicial irresponsibility. Men set aside for themselves a
kind of arena in which the laws of civil society should not prevail.
There may or may not be rules governing the private battlefield, but
these rules are supposedly special, removing men from the jurisdic-
tion of civil law. The protection of life and limb which is basic to the
civil law is supposedly suspended by mutual consent. “Common”
laws supposedly have no force over “uncommon” men during the
period of the duel. Somehow, the law of God does not apply to private
warriors who defend their own honor and seek to impose a mutually
agreed-upon form of punishment on their rivals.

But the laws of God do apply. “The Bible does not permit the use
of force to resolve disputes, except where force is lawfully exercised
by God’s ordained officer, the civil magistrate. To put it another way,
the Bible requires men to submit to arbitration, and categorically
prohibits them from taking their own personal vengeance (Rom.
12:17-13:7)."1

An obvious implication of the biblical law against dueling is the
prohibition of gladiatorial contests, which would include boxing. A
boxer who kills another man in the ring should be executed. Another
implication is the necessity of rejecting the notion of a “fair fight.”
There is no such thing as a fair fight. Flight is almost always prefer-
able to private fighting, but where fighting is unavoidable, it should
be an all-out confrontation. Should a person “fight fair” when his
wife is attacked? Should women under attack from a man “fight fairly”?
The answer ought to be clear."! Thus, the code of the duel is doubly
perverse: first, it imputes cowardice to a man who would seek to
keep the peace by walking away from a challenge to his honor, sec-
ond, it restricts a man’s lawful self-defense to a set of agreed-upon
“rules of the game.” Fighting is not a game; it is either an evil asser-
tion of personal autonomy or else a necessary defense of life, limb,
and perhaps property.

10. James B. Jordan, The Law of the Covenant: An Exposition of Exodus 21-23 (Tyler,
Texas: Institute for Christian Economics, 1984), p. 110.
11. Ibid., p. 112.
