What Are the Case Laws? 105

the daughters of Zelophehad for inheritance rights in the absence of
male heirs.”4*

He gives some good reasons for this contrast. One is that the
climate of Palestine is inhospitable to the survival of parchment and
papyrus. In contrast, Mesopotamian collections of tablets and sealed
rolls have been found in profusion during archeological digs. “The
bulk of the written remains from ancient Mesopotamia is accidental
in the sense that all of it has been recovered by legitimate or illicit ex-
cavation.”45 It may be that attempts to impose coherence on the “in-
coherent assemblage of data of widcly disparate dates” may be mis-
leading. In the late 1970's, I heard a lecture by the editor of the Bib/i-
cal Archaeology Review, David N. Freedman, who informed us that only
about 10 percent of the tablets for any culture or archeological site
are ever translated. There is always another discovery to catch the
fancy of the archaeologists, and they eventually grow bored with the
translation of seemingly similar commercial records and tax records.
Furthermore, there are not that many specialists in the ancient lan-
guages, and fewer still who are social or legal theorists. They go on
to new tablets instead of spending a lifetime interpreting the tablets
they have already translated.

Finkelstein does not emphasize these more technical aspects of
the differences between biblical and Near Eastern texts. The really
important difference in the rate of survival, he says, was theological.
“The Israelite nation was bound by an ancient and sacred pact with
its deity to organize and conduct its life, both personally and institu-
tionally, in accordance with the divinely ordained prescriptions,
Directly or indirectly, whatever is included in the Old Testament by
way of ‘historical’ information is meant to reinforce that central
thesis; the vicissitudes of the people through the millennium embraced
by the biblical time span serve as hardly more than a barometer of
the nation’s fidelity to, or perfidy against, its pact with Yahweh.
Everything is subordinated to this overriding purpose, and whatever

44, J. J. Finkelstein, The Ox That Gored (Philadelphia: American Philosophical
Society; 1981), p. 42. Boecker writes: “There are nu OT ‘rules of cour,” We must re-
member above all that in its basic message the OT is not interested in conveying a
picture of legal processes in Ancient Israel. Its concern lies elsewhere. Iis purpose is
to report God’s activity in and with Israel and to demonstrate Israel’s answer to this
activity.” Hans Jochen Boecker, Law and the Administration of Justice in the Old Testament
and Ancient East, translated by Jeremy Moiser (Minneapolis, Minnesota: Augsburg,
[1976] 1980}, p. 28.

45, Finkelstein, iid., p. 44.
