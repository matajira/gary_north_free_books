766 TOOLS OF DOMINION

the integrity of the predictability of the law. It is an attempt to make
law the plaything of full-time legal technicians rather than the jury’s
application to trial court evidence of general laws that can be under-
stood by the vast majority of those who are covenantally under its
provisions. This is why judges are to be selected in terms of their
reputation for honesty (Ex, 18:21). Ethics, not mental gymnastics by
highly trained legal specialists, is God’s screening system for judges.
This is also why God required that His law be read publicly to all
residents of Israel during the year of release, once every seven years
(Deut. 31:9-13). He wants people to know in advance what He re-
quires of them ethically.

Judges and Justice

Law enforcement is ideally to be immune to a judge’s personal
connections to the accused, whether pro or con. Enemies deserve
justice. So do close relatives. All men deserve justice, meaning the
impartial (but never impersonal) application of biblical law to every
aspect of their lives—judgments imposed not just by the State, or
even primarily by the State, but by all forms of government, includ-
ing self-government, The emotions of the judge are not the issue; ex-
ternal justice is the issue. An emotion-filled judge is commanded by
God to provide the same impartial judgment which would be
rendered by a disinterested judge. The issue is not emotion; the issue
is self-government under biblical law. God is emotional. He fates
covenant-breakers as passionately as He loves covenant-keepers.
How else could He create the cternal lake of fire for His enemies?
Ont of His love for them? Hardly. Why else would He recommend
that we do good deeds to our enemies, so that we might heap coals of
fire on their heads (Rom. 12:20)? David could say, “Do not I hate
them, O Lorp, that hate thee? And am I not grieved with those who
rise up against thee? I hate them with perfect hatred: I count them
mine enemies” (Ps, 139:21-22). Nevertheless, to render anything less
than impartial justice is to impugn the character of both the law and
the Law-giver.

The doctrine of the atonement affirms this principle of impar-
tiality despite emotion. The demands of the law must be met. God
the Father spared not His own Son, despite His emotional in-
volvement with His Son. Emotions may be present in certain judi-
cial cases, but they are not to influence the application of God's stan-
dards to these cases.
