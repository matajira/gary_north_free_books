946 TOOLS OF DOMINION

so forth. The question is never elite or no elite. It is always a ques-
tion of which elite. It is a question of which sovereign agent. The
Bible is clear: God is completely, absolutely sovereign over the crea-
tion, and men are subordinately, inescapably responsible for their
actions. Thus, the goal of covenant-keepers is to work toward a so-
cial order in which every institution reflects this dual sovereignty,
absolute and delegated. It is the creation of an entire world order
that prays, “Thy kingdom come, Thy will be done in earth, as it is in
heaven” (Matt. 6:10).

A subset of this broad social goal is politics. Politically, the only
legitimate long-term biblical goal is the creation of a worldwide theo-
cratic republic.* It is the creation of a bottom-up political order
whose civil courts enforce the law of God, and whose people rejoice,
not because such a law-order is natural but because it is supernatural.

The Restoration of Covenant Order

The primary social function of civil law is to persuade God to
withdraw His negative sanctions. The State acts as God’s agent in
imposing sanctions against sin, This is the biblical rationale of civil
laws against so-called victimless crimes. Obviously, this purpose
relates to the hierarchical nature of all society: the society is under
God, meaning under His temporal sanctions.

There is also a secondary goal of civil law: the restoration of so-
cial order among men. This, too, is hierarchical. If a person owns a
piece of property, then he exercises dominion over it in terms of his
subordination to God. He acts as God’s agent in a hierarchical sys-
tem of ownership, which Christians call stewardship. When a criminal
or negligent person invades this hierarchical system of ownership,
God calls the civil magistrate to defend His interests, and therefore
also His steward’s interests. The system of justice in the Bible is
geared to restoration of the original God-assigned hierarchical order,

The issues of crime and punishment are inescapably questions of
the appropriate hierarchy. The victim has been victimized by some-
one who has asserted a judicially illegitimate authority over him.
The criminal in some way invaded the victim’s legitimate, God-
given sphere of personal responsibility. The criminal subordinated

23. Gary DeMar, Ruler of the Nations: Biblical Blueprints for Government (Ft. Worth,
Texas: Dominion Press, 1987); Gary North, Healer of the Nations: Biblical Blueprints for
International Relations (Ft, Worth, Texas: Dominion Press, 1987).
