870 TOOLS OF DOMINION

version of linear history, They settle for slow decay rather than cycles.
The goal is to escape the judgment of God. All of them prefer to avoid
the truth; for covenant-breakers, the growth process will be cut short.
A new downward cycle will triumph, they argue. Entropy will triumph.
Anyway, something will triumph, but not the God of the Bible.

Tn the 1960's and 1970's, a new phenomenon hit the academic and
intellectual world: defenders of no-growth economics.*? Prior to this,
virtually all professional economists had been concerned with foster-
ing economic growth.* This was part of an overall attack on growth
in general.*! Population growth was the primary target of these
attacks.>? From 1965 through 1976, governments had poured over a
billion and a quarter dollars into programs promoting worldwide
population control, and the Rockefeller Foundation and Ford Foun-
dation added another $250 million. All of this public concern over
the population explosion was virtually an overnight phenomenon,
beginning around 1965. All of it sprang from anti-Christian roots.+

Rushdoony’s comments on pagan antiquity’s hostility to change
is applicable to the zero-growth movement of the modern humanist
world: “The pagan hatred of change was also a form of asceticism,
and it is present in virtually all anti-Christianity. The hatred of
change leads to attempts to stop change, to stop history, and to
create an end-of-history civilization, a final order which will end
mutability and give man an unchanging world. Part of this order in-

49, The most prominent academic economist in the no-growth camp is E. J.
Mishan: The Costs of Economic Growth (New York: Praeger, 1967); The Economic
Growth Debate: An Assessment (London: George Allen & Unwin, 1977).

50. Bert F. Hoselitz (ed.), Theories of Hconamic Grouth (New York: Free Press,
1960). This book traces economic theories on growth back to the seventeenth century.

51. Dennis Meadows, ef al,, The Limits to Growth (New York: Universe Books,
1972). See also Mancur Olson and Hans H. Landsberg (eds.), The No-Grozwth Society
(New York: Norton, 1973).

52. Paul Ehrlich, The Population Bomb (New York; Ballentine, 1968). This be-
came a runaway best-seller, Sce also Gordon Rattray Taylor, The Biological Time
Bomb (New York: World, 1968). These books are in contrast to an earlier, more re-
strained discussion of population issues: Philip M. Hauser, The Population Dilemma
(Englewood Cliffs, New Jersey: Prentice-Hall, 1963). Then came a Presidential
commission report, Population and the American Future (New York: Signet, 1972), a
popular paperback version of a government report. The story was the same: the
danger of population growth,

53. Julian Simon, The Ultimate Resource (Princeton, New Jersey: Princeton
University Press, 1981), p. 292.

54, See my critique in Moses and Pharaoh, Appendix B: “The Demographics of
Decline.” See also James A. Weber, Grow or Die! (New Rochelle, New York: Arlington
House, 1977).
