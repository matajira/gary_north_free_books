692 TOOLS OF DOMINION

amiliennialists, their defense of creationism has been based on hu-
manistic science’s theory of entropy (the second law of thermo-
dynamics), which resis on the inescapability of God’s curse of the
cosmos in Genesis 3:17-19, rather than the doctrine of Christ’s defini-
tive restoration of all things by His resurrection, and the progressive
(though imperfect) restoration of the pre-Fall world through the
power of the Holy Spirit and the extension of biblical law. ‘Thus,
modern Bible-affirming Christians have found it difficult to refute by
an appeal to the Bible the modern messianic quest for socialistic per-
fection.*° They cannot successfully defend the idea of the free market
economy as an institutional manifestation of the fourth point of the
biblical covenant, the principle of judgment-sanctions: blessing and
cursing, profit and loss.3!

 

The Free Market’s Auction Process

The pricing principle enunciated by the villain in Frank Norris’
turn-of-the-century novel, The Octopus, is a morally valid principle
for commercial transactions: “All the traffic will bear.” At an auction,
the highest-bidding buyer gets the sought-for asset. This principle
reigns at every auction: the high bid wins. Yet, there is hardly any
principle of capitalism that is more hated, and more criticized, than
this one. The only one that receives greater criticism is the capitalist
principle of economic inequality, especially inequality of inheritance
at birth. But the right (legal immunity) of unequal inheritance is the
legal manifestation of point five of the biblical covenant, inheritance-
disinheritance.*? In short, capitalism is hated because visible institu-
tional manifestations of God’s covenant are hated.

The free market economic system is essentially a giant auction. If
potential buyers at an auction were repeatedly frustrated when low-
bidding competitors were favored by the auctioneer, it would even-
tually destroy the auction. Similarly, if sellers of goods and services
in a free market economy were unwilling to honor this principle of
“high bid wins” most of the time (though not necessarily in every casc),
they would destroy the market as an institution for producing and
allocating scarce economic resources, By refusing to honor the “high

30. Gary North, Is the World Running Down? Crisis in the Christian Worldview (Tyler,
Texas: Institute for Christian Economies, 1988).

31. Gary North, Inherit the Earth: Biblical Blueprints for Economics (Ec, Worth,
Texas; Dominion Press, 1987), ch. 9.

32. Sutton, That You May Prosper, ch. 5.
