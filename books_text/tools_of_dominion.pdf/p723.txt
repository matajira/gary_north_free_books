The Prohibition Against Usury 715

the loan’s contractual arrangement that the borrower is servant to the
lender. If he cannot repay, he will go into bondservice until the next
sabbatical year, or until his debt is repaid, whichever comes first.

How would the civil magistrate in Israel know which kind of loan
was in force, commercial or charitable, and therefore whether interest
was valid or illegal? By examining the nature of the loan’s collateral.
Ifa loan went to an individual who, if he should default on the loan,
would be placed in debt slavery, then this was a charitable loan gov-
erned by the provisions of Deuteronomy 15. This is why the year of
release applied to both kinds of servitude: debt servitude and bodily
servitude that arose because of a man’s default on a charity loan.

Furthermore, if it was a loan with the individual's cloak as secur-
ity, then it was also a zero-interest loan. The collateral described in
Exodus 22:25-27 insured little more than that the individual was a
local resident— he had to come to the lender to get it back each eve-
ning—and that the loan was temporary. (It also made multiple in-
debtedness more difficult.)? It would have been a very small loan.
This is clearly not a business loan. A business loan would have a
different kind of collateral: property that was not crucial to personal
survival on a cold night. If the borrower defaulted on a commercial
loan, he would forfeit the property specified in the loan contract. He
would not forfeit his freedom or his children’s freedom. In short, the
Old Testament biblical texts governing Jending specify that certain
kinds of loans would have certain kinds of collateral, and wherever
these forms of collateral appeared, the lender could not legally de-
mand an interest payment.

Biblical civil law is exclusively negative law— prohibiting evil
public deeds—not positive law, which enjoins the performance of
righteous public deeds, An example of this distinction is the enforce-
ment of the tithe: church courts can legitimately require voling
members to tithe as a condition of maintaining their voting church
membership; the State cannot legitimately require residents to tithe
to a church or other organization on threat of civil punishment.

Once the contract is made, the lender is placed under the limits
of the civil law. He may not extract interest from the borrower, even
a resident alien. But the borrower also is placed under limits: if he
defaults, he can be sold into bondservice. Each party is under lim-
its. Each has decided that this is a true poor loan situation. Each

 

9. Sec below: “Multiple Indebtedness,” pp. 738-40
