Intraduction 5

serve as a reliable guide to economic reconstruction than some fat
polemical tract published by the Maryknoll Order or. written by a
sociology professor at Wheaton College.

Tools of Dominion is a commentary, not a novel; it is a reference
work, not a catechism. It tries to accomplish a great deal: exegete
verses, describe how they applied in the Old Testament era, explain
why they should be applied today, and offer examples of how they
might be applied in practice. It is large because I want it to serve for
many years (preferably centuries) as one of the two key reference
works on specific applications of biblical law in economics and juris-
prudence; the other is not yet written: my commentary on Deuter-
onomy. I have decided to extract from this book separate sections
that deal with narrower problems, which I intend to publish under
separate titles: Clean Living: A Biblical View of Pollution, Victim’s Rights:
A Biblical View of Jurisprudence, and Slavery: A Biblical View. Neverthe-
Jess, I have retained the same information in this volume, simply be-
cause not every library will have the other three books plus this one
on its shelves. I want to make certain that readers of this comprehen-
sive volume have all of my arguments in front of them.

In some ways, I wish I could imitate Moses Maimonides, the
late-twelfth-century Jewish scholar. In defending the style of the
enormous output of his life’s literary work (he was also a full-time
physician to the Sultan in Cairo), including his monumental
fourteen-volume Code (the Mishneh Torah), he wrote: “All our works
are concise and to the point. We have no intention of writing bulky
books nor of spending time on that which is useless. Hence when we
explain anything, we explain only what is necessary and only in the
measure required to understand it, and whatever we write is in sum-
mary form... . Were I able to condense the entire Talmud into a
single chapter, I would not do so in two.” The problem with his con-
cise style is this: when we go to his Code (which is not a detailed com-
mentary, despite its huge length), time and again we cannot follow
his reasoning. It is not simply because we are gentiles living many
centuries later; learned contemporary rabbinical correspondents ex-
pressed this same dissatisfaction to him.’ It takes considerable expla-

6, Cited in Isadore Twersky, Introduction ta the Cade af Maimonides (Mishneh Torah)
(New Haven, Connecticut: Yale University Press, 1980}, p. 45.

7, Sce, for example, his lengthy reply to Rabbi Phinehas ben Meshullam, judge in
Alexandria: ébid., pp. 30-37. Twersky cites Rabbi Joseph Karo, the sixteenth-century
scholar and Kabbalist: “The generations that followed him could not understand his
works well . . . for the source of every decision is concealed from them. . . .” Twersky
then remarks: “To this day [1980], the quest for Mishneh Torah sources in unknown
Midrashim and Geonic responsa, variant readings, etc., continues unabated as one

of the main forms of Rabbinic scholarship.” /bid., p. 106.
