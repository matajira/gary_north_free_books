312 TOOLS OF DOMINION

House and Ice go on to say that “in Israel this penalty [execution]
was exacted for various crimes. . . .”* If they mean merely that in
Israel, the maximum sanction of execution could be required by the
victim in several capital crimes, then they are correct. If they mean
that in those cases where the State lawfully prosecuted in God’s
name as His designated representative, and where the pleonasm
“dying, he shall surcly die” was attached to the biblical sanction, then
they are also correct. If this is all they mean, however, then they
have not said anything very significant. They have not shown that
God restricted these judicial principles to Old Covenant Israel.

The judicial principle of a maximum allowable sanction for any given
crime was also in principle God’s requirement for the nations. With-
out this God-imposed judicial restriction, the State can lawfully be-
come all-powerful, messianic, and therefore demonic. There will
always be sanctions imposed by civil government. The only question
is: Whose law establishes the specified judicial limits of State-
imposed sanctions, God's or self-proclaimed autonomous man’s?

To answer, as House and Ice do, that it depends upon when and
where you live in God's world, is to abandon the concept of universal
biblical ethics and therefore also to abandon the principle of univer-
sally restricted civil governments. Any attempted distinction be-
tween the Old Covenant nations and Mosaic Isracl which is based
on a theory of differing judicial sanctions for the same civil crimes is
misguided. Civil sanctions are always specified by God because God
always wants limits on the State and always wants to see victims protected. In
other words, He always wants judicial limits on the pretensions of
autonomous man. God killed nations under the Old Covenant, just
as He kills New Covenant nations, because they failed to apply His
civil sanctions in history. If this was not the message which Jonah
brought to Nineveh, what was?

The principle of victim-imposed sanctions is also God's require-
ment for all nations in this New Covenant era, now that the death,
resurrection, and ascension of Jesus Christ, plus the sending of the
Holy Spirit and the creation of the church, have extended God's now-
resurrected law-order to the nations. The New Covenant is truly new;
its Bible-specified laws and sanctions have been universalized defini-
tively in history by the earthly ministry of Jesus Christ. The resurrec-
tion is behind us. Surely the sanctions of God’s law for the nations
are no less binding today than before Christ arose from the dead and

39. Idem.
