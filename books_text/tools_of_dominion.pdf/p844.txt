836 TOOLS OF DOMINION

there a man which had not on a wedding garment: And he saith unto him,
Friend, how camest thou in hither not having a wedding garment? And he was
speechless. Then said the king to the servants, Bind him hand and foot, and
take him away, and cast him into outer darkness; there shall be weeping and
gnashing of teeth. For many are called, but few are chosen (Matt 22:2-14),

There is no doubt that Jesus was referring here to Isracl. The
Pharisees understood His accusation. “Then went the Pharisees, and
took counsel how they might entangle him in his talk” (Matt. 22:15).

There were two crimes associated with the festivals of the king-
dom: 1) refusing to come when invited and 2) refusing to bear the ap-
propriate mark of subordination: in Israel, circumcision; in the par-
able, a wedding garment.’ It is an honor to be invited and a curse to
refuse to come. It is an honor to attend, but only those who have
subordinated themselves publicly to the heavenly King should dare
to enter His presence.

The annual festivals of Israel were mandatory for those males
who were under God’s ecclesiastical jurisdiction. These were mem-
bers of the congregation. The question then arises: If it was required
that every circumcised male attend the feasts, what were the sanc-
tions for non-attendance? Who imposed them?

What Kind of Negative Sanctions?

Ihave argued throughout this volume that biblical civil law does
not set forth positive injunctions to do good. It only enforces laws
against publicly evil acts, as defined by God’s revealed law, This law
of the compulsory feasts initially appears to be an exception to this
rule. It is not an exception. Because no negative sanction is men-
tioned in the various texts dealing with the required festivals, we
should initially conclude that this was not a civil law. Only if we can
derive appropriate civil sanctions by examining the nature of the
crime should we conclude that this was a civil law. I can see no ap-
propriate sanctions. There was no earthly victim of a crime who
could bring charges. There seems to be no appropriate fine to be dis-
tributed to some future victim of an unknown criminal, Whipping
seems inappropriate, since the crime is not a positive assault on pub-
lic morality.

It seems a likely inference that the appropriate negative sanction was
excommunication from the priestly congregation. By failing to attend the re-

12. This is clearly symbolic of baptism.
