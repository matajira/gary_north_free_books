908 TOOLS OF DOMINION

Why does Rushdoony make this unwarranted leap from an aton-
ing tabernacle payment during wartime to a permanent payment to
the tabernacle as a civil tax? Why doesn’t he see the enormous threat
to liberty involved in making the State a tax-collector in the name of
atonement? Why does he fail to recognize that if this was the only le-
gitimate tax in Old Testament Israel, that it would have created
either an ecclesiocracy or a political tyranny? If the atonement pay-
ment was in fact a tax, one collected by the tabernacle’s agents,
meaning Aaronic priests, to be doled out as they saw fit to the civil
authorities, then the church would inevitably be at the top of a single
civil pyramid. On the other hand, if the civil magistrates possessed
the authority to enter the tabernacle and collect the atonement pay-
ment, then the State would be at the top. Yet Rushdoony always
argues that there is no single church-State pyramid of power in a
biblical commonwealth; church and State are separate sovereign au-
thorities under God and God’s law.

Rushdoony’s Unstated Problem

His unstated problem is that he does not want to face an unplea-
sant reality: the Old Testament never specifically says anything about
what is proper for civil taxation, except in Samuel’s warning against
the king’s collection of as great a percentage of a person’s income as
10 percent (I Sam. 8). This is James Jordan’s conclusion.’ It is also
mine. If defenders of biblical law cannot point to any specific biblical
laws that govern civil taxation, an apparent gap in their whole her-
meneutic is exposed for all to see.

Rushdoony prefers not to face this problem directly, although he
clearly recognizes that it exists. “Commentaries and Bible dic-
tionaries on the whole cite no law governing taxation. One would
assume, from reading them, that no system of taxation existed in an-
cient Israel, and that the Mosaic law did not speak on the subject.”!®
If the Bible is truly silent here, then the theonomist is placed in the
seemingly embarrassing position of claiming that the Old
Testament’s law-order has specific guidelines and answers for all so-
cial and civil policy, yet he is unable to find explicit rules governing
what has become the central issue of civil sovereignty in the twenti-
eth century, namely, the legal sanction of tax collection. Yet apart
from Samuel's critique of the king’s collecting a tithe, the only

15. Jordan, Law of the Covenant, p. 239.
16. Rushdoony, Jnstitutes, p. 281.
