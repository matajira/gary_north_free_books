708 TOOLS OF DOMINION

People may choose not to obey God’s directive, of course. Poten-
tial lenders can simply refuse to make loans to brothers in distress,
and there is nothing in biblical law that allows the authorities to take
any kind of legal action against them. Poor people can only appeal to
a lender’s conscience. For another, lenders can get around the prohi-
bition in many ways, such as by unofficially requiring the borrower
to perform some sort of service, or requiring the borrower to buy
goods or services that the lender sells. (This latter restriction is a fa-
miliar requirement of certain kinds of U.S. government loans and
economic aid to foreign nations: they are required to buy goods from
U.S. companies.) Nevertheless, God’s law is clear: all such subter-
fuges are immoral, and the victims will cry out to God, who will hear
their complaints (Ex. 22:27).

Another product of this prohibition against usury would be polit-
ical pressures from lenders in a money economy to reduce prices by
reducing the money supply. If the money supply is stabilized, or
even lowered, this will tend to reduce prices. Thus, a return of the
same amount of gold, silver, or paper money will in effect grant lend-
ers increased wealth. They can buy a greater quantity of goods and
services when the loan is repaid. Should this political pressure fail to
achieve its goal, and should monetary inflation continue, then lend-
ers will prefer to loan goods rather than money, with repayment de-
nominated in goods of equal quality. They will at least regain an
equal quantity of goods that have appreciated in value (as denomi-
nated in the depreciating monetary unit).

The prohibition on usury clearly and absolutely prohibits interest
payments on all charitable loans to other Christians. This includes
loans to churches and other non-profit institutions that come to
Christians in the name of Christ. The church is not a business. The
Christian who loans the church anything, at any time, for which he
requires an extra amount in repayment, is violating the law against
usury. Any leader in a church or non-profit Christian organization
who encourages Christians to make interest-bearing loans to it is in-
volving its supporters in the sin of usury. This restriction on “church
bonds” is almost universally ignored by denominational leaders to-
day. They ignore the prohibition against usury. The Bible is clear on
this point: usury is a terrible crime (Jer. 15:10). The prophet Ezekiel
announced that it is actually a capital crime in the eyes of God, and
will not go unpunished (Ezek. 18:8-9, 13). Yet church and Christian
school leaders in almost every denomination can be found offering
