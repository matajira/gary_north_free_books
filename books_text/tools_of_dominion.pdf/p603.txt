Pollution, Ownership, and Responsibility 595

cally impotent public. This is especiaily true of the Soviet Union.%
Managers use the State’s resources to benefit their own careers,
which means meeting State-assigned production quotas. The subtle
pressures and rewards of private ownership are missing; socialist
plans are crude and focus on aggregate output. Little else matters to
the manager, except possibly laying up hidden reserves to barter
with or steal and then sell into the black market. He must make his
factory’s quota (plus a few percentage points more, to earn his
bonus). The environment suffers as a direct result.

Pollution is controlled by a combination of widespread private
ownership, and local and regional civil government enforcement of
Exodus 22:5-6. Socialist owncrship is guaranteed to produce pollu-
ause it places at the top of the list the goals of non-owning

 

tion be
factory managers.

Solutions to Pollution

The first step is to recognize that men are responsible for their actions.
The man who pollutes the environment in such a way that it in-
fringes on the way of life of his neighbors must be made to pay resti-
tution. He is responsible; he must pay.

There are always problems in applying this rule. Here are some
basic ones. First, it may be impossible to identify a single polluter as
the major source of pollution. An entire region may be filled with
polluting industries. In this case, the local civil government or gov-
ernments will have to begin to formulate general policies that en-
courage all polluters to reduce their polluting activities, even though
each polluter cannot be matched precisely with all those who are
harmed by the pollution,

This raises some very hard legal questions. The main one is that
of strict Hability.* If a plaintiff cannot prove that a specific polluter

 

83. See Appendi: “Pollution in the Soviet Union.”

84. Richard Epstein, an articulate defender of strict liability, contrasts his posi-
tion with what he calls a negligence theory of law. “I'he development of the common
law of tort has been marked by the opposition belween two major theories. The first
holds that a plaintiff should be entitled, prima facie, to recover from a defendant
who has caused him harm only if the defendant intended to harm the plaintiff or failed
to take reasonable steps to avoid inflicting the harm. ‘Ihe alternative theory, that of
strict liability, holds the defendant prima facie liable for the harm caused whether or
aot either of the two further conditions relating lo negligence and intent is satisfied.”
Epstein, A Theory of Strict Liability: Toward a Reformulation of Tort Law (San Francisco:
Cato Institute, 1980), p. 5. He distinguishes four cascs governing private tort (law

 
