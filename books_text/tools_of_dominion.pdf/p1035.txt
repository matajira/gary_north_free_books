Maimonides’ Code: Is It Biblical? 1027

ual relations with a small boy who is “less than nine years of age,”%”
as the annotator says?

Christians do not ask such questions today. Therefore, Jews do
not answer them. The fact is, virtually all modern Christian scholars
—at least those who publish—are completely unfamiliar with the
passages in the Talmud that I have cited in this essay, and Jews do
not try to defend such passages; they remain discreetly silent. There
has been a kind of implicit cease-fire agreement regarding the ethical
details of the Talmud, and a willingness on both sides to limit all dis-
cussions of the ethics of traditional Judaism and especially the
Talmud to general ethical principles that have been derived from the
Jess controversial passages. So, over the years, the Talmud has fallen
into the shadows, Most Jews do not read it any more. Yet it is only
here that we find a detailed account of what Paul calls “the traditions
of my fathers” (Gal. 1:14).

Concealment and Initiation

Jews for many centuries hid the Talmud from the eyes of gen-
tiles. They correctly surmised that Christian leaders would be shocked
and outraged if they thought that such teachings were the basis of
the autonomous civil legal order that Jews enjoyed through most of
medieval history. From time to time, the authorities ordered the
conliscation and burning of copies of the Talmud. Rabbi Trattner
provides a list of about two dozen of these edicts, from 1240 to 1757.%
But he misleads his Christian audience (his publisher, Thomas Nel-
son, published and still publishes predominantly Christian books)
when he offers these three reasons why Christian magistrates have
been so hostile to the Talmud in the past:

1, Since it forms the main teaching of the Jewish religion, it has been
regarded as the supreme obstacle in preventing Jews from being converted
to Christianity.

2. Since the Talmud interprets the Old Testament by reshaping ancient
Biblical laws to meet the needs of post-Riblical times, it has been charged
with the falsification of Scripture.

3. Since the Ya/mud is a non-Christian production, it has been accused
of harboring an evil and irreverent attitude towards Christ and the
Church.

97. Kethuboth ta.

98, Ernest R. Trattner, Understanding the Talmud (New York: Thomas Nelson &
Sons, 1955), pp. 200-1.

99. Ibid., p. 198.

 
