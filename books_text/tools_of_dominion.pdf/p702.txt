694 TOOLS OF DOMINION

making their voluntary exchanges. There is no way for judges to dis-
tinguish “oppressive” transactions from “fust barely oppressive”
transactions and “not quite oppressive” transactions by means of an
appeal to percentages, such as 8 percent profit per sale or {5 percent
profit on invested capital (both of which are at least 50 percent above
the normal rate of before-tax profits in the United States).°* To make
lawful judicial decisions, judges need moral constants; but economic
percentages change over time. The question, “How much of any-
thing should be universally illegal?”, has baffled moral philosophers
for millennia. Only in rare instances, such as the tithe of 10 percent,
does the Bible give a specific answer to a question regarding a legal
minimum percentage.

The Lawful Domain of Conscience

Conscience is a valid, though not exclusive, guide to individual
action. It is self-government which regulates the overwhelming majority
of all human actions, Men must not be burdened with unnecessary
guilt, nor should they become libertines, sinning against themselves
because somc other agency of government is not authorized by God
to step in and call a halt to their activities. The question is: What are
the proper standards for men to use in determining whether or not a
specific transaction is oppressive, biblically speaking?

The Bible mentions strangers, widows, the poor, and the father-

 

less as the representative examples of peaple who are easily ex-
ploited. In dealing with these people, what questions should the
sharp bargainer ask himself? What kinds of offers would be innately
immoral?

34, What is not understood by most Americans, as a poll taken annually by the
Opinion Research Corporation reveals year after year, is that the average rate of net
after-tax profit on sales in the United States is about 5 percent or less. See, for exam-
ple, “Public Attitudes Toward Corporate Profits,” ORC Report to Management (Aug.
1981). The average rate of before-tax profits on invested capital (excluding banks
and savings & loan associations) is around 10 percent. In 1964, the profit rate was
about 16 percent. This figure declined steadily in the United States, 1964-80, corre-
sponding to the coming of inflation and the vast expansion of the welfare State.
After taxes, of course, it is substantially less. See Dale N. Allman, “The Decline in
Business Profitability: A Disaggregated Analysis,” Economic Review, Federal Reserve
Bank of Kansas City (Jan. 1983). Employee compensation varies between 85 per-
cent and 90 percent of after-lax business income, year after year.

35. ‘Then the experts debate over the question, “10 percent of what?” They also
debate: “Does the Bible require a third-year additional tithe?”
