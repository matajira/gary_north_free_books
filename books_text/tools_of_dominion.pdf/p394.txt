386 TOOLS OF DOMINION

women and their well-paid, state-licensed accomplices. Meanwhile,
these critics of biblical law are busy challenging any defenders of the
law with criticisms along these lines: “You would reimpose the bar-
baric principle of poking out a man’s eye or cutting off his hand. This
is nothing but vengeance, a return to savagery. What possible good
would it do the victim to see the assailant suffer physical damage
identical to his own? Why not impose some sort of economic restitu-
tion to the victim? To inflict permanent injury on the assailant is to re-
duce his productivity and therefore the wealth of the community, By
returning to Old Testament law, you are returning to the tribal laws of
a primitive people.”!* (This line of criticism incorrectly assumes that
the dex talionis principle was not in fact designed by God to encourage
economic restitution to the victim from the criminal. Chapter 12 will
demonstrate that lex éalionis promotes economic restitution.)
Nevertheless, the question remains: Which is truly “barbaric,” mass
murder through legalized abortion or the required judicial sanctions revealed in
biblical law? The Christian antinomians of our day—that is to say,
virtually all Christians—have voted for the barbaric character of
biblical law. They are faced with a choice: Minimal sanctions
against abortion or the civil enforcement of biblical law? Their an-
swer is automatic. They shout to their elected civil magistrates,
“Give us Barabbas!” Better to suffer politically the silent screams of
murdered babies, they conclude, than to suffer the theocratic embar-
rassment of calling for the public execution of convicted abortionists.1®
The babies who are targeted for destruction have only a confused,
inconsistent, waffling, squabbling, rag-tag army of Christians to

15. Henry Schaeffer wrote a book called ‘Ue Social Legislation of the Primitive Semites
(New Haven, Connecticut: Yale University Press, 1915). The title is revealing. He
did not comment on the “eye for eye” passages.

16. We must not miss the point: the inevitable issue here is theocracy. When a
Christian calls for the execution of the convicted abortionist, he is necessarily calling
for the enforcement of God's revealed law by the civil magistrate. This fear of being
labeled a theocrat is why James Dobson chooses to weaken his response to a pro-
abortion physician by not dealing forthrightly with Exodus 21:22-25: “Do you agree
that if a man beats his slave to death, he is to be considered guilty only if the individ-
ual dies instantly? If the slave lives a few days, the owner is considered not guilty
(Exodus 21:20-21)[?] Do you believe that we should stone to death rebellious chil-
dren (Deuteronomy 21:18-21)? Do you really believe we can draw subtle meaning
about complex issues from Mosaic law, when even the obvious interpretation makes
na sense to us today? We can hardly select what we will and will not apply now. If we
accept the verses you cited, we are obligated to deal with every last jot and tittle”
Dobson, “Dialogue on Abortion,” in James Dobson and Gary Bergel, The Decision of
Life (Arcadia, California: Focus on the Family, 1986), p. 14.

 

 
