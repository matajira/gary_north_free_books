170 TOOLS OF DOMINION

history the moral character of God the law-giver and Judge. Jesus
said that those who have been delivered from great wrath are more
thankful than those who have been delivered from less wrath (Luke
7:40-43). Christians should rejoice in their position as bondservants
to the Most High God because they understand the magnitude of the
wrath from which God has graciously delivered them.

This threat of eternal judgment now faces at least four to five bil-
lion people who are presently alive, which is why we need worldwide
revival now, in this generation. We need more evangelism. But our
enthusiasm for evangelism should be in part motivated by our confi-
dent knowledge that those souls that have already been condemned
by God now face eternal agony, without hope, without peace, and
without escape, forever and ever, amen. They have gone into eternal
slavery as people who are now and forever more under God's whip,
for they did not obey God when they had the option to do so. They
chose to remain unrighteous servants. They will be on God’s whip-
ping block forever.

Without the doctrine of hell, the meaning of Christ’s gospel of
salvation becomes unclear. Without God’s eternal judgment, His
eternal mercy has no comparable eternal significance. Without the
eternal fires of judgment as its background, the agony of Jesus Christ
on the cross was a mistake, a case of overkill. The threat of the lake of
fire is not the threat of mere annihilation; it is the threat of being tor-
tured forever by God Himself, who does everything perfectly.'®?

My point is simple enough: any ¢arth/y punishment that God has
brought, may bring, or wants His people to bring lawfully as His or-
dained agents of both mercy and condemnation, is minimal punish-
ment when compared to the curse of eternal torment. Such earthly
cursings are hardly worth discussing, compared to the doctrine of

132, The doctrine of the annihilation of the soul in hell has generally been con-
fined to the cults or borderline cults. This has begun to change in certain neo-
evangelical circles. The annihilationists argue that God does not punish the lost for-
ever, but instead annihilates them. The most detailed defense of this position is
Seventh-Day Adventist Edward William Fudge’s book, The Fire That Consumes: A
Biblical and Historical Study of Final Punishment (Houston, Texas: Providential Press,
1982). It is not surprising that this book should have changed the views of several
prominent neo-evangelical scholars, for ours is an aye that is generally hostile to the
idea of God's covenantal cause and effect in history, let alone beyond history. God
the Judge is as disturbing to men as God the Law-giver and God the six-literal-day
Creator. Such a view of God does nat conform to modern man’s Darwinian view of
the universe. For a critique of Fudge, see Robert A. Morey, Death and the Afterlife
(Minneapolis, Minnesota: Bethany House, 1984), pp. 199-222.

 
