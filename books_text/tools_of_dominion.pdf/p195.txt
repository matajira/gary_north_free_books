A Biblical Theology of Slavery 187

Westermann keeps asking questions that he never really an-
swers. “What were the elements inherent in Christian doctrine,
whcther original in Christianity or borrowed, and what were the fac-
tors in the later structure of Christian power which sanction the
opinion that Christian teaching, the Christian way of life, and Chris-
tian organization must, in the final analysis, destroy slavery???
The early church fathers were indifferent to the existence of the slave
trade. Why?

Paul’s answer, Westermann says, was that slavery is punishment
for sin. It is indeed, and the Old Testament presents it as such, but
how does this explain why the New Testament does not call for aboli-
tion? Isn’t ours the new order of Christ? Why should Christians be
placed in permanent bondage to pagans or to cach other? Paul sent
Onesimus back to Philemon. Why would he have done this if slavery
is inherently wrong? Why condone it implicitly by not insisting that
Philemon free Onesimus? Why encourage slavery by silence? John
Murray, a twentieth-century Presbyterian theologian and ethicist,
stated the cthical problem quite bluntly: “If the institution is the
moral evil it is alleged to be by abolitionists, if it is essentially a viola-
tion of basic human right and liberty, if slave-holding is the
monstrosity claimed, it is, to say the least, very strange that the
apostles who were so directly concerned with these evils did not
overtly condemn the institution and require slave-holders to practice
emancipation. If slavery per se is immorality and, because of its
prevalence, was a rampant vice in the first century, we would be
compelled to conclude that the high ethic of the New Testament
would have issued its proscription, But this is not what we find.”?!*

Breaking With the Old Covenant

The biblical answer to Murray’s question cannot be grasped
without a redemptive-historical understanding of history. The defin-
itive break with the Old Covenant was madc with the death and res-
urrection of Christ, Then came His ascension and the coming of the
Holy Spirit at Pentecost. Then came the New Testament, book by
book. Finally, the fall of Jerusalem ended Israel’s old order, leaving
Christ's new order as the covenantal basis of redeemed society. All of

213. fdem.
214. John Murray, Principles of Conduct (Grand Rapids, Michigan: Eerdmans,
1957), p. 94
