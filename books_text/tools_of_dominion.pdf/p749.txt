The Prohibition Against Usury 744

know that he has a lot of guards watching everything, and that he
has always been scrupulously honest. He then betrays this trust. He
issues warehouse receipts for gold for which there is no gold in
reserve. He then loans these receipts to borrowers. The receipts
serve as money. People accept them in exchange for goods and ser-
vices. These warehouse receipts are considered “as good as gold.”
Why? Because they are always exchangeable for gold upon demand.
Just take the piece of paper to the warehouse, and get your gold.
No problem!

But now there és a problem. There are more receipts for gold
than there is gold in reserve to pay all the potential bearers on de-
mand. These “demand deposits” are now vulnerable to that most
feared of financial events, a bank run. Depositors who have receipts
come down and demand repayment. There is not enough gold in
reserve to meet the total demand.

The warehouse has placed itself in a position similar to that of the
poor man who immorally secures loans from a dozen lenders on the
basis of one piece of collateral. The warehouse owner has become a
banker. He makes loans, for which borrowers agree to pay him inter-
est in the future, along with a return of the principal. But the money,
once loaned out, is gone until the day that repayment comes. The
warehouse is vulnerable to a run on the deposits. The warehouse
owes gold to the depositors. It is indebted to them. The deposits are
legal liabilities to the bank. The bank has become multipally indebted.

The Creation of Money

The warehouse receipt circulates as if it were gold. Now, if gold
serves as money in that society, the pieces of paper will also serve
as money.

When these pieces of paper are pure moncy-metal substitutes,
nothing changes. Physical gold is taken out of circulation and put
into a warehouse. A piece of paper (a warehouse receipt) substitutes
for the physical gold. No new money has come into circulation. No
money has been taken out of circulation. Nothing fundamental
changes, except for convenience. But if the warehouse owner writes
up a warehouse receipt for gold when there is no new gold on
deposit, then he has increased the money supply in the community.
No one has come to the warehouse and deposited gold (taken it out
of the day-to-day economy). So the warehouse receipt is inescapably
inflationary. It is an addition of money into the economy. (I am defin-
