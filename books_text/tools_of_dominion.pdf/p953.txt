Conclusion 945

What the case laws provide is an alternative to the messianic
State. The case laws provide sanctions that match the magnitude of
the crime. The basic penalty for crimes against property and body is
some form of restitution. Crimes against the integrity of God are
capital crimes: those convicted of such infractions are delivered into
God's court for His direct judgment. As history moves closer to the
day of judgment, socicty will progressively be conformed to these
standards. Democratically, meaning a bottom-up movement of the
Holy Spirit, voters will enact the whole law-order of God. Thus,
what the Puritans attempted to do in England was wrong because it
was a top-down imposition of God’s law. What the New England
Puritans attempted to do was valid; there was general agreement
about biblical civil law. But immigration and defections within Puri-
tanism after 1660 changed the circumstances.

What the critics of theocracy always assume is that it has to be
anti-democratic. But if the Spirit of God moves a vast majority of
men to confess Jesus Christ as Lord and Savior, and if they rcturn to
the Old Testament in search of biblical blueprints, then the resulting
theocratic republic will be legitimate in terms of democratic stan-
dards, assuming that such standards refer simply and exclusively to
techniques of campaigning and voting.”

When that theocratic majority appears, you can bet your life that
the humanists will then try to subvert it by means of an elitist con-
spiracy. We read about such a revolt against Moses and Aaron in
Numbers 16. It was done in the name of the People: “And they
gathered themselves together against Moses and against Aaron, and
said unto them, Ye take tuo much upon you, seeing all the congrega-
tion are holy, every one of them, and the Lorp is among them:
wherefore then lift ye up yourselves above the congregation of the
Lory?” (v. 3). We read about the final such attempt in Revelation
20:8-9, at the very end of history. These voices of the People are in
favor of democracy for only so long as they can control a majority of
voters by means of a hierarchical elite that pretends to listen to the
People—an elite far more subtle than the Communists’ one-party
dictatorship in the name of the people.

A sovercign agent always acts through spokesmen in a hierarchy,
There will always be an elite: intellectual, educational, military, and

22. Modern democratic theory ix far more than a theory of legitimate electoral
techniques, It has the character of being a rival religion. Cf. Charles Fergusson, The
Religion of Democracy (New York: Funk & Wagnalls, 1900).
