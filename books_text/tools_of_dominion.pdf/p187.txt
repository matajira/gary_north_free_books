A Biblical Theology of Slavery 179

indentured servitude did not end in the northern colonies; it per-
sisted, though on a declining basis, through the first quarter of the
nineteenth century.!’* On the other hand, the abolition of slavery in
the British West Indies in 1833 revived the use of indentured servi-
tude, with Asia furnishing the bulk of the immigrants.'75 This prac-
tice continued until 1917, when the British government made it illegal
for people from India to indenture themselves in order to pay for
their emigration from India.1”6 Asians also came to Hawaii as inden-
tured servants in the mid-nineteenth century.!77

There is little doubt that voluntary indentured servitude was a
rational economic response by both importing masters and im-
migrating servants, given the high costs of ocean transportation. !78
When these costs fell in the late nineteenth century because of the
development of steamships, indentured servitude declined rapidly.
The number of immigrants to the United States rose from 5 million
in the entire antebellum period!7* to 10 million in the next 30 years,
and 15 million in the 15 years thereafter.1*° “The change in countries
of origin was equally dramatic: 87 percent of the immigrants were
from northern and Western Europe in 1882, but twenty-five years
later, 81 percent were from southern and eastern Europe.”!*!

Southern Slavery

The eighteenth century saw the development of what became the
antebellum slave system in the American South. By the eve of the
American Revolution, half of Virginia’s population was black, and
two-thirds of South Carolina’s.'8? By 1830, the Negro slave system
had become permanent in the South. Southerners by then measured
their rank in society, Stampp says, by counting their slaves.1°9 At

174, Galenson, op. cit., pp. 12-13, This is an excellent summary essay, and I have
relied on it heavily. See also Galenson, White Servitude in Colonial America: An Economic
Analysis (Cambridge, Massachusetts: Harvard University Press, 1981).

175. Ibid., p. 14. See Galenson’s footnote 38 for bibliography.

176, Ibid., p. 26.

177. Tid., p. 15.

178, Ibid., pp. 16-21.

179, The word “antebellum” refers to the pre-Civil War period in the United
States.

180. Thomas Sowell, Ethnic America: A Histry (New York: Basic Books, 1981),
p. 13.

181, Idem. For other possible economic explanations, see Galenson, op. cit., pp.
21-24.

182, Stampp, Peculiar Institution, p, 24.

183. Ibid., p. 27.

 
