7

‘VICTIM’S RIGHTS VS.
THE MESSIANIC STATE

And he that smiteth his father, or his mother, shall be surely put to
death (Ex. 21:15).

And he that curseth his father, or his mother, shall surely be put to
death (Ex. 24:17),

The theocentric principle here is obvious: God the Father must
not be attacked by His children. Parents are God’s covenantal agents
in the family, which is a hierarchical, oath-bound covenantal institu-
tion, They are God’s covenantal representatives in the family. To strike
an earthly parent is the covenantal equivalent of striking at God. It
is an act of moral rebellion so great that the death penalty is invoked.

The doctrine of hierarchy, which includes the doctrine of repre-
sentation,! is point two of the biblical covenant model. The Book of
Exodus, the second book in the Pentateuch, is primarily concerned
with point two of the covenant, for the Pentateuch is itself structured
in terms of the biblical covenant’s five-point structure.? It is appro-
priate that questions relating to representation should be the focus of
several of the case laws of Exodus.

‘The covenant’s representation principle is built into the creation,
We know that the visible creation testifies to the existence of the in-
visible God. “For the invisible things of him from the creation of the
world are clearly seen, being understood by the things that are
made, even his eternal power and Godhead; so that they are without
excuse” (Rom. 1:20). Men, as creatures, cannot strike at God directly.

1. Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas: Insti-
tute for Christian Economics, 1987), pp. 46-47.

2. Gary North, The Dominion Covenant: Genesis (2nd ed.; Tyler, Texas: Institute for
Christian Economics, 1987), pp. x-xiv.

278
