414 TOOLS OF DOMINION

as their operational models the judicial systems of contemporary
covenant-breaking societies. They have “baptized” or “circumcised”
prevailing judicial practices. But these judicial standards have usually
been opposed to the Bible’s lex talionis standard. There is little doubt
that the covenant-breaker is hostile to the judicial principle of “an
eye for an eye.” First, it reminds him of the existence of a sovereign
God who will judge him perfectly, so he prefers to affirm a more “mer-
ciful” judicial standard. He understands the biblical principle pro-
claimed by Jesus Christ: “For with what judgment ye judge, ye shall
be judged: and with what measure ye mete, it shall be measured to
you again” (Matt 7:2.) Second, he also wants to worship autonomous
man through worshipping the State, so he wants the State to be more
rigorous than God in imposing historical sanctions, as a testimony to
autonomous man’s might. These goals are clearly contradictory. By
pursuing both, autonomous man produces judicial schizophrenia.

In some societies, covenant-breakers have regarded (ex talionis as
too lax. This is surely the case in totalitarian societies today. The
Gulag Archipelago of the Soviet Union is clearly not the product of
biblical law. Yet in other societies, dex talionis is regarded as in-
humane. Western humanists today regard biblical law as primitive
and brutal. We must ask: Is the concern of humanists for the “brutal-
ity” shown by the Bible’s “cye for cye” principle misguided? Shouldn't
their concern be focused instead on the brutality of the criminal
against the innocent victim? Isn’t the lex falonis principle a deterrent
to crime, especially repeated crimes by a criminal class? Shouldn't
our concern be with the victims of violent crime rather than with the
criminals who commit them?

What we find today in the United States is a perverse mixture of
judicial laxness and tyranny, both of which are inevitable in any so-
ciety that rejects biblical law as its civil standard. Members of the
voting public occasionally perceive this judicial schizophrenia, but
they have no understanding of its cause or its biblical solution. They
have denied the authority of biblical law so systematically that they
no longer have any awareness of how God’s judicial standard would
provide justice for modern civilization. This judicial blindness is as true
of seminary theologians and pastors as it is of the average covenant-
breaker. There has been a deliberate Christian blackout regarding
the legitimacy of biblical law for over three centuries.”

2. By the 1680's, the Puritan pastors of New England had ceased preaching ser-
mons on the need for specific pieces of civil legislation in the name of the Bible, Gary
North, Puritan Economic Experiments (Tyler, Texas: Institute for Christian Economics,
1988), pp. 35-39, 57-59.
