262 TOOLS OF DOMINION

was adopted into the family of her husband. The Western practice of
giving the bride the last name of her husband indicates her adoption
into the bridegroom’s family. This is also why both sets of parental
in-laws are usually referred to as Mom or Dad by the children. It isa
verbal acknowledgment of the covenantal relationship of adoption.

The Concubine

Rachel and Leah complained that their father Laban had squan-
dered the inheritance that they and their children were entitled to
(Gen, 31:14-16), treating them as if they had been sold into slavery.
They had in mind the accumulated earnings of fourteen years of
Jacob’s labors to pay their bride prices. Jacob had earned this wealth
back from Laban, as they recognized (v. 16), but this meant that it
once again belonged to Jacob; they still had no dowries. They were
being relegated by their father to the status of concubines, not wives.

In ancient Israel, keeping the bride price was the economic
equivalent of selling a daughter into slavery. When a father in this
way sold his daughter to a husband, he was legally making her a
concubine. He did not pass on to her any portion of the money he
had received from the bridegroom or her future father-in-law. He
kept it all. This is why the transaction was a purchase. His daughter
was becoming a bondservant inside another man’s houschold. This
bondservice would not be governed by the sabbatical principle of the
year of release. Also, her father did not retain the right of redeeming
her as her kinsman-redeemer, unless the man who bought her de-
cided before the marriage to return her, and her father could and
would repay him his bride price. Thus, a concubine was a perma-
nent bondservant who worked at the discretion of her husband,

Does this mean that her betrothed husband could have sold her
to another Hebrew at will? To answer this question, we must first
look at the covenantal nature of her position. The text spcaks of “her
master, who hath betrothed her.” The betrothal constituted a mar-
riage promise, but because she was not a free woman, meaning a
woman with a dowry, this was not a totally binding vow on his part.
Tt was not the same legally as a promise to marry a free woman for
whom a bride price had been paid, and who brought a dowry into
the marriage. We know that it was not the same, because it was not
considered adultery for another man to have sexual relations with
her, The two would be scourged but not executed, “because she was
not free” (Lev. 19:20). If a woman possessed a dowry, then a
