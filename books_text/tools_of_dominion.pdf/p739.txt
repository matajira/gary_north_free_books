‘The Prohibition Against Usury 731

Now, think about this proccss of discounting for cash. We call
this process capitalization. Let us assume that you own an ounce of
gold today. An ounce of gold fifty years from now, or twenty years
from now, is not worth your ounce of gold today. A future ounce of
gold, whether scheduled to be received a year {rom now or a thousand
and one years from now, is discounted in your mind. We have there-
fore discovered a law of human action (which applies in economics):
the present cash market value of expected future goods 1s always discounted com-
pared with the present cash market value of the identical physical goods.

What is this discount called? It is called the rate of interest. You dis-
count the future value to you of any good compared to what that
same good is worth to you immediately, whether it is that auto-
mobile or an ounce of gold from that piece of property. For me to get
you to hand over the present good today (money), I have to promise
to return it to you in the future, plus extra money or other benefit. In
other words, I have to pay you interest.

Let us consider another example. You win a brand new Rolls-
Royce automobile. These cars do not change in styling very often.
They actually look more like a 1953 Packard than like a new car. But
they are status symbols. Assume that all taxes are paid by the prize-
granter. You are now offered a choice: delivery of the car today or in
a year, The style probably will not change (low risk factor). Tastes of
the very rich public for Rolls-Royces probably will not change. The
car will be taken care of, you are assured. Make your choice: the car
now or the car in a year. The choice is obvious, Why is it obvious? Be~
cause of interest, meaning time-preference, “Better now than later!”

Why do some people seriously believe that your preference here
is pathological, the product of your morally diseased mind? Because
they are utopians.

Utopianism: A World Without Scarcity

It would be nice if I did not have to mention any of the following
crackpot theories of economics. The reason why this task is
unavoidable is that these ideas have spread far and wide in Christian
circles. Christian economics has been an ignored topic for centuries.
What has passed for Christian economics in the past has either been
baptized moralism or baptized humanism, Numerous crackpot
schemes have been promoted in the name of Christian economics,
and still are being promoted. The closer we get to the question of
