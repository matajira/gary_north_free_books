Selling the Birthright: The Ratification of the U.S. Constitution 1207

stitution is not only a law but also a contract or covenant.”5? The
question is: Whose sanctions are invoked by this covenant docu-
ment? Clearly, autonomous man’s sanctions. Rushdoony knows
this. So he has restructured all political theory to create a justifica-
tion of this absence of any reference to God’s law or God's sanctions:
“Second, we must remember that the Constitution can make no man
nor nation good; it is not a moral code. It does not give us a substan-
tive morality, but it does reflect a procedural morality.”®

Notice the shift in the argument: the Constitution cannot make
anyone good. This is the standard humanist line against all Chris-
tian legislation: “You can’t legislate morality!” What Rushdoony has
always maintained is that you can’t legislate anything except morality.
As he wrote in the Institutes of Biblical Law (1973), “But, it must be
noted, coercion against evil-duers is the required and inescapable duty of the
civil authority.” Again, “law is a form of warfare. By law, certain acts
are abolished, and the persons committing those acts either executed
or brought into conformity to law.”

Of course the Constitution cannot make anyone good; the function
of biblical civil law is not to make anyone good; it is to suppress public
evil. For 30 years, Rushdoony previously had argued that any other
view of civil law is the “work’s doctrine” of all non-Christian religion:
salvation by law, This is humanism’s view, he always insisted: “Man
finds salvation through political programs, through legislation, so
that salvation is an enactment of the state.”®*. What is the Christian alter-
native? To enforce God’s law and God’s sanctions in history, and only
God’s law and God’s sanctions.

The second aspect of man under law is that man’s relationship to law be-
comes ministerial, not legislative, that is, man does not create law, does not
decree what shall be right and wrong simply in terms of his will. Instead,
man seeks, in his law-making, to approximate and administer fundamental

57. Rushdoony, “U.S. Constitution,” p. 21.

58, fbid., p. 22.

59. Rushdoony, The Institutes of Biblical Law (Nutley, New Jersey: Craig Press,
1973), p. 292. Contrast this statement with the following position taken in the journal
of Christian Reconstruction: “God did not make salvation coercive. Neither is morality
coercive, . , . Punishing sin is not a role delegated to civil government,” Tommy W.
Rogers, “Federalism and Republican Government: An Application of Biblically
Derived Cultural Ethos to Policical Economy,” vol. XII, No. 1 (1988), p. 95.

60. Thid., p. 191, See also pp. 92-95.

61. Rushdvony, Politics of Guill and Pity (Fairfax, Virginia: Thoburn Press, [1970]
1978), p. 145.
