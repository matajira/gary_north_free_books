530 TOOLS OF DOMINION

Why? Because if they go unpunished, God threatens to curse the
community. Thus, criminal law in the Bible was not enforced “in the
name of the community,” but in the name of God, so as to protect the
community from God's wrath.

Restitution to God

Phillips is consistent in his errors, at least; he also argues that
Hebrew covenant law was exclusively criminal law, meaning that its
goal was solely the enforcement of public morals, rather than civil
law (torts), in which restitution to the victim was primary. This
definition, if correct, would remove from covenant law all biblical
statutes that require restitution to victims. What he is trying to do is
separate the case laws of Exodus from the Ten Commandments. If
believed, this argument would make it far easier for antinomians to
reject the continuing validity of the case laws in New Testament
times, for the case laws of Exodus and other books rest heavily on the
imposition of restitution payments to victims. The antinomians
could publicly claim allegiance to the Ten Commandments, but then
they could distance themselves from the specific applications of these
commandments through the case laws, for they have concluded that
the case laws are unconnected to the Decalogue because these are
“civil” laws rather than “criminal” laws. *? Phillips writes: “But it is the
contention of this study that Israel herself understood the Decalogue
as her criminal law code, and that the law contained in it, and devel-
oped from it, was sharply distinguished from her civil law.”#

Tf true, then all you need to do to escape from the covenantal,
State-enforced requirements of the Decalogue is to make the Ten
Commandments appear ridiculous. This he atternpts in Chapter 2.
“Initially only free adult males were subject to Israel's criminal law,
for only they could have entered into the covenant relationship with
Yahweh. . . . But women did not enter into the covenant relation-
ship, and were therefore outside the scope of the criminal law. They
had no legal status, being the personal property first of their fathers

46, Ibid., pp. 10-11.

47. Phillips says that the “Book of the Covenant,” meaning Exodus 21-25, was a
product af David's reign, with some of it quite possibly written by David himself.
Ibid., ch. 14.

48, Bid., p. 1.
