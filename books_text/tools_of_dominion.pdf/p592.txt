584 TOOLS OF DOMINION

dress grievances. Local conditions, local standards of cleanliness,
silence, or whatever, involve local conflicts: these are best settled by
local governmental units.

For example, if a national civil government imposes general
pollution-contro! standards for clean air, local communities could be
damaged economically, A community may have a polluting factory
as its primary employment base. The factory is bankrupted by the
newly applied national standards. Its owners, or rival producers of
similar goods, may choose to move capital into a foreign nation
whose political leaders are more anxious to create jobs than to avoid
pollution. The pollution is simply shifted ‘off shore.” This may be a
good thing, overall, perhaps this particular sort of pollution will be
less of a problem in some other geographical environment that is
blessed with pollution-reducing wind patterns. Or a foreign nation
may have a less dense population. The first question is: Who knows
best? Is some political body or bureaucratic agency thousands of
miles away from the affected areas sufficiently informed about local
effects of such decisions?

A second relevant question is: Who pays? Rich voters in some
regions of a country may be making political decisions that adversely
affect poorer voters in different regions whenever national environ-
mental standards are imposed. Are such national standards really
that crucial to the survival of the environment? Can local geographi-
cal regions really destroy the ecology of the entire nation? What kind
of proof can the defenders of national pollution-control standards
present to defend their conclusion that such standards are exclu-
sively a matter of national self-interest?

Subsidies to the Politically Skilled

One reason why we get national ecology or pollution-control
standards is because of the costs of political mobilization. It is less ex-
pensive for special-interest groups to lobby a few hundred politicians
in the nation’s capital or to gain control over a Washington bureau-
cracy than it is to conduct a lobbying campaign in every regional leg-
islature and local town council. The national civil government then
preempts the regional units of civil government. This centralizes politi-
cal power, which leads increasingly to a reduction of everyone's politi-
cal freedom,

Why should residents of Los Angeles, California, or Denver, Col-
orado, whe live in peculiar geographical environments (stagnant air
