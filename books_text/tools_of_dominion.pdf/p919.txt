Blood Money, Not Head Tax

Ed Powell’s Modification

Ed Powell’s essay, “God’s Plan of Taxation,” is an extension of
Rushdoony’s position, which is why Rushdoony allowed it to appear
in his only co-authored book, There is one interesting addition that
Powell makes, however. He quite correctly points out that the
Levites were not subject to military conscription (Num, 1:47-49),
and therefore they were not required to pay the so-called poll tax.
Rushdoony had insisted in the Institutes: “It was paid by Levites and
all others.”* Powell argues that the Levites were not part of the civil
order, and so were not required to pay any tax to the State, and this
was the only tax the State could lawfully collect, according to both
Rushdoony and Powell. “This tax went solely for the purpose of sup-
porting the state, and only those who were members of the civil order
because of their military service paid it.”** Thus, in Powell’s version.
of political theory, civil citizenship is based on two things, the pay-
ment of taxes and participation in the military. He clearly recognizes
the connection between the “tax” of Exodus 30:11-16 and military ser-
vice. Would he conclude that in New Testament times, ordained
ministers of the gospel should not be allowed to vote or be required
to pay taxes? If he denies this, then would he then conclude that they
should be subject to military conscription?

What Powell does not recognize is central to Jordan’s argument
and mine: by becoming a Nazarite during a holy war, the soldier in Old Cove-
nant Israel became a temporary priest. It was the army’s very position as a
temporary priesthood that made the payment of blood money man-
datory if the soldiers were to avoid the plague when God came into
the camp. Thus, the requirement to pay blood money to the taber-
nacle had nothing to do with the supposed status of the Levites as
being outside the civil order. It had everything to do with the need
for atonement by those who were temporarily set aside (made holy}
for God’s special purposes during a war.

The Rushdoony-Powell position leads to innumerable problems,
especially in extending into New Testament times the erroneous
principle of the head tax as the sole means of State financing. I have
dwelt at some length on this explanation of Exodus 30:11-16 only be-

23. Rushdoony, Institutes, p. 80.

24, Rushdoony and Powell, Tithing and Dominion (Fairfax, Virginia; Thoburn
Press, 1979), p. 64, The irony here is that it was my “freewill” offering to Pastor
Robert Thoburn’s church in Fairfax, Virginia, that financed the publication of this
book.

alse
