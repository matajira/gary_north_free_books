1040 TOOLS OF DOMINION

tile, and that to win the Jew to Christ he must crush any and every
hope for salvation which is related in any way to the fact that he is a
Jew and a ‘son of Torah,’ the sooner the Christian will honor his
Lord by his witness to the Jew and the more effective will his witness
become.”!86 There is no valid message of salvation in the Talmud.
This was Peter’s message to Israel:

Be it known unto you all, and to all the people of Israel, that by the name of
Jesus Christ of Nazareth, whom yc crucified, whom God raised from the dead,
even by him doth this man stand here before you whole. This is the stone
which was set at nought of you builders, which is become the head of the
corner. Neither is there salvation in any other: for there is none other name
under heaven given among men, whereby we must be saved (Acts 4:10-13).

Orthodox Judaism is at war with the Old Testament. This is the
primary thesis of this essay. But, unlike Reform Judaism, which is
infected with the same biblical higher criticism that has undermined
mainstream Christianity, Orthodox Judaism claims to accept the
Old Testament as the inspired Word of God. How, then, can anyone
rightfully say that Orthodox Judaism is at war with the Old Testa-
ment? Only by accepting Jesus’ words literally:

I am come in my Father’s name, and ye receive me not: if another shall
come in his own mame, him ye will receive. How can ye believe, which re-
ceive honour one of another, and seek not the honour that cometh from
God only? Do not think that I will accusc you to the Father: there is one
that accuscth you, even Moses, in whom ye trust. For had ye believed
Moses, ye would have believed me: for he wrote of me. But if ye believe not
his writings, how shall ye believe my words? {John 5:43-47).

To demonstrate the accuracy of Jesus’ words, I here present a
summary of the exegetical methodology of the judicial writings of the
most famous and most respected master of the Talmud in the history
of Judaism: Moses Maimonides.

Rabbi Moshe ben Maimon, The Rambam

Few gentile scholars have ever heard of the Mishneh Torah, but all
medieval historians and specialists in the history of Western philosophy
know of Maimonides. Moshe, the son of Maimon, better known as
Maimonides (1134-1204), is by far the most famous Jew in medieval

136. Robert L. Reymond, Editor’s Preface, to Van Til, Christ and the Jews, p. v.
