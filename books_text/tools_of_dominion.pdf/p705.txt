Oppression, Omniscience, and fudgment 697

If the seller of a good or service is protected by the judges from other
competitors who might otherwise enter the market and makc the buyer
a better (lower price) offer, then the seller is oppressing the buyer. He
may not have approved of this legislation or judicial interpretation,
but he is now the beneficiary. If such restrictive legislation is in force,
then the seller must do his best to sell his product or service to the
buyer at a price that would prevail if there were open competition.

The problem, of course, is that in éhe absence of a free market, no one
can really be sure just what such a free market price might be. Without the
information made available through market competition, buyers
and sellers are left without reliable indicators of the true conditions
of supply and demand.” Moral decisions concerning “fair” pricing
are therefore made more difficult— more expensive to solve—by the
State’s interference with the flow of economic information. The pre-
vailing price in a government-regulated market raises moral ques-
tions concerning fairness precisely because it is not a competitive
market price. Moral dilemmas for honest sellers are created by the
State’s interference because this interference creates opportunities for
sellers to extract monopoly profits from buyers, The “non-monopoly”
price can only be guessed at by judges, buyers, and sellers.

4, Better Information

The economically stronger party in a transaction may have bet-
ter information at his disposal. How much of this is he morally re-
quired to give to the economically weaker seller? If he asks a lower
price, then he is, economically speaking, transferring the valuc of his
information to the other party in the exchange.

The civil government should not compel the transfer of such in-
formation. If such a law were passed, it would inhibit the quest for
better information on the part of all participants, which would even-
tually harm all people in the society.’ Besides, judges would: face
that age-old problem, defining exactly how much of his information
the economically stronger seller (or buyer) is required to give up to
the other person before a voluntary exchange is legal. For that mat-

39. Ludwig von Mises, “Economic Calculation in the Socialist Commonwealth”
(1920), in F. A. Hayck (cd.), Collectivist Economic Planning (London: Routledge &
Kegan Paul, [1935] 1963); T. J. B. Hoff, Economic Calculation in the Socialist Society
(London: Hodge, 1949), reprinted by Liberty Press, Indianapolis, Indiena, 1981;
Hayek, Individualism and Ecomomic Order (University of Chicago Press, 1948), chaps. 7-9.

40. Thomas Sowell, Knowledge and Decisions (New York: Basie Books, 1980), ch. 8

41. Gary North, “Exploitation and Knowledge,” The Freeman (Jan. 1982).
