Appendix G

LOTS OF FREE TIME: THE EXISTENTIALIST
UTOPIA OF S. C. MOONEY

Another popular excuse for usury is that it ix no different than rent, It
is said that “interest” is merely rent on “money”, and that if rent is assumed
to be legitimate, then usury would have to be considered legitimate as well.
... The economic similarity between usury and the rent of property
readily is admitted. However, this close connection does not serve to legit-
imize usury, as Locke et al suppose; but to condemn rents. . . , [I}t is
not lawful for one to sell the use of his property (rent).

5. CG. Mooney?

Consider the economic logic offered by any promoter of a zero-
interest economy. As I have argued in Chapter 23, he is the eco-
nomic world’s equivalent of the self-proclaimed scientist who insists
that a perpetual motion machine is legal. But the promoter of a zero-
interest economy is really far worse: he is like a crackpot physicist
who insists that only perpetual motion machines should be legal. He
is the classic defender of something (the use of an asset over time) for
nothing (no rental fee). He says that you can construct an honest,
fair, and productive economy by making interest payments illegal.

Again, let me apologize in advance for filling up space in this
commentary with arguments against nonsense. If this nonsense, or
nonsense quite similar to it, had not been offered in the name of the
Bible for about a millennium and a half, I would not bother to com-
ment on it. Life is too short, and this book is too long. But the lure of
crackpot theories of interest has been with us for a long, long time;
first, under the authoritarian rule of clerics in an era before econom-
ics was an intellectual disciplinc, and second, under the hoped-for

1. S.C. Mooney, Usury: Destroyer of Nations (Warsaw, Ohio: Thopolis, 1988), pp.
172, 173.

1179
