Appendix H

SELLING THE BIRTHRIGHT: THE RATIFICATION
OF THE U.S. CONSTITUTION!

And Jacob sod pottage: and Esau came from the field, and he was
faint: And Esau said to Jacob, Feed me, I pray thee, with that same red
pottage; for I am faint: therefore was his name called Edom. And Jacob
said, Sell me this day thy birthright. And Esau said, Behold, 1 am at the
point to die: and what profit shall this birthright do to me? And Jacob
said, Swear to me this day; and he sware unto him: and he sold his
birthright unto Jacob. Then Jacob gave Esau bread and pottage of len-
tiles; and he did eat and drink, and rose up, and went his way: thus
Esau despised his birthright (Gen. 25:29-34).

The church . . . was thrown out into the street by the lawyers of
Philadelphia, who decided not to have a Christian country... . [I]n
effect, they took all the promises of religion, the pursuit of happiness,
safely, security, all kinds of things, and they set up a lawyers’ paradise,
and the church was disenfranchised totally.

Otto Scott (1988)2

Otto Scott, in a perceptive essay on the ever-changing U.S. Con-
stitution, warns us against becoming deluded by “a sloganized his-
tory” of this nation and its Constitution, He traces the history of
growing tyranny in the United States in terms of the steady transfor-
mation and reinterpretation of the Constitution. “The history of the
Constitution of the United States, like all other aspects of our na-
tional history, reflects the changes in American society and govern-
ment through the years. To understand these changes it is essential

1. A more developed version of this thesis is presented in my book, Political Poly-
theism: The Myth of Pheratism (Tyler, Texas: Institute for Christian Economics, 1989),
Part 3.

2. “Rasy Chair” audiotape #165 (March 10, 1988), distributed by the Chalcedon
Foundation, P. O. Box 158, Vallecito, California 95251.

1190
