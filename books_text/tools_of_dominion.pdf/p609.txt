Poilution, Ownership, and Responsibility 601

levels of pollution is as far-fetched as science’s ability to come up with
socially optimal anything (especially an optimal investment of scarce
economic resources in scientific reports). Because humanistic econo-
mists cannot scientifically make interpersonal comparisons of sub-
jective utility, it is iegitimate to assert the ability of any economist
or other scientist to offer advice or data on how to achieve socially
optimal anything, All the equations in the world will not add one
iota of knowledge that will prove useful to any economist who relies
exclusively on subjective economic theory in his search for socially
optimal levels of pollution. (If the equations are sufficiently elegant
to be utterly irrelevant, they could, on the other hand, win the de-
veloper a Nobel Prize in economics.}

The modern State is becoming messianic. Its supporters believe
that salvation is essentially political. Thus, they promote State ac-
tion in their efforts to heal the environment. Instead, we should
begin with the issue of legal responsibility. Individuals are to be held
accountable for their actions— by God, by the civil government, and
by the free market.

When many polluters are harming many people, the State must
intervene and impose sanctions against all producers of the particul-
ar type of generally unwanted pollution. But in doing so, the officials
must count the costs to society of the intervention’s effects of people’s
faith in private ownership. The intervention must be made in terms
of a defense of private property rights, not its abolition, The rise of
the messianic State is a greater threat to liberty than pollution is.
Pollution is a recognized evil; the messianic State is the agent of a
rival religion.

A whole system of incentives and sanctions is available: fines, tax
credits, pollution control standards, and even outright prohibition.
What must be recognized is that the quest for zero pollution is mes-
sianic. It is a program that covers the real intent of its promoters:
salvation by legislation. If men do not restrain themselves voluntar-
ily as both polluters and pollution-fighters, the social order will be
torn apart by the messianic quest for the perfect environment. Such
an environment is available only after the final judgment, when the
curse is removed (Rom. 8:19-22),

The Messianic Quest for Zero Pollution

The question of pollution, ultimately, is a question of stewardship,
meaning personal responsibility. The Bible affirms that each man is
responsible for his actions. No man is to pass along the costs of his
