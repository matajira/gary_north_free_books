864 TOOLS OF DOMINION

What about the third world? The introduction of DDT and anti-
biotics into third world nations has received considerable attention
from those who try to explain the post-World War II population ex-
plosion in these areas. Another reason is the increasing urbanization
of many areas and the introduction of modern agricultural tech-
niques. The two most ignored major technological innovations that
have extended life expectancy in backward countries, according to
economist Peter Drucker, were the invention by an unknown Ameri-
can in the 1860’s of wire mesh screens for doors and windows, which
poor families adopted lo escape flying insects, and the separation of
drinking water supplies [rom latrine areas, a technique known be-
jore Alexander of Macedon. These two ignored developments are the
primary health care component of the third world’s population ex-
plosion, he argues.®2

The USSR: A Third-World Nation Medically

The Soviet Union is the great actuarial exception among major
industrial nations. Its reported life expectancy is no higher than
Communist China’s, which is a vast underdeveloped nation.* In re-
cent years, life expectancy has declined in the USSR. Reported in-
fant mortality rose from 22 deaths per 1,000 live births in 1971 to over
31 in 1977. The reported data have declined to about 29 in 1980.%*
The age-adjusted death rates of the USSR and the United States in-
tersected in 1966 at about 7.5 per 1,000. Since then, the Soviet death
rate has climbed to over 9 per 1,000, while the U.S. rate has fallen to
about 6 per 1,000.%

But the reported data probably understate the reality. On De-
cember 7, 1988, a massive earthquake struck the Armenian region of
the USSR. In less than one minute, 400,000 people were left home-
less in the middle of winter. The death toll was initially estimated to
be as high as 100,000 (later revised by the Soviet government to
25,000). The Soviets then called for international aid to the victims,
a sign of its third-world status economically.

In the immediate aftermath of the tragedy, a Los Angeles Times
wire story revealed the fact that during the previous two years, as a

32. Peter Drucker, Management: Tasks, Responsibilities, Practices (New York: Harper
& Row, 1974), p. 330.

33. Adlas of the Uniled States, p. 118.

34, Knaus, Iuside Russian Medicine, chart, p. 375

35, Ibid., chart, p, 376.
