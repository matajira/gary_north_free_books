738 TOOLS OF DOMINION

consumer loan, Second, to reduce multiple indebtedness. While the
lender cannot use the cloak during the night, the debtor cannot use it
during the day. He cannot use the same cloak as collateral for several
loans at the same time.® He is limited in his ability to indebt himself
and his future.

Character: A lender is not required to take any form of collateral.
This indicates that a major form of collateral for a loan is the lender’s
perception of the borrower's character and his ability to repay the
loan. Character, in fact, is a better form of collateral, since the
lender does not have to go to the trouble of returning the cloak each
evening. This reduces transaction costs. The less trustworthy the
borrower’s character, the more likely that a lender would require the
cloak, fearing multiple indebtedness.

Multiple Indebtedness

There is a very important application of the law of collateral, one
that is seldom discussed. Consider the case of a poor man who comes
in search of an emergency loan from his neighbor. The neighbor
assesses the man’s character, and concludes that the man is likely to
repay the loan. The lender has made a mistake. The man may visit
several people to ask for an emergency loan. If he collects from all of
them, he may waste the money. Even if he repays these loans, he has
dealt fraudulently with lenders by accepting numerous interest-
bearing loans. They have unknowingly borne added risk.

But what if the lender suspects that the borrower is somewhat
unreliable. The lender wants to honor God, so he intends to make
the loan. But he wants collateral. He wants to give the borrower an
economic incentive to repay the loan as soon as possible. The man is
poor. He has no collateral of value. But the lender can still demand the
man’s cloak. He is not allowed to take the widow’s cloak (Deut. 24:17).

What good is this cloak to the lender? He must return it in the
evening, when the man needs it. It cannot be sold. It cannot be used
by anyone in the lender’s household. It is a nuisance, for it must be
returned each evening. But it has two important economic func-
tions. First, the borrower has to come back every evening to get it
back. This is an inconvenience. He will have an added incentive to

63. ‘This was the opinion of the twelfth-century Jewish scholar, Ibn Ezra, citing
Saadia Gaon. Nchama Leibowitz, Studies in Shemat, Part 2 (Jerusalem: World Zion-
ist Organization, 1976), p. 418.
