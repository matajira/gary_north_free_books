The Epistemological Problem of Social Cost 1137

respect to economic efficiency (formal rationality), Weber argued,
capitalism’s critics very often take offense: “All of thesc [substantively
rational, ethical—G.N.] approaches may consider the ‘purely for-
maf rationality of calculation in monetary terms as of quite second-
ary importance or even as fundamentally inimical to their respective
ultimate ends, even before anything has been said about the conse-
quences of the specifically modern calculating attitude.*4° In short,
Weber concluded, “Formal and substantive rationality, no matter by
what standard the latter is measured, are always in principle sepa-
rate things, no matter that in many (and under certain very artificial
assumptions even in all) cases they may coincide empirically.”!”
This dialectical tension is basic to Weber's sociological analysis."
Economists who defend the free market seldom acknowledge the
nature of this fundamental debate between the free market's intellec-
tual defenders and the free market's critics. Their “value-free” meth-
odology and their methodological individualism blind them to the
realities of the debate—a debate over morality, values, and the
effects of voluntary economic transactions on social aggregates. Free
market economists cannot seem to understand those scholars and
critics who raise the question of individual morality, let alone social
consequences and social values, and who then ignore questions of
economic efficiency for the attainment of the economic goals of indi-
viduals. The economists dismiss such criticisms as amateurish and
irrational; the fact that most people accept the perspective of the
critics does not faze the economists, most of whom see this battle as a
technical academic debate rather than a life-and-death war for West-
em civilization, They see all conflicts as in principle resolvable “at
the margin, at some price.” They prefer not to discuss the Gulag.
Anti-capitalist critics, of course, really do tend to ignore ques-
tions of efficiency, a concept which does have to be considered care-
fully in any relevant discussion of men’s economic ability to pursue
moral goals, both personal and social. Weber recognized this:
“Where a planned economy is radically carried out, it must further
accept the inevitable reduction in formal, calculatory rationality

146, ibid., p. 86, See a slightly different translation of this passage and the one in
the preceding footnote in Weber, The Theory of Social and Economic Organization, edited
by Talcott Parsons (New York: The Free Press, [1947] 1964), pp. 185-86.

147. Ibid., p. 108. { Theory, p. 212.]

148, Gary North, “Max Weber: Rationalism, Irrationalism, and the Bureaucratic
Cage,” in North (ed.), Foundations of Christian Scholarship, pp. 141-46.
