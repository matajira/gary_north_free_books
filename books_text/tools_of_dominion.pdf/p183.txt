A Biblical Theology of Slavery 175

plausible conclusion, one which is appropriately ironic: “In Africa
much of the economic profits were finally dissipated because of the
common property nature of the resource. When the defense costs in-
curred by the potential quarry are added in, the total net impact of
the slave trade was almost certainly negative even to the Africans
who remained in Africa. Thus, the employers and purveyors of
slaves gained from the trade only what they could have gained in the
absence of the trade. The absence of the enforcement of private
property rights in human beings probably made the society of the
fishers of men net losers. The only group of clear gainers from the
British trans-Atlantic slave trade, and even these gains were small,
were the European consumers of sugar and tobacco and other plan-
tation crops. They were given the chance to purchase dental decay
and lung cancer at somewhat lower prices than would have been the
case without the slave trade."
God will not be mocked.

From Indentured Servitude to Racist Slavery

The issue of slavery casts a dark shadow over United States history.
The Civil War (1861-65) was fought over the three questions: 1) will
slavery be allowed to spread, 2) what to do with freed Negro slaves, and
3) should the Union be preserved? 4? After the war, Southern apologists
focused on the third question, the preservation of the Union and the
constitutionality of states’ rights, as the legal justification of secession.14*
The idea that the war had been fought to defend slavery disappeared in
the South as soon as Gen, Lee surrendered; many southerners, includ-
ing Lee himself, professed relief that the war had destroyed slavery. '*9

146. Thomas and Bean, “Fishers of Men,” op. cil., p. 914.

147, This was the classroom asscssment by the historian and remarkable Civil
War bibliographer E. B. Long. I have never heard the causes of the war better sum-
marized. Whether this was his own assessment or something he picked up from
another historian, I do not remember.

148. See, for example, the two-volume book by the former Vice President of the
Confederacy, Alexander H, Stephens, A Constitutional View of the Late War Between the
States, (Philadelphia: National Pub. Co., 1867, 1870). On Stephens, see R. J. Rush-
doony, The Nature of the American Sysiem (Fairfax, Virginia: Thoburn Press, [1963]
1978), ch. 3.

149. Richard E. Beringer, et al., Why the South Lost the Civil War (Athens: Univer-
sity of Georgia Press, 1986), p. 361. In retrospect, the South’s apologists offered
other issues as the “true” reasons for the war: the defense of white supremacy, the
Constitution (states’ rights), and Southern honor: ibid., ch. 16. Another reason, ac-
cording te Southern apologists, was the high-tariff position of the industrial North:
Robert L. Dabney, Discussions, 4 vols. (Vallecito, California: Ross House, [1892]
1980), IV, pp. 87-107. See the discussion of the 1861 Morrill tariff act in J. G. Ran-
dali and David Donald, The Civil War and Reconstruction (2nd ed.; Boston: D. G.
Heath, 1961), pp. 286-87.

 
