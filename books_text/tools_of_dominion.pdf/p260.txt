252 TOOLS OP DOMINION

possible for a daughter of a poor Israelite to marry into a family that
could afford to pay a bride price. In effect, thes option of concubinage
was a poor girl’s way out of poverty. Her father had no way to protect her
economically. If every marriage had required a dowry, she might
never have been able to marry, Her future as a mother would have
been cut off. So God graciously established a way out: concubinage.

What this points Lo is something that the Bible never says ex-
plicitly, but which Ezekiel 16 points to: the biblical requirement of
the bride price. A wife must be purchased. Without the payment of a
bride price, there can be no marriage and no future for the pros-
pective bride, The dowry was optional in the Old Testament econ-
omy; the bride price was mandatory.

Bride Price and Dowry

God gave Israel jewels and bracelets. This is reminiscent of the
gifts to Rebekah from Abraham’s servant: “And the servant brought
forth jewels of silver, and jewels of gold, and raiment, and gave them
to Rebekah: he gave also to her brother and to her mother precious
things” (Gen. 24:53), Abraham, as Isaac’s father, used his capital to
pay the girl and her relatives, The property would ultimately have
become Isaac’s, however, for it was part of his inheritance. Abraham
acted as a represeniative of his son. He supplied the bride price, and his
own agent acted in Isaac’s best interests. The gifts from Abraham
served as her dowry, and the gifts to the relatives served as a bride
price. This indicates that the bride price could be separated from the
dowry, meaning that the family could keep part of the total payment without
passing the tolal bride prive to the daughter as her dowry. This could become a
means of increasing the capital base of the family of the bride. This would
clearly have made the daughter an economic asset for her family,

There was a covenantal reason for this economic obligation on
the part of a bridegroom. The father of the prospective bride repre-
sented God to his daughter, This covenantal authority before God —
this position as God’s representative to his daughter —had to be lawfully
transferred from the father to the bridegroom. By paying the bride
price to her father, the dridegroom ritually swore to a lifetime of faithfulness to
his wife as God's representative over her, faithfulness comparable to what her
father’s faithfulness to her had been. This is precisely what Jesus swore
to God the Father in His role as the cosmic Bridegroom. He paid the
price at Calvary. God then transferred all authority over heaven and
earth to Christ as His lawful representative (Matt. 28:18-20).
