TOOLS OF DOMINION

tion that God’s revealed law is not inconsistent. But here we have
what appearf to be two rules regarding the stranger: you may not
lawfully charge the stranger interest, yet you may lawfully charge
him interest. How can we reconcile these two statements?

The answer is that the Hebrew word used in Leviticus 25:35, trans-
literated geyr [gare], is not the same as the Hebrew word in Deuteron-
omy 23:20, Similarly, ojourner” [to-shawb] is related to paw-shab,*
meaning “sit,” and implying “remain,” “settle,” “dwell,” or even
“marry.”> To-shawb therefore means resident alien. The stranger [nok-
ve] referred to in Deuteronomy 23:20 was simply a foreigner.® Two
different kinds of “stranger” are referred to in the two verses, Thus, if
the resident alicn was poor, and if he was willing to live in Israel
under the terms of the civil covenant, then he was entitled to a spe-
cial degree of civil legal protection. What was this legal protection? If
he fell into poverty, he was not to be asked to pay interest on any
loan that a richer man extended to him. With respect to usury, he
was to bc treated as a poverty-stricken Hebrew. Not so the foreigner.

What must be understood is that the economic setting is clearly
the relief of the poor, The recipient was any poor person who had fallen
into poverty through no ethical fault of his own, and who was willing
to remain under God’s civil hierarchy.

There is a parallel passage in Deuteronomy 15. Deuteronomy 15
lists the economic laws governing Israel’s national sabbatical year. In
this national year of releasc, the text literally says, all debts te neigh-
bors are to be forgiven: “At the end of every seven years thou shalt
make a release. And this is the manner of the release: Every creditor
that lendeth ought unto his neighbour shall! release it; he shall not
exact it of his neighbour, or of his brother; because it is called the
Lorn’s release” (Deut. 15:1-2). The text is clear: the neighborly loan
is the focus of the law.

At least one kind of loan was explicitly exempted by the text:
loans to non-resident foreigners: “Of a foreigner [nok-ree| thou
mayest exact it again: but that which is thine with thy brother thine
hand shall release” (Deut. 15:3). This could have bcen a traveller or

4, James Strong, “Hebrew and Chaldce Dictionary,” in ‘he Exhaustive Concordance
of the Bible (New York: Abington, [1890] 1961), p. 123.

5. ibid., p. 52.

6. ‘his is the translation given in the Revised Standard Version, the New Ameri-
can Standard Bible, und the New International Version. The alien and the so-
journer were equivalents judicially in Old Covenant law. ‘The NIV translates
Teviticus 25:35 as “an alien or a temporary resident.”
