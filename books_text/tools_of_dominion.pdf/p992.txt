984 TOOLS OF DOMINION

such a doctrine, yet that is where their amillennial pessimism in-
evitably leads. Dutch Calvinists preach the cultural mandate (do-
minion covenant), but they simultaneously preach that it cannot be
fulfilled. But biblical law is basic to the fulfillment of the cultural
mandate. Therefore, the amillennialist who preaches the obligation
of trying to fulfil the cultural mandate without biblical law thereby
plunges himself either into the camp of the chaos cults (mystics,
revolutionaries) or into the camp of the natural-law, common-
ground philosophers. There are only four possibilities: revealed law,
natural law, chaos, or a mixture.

This leads me to my next point. It is somewhat speculative and
may not be completely accurate. It is an idea which ought to be pur-
sued, however, to see if it is accurate. | think that the reason why the
philosophy of Herman Dooyeweerd, the Dutch philosopher of law,
had some temporary impact in Dutch Calvinist intellectual circles in
the late 1960s and early 1970s is that Dooyeweerd’s theory of sphere
sovereignty — sphere laws that are not to be filled in by means of
revealed, Old Testament law—is consistent with the amillennial
(Dutch) version of the cultural mandate. Dooyeweerd’s system and
Dutch amillennialism are essentially antinomian. This is why I
wrote my 1967 essay, “Social Antinomianism,” in response to the
Dooyeweerdian professor at the Free University of Amsterdam,
A. Troost. !5

Either the Dooyeweerdians wind up as mystics, or else they try to
create a new kind of “common-ground philosophy” to link believers
and unbelievers. It is Dooyeweerd’s outspoken resistance to Old Tes-
tament and New Testament authority over the content of his hypothe-
sized sphere laws that has led his increasingly radical, increasingly
antinomian followers into anti-Christian paths. You cannot preach
the dominion covenant and then turn around and deny the efficacy
of biblical law in culture, Yet this is what all the Dutch adherents to
common grace have done. They deny the cultural efficacy of biblical
law, by necessity, because their eschatological interpretations have
led them to conclude that there can be no external, cultural victory
in time and on earth by faithful Christians. Epistemological self-
consciousness will increase, but things only get worse over time.

If you preach that biblical law produces “positive feedback,” both
personally and culturally — that God rewards covenant-keepers and

15. Gary North, The Sinai Strategy: Economics and the Ten Commandments (Tyler, Texas:
Institute for Christian Economics, 1986), Appendix C: “Social Antinomianism.”
