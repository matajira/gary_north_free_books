Victim’s Rights vs. the Messianic State 3iL

eous. In calling for the death of incorrigible juvenile delinquents,
which means, therefore, in terms of case law, the death of incorrigi-
ble adult delinquents; the law declares, ‘so shalt thou put evil away
from among you; and all Israel shall hear and fear’ (Deut. 2t:21)727

What is true of this case law is true of all the other capital cases in
which this pleonasm occurs and in which the victim is the specified
agent who brings the covenant lawsuit. The victim has the option of
specifying the penalty. If the case is one in which the State lawfully
prosecutes in God’s name, then the pleonasm is binding. Execution
is mandatory.

Noah’s Covenant and Execution

Dispensational authors H. Wayne House and Thomas D. Ice
present a weak case for their speculations regarding the pre-New
Covenant legal order as it applied to the nations. They insist that
“Nowhere in the nations is capital punishment obligatorily extended
beyond the penalty for taking human life. . . .”8° They assert, though
do not prove, that none of the Mosaic law’s sanctions ever applied
directly or even was intended in principle to apply to the nations, ex-
cept the capital sanction for murder. This unique sanction is binding
on all men always, they argue, so its authority came from Noah to
Moses; it in no way went from Moses to the nations.

This is a clever attempt to escape the suggestion that in the New
Covenant era, Christians have a responsibility to pressure civil gov-
ernments to impose specific sanctions against specific crimes on the
basis of biblical revelation. Such a view of “Noahic biblical law,” if
correct, would allow Christians to avoid personal responsibility in
civil affairs, since they could not speak authoritatively in the name of
the Lord when it comes to specifying civil crimes or penalties. The
price of such a theological position regarding biblical law is, predict-
ably, the cultural, political, and judicial irrelevance of Christianity.
This is why dispensationalism is in principle culturally retreatist and
culturally irrelevant, and why no dispensationalist in over a century
and a half has published a book on Christian social ethics during the
so-called “Church Age.”

37, Rushdoony, Fnstitutes of Biblical Law, pp. 77-78; cf. p. 188. See below, pp.
410-11,

38. H. Wayne House and Thomas D. Ice, Dominion Theology: Blessing or Curse?
(Portland, Oregon: Multnomah Press, 1988), p. 90.
