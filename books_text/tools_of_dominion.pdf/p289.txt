Victim’s Rights vs, the Messianic State 281

except what has been commanded. The decision-making private indi-
vidual is tightly restricted; the centralized State is expanded. This is
the governing principle of all socialist economic planning. It assumes
the omniscience and omnicompetence of distant central planners.®

What a free society needs is predictable law.© The maximum
sanction for any crime must be specified in written law or at least in
traditional legal precedent. The criminal must know the maximum
negative consequences of conviction. He is under law, but so are his
judges. The State as well as the criminal are restrained under bibli-
cal law. The State is placed under tight judicial restraints, and first
and foremost of these restraints is the requirement that crimes and
their respective sanctions be announced in advance. There must be
no ex past facto statutes or sanctions. This reduces the arbitrary au-
thority of judges to apply sanctions or increase sanctions beyond
what is specified in the law code. They sometimes possess the au-
thority to reduce the specified sanctions, as this chapter argues, but
never to increase them. This restriction drastically reduces the
growth of arbitrary civil power. (By adhering to this biblical princi-
ple of responsible freedom under specified law, the West made possi-
ble the development of modern capitalism and its accompanying
high per capita wealth.)

The limits on the biblical State’s ability to impose arbitrary sanc-
tions are derived from three case-law principles. First, the God-given
authority of the victim to refuse to prosecute, and also his authority
to reduce the applicable sanctions upon conviction of the criminal,
restricts the power of the civil magistrate. Second, the maximum
sanction allowed by existing law keeps the State under restraint,
Third, the pleonasm of execution—“dying, he shall die”— inhibits the
authority of the judges to subsidize outrageous crimes by imposing
reduced sanctions in specific cases: where the State has lawfully initi-
ated the covenant lawsuit because there is no earthly victim who
could initiate it. To deny any of these principles is to promote the ad-
vent of the messianic State.

To describe the working of these three case-law principles, we
need to begin with the maximum civil sanction: execution. Because
public execution is the maximum civil sanction allowed by God’s
law, it has the most critics.

5. Gary North, Marx's Religion of Revolution: Regeneration Through Chans (Tyler,
Texas: Institute for Christian Economics, [1968] 1989), Appendix A: “Socialist. Reo-
nomic Calculation.”

6. FA. Hayek, The Constitution of Liberty (University of Chicago Press, 1960).

 
