532 TOOLS OF DOMINION

in eternity. God therefore threatens the whole community for its fail-
ure to impose civil sanctions against such crimes. If there were no
threat of God's sanctions against the community for the failure of the
magistrates to enforce all statutes assigned by the Bible to the civil
magistrates for enforcement, then there would be no biblical justifi-
cation for sanctions against such “victimlcss crimes” as prostitution,
pornography, and drug dealing. Because he rejects the idea of such a
covenant, classical liberal economist and legal theorist F. A. Hayek
rejects laws against “victimless crimes,” saying that they are illegiti-
mate interventions of the civil government, “At least where it is not
believed that the whole group may be punished by a supernatural
power for the sins of individuals. . . .”* But that is the whole point:
such a community-threatening God does exist.

Many actions that are specified in the Bible as sins are not to be
tried and judged by the civil magistrate, but this is not evidence of
neglect by God; it is instead a restraint on the growth of messtanic civil gov-
ernment, The absence of civil penalties against such designated sinful
behavior indicates only a postponement of judgment until the
sinner’s final and eternal restitution payment to God. Through their
public enforcement of God’s law, civil magistrates warn people of the
necessity of obeying God, the cosmic Enforcer: “By the fear of the
Lorp men depart from evil” (Prov. 16:6b). This legitimate fear is to
be both personal and national, for God’s punishments in history are
imposed on individuals and nations: “If thou wilt not observe to do
all the words of this law that are written in this book, that thou
mayest fear this glorious and fearful name, ‘He Lorv ry Gov; then
the Lorp will make thy plagues wonderful, and the plagues of thy
seed, even great plagues, and of long continuance, and sore sick-
nesses, and of long continuance” (Deut. 28:58-59).

The necessity of making restitution reminds the covenanted na-
tion to fear the God who exacts a perfect restitution payment to
Himself on judgment day, and who brings His wrath in history as a
warning of the final judgment to come. He brings His wrath either
through lawfully constituted civil government or, if civil government
refuses to honor the terms of His covenant, through such visible
judgments as wars, plagues, and famines. This is why the nation
was warned to fear God, immediately after the presentation of the
Ten Commandments: “. . . God is come to prove you, and that his
fear may be before your faces, that ye sin not” (Ex. 20:20b).

50. F.A. Hayek, Laie, Legislation and Liberty, 3 vols, (University of Chicago Press,
1973), I, p. 101.
