356 TOOLS OF DOMINION

without a change in heart, the people eventually return to their old
ways. The Revolution consumes its own children. The prophet is
again put on the run (I Ki. 19).

The humanist courts of our day appeal to religious pluralism, yet
they are creating judicial tyranny.*' The anti-feud, anti-clan,*? anti-
duel ethic of once-Christian Western bourgeois cultures — societies in
which social peace has fostered economic growth—is being under-
mined by judges who are creating lawlessness in the name of a puri-
fied humanist legal system. Judicial pluralism must be replaced, but
not from the top down, and not from the vigilante’s noose outward.
The satanic myth of legal pluralism must be replaced by the power
of the Holy Spirit in the hearts of men. The Holy Spirit is the en-
forcer in New Testament times.

Conclusion

Social order requires a degree of social peacc. When biblical law
began to influence the civil governments of the West, an increase of
social peace and social order took place. This, in turn, led to greater
economic growth and technological development.

Christian culture is orderly. The Christian West steadily abol-
ished or redirected the chaos festivals of the pagan world, until the
growth of humanism-paganism began to reverse this process.»
Legal systems became predictable, as the “eye for eye” principle
spread alongside the gospel of salvation. The unpredictable violence
of State power was thereby reduced. In private relationships, men
were not allowed to vent their wrath on each other in acts of vio-

51. Carrol D. Kilgore, judicial ‘jranny (Nashville, Tennessee: Nelson, 1977).

52. Weber wrote: “When Christianity became the religion of these peoples who
bad been so profoundly shaken in all their traditions, it finally destroyed whatever
religious significance these clan ties retained; perhaps, indeed, it was precisely the
weakness or absence of such magical and taboo barriers which made the conversion
possible. The often very significant role played by the parish community in the admin-
istrative organization of medieval cities is only onc of the many symptoms pointing to
this quality of the Christian religion which, in dissolving clan ties, importantly
shaped the medieval city.” He contrasts this anti-clan perspective with that of Islam.
Max Weber, Economy and Saciely: An Outline of Interpretive Sociology, edited by Guenther
Roth and Claus Wittich (New York: Bedminster Press, [1924] 1968), p. 1244

83. Gary North, The Sinai Strategy: Economics and the Ten Commandments (Tyler,
Texas: Institute for Christian Economics, 1986), pp. 223-26.

54, Peter Gay aptly titled the first volume of his study of the Enlightenment, The
Rise of Modern Paganism (New York: Knopf, 1966). The two-volume study is titled,
The Enlightenment: An Interpretation (New York: Knopf, 1966, 1969).
