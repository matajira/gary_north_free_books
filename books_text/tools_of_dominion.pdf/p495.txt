The Uncovered Pit 487

for which he is responsible,.”* The guardian is always responsible be-
fore God for the administration of everything under his legal authority.

Hirsch goes so far as to say that our willingness to indemnify a
victim is not enough, morally speaking; we must take care not to
allow damage in the first place. “Once you have done harm the only
thing you are able to do is to pay compensation; you can never undo
the harm and wipe out all its consequences.”* A righteous person
should become a blessing for those around him. “You, with all your
belongings, should become a blessing; be on your guard that you
and your belongings do not become a curse! Watch over all your
belongings so that they do no harm to your neighbour! And also
what you throw away or pour away—see to it that it do no harm;
you ought to bring good, so do not bring evil!” Thus, our economic
responsibility is an active responsibility. We must actively seek to
avoid harming others. It is within this moral framework that the
Bible discusses the uncovered pit.

Animals and Children

This case law deals specifically with animals. It does not mention
people. Why not? Because the pit is almost certainly located on the
land of the person who digs it, An animal that wanders onto the
man’s property has no understanding of private property rights.
Presumably, no fence has restrained it from coming onto the prop-
erty. If a fence is present, then the animal would have to knock it
down to get onto the property. The damage to the fencing would
then be the responsibility of the owner of the animal. He should have
restrained his animal. The fence in such an instance serves as the legal
equivalent of a cover. But unrestrained access to the area of the uncov-
ered pit places the responsibility on the land-owner. An animal is not
expected to honor the law against trespassing.

What holds true for an animal is also true for a young child. If
the child is not restrained by a fence or a cover over the pit, then the
owner is liable. Like an ox with a reputation for violence, so is the
uncovered pit. The owner is responsible, The parents of a child who
is killed by a fall into an uncovered pit are entitled to the same restitu-
tion as the heirs of a victim of an ox that was known to be dangerous.

3. Samson Raphael Hirsch, Horeb. A Philosophy of Jewish Laws and Observances,
wwans. I. Grunfeld (New York: Soncino, [1837] 1962), pp. 243-44, paragraph 360.

4. Ibid., p. 247, paragraph 367.

5, Ibid., p. 248, paragraph 367.
