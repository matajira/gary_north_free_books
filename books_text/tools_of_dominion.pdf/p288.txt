280 TOGLS OF DOMINION

charges against the suspected criminal only if they can persuade the
court that they have become victims by the original victim’s failure
to prosecute,

What we must understand is that in biblical jurisprudence, i ts
the victim whose rights must always be upheld, not simply because he was
harmed by the criminal, but also because he served as God's surrogate
when he became the victim. God is the primary victim, and His rights
must be upheld first and foremost. His specified judicial sanctions
must be enforced by His designated covenantal representatives. His
casc laws provide mankind with the proper guidelines of how His
honor is to be upheld in various cases.

There is another Bible-sanctioned office to consider, the office of
witness. The witness is authorized to bring relevant information to
one of these covenantal judges, so that the judge can initiate the cov-
cnant lawsuit against the suspected violator.* The witness plays a
very important role in the prosecution of God’s covenant lawsuits.
Without at least two witnesses, it is legal to execute anyone (Deut.
17:6). Also, the affirming witnesses in a capital lawsuit must be the
first people to cast stones (Deut. 17:7).

The Biblical Hierarchical Structure

Adam was allowed to do anything he wanted in the garden, except
eat from the forbidden tree. There was a specific sanction attached to
that crime, a capital sanction. This reveals a fundamental biblical
judicial principle: anything ts permitied untess it is explicitly prohibited by
law, or prohibited by an extension of a case law's principle. This principle
places the individual under public law, but it also relies on self-
government as the primary policing device. It creates the bottom-up
appeals court character of biblical society. Men are judicially free to
act however they please unless society, through its various cove-
nantal courts, has been authorized by God’s Bible-revealed law to
restrict certain specified kinds of behavior.

The bottom-up appeals court structure of the biblical hierarchy
is in opposition to the principle of top-down bureaucratic control.
Under the latter hierarchical system, in theory nothing is permitted

4. The hostility of siblings against “tattle tales” in a family is easily explainable:
youthful law-breakers resent judgment. They resent witnesses whose action brings
the dreaded sanctions. But what about parents? Parents who side with the critics of
“tattle tales” are thereby attempling to cscape their God-given role as judges. They
are saying, in principle, “We dor’t want to know about it, We don't want to serves as
judges, despite our position as God's designated representative agents in this family.”
