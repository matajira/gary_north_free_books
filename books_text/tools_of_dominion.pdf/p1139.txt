The Epistemological Problem of Social Cost 1131

tales of horror and frustration.” 3+

A Permanent Ethical Model

In contrast to economic models that are supposedly timeless
abstractions from the flux of human existence, God offers His law.
This ethical law-order was designed by God to govern His creation.
His ethical precepts were given to man as a means of subduing real-
ity, including man himself. A perfect man, Jesus Christ, walked the
earth and lived His life in terms of this revealed law. God’s law is
therefore not strictly “otherworldly,” in the sense of applying only toa
world beyond the human action, nor is it strictly “this-worldly,” in
the sense of being the product of human speculation. It is supernatu-
ral, yet delivered through revelation by God to mankind, It stands as
both an ethical foundation of human action and as a tool of domin-
ion. It explains the operations of the world to us, and it gives us
power to exercise dominion over the creation.

Sanctification: Three Steps

We say that an individual is saved through God’s imputation (ju-
dicial declaration) of Christ’s righteousness to a sinner. This is called
Justification, It is a judicial act, God’s declaration of “not guilty” by
reason of the penalty which was paid by Jesus Christ. But this judi-
cial act also has moral effects. God simultaneously sanctifies a person —
sets him apart cthically or morally — in a definitive way at the moment

134. L. M. Lachmann, “The Role of Expectations in Economics as a Social
Science,” Economica, New Series, Vol. X (February 1943), p. 16, Lachmann is the
“Austrian School” economist who has been insistent on the danger of relying heavily
on general equilibrium models. “Such smooth transition from one equilibrium
(long-run or short-run) to another virtually bars not only discussion of the process in
which we are intercsted here, but of all true economic processes. . . . And all tou
soon we shali also allow ourselves to forget that what is of real economic interest are
not the equilibria, even if they exisi, which is in any case doubtfut, but what hap-
pens between them.” Lachmann, “The Market Economy and the Distribution of
‘Wealih,? in Mary Sennholz (ed.), On Freedom and Free Enterprise, p. 186, Lachmann's
expressed hope in 1956 has not come true —in fact, the reverse has taken place: “It is
very much to be hoped that economists in the future will show themselves less inclined
than they have been in the past to look for ready-made, but spurious, coherence,
and that they will take a greater interest in the variety of ways in which the human
mind in action produces coherence out of an initially incoherent situation” (p. 187).
Nevertheless, his Kantian individualism, with the human mind serving as the en-
trepreneurial provider of coherence to an incoherent world, is as impotent to deal
epistemologically with the realities of God’s creation as are the defenders of generat
equilibrium theory.
