912 TOOLS OF DOMINION

cause Rushdoony’s Jnstitudes presented the preliminary model of the
Christian Reconstruction position. His few remarks on taxation are
found in the sections of the Jnstifutes that attempt to explain this pas-
sage. Thus, by systematically restricting any discussion of biblical
taxation to the supposed civil head tax of the Old Testament, Rush-
doony has eliminated the possibility of discussing such alternative
tax policies as the gasoline tax used exclusively for local roadways, or
income taxes lower than 10 percent, or sales taxes lower than 10 per-
cent. He has made the head tax as the sole source of civic revenue, a
conclusion unwarranted by the text and unworkable in practice.

Conclusion

The atonement money required from each adult male in Israel
prior to a holy war had nothing to do with civil taxation. It was a
unique assessment that took place only during the military census,
and the taking of such a census was authorized by God only when
war threatened the commonwealth. The State was not allowed to
conduct such a census under any other circumstances (II Sam. 24).
For the civil magistrate to have collected such a blood covering pay-
ment as a civil tax would have been an abomination. To have made
it the only civil tax in Israel, to be collected on an annual or other reg-
ular basis, would have brought the wrath of God on the State. The
collection of this mandatory payment was exclusively a priestly func-
tion. Thus, any discussion of the methods and limits of lawful civil
taxation in Old Testament Israel must be based on passages other
than Exodus 30:11-16.

R. J. Rushdoony’s discussion of the atonement money as a civil
head tax has been widely quoted by those who are not his followers
on any other issue, especially those involved in the tax-rebellion
movement, a movement which Rushdoony has repudiated. His
work on biblical law has attracted British Israelites or “Identity”
cultists who are the backbone of the tax rebellion movement. They
seek a biblical argument to justify their refusal to pay the income
tax. They argue that only a head tax is legitimate. But any appeal to
Exodus 30:11-16 to defend such a position is illegitimate. This re-
quired payment was not a head tax or any other kind of tax; it was a
blood covering for warriors-become-Nazarite priests who were about
to go into battle.
