822 TOOLS OF DOMINION

Eschatologies of the Stripped Field

Because the church has seen so few examples of successful evan-
gelism, and because even the successful examples scem to fall back
into paganism within a few centuries, Christians have come to adopt
eschatologies that deny liberation for gleaners. They see themselves
and their spiritual colleagues as people who are locked in a vicious
“cycle of poverty,” to borrow the language of paganismm’s modern wel-
fare economics. They see no hope beyond the stripped field. Life
only offers minimal opportunities for harvesting souls, they believe.
“What we have today as gleaners is all that we or our heirs can ex-
pect in history.” They lose faith in the jubilee year, when the land of
their fathers reverts to them. They lose faith in the ability of the
heavenly Observer to identify and hire good workers and to place
them in new positions of responsibility. So, Christians have invented
eschatologies that conform to their rejection of any vision of tempo-
ral liberation —eschatologies of the siripped field. Men with battered
spirits preach that nothing Christians can do as spiritual gleancrs
will ever fill the sacks to overflowing. They see no covenantal cause-
and-effect relationship between gleaning and liberation. They
preach a new gospel of the kingdom — the kingdom of perpetual leftovers.
They do not recognize that there is a valid historical function of
gleaning: the public identification of those bondservants who actively
seek liberation and who pursue every legitimate avenue of escape.

New Testament Applications

In Israel, the sabbatical year of release was national and simul-
taneous. It was a negative injunction, and therefore a civil law, for it
forbade something that was a positive evil: working the land without
a break. We know what an appropriate penalty might have been:
double restitution of that year’s harvest, with the produce going to
the priests as a payment to God. To pay that, the owner would prob-
ably have had to sell himself into slavery: a symbol of the transition
from grace to wrath, a symbolic return to Egypt.

Today, there is no common year of release, nationally or interna-
tionally. The reason for this lack of a common year of release is be-
cause the enforcement of the New Testament sabbath has been de-
centralized. God now assigns to individuals the responsibility of
deciding how to observe the sabbath. This decentralization of the
locus of enforcement has led to the abolition of a common sabbatical
