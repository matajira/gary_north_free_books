214 TOOLS OF DOMINION

codes to such matters as dowrics and bride price payments is evi-
dence of this primitivism.! But it is the modern world that is primi-
tive, for it has abandoned a covenantal view of marriage, and has
substituted casily broken mutual contracts, where fathers have no
responsibilities to investigate the economic competence of pros-
pective sons-in-law, and wives have little legal protection from the
courts if husbands decide to break their marriage contracts.’
Women have become the economic victims of divorce,”

The Family as the Primary Protection Agency

Marriage is not lawless. It is a covenantal institution. It is the
primary training ground for the next generation. It is the primary
institution for welfare: care of the young, care of the aged, and edu-
cation, It is the primary agency of economic inheritance. The family is
therefore the primary institutional arrangement for fulfilling the terms of the do-

10. The Hammurabi Code devotes considerable space to these matters, para-
graphs 128-84, Ancient Near Lastern Texts Relating lo the Old Testament, edited by James
B. Pritchard (3rd ed.; Princeton, New Jersey: Princeton University Press, 1969),
pp. 171-74. Not equally detailed are the laws of Eshnunna, paragraphs 17-28; ibid.,
P- 162; the Middle Assyrian laws, paragraphs 25-48: ibid., pp. 182-84; and the Hit
tite laws, paragraphs 26-36: ibid., p. 190.

H. In Vicwrian England, custody of the children automatically went to the
divorced husband. This reduced the incentive for divorce on both sides. The hus-
band feared the responsibility of taking care of the children, and the wife did not
want to abandon them. As William Tucker comments: “The Victorian system
favored neither men nor women: Ii favored families. . . . They loaded the system
against the individual interests of men and women to keep both committed to the
family.” Only after 1910 did social workers and the courts shift the balance and begin
to grant mothers automatic custody of the children. William~Tucker, “Victorian
Savvy,” New York Times ( June 26, 1983). The biblical approach is different: children
go to the innocent victim of the sinning marriage partner,

12. Economic studies made in the 1980's indicate that in the United States,
women who got divorced yaw their standard of living drop by over 70 percent within
a year, while divorced men saw an increase of over 40 percent in the same period.
Lenore J. Weitaman, The Divorce Revolution: ‘The Unexpected Social and Economic Conse-
quences for Women and Children in America (New York: The Free Press, 1985), p. xii. As
she writes, “the major economic result of the divorce law revolution is the systematic
impoverishment of divorced women and their children, ‘hey have become the new
poor.” Ibid., p. xiv; cited by George Grant, The Dispossessed: Homelessness in America
(Ft. Worth, Texas: Dominion Press, 1996), p. 79. In 1940, one out of six marriages
ended in divorce in the U,8.,; in 1980, it was 50 percent. Grant comments: “With no-
fault divorce laws in place, depriving women of alimony, child custudy support, or
appropriate property setllements, we can expect the feminization of poverty to con-
tinue to escalate exponentially” (p. 79).

13. Sutton, That You May Prosper, ch. 8. The code of Hammurabi specified that an
aristocrat who acquired a wife without contracts for her did not have a wife: para-
graph 128. Ancient Near Basten Texts, p. 171.

 
