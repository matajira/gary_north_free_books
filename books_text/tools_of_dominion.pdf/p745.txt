The Prohibition Against Usury 737

inate the primary biblical objection against collateralized debt: the
subsidy that monetary inflation offers to debtors, They could not pay
off their debts with depreciated money.

What about unsecured debt? That has to be the decision of the
lender. Are the risks worth it? He decides. He should have the legal
right to extend credit. The creditor believes that debt is to his advan-
tage. The Bible says that such personal debt is best avoided (Rom.
13:8), but it does not forbid debt. In some cases, debt may actually
be to the benefit of the debtor. Debt to finance a higher education is
one example. But the debtor must always understand that by taking
an unsecured debt, he is risking disgrace. He has in principle be-
come a bondservant (Prov. 22:7).

In a biblical social order, a defaulting debtor would be required
to sell everything he owns to pay his creditors. “The wicked bor-
roweth, and payeth not again: but the righteous sheweth mercy, and
giveth” (Ps, 37:21), There must be sanctions against such public
wickedness as defaulting on a loan. When a person declares bank-
ruptcy, he is publicly announcing that the total value of his posses-
sions is insufficient to repay his creditor or creditors. He violates the
terms of the loan’s contract if he retains any personal assets after
declaring bankruptcy. He must turn over everything he owes to his
creditor up to the amount specified in the contract. (Some societies
may allow him to retain some of his possessions, but this exception
was known to lenders beforehand, and the added risk to the creditor
was already built into the loan’s risk premium.) He cannot legiti-
matcly be sold into indentured servitude unless this was specified in
the loan contract, and if it was, then the loan had to be a zero-inter-
est‘ charity loan, as I have argued above (“Defining Poverty by
Statute”). (There should be little doubt that the abolition of debtors
prison in the West during the late-nineteenth century was an act in
conformity with biblical law’s standards of debt and repayment.)

If such laws were on the statute books, there would be a lot less
consumer debt.

Collateral

The lender is permitted to take a poor man’s cloak as collateral,
but the cloak must be returned at night. This is a strange form of col-
lateral, since the lender cannot use it when it is most needed. Its pur-
pose is two-fold. First, to restrict loans of charity to local regions when-
ever possible. Lenders are supposed to be in close contact with bor-
rowers. They should know their character. Lenders are very likely
employers. They can distinguish a true emergency from a disguised
