A Biblical Theology of Slavery 169

Satan’s now-permanent followers, meaning eternally condemned
sinners, either dead humans or fallen angels? '?°

Christians must openly acknowledge and gain courage from the
fact that God’s enemies are dealing with a God so powerful that He
has already condemned to hell the vast majority of all those people
who have died in the past. Christians know this; they have sent mis-
sionaries all over the world to help prevent a repetition of this; but
they seldom like to think about it in its grim details. God is presently
iorturing the vast majority of those who have ever lived, and He will continue to
do so forever.!°\ Christians should never forget this, for it reflects in

130. One answer, based on biblical psychology, is that we are humans rather than
angels. Our sympathies are instinctively with deceased humans. We may be occa-
sionally tempted to worry about deceased friends or relatives who we are sure have
died outside God’s covenant. This is wasted worry. Once departed from this life,
covenant-breakers retain none of their former personal gifts (restraints on evil) of
God’s common grace; they are now wholly self-consistent in their total rebellion
against God. Their total depravity is now unrestrained. There is no longer anything
eft of them that is worth loving, caring about, or worrying about. Sentimentalism is
legitimate regarding our memories of the common grace-blessed lives of now-
deceased covenant-breakers during their stay on earth; it is a sinful denial of the
faith when indulged in concerning their present condition or location, We arc to take
seriously in our own thinking the events of Ezekiel 9, Gad’s sending of the faithful
destroyers through the streets of Jerusalem, whom He instructed; *. . . let not your
eye spare, neither have ye pity: slay utterly old and young, both maids, and litile
children, and women” (vv. 5b-6a). Mercy for covenant-breakers ends forever on the
far side of death’s door. For them, Christians cannot lawfully extend the false hope
and judicially inappropriate blessing, “rest in peace.”

131. The right to inflict torture on anyone is an exclusive prerogative of God in
His office of cosmic Judge, which is why men and angels are not supposed to imitate
Him in this practice. This is the theological basis of civil laws against torture, which
was constantly denied in Christian Europe for centuries until the era of the French
Revolution; John H. Langbein, Torture and the Law of Prof: Europe und England in the
Ancien Regime (University of Chicago Press, 1977). It is also why God-denying, self-
consistent Marxist societies use torture as a normal implement of social policy: they
see the State as the agent of the Communist Party as having replaced God, includ-
ing His office as Judge. For details, see Sidney Bloch and Peter Reddaway,
Psychiatric Terror; How Soviet Psychiatry Is Used to Suppress Dissent (New York: Basic
Bovks, 1977). Solzhenitsyn describes techniques of torture used by Soviet inter-
rogators ta force people to give up their gold in the late 1920's: Aleksandr Solzhenitsyn,
The Gulag Archipelago, 1918-1956, pp. 53-54. Medvedev dates the beginning of torture
as public policy with the collectivization of Soviet agriculture, which he dates as be-
ginning in 1930. Roy A. Medvedev, Let History Judge: The Origins and Consequences of
Stalinism (New York: Knopf, 1971), p. 398. Robert Conquest argues that torture be-
came official policy in 1937 for gaining confessions out of accused Party members,
although the great purge had begun in 1934: Conquest, Je Great Terror: Statin’s Purge
of the Thirties (New York: Collier, [1968] 1973), p. 195. Medvedev describes the prac-
tices used during the purge: of. cif., ch. 8; cf. pp. 303, 476-77. For an account of
more recent Communist torture, see the book by the Cuban refugee who spent over
two decades in Cuban prisons, Armando Valladares, Against All Hope (New York:
Knopf, 1986).
