640 TOOLS OF DOMINION

the goods loaned out. All property is God’s; He delegates certain
rights and responsibilities to specific people. The goal of this delega-
tion of this stewardship system is to extend God’s dominion on earth.
Thus, ownership has inescapable legal implications, that is, cove-
nantal implications. There cannot be ownership without legal re-
sponsibility. These laws set forth the limits of the “bundle of rights”
in three types of lending transactions: 1) when the owner or his agent
accompanies his property, 2) when he does not accompany his prop-
erty, and 3) when he rents his property for a fee. In the first case, the
rights and therefore responsibilities of ownership remain with the
owner. In the second, they shift to the borrower. In the third, they
remain with the owner.

‘There is a system of strict liability operating here. The borrower
assumes risks when he borrows a work animal. He is asking another
person to give him something free of charge. He is asking for grace.
The borrower becomes responsible for the proper administration of
the other person’s property. If the animal dies of natural causes, the
borrower has to repay the owner. Who can be sure what killed it?
Was it overworked or not? When the borrower asks for grace from
his neighbor, he must not expect unlimited grace. Biblical law estab-
lishes the limits of his responsibility. !?

Conclusion

This section of the case laws refers (o the voluntary, charitable
care of a neighbor’s animals. Rules are established regarding the ex-
tent of personal responsibility for the caretaking of animals. There is
no penalty imposed on the caretaker if an animal is carried off when
no one sees it, if he swears before the judges that he has not stolen
the animal. He is responsible for restoring, like for like, any animal

12. Maimonides argued that if the animal died of natural causes during normal
work activities, the borrower is exempt: Civil Laws, “Lreatise I, Laws Concerning
Borrowing and Depositing,” Chapter One, Section One, p. 52. Incredibly, he argued
that if a man asks another man for a drink of water and also to borrow his work ani-
mal, no matter what happens to the animal, he owes the lender nothing. Why? Because
this is a case of “the owner thereof be with it” (Ex, 22:13), “Whether the com-
modatary borrowed the services of thc owner or hired them, whether he borrowed
the services for the sarne work, or for other work, or for anything in the world . . . it
is a case of borrowing with the owner and the commodatary is quit. If, however, he
borrowed the animul first, and then the owner gave him water to drink, it is not a
case of borrowing with the owner. And so it is in all similar cases.” Jbid,, Chapter
Two, Section One, p. 55. This sort of reasoning places barriers of extreme legalism
in hetween neighbors. Legal technicalities can overwhelm personal relationships.

 
