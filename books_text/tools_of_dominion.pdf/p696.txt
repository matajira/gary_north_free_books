688 TOOLS OF DOMINION

of the tithe are to be upheld by ecclesiastical law. There is no New
Testament evidence that either tithing or gleaning has been abol-
ished as a moral and ecclesiastical requirement. Gifts to the poor, we
are told, are made to God, and He promises to repay them (Prov.
19:17). He brings positive sanctions to those who obey His positive
injunctions. He leaves to the priests the task of imposing moral sanc-
tions for His positive injunctions. For example, the church enforces
the tithe, and it grants a positive sanction to those who pay: the right
of voting membership. The church also supports poor widows when
relatives cannot or refuse to do so (I Tim. 5:3-10). It excommuni-
cates relatives who can support widows but refuse, for they are worse
than infidels (I Tim. 5:8).

 

Limited Knowledge

A voluntary exchange can be oppressive to a weaker party, bibli-
cally speaking, even though economic analysis does not provide the
civil or ecclesiastical authorities the guideline, and therefore the abil-
ity, to render lawful judgment in specific cases. Why is the institu-
tional government limited? Because there are limits on the knowledge
watlable to observers of any economic transaction. Each party
entered the transaction hoping to benefit. Sometimes men may cry
“oppression” when they are secretly pleased with the bargain: “It is
naught, it is naught, saith the buyer: but when he is gone his way,
then he boasteth” (Prov. 20:14). No man can measure another man’s
subjective benefits; no man or committee of men can compare the
gains of each party in a voluntary exchange.” But God can make
such estimations, as Jesus demonstrated when He assessed the ex-
tent of the economic sacrifice of the widow who gave away her two
“mites,” or small coins (Mark 12:42-44). The fact that the authorities
are not omniscient does not relieve sharp bargainers from their obli-
gation of being alert to the weak position of the defenseless, and to
make adjustments in favor of the weak in their exchanges with them.

By not seeking maximum profits in such transactions, strong
bargainers thereby grant a non-humiliating form of charity. A good bar-
gainer always seeks to guess what the other man is willing and able
to pay. If he is confident in his ability to make this exceedingly diffi-
cult estimation, then he should have comparable confidence in his
ability to make an estimation of how much the other, weaker bar-
gainer may seed.

25. North, Dominion Covenant: Genesis, ch. 4: “Economic Value: Objective and
Subjective.”
