114 TOOLS OF DOMINION

kingship. . . . Whatever opposition is offered against His kingdom
will be crushed absolutely. . . . The point of the quotation here is
that the Christian overcomers, in this age, are promised a share in
the messianic reign of Jesus Christ, in time and on earth. In spite of
all opposition, God has set up His King over the nations (cf. Ps.
2:1-6), Those who are obedient to His commands will rule the world,
reconstructing it for His glory in terms of His laws. Psalm 2 shows
God laughing and sneering at the pitiful attempts of the wicked to
fight against and overthrow His Kingdom. . . . The nation that will
not serve us will perish (Isa. 60:12); all the peoples of the earth will
be subdued under our feet (Ps. 47:1-3)— promises made originally to
Israel, but now to be fulfilled in the New Israel, the Church.”*

Chilton’s Days of Vengeance is clearly a postmillennial and theo-
nomic book. Kline totally rejects both positions. He has adopted
Mendenhall’s theologically innocuous phrase, “historical prologue”
to describe this section of the covenant model, The phrase softens
the obvious hierarchical and dominical aspects of point two of the
biblical covenant. It was Sutton’s lecture and preliminary essays on
the covenant model that persuaded Chilton to restructure his Reve-
lation commentary into a five-point model. Thus, Chilton’s self-
conscious adoption of Kline’s phrase,? rather than Sutton’s language
of hierarchy, is misleading; Chilton’s terminology makes its appear
that Days of Vengeance is closer to Kline’s theological use of point two
than to Sutton’s, which is obviously not the case, as Kline will be the
first to admit. Sutton’s That You May Prosper and Chilton’s Days of Ven-
geance stand as eloquent refutations of Kline’s amillennial rejection of
the God-enforced cause-and-effect relationship between ethics (bibli-
cal law) and temporal eschatology (postmillennialism). Sutton is
open about this difference of interpretation; !° Chilton is not.

Old Testament Slavery Was Covenantal

“Man is born free; and everywhere he is in chains.”"! So begins
Rousseau’s Social Contract (1762), perhaps the single most influential

8. Ibid., p. 117.

9. Part Two: “Historical Prologue: The Letiers to the’ Seven Churches,” zhid., p. 85,

10, Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas:
Institute for Christian Econamics, 1987), Appendix 7; “Meredith G. Kline: Yes and
No.”

11, Jean Jacques Rousseau, The Sacial Contract (1762), ch, I. L am using the Cole
edition in Dent’s Everyman's Library.
