1084 TOOLS OF DOMINION

This perception of the magnitude of the debate has been lost on
modern Bible scholars. Humanists have rewritten history in order to
downplay the importance of the Bible in Wesiern thought and culture.
Evangelical Christians have generally agreed to this view of Western
history, almost by default. Members of the evangelical scholarly world
have been trained by the humanists who control access to the major
institutions of higher learning (i-e., trade union certification). At the
same timc, laymen in the pews have also accepted the humanists’ view
of the peripheral nature of the Bible’s influence in the early modern
history because such a view of the Bible’s lack of relevance in history
conforms to the mind-set of what has been called the left wing of the
Reformation: Anabaptist pietism. This tradition has been at war with
Old Testament law from the beginning. Indeed, this movenent was one
of the forerunners of higher criticism, for it contrasted the Bible with
the inner testimony of man’s spirit, and elevated the latter over the for-
mer. This legacy of the internalization of the Word of God triumphed
in the modern church through the influence of twentieth-century
fundamentalism: grace over law.>? Once again, we see evidence of the
implicit alliance between the power religion and the escape religion.

It is time for Christian scholars of the Old ‘Llestament to stop their
fruitless shadow-boxing with higher critics who will no more listen to
Bible-detending scholars than they have listened to Moses and Christ.
It is time for orthodox Bible scholars to go to the Pentateuch to find
gut what it says, not to discover some new bit of evidence that Moses
really and truly did say it. There is no doubt a place in the division of
intellectual labor for linguistically skilled Christians to defend the in-
tegrity of the Bible against the incoherent slanders of higher critics,
but this technical task should be put on a low-priority basis. What we
do need is @ great deal of research on the chronology of the Pentateuch
—not on when Moses wrote the Pentateuch, but on what was going
on in the surrounding nations at the time of the exodus. We need a
reconstruction of ancient chronology, one based on the presupposition
that the Bible gives us the authoritative primary source documents,
not Egypt or Babylon. Such a project would keep a lot of linguistically
skilled scholars productively busy for several generations.

Meanwhile, let the higher critics drown in their own footnotes,
the way that Arius died by falling head-first into a privy.™ Let the
dead bury the dead, preferably face down in a scholarly journal.

 

 

52. Reventlow, Authority of the Bible, ch. 3.

53. Frank, Less Than Conquerors.

54, R. J. Rushdoony, Foundations of Social Order: Studias in the Creeds and Councils of
the Early Church (Fairfax, Virginia: Thoburn Press, [1969] 1978), p. 17
