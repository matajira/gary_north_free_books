Criminal Law and Restoration 407

Western Europe abandoned debtors prison during the decade
1867-77.”6 Legislators at last recognized that it did victims no good to
see a debtor cast into prison until he paid, since he could not earn his
way out. It is not coincidental that Europe passed such legislation in
the same era that the United States and Russia abolished slavery,
another system that also did not provide a way for people to buy
their way out.

The ultimate earthly prison is the concentration camp. While the
modern Soviet camp has economic functions, the cruelty of long sen-
tences is obvious. Under Stalin, these sentences were incredibly gro-
tesque. As many as 30 million people were sent into the camps, never
to return.”? The magnitude of the crime against humanity seems irra-
tionally cruel.”® They were irrational, according to Solzhenitsyn.
The first thought of the arrested person was always, “Me? What
for?”?5 From 1934 on, a soldier captured in wartime was automatic-
ally given a ten-year sentence upon being freed from the enemy.®°
Encircled military units got ten-year sentences after 1941.8! Failure to
denounce specified evil acts carried an indeterminate sentence.
Quotas for arrests made the diversity of the camps fantastic, he says;
there was no logic to them.® A chance meeting with a condemned
man could get you ten years.*t Owning a radio tube was worth ten
years.® In 1948, the average sentence increased to 25 years;
juveniles received ten.*

The classic story he tells was of a district Party conference in
Moscow Province. At the end of the conference, someone called for
a tribute to Stalin. A wave of applause began, and continued.
Everyone was afraid to be the first person to stop clapping, for fear of

76. France abolished debtors prison in 1867; England abolished it by the Debtors
Act of 1869. Ireland followed in 1872, Scotland in 1880. Switzcrland and Norway
abolished it in 1874, Italy in 1877, “Debt,” Encyclopaedia Britannica (1th ed.; New
York: Encyclopaedia Britannica, Inc., 1910), VII, p. 906.

77. Robert Conquest, The Great Terror: Stalin’s Purges of the Thirties (rev. ed.; New
York: Collier, 1973), p. 710.

78. Van den Haag, Punishing Criminals, p. 43.

79. Aleksandr Solzhenitsyn, The Gulag Archipelago, 1918-1956: An Experiment in
stigation, I-17 (New York: Harper & Row, 1974), p. 4.

 
 

82. fbid., pp. 67, 363.
83. Jbid., p. 71.
84. Thid., p. 73.
85. Zbid., p. 78.
86. Tbid., p. 91.
