682 TOOLS OF DOMINION

The question at hand, therefore, is this: To what extent is the
practice of oppression or affliction a matter of civil jurisdiction?
What is the responsibility of the civil government in suppressing eco-
nomic oppression by means of its legal monopoly of violence? Fur-
thermore, is an ecclesiastical court responsible in some way to step in
and call a halt to economic oppression? Will the criteria used by ec-
clesiastical courts be different from those used by civil courts? Such
questions have baffled Christian commentators for centuries.

Criteria of Oppression

What, precisely, are criteria of economic oppression? The medieval
scholastic theologians struggled long and hard with questions
relating to the “just price,” and “usurious loans.” What is a “fair
profit”? Without exception, the analytical attempts of the scholars
failed to survive the test of applying the criteria. The late-medieval
scholastic theologians actually defined the “just price” as the competi-
tive markel price, so long as the market price was not the result of price
fixing by public or monopolistic concerns. 7

The same problem disrupted the attempts of the early New England
Puritans to establish formal standards of economic justice. A famous
instance was the trial of Capt. Robert Keayne, a Boston merchant,
who was convicted in 1639 of having taken unjust profits on the sale
of foreign commodities (specifically, above 50 percent in some in-
stances, and above 100 percent in others).'® The fine was sei by the
deputies (the “lower” court, or lower chamber of the legislature-court
of the Massachusetts Bay Colony) at £200; the magistrates (“upper”
court) reduced it to £100.

He was punished, Gov. Winthrop argued, because the colony’s
leaders were determined to take action, “For the cry of the country
was so great against oppression, and some of the elders and magis-

17. Marjorie Grice-Hutchinson, The School of Salamanca: Readings in Spanish Mone-
tary Theory, 1544-1605 (Oxford: The Clarendon Press, 1952); Joseph Schumpeter,
Tiistory of Economic Analysis (New York: Oxford University Press, 1954), pp. 98-99;
Raymond de Roover, “The Concept of the Just Price: Theory and Economic Policy,”
Journal of Economic History, XVII (1958), pp. 418-34; Murray N, Rothbard, “Late
Medieval Origins of Free Market Economic Thought,” journal of Christian Recon-
struction, IL (Summer 1975); Alejandro A. Chafuen, Christians for Freedom: Late-
Scholastic Economics (San Francisco: Ignatius, 1986), ch. 7.

18. The account of bis conviction is found in the diary of Gov. John Winthrop:
Winthrop’s Journal: “History of New England,” 1630-1649, edited by J. Franklin Jamison,
2 vols. (New York: Barnes & Noble, [1908] 1966), I, pp. 315-26.
