CONCLUSION 251

the separation between wheat and tares must come
at the end of history, not a thousand years before the
end (or, in the dispensational, pretribulational, pre-
millennial framework, 1007 years before). I have
alienated postmillennial pietists who read and de-
light in the works of Jonathan Edwards by arguing
that Edwards’ tradition was destructive to biblical
law in 1740 and still is. It leads nowhere unless it
matures and adopts the concept of biblical law as a
tool of victory. I have alienated the Bible Presbyter-
ian Church, since its leaders deny the dominion cov-
enant. I have alienated Greg Bahnsen by implying
that one of his published arguments isn’t consistent,
and even worse, that one of Meredith Kline’s anti-
Bahnsen arguments is. Have I missed anyone? Oh,
yes, I have alienated postmillennial Arminians
(“positive confession” charismatics) by arguing that
the rebels in the last day are not simply backslidden
Christians,

Having accomplished this, I hope that others will
follow through on the outline I have sketched relat-
ing common grace, eschatology, and biblical law.
Let those few who take this book seriously avoid the
theological land mines that still clutter up the land-
scape. There are refinements that must be made,
implications that must be discovered and then
worked out. I hope that my contribution will make
other men’s tasks that much easier.
