198 DOMINION AND COMMON GRACE

common task of all men, subduing the earth. Thus,
there can be cooperation between Christians and
non-Christians.

Conclusion

The Fall of man was ethical, not intellectual.
Men’s minds are under a curse, for man himself is
under a curse, but the problem with man’s mind is
primarily ethical. Thus, Christians can use the tech-
nical skills and specialized knowledge of the unbe-
lievers, just as unbelievers can use the Christian's
talents. The division of labor through voluntary
market exchange helps each group build up its re-
spective kingdom.

We can cooperate with the enemy in positive
projects because of common grace. Our long-term
goals will be achieved because we have special grace.
We can set the agenda. We have the ethical goods;
they have the ethical “bads.” They want the benefits
of biblical social order. They can be the hewers of
wood and drawers of water (Josh. 9) until the day
when they at last rebel, and God crushes them for all
eternity.

In summary:

1, Both wheat and tares develop to maturity.

2. There is ebb and flow in the expansion
of God's visible earthly kingdom.

3. There is creedal progress.

4, Covenant-breakers also develop episte-
mologically,

5. Power-seekers do not work out in prac-
