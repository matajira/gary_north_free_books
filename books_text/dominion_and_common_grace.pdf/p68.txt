52 GOMINION AND COMMON GRACE

returns to condemn them on the day of judgment,
heaping coals of fire on their heads. On the other
hand, the common grace of God in law also must be
seen as a part of the program of special grace to His
people. God's special gifts to His people, person by
person, are the source of varying rewards on the day
of judgment (I Cor. 3:11-15). Similarly, common
grace serves to condemn the rebels proportionately
to the benefits they have received on earth, and it
serves as the operating backdrop for the special grace
given to God's people.

The laws of God offer a source of order, power,
and dominion. Some men use this common grace to
their ultimate destruction, while others use it to their
eternal benefit. It is nonetheless common, despite its
differing effects on the eternal state of men.

The Good That Men Do

The Bible teaches that there is no good thing in-
herent in fallen man; his heart is wicked and deceit-
ful (Jer. 17:9). All our self-proclaimed righteousness
is as filthy rags in the sight of God (Isa. 64:6).
Nevertheless, we also know that history has mean-
ing, that there are permanent standards that enable
us to distinguish the life of God-hating Communist
Joseph Stalin from the life of God-hating pantheist
Albert Schweitzer. There are different punishments
for different unregenerate men (Luke 12:45-48).
This does not mean that God in some way favors one
lost soul more than another, [t only means that in the
eternal plan of God there must be an eternal affirmation
of the validity and permanence of His law, It is worse to be
