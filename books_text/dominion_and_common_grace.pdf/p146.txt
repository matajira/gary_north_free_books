130 DOMINION AND COMMON GRACE

The difference between the humanist power-
seekers and the more fully consistent but suicidal
tribal pagans is the difference between the Commun-
ists and the Ik. It is the difference between power re-
ligion and escape religion. Some Eastern mystic
who seeks escape through ascetic techniques of with-
drawal, or some Western imitator with an alpha
wave machine and earphones (“Become an instant
electronic yogi!”), is acting far more consistently
with the anti-Christian philosophy of ultimate mean-
inglessness than a Communist revolutionary is. The
yogi is not fully consistent: he still needs discipline
techniques, and discipline implies an orderly uni-
verse. But he is more consistent than the Commun-
ist. He is not seeking the salvation of a world of com-
plete iltusion (maya) through the exercise of power.

Satan needs a chain of command in order to ex-
ercise power. Thus, in order to create the greatest
havoc for the church, Satan and his followers need to
imitate the church. Like the child who needs to sit on
his father’s lap in order to slap him, so does the rebel
need a crude imitation of God’s dominion theology
in order to exercise power. A child who rejects the
idea of his father’s lap cannot seriously hope to slap
him. The anti-Christian has officially adopted an
“anti-lap” theory of existence. He admits no cause-
and-effect relationship between lap and slap. To the
extent that he acts consistently with this view, he be-
comes impotent to attack God's people.

18. Gary North, Moses end Pharaoh: Dominion Religion vs. Power
Religion (Tyler, Texas: Institute for Christian Economics, 1985),
Introduction.

 

 
