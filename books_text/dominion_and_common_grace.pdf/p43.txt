THE FAVOR OF GOD 27

If thine enemy be hungry, give him bread
to eat; and if he be thirsty, give him water to
drink: For thou shalt heap coals of fire upon
his head, and the Lord shall reward thee.

Why are we to be kind to our enemies? First, be-
cause God instructs us to be kind. He is graciously
kind to them, and we are to imitate Him. Second, by
showing mercy, we thereby heap coals of fire on their
rebellious heads. From him to whom much is given,
much shall be required (Luke 12:47-48). Our enemy
will receive greater punishment through all eternity
because we have been merciful to him. Third, we
are promised a reward from God, which is always a
solid reason for being obedient to His commands.
The language could not be any plainer. Any discus-
sion of common grace which omits Proverbs 25:21-22
(Romans 12:20) from consideration is a misleading
and incomplete discussion of the topic. And I hasten
to point out, Van Til never mentions it.

Love and Hate in Biblical Law

he Bible is very clear. The problem with the
vast majority of interpreters is that they still are in-
fluenced by the standards of self-proclaimed autono-
mous humanism. Biblically, love is the fulfilling of the
law (Rom. 13:8). Love thy neighbor, we are in-
structed. Treat him with respect. Do not oppress or
cheat him. Do not covet his goods or his wife. Do not
steal from him, In treating him lawfully, you have
fulfilled the commandment to love him. In so doing,
you have rendered him without excuse on the day of
