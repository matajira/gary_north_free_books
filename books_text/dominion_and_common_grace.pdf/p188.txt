172 COMINION AND COMMON GRACE

in the West, leading to an extension of common grace
throughout Western culture. Economic growth has in-
creased; indeed, the concept of linear, compound
growth is unique to the West, and the theological
foundations of this belief were laid by the Reforma-
tion. Calvin had distinctly postmillennial leanings,®
although these were partially offset by a degree of
amillennial pessimism.”

It was during the period 1560-1640 that many of
the English Puritans adopted postmillennialism,®
and this doctrine was fundamental in changing the
time perspective of the Puritan merchants who laid
the foundations of modern capitalism. Longer life-
spans have also appeared in the West, primarily due
to the application of technology to living conditions.
Applied technology is, in turn, a praduct of Christi-
anity and especially Protestant Christianity.?

In the era prophesied by Isaiah, unbelievers will
once again come to know the benefits of God's law.
No longer shall they almost totally twist God’s reve-

6. Greg L. Bahnsen, “The Prima Facie Acceptability of Post-
millennialism,” Journal of Christian Reconstruction, WL (Winter
1976-77), pp. 69-76.

7. Gary North, “The Economic Thought of Luther and Cal-
vin, ibid., IL (Summer 1975), pp. 103-5.

8. James R. Payton, Jr., “The Emergence of Postmillennial-
ism in English Puritanism,” Journal of Christian Reconstruction, VI
(Summer 1979).

9. Robert K. Merton, Social Theory and Social Structure (rev.
ed.; New York: Free Press of Glencoe, 1957), ch. 18: “Puritan-
ism, Pictism, and Science”; E. L. Hebden Taylor, “The Role of
Puritanism-Calvinism in the Rise of Modern Science,” fournal of
Christian Reconstruction, VI (Summer 1979), Charles Dykes,
“Medieval Speculation, Puritanism, and Modern Science,” ibid.
