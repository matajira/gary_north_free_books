256 DOMINION AND COMMON GRACE.

defensive fight would turn out.

Machen’s challenge to the modernists was front-
page news in the New York Times, from 1923 until he
and his faithful little band of 34 mostly younger
pastors— what the Church calls “teaching elders”
17 ruling elders, and 79 laymen left the Church in
June of 1936. Today, we find it difficult to believe
that theology was a major issue in the secular press,
but it was, insofar as theological issues determined
who would control the funds, boards, and influence
of the denominations in the inter-war decades.

Warfield’s name had not been featured in the
newspapers of his day, for he was content to remain
at his calling. He rallied no troops, issued no mani-
festos, and appealed no judicial decisions through
the Presbyterian court system. What he did was to
lay down an intellectual and theological foundation
that might be used in the future, he believed, to re-
construct the entire ecclesiastical order, and after
that, the world.

“Old Princeton”

Tt is generally acknowledged that Princeton
Theological Seminary was, from its founding in the
early nineteenth century until Machen’s departure
in 1929, the world’s leading academic institution of
conservative Protestant scholarship. It was almost a
family enterprise, so dominant were the names of
Hodge and Alexander in the nineteenth century.
Benjamin B. Warfield was the last of these giants
whose name is exclusively associated with Princeton.
