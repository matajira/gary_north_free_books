THE INSIDEMAN 229

prehensive vision of secular salvation for society, it
can compete successfully with Christianity, especially
the escapist versions of Christianity. The Communist
liberation theologian José Miranda is self-conscious
about the ineffectiveness of escapist Christianity:

Now, the Matthean expression “the king-
dom of the heavens” was the only one serving
the escapist theologians as pretext for maintain-
ing that the kingdom was to be realized in the
other world. Not even texts about glory or en-
tering into glory provided them any support,
for the Psalms explicitly teach, “Salvation sur-
rounds those who fear him, so that the glory
will dwell in our land” (Ps. 85:10)."

Hence what paradise might be, or being
with Christ, or Abraham’s bosom, or the heav-
enly treasure, is a question we could well leave
aside, because what matters to us is the defini-
tive kingdom, which constitutes the central
content of the message of Jesus. The escapists
can have paradise.”

To speak of a kingdom of God in the other
world is not only to found a new religion with-
out any relationship with the teaching of Christ
(for none of the texts wielded by escapist theol-
ogy mentions the kingdom); it is to assert ex-
actly the contrary of what Christ teaches: “The

18, José Miranda, Communism in the Bible (Maryknoll, New
York: Orbis Books, 1982), p. 14.
19, Ibid, p. 15.
