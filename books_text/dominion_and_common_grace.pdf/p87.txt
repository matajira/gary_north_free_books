WHEAT AND TARES: HISTORICAL CONTINUITY 71

supposed God-given right of power-secking God-
haters to impose “neutral” humanist civil law on
Christians. These Christians defend humanist civil
Jaw in the name of freedom. They say that biblical
Jaw in the hands of Christians will always lead to tyr-
anny. (But they admit readily that biblical law in the
Old Testament, before the resurrection of Christ,
and before He sent the Holy Spirit to lead His
church into all truth, was the basis of peace and free-
dom. You figure it out; it is beyond my powers of
comprehension.) They are saying implicitly that the
tares can be trusted to care for the needs of the field,
including the wheat, but the wheat cannot be trusted
to care for the field at all.

The fact is, we need biblical law in order to pre-
serve historical continuity. We need biblical law in
order to avoid humanism’s religion of revolution,
especially Marx’s version.® In order to be preserved,
the field (world) needs Christians in positions of au-
thority in every area of life; this means that the rep-
robates will be treated lawfully. They will be given
civil freedom precisely decause humanists (such as
bloody Marxists, bloody Nazis, and bloody Mus-
lims) will net be in control.

Differentiation

God intends for the dominion religion of the
Bible to triumph over both the power religion and
the escape religion. This is the fundamental issue of
differentiation in history. Men are not passive. They are
commanded to be active, to seek dominion over

§. Gary North, Marx’s Religion of Revolution: The Doctrine of Cre-
ative Destruction (Nutley, New Jersey: Craig Press, 1968).
