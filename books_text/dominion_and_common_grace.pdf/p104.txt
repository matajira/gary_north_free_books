88 DOMINION AND COMMON GRACE

Rushdoony, yet his view is equally opposed to the
amillennialism of the anti-Chalcedon amillennial
theologian (and former colleague of Van Til’s), Mer-
edith G. Kline, who openly rejects Rushdoony’s
postmillennial eschatology.

Kline explicitly rejects Van Til’s conclusion that
common grace declines over time, although he does
not mention Van Til as the source of this view. Kline
judiciously pins the tail on another donkey. He says
that this view of common grace as earlier grace is
what the Chalcedon postmillennialists teach. Kline
is incorrect: Greg Bahnsen, James Jordan, David
Chilton, and I all reject this view of common grace,
and we are all Chalcedon-trained postmillennialists.
We were all on the payroll of Chalcedon in the
1970's. (And one by one, we all left Chalcedon as we
came to it: fired with enthusiasm!) The original
essay from which this book is derived appeared in
The Journal of Christian Reconstruction two years before
Kline’s essay was published, and which he cites,
clearly not having understood it. Only R. J. Rush-
doony has affirmed Van Til’s view of common grace,
despite the fact that such a view conflicts with his
postmillennialism.*

5. Rushdoony categorically rejects amillennialism, calling it
“impotent religion” and “blasphemy”-~an implicit attack on Van
Til—yet he affirms the validity of Van Til’s common grace posi-
tion, calling for the substitution of Van Til's “earlier grace” con-
cept for “cornmon grace.” Rushdoony’s anti-amillennial essay ap-
peared in Journal of Christian Reconstruction, TL (Winter 1976-77):
“Postmillennialism versus Impotent Religion.” His pro-“earlier
grace” statement appeared in his review of E. L. Hebden
Taylor's book, The Christian Philosophy of Law, Politics and the State,
