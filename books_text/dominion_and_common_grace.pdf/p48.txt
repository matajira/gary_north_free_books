32 DOMINION AND COMMON GRACE

works in relation to the grace that God has shown to
him in history). We also see in Christ and his follow-
ers the working out of this same dual principle: equal
ultimacy (the perfect humanity of Christ imputed to
all redeemed men), but with unequal effects (appro-
priate judgment in terms of the individual’s works
in relation to the grace that God has shown to him in
history).

What I am arguing in this book is that the two
aspects of the covenant—blessing and cursing—are
not equally ultimate in their respective effects in his-
tory, just as they are not equal in their eternal effects.
Different individuals experience different histories,
depending on the extent to which they affirm or deny
the covenant by their actions. Similarly, different so-
cieties experience different histories, depending on
the extent to which they affirm or deny the covenant
by their actions.

The working out of the principle of covenantal
blessing can lead to the positive feedback operation:
historical blessing to covenantal reaffirmation to
greater historical blessing . . . (Deut. 8:18). (A theo-
nomic postmillennialist should argue that it does
eventually operate in history in this fashion, leading
to millennial blessings.) The working out of cove-
nantal cursing leads to temporal scattering and de-
struction (Deut. 8:19-20). Every Christian theolog-
ian admits that the working out of covenantal curs-
ing in history eventually leads to final destruction for
God’s enemies. Theologians debate only about the
historical path of this development to final judg-
ment; premillennial, amillennial, or postmillennial.
