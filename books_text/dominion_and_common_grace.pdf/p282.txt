266 DOMINION AND COMMON GRACE

epistemologically. Van Til uses the “transcendental”
proof of God: that without presupposing the God of the
Bible, man can say nothing logical. In contrast to
Warfield, Van Til argues that all unregenerate men
use their anti-God presuppositions to come to the
“logical” conclusion that the God of the Bible cannot
possibly exist. Therefore, if we allow the natural
man to use his logic in this way —if we allow him to
assume that we all begin with the same presupposi-
tions about reality as autonomous men—then we
cannot deal with him effectively. We have violated
the Bible’s first principle, namely, that it is God who
is sovereign, and therefore man has no autonomy.

Warfield wanted to appeal to the common “right
reason” of man in his defense of the faith, but, as
Van Til comments, “in Apologetics, Warfield wanted
to operate in neutral territory with the non-believer,
He thought that this was the only way to show to the
unbeliever that theism and Christianity are objec-
tively true. He sought for an objectivity that bridged
the gulf between Kuyper’s ‘natural’ and special prin-
ciples.” Then, he makes himself clear; “I have chosen
the position of Abraham Kuyper.”®

We must confront the natural man with the
bankruptcy of his position. We do need to challenge
him logically, but only by using God’s logic, because
“no challenge is presented to him unless it is shown
him that on his principle he would destroy all truth
and meaning. Then, if the Holy Spirit enlightens

12. Cornelius Van Til, The Defense of the Faith (2nd ed.; Phila-
delphia: Presbyterian & Reformed, 1963), p. 265.
