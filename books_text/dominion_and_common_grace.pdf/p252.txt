CONCLUSION

For whom he did foreknow, he also did predestinate,
to be conformed to the image of his son, that he might be
the firstborn among many brethren (Romans 8:29).

And be not conformed to this world: but be ye
transformed by the renewing of your mind, that ye may
prove what is the good, and acceptable, and perfect,
will of God (Rom. 12:2).

Be ye followers [imitators— NASB] of me, even as I
alsa am of Christ (I Cor. 11:2.

The Christian is called to ethical self-consciousness.
Out of this comes epistemological self-consciousness.
Ethics is the fundamental issue, not philosophical
knowledge.

The increase in the ethical understanding of
Christians results in their increasing understanding
of the Bible’s principles of knowledge. Christians
think God’s thoughts after Him, as creatures made
in His image.
