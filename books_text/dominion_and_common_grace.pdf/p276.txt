260 DOMINION AND COMMON GRACE

fessors had graduated them. This was not quite the
same as having baptized their theological views, but
over time, this is what graduation from seminary
came to mean. The seminary degree was, after 1893
{and probably from 1812 onward), very nearly a
guarantee of eventual ecclesiastical licensure.

Warfield recognized the threat, but he only dis-
cussed it publicly late in his career. He saw the sem-
inary as a support institution, one with distinct limita-
tions. “It is not the function of the seminary to give
young men their entire training for the ministry.
That is the concern of the presbytery; and no other
organization can supersede the presbytery in this
business. The seminary is only an instrument which
the presbytery uses in training young men for the
ministry. An instrument, not the instrument. The
presbytery uses other instruments also in this
work.”* But no matter how hard he or other Calvin-
istic Presbyterians might proclaim the legitimate
sovereignty of the presbytery, their rationalism and
their respect for the institutions of higher (humanist)
learning eventually undercut their warnings.

The implicit rationalism of the old Presbyterian-
ism led into the quicksand of certification. Once a
man had earned his degree from an approved semin-
ary, it became very difficult for laymen to challenge
him when he sought ordination, and the very fact
that he had a degree made him very nearly an “initi-

4. “The Purpose of the Seminary," The Presbyterian (Nov. 22,
1917); reprinted in Selected Shorter Writings of Benjamin B. Warfield—
I, edited by John E. Meeter (Nutley, New Jersey: Presbyterian
& Reformed, 1970), p. 374.
