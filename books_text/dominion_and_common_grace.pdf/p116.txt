100 DOMINION ANP COMMON GRACE

takes over, and the end result is destruction. This is
why, at the end of the millennial age, the unregener-
ate try once again to assert their autonomy from
God. They attack the church of the faithful (Rev.
20:8-9a). They attempt once more to exercise auton-
omous power, And the crack of doom sounds, not for
the regenerate (for which there is no doom), but
rather for the unregenerate (Rev. 20:9b).

Differentiation and Progress

The process of differentiation is not constant over
time. It ebbs and flows. Its general direction is to-
ward epistemological self-consciousness. But Chris-
tians are not always faithful, any more than the He-
brews were in the days of the judges. The early
church defeated pagan Rome, but then the secular
remnants of Rome compromised the church. The
Reformation launched a new era of cultural growth,
but the Counter-Reformation struck back, and the
secularism of the Renaissance and then the Enlight-
enment overshadowed both, and still does.

This is not cyclical history, for history is linear.
There was a creation, a Fall, a people called out of
bondage, an incarnation, a resurrection, and Pente-
cost. There will be an era of epistemological self-
consciousness, as promised in Isaiah 32. There will
be a final rebellion and judgment. There has been a
Christian nation called the United States. There has
been a secular nation called the United States. (The
dividing line was the Civil War, or War of Southern
Secession, or War Between the States, or War of
Northern Aggression—take your pick.) Back and
