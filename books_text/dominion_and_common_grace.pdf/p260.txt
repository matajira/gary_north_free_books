244 DOMINION AND COMMON GRACE

There is continuity in life, despite discontinui-
ties. The wealth of the sinner is laid up for the just.
Satan would like to burn God’s field, but he cannot.
The tares and wheat grow to maturity, and then the
reapers go out to harvest the wheat, cutting away the
chaff and tossing it into the fire. Satan would like to
turn back the crack of doom, return to ground zero,
return to the garden of Eden, when the dominion
covenant was first given. He cannot do this. History
moves forward toward the fulfillment of the domin-
ion covenant (Gen. 1:28)—as much a fulfillment as
pre-final-judgment mankind can achieve. At that
point, common grace produces malevolence —abso-
lutely and finally malevolence when Satan uses the
last of his time and the last of his power to strike out
against God's people. When he uses his gifts to be-
come finally, totally destructive, he is cut down from
above. This final culmination of common grace is Satan’s
crack of doom.

And the meek— meek before God, active toward
His creation—shall at last inherit the earth. A re-
newed earth and renewed heaven is the final payment
by God the Father to His Son and to those He has
given to His Son. This is the postmillennial hope.

Answers
In the Introduction to this book, I asked a series
of questions. Let me summarize my answers.

Does a gift from God imply His favor?

No. A gift from God is given to unbelievers for
two primary reasons: to bring them to humble,
grateful repentance, and to heap coals of fire on the
