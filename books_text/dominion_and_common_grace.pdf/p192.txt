‘176 DOMINION AND COMMON GRACE

biblical law."5 But these blessings do not necessarily
include universal regeneration. The blessmmgs only
require the extension of Christian culture. For the
long-term progress of culture, of course, this in-
crease of common grace (or reduction of the com-
mon curse) must be reinforced (rejuvenated and
renewed) by special grace—conversions. But the
blessings can remain for a generation or more after
special grace has been removed, and as far as the ex-

15. John Murray's excellent commentary, The Epistle to the
Romans (Grand Rapids, Michigan: Eerdmans, 1965), contains
an extensive analysis of Romans 11, the section dealing with the
future conversion of the Jews. Murray stresses that God’s re~
grafting in of Israel leads to covenantal blessings unparalleled in
human history. But the Israel referred to in Romans I, argues
Murray, is not national or political Israel, but the natural seed of
Abraham. This seems to mean genetic Israel.

A major historical problem appears at this point. There is
some evidence (though not conclusive) that the bulk of those
known today as Ashkenazi Jews are the heirs of a converted tribe
of Turkish people, the Khazars. It is well-known among Euro-
pean history scholars that such a conversion took place around
740 a.p. The Eastern European and Russian Jews may have
come from this stock. They have married other Jews, however:
the Sephardic or diaspora Jews who fled primarily to western
Europe. The Yemenite Jews, who stayed in the land of Palestine,
also are descendants of Abraham. The counter-evidence against
this thesis of the Khazars as modern Jews is primarily linguistic:
Yiddish does not bear traces of any Turkic language. On the
kingdom of the Khazars, see Arthur Koestler, The Thirteenth
‘Tribe: The Khazar Empire and its Heritoge (New York: Random
House, 1976).

If the Israel referred to in Romans il is-primarily genetic,
then it may not be necessary that all Jews be converted. What,
then, is the Jew in Romans 11? Covenantal? 1 wrote to Murray
in the late 1960's to get his opinion on the implications of the
Khazars for his exegesis of Romans 11, but he did not respond.
