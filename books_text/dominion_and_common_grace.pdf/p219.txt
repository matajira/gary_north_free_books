FILLED vesseLts 203

It is crystal clear what Paul is arguing. The
structure of his argument is obvious. God makes two
sorts of ethical vessels out of one common clay, hu-
manity. Each type of vessel has its respective eternal
destiny. The vessels have no say in the matter. They
cannot legitimately reply to the Creator, “Why have
you made me thus? And since you have, how can
you legitimately hold me responsible for my ethical
acts and my eternal destiny?” Why not? Because
God is the sovereign Creator.

I realize that this argument leaves no room for
what is commonly called free will, meaning auton-
omy, meaning that God either does not control (or
may not even know) whether a man will or will not
accept His grace in Jesus Christ. On the contrary:
God knows, God determines, and God is the sover-
eign Potter. He predestinates. He chose the redeemed
before the foundation of the world. “Blessed be the
God and Father of our Lord Jesus Christ, who hath
blessed us with all spiritual blessings in heavenly
places in Christ; according as he hath chosen us in
him before the foundation of the world, that we
should be holy and without blame before him in
love: having predestinated us unto the adoption of
children by Jesus Christ to himself, according to the
good pleasure of his will, to the praise of his glory of
his grace, wherein he hath made us accepted in the
beloved” (Eph, 1:3-6).

Would any reader test himself to see if any traces
of humanism remain in his thinking? Here is éhe test.
If the theology of Paul in Romans 9 and Ephesians 1
in any way disturbs a person, then there are traces of
