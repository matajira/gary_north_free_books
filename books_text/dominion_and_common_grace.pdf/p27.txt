INTRODUCTION 1%

Kant’s name will be there, too, but only in a four-
page string of quotations from a book written in 1916
or 1932 by a scholar you have never heard of. (No
direct citations from Kant? Hardly ever. Phenome-
nal!) He will refer to a Bible verse occasionally, but
the rarest diamond of all is a page of detailed Bible
exposition.® You will learn about univocal and equi-
vocal reasoning. Continuity will be challenging dis-
continuity. Rationalism will be doing endless battle
with irrationalism. The one will be smothering the
many, whenever the many aren’t overwhelming the
one. (These last four conflicts are, if I understand
him correctly, all variations of essentially the same
intellectual problem.)

Watch for his analogies. Rationalism and irra-
tionalism will be taking in each other’s washing for a
living. There will be a chain of being lying around
somewhere, probably right next to the infinitely long
cord that the beads with no holes are supposed to
decorate. Some child will be trying to slap her
father’s face while sitting on his lap, and someone
out in the garage will be sharpening a buzz saw that
is set at the wrong angle. Warning: if you don’t
watch your step, you could trip over the full-bucket
problem. And so it goes, book after book.

 

and his two fellow students (including another of my teachers,
Philip Wheelwright) would be assigned a passage in Plato or
Aristotle in the original Greek. They would then go into the sem-
inar to discuss what they had read.

8. An exception is the first half of The God of Hope (Phillips-
burg, New Jersey: Presbyterian & Reformed, 1978). These
chapters are sermons. But there is not much exegesis even here.
