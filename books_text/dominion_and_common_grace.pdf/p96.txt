80 DOMINION AND COMMON GRACE

and the former shall not be remembered, nor come
to mind” (65:17). There is a manifestation of this era
in history. It began with Christ’s resurrection, the
greatest manifestation of God’s kingdom, and it de-
velops throughout New Testament history. This is
the biblical basis for the idea of progress, a uniquely
Christian idea: an eschatology of victory in history
over the physical effects of sin, meaning victory in
history over God's curse. I cannot stress this too
much: victory in history.

Continuity: Common Grace
‘We now return to the question of common grace.
I have already defined common grace as continuity
(Introduction). The question now presents itself:
What is the nature of this continuity?

Withdrawing Common Grace: Amillennialism

The amillennialist says that the slow, downward
drift of culture parallels the growth in self-awareness
and improving judgment. This has to mean that com-
mon grace is to be withdrawn as time progresses. The re-
straining hand of God will be progressively removed.
Since the amillennialist believes that things will get
worse before the final judgment, he has to interpret
common grace as earlier grace (assuming he admits
the existence of common grace at all), This has been
stated most forcefully by Van Til, who holds a doc-
trine of common grace and who is a self-conscious
amillennialist:
