VAN TIL'S VERSION OF COMMON GRACE 101

forth, ebb and flow, but with a long-range goal. His-
tory is headed somewhere.

There has also been progress. We see this espe-
cially in the progress of Christian creeds. Look at the
Apostles’ Creed. Then look at the Westminster Con-
fession of Faith. Only a fool or a heretic would deny
theological progress. There has also been a parallel
growth in wealth, knowledge, and culture. Are the
two developments, theological and cultural, com-
pletely unrelated? What are we to say, that technol-
ogy as such is the devil’s, and that since God’s com-
mon grace has supposedly been steadily withdrawn
as the creeds have been steadily improved, the mod-
ern world’s development is therefore the autonomous
creative work of Satan (since God’s common grace
cannot account for this progress)? Is Satan creative
autonomously creative? If not, from whence comes
our wealth, our knowledge, and our power? Is it not
from God? Is not Satan the great imitator? But whose
progress has he imitated? Whose cultural develop-
ment has he attempted to steal, twist, and destroy?
Where did the progress come from—and how?

There has been progress since the days of Noah
—not straight-line progress, not pure compound
growth, but progress nonetheless. Christianity pro-
duced it, secularism stole it, and today we seem to be
at another crossroad: Can the Christians sustain
what they began, given their present compromises
with secularism? Can the secularists sustain what
they and the Christians have constructed, now that
their spiritual capital is running low, and the Chris-
tians’ cultural bank account is close to empty?
