132 DOMINION AND COMMON GRACE

sistent with the new man within him, and therefore
by adhering ever more closely to God's law. Biblical
law is the covenant-keeper’s fully self-consistent tool of
dominion.

Second, the covenant-breaker exercises power by
becoming inconsistent with his ultimate philosophy of
randomness. He can commit effective crimes only by
stealing the worldview of Christians. The bigger the .
crimes he wishes to commit (the ethical impulse of
evil), the more carefully he must plan (the epistemo-
logical impulse of righteousness: counting the costs
[Luke 14:28-30]). The Christian can work to fulfill
the dominion covenant through a life of consistent
thought and action; the anti-Christian can achieve
an offensive, destructive campaign against the
Christians —as contrasted to a self-destructive life of
drugs and debauchery ~ only by stealing the biblical
worldview and twisting it to evil purposes,

In short, to become really evil you need to become preity
good.

The Bible says that all those who hate God love
death (Prov. 8:36b). Therefore, for God-haters to
live consistently, they would have to commit suicide.
It is not surprising that the French existentialist phi-
losopher Albert Camus was fascinated with the pos-
sibility of suicide. It was consistent with his philoso-
phy of meaninglessness. To become a historic threat
to Christians, unbelievers must restrain their own ulti-
mate impulse, namely, the quest for death. Thus, their
increase in epistemological self-consciousness over
time is incomplete, until the final rebellion, when
their very act of rebellion brings on the final judg-
