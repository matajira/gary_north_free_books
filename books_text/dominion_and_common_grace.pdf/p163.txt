ESCHATOLOGY AND BIBLICAL LAW 147

not operate in this premillennial era. In this respect,
premillennialists agree with amillennialists.

Very few premillennialists have thought about
(let alone written about) the concept of common
grace. It has no practical relevance to premillennial
theology. Few premillennialists believe that we are
still under the terms of the dominion covenant. The
premillennial Bible Presbyterian Church in 1970 cat-
egorically denied the New Testament validity of the
cultural mandate.32

If some premillennialist does have a theory of
common grace which applies to the church age,
meaning history this side of the Rapture, it would
have to be similar to the amillennialist version. It
would deny the relevance of the positive feedback
process of covenantal blessings. Nevertheless, it
would at least be more consistent than the amillen-
nial version. Since the cultural mandate is no longer
in force, according to most premillennialists, the
schizophrenic and frustrating program of Dutch
amillennialism is absent: at least premillennialists do
not feel called by God to do what God says cannot
and will not be done in history anyway. The premil-
lennialist says that the cultural victory of Bible-
believing people will come on earth only after the
great discontinuous event of the Rapture. This is
“the blessed hope.” It will be exclusively God’s work.
The church is off the hook.

“Off the hook.” This is the heart and soul of pre-

32, R. J. Rushdoony, The Institutes of Biblical Law (Nutley,
New Jersey: Craig Press, 1973), pp. 723-24.
