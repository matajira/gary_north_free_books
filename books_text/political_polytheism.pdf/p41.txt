Iniroduction 17

keeping suspicious donors and parents perpetually confused. Sec-
ond, they can then read the latest (i.e., decade-old) fads of political
liberalism into these “creation ordinances.” They get the benefits of
God's doctrine of creation without any of the restraints of God’s
Bible-revealed law.

These scholars are like unmarried couples who live together
under the protection of common law marriage precedents. If these
unions can survive for five consecutive years, they become techni-
cally legal marriages for purposes of ownership and inheritance. Un-
til then, however, the door remains open for leaving and switching
partners. During the first five years, they can reassure themselves
and their potential in-laws with the convenient excuse, “We're still
moving toward common-law status.” In fact, they are fornicators.
This is modern neo-evangelicalism in a nutshell: at best a movement
of theological common-law marriages; at worst, a movement of
theological fornicators,

White Uniforms and Sinking Ships

The doctrine of Christian political pluralism rested from the be-
ginning on the doctrine of natural rights and natural law. That com-
forting faith is now forever dead, buried by the spiritual heirs of the
pagan Greeks who invented it in the first place. This loss of faith in
natural law is admitted even by defenders of Christian political plur-
alism.*® Nevertheless, they stubbornly refuse to admit publicly that
without this key epistemological*? assumption, political pluralism
becomes a visibly. drifting ship with a hole in its side.

The Leaking Ship

This ship is taking on water fast. It is already 2,000 miles out to
sea; with no radio on board and very little dehydrated food re-
maining. A huge storm is on the horizon. There is only one hope
available: a motorized lifeboat with a good set of maps, a compass,
and enough fuel to get a few hundred miles. It even has an emer-
gency sail. But there is one catch: its name. ‘The former owner of the
shipping line—who still holds a second mortgage on the company,
and the present owners are several payments behind—named it the

48. Cf. Gordon J. Spykman, “The Principled Pluralist Position,” Gad and Politics,
pp. 90-91.
49, Epistemology: “What men can know, and how they can know it.”
