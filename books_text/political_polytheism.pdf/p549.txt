Conclusion, Part 3 525

Congress then sanctioned this act of civil covenant-breaking when
its members signed the document (point four). Had they made their
case for separation in terms of the monarchy’s 250-year-old break
with the Bible—Erastianism, the theology of the national State
Church— or with the growing Deism of the Parliament and the re-
sultant corruption and tyranny, an unlimited Parliamentary power
asserted by Parliament’ and defended by Blackstone, they could
have justified their civil rebellion biblically, But they chose to have
Christianity’s mortal enemy write the nation’s covenant-breaking
document. And so John Winthrop’s dream died.

There is no neutrality, There is no neutral legal ground between a
civil covenant under one sovereign and a civil covenant under
another. A new covenant and a new sovereign is substituted for an
earlier covenant and sovereign. To use the language of the Arminian
and Deistic social contract theorists, there is never a return to the
“state of nature.” The colonists knew this much, even if they did not
understand biblical covenant theology very well. They were neces-
sarily creating a new civil covenant when they broke the old one.
This is why Congress on July 4 set up a committee to create a na-
tional seal.

Church and State in 1776

Great Britain had unquestionably become bureaucratic. It was
no longer the nation it had once been. But it was still a covenantally
Christian nation. In fact, one of the major resentments that the Prot-
estants of the colonies had against Great Britain was that they be-
lieved that the Church of England was planning to send a bishop to
the colonies, therefore making it much easier to ordain new Anglican
pastors here. Previously, candidates for the Anglican ministry had
been required to travel to London, where the Bishop of London
would consider ordaining them. No one else had this authority. This

5, Parliament’s Declaratory Act of Feb. 3, 1766, was announced in preparation
for Parliament’s repeal of the Stamp Act (taxes on formal sanctioning documents in
the colonies) two weeks later. The Declaratory Act affirmed the following: “That the
King’s Majesty, by and with the advice and consent of the Lords spiritual and tem-
poral and Commons of Great Britain in parliament assembled, had, hath, and of
right ought to have, full power and authority to make laws and statutes of sufficient
force and validity to bind the colonies and people of America, subjects of the crown
of Great Britain, in all cases whatsoever” Edmund S, Morgan and Helen M.
Morgan, The Stamp Act Crisis: Prologue to Revolution (New York: Collier, 1963), pp.
347-48.
