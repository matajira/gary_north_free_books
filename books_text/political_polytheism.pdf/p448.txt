424 POLITICA POLYTHEISM

over a procession in Philadelphia on December 27, 1778, after the
evacuation of the British. Dressed in full Masonic attire, he marched
through the city with three hundred other Masons, and then held a
Masonic service at Christ Church, which became his.congregation
of preference during his Presidency.*°

As President, he received many honors from local lodges. His
written replies to them were generous. He never wavered in his at-
tachment to Masonry, In a letter to King David’s Lodge No. 1 of
Newport Rhode island, written on Sunday, August 22, 1790, Wash-
ington wrote: “Being persuaded that a just application of the princi-
ples, on which the Masonic Fraternity is founded, must be promotive
of private virtue and public prosperity, I shall always be happy to ad-
vance the interests of the Society, and to be considered by them as a
deserving brother.”*! In several letters, he referred to God as the
Supreme Architect. A representative example is his letter to Penn-
sylvania Masons (Dec. 27, 1791): “. . . Lyequest you will be-assured
of my best wishes and earnest prayers for your happiness while you
remain in this terrestrial Mansion, and that we may thereafter meet
as brethren in the Eternal Temple of the Supreme Architect.”52

John Eidsmoe, in his book-length attempt to defend the Consti-
tution as a Christian document, takes seriously Washington’s out-
right lie—it can be nothing else —in a letter to G. W. Snyder in 1798,
that he had not been in a masonic lodge “more than once or twice in
the last thirty years.”*3 One does not become the Grand Master of a
lodge by attending services once or twice over thirty years, but one
can certainly fool two centuries of Christian critics by lying through
one’s wooden teeth about it.5+

That he may have been a Christian in private ts possible, though
his attitude toward the Church betrays a woeful misunderstanding of

50, Ibid., p. 246; citing Roth, pp. 63-64, and Tatsch, Freemasonry in the Thirteen
Colonies, pp. 206-11.

51. Tatsch, Facts About Washington, p. 14.

52. Ibid., p. 18

59. John Eidsmoe, Christianity and the Constitution: The Faith of Our Founding Fathers
(Grand Rapids, Michigan: Baker Book House, 1987), p. 125, citing John C. Fitz-
patrick (ed.), The Writings of George Washington from the Original Manuscript Sources,
1745-1799 (Washington, D.C.: U.S. Government Printing Office, 1931-44), vol. 34,
p. 453.

54, Washington's false teeth, attributed to fellow Mason Paul Revere, were made
by John Greenwood, who as a boy was a neighbor of Revere’s during the period of
Revere’s brief, ill-fated or ill-fitting career as a dentist. Esther Forbes, Paul Revere and
‘the World He Lived In (Boston: Houghton Mifflin, [1942] 1962), p, 133.
