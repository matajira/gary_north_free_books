512 POLITICAL POLYTHEISM

over the defeated South. During Reconstruction, a man was con-
victed in a military court of certain acts that were deemed by that
court as obstructing Reconstruction. The Supreme Court decided to
revicw the case. Here is the analysis of the case from the Library of
Congress:

Anticipating that the Court might void, or at least undermine, congres-
sional reconstruction of the Confederate States, Congress enacted over the
President’s veto a provision repealing the act which authorized the appeal
McCardie had taken, Although the Court had already heard argument on
the merits, it then dismissed for want to jurisdiction. “We are not at liberty
to inquire into the motives of the legislators. We can only examine into its
power under the Constitution; and the power to make exceptions to the ap-
pellate jurisdiction of this court is given by express words.”

The President had been asked to sign the measure, but the text of
the analysis does not say why. The Constitution surely does not
mention any such requirement. Perhaps Congress submitted it to
President Johnson out of spite; they knew his veto could be over-
ridden. In any case, the Court withdrew peacefully. It had no
choice. The Constitution is clear, and previous cases had admitted
such authority on the part of Congress.

Initial Judicial Restraint

Obviously, this is a very ticklish subject. Like the principle of ju-
dicial review, it was seldom invoked in the early days of the republic.
Judicial review is not a principle written into the Constitution. Chief
Justice John Marshall invoked it in the famous Marbury v. Madison
case in 1803 when he declared an Act of Congress unconstitutional.
The only other time prior to the Civil War that the Court invoked it
was in the Dred Scott x. Sandford case of 1857, which more or less guar-
anteed the Civil War. The Court determined that Dred Scott was the
property of his southern owner, even though he had been taken into
states that did not recognize the lawfulness of chattel slavery. He did
not thereby become a citizen, so he could not sue in federal court,
the Supreme Court declared. The Court declared that Negroes
could not be citizens of the U.S., although they could become state
citizens. That decision was overruled at the cost of 600,000 dead.
The 14th Amendment (1868) was the result.

60. Congressional Research Service of the Library of Congress, The Constitution of the
United States: Analysis and Interpretation (U.S. Government Printing Office, 1972), p. 752.
