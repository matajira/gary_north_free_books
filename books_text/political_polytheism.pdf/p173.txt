Halfway Covenant Ethics 149

Christian civilization, not a humanist one. They will be rebelling
against a superior degree of testimony regarding the effectiveness
and reliability of God’s covenantal sanctions in history. Most impor-
tant, they will not be allowed to inflict visible defeat on the Church
before Christ forever silences them in final judgment.

It is theoretically possible for a consistent Christian Reconstruc-
tionist to argue that there will steadily be a religious falting away in
history that will be comparable to Israel’s falling away in the days of
Jesus. But it would not be possible for a Christian Reconstructionist
to argue that this falling away will be accompanied by the steady re-
placement of Christian-run institutions by pagans. This would be a
denial of God’s covenant sanctions in history. This is why most
Christian Reconstructionists are postmillennialists. They do not be-
lieve that God’s covenant sanctions can or will fail in history, and
they also do not believe that it is possible in the long run for a minor-
ity of Christians to run the social order on a biblical basis in a world
that has abandoned faith in the saving work of Jesus Christ.

Why do most Christian Reconstructionists believe that there will
not be this falling away until the very end of the millennial era, just
before the second coming of Christ? Because most Christian Reconstruc-
fionists have a very high view of the historical effectiveness of the work of the
Holy Spirit. Premillennialists argue that Jesus must be present bodily
in order to usher in His earthly kingdom (Christian civilization). So,
for that matter, do amillennialists; they just say that because He will
not return bodily to set up His earthly kingdom until He returns at
the final judgment, there will never be a pre-final judgment earthly
kingdom (Christian civilization).

In conirast to these culturally pessimistic views, most Christian
Reconstructionists believe that today’s presence of the Holy Spirit is
sufficient to empower Christians to establish, as Christ's ecclesiasti-
cal “body” and also as His personal representatives and am-
bassadors, a self-consciously Christian civilization. We must never
forget that Jesus departed from this world bodily in order that the Holy Spirit
might come. He spoke of this as an advantage to the Church. “Never-
theless I tell you the truth; It is expedient for you that I go away: for
if I go not away, the Comforter will not come unto you; but if I
depart, J will send him unto you” (John 16:7). Understand what this
means. Because jesus Christ is not bodily present in this world of sin, the
Church ts far better off. Because He is not present, the Church has far
greater power. Jesus could not have said it any plainer than this:
