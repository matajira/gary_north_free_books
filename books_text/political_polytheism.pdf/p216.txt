192 POLITICAL POLYTHEISM

Hollinger

But Wells is mild compared with David P. Hollinger, a professor
at Alliance Theological Seminary. Listen to his assessment of the
supposed irrelevance, impotence, and downright malevolence of the
idea of a Christian society: “Only a solid biblical doctrine of the church
enables us to assert that the kingdom of God is qualitatively different
from the powers of this world, and that the latter cannot live by spe-
cific Christian principles, since they do indeed have a different view
of reality. To impose Christianity on society, or seek to make it the
legal basis of a social order, tends to generate undue hostility in a
pluralistic context. More importantly, it undermines the uniqueness
of the church. The theocratic ideal, which Schaeffer seems implicitly
to support, fails to recall that it is the church, not America, that is ‘a
chosen people, a royal priesthood, a holy nation, a people belonging
to God, that you may declare the praises of him who called you out
of darkness into his wonderful light.’””? And what is he professor of?
You may not believe this: Church and Society.

What he seems to be saying is that if Christian standards are
used to govern society in general, this “undermines the uniqueness
of the church.” Does this also include standards for the family? If so,
then on what basis shall God-fearing people promote civil laws

‘ against incest? Or bestiality?

This raises another important question: Will the pluralist princi-
ple of “equal time for Satan” exist in eternity? If not, will this
absence of rival theological views in eternity undermine the unique-
ness of the Church?

Schaeffer actually said pretty much what Hollinger says about
the necessity of distinguishing America from the Church, as well as
the necessity of political pluralism, but Hollinger is so outraged—1
see no other obvious motivation — by the mildly worded moral warn-
ing that Schaeffer raises regarding pluralism’s moral relativism (i.e.,
immorality) that he literally cannot understand what Schaeffer has
clearly written. This seldom stops a liberal from going into print.

Judicial Blindness

I am preparing you for evidence in Chapter 5 that Christian aca-
demic pluralists are so utterly enraged by the very idea of biblical
theocracy — the visible manifestation of God’s kingdom (civilization)

72. Hollinger, “Schaeffer on Ethics,” ihid., p. 266.
