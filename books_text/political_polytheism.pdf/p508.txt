484 POLITICAL POLYTHEISM

civil magistrate. The Mason, as an initiate, would face conflicting
loyalties when called on by the civil magistrate to reveal details of his
“craft.” Should he reveal secrets to the magistrate or remain faithful
to his “craft”? If he takes seriously the terminology of the reported
oaths in Masonry, then there would be a strong temptation to refuse
to testify and suffer the civil consequences, or else to lie. We would
expect to find that Masonic literature would pudticly place all oaths
on equal par. In secret, of course, this public neutrality would
vanish; the key loyalty would have to be to the guild. This publicly
revealed position of “equally binding oaths” would tend ta weaken
the initiate’s commitment to the civil magistrate, leaving him to
worry about the vivid verbal terms of Masonry’s self-maledictory
oaths. What we find is just such “public neutrality” concerning the
equality of all oaths.

The oath of the third-degree Mason refers to “so vile and per-
jured a wretch as I.” Using this as a guide, we can learn just how well
Masonic leaders understand the close relationship between self-
maledictory oaths and God's judgment, Under “perjury,” the Encyclo-
paedia of Freemasonry declares:

In the municipal law perjury is defined to be a wilful false swearing to a ma-
terial matter, when an oath has been administered by lawful authority, The
violation of vows or promissory oaths taken before one who is not legally
authorized to administer them, that is to say, one who is not a magistrate,
does not in law involve the crime of perjury. Such is the technical definition
of the law; but the moral sense of mankind does not assent to such a doc-
trine, and considers perjury, as the root of the word indicates, the doing of
that which one has sworn not to do, or the omitting to do that which he has
sworn to do, The old Romans seem to have taken a sensible view of the
crime of perjury. Among them oaths were not often administered, and, in
general, a promise made under oath had no more binding power in a court
of justice than it would have had without the oath. False swearing was with
them a matter of conscience, and the person who was guilty of it was re-
sponsible to the Deity alone. The violation of a promise under oath and of
one not under such a form was considered alike, and neither was more
liable to human punishment than the other. But perjury was not deemed to
be without any kind of punishment. Cicero expressed the Roman senti-
ment when he said “perjurii poena divina exitium; humana dedecus” — the
divine punishment of perjury is destruction; the human, infamy. Hence every oath
was accompanied by an execration, or an appeal to God to punish the
swearer should he falsify his oath. . . .

Freemasons look in this light on what is called the penalty; it is an invocation
of God’s vengeance on him who takes the vow, should he ever violate it;
