384 POLITICAL POLYTHEISM

To serve in Congress under the Articles, a man had to be ap-
pointed by his state legislature. He could be recalled at any time. He
could serve in only three years out of every six. He was under public
scrutiny continually. And to exercise the authority entrusted to him
by his state legislature, he had to take an oath. These oaths in most
states were both political and religious, The officer of the state had to
swear allegiance to the state constitution and also allegiance to God.
Consider Delaware’s required oath:

Art, 22, Every person who shall be chosen a member of either house, or ap-
pointed to any office or place of trust, before taking his seat, or entering
upon the execution of his office, shall take the following oath, or affirma-
tion, if conscientiously scrupulous of taking an oath, to wit:

“I, A B, will bear true allegiance to the Delaware State, submit to its
constitution and laws, and do no act wittingly whereby the freedom thereof
may be prejudiced.”

And also make and subscribe the following declaration, to wit:

“I, AB, do profess faith in God the Father, and in Jesus Christ His only
Son, and in the Holy Ghost, one God, blessed for evermore; and I do
acknowledge the holy scriptures of the Old and New Testament to be given
by divine inspiration.”

And all officers shall also take an oath of office.?”

The Constitution of Vermont in 1777 was not much different:

Section IX. A quorum of the house of representatives shall consist of two-
thirds of the whole number of members elected and having met and chosen
their speaker, shall, each of them, before they proceed to business, take and
subscribe, as well the oath of fidelity and allegiance herein after directed, as
the following oath or affirmation, viz.

I do solemnly swear, by the ever living God, (or, I do
solemnly affirm in the presence of Almighty God) that as a member of this assembly, I
will not propose or assent to any bill, vote, or resolution, which shall appear to me in-
Jurious to the people; nor do or consent to any act or thing whatever, that shall have a
tendency to lessen or abridge their rights and privileges, as declared in the Constitution of
this State; but will, in all things, conduct myself as a faithful, honest representative and
guardian of the people, according to the best of my judgment and abilities.

And each member, before he takes his seat, shall make and subscribe
the following declaration, viz.

I do believe in one God, the Creator and Governor of the universe, the rewarder of

27. Philip B. Kurland and Ralph Lerner (eds.), The Founders’ Constitution, 5 vols.
(Ghicago: University of Ghicago Press, 1987), V, p. 634.
