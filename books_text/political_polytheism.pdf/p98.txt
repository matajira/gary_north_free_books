74 POLITICAL POLYTHEISM

rich can “buy their way in,” but the poor are told to leave. The selec-
tive barriers remain.

Immigration was not a political problem for ancient Israel. It
was not physical birth that entitled a person to citizenship. It was
rather the physical mark of a covenant oath, circumcision: the out-
ward sign of the new birth.!7

Thus, we reach a most important conclusion: Israel was @ sanc-
tuary because Israel was God's theocracy, There are no remaining national
sanctuaries today because citizenship and rulership are based now
on mass democracy. The State" is officially a covenant institution
established by man rather than God. It derives its authority solely
from the sovereign People. This is why in some Communist dictator-
ships and other dictatorships, it is against the law to refuse to vote on
election day. The People must speak, even in one-party dictator-
ships. The public approval of the People is the source of the regime’s
legitimacy.

This crucial shift in the covenantal basis of citizenship is what the
conspiracy of silence is all about. This is what the political con-
spiracy two centuries ago was all about. The conspirators have sub-
stituted new oaths, and Christian citizens have naively agreed to
this, thereby sealing the doom of national sanctuaries in the twenti-
eth century.9

Do you now understand the Old Covenant concept of sanctuary?
Do you basically approve of the concept? Do you see why it was that
without a biblical theocracy based on public oaths to God there
could have been no Old Covenant biblical sanctuary? What was the
nature of this theocracy? Political recognition of the principle of pub-
lic covenant oaths that call down covenant sanctions — external (tem-
poral, civil) and eternal — under God’s covenant law.

Do you think there is anything in the New Testament that has
significantly altered this judicial principle? If so, how? Think about
it. And then read on.

17. The reason why women did not normally exercise civil office in ancient Israel
was that they were not circumcised. They could serve as judges only through their
husbands, or because of their late husbands in the case of widows. Widows, as heads
of households, could take independent vaws (Deut. 30:9). Because females are bap-
tized, this judicial restriction no longer applies in the New Covenant era.

48. I capitalize State when I refer to civil government in general. I do not do so
when I refer to the regional political jurisdiction known as a state.

19, See Part 3, below.
