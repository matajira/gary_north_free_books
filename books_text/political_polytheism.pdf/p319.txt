Halfway Covenant Historiography 295

Marsden’s conclusions; Marsden writes something that seems to
support Noll. Hatch closed his only account the year he opened it, in
1977, but still dutifully co-signs the other man’s checks, and also runs
the checks back and forth between their banks. When things get
really desperate, they call in Woodbridge to cover.a check or two. To
the outsider who has never had an accounting course, they all look
rich. After all, they all drive nice cars and have credit cards from
three dozen oil companies.

Eventually, all check kiting schemes collapse. Then all the checks
are returned, marked “insufficient funds.” My offense in all this is to
expose the scheme by calling on all their readers to demand payment
in cash. Let us hope that their next co-operative endeayor will not be
counterfeiting.

Slow Learners

I write in this book’s Conclusion that Christians seem to learn
more slowly than their opponents during the “waking up phase” of
any era, While our enemies are “sealing the tomh” to prevent us
from faking the resurrection which they fear above all (Matt.
27:62-66), we Christians are fleeing from the scene of judgment in
confusion. Christ’s power-holding enemies need not fear our efforts!
The only thing that saves Christians is that God does direct every
event in history, and the progressive manifestation of God’s kingdom
in history does take place. The first stages of this “resurrection” of
Christian society take place without the awareness by Christians, let
alone their public approval. Eventually, however, a minority of
Christians. will wake up, re-group, and begin the work of compre-
hensive dominion anew. We have seen this again and again in his-
tory, most notably during the Protestant Reformation —a fact of his-
tory which nearly gags Protestant leaders today, especially college-
level historians.

These newly awakened Christians do not receive moral support
from the Church’s intellectual leaders of the sleeping phase; the lat-
ter have too much of their personal and intellectual capital invested
in the dying world of the sealed tomb. Like the Hebrew assistants to
the Egyptian taskmasters, or like the slave “trustees” of the Southern
plantation system, those who bear the tenured whip in the name of
the slave-masters resent any signs of God’s deliverance in history.
Such deliverance calls both their certification and their source of in-
come into question. They prefer that their fellow-slaves content
