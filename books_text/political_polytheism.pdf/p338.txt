314 POLITICAL POLYTHEISM

craft, adultery, fornication, perjury, kidnapping, whoremongering,
etc, They did not, as had been done in Massachusetts, identify these
crimes as crimes listed in the Old Testament, with passages cited
(e.g., Massachusetts’ Body of Liberties, 1641). Instead, they made
this statement:

These are the laws that concern all men, and these are the penalties for
transgression thereof, which, by common assent, and ratified and estab-
lished throughout the whole colony; and otherwise than thus what is herein
forbidden, all men may walk as their consciences persuade them, everyone
in the name of his god. And let the saints of the most high walk in this col-
ony without molestation in the name of Jehovah, their God for ever and
ever, etc., etc.?

This meant, however, that non-saints had the same civil powers and
immunities, that they, too, could walk in the colony without moles-
tation, and more to the point covenantally, vote in all colonial clec-
tions, “everyone in the name of his god,” or lack thereof.

In 1663, Charles I, as a self-identified Christian monarch, granted
to them in the name of “the true Christian ffaith,” a special dispensa-
tion: they would not have to worship God according to the Church of
England, “or take or subscribe the oaths and articles made and es-
tablished in that behalfe; . . .” The charter then adopted language
that was to be repeated again and again in the next hundred years of
charter-granting and constitution-making: “. . . noe person within
the sayd colonye, at any tyme hereafter, shall bec any wisc molested,
punished, disquieted, or called into question, for any differences in
opinione in matters of religion, and doe not actually disturb the civill
peace of our sayd colony: . . .”!° This he called a “hopefull under-
takeinge.”" The charter mentioned “the good Providence of God,
from whome the Plantationes have taken their name,”! but that was
a mere formality; the heart of the experiment was judicial. What is
remarkable in retrospect—and what has become standard fare in
making the case for modern Christian pluralism — was the King’s ex~
press hope that by severing the colony’s civil government from reli-
gion, the settlers “may bee in the better capacity to defend them-

9. Ibid, I, p. 349.

10. "Charter of Rhode Island and the Providence Plantation, July 8, 1663,” ibid.,
1, p. 121.

11. Hid., I, p. 120,

12, Idem.
