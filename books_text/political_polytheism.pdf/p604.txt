580 POLITICAL POLYTHEISM

in Christian civilization (Noll, Hatch, Marsden, as well as all dis-
pensational fundamentalists) nor regard it as a good use of one’s gifts
to promote it. As dispensationalist Peter Lalonde said: “It’s a ques~
tion, ‘Do you polish brass on a sinking ship? And if they’re working
on setting up new institutions, instead of going out and winning the
lost for Christ, then they're wasting the most valuable time on the
planet earth right now. . . .”4

Tom Paine’s Demon: The Bible

We know where antinomian (anti-covenantal) theology has
headed in the past: to Unitarianism, atheism, pluralism, moral
debauchery, national bankruptcy, and bloody revolution. It winds
up with the theology of Tom Paine: that in consideration of “the
obscene stories, the voluptuous debaucheries, the cruel and tor-
turous executions, the unrelenting vindictiveness, with which more
than half the Bible is filled, it would be more consistent that we
called it the word of a demon, than the word of God.”15

Is the Old Testament the word of a demon? If not, then why do
antinomian Christians — liberals and conservatives, neo-evangelicals
and fundamentalists—continue to ridicule Old Testament law?
They stick their fists in the face of the God of Psalm 119, and shout in
defiance of His law: “Is God really nothing more than the abstract,
impersonal dispenser of equally abstract and impersonal laws?”'6
Yes, He is much more than this. Among other things, He is the Eter-
nal Slavemaster over those who rebel against Him, the dispenser not
of abstract law but of personally experienced agony forever and ever.
Hell is real, The lake of fire is real. God is therefore not to be mock-
ed. But He has many mockers, and many of these mockers call
themselves by His name. They do not fear Him. For now. But even-
tually God will stick His fist in their faces. People may choose to ig-
nore Gad’s law; they will not be able to ignore AIDS.

Another major alternative to Paine’s sort of outright apostasy is
some variation of Marcion’s second-century heresy of the two-gods

14. Dominion: A Dangerous New Theology, Tape One of Dominion: The Word and the
New World Order, a 3-tape set distributed by the Omega-Latter, Ontario, Canada,
1987

15. The Age of Reason, Pt. I; cited by David Brion Davis, The Problem of Slavery in the
Age of Revolution, 1770-1823 (Ithaca, New York: Cornell University Press, 1975), p. 525.

16. Rodney Clapp, “Democracy as Heresy,” Christianity Today (Feb. 20, 1987),
p. 23.
