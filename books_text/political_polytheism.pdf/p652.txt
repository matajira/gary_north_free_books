628 POLITICAL POLYTHEISM

chica) discipline. A little bit of kingdom, but not too much —a dis-
count kingdom. We can almost hear it over the store’s intercom:
“Shoppers, today K-Mart has a fantastic deal on God’s kingdom.
Sixty percent off!” Not just 60% off the price; 60% off the kingdom
itself.

Invisible Kingdom, Silent God

Does God act in history? Yes, but He supposedly never tells us
when. He honors His covenant, no doubt, but supposedly this has
no public manifestations.* This way, God is nicely silent, which then
allows us Christians to make a permanent cease-fire agreement with
Satan's covenanted followers, who are forever to be our lawful earthly
employers and masters. We need their respect, you understand. We
need to duy their respect with our silence: As historian Mark Noll
says so plainly, “Christian historians themselves made a strategic ad-
justment that both opened the door to their participation in the uni-
versity world and encouraged more respectful treatment of religion.
This adjustment was to abandon —at least while working within uni-
versity precincts — the tradition of providential historiography stretch-
ing to Constantine’s Eusebius. It marked a willingness to consider
history writing in the sphere of creation rather than in the sphere of
grace, a manifestation of general rather than special revelation.”* In
other words, none of God's covenant sanctions in history are to be acknowt-
edged by Christian historians, Not those who expect tenure, anyway.

What these people want is discount salvation. Low, low prices, but
with all the “frills” removed. They want personal healing but without
any personal responsibility for extending the kingdom of God into
every area of life. They want a Creator God who is not a full-scale
Redeemer God. They want a God whe tells them, “Look, this world
is fallen, but not #haé fallen. It is corrupt, but not that corrupt. You
can find a degree of peace here. Just think of me as the God who
created the world, who then watched Adam pollute the world with
sin, yet who really isn’t interested in redeeming the world com-
pletely, ‘cause I'm just good old God the Creator, not God the
Redeemer. I am God the Sustainer of limited perversion in history,

3. This is the theology of Meredith G. Kline, George M. Marsden, and just
about every other Christian intellectual over the last century. In their theology,
AIDS is just another random event, humanly speaking.

4. M. Noll, “Contemporary Historical Writing: Practice and Presuppositions,”
Christianity and History Newsletter (Feb. 1988), p. 17.
