The Strategy of Deception 415

Mount Vernon. These men, in the words of Forrest McDonald, had
been “chagrined by the impotence of Congress, the recalcitrance of
state particularists and republican ideologues, and the seeming in-
difference of the population at large. . . .”® This phrase, “the seem-
ing indifference of the population at large,” is highly significant. It
testifies to a lack of concern and the absence of any sense of national
crisis on the part of the public in the year of the great Convention.
The sense of crisis was felt mainly by the nationalists at the Conven-
tion, the sense of crisis that they might “miss the moment,” or. in con-
temporary terms, miss the “window of opportunity.”

A Handful of Disgruntled Men

Americans think of the Philadelphia Convention as the place
where all the giants of the Revolutionary War era met to settle the
fate of the republican experiment. Some giants did show up; not all
of them. In retrospect, historians have usually defined “giants” as
those who did show up and did “stay with the program,” meaning
Madison’s coup. (The victors write the textbooks.) Early Constitution
history specialist Forrest McDonald’s description of the opening day
of the Converition is far closer to the truth: some of the best men
stayed away.

The list of distinguished Americans cerlain not to come was large. Only
one of the great diplomats of the Revolution; Franklin, would be there;
John Jay of New York and Henry Laurens of South Carolina had not been
chosen, and Thomas Jefferson and John Adams were in Europe as am-
bassadors. Most of the great Republicans would likewise be missing.
Thomas Paine (“Where liberty is not, Sir, there is my country”) was also in
Europe, hoping to spread the gospel of republican revolution. Neither Sam
Adams nor John Hancock of Massachusetts nor Richard Henry Lee and
Patrick Henry of Virginia chose to come (Henry did not because, he said,
“] smelt a rat”; the others offered no excuses).!°

It is important to note that Henry was a dedicated, Bible-believing
Christian.) Sam Adams, who also refused to attend, was either a

9. Forrest McDonald, Novus Ordo Seclérum: The Intellectual Origins of the Constitution
(Lawrence, Kansas: University Press of Kansas, 1985), p. 172.

10. Forrest McDonald, E Pluribus Unum: The Formation of the American Republic,
1776-1790 (Andianapolis, Indiana: Liberty Press, [1965] 1979), pp. 259-60.

11. On Henry's use of the Bible in his rhetoric, see Charles L. Cohen, “The
‘Liberty or Death’ Speech: A Note on Religion and Revolutionary Rhetoric,”
William & Mary Quarterly, 3rd ser., XXXVI (Oct. 1981), pp. 702-17.
