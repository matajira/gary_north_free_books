Halfway Covenant Social Criticism 217

independents both calling for your scalp, and with covenant theologians
standing on the sidelines, watching you get ripped to pieces? 197

By the time this essay appeared, James Robison, the hard-
preaching Baptist evangelist who in 1980 had co-sponsored and
hosted the openly political National Affairs Briefing Conference in
Dallas, had adopted a radical form of pietism, advocating a near-
charismatic view of life. A local charismatic layman had supposedly
chased the demons out of his life, as Robison described it in 1981,
and Robison was never the same. He became a spokesman for a the-
ology of zero confrontations among Christians. In 1984, Schaeffer
died, having lost the support of thousands of his pietist disciples be-
cause of The Great Evangelical Disaster. By 1985, Falwell had left the
visible political scene; in the summer of 1989, he shut down the
Moral Majority, his political action organization (which had been all
but dead since at least 1984). None of these men ever resolved this
theological dilemma. '##

Returning Halfway to the Covenant

Schaeffer's criticism of modern humanist culture forced him in
principle back to a doctrine he had been taught in seminary: the cov-
enant, But rather than just a Church covenant—the circumscribed
topic of seminary classrooms — the biblical covenant is also civil. The
concept of God’s kingdom on earth threatened Schaeffer's views
about both ethics and eschatology, just as it had threatened Van Til’s
view. Van Til could hide inside the Westminster Seminary library,
protesting that culture was not his field, pretending that his refutation
of natural law theory had not in principle unleashed the theocratic
whirlwind. Because of the nature of Schaeffer’s chosen arena of intel-
lectual confrontation, he could not hide from tough cultural questions
that could only be answered in one of two ways: either the biblical
covenant or the myth of neutrality (natural law or existentialism). He
feared the first and publicly denied the second. Then his fear over-
came him, and he dejectedly re-adopted the second alternative, in the
form of political pluralism. He died without resolving the dilemma.

137. North, “Intellectual Schizophrenia of the New Christian Right,” Christianity
and Civilization (1982), pp. 29-30.

138. The classic statement of this theological schizophrenia between “socially relevant
Christianity” and “anti-theocratic Christianity” is Charles Colson’s book, Kingdoms in
Conftict (1987), co-published by fundamentalist publisher Zondervan and humanist
publisher William Morrow.
