590 POLITICAL POLYTHEISM

disagree with them concerning the supposed necessity of defining
theocracy as a top-down social transformation. If God’s kingdom
rule is.to be widespread in its influence in society, this transforma-
tion must be from the bottom-up: se/f-government under God. So, we do
not call for a theocratic bureaucracy, either now or in the future.
Such a top-down bureaucracy is not called for in the Bible, is impos-
sible to maintain without unlawful coercion, and is not necessary to
impose to bring in the kingdom. Christian Reconstructionists call
instead for a decentralized, international, theocratic republic.** Such
a republic is ethically necessary, now and in the future, and it will be
historically possible in the future, when the Holy Spirit begins His
visibly triumphant sweep of the nations.

If postmillennialism is incorrect, and the Holy Spirit does not act
to bring huge numbers of people to eternal life, then Christians must
be content with only partial social reconstruction, and only partial
external blessings from God. The earthly manifestations of God’s

 

heavenly kingdom will necessarily be limited. When we pray, “Thy
kingdom come, thy will be done in earth, as it is in heaven,” we
should expect God to answer this prayer. Not all Christians pray this
prayer. Many dispensationalists do not, saying that it is a Jewish-era
prayer of Jesus, not a “Church Age” prayer. They at least are consist-
ent. Many less consistent Christians teach that God will never answer
this prayer before Jesus comes again physically to rule the world in
person (amillennialists and premillennialists), yet they still ritually
pray this prayer in church. If they are correct about the earthly king-
dom of God, then we will not see the pre-second coming advent of a
holy commonwealth in which God's laws are honored. We must con-
tent ourselves with less.

It is not possible to ramrod God’s blessings from the top down,
unless you are God. (If you are God, this ramrodding is the only sys-
tem that works, convert by convert.) Only humanists believe that
man is God. They do indeed believe in social salvation through ram-
rodding by the State. Christians are simply trying to get the ramrod
away from them and melt it down. This melted ramrod could then
be used to make a great grave marker for humanism: “The God That
Failed.”

34, E..C, Wines, The Hebrew Republic (Rt. 1, Box 65-2, Wrightstown, New Jer-
sey: American Presbyterian Press, 1980). This is a reprint of a short section of
Wines’ late nineteenth-century book, Commentary on the Laws of the Ancient Hebrews,
Book IT,
