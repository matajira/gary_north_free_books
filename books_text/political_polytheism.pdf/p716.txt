692 POLITICAL POLYTHEISM

than Marxism can permit counter-revolution, or monarchy a move to exe-
cute the king, or a republic an attempt to destroy the republic and create a
dictatorship.

[1973:] The commandment is, “Thou shalt have no other gods before
me.” In our polytheistic world, the many other gods are the many peoples,
every man his own god. Every man under humanism is his own law, and
his own universe. ®

[1988:] The Constitution is no defense against idolatry; . . .*

The Problem of Dualism

Here is a basic dualism of all humanistic thought: ethics vs. pro-
cedure in the judicial system. Max Weber, the great German sociolo-
gist, spent considerable space dealing with this dualism, and I de-
voted a section of my essay on Weber io just this topic in Chalcedon’s
book of essays honoring Van Til.® I concluded that discussion with
this warning: “Weber’s vision of the increasingly bureaucratic, ra-
tionalized society hinged on the very real probability of such a subor-
dination of substantive law to formal law. . . . He hated what he
saw, but he saw no escape. Bureaucracy, whether socialistic or capi-
talistic, is here.”®

Today, reversing his entire intellectual career (except for his early
view on the Constitution as somehow an implicitly Christian docu-
ment), including his commitment to Van Til’s presuppositional apol-
ogetics, as well as his commitment to biblical law, Rushdoony says
that the Constitution’s procedural morality can be end ts legitimately
religiously neutral, and that any interest group can adopt the Con-
stitution’s procedural morality to create whatever law-order it pre-
fers, without violating the text of the nation’s covenanting docu-
ment. But the text is all there is of the underlying religious foundation. If the
text were silent, then there would be no formal underpinning. But
the text is not silent. The text categorically prohibits the imposition
of the biblical covenant oath in civil law. Let us put it covenantally:
what the text of the U.S. Constitution prohibits is covenantal Christianity,

64. Tbid., pp. 38-39.

65. Ibid., p. 40.

66. Rushdoony, “U.S. Constitution,” p. 43.

67. Gary North (ed.), Foundations of Christian Scholarship: Essays in the Van Til
Perspective (Vallecito, California: Ross House Books, 1976), pp. 141-46.

68. Ibid., p. 146.
