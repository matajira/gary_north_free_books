From Coup to Revolution 473

Grand Architect. This Architect, however, was not the creedal God
of the Bible, and therefore not the covenantally divisive God of
either the Puritans or the Anglicans. This universalism or ecumen-
ism can be seen clearly in the Afiman Rezon, the constitutional hand-
book of Ancient Masonry.

The world’s GREAT ARCHITECT is our supreme Master; and the un-
erring rule he has given us, is that by which we work; religious disputes are
never suffered within the Lodge; for, as Masons, we only pursue the uni-
versal religion, or the religion of nature. This is the centre which unites the
most different principles in one sacred band, and brings together those who
were most distant from one another.

This God was a kind of Kantian “limiting concept” that under-
girded the phenomenal realm of cause and effect. This God was a
useful hypothesis. He was as impersonal as a mathematical formula.
In fact, the Masons regarded the knowledge of God in man to be
essentially the same as the knowledge of geometry.9? God's manifes-
tation in history is in His Masonic brotherhood. Masons in fellow-
ship manifest his presence. This quest for God’s presence is why the
pantheists could so easily capture existing Masonic lodges and adapt
them for their own purposes.

2. Hievarchy/Representation

Second, the theory of Masonic hierarchy was very much like that
of Puritan congregationalism: a structured assembly of moraj equals
with ranks in terms of ordination and function. A commoner outside
the Masonic hall could be elected Grand Master inside. Buck pri-
vates could rule generals. There was a hierarchy, but it was officially
egalitarian. It was officially open to all men, not just the elite. More
to the point, Masonry was a means by which average men could
come into contact with the rich and famous. Unlike real-world
churches, which officially possess an egalitarian worldview regarding
its members, but whose members seldom display it, Masonry ap-
peared to embody this originally Christian ideal, expounded in the
Epistle of James:

91. Ibid., pp. 106-7.
92. [James Anderson], The Constitutions of Free-Masons, p. 7. Printed by Benjamin
Franklin, 1734.
