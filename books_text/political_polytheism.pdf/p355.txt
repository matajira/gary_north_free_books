The Theological Origins of the U.S. Constitution 331

The former were closet heretics; the latter were open apostates, The
former were philosophical nominalists; the latter were philosophical
realists. The former were methodological individualists; the latter
were methodological collectivists. The former saw the “natural” de-
velopment of society as the unplanned, evolutionary outcome of vol-
untary legal contracts among men, contracts capable of revision; the
latter saw society as a voluntary metaphysical contract that cannot
subsequently be broken after consummation, and which is incarnate
in the State. Both groups sought to establish a new order of the ages
by substituting their respective forms of the covenant for the biblical
forms.

The Commonwealthmen

Bailyn traces the ideological origins of the American Revolution
to five sources: classical antiquity, especially Rome; the writings of
Enlightenment rationalism—Locke, Rousseau, Voltaire, Grotius,
Montesquieu, Vattel, Pufendorf, Baccaria; English common law;
Puritan covenant theology; and most important, the “Old Whigs” of
the early eighteenth century.*> These were the “Commonwealth-
men,” the intellectual heirs of those dissenting religious and human-
ist groups that -first made their appearance during the English Civil
War of 1640-60,5¢

The Commonwealthmen appealed back to the tradition of reli-
gious toleration that had been established by Oliver Cromwell dur-
ing the Puritan Revolution. His New Model Army was filled with
dissenters, and Cromwell. gave them what they wanted.5’ He
created a Trinitarian civil government in which all Protestant
churches would have equal access politically, and the state would be
guided by “the common light of Christianity.”°* (I call this “Athana-

55. Bernard Bailyn, The Ideological Origins of the American Revolution (Cambridge,
Massachusetts: Belknap Press of Harvard University Press, 1967), ch. 2.

56. A detailed study of their movement is found in Caroline Robbins, The Highteenth-
Century Commonaalthman (Cambridge, Massachusetts: Harvard University Press,
1959).

57. Christopher Hill, The World Turned Upside Down: Radical Ideas During the Eng:
lish Revolution (New York: Viking, 1972).

58. Nichols, “John Witherspoon,” op. cit., p. 172. He argues that this was Wither-
spoon’s view, not Roger Williams’ secularized version. There is nothing in the writ-
ings or life of Witherspoon that I have seen that would persuade me of Nichols’
thesis, Witherspoon echoed Locke, and Locke’s theory was basically Williams’
theory with Deistic modifications: a natural-law based political polytheism in the
name of an undefined Divine Agent. He did not refer to Cromwell.
