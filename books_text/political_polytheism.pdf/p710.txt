686 POLITICAL POLYTHEISM

Forces for secularization were present in Washington’s day and later,
French sympathizers and Jacobins, deists, Illuminati, Freemasons, and
soon the Unitarians. But the legal steps towards secularization were only
taken in the 1950's and 1960’s by the U.S. Supreme Court. For the sake of
argument [!!!!!—G.N.], we may concede to the liberal, and to some ortho-
dox Christian scholars,# that Deism had made extensive inroads into
America by 1776, and 1787, and that the men of the Constitutional Conven-
tion, and Washington, were influenced by it. The fact still remains that
they did not attempt to create a secular state, The states were Christian
states, and the federal union, while barred from intervention in this area,
was not itself secular. The citizens were citizens of their respective states
and of the United States simultaneously. They could not be under two sets
of religious law.

This is mytho-history designed to calm the fears of Bible-believing
Christians as they look back to the origin of the Constitution, Of
course the Framers created a secular state. The secular character of
the federal union was established by the oath of office. Politically, the
Framers could not in one fell swoop create a secular state in a Chris-
tian country; judicially and covenantally, they surely did: Hamilton
made it clear in Federalist 27 that the oath of allegiance to the Consti-
tution superseded all state oaths. That was why he insisted on it. Yet
Rushdoony substitutes the language of Church worship when speak-
ing of early American politics: “Officers of the federal govérnment,
president and congress, worshipped as an official body, but without
preference extended to a single church.”# This was true enough, but
it implied a great deal more than denominational neutrality; it im-
plied secularism. It led directly to the rise of religious pluralism, in
which Christianity receives no notice as the nation’s religion.

Today’s secularism is not simply the product of Chief Justice Earl
Warren and his court, let alone the theology of Madalyn Murray
O’Hair. It was implicit from 1789. It was made official in February,
1860, when the House of Representatives invited the first rabbi to
give the invocation, only a few years after the first synagogue was es-
tablished in Washington. They invited a New York rabbi, since no

 

43, He seems to have in mind here C. Gregg Singer's A Theological Interpretation of
American History (Nutley, New Jersey: Craig Press, 1964), ch. 2: “Deism in Colonial
Life.”

44, Rushdoony, Nature of the American System, p. 48.

45. Idem.

 
