Our mental image of the religious patriot is distorted because
modern accounts do treat the political paragraphs as a series of
theoretical expositions of Locke, separated from what precedes and
follows. When these orations are read as wholes, they immediately
reveal that the sociological sections are structural parts of a rhetori-
cal pattern. Embedded in their contexts, these are not abstractions
but inherent parts of a theology. It was for this reason that they had
so energizing an effect upon their religious auditors. The American
situation, as the preachers saw it, was not what Paine presented in
Common Sense—a community of hard-working, rational creatures
being put upon by an irrational tyrant —but was more like the recur-
rent predicament of the chosen people in the Bible. As Samuel
Cooper declared on October 25, 1780, upon the inauguration of the
Constitution of Massachusetts, America was a new Israel, selected
to be “a theatre for the display of some of the most astonishing dis-
pensations of his Providence.” The Jews originally were a free repub-
lic fourided on a covenant over which God “in peculiar favor to that
people, was pleased to preside.” When they offended Him, He pun-
ished them by destroying their republic, subjecting them to a king.
Thus while we today need no revelation to inform us that we are all
born free and equal and that sovereignty resides in the people—
“these are the plain dictates of that reason and common sense with
which the common parent has informed the human bosom”—still
Scripture also makes these truths explicit.

Once this light is allowed to play on the scene, we perceive the
shallowness of that view which would treat the religious appeal as a
calculated propaganda maneuver. The ministers did not have to
“sell” the Revolution to a public sluggish to “buy.” They were spelling
out what both they and the people sincerely believed, nor were they
distracted by worries about the probability that Jefferson held all
their constructions to be nonsense. A pure rationalism such as his
might have declared the independence of these folk, but it could
never have inspired them to fight for it.

Perry Miller*

*Miller, “From the Covenant to the Revival,” in The Shaping of American Religion, 4
vols:, edited by James Ward Smith and A. Leland Jamison (Princeton, New Jersey:
Princeton University Press, 1961), I, pp. 342-43.
