292 POLITICAL POLYTHEISM

Because God’s covenant sanctions are supposedly not revealed in
New Covenant history, although He swore that He would enforce
them in Leviticus 26 and Deuteronomy 28, Christians today therefore
have no justification in seeking to impose the civil government's sanc-
tions as set forth by God, and as Moses and Joshua imposed them.

Marsden has made his position clear: *. . . national distinctions
are no longer the basis for God’s blessings and curses as in the Old
Testament.”?25 Therefore, there can be no national covenant. There-
fore, there can be no Christian nation. Therefore, The Search for
Christian America did not find a Christian America. Surprise, sur-
prise! Will wonders never cease?

Ts the Ballot Box Really Sovereign?

Pluralism presents itself publicly as a political philosophy of
strictly formal procedures. It carefully distinguishes these formal
procedures from any particular moral or substantive goal (i.e., eth-
ics). Its philosophical defense is based on the logic of a certain form
of eighteenth-century, Anglo-Saxon, liberal Enlightenment human-
ism: the liberty of the autonomous individual citizen. This defense
has usually been connected with arguments relating to the freedom
of conscience. Pluralism is officially supposed to avoid being drawn
into debates over the ethical content of legislation. Ethically speak-
ing, as far as the official philosophy of pluralism is concerned, any-
thing goes, just so long as the State’s formal procedural requirements
are adhered to. I offer as evidence “abortion on demand” in the U.S.,
a law which was declared retroactively by the U.S. Supreme Court,
not by any state or federal legislature. “The Supreme Court has
ruled,” we are told by smirking, sanctimonious defenders of conven-
ient mass murder. “But abortion is immoral!” Christians reply. Reply:
“Nevertheless, the Court has riled. It’s the law.” To which the per-
ceptive Christian may choose to answer: “Then let’s test the law . . .
if necessary, 20,000 times a year in the courts.” And if the outraged
pluralist replies, “That's not playing fair; that would destroy the
court system,” the Christian may choose to remain silent, a Consti-
tutional guarantee.

The problem of pluralism is far deeper than just its vulnerability
to principled court-jamming. Who or what is to say that a formal ju-
dicial structure is ethically (substantively) sacrosanct? Who or what

225. Marsden, “Teaching of History,” of. cit., p. 40.
