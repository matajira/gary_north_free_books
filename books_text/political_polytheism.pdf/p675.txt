Conclusion 651

own nations to affirm God’s national covenant? The vast majority of
all social philosophers say no. They hate the very thought of a na-
tional covenant, let alone an international covenant.*® We are told
that pluralism is God’s plan for the ages, at least since the death and
resurrection of Jesus Christ. They argue that political and religious
pluralism are morally binding forever, and not just legally binding
today.

To which I respond: Who says so? Certainly not the philosophy
of political pluralism itself. Political pluralism, unique among all
political philosophies, allows for its own institutional suicide, It
allows people to repeal judicial pluralism. And in fact, this is pre-
cisely what is being done today by humanists in the United States,
led by the “neutral” U.S. Supreme Court. They are destroying
Christian judicial standards. The myth of pluralism is now being ex-
posed for what it has always been: a philosophical covering for soci-
ety during temporary periods when no one political group can gain
the allegiance, or at least the compliance, of the voters.

The judicial mark of political pluralism is the right to amend the
theological character of the national judicial contract. This is what
the humanists did nationally in 1787-88. This is what they have been
doing to state covenants in the United States since at least the time of
the Civil War. They used to be subile about this; confident of their
power today, they no longer are subtle in the least. Nevertheless, it
has only begun to occur to a minority of Christian activists that the
national judicial system is systematically stacked against them. What
they need to understand is this: ét always has been. From 1789 to the
present, the American political system has been stacked against
Christianity, for it rests on a pair of myths: the myth of political plu-
ralism and the myth of neutral natural law.

Fortunately, the American political system can be changed
peacefully, through the amending process. Political pluralism can be
reversed through amendment when self-conscious Christians have
the experience, the worldview, and the votes. If this takes two or
three centuries to accomplish, fine. Christians have time; humanists
don't. If the great reversal takes place in the aftermath of a collapsed
humanist culture, equally fine. Humanism does not work. It will
fail, When it fails, Christians must faithfully pick up the pieces and
rebuild according to biblical blueprints.

45. Gary DeMar, Ruler of the Nations: Biblical Blueprints for Government (Ft. Worth,
‘Texas: Dominion Press, 1987).
