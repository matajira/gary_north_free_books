The Strategy of Deception 444

The problem, in “David's” view, was that the new nation was
about to imitate the government of Rhode Island, or as he referred
to that province, “our next neighbours.” As editor Herbert J. Storing
comments, “This is one of the rare statements in the Federalist —
Anti-Federalist debate concerning the widely agreed-upon political
excesses of Rhode Island and her religious toleration.”!29 “David”
foresaw that if the new nation adopted as its civil model the anti-
covenantal, anti-oath contractualism of Rhode Island's political
theory, it would eventually become like Rhode Island. This thought
disturbed him: the result would be a national civil government based
strictly on force:

We have now seen what have been the principles generally adopted by
mankind, and to what degree they have been adopted in our own state.
Before we decide in favour of our practice, let us sec what has been the suc-
cess of those who have made no publick provision for religion, Untuckily
we have only to consult our next neighbours, In consequence of this publick
inattention they derive the vast benefit of being able to do whatever they
please without any compunction. Taught from their infancy to ridicule our
formality as the effect of hypocrisy, they have no principles of restraint but
Jaws of their own making; and from such laws may Heaven defend us. If
this is the success that attends leaving religion to shift wholly for itself, we
shall be at no loss to determine, that it is not more difficult to build an
elegant house without tools to work with, than it is to establish a durable
government without the publick protection of religion, What the system is
which is most proper,for our circumstances will not take long to determine.
It must be that which has adopted the purest moral principles, and which is
interwoven in the laws and constitution of our country, and upon which are
founded the habits of our people. Upon this foundation we have established
a government of influence and opinion, and therefore secured by the affec-
tions of the people; and when this foundation is removed, a government of
mere force must arise. 4°

Like John the Baptist, “David? was a voice crying in the wilder-
ness. Or more to the point, he was a voice crying in the promised
land warning people against departing into the wilderness; the
Rhode Island wilderness. They did not heed his warning.

129. Ind., TV, p. 249,
130, Ibid., IV, p. 248.
