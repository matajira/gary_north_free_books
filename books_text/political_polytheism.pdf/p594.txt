570 POLITICAL POLY THEISM

kept getting revised.’ Tugwell, one of the original members of Presi-
dent Franklin Roosevelt’s left wing “brains trust,”!* issued the for-
tieth draft of these discussions in The Emerging Constitution in 1974.18
Another group proposing a new constitution is the Committee on
the Constitutional System. '® Still another is the Jefferson Committee,
There is a growing fear among conservatives that a Constitutional
Convention will be called—the first since 1787 —and then captured
by the humanist Left. They are quite properly fearful of any such
move to amend the Constitution. !? Some traditional liberals are also
skeptical about any major tampering with the Constitution. 8

A New Covenant Document?

In mid-1986, Chief Justice Burger resigned from the Supreme
Court to take over the directorship of the Bicentennial Constitu-
tional Commission, whose meetings are closed to the public, despite
being funded by the federal government.!® This was a visibly
strange move: from the highest judicial position in the land to the
head of an obscure—deliberately obscure—closed-door “educa-
tional” organization. A political cartoon by Oliphant in late June,
1986, featured a drawing of Burger seated behind a little gift stand
which was located directly in front of the Supreme Court. The sign
on the stand said: “Constitutional Bicentennial Souvenirs.” Smaller
signs said: “Gifts!” “Buttons,” “Ashtrays.” The caption said: “Warren
Burger's most confusing decision.”

13. “Constitution for a United Republics of America: A Model for Discussion,
Version XXXVI (1970),” in Center Magazine (Sept./Oct, 1970).

14. R. G. Tugwell, The Brains Trust (New York: Viking, 1968); Bernard Stern-
sher, Rexford Tugwell and the New Deal (New. Brunswick, New Jersey: Rutgers Uni-
versity Press, 1964).

15. New York: Harper & Row Publishers.

16. Marvin Stone, “A Better Constitution?” The Editor’s Page, U. S. News and
World Report (April 9, 1984), p. 84.

17. Phyllis Schlafly, Plotting to Rewrite the Constitution, brochure (Feb. 1987); W.
Cleon Skousen, What Is Behind the Frantic Drive for a New Constitution (Provo, Utah:
Freeman Institute, n.d.).

18. “Leave the Gonstitution alone” suggests Arthur M, Schlesinger, Jr., who re-
jects the plans of the Gommittec on the Constitutional System: Associated Press story
by Donald M. Rothberg, Nov. 3, 1985.

19. Ralph Nader’s organization, Public Citizen, sued the Commission in October
1985 because of this closed-door policy. United Press International story, October Ii,
1985. The injunction was dismissed thal year by the Federal district court on the
grounds that these hearings were not advisory hearings but operational mestings, and
therefore not subject to the “open door” clause of the Advisory Act. The meetings re~
main closed.
