734

subordination of Church, 515, 526,
550
tyranny, 592-93
“wall of separation,” 410
Williams, 539 (see also Williams)
Cicero, 375, 453, 536, 579
Cincinnati, Society of, 434
circumcision, 70, 74
citizen, 28, 386
citizenship
hirch, 74, 393
cliecks & balances, 179
Christianity & classical, 81, 179
civil subordination only, 393
confession, 595
Constitution (1789), 676
dual, 620-21
Enlightenment, 676
Fourteenth Amendment, 392-94
geography, 393
Israel, 74
Massachusetts Bay Colony, 393
national, 451
oath, 72, 309, 393-94
Schaeffer on, 181
slaves, 393, 512
theocracy, 74
city on a hill, 90, 244, 361, 514
civil government (see Stace)
civil religion, 329, 360, 513-15, 655,
701-2, 704
civil rights, 72
Civil. War
centralized authority, 3950
citizenship, 393, 512
concealed, 116
Constitution, 366
Dred Scott, §12
family of man, 83-84
indivisible government, 388
irrepressible canflict, 264-65
Madisan’s silence, 497
resistance to new covenant, 386
Rhode Island, 117
Rousseau &, 391
tate constitutions, 682
civilization, 646-54 (see alse kingdom.
of God}

 

POLITICAL

POLYTHEISM

Clapp, Rodney, 34, 580
Clarke, Samuel, 346, 349-50
clans, 76
Clauson, Kevin, 656
clock analogy, 354, 364
coercion
civil law &, 191
democracy &, 265
Holy Spirit, 589
Madison on, 380
Neuhaus on, 23
persuasion, 101-102
Coke, Edward, 377-78
College of New Jersey, 526, 547-48
(see also Princeton)
college professors, 585
colleges, 15-1%
colonies, 269-76, 383-85
Columbus, 10
Commager, H. S., 316, 485, 489
commerce, 453
Committees of Correspondence, 435
common enemy, 522-23, 527
common good, 697
common grace
covenant model (universal), 491
external blessings, 637
fading of, 697
Japan, 637
logic, 225-26
national rivalry, 531
natural law, 187
restraint, 301
Van Til, 143
Williams, 539
common ground
American Revolution, 522-28
ethical, 521-22
Great Awakening, 361
history of, xv, 521, 535
justice, xxi, 538
mark of sovereignty, 392
Newtonianism, 333, 351
social theory, 538
common law marriage, 17
comman sense, 319
Commonwealthmen, 321-23, 351,
471, 500, 515
