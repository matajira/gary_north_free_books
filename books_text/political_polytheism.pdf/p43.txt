Introduction 19

grim, and my prescription sounds so unpalatable. After all, the ship
has done so well inthe past, and nobody likes to eat dehydrated food.

The Risk of Being Premature

The Puritan Revolution of 1638-60 was premature. The people
of England were not ready for a world without king or bishops, and
Calvinist intellectual and military leaders soon found that they could
not control the social forces.that their pamphlets and their cavalry
had unleashed. The growing reaction against “the rule of the saints”
during the 1650's, and especially after the restoration of Charles II to
the English throne in.1660, devastated English and Scottish Calvin-
ism. They never recovered. English and American colonial Calvin-
ism became pietistic and antinomian within a generation.5! So did
Holland’s Calvinism during this same period. Few- traces of postmil-
lennialism’can-be found in Dutch Calvinism after 1700. Radical En-
lightenment ideas and. clandestine political organizations had been
released as a side-effect of the Puritan Revolution in England; they
spread rapidly to Holland in the 1690's, and from there to the whole
of, Europe.*? So did the Christian-Newtonian synthesis and its
accompanying Arminianism and Socinianism.™ It does not pay to
be premature.

It is not my intention to encourage the repetition of that seventeenth-
century mistake. Productive social transformation takes time and
lots of it. Great religious and social discontinuities are inevitable in
human history, but in order to produce beneficial results, they must
be preceded by and followed by long periods of patient and disci-
plined thought, investment, and work. In this sense, I am a conser-
vative social theorist. I'see an enormous discontinuity looming, and

51. The testanient of this morbid introspective pietism is William Gurnall’s two-
volume treatise, The Christian In Complete Armour; A ‘Treatise Of the Saints’ War against the
Devil (London: Banner of Truth, [1655-62] 1964). A study of the American Puritans
is Gary North, “From Medieval Economics to Indecisive Pietism in New England,
1661-1690," Journal of Christian Reconstruction, VI (Summer 1979), pp. 136-74.

52, The writings of historian Margaret C. Jacob are important studies of how
this took place. See the detailed annotated bibliography in Jacob’s book, The Radical
Enlightenment: Pantheisis, Freemasons and Republicans (London: George Allen & Unwin,
1981), pp. 1-19.

53. Margaret C. Jacob, The Cultural Meaning of the Scientific: Revolution (Phila-
detphia: Temple University Press, 1988).

54. Gary North, Moses and Pharaoh: Dominion Religion vs. Power Religion (Tyler,
‘Texas: Institute for Christian Economics, 1985), ch, 12.
