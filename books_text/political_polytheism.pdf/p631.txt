Winners and Losers in History 607

eschatology cannot not deal with the fact that the positive blessings
of the covenant will be manifested in history, when sinners will still
be alive. He would have to come out and say that all the historically
specific language is really symbolic. To argue such a position is to argue
nonsense in public; thus, he simply refused to discuss the passage.

It is this passage, more than any other in the Bible, that catego-
rically refutes amillennialism. This passage deserves a whole vol-
ume; instead, it receives the silent treatment. Instead of dealing with
the text that speaks of the positive sanction of the kingdom of God in
history, Hughes removes God’s kingdom entirely from history: “Fur-
ther, the Kingdom is no longer of the earth, earthly, but of God and
heavenly, being the Kingdom of God and Heaven.” The blessings
of God are equally ethereal: “At present spiritual blessings are en-
joyed by faith without sight, but in the New Heavens and the New
Earth all will be visible, but that which will be seen will not be that of
the past old order but the transformed glorified creation —a truly lit-
eral spiritual creation.”* In short, éhere is no continuity between history's
blessings and heaven's blessings. This is why amillennialism, for all its
public denial of premillennialism’s kingdom discontinuity —bless-
ings in history and God’s civic negative sanctions in history —is itself
radically discontinuous. There are no positive kingdom sanctions in history
for amillennialism. Because of this radical discontinuity of sanctions,
the amillennialist also asserts a radical discontinuity of ethics: no
Old Testament civil stipulations for the New Testament era.

The Calvinist amillennialist affirms one half of point one of the
biblical covenant: the transcendent sovereignty of God. This sover-
eignty, however, is supposedly not manifested in New Covenant
kingdom history, i.e., there is no visible sign of God’s judicial presence
in this era of history. We see this position articulated clearly in the
book by Raymond Zorn on the kingdom of God. Zorn begins his
amillennial study with these words: “In the broadest sense God’s
Kingdom refers to the most extended reaches of His sovereignty. As
Psalm 103:19 puts it, ‘The Lord hath prepared his throne in the heav-
ens; and his kingdom ruleth over all.’”. The kingdom of God is all-

3. Hughes, New Heaven, p. 160.

4, Bid., p. 161,

5, Raymond O. Zorn, Church and Kingdom (Philadelphia: Presbyterian and Re-
formed, 1962), p. 1, Zorn, an amillennialist, stresses the kingdom as the reign of
God rather than the sphere or domain of His rule (p. 1). Greg Bahnsen’s response to
this sort of argument is correct: it is ridiculous to speak of the reign of a king whose
kingdom has few if any historical manifestations that are as comprehensive in scope
