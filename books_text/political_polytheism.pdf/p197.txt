Halfway Covenant Social Criticism 173

theological professor [note: he does not say “the professor who had
taught Schaeffer apologetics four decades earlier”], who for years
often publicly criticized Schaeffer's way of helping people become
Christians, retired and then wrote a long critical treatment of
Schaeffer's view of Christian faith and evangelism. Rev. Schaeffer
responded privately to some within L’Abri: ‘I would hate to think
that I might spend my retirement doing something like that!’ ”?°
Clearly, Schaeffer took these criticisms personally, never under-
standing that Van Til wrote strictly from a professional and intellec-
tual basis. He treated everyone else the same way! Van Til under-
stood what every Christian apologist must do: defend the faith, espe-
cially from those who have made crucial errors that will inevitably
Jead to disastrous intellectual (i.e., moral) compromise later on.
This is exactly what has been the fate of the Schaeffer movement in
less than half a decade since his death: disastrous compromises with
secular humanism, and all in the name of Christian pluralism.

This same commitment to the neutrality doctrine was subse-
quently reflected in Schaeffer's social theory in A Christian Manifesto
(1981). What I argue in this chapter is that his commitment to the
neutrality doctrine in philosophy destroyed what little remained of
the Schaeffer movement after his death in 1984, When the Schaeffer-
ites at long last realized that they could not maintain a commitment
to biblical Christianity and ethical and epistemological neutrality in
their so-called “secular” callings, they abandoned their formerly out-
spoken commitment to Christianity in their respective professional
callings. This has been most visible in Dr. C. Everett Koop’s “con-
dom wars” and in Franky Schaeffer's ill-fated 1987 R-rated movie,
Wired to Kill.

Every Christian social theorist is required by God to make a key
intellectual (but ultimately ethical) decision. He must identify one
and only one of the following as a myth: neutrality or biblical theocracy.
Christian social theorists today will do almost anything to avoid
making this identification, but sooner or later they have to decide.
From the demise of Puritanism after 1660 until the appearance of the
Christian Reconstruction movement, Christian social theorists
sought to avoid this decision, in order to avoid deciding in favor of
theocracy, meaning the world under God's revealed law. Implicitly

29. Louis Gifford Parkhurst, Jx., Francis Schaeffer: ‘the Man and His Message
(Wheaton, Ilinois: Tyndale House, 1985), p. 49.
