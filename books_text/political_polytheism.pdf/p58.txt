34 POLITICAL POLYTHEISM

is the sovereign judge.in history, not God. This man-centered theol-
ogy is the heart of Darwinism, not its technical discussions about
genetic or environmental changes.®

This view of history is basic to all of modern scholarship, and the
vast majority of those teaching social theory and social ethics in
Christian colleges have adopted the basic anti-covenantal perspec-
tive of this worldview, at least with respect to New Testament era
history. The assertion that nations remain poor because they are
breaking the external terms of God’s covenant outrages the modern
Christian intellectual. It was not random that in its. hatchet job on
the Christian Reconstructionists, Christianity Today van a clever pen
drawing caricature of me close by. my statement: “The: so-called
underdeveloped societies are underdeveloped: because they are so-
cialist, demonist, and cutsed.”’ I said it, I have defended it intellec-
tually,’ and author Rodney Clapp cited it because he apparently re-
garded it as the most offensive statement that he. could locate in his
rather cursory examination of my writings. He recognized that the
neo-evangelical audience of Christianity Today. would take great
offense at such a statement.°

What I am arguing is simple: those people who truly believe that
God’s multi-institutional covenant is binding must of necessity also
believe that it is Aistorically and judicially binding with respect to all three
covenant (oath-bound)" institutions: family, Church, and State.
Conversely, if people do not believe that God’s covenant is histori-
cally and judicially binding with:respect to nations and local civil gov-
ernments, then they have denied the relevance of Deuteronomy 4:5-6.
They implicitly believe that the biblical doctrine of God’s national
covenant is some kind of New Testament theological “limiting con-

6. North, Dominion Covenant: ’ Genesis, Appendix A: “Froin Cosmic Purposeless-
ness to Humanistic Sovereignty.”

7, Rodney Clapp, “Democracy as Heresy,” Christianity Today (Feb. 20, 1987),
p. 23.

8. North, Unholy Spirits, ch. 8. This also appeared in the original version of this
book, None Dare Call It Witchcraft (New Rochelle, New York: Arlington House,
1976).

9. Keynesian William Diehl tok offense at this cause-and-effect explanation of
culture-wide poverty, citing in response Jesus’ denial of this relationship in-the case on
an individual blind man ( John 9:1-3): “A Guided-Market Response,” in Clouse (ed.),
Wealth and Poverty, pp. 71-72. Art Gish was also upset: ibid., p. 78.

10. Gary North, The Sinai Strategy: Economics and the Ten Commandments (Tyler,
Texas: Institute for Christian Economics, 1986), ch. 3: “Oaths, Covenants, and
Contracts,”
