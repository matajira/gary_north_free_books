The Restoration of Biblical Pluralism 599

had no knowledge of what was going on (Luke 7), so is the judicial au-
thority of Christ transferred through the sacrament in infant baptism.
Baptism does not save anyone; it merely places people under the
formal, legal, covenantal sanctions of God. Those legally under a
Christian’s covenantal authority in a family should be baptized when.
the head of the household is, even if the subordinate is not a believer.
The issue is not saving faith for those under lawful covenantal au-
thority; the issue is obedience to God’s law within a covenantal hierarchy,
Baptism of infants is therefore mandatory.*? Obviously, children of
non-communicant members have no right to the sacrament of bap-
tism—a denial of original halfway covenantalism in New England.
But if this is true, then infant communion or very young child
communion is equally mandatory, a fact that has not been accepted
by modern ‘halfway covenant” Presbyterianism.* Churches give
communion to retarded and senile people who do not know what is
going on or what is being symbolized; churches have the same obli-
gation to intellectually immature children of a communicant mem-
ber. This is so obvious that only halfway covenant theology blinds
the modern Church to the nature of communion. The modern
Church is still hypnotized by the Greek ideal of the primacy of the
intellect rather than the primacy of the covenant. Any church that
refuses to give communion based on baptism and church member-
ship alone, and then justifies itself in terms of the intellect, is con-
demning itself by “endangering the souls of intellectually incompetent
retarded and senile people” if it continues to serve them communion.
The churches must move to infant baptism and weekly commun-
ion, (Weekly communion was John Calvin’s choice, but the civil
magistrates of Geneva prohibited this practice. They saw the threat
to their authority.) Churches may have to do this in steps, just as the
defenders of theocratic republicanism should move only as the politi-
cal environment allows. We are not to become revolutionaries. But
the long-term goal is clear: infant baptism and also weekly communion for
all baptized church members. If God’s people reject the legitimacy and
necessity of the sanctions of covenant theology in the churches, then
surely they will not persuade anyone else regarding the sanctions of
covenant theology in civil government.
Once this is done, the negative sanction of public excommunica-
tion will become meaningful. People must be consigned to hell ver-

42. Sutton, That You May Prosper, Appendix 9.
43. Idem.
