500 POLITICAL POLYTHEISM

had made this appeal to the voters not only logical but covenantally
necessary. And being necessary, Mr. Madison did his organizational
homework well in advance. He made sure that the Federalists would
speak for the People.

Let us not be naive, We are not classroom historians, after all.
When we read of elections behind the Iron Curtain—pre-1989 elec-
tions, that is—or elections in some African democracy, we are not
surprised to learn that the existing national administration has been
re-elected almost unanimously. We are not surprised because we
know that the elections were rigged by those in power. We know it
was not a representative procedure. Yet how many American history
textbooks raise the obvious question: How did it happen that nine
out of the first nine state ratifying conventions voted to ratify, yet
from what we can determine from the documentary record, the ac-
tual voting public was evenly split? The Man Who Hated Mono-
lithic Faction organized one whale of a monolithic faction in 1787-88.
The angry young man got even.

This theology of the People had been prominent in political
theory since at least the sixteenth century, but it had been offset by the
Christian doctrine of the Creator God. He was seen as both the initi-
ating authority and the final authority. Men had long debated over
who held lawful claim to be God's final earthly authority, but there
had been no doubt that this final earthly authority was under God.
But in the early eighteenth century, this assumption steadily disap-
peared in the writings of the Commonwealthmen, especially in the
popular newspaper, Cato’s Letters. The language of divinity is applied
to the People in this 1721 essay on libel:

IT have long thought, that the World are very much mistaken in their
Idea and Distinction of Libels. It has been hitherto generally understood
that there were no other Libels but those against Magistrates, and those
against private Men: Now, to me there seems to be a third Sort of Libels,
full as destructive as any of the former can possibly be; I mean, Libels
against the People. It was otherwise at Athens and Rome; where, though par-
ticular Men, and even great Men, were often treated with much Freedom
and Severity, when they deserved it; yet the People, the Body of the People,
were spoken of with the utmost Regard and Reverence: The sacred Privileges
of the People, The inviolable Majesty of the People, The auful Authority of the People,
and The unappeatable Judgment of the People.”

27. “Reflections upon Libelling” (June 10, 1721); reprinted in The English Libertar-
ian Heritage, edited by David L, Jacobson (Indianapolis, Indiana: Bobbs-Merrill,
1965), p. 75, Italics in original.
