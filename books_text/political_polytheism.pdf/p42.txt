18 POLITICAL POLYTHEISM

Covenant Theocracy. Because of this, no ship’s officer is willing to get
into it; they are all graduates of the Erasmus Naval Institute. Further-
more, they are all doing whatever they can to keep the growing
number of alarmed passengers from launching it on their own. They
keep announcing the following message over the-ship’s intercom:
“The ship is fine. No problem. Don’t worry; be happy! Be calm, be
cool, and be collected.” Meanwhile, the storm clouds keep getting
closer, and the ship is listing to port.

Question; Should the passengers rearrange the deck chairs one
more time or head for the lifeboat? I have made my decision; what
about you?

There are a lot of passengers who still take the ship’s officers seri-
ously. They. still: have faith in what they are being told ’rather than
what: they can clearly see. After all, these officers are so-.."..so ofi-
cial! They are all dressed in clean, white uniforms. Could they really
be wrong? Could a man in a white uniform be unreliable? (Sée
Chapter 4.) They were certified by the authorities back on shore.
Could the authorities have been wrong? The thought never crosses
the passengers’ minds that the authorities back on shore had cut a
deal with the ship’s owners: “We will certify your ship and your crew
if you continue publicly to support our right to certify everyone. And
we promise to prohibit any uncertified rival entrepreneur from start-
ing a rival cruise ship line.”

In the world of business this is called licensing. In the academic
world, it is called regional accreditation and classroom tenure. What
this system of certification invariably produces in the long run is a
fleet of high-priced leaking ships and a new generation of white uni-
formed officers who don’t know how to navigate. But they dé look
impressive.

The purpose of this book is to show what does not work, and.then
to offer an alternative. Like an informed passenger on that leaking
ship, I need. to alert my fellow passengers to the problem before I can
expect them to head for the lifeboat. I have confidence that the life-
boat is sea-worthy,™ I also have confidence that the ship is sinking.
The time has come for me to call, “Abandon ship!” The problem is,
not many people will believe me initially. My diagnosis sounds so

50. R. J. Rushdoony, The Institutes of Biblical Law (Nutley, New Jersey: Craig
Press, 1973); Greg L. Bahnsen, Theonomy in Christian Ethics (2nd ed.; Phillipsburg,
New Jersey: Presbyterian & Reformed, 1984).
