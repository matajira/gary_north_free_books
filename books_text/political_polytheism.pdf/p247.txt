5
HALFWAY COVENANT HISTORIOGRAPHY

What of the place of the Bible, then, as a basis for political action?
Should we not bring the nation and its legislation back ta the Bible? Here
we have to make a careful distinction. Christians’ own political decisions
should be informed by biblical principles. This is an important point not
to miss. Nevertheless, when bringing these decisions to bear on civic de-
bate and legislation we must agree to the rules of the civic game, which
under the American constitution are pluralistic. That means that no mat-
ter how strongly the Bible or other revelation informs our political views,
for the purposes of civic debate and legislation we wei! not appeal
simply to religious authority.

Noll, Hatch, & Marsden (1983)'

Historians Noll, Hatch, and Marsden offer us what they propose
as a defense of political pluralism as the theoretical and judicial basis
of the American political experiment. They offer this as Christian
scholars.? They are professors in both senses: collegiate and confes-
sional. What I wish to discuss in this chapter is one question: Are
they trustworthy professors?

Their statement regarding the Bible and political action sounds
so reasonable. What Christian would reject the idea of becoming in-
formed by “biblical principles”? This does raise some controversial
questions: “Which principles? Derived through what source?”
Another important question: Are these biblical principles permanent
principles? If so, then they are éaws. Are these men calling for a

1, Mark A. Noll, Nathan OQ, Hatch, and George M. Marsden, he Search for
Christian America (Westchester, Illinois: Crossway, 1983), p. 134.

2, Noll teaches at Wheaton College, Hatch at Notre Dame, and Marsden in 1983
taught at Calvin College. He is now a professor at Duke Divinity School, one of the
most radically liberal theology departments in the U.S. It has a great library, how-
ever, which I thoroughly enjoyed using free of charge in the late 1970's.

223
