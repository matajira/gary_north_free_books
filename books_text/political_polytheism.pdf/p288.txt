264 POLITICAL POLYTHEISM

ism in the Civil Compact Derby, an aged, winded nag that has al-
ready begun to fade in the stretch.

They insist that “The American political system is very frankly a
system of compromises.”!26 It surely was deeply compromised in
1789 with respect to the institution of chattel slavery, and all the
political pluralism in the world could not have made that comprom-
ise any less morally repugnant. The inherent conflict escalated; it
had to. The Civil War was an irrepressible conflict, given the unwill-
ingness of the South to honor the New Testament’s annulment of
Israel’s jubilee land tenure and its permanent slave system. What
this nation needed after 1800 with respect to chattel slavery was not
more pluralism but less. It needed a Constitutional settlement that
announced at long last a closed door to the religion, ideology, eco-
nomics, and civilization of chattel slavery. The world needed to close
that open door from the day that Jesus announced His fulfillment of
the jubilee year. !?”

The American Civil War stands as a visible testimony to the fail-
ure of political pluralism. Bullets replaced ballots. The North’s view
of Constitutional law triumphed over the South’s, not in political cau-
cuses but on battlefields. Why do our authors refer again and again
to the evils of American chattel slavery, if they also think that politi-
cal pluralism provides anything like permanent peaceful solutions to
what are clearly inescapable moral conflicts? Why do they want to
avoid getting fundamental moral issues solved politically, and in
terms of biblical revelation? I perceive that they are of the opinion
that the moral issues raised exclusively or primarily by Christian
theology are of far less importance morally and especially judicially
than common-ground (i.e., Unitarian-dominated) abolitionism was,
and so these Christian issues must be adjudicated in religiously “neu-
tral” civil courts. This leads to the following outlook: “The issue of
slavery dealt with by the Dred Scott case was too important an issue to
be left to the Supreme Court to decide; Roe ». Wade, on the other
hand, is not equally crucial, so Christians must abide by it.”

In Britain, the slave issue was solved by democratic coercion; the
slave-owners did not have the votes after 1832. Not so in the U.S.
Slaves were kept in chains here for an additional three decades, and

126, ‘The Search, p. 133.
127. Gary North, Tools of Dominion: ‘The Case Laws of Exodus (Tyler, ‘Texas: Insti-
tute for Christian Economics, 1989), pp. 228-30.
