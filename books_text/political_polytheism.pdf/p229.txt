Halfway Covenant Social Criticism 205

pluralism seriously as an ethical system are risking similar results in
their careers: acceptance by the humanist media at the cost of a be-
trayal of both their faith and their Christian constituents. The price
is too high, But Christians have been enthusiastically paying it in
North America for over three hundred years.

Iprrell Writes Koop’s Epitaph

T cannot resist quoting at tength from R. Emmett Tyrrell’s evalu-
ation of Koop’s tenure in office. Tyrrell is not a Christian, but he is a
conscrvative. As editor of The American Spectator, he brought the mag-
azine (now a tabloid) from utter obscurity in 1967 to perhaps the
leading popular organ of conservative opinion in the United States
by the early 1980’s.!13 His editorial style is modeled along the lines of
H. L. Mencken’s. But allowing for his stylistic exaggeration,
Tyrrell’s comments are on target. He wrote the political obituary of
CG. Everett Koop a few weeks before Koop publicly announced his
retirement. It appeared in his regular column, “Public Nuisances.”

As with most evangelical Christians, he was not on the offensive, expanding
the power of the church against state; rathcr he was the embattled believer
attempting to preserve traditional morality from the onslaughts of the New
Age ideologues directing the police power of the state against the church.
But once in office Dr, Koop proved his modern political mettle, which is to
say his capacity for publicity, braggadocio, and sniffing out the course of
least resistance. Thus as the months slipped by so did Surgeon General
Koop, and in June 1987 our own Tom Bethel) awarded the Surgeon General
his Strange New Respect Award “for putting condoms before continence,”
Bethell confers this award upon that conservative who suddenly rises in the
esteem of Liberals by seeing the world from their point of view. . . . From
opposing abortion and homosexuality he has slipped into a mindless toler-
ance, with no explanation offered. His solution to the spread of AIDS is the
placcbo propounded by all dope-fetchers, to wit: he urges education, Fur-
thermore, he is an aficionado of the condor, He beseeches the sexually
aroused — whether homosexual or heterosexual—to roll on those condoms,
consult a nearby sex manual, and recall all relevant data proffered in Sex
Ed. Sex education is the cure for all the perils of Eros, according to Dr.
Koop, and the great enlightenment should begin with kindergarten and
continue until all kinks have been covered. . . . How sex education might
benefit kindergartners who have only recently discovered that their private
parts are useful for discharging bodily waste, I cannot imagine. 1!

113. T wrote for it in the early 1970's until [ started my own publishing frm.
114, Tyrrell, “Chicken Koop,” pp. 34-35.
