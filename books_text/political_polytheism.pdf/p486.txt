462 POLITIGAL ‘POLYTHEISM

State Constitutions

The colonies’ state constitutions were explicitly religious. This
was especially true of the New England constitutions. The old Puri-
tan rigor was still visible at the outbreak of the Revolution.
Vermont's 1777 constitution begins with the natural rights of man
(Section I), goes to a defense of private property (Section II), and
then sets forth the right of religious conscience, “regulated by the
word of GOD. . . .” There is full religious freedom for anyone to
worship any way he chooses, just so long as he is a protestant: “. . . nor
can any man who professes the protestant religion, be justly deprived
or abridged of any civil right, as a citizen, on account of his religious
sentiment. . . .” The public authorities have no authorization to in-
terfere with people’s rights of conscience; “nevertheless, every sect or
denomination of people ought to observe the Sabbath, or the Lord’s
day, and keep up, and support, some sort of religious worship, which
to them shall seem most agreeable to the revealed will of GOD.”
(Not reproduced in this American Bar Association compilation are
the crucially important clauses regarding the required confessional
oath administered to state officers, such as those I have reproduced
in Chapter 7 under the section, “Before the Constitution.”)

The 1780 Massachusetts constitution and the 1784 New Hampshire
constitution had almost identical passages requiring public worship.
Section I of the Massachusetts document affirms that “All men are
born free and equal, and have natural, essential, and unalienable
rights,” and then lists men’s lives, liberties, and property ownership.
Section I] says: “It is the right as well as the duty of all men in society,
publicly, and at stated seasons, to worship the SUPREME BEING,
the great Creator and Preserver of the universe.” This sounds uni-
versalistic and even Masonic. But Section III establishes the right of
the state to support the building of churches-and the payment of
ministers’ salaries. All the denominations were placed on equal
status. Section III ends with these words: “And every denomination
of Christians, demeaning themselves peaceably, and as good sub-
jects of the commonwealth, shall be equally under the protection of
the law. . . .”6 The same religious provisions are found in Sections
I-VI of the New Hampshire constitution, and Section VI repeats

 

55. Richard L. Perry and John C. Cooper, Tie Sources of Our Liberties (Chicago:
American Bar Association, 1952), p. 365.
36, Ibid., p. 375.
