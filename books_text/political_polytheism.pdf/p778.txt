754 POLITICAL POLYTIIEISM

procedure, 688
Rhode Island, 380
Rome, 80
Rushdoony on, 695
Rushdoony’s, 693-94
Schaeffer, 171-74, 185
State, 96-97, 539
subsidies, 96-97
tattered flag, 107
test oath, 367
vampire bat, 654
Van Til, 53, 129, 638
Whigs, 361n
Williams, 114
New Hampshire, 462-63
New Heavens & New Earth, 606-7
New Israel, 243-45, 513, 718
New Jersey, 272-73
New Lights, 359
New School, 359
New Side, 359
New Testament (see Bible)
New York, 273
newspapers, 436
Newton, Isaac
alchemy, 335-36
Babylonian, 336
cover-up, 350
Einstein &, 52
ether, 345-46
god of, 341-42, 346, 354-55, 523,
524, 558, 579
gravity, 345
Hume, 579
key figure, x
Jatitudinarians, 351
legacy of, 323-24
McDonald on, 369
natural law, 335
Opticks, 345-46
Principia, 340-41
providence, 340-41
providentialism, 348
Temple, 342
theism, 105
worldview, 334
Newton, Joseph Fort, 468

Newtonianism
alliance, 333, 351
autonomy, 362
Christianity &, 349-55, 461
civil liberties, 449
classics &, 328
clock analogy, 354, 364
Constitution &, 368, 453, 459
compromise with, 461
Deistic, 341, 347
dualism, 339-40
dynamic, 355
ecumenical, 468
experience, 364
Great Awakening, 356
halfway covenant system, 347
impersonal, 347
impersonal sin, 254
liberal Protestantism, 351
monotheistic, 468
pantheism, 337, 348
power, 355
replaced, 454
social theory, 461
threat, 279
Unitarian, 345, 523
universalism, 459, 468
Niebuhr, H. Richard
antinomian, 668-69
apostate, 668, 671
Barthian, 668, 672
blueprints, 667
dilemma, 232-35
false Christ, 668
final judgment, 669, 671-72
incoherent, 670-71
Kantian, 233
Marsden &, 261
mumbling, 669
noumenal, 671-72
relativism of faith, 668-70
theological swamp, 669
Van Til on, 672
Niebuhr, Reinhold, 233-34
Nisbet, Robert
progress, 614
prophecies, 614
