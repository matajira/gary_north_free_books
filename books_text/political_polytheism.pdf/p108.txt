84 POLITICAL POLYTHEISM

structs each person: “Choose this day which of these two interna-
tional ‘public works projects’ you wish to get devote your life to.”5!

One of the areas of conflict between the kingdom of Christ and
the kingdom of Satan is politics. Humanism makes this arena the
most prominent one, not Christianity. Salvation by politics is a hu-
manist myth. As the kingdom of humanism has extended over the
face of the earth, political conflicts have escalated. The twentieth
century has been the age of mass warfare and the age of politics.”
These two facts are not randomly connected.

Christian political theory asks this question: Who is authorized to
speak for God in civil government? Also: On what judicial basis is this
spokesman so authorized? Someone must speak the Word of God in
judgment in civil government, just as in church government and
family government. If God’s judgmental Word is not spoken, then
man’s judgmental word will be, The question is; Can there be a per-
manent political arrangement in history in which God’s Word and
man’s word are spoken together?

This is the fundamental question answered by political plural-
ism. It answers yes. How can Christians legitimately also answer yes?

Problems With Political Pluralism

There are many problems with the modern philosophy of politi-
cal pluralism. Permit me to list five of them.

1. Political pluralism is founded on a lie, namely, that all political issues
are not at bottom religious, Political pluralists refuse to admit that tempor-
ary religious and cultural cease-fires are not permanent peace treaties. At.
best, pluralism masks the escalating historical conflicts only for a season.

2. Political pluralism is ultimately based on polytheism: many moralities,
many gods, many gods. It ignores the fact that every god offers a covenant to
men, and each insists that men submit to its terms. The terms vary, god to
god.

3, Under political pluralism, we eventually get civil wars anyway. One
side sees its covenant as the covenant of life, and its rival’s covenant as a
covenant with death,

51. Gary North, Healey of the Nations: Biblical Blueprints for International Relations (Fl.
Worth, Texas: Dominion Press, 1987).

52, Cf. Robert Nisbet, The Present Age: Progress and Anarchy in Modern America (New
York: Harper & Row, 1988).
