Renewed Covenant or Broken Covenant? 405

United States it, like the pyramid, reflects the vague ‘Great Archi-
tect’ deism of American Freemasonry rather than faith in the per-
sonal God of Christianity,”®°

That Brown should appeal to the reverse of the Great Seal, the
all-seeing eye and the pyramid, is significant, though even Brown is
unaware of just how significant. The Congress on July 4 appointed a
committee to recommend designs for a seal of the U.S. The commit-
tee was made up of Thomas Jefferson, John Adams, and Benjamin
Franklin. The obverse (front) of the Great Seal is the eagle. The re-
verse of the Great Seal is the all-seeing eye above a pyramid, a famil-
iar Masonic symbol. There is an oddity here, one which is seldom
mentioned: there is no reverse side of a corporate seal. A seal is used
to produce an impression. It is either a one-piece seal for impressing
wax, or a convex and concave matching. pair for impressing a piece
of paper. (This “reverse seal” was ignored by the government for a
century and a half until Henry A. Wallace, Franklin Roosevelt's
politically radical Secretary of Agriculture*! and resident occult
mystic, persuaded the Secretary of the Treasury to restore it to public
view by placing it on the back of the one dollar bill, the most common
currency unit. This was done in 1935, and remains with us still.)
We have returned symbolically in this century to the original national
halfway covenant — the Declaration and the Articles of Confederation
~that invoked the god of Masonry. Men need symbolic manifesta-
tions of ultimate sovereignty, and the eagle is no longer sufficient in a
judicially secular age of confusion and despair. The eagle, the sym-
bol on the national seal under the Declaration and the Articles, is no
longer the sole national image that pops into Americans’ minds.

89. Harold O. J. Brown, “The Christian America Major Response,” in Ged and
Politics, p, 256,

90. Monroe E. Deutsch, “E Pluribus Unum,” Classical Journal, XVIII (April
1923), p. 387.

91. It was under Waltace’s Department of Agriculture, in the Agricultural Ad-
justment Administration (AAA), that the first major Communist cell in the federal
government was formed in 1933, the Ware yroup. See Allen Weinstein, Perjury: The
Hiss-Chambers Case (New York; Knopf, 1978), ch. 4: “The Ware Group and the New
Deal.” Ware was the son of “Mother Bloor,” one of the leaders of the U.S. Commu-
nist Party: ibid., p. 5.

92. Arthur M, Schlesinger, Jx., The Coming of the New Deal (Boston: Houghton
Mifflin, 1959), vol. 2 of The Age of Roosevelt, pp. 29-34, Wallace was later Roosevelt's
Vice President, 1941-45, He was replaced as V-P. by Harry S, Truman in January of
1945, three months before Roosevelt's death; otherwise, Wallace would have become
President.

 
