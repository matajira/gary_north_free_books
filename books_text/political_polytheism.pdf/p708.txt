684 POLITIGAL POLYTHEISM

ence and disobedience.”3° But what does the Constitution actually
say? Exactly the opposite: “no religious Test shall ever be required as
a Qualification to any Office or public Trust under the United
States.” To put it mildly, this was deliberate deception. Rushdoony is
determined not to face the facts of the U.S. Constitution, and he
does not want his audience to do so, either.

To his own American audiotape audience, Rushdoony insisted:
“The Constitution required an oath of office. To us this doesn’t mean
much. Then it meant that you swore to Almighty God and involved
all the curses and blessings of Deuteronomy 28 and Leviticus 26 for
obedience and disobedience. Nobody knows that anymore.”3’
Nobody knew it then, either, Deuteronomy 28 was about as far from
George Washington’s mind as might be imagined. Rushdoony has
never offered so much as a footnote supporting such a claim with
respect to the U.S, Constitution. The story is mythical. What he has
done is to pretend that the Trinitarian oath-taking that did take place
at the state level had become a Christian oath-taking ceremony at
the federal level. The opposite is the case, and it was the statist ele-
ment of the federal oath which steadily replaced the theistic oaths in
the states.

How, in good conscience, could he announce this to his fol-
lowers? “An oath to the men who wrote the Constitution was a Bibli-
cal fact and a social necessity.” If this was true, then why did they
exclude God from the mandatory oath? They well understood the
importance of oaths.*° They insisted on a required oath as the judi-

36. Rushdoony, The “Atheism” of the Early Church (Blackheath, New South Wales:
Logos Foundation, 1983), p. 77.

37. Rushdoony, question and answer session at the end of his message on Leviti-
cus 8:1-13 (Jan. 30, 1987).

38. Rushdoony, “The United States Constitution,” Journal of Christian Recon-
struction, XII, No. 1 (1988), pp. 28-29.

39, Writes Albert G. Mackey, the Masonic historian: “It is objected that the oath
is attended with a penalty of a serious or capital nature. If this be the case, it does
not appear that the expression of a penalty of any nature whatever can affect the
purport or augment the solemnity of an oath, which is, in fact, an attestation of God
to the truth of a declaration, as a witness and avenger; and hence every oath in-
cludes in itself, and as its very essence, the covenant of God's wrath, the heaviest of
all penalties, as the necessary consequence of its violation.” Albert G. Mackey (ed.),
An Encyclopaedia of Freemasonry and Its Kindred Sciences, 2 vols. (New York: Masonic
History Co., [1873] 1925), II, p. 523. On the illegitimacy of such self-maledictory
oaths except in Church, State, or family, see Gary North, The Sinai Strategy: Economics
and the ‘Ten Commandments (Tyler, Texas: Institute for Christian Economics, 1986), ch,
3.
