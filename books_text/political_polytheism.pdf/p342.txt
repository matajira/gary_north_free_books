318 POLITICAL POLYTHEISM

American history: Jonathan Edwards,? John Witherspoon, and
Virginian Woodrow Wilson. Two other less famous presidents
played important roles in transforming the Presbyterians: Virginian
Samuel Davies, a leader in the Great Awakening,? who succeeded
Edwards briefly until his death, and the Scottish defender of natural
law who brought “Christian” evolutionism to young Presbyterian
gentlemen in the late nineteenth century, James McCosh.* If we
count William Tennent’s “Log College” as the predecessor of the Col-
lege of New Jersey, then we should add his name to the list. Every
Presbyterian clergyman except one who was prominent in the Great
Awakening was a Log College man.>

I begin my discussion of apostate covenantalism where Rushdoony
began his discussion of what he regards as covenantally Christian
America: with Rev. John Witherspoon. He was the teacher of the man
who is often called the Father of the Constitution, James Madison.®
He was a signer of the Declaration of Independence, the only minis-
ter of the gospel to do so.

Witherspoon serves as perhaps the best example in the history of
the Christian Church as a man who defended a halfway covenant
philosophy and subsequently pressed for an apostate national cove-
nant. He was the most prominent clergyman in the colonies during
the Revolutionary War. He was hated by the British. When British
troops captured Rev. John Rosborough, they bayoneted him on the
spot, thinking that they had captured Witherspoon.’ He was there-

2. Aaron Burr was Edwards’ son-in-law; Burr’s father had been president of
Princeton, where Burr graduated. He requested and received permission to be buried
in the cemetery plot of the presidents of Princeton, although for the first twenty
years, the grave went unmarked. Milun Lomask, Aaron Burr: The Conspiracy and
Years of Exile, 1805-1836 (New York: Farrar, Straus, Giroux, 1982), pp. 404-5.

3. It was during a college fund-raising tour in England with Gilbert ‘Tennent in
1755 that Davies presented his civil case for religious toleration of dissenting
churches in Virginia, which Davies won, This subsequently increased the degree of
toleration for colonial dissenters generally. This was probably the most significant
college fund-raising program in American history. Sce the entry for Davies in Dic-
tionary of American Religious Biography, edited by Henry Warren Bowden (Westport,
Connecticut; Greenwood Pres, 197).

4, J. David Hoeveler, Jr., james McGosh and the Scottish Intellectual Tradition: From
Glasgow to Princeton (Princeton, New Jersey: Princeton University Press, 1981). After
he retired, McGosh wrote a biography of Witherspoon (1890).

3. Dictionary, entry under William ‘Tennent, p. 459.

6. For example, Neal Riemer, fames Madison: Creating the American Constitution
(Washington, D.C.: Congressional Quarterly, 1986).

7. James Hastings Nichols, “John Witherspoon on Church and State? Journal of
Presbyterian History, XLVIE (Sept. 1964), pp. 166-67.

  

  
