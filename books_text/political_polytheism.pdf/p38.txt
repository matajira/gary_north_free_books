4 POLITICAL POLYTIIEISM

in the balance, not to mention Western civilization. He was hated
for this, perhaps most of all. His contemporaries wanted peace.

This gave Luther an enormous advantage. He wanted victory,
and he developed his tactics accordingly. Luther was the pioneer of
the pugnacious pamphlet, as well as the vicar of vitriol and the rector
of ridicule. He adopied Augustine's rule: anything that is ridiculous
deserves to be ridiculed. His spiritual heirs have conveniently for-
gotten about the scatological cartoons of the Pope that Luther put in
his pamphlets. Had he restricted his expressions of theological oppo-
sition to the Roman Church by the accepted rules of discourse with-
in the academic community, there would probably have been no
Reformation in his day. He would have been burned at the stake.
Instead, the authorities had to content themselves with burning his
pamphlets.

Luther’s academic peers wanted peace. So do most of his spiri-
tual and institutional heirs today. Well, they won't all be able to get it

..at least not while I’m alive and have a word processor in front of
me. And not while AIDS is spreading, either. C. Everett Koop’s sol-
ution to AIDS is condoms. This will not work. My critics’ solution to
me is public silence and private murmuring. This will not work,
either. There is only one strategy that can possibly shut me up, or at
least remove me as a serious combatant: prove in print, point by
point and verse by verse, that my theology is dead wrong and so is
my analysis of the long-term compromise with humanism; commu-
nicate this in language that literate Christians can understand; and.
then spend as much time and money as I spend in getting this message
to the as yet undecided troops. My prediction: not very likely.

Erasmus lost the polemical battle—though not the academic eti-
quette battle — because he viewed the battlefield of the Reformation
in terms of the scholar’s study. He decided to avoid engaging in pro-
tracted conflict, He refused to face the fact that protracted conflict is
all that the followers of Jesus Christ have ever been promised on
earth, even during the millennium, Christians are told to find inter-
nal peace in the midst of a raging battle. This is no doubt difficult,
but however it is to be accomplished, surrendering the battalion’s
battle flag is not the proper way.

Luther won at least the preliminary phase of the theological and
institutional battle because his opponents could not match his theol-

40. See The City of God, XVIIL-40, on those who oppose the six-day creation,
