 

Abercrombie, James, 420
abolitionism, 655
abortion
Christian women, 627
common cause, 55
Japan, 637
Koop, 199-201
life-and-death issue, 228
no neutrality, xix, 178, 265
pietism, 246
religious views, 601
Schaeffers on, 176, 178
screening (“immigration”), 190
statistics, 106
subsidized, 202-3
Supreme Court, 226, 292, 396
ACLU, 288, 392, 566, 572, 660
agtivism, 21-22, 176, 198, 659, 667
Adair, Douglass, 307, 369
Adam, 40, 135 (see also Eve)
Adams, John
architect, 382-83, 514
Christianity, 329
Declaration, 409
Deist, 406
Father of Constitution, 428
hatred of Calvinism, 406
public morality, 329-30
religion as instrumental, 329-30
secular Constitution, 382
Unitarian, 406
Vitruvius, 382-3
Adams, John Quincy, 697
Adams, Sam, 414, 435
adultery, 209-10

INDEX

agenda, 55
agreement, 126
Ahab, 565, 567
Ahiman Rezon, 425, 467, 472-73, 475
AIDS, 14, 200-1, 203, 205, 567,
577, 628n
Albany Plan of Union, 466
alchemy, 335-39
aliens, 68, 72, 106 (see also
strangers in the gate)
ambiguity, 229-30, 231-32,
236, 240-43, 261
amending process, 101, 224, 293,
364, 395, 411, 444, 560, 568-69,
651, 657
America
atheistic government, 676
Christian nation, 235, 238, 241,
256-57, 277, 284, 296, 564-66
civil religion, 514-15
Enlightenment, 677-81
failure of Christians, xiv
limited liability corporation, 654
national identity, 654
natural law, 680
neutrality, 655
Protestant pluralism, 280
sanctions are gone, 59, 298
secularization of, 367, 655
American Civil Liberties Union
(see ACLU)
American Revalution
army lodges, 471
biblical images, 513
“black regiment,” 526

727
