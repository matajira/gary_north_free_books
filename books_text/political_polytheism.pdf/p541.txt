“We the People”: From Vassal to Suzerain to Serf 57

the Antifederalists: the centralization of power, the weakening of
local juries,” the Civil War, the Fourteenth Amendment, and a
Senate filled with atheists.

Conclusion

The Preamble of the Constitution and the plebiscite of 1788 es-
tablished a new covenantal foundation for the American republic. It
transferred ultimate sovereignty from God to the People as a whole,
and mediatory political sovereignty from the states to the national
government. The question then became: Which branch speaks au-
thoritatively in the name of the new divinity? While the Framers did
not expect the Supreme Court to emerge as the People’s spokesman,
it was inherent in the nature of the Constitutional settlement: 1) the
inescapable doctrine of judicial review; 2) a unitary reviewer (i-e.,
no provision for an appeal to the plural sovereignties of President
and Congress); 3) lifetime tenure for federal judges (continuity of
the spoken word). The lawyers created a civil government made in
their own image, and they transferred penultimate sovereignty to the
‘lawyers’ lawyers,” those sitting permanently on the Supreme Court
until they die or voluntarily resign. Only the voters can overcome the
Court through the amending process, or so it has appeared.

There is no escape from judicial authority. There must always be
a final earthly court of appeal. The Framers did not fully recognize
this. They should have seen that the Constitutional doctrine of judi-
cial review was inevitable. The only way that they could have over-
come this transfer of ultimate sovereignty to the Supreme Court
would have been the creation of some sort of appeals structure
beyond the Court, such as my three-quarter’s vote suggestion. In-
stead, the Constitution lodges theoretical judicial authority in the
People, and final practical authority in the hands of five people (a
five-to-four decision of the Court).

The fact is that there must always be a voice that interprets the will of the
Sovereign agent in history. Today, the amorphous deity “We the People”
is represented in a sovereign way by five people. A Constitutional
amendment can override the Court, as can a new Convention, but
these alterations are costly to organize and infrequent. The Court
not only reigns today; it also rules.

75, Ihid., p. 106.
