Conclusion 653

not presume upon God to maintain His grace irrespective of the cov-
enantal unfaithfulness of those who call themselves by His name.

What is needed is a very simple modification of the U.S. Consti-
tution. First, the Preamble should begin: “We the people of the
United States, as the lawful delegated agents of the Trinitarian
God of the Bible, do ordain and establish. 48 Second, Article
VI, Clause 3, should state “The Senators and Representatives before
mentioned, and the Members of the several State Legislatures, and
all the executive and judicial Officers, both of the United States and
of the several States, shall be bound by Oath or Affirmation, to sup-
port this Constitution; and a Trinitarian réligious Test shall be re-
quired as a Qualification to any Office or public Trust under the
United States.” These minimal steps would mark the overthrow of
the Masonic revolution of 1787-88.

  

National Judgments and False Theology

God always bring judgments that are part and parcel of the spe-
cific sins of the culture. This is the lesson of the Book of Judges.
James Jordan. writes:

Israel had become enslaved to the Canaanite gods; it was therefore
logical and necessary that they also become enslaved to the Canaanite cul-
ture. In effect God said, “So you like the gods of Ammon? Well then, you’re
going to just love being under Ammonite culture! Oh, you don’t like being
in bondage to Ammon? You'd like to have Me as your God once again?
Wonderful, Tl send a judge, who will have My Son as his Captain, and set
you free from Ammon.” Yet, in a few years God would be saying this: “So
you like the gods of Philistia? Well, I gather then that you will be extremely
happy under Philistine culture!” And so it would go.

God’s judgments are never arbitrary. God chastises and curses people
by giving them what they want. Israel wanted Baalism as a philosophy, so
God gave them into the hands of Baalistic civilizations. Since they were
slaves of the gods of these cultures, it was only proper that they should be
slaves of the cultures themselves as well.

Modern Americans have worshipped at the temple of political
neutrality. The progressive disintegration of American civilization is

48. It is not sufficient co call for an amendment that names Jesus Christ as Lord
of the national covenant. There are cults that proclaim Jesus Christ as Lord. They
are anti-Trinitarian, however, and the inclusion of a statement identifying God as a
Trinity is necessary.

49. James B. Jordan, judges: Guds War Against Humanism (Tyler, Texas: Geneva
Ministries, 1985), p. 41.
