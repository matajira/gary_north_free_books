766 POLITICAL POLYTHEISM

South (American), 602
sovereignty
Articles of Confederation, 378
Constitution, 386
Declaration of Independence, 524
democracy, 647
dual, 650
final, 500, 502
Framers’ view, 381, 699
immanent, 402
initiating, 500, 502
judicial, 502-7
legitimate, 507
legislatures, 378
Madison on, 446
man, 85 (see also autonomous
man)
money, 552-33
national, 381-83, 497
Parliament, 377-78
People, 386, 496, 517, 647 (see also
People)
People and Court, 440
Rousseau, 256
Rushdoony on, 697-98
sanctions, 640
transfer of, 458, 517
voice of, 375, 541-42
voters, 568-69
Sovereignty of God
election, 37
denial, 44
Hamilton on money, 552
history, 159
sanctions, 159
surrender of, 4
Soviet Union, 91, 652
Spinoza, Baruch, 576
spokesman, 541-42
Spykrian, Gordon, 16n, 296, 399n
squatter’s rights, 142, 160
St. Andrews Lodge, 474 (see also
Green Dragon Tavern)
Stamp Act, 525n
standards, 126, 177, 183, 191, 212,
230, 575 {see also biblical law)
Standish, Miles, 274

stars, 7In
State
anointing, 527
autonomous, 641-42
bankrupt, 526
Church (see Church and State)
contract theory, 82
divine right, 542
greatest power, 494, 501
immune to gospel?, 621
imitation final sanctions, 642
messianic, 647
negative, 464, 526
negative sanctions, 94-95
neutral?, 539
new god (1788), 528-29
positive sanctions, 645
progressive sanctification, 208
Puritan view, 645
sanctions, 69, 89, 642, 647
sovereign, 494, 496
taxes, 294
unleashed (contractualism), 494
voting, 69
zero, 641-42
state constitutions, 379, 384-85, 450
state oaths (sée test oath; state}
states’ rights, 381, 392, 488
Stephens, Alexander, 312
Stoddard, Solomon, 368
Stoics, x, xxi, 81, 240, 352, 681
stonemasonry, 477
stranger in the gates
apostasy of Israel, 76
Athens, 62
Christiane?, 564
educators, 75-76
mixed population, xiii
oppressor, 75-83
politically excluded, 28, 70-74
protection of, 68, 71-72
sanctuary, 66-74
social cohesion, 98
subyersion by, 70-71
taxation and cults, 97
vietory by, 75, 564
