578 POLITICAL POLYTHEISM

they are certainly perceived today as threats. Added to these grim
possibilities is the much more predictable threat of an international
economic collapse as a result of the vast build-up of international
debt; this in turn could produce domestic political transformations.
Also possible is the spread of terrorism and Marxist revolution.
Drug addiction is spreading like a plague. Changes in the weather as
a result of the use of fussil fuels (the “greenhouse effect”) are in the
newspapers because of international drought. Agricultural output
may be endangered, long term, by weather changes and also by soil
erosion. We are not sure. What Christians should be certain of is
this: God has been plowing up our ethically erosion-prone world since World
War I, and this process is accelerating,

This has created a unique opportunity for Christian revival, but
this time the revival could lead to a broad-based cultural transforma-
tion. In short, revival could produce an international revolution:
family by family, church by church, nation by nation. For a true so-
cial revolution to take place, there must be a transformation of the
Jegal order. This transformation takes several generations, but with-
out it, there has been no revolution, only a coup détat.’ There is today
an international crisis in the Western legal tradition.* This, far more
than the build-up of nuclear weapons or the appearance of AIDS,
testifies to the likelihood of a comprehensive, international revolution
—not necessarily violent, but a revolution nonetheless. The Holy
Spirit could produce such a revolution without firing a shot or launch-
ing a missile. This is my prayer. It should be every Christian’s prayer.

Harold Berman’s point is correct: without a transformation of the legal
system, there is no revolution, This is why I have devoted so much space
to explain the case laws of Exodus.® It is these laws, and their amplifi-
cation in the Book of Deuteronomy, that must serve as the foundation
of any systematically, self-consciously Christian revolution, Once the
myth of neutrality is abandoned —really abandoned, not just verbally
admitted to be a myth—then the inevitable question arises: By what
standard? Christians who have abandoned faith in the myth of neutral-
ity have only one possible answer: “By this standard: biblical law.”!°

 

. Harold J. Berman, Law and Revolution: The Formation of the Western Legal Tradi-
tion (Cambridge, Massachusetts: Harvard University Press, 1983), p. 20.

8. [bid., pp. 33-41.

9. Gary North, Tools of Dominion: The Case Laws of Exodus (Tyler, Texas: Institute
for Christian Economics, 1989)

10. Greg L. Bahnsen, By This Standard: The Authority of God's Law Today (Tyler,
‘Yexas: Institute for Christian Economics, 1983).

 
