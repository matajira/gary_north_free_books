The covenant theology was governed by this insight and by this
conception. It was in the Reformed theology that the covenant theol-
ogy developed, and the greatest contribution of covenant theology
was its covenant soteriology and eschatology.

It would not be, however, in the interests of theological conserva-
tion or theological progress for us to think that the covenant theology
is in all respects definitive and that there is no further need for cor-
rection, modification, and expansion. Theology must always be
undergoing reformation. The human understanding is imperfect.
However architectonic may be the systematic constructions of any
one generation or group of generations, there always remains the
need for correction and reconstruction so that the structure may be
brought into closer approximation to Scripture and the reproduction
be a more faithful transcript or reflection of the heavenly exemplar.
It appears to me that the covenant theology, notwithstanding the
finesse of analysis with which it was worked out and the grandeur of
its articulated systematization, needs recasting. We would not pre-
sume to claim that we shall be so successful in this task that the
reconstruction will displace and supersede the work of the classic
covenant theologians. But with their help we may be able to contrib-
ute a little towards a more biblically articulated and formulated con-
struction of the covenant concept and of its application to our faith,
love, and hope.

John Murray (1953)*

“Murray, The Covenant of Grace (Phillipsburg, New Jersey: Presbyterian &
Reformed, [1953] 1988), pp. 4-5.
