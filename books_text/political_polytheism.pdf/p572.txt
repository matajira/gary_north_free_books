548 POLITICAL POLYTHEISM

a Freemason. But this puts it too mildly. Jonathan Belcher was the
original Freemason in the colonies, having been initiated in London
in 1704,5* He was literally the pioneer. One Masonic historian refers
to him as “the Senior Freemason of America.” After his initiation,
he experienced rapid success as a merchant.*¢ His son became the
Deputy Grand Master of the Provincial Grand Lodge of Massachu-
setts at its founding in 1733.57 In 1741, the brethren of the First Lodge
read a message to Mr. Belcher, who had been succeeded by a new
governor the previous spring. The lodge thanked him for “the many
favours You have always shared (when in Power) to Masonry in
General. . . .”*! The spirit of nondenominationalism at the College
of New Jersey was not going to be overturned by Brother Belcher!
It should be no surprise to learn what President Witherspoon re-
vealed in 1776, in his quest for nondenominational money from
donors in Bermuda, namely, that no discussion of church government
was tolerated at the college. “Every question about forms of church
government is so entirely excluded that . . . if they [the students] know
nothing more of religious controversy than what they learned here,
they have that Science wholly to begin.” Thus, concludes Trinterud,
James Madison did not learn about Presbyterian polity from Withers-
poon. “The theological doctrine of natural law and the political theory
of natural rights provided the meeting place for Presbyterian and citi-
zen rather than the Presbyterian form of Church government, New
England Congregationalists and Virginia Episcopalians stood with
American Presbyterian laymen in this political theory, and with this
common heritage they were able to work together although their
heritages in ecclesiastical polity still separated them widely.”©
Brother Belcher would have been proud.

Whigs Ecclesiastical

Three weeks after Witherspoon delivered his speech, on June 21,
1788, New Hampshire's convention became the ninth state conven-

54. J. Hugo Yatsch, Freemasonry in the Thirteen Colonies (New York: Macoy, 1929),
p. 27.

55. Melvin M. Johnson, Yhe Beginnings of Freemasonry in America (New York:
Doran, 1924), p. 49.

56. His father had been a successful merchant, to, but not on Jonathan’s scale.

57. Tatsch, Freemasonry, p. 30.

58. Cited in Johnson, Beginnings, p. 255.

59. Cited in Trinterud, Forming, p. 256.

60. Ibid., p. 257.
