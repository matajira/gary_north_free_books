Rushdoony on the Constitution 691

But no law-order can survive if it does not defend its core faith by rigorous
sanctions. The law-order of humanism leads only to anarchy. Lacking ab-
solutes, a humanistic law-order tolerates everything which denies absolutes
while warring against Biblical faith. The only law of humanism is ulti-
mately this, that there is no law except self-assertion, It is “Do what thou
wilt.”, . . To tolerate an alien law-order is a very real subsidy of it: it is a
warrant for life to that alien law-order, and a sentence of death against the
established law-order.

 

The Death Warrant

The Framers at the Constitutional Convention issued a death
warrant against Christianity, but for tactical reasons, they and their
spiritual heirs refused for several generations to deliver it to the in-
tended victims. They covered this covenantal death sentence with a
lot of platitudes about the hand of Providence, the need for Morality,
the grand design of the universe, and similar Masonic shibboleths.
The death sentence was officially delivered by the Fourteenth
Amendment. It has been carried out with escalating enthusiasm
since the 1950’s. But Rushdoony dares not admit this chain of cove-
nantal events. He writes as though everything humanistic in Ameri-
can life is the product of a conspiracy of New England’s Unitarians
and the radical Republicans of the Civil War era. To admit the his-
torical truth of 1787-89 would mean that a restoration of so-called
“original American Constitutionalism”’ would change nothing cove-
nantally. The nation would still rest judicially on an apostate covenant.

The Constitution must prevent treason. Every constitution must.
Treason is always a religious issue. The question must be raised: In
terms of the U.S. Constitution, what constitutes treason, Christian-
ity or pluralism (secular humanism)? If you want to see the change
in Rushdoony’s thinking, consider these observations:

[1973:] The question thus is a basic one: what constitutes treason in a
culture? Idolatry, i.e., treason to God, or treason to the state? ®

[1973:] Because for Biblical law the foundation is the one true God, the
central offense is therefore treason to that God by idolatry. Every law-order
has its concept of treason. . . . Basic to the health of a society is the integrity
of its foundation. To allow tampering with its foundation is to allow its total
subversion. Biblical law can no more permit the propagation of idolatry

62. Rushdoony, Institutes, pp. 66, 67.
63. Ibid., p. 68.
