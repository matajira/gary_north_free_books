236 POLITICAL POLYTHEISM

Notice the weasel word, “truly,” It gives him a great deal of lati-
tude to dance the professional historian’s jig. So does the phrase,
“some important ways.” Then he refers to the “ambiguous character
of much of the Puritan cultural achievement and influence.”3? What
is in fact ambiguous is Dr. Marsden’s reconstruction. He admits, to
his credit, that “This thesis in turn is based on a more theoretical ar-
gument questioning whether there are likely to be found any actual
historical examples of truly Christian cultures.”33 Notice where this
is headed. If in theory it is unlikely that there can be any historical ex-
amples of truly Christian cultures—the heart, mind, and soul of
“Marsdenism” and neo-evangelical historiography generally —then
of course the Puritans did not achieve a truly Christian society.

The Puritan Experiment

He admits that they had a “relatively free hand” in building their
society. They represent “an uncommonly ideal ‘laboratory’ in which
to analyze the possibilities and pitfalls of a truly Christian culture,”>+
He also admits that “Puritan conceptions long remained major influ-
ences in America.”5 He cites Church historian Sydney Ahlstrom’s
recognition of the “dominance of Puritanism in the American reli-
gious heritage.”® But, asks Marsden, did they provide “a truly
Christian basis for American culture?” Then he discusses the diffi-
culties in defining Puritan and Christian. This is the historian’s am-
biguities game, a tactic developed early in his training, a skill espe-
cially useful when presenting a “new, improved” interpretation of
something. He lists several signs of a truly Christian culture: peace,
charity to the poor, and morality. “Cultural activities such as learn-
ing, business, or the subduing of nature would be pursued basically
in accord with God's will.”? Notice here the phrase “God’s will.”
Notice the absence of another phrase, “God’s Bible-revealed law.”
This is not a mistake on Dr. Marsden’s part; this, too, is basic to all
of modern neo-evangelical social concern. It refuses categorically to
appeal directly to Old Testament law as the basis for discovering
God’s will, This would be “legalistic.” More to the point, an appeal to

32. Ibid., p. 242.
33. fdem.
34. Idem.
35. Idem.
36. Ibid., p, 243.
37. Ibid., p. 244.
