The Theological Origins of the U.S. Constitution 337

mystical and magical goal of alchemy: the self-transcendence of the al-
chemist. The alchemist, by a manipulation of the elements, seeks to
achieve a leap of being, what today would be called an evolutionary
leap. The familiar legend of the philosopher's stone — the alchemical
means of transforming base metals into gold—neglects the real goal
which this transformation merely symbolizes: the transformation of
the alchemist, and by implication and representation, of humanity.”
“Gold, we repeat, is the symbol of immortality.”8° To dabble in al-
chemy, even for intellectually technical reasons, is to come very close
to the messianic impulse of the deification of man. It is like dabbling
in magic; it has consequences.

One of the consequences was the French Revolution. Margaret
Jacob’s Radical Enlightenment is clear about the spread of pantheistic
versions of Newtonianism into France through the Netherlands and
Freemasonry. With it came a proclivity for the old neoplatonic Ren-
aissance view of man and society. This view is analogous to
alchemy’s view of man, They both begin with the presupposition of
magic and hermeticism: “As above, so below.”#! There is an ontologi-
cal relationship between man and the cosmos, a chain of being.
Molnar put is this way: “. . . it means that there is an absolute al-
though hidden concordance between the lower and the higher
worlds, the key of which lends to the magus incalculable powers.”®?
Thus, by manipulating the cosmos, the initiate can change the
nature of man (e.g., environmental determinism). On the other
hand, by manipulating something near at hand, he can affect some-
thing far away® (e.g., both voodoo and modern scientific chaos
theory).84 One manipulates the external elements in order to change

79, Mircea Eliade, The Forge and the Crucible: The Origins and Structures of Alchemy
(New York: Harper Torchbooks, [1956] 1971). See especially chapter 13: “Alchemy
and Initiation.” For a detailed bibliography on alchemy, see Alan Pritchard, Alchemy.
A bibliography of English-language writings (London: Routledge & Kegan Paul, 1980).

80. Eliade, idd., p. 151.

81. Hermeticism is named after the mythical Hermes ‘[rismegistus, who enun-
ciated this above-below principle. Molnar quite properly makes this the theme of his
discussion of modern subjectivist philosophy and revolutionary politics. Thomas
Molnar, God and the Knowledge of Reulity (New York: Basic Books, 1973),

82. Ibid, p. 82.

83. Ibid., p. 83.

84. In contemporary chaos theory, the so-called butterfly effect teaches that tiny
disturbances at one end of the environment (the butterfly in California) can produce
large-scale effects on the other side of the environment (the storm in New York). See
James Gleik, Chaos: Making a Naw Science (New York: Viking, 1987), ch. 1.
