Appendix E
THE MASSACHUSETTS CONSTITUTION OF 1780

This state constitution was the product of over four years a open
debate, suggestions from towns, and suggestions from John Adams.
Here I reproduce its section on oaths for public officials. It is taken from
The Popular Sources of Political Authority: Documents on the Massachusetts
Constitution of 1780, edited by Oscar and Mary Handlin (Cambridge,
Massachusetts: Belknap Press of Harvard University Press, 1966),
pp. 467-69, The oath was officially Christian, it invoked the name of
the Congress of the United States, and it was supposedly an un-
breakable covenant, all of which became important with respect to
the Constitutional Convention of 1787 and its outcome.

Chapter VI

Oaths and Subscriptions; Incompatibility of and Exclusion from Offices;
Pecuniary Qualifications; Commissions; Writs; Confirmation of Laws;
Habeas Corpus; The Enacting Style; Continuance of Officers; Provision for a
future Revisal of the Constitution, etc.

ART. I.—Any person chosen Governor, Lieutenant-Governor,
Counsellor, Senator, or Representative, and accepting the trust,
shall, before he proceed to execute the duties of his place or office,
make and subscribe the following declaration, viz. —

“I, A.B. do declare, that I believe the christian religion, and have
a firm persuasion of its truth; and that I am seized and possessed, in
my own right, of the property required by the Constitution as one
qualification for the office or place to which I am elected.”

AND the Governor, Lieutenant-Governor, and Counsellors,
shall make and subscribe the said declaration, in the presence of the
two Houses of Assembly; and the Senators and Representatives first
elected under this Constitution, before the President and five of the

719
