Halfway Covenant Historiography 269

What does he mean, “more nominally Christian’? With the ex-
ception of Rhode Island, they were legally, covenantally Christian
governments. They were chartered under the King and Parliament.
Blasphemy was still a major crime in England in the late seventeenth
century. In half the colonies, colony-level civil magistrates had to
take a Trinitarian oath in order to hold office, and this was true up
until the American Revolution and in’ some cases beyond. !39

Virginia

In 1610-11, Gen. Thomas Gates began his colonial laws with a
call to men’s duty to God, to whom men owe “highest and supreme
duty. . . .”!*° He required all captains and officers to haye morning
and evening prayer. The death penalty was to be imposed on anyone
speaking impiously regarding any Person of the Trinity.'41 “No man
shall speak any word or do any act which may tend to the derision or
despite of God’s holy word, upon pain of death; nor shall any man
unworthily demean himself unto any preacher or minister of the
same, but generally hold them in all reverent regard and dutiful en-
treaty; otherwise he the offender shall be openly whipped three times
and ask public forgiveness in the assembly of the congregation three
several Sabbath days.”!*2 Sodomy, rape (including the rape of an In-
dian), and adultery were specified as capital crimes. Sacrilege — theft
of church property —was a capital crime.'*8

Later laws were less theologically rigorous. The laws of 1619
required weekly church attendance, and fines were imposed on
violators.‘4* The church remained extremely important in Vir-
ginia’s history. At the time of the Great Awakening in the mid-
1700's, the issue of church establishment flared up politically, indicat-
ing that Christian issues were still basic to the disputes of colonial
era Virginia. '*5

139. See Part 3.

140, “Articles, Laws, and Orders, Divine, Politic, and Martial for the Colony in
Virginia, 1610-1611,’ in Foundations of Colonial America: A Documentary History, edited by
W. Keith Kavenaugh, 3 vols, (New York: Chelsea House, 1973), HI, p. 1869.

141, Mbid., TIT, p. 1870.

142. Idem

143. Ibid, TH, p. 1871,

144. Ibid., IIL, p. 1888.

145, Rhys Isaac, The Tranyformation of Virginia, 1740-1790 (Williamsburg,
Virginia: Institute of Early American History and Culture, 1982), Pt. If.
