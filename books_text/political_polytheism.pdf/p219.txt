Halfway Covenant Social Criticism 195

same issue of the Journal of Christian Reconstruction in which Chilton’s
essay had appeared.

Flinn: Rutherford suggests that there are levels of resistance in which a
private person may engage. Firstly, he must defend himself by supplications
and apologies; secondly, he must seek to flee if at all possible; and, thirdly,
he may use violence te defend himself. One should not employ violence if
he may save himself by flight; so one should not employ flight if he can save
and defend himself by supplications and the employment af constitutional
means of redress. Rutherford illustrates this pattern of resistance from the
hfe of David.”

On the other hand, when the offense is against a corporale group such as a
duly constituted state or town or local body, or such as a church, then flight
is often an impractical and unrealistic means of resistance.”

Schaeffer: In such an instance, for the private person, the individual,
Rutherford suggested that there are three appropriate levels of resistance:
First, he must defend himself by protest (in contemporary society this would
most often be by legal action); second, he must flee if at all possible; and,
third, he may use force, if necessary, to defend himself. One should not em-
ploy force if he may save himself by flight; nor should one employ flight if
he can save himself and defend himself by protest and the employment of
constitutional means of redress, Rutherford illustrated this pattern of resis-
tance from the life of David as it is recorded in the Old Testament.®?

On the other hand, when the state commits illegitimate acts against 4
corporate body—~ such as a duly constituted state or local body, or even a church
—then flight is often an impractical and unrealistic means of resistance.#!

Even some of the italics are the same! Chilton complained about
these clear-cut cases of plagiarism in a letter to Schaeffer, and he re-
ceived a reply from a subordinate pleading that Schaeffer had been
given the material from a researcher without any source notes at-
tached, and that the lack of acknowledgment was not really Schaeffer's
fault. If this was the case, it is pathetic. If thts was not the case, then
it is also pathetic.

In the eighth printing of Christian Manifesto, dated 1982, Schaeffer
acknowledged in a pair of footnotes his debt to the two articles in

78. Richard Flinn, “Samuel Rutherford and Puritan Political Theory,” Journal of
Christian Reconstruction, op. cit., pp. 68-69.

79. Ibid., p. 69.

80. Complete Works, V, p. 475.

81. Ibid., V, pp. 475-76.
