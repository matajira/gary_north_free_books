228 An Introduction to Christian Economics

through his power to Jabor.? LeFevre argues that a man’s original
claim extends to as much property as he wants; boundary disputes are
to be settled by a system of voluntary arbitration.’ Each sees the
other’s position as inoperative and/or irrational, Both men, how-
ever, would reject the conservative’s belief in the legitimacy of State
power to settle such arguments by force, if force is necessary.

The conservative argues that the market is a mechanismt which can
provide a zone of human freedom. Obviously, some conservatives
favor tariffs, or “emergency” price controls, but I do not wish to deal
with these proposals, since they tend to be special interest oriented
in too many cases, rather than theoretically serious proposals. In the
case of pure theory, the conservative is never willing to give the
market full.autonomy. There is a reason for this. The conservative
is perfectly willing to admit that the unhampered market is the most
efficient mechanism for the distribution and production of scarce
economic resources, That is why he refuses to give it full autonomy.
There are some things he does not wish to see accomplished in an
efficient manner, The following are obvious examples (obvious, that
is, to conservatives): the production and distribution of heroin,
especially into the hands of minors; the open, unchecked, and
thoroughly voluntary exchange of sexual favors for money; the un-
restricted sale of pornography, especially to minors; the sale of mili-
tary secrets to the highest bidder; the right to build a noisy factory
in the midst of a residential district without compensation to local
owners who refuse to sell their property, A conservative would also
support the enforcement of quarantine procedures on members of
the community who carry contagious diseases that pose a threat to the
very operation of the community, especially in congested urban areas.
Without all these openly coercive powers, the State could not protect
society from forces that would destroy the very fabric of society.
The conservative believes that men wil! live under any system except
anarchy, so it is wiser in the long run to permit the State to exercise
some power. If this is not done, men may choose to live under the
tule of totalitarian power. If society does not provide coercive insti-
tutional atrangements that preserve pockets of limited intolerance, it
cannot defend itself from forces that would lead to the intolerance of
totalitarian regimes.®

6. Murray N, Rothbard, Man, Economy and State (Princeton: Van Nos-
trand, 1962}, I, 78-79; of. Rothbard, “The Anatomy of the State,” Rampart
Journal, T (Summer, 1965), p. 3.

7. Robert LeFevre, Rampart Journal, 1 (Summer, 1965), pp. 51 ff, 76-77.

8. Benjamin E. Lippencott, whose. interventionist economic perspective is
opposed to both traditional conservatism and free market anarchism, has never-
theless provided a good survey of the problems posed to a free society by
