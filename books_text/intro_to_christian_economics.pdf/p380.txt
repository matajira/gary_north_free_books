368 An Introduction to Christian Economics

of benefiting from the open hand of God only to forget the sovereign
demands of the giver; destruction will be the result (Deut. 8:11 ff).
But the distinction between investment and usury stands as a reminder
against the fusing of charity into the realm of the calling. One may not
make a living through loans to needy brothers; such a living is an abomi-
nation in the sight of God. It is not the case, as one advocate of a
totally laissez-faire free market has attempted to argue, that the best
form of charity is a profit-making investment in capital which will
create jobs! To accept that premise the Christian would have to blur
the God-given distinctions between business and voluntary charity.
Business involves an economic return (or at least a potential for mak~
ing a profit) to the investor; charity involves the transfer of scarce
economic resources to another, with no thought of return (Matt, 10:8;
Luke 6:35).

A man can hardly call himself a faithful steward if he seals off charity
from business in an absolute manner. Businesses are supposed to earn
profits if they are to be successful, as several parables of Jesus indicate.
However, ruthless competition that is utterly devoid of mercy is also con-
demmed in the parables, But the fact that a particular young ruler was told
to sell all of his goods and give everything to the poor does not stand as
the requirement for every steward. Nor does the example of the church
at Jerusalem in Acts. 4:32 prevail as the mode! for all churches. A
man must be careful not to drown out the revelation of God in His
word, listening only to the parables of profit or only to the examples of
total poverty. He is responsible before God to respond to the leading of
God’s Spirit at different times and along each turn in life’s path, We
are warned ta grow spiritually by means of earthly parables of
economic stewardship. The fact that God may demand a man to give
up all that he has does not imply that God is sanctioning the moral
validity of continual economic. losses. What God is saying is that one
must not be morally ruthless in business, nor morally wasteful in charity,
“Share the wealth” is a Biblical principle, and the normal means of
this sharing is the tithe. The general principle is not “destroy all
wealth” through universal, indiscriminate giving. In short, business
is not charity, though it may be and should be merciful. Charity should
be carefully administered in a “business-like” fashion—with honest
accounting, budgeting, etc.—but it is not a business, ie., not a profit-
making economic endeavor. They are separate, sovereign realms. Their
differences must be respected.

One important difference is in the yery bureaucratic structure pro-
duced by each form of stewardship. Professor Mises has distinguished

 

11, FA. Harper, “The Greatest Economic Charity,” in Mary Sennholz, ed.,
On Freedom and Free Enterprise (Princeton: Van Nostrand, 1956), pp. 94-107.
