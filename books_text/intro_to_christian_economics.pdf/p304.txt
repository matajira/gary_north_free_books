292 An Introduction to Christian Economics

phrase, “shortage of teachers,” is to have any meaning at all, it
must be qualified by the phrase, “at a particular wage level.” There
is no question about the fact that, at present high wage levels, there
is nothing resembling an undersupply of teachers. There is no ques-
tion that there is an imbalance of supply and demand at prescnt wage
levels,

Educators need to ask themselves two crucial questions. First,
why are wages so inflexible downward? Second, why were those
whose task it is to forecast the needs in education so shortsighted?
How did it happen, for example, that in 1963 the estimated need for
new teachers at the college level in history was set at 390 for 1969-70,
whereas the actual need turned out to be 500, and the actual supply
was 8812° Why did so few graduate advisors take seriously the
estimates presented by Clark Kerr, then president of the University
of California (1966), that only two-thirds of the 1971 Ph.D.’s could
be employed in the colleges?*?

Minimum Wage Floors

About 75 percent of those attending institutions of higher learning
are in tax-supported public schools. By their very financing structure,
these institutions are notoriously unresponsive to market conditions
of supply and demand. For many decades, legislatures have met the
basic budget demands of the colleges in the United States,.and this
has tended to insulate the schools and scholars from external eco-
nomic realities. They are not paid to forecast market conditions in
the future, and they do not concern themselves with such matters, at
least not at the graduate advisory level. The private schools, sup-
ported by foundations and government research grants, are frequently
as lax as the public schools, They are, in every sense of the word,
guilds.

Historically, guilds have resisted price and wage competition. They
speak of themselves as “quality-oriented,” which implies an elitist
perspective, since it is price competition which has always character-
ized production for a mass market.44 Educational institutions have
been caught in a dilemma: they are supposed to maintain quality
without compromise, yet supply the needs of mass education, Schools
are to be simultaneously democratic (supported by tax funds) and

3. Chronicle (June 8, 1970), p. 7.

10. Kerr's estimate was revealed at a meeting of California Club, the student
advisory body in the University of California. He was simply reporting the
data. Allan Cartter, chancellor of New York University, had produced the
figures as early as 1964, but few scholars believed him. Things are getting
worse, he says now: Science, 172 (April 9, 1971), pp. 132-140.

ai), Max Weber, General Economic History iNew York: Collier, [1920]
1961), p.
