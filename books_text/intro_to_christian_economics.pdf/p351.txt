Ownership: Free but Not Cheap 339

fiat, where only a right to. bargain in an open market had existed
previously.

Trade union members are not alone in this confusion, unfortu-
nately. Many, many businessmen involve themselves in precisely
the same error. They use the interference of State violence to keep
outsiders away from the bargaining table, A three-way structure
should exist: the consumer, the American producer, and the foreign.
producer. Instead, the American businessman seeks to make the
structure a two-way arrangement: the consumer and only the Ameri-
can producer, Like the labor union member, he secks to transform
aright to bid in the market into an exclusive title of entry into the
market, The usual means for this kind of operation is the tariff or
the import quota. In principle it is identical to the activity of the
State-supported trade union. Ironically, many businessmen who de-
rive great personal satisfaction from castigating the “immoral” trade
unions involve themselves in the same “immorality.” The game is the
game: State “protection” from outside interference—the exclusive-
ness of a legal title to private property. Instead of a legal title to
dispose of their assets and skills as they see fit, in open competition,
subject to the imposition of the burdens of the responsibilities of
ownership, businessmen want title to an exclusive right to dispose of
their assets, apart from competition, apart from the full burdens
(costs) of responsible ownership. Only the intervention of the State
can grant such an escape from responsibility, so they call for the
intervention of the State. Men simply like to enjoy the fruits of
ownership apart from the responsibilities of ownership. They give
up some of their freedom (or their neighbor’s freedom) in order to
escape from responsibility, They call for the creation of legal titles
where none could exist on a free market.

Conclusion

On the one hand, the owner of an exclusive titlek—a property
Tight—cannot escape the costs of ownership and the concomitant
obligation to act as a steward of his goods for the public’s benefit.
He cannot escape so Jong as political intervention into the market
does not occur. The fruits of ownership are not separated from the
burdens of ownership. On the other hand, those who seek to make
a bargain cannot, apart from State coercion or private violence,
transform the right to dispose of one’s own property (talents) into
an exclusive title to dispose of that property on a specific market
apart from entry by other property owners who wish to bargain with
their property. Titles of ownership refer to the control of property
and skills by the owner; they do not refer to reciprocal relationships
