 

The Intelligent Woman’s Guide to Inflation 21

precisely because in these cases it is a scarce commodity that would
be in heavy demand.

Certain commodities have functioned historically as money better
than others because they possess these four qualities: durability,
transportability, divisibility, and scarcity. Gold is obviously one of
these. It is very scarce, absolutely impervious to rust or decay, and
so divisible that in its pure state it can be cut with a knife. Silver is
another such commodity, although being in greater supply, its value
yas generally been less than gold. In some communities, cattle or
other livestock may serve as a means of exchange. There have even
been cases in which wonien have served as money (the big defect
here is clearly the divisibility factor: half a woman is worse than
none at all),

Yn ancient times, governments would stamp gold or silver coins
with the official seal, testifying to the honesty of the coinage. This
worked very well so long as the governments remained honest.
Greed, however, is not a monopoly of those private citizens referred
to as counterfeiters; governments can play the same game for exactly
the same reasons: people like to spend more than they earn, The
collapse of the Roman Empire was intimately linked with the practice
of the emperors of adding cheap metals into the molten gold or silver,
yet calling the resulting coins pure. This enabled the Roman state to
expand its expenditures without increasing the visible rate of taxation.
But as more and more of these phony coins were turned out by the
mint, the value of any individual coin began to fall, As the supply of
the coins went up, the purchasing power of any given coin dropped
lower. After a while, no one wanted the coins any more, The eco-
nomic system actually reverted to barter in some areas after the sec-
ond century A.D., as people traded corn for tools, bread for labor, etc.

During the later Middle Ages (¢. 1200), a new development took
place. The specific practices varied from place to place and from
one era to the next, but the general pattern is easy enough to explain.
Certain individuals within the local community became known as
men who would store precious metals, In many cases, these were
goldsmiths or metalworkers, Individuals would come to these estab-
Ushments in order to deposit their coins for safekeeping; the gold-
smith in return would present the owner with an JOU of some kind.
The IOU’s issued by the firm could be used as money, just as the coins
had been used earlier. The paper IOU’s (which later developed inta
checking accounts) were therefore money substitutes at first; they
were valuable only because the coins they represented had value.

Obviously, only men of means would use these services. The
prestige of these local men of affairs in time was transferred to their
