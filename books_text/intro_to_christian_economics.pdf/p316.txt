304 An Introduction to Christian Economics.

should abandon it. For every minute that the project is allowed to
function it is taking money out of the business. In other words, it
is using up scarce resources when those resources might better be
employed to satisfy some other consumer demand (or be used by
a more efficient firm to satisfy a given demand more effectively).

A rather far-fetched analogy might be used here to clarify the
meaning of the sunk costs concept. Imagine a man who is suspended
from a large helium balloon by a rope. How he got there is
irrelevant for our example. It seemed like a good idea at the time.
He is. now some fourteen feet above the ground. Naturally, he does
not want to let go at this point. But the balloon carries him higher,
say, to twenty feet. He is now in a worse position than before. The
issue which confronts him is simple: shall he let go of the rope
now or later? His decision will be promoted by what he thinks the
situation will be in the future: if the balloon is likely to climb higher,
he should let go; if it will soon be slowly descending, he should hang
on. This much, however, is certain: he failed to drop when he was
only fourteen fect off the ground. Perhaps he should have let go
then; possibly he now wishes he had done so, But the fact remains
that he did not let go then, and his decision cannot now be based
upon any consideration of a fourteen-foot-drop-five-minutes-ago uni-
verse. It is the future as compared with the present, not the past,
which must determine any rational decision, The past is gone, for
better or worse.

Unused Capacity

Along these same lines, we are frequently confronted with the
familiar socialist argument that capitalism creates unemployment
and permits idle resources. “Look at the deserted steel mills, Under
socialism, the government sees to it that all the capacity of the econ-
omy is fully utilized.” The answer to this line of reasoning involves
the concept of sunk costs.

Take the steel mill example. Many mills were built years ago.
They were built under an earlier system of technology: the plants
may have cost more to construct than today (not in-dollars, of course,
but in comparison to the cost of living at that time); the plants were
designed for processes of steel production now outdated. They were
built under a certain set of assumptions about the state of the econ-
omy: the demand for steel, the nature of the competition, the al-
ternative metals that could be substituted for steel, the costs of raw
materials and labor, and so on. Some or all of those assumptions
have proven erroneous with the passing of time. The plants began
to produce losses because the entrepreneurs, being human, were
