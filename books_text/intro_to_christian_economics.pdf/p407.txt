Bibliography 395

of mass production within the framework of a civilization based on
the culture of the lowest common denominator. 4 Humane Econ-
omy (1960) is now published by the Intercollegiate Studies Institute,
Bryn Mawr, Pennsylvania, in an inexpensive paperback. Economics
of the Free Society (1963), which would make a good introductory
textbook, and Against the Tide (1969), a collection of his essays,
are both published by Regnery. Hodge Co., London, publishes
Civitas Humana (1948), The Social Crisis of Our Time (1950),
and International Economic Disintegration (1942). Welfare, Free-
dom, and Inflation (1964), a short book, is available from the Uni-
versity of Alabama Press.

Philip H. Wicksteed’s The Conunon Sense of Political Economy
(1933) is a two-volume set published by Routledge and Kegan Paul,
London, Wicksteed, says Mises, was the last great English economist.
Routledge also publishes Knut Wicksell’s two-volume Lectures on
Political Economy (1935). George Allen and Unwin, London, pub-
lishes Value, Capital and Rent (1954) and Selected Papers on Eco-
nomic Theory (1958). Wicksell was quite good on monetary theory,
and volume two of the Lectures is basic reading.

Eugen von Boehm-Bawerk taught both Mises and Joseph Schum-
peter, perhaps the two greatest economists of this century. His
monumental Capital and Interest, a three-volume set, is available
from the Libertarian Press, South Holland, Illinois. The second
volume, The Positive Theory of Capital, is the crucial one. No one
calling himself an economist should avoid reading this book. Boehm-
Bawerk was one of the clearest, most careful, most logical econo-
mists ever to have written——J think the greatest economist of all
time. He avoids no controversy, and his footnotes reveal the extent
of his knowledge. Libertarian Press alsa publishes The Shorter
Classics of Boehm-Bawerk, (Or, if you prefer, BOhm-Bawerk.)

Frank H. Knight, the most influential teacher-economist in an
American classroom, has contributed several important books, among
them Risk, Uncertainty and Profit (Harper Torchbook, [1921])
and The Economic Organization (Harper Torchbook, [1933]). The
latter could be used in an introductory economics classroom.

Henry Hazlitt, a man who never graduated from college, and who
therefore can write well and think clearly, has written a true classic,
Economics in One Lesson (Macfadden, [1946]}. He is the author
of Man vs. the Welfare State (Arlington House, 1969) and The Con-
quest of Poverty (Arlington House, 1973). He has compiled an
amotated bibliography of libertarian and conservative books, The
Free Man's Library (Van Nostrand, 1956). His book refuting John
Maynard Keynes, The Failure of the “New Economics” (Van Nos-
