The Intelligent Woman’s Guide to Inflation 27

The only alternative, in the short run, is to establish what econo-
mists call floating exchange rates. This sounds very technical, but
it only means that the free market would decide how much a dollar
js worth in relation to a French franc or an English pound. At present,
the exchange rates (comparative values) of the currencies are fixed
by law within very rigid limits. The governments and their central
banks make the decision, and it is difficult to change these prices.
This would mean that prices of foreign goods might fluctuate a lot
more, as one country deflated its currency or inflated it, but there
would be no more “dollat shortages” or “franc gluts’” on the markets.
You could get all the foreign currencies you could want if you were
willing to pay the going price. Many economists, notably the Uni-
versity of Chicago’s Milton Friedman, have advocated such a plan,
but governments, until quite recently, have not wanted to adopt such
a measure. It would take power out of their hands, and it would
also make inflationary policies much more obvious to the public, as
inter-curtency prices would fluctuate rapidly ‘in response to such
policies. But fear of an international monetary crisis has made
flexible exchange rates appear somewhat more desirable than be-
fore. Flexibility is often preferable to intermittent, but violent, crises.

Flexible exchange rates will not solve our long-run problem. Jt
the gold continues to drain out of our Treasury, we will eventually
tun out of gald. It cannot be mined rapidiy enough to replace what
is being purchased abroad. Yet if we absolutely refuse to pay out
the gold, the value of the dollar will be undermined abroad, Euro-
peans will assume (perhaps correctly) that we are not going to stop
the inflation, and that the dollar will drop lower and lower in. ex-
change valuc as the number of them in circulation goes up. This
could cause a panic in the world money markets, throwing world trade
out of kilter, Economists and statesmen in all nations of the free
world fear just this possibility. It would be too dangerous for us
to risk a general cessation of all gold sales. Even if we did it through
some legalistic trick, such as demanding that long forgotten war debts
be paid, the result would be the same, Insolvency by any other name
is still insolvency.

This, in short, is the nature of the problem facing Mr. Nixon and
his advisers. We can print more money (or create it through the
national banking system), inflate further, lower the value of the
dollar, encourage a gold rush on our reserves, and gain the wrath
of foreign opinion, This policy cannot go on forever without serious
international repercussions, both political and economic. On the
other hand, we can raise interest rates, raise taxes, slow down the
growth of our domestic economy, balance our budgets (national,
