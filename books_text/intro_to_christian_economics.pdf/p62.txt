50 An Introduction to Christian Economics

The international gold standard, like the free market's rate of interest,
is an equilibrating device, What it is supposed to equilibrate is not
gross world trade but net trade balances. Boarman’s words throw
considerable light on the perpetual discussion conccrning the in-
crease of “world monetary liquidity”:

A country will experience a net movement of its reserves, in or
cut, only where its exports of goods and services and imports of
capital are insufficient to offset its imports of goods and services
and exports of capital. Equilibrium im the balance of payments
is attained not by increasing the quantity of mythical “world
money” but by establishing conditions in which autonomous
movements of capital will offset the net results, positive and
negative, of the balance of trade.

Some trade imbalances are temporarily inevitable. Natural or
social disasters take place, and these may reduce a nation’s produc-
tivity for a period of time, The nation’s “savings”—its gold stack—
can then be used to purchase goods and services abroad, Specifically,
it will purchase with gold all those goods and services needed above
those available in trade for current exports. If a nation plans to
fight a long war, or if it expects domestic rioting, then, of course, it
should have a larger gold stock than a nation which expects peaccful
conditions. If a nation plans to print up millions and even billions
of IOU slips in order to purchase foreign goods, it had better have a
large gold stock to redeem the slips. But that is merely another kind
of trade imbalance, and is covered by Boarman’s exposition.

A nation which relies on its free market mechanism to balance
supply and demand, imports and exports, production and consump-
tion, will not need a large gold stock to encourage trade. Gold's
function is to act as a restraint on goverments’ spending more than
they take in. If a government takes in revenues from the citizenry,
and exports the paper bills or fully backed credit ta pay for some
foreign good, then there should be no necessity to deplete its semi-
permanent gold reserves, They will sit idle—idle in the sense of
physical movement, but not idle in the sense of being economically
irrelevant.

The fact that the gold does not move is no more (and no less) sig-
nificant than the fact that the guards who are protecting the gold
can sit quietly on the job if the storage system is really efficient.
Gold guards us from that old messianic dream of getting something
for nothing; that is also the function of the guards who protect the
gold, The guard who is not very important in a “thief-proof” building
is also a kind of “equilibrating device”; he is there in case the over-all
system should experience a temporary failure.
