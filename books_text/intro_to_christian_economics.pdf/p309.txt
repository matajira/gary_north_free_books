Subsidizing a Crisis: The Teacher Glut 297

of Ph.D.’s: planning was not made in terms of an unhampered market
but rather in terms of a government-subsidized market. Demand
was cut off sharply by falling school budgets, but candidates for the
Ph.D. degree are not rapidly responsive to this contraction: the other
man may not be able to find a job, but each candidate believes that
he will finish his dissertation and get the available position, A market
geared to the dream of continual expansion has been cut short, and
few persons within the structure are economically oriented enough
to respond as rapidly as free market participants are forced to do.
Like the civilized Eskimos who have forgotten how to build an igloo,
those supplying Ph.D.’s have forgotten the hard realities of a market
characterized by uncertainty. The result has been the teacher glut.

This market, like all markets, will eventually respond to the con-
ditions of supply and demand. Departments will cut back on
enrollments, especially as budgets are trimmed during a time of
inflation. Fellowships will shrink in number, Federal grants to the
scientists will not increase exponentially any longer. In time, teaching
loads will be increased in many universities; wage inflexibility down-
ward will be compensated for through these increased teaching
responsibilities. But it is unlikely that these changes will come
overnight. It is likely that the glut will continue for some time. New
graduates will find it very difficult to break into their first jobs; pro-
fessors’ mobility will drop, the inevitable result of wage inflexibility.
One rigidity creates others, Inflation will continue to eat away at
teachers’ salaries, thus bringing real wages into line with the con-
ditions of supply and demand, and the oversupply of available talent
will thwart attempts to unionize the profession—attempts which are
on the increase now, as the Ph.D., in and of itself, no longer functions
as an effective barrier to entry into the guild.

What we are witnessing is a major transformation of the function
of the Ph.D. degree itself. Once a prestige indicator and a monopolis-
tic grant to the holder, today it is faltering in both capacities. In the
jong run, this development may be for the best, The mystique of
the Ph,D. has for too long been unchallenged. It has degenerated into
little more than an official certification of intellectual drudgery. As
E. Alden Dunham of the Carnegie Corporation of New York has
written:

Every ill besetting our colleges and universities is related in one
way or another to the Ph.D, degree—student alienation, irrele-
vant curricula, uninspired teaching, ironclad adherence to what
may be outmoded traditions, absentee professors, extravagantly
high costs of research and graduate education. . . . [It is] in-
appropriate for most college teaching jobs in this country, es-
pecially at the lower division level, Yet it remains the only
