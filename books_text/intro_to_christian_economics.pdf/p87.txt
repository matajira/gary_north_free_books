Chapter VIII
JHE THEOLOGY OF THE EXPONENTIAL CURVE

{This is a peculiar article, apparently. I have had more favor~
able comments about it than on any other piece, apart from
the Women’s Lib essay. Yet I have also had the greatest
number of people remark that they just could not understand
it. Personally, I think it is my best piece so far, for it explores
some crucial issues. Is there meaning in life? Social science
cannoi tell us. Is there progress in human affairs? Social
science and historians are no longer very sure. Is this a finite
universe? Everyone says sa, but economists produce “growth
models” as if resources were infinite. Questions like these
bother few contemporary Christians, since moderna Christi-
anity is essentially pietistic, retreatist, convinced that there is
no real social progress, convinced that history has very litile
meaning, convinced that Jesus is coming soon. So we find that
Christianity, which might inform social science of meaning in
life, divorces itself from social scientists and their wark alto-
gether, except to baptize an occasional program or two (such
as federal aid to Christian colleges). The blind lead the blind
into the ditch,

Christians must realize that history has meaning, and there is
real progress in lije; the earth is being subdued, although
erratically; economic growth in the aggregate is possible over
time, but not without limits; man is finite, but individual men
have the capacity for personal development; freedom is worth
defending. Basically, God is God, and not the State.)

Growth for the sake of growth is the ideology of the cancer cell.
Edward Abbey

Carl Becker, the late Cornell professor, was once regarded as the
“dean of America’s historians.” His most famous work, The Heaven-
ly City of the Eighteenth-Century Philosophers (1932), was a literate
defense of the idea that the “rationalism” of the Enlightenment was
really a religious faith modeled after the thirteenth century’s theologi-
cal concerns. Enlightenment rationalism is best understood, Becker
argued, as secularized. theology; its presuppositions were as unprov-
able as those of the scholastic philosophers. Both systems, in the

15
