34 An Introduction to Christian Economics

42 of his little book, What You Should Know About Inflation. But
this should not blind us to the original causes: the treasury’s counter-
feiting and the goverumentally protected fractional reserve banking
system.

What, then, are the effects of this inflation on the economic sys-
tem? It is my hope that the answer to this question will be grasped
more quickly through the use of the analogy of the dope addict. The
nation which goes on an inflation kick, such as the one the United
States has been on for well over a century, must suffer all the at-
tending characteristics which inescapably accompany such a kick.
Tn at least six ways, the parallels between the addicted person and
the addicted economy are strikingly close.

1. The “Sunk”? Enters at a Given Point

This is an extremely important point to understand. The new
money does not appear simultaneously and in equal amounts, through
some miraculous decree, in all men’s pockets, any more than equal
molecules of the drug appear simultaneously in every cell of the
addict's body. Each individual’s bank account is not increased by
$5 more than it was yesterday. Certain individuals and firms, those
closest to the State’s treasury or the banks’ vaults, receive the new
money before others da, either in payment for services rendered or
in money loaned to them. Inflation enters the economy at a point
or points and spreads out; the drug enters an addict’s vein, and this
foreign matter is carried through his system. In both cases, the “junk”
enters at a point and takes time to spread.

There are several differences, though, which cannot be ignored.
The spread of inflation is far more uneven than is the spread of the
drug. The first individuals’ incomes are immediately swelled, and
they find themselves able to purchase goods ‘at yesterday’s less in-
flated prices. They can therefore buy more than those who have
not yet reécived quantities of the new, unbacked currency, and this
latter group is no longer able to compete so well as the possessors
of the counterfeits. Since yesterday’s prices were designed by the
sellers to enable them to self the entire stock of each commodity at
the maximum profit, the firms or individuals with the new money will
either help deplete the stock of goods first, leaving warehouses empty
for their competitors who desire to purchase goods at the given price,
or the new money owners will be in a favorable position to bid up
the prices so that the competitors will have to bow out. The first
group gains, undoubtedly, but only at the expense of the second
group, the group which can no longer compete successfully through
no fault of its own. The latter group bears the costs, costs which are
