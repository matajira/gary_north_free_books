The Mythology of Spaceship Earth 273

It would involve turning over the task of ordering literally quintil-
tions of economic relationships to a centralized elite with necessarily
limited knowledge. The results can be predicted: irrational de-
cisions, petty bureaucratic coercion, and a loss of political freedom,

Governments can provide certain services that, by their very na-
ture, men do not want to see offered to the highest bidder, as on
a free market. Justice is not to be purchased for the profit of the
judges involved. Governments are seldom efficient in solving com-
plex, interpersonal problems that require a careful balancing of sup-
plies and demands (for they are plural until registered, specifically,
on a market, by a given supplier and a specific purchaser); when
personal. preferences of many individuals involving varied and even
conflicting goals are the issue, governments are not particularly
successful agents for getting things settled. The fine shadings are
lost in the aggregate decisions,

Therefore, to take a leap of faith from some particular instance
of a “successful” government project—success defined as the op-
erationally satisfactory completion of a certain unquestioned goal—
to the realm of economic planning involves a faith far greater than
anything imagined by the medieval scholastics. Yet Dr. Irving
Bengelsdorf, a staff writer with the Los Angeles Times, thinks that
“there may be hope” along this line of thinking, in spite of the diffi-
culties inherent in any computerized quantification of qualitative
personal preferences, He states the problem well; he cannot show
how his answer is linked operationally with the problem he states:

In contrast to the novel and uncluttered venture of getting to the
moon, fan] uninhabited, non-social, non-political moon, the
problems of society are exceedingly complex to solve because
any solution demands that people have to change their daily
ways of life, their interactions with other people. This is diffi-
cult to do, For, from birth, people already come overlaid with
traditional prejudices, encrusted with hoary cultures, and swad-
dled in ancient customs. And these are hard to change.

But, there may be hope. Both the Apollo 11 flight and the Man-
hattan Project of World War II show that once a clear goal has
been set, a vast, complex project involving large numbers of
people with different training and skifls working together can
achieve a solution.”

Between the first paragraph and the second lies a social revolution.
Also present in the gap is the unstated assumption that we can
reduce the complexities of society to “a clear goal,” which is pre-
cisely the problem governments have not learned to solve. I am

6. Ibid. p. 193.
7. Irving 8. Bengeisdorf, Los Angeles Times, July 24, 1969.
