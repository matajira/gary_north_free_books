354 An Introduction to Christian Economics

that the basic implications of Troost’s essay are ultimately anti-
nomian, and for this reason it deserves an extended analysis.

The problem which faces the Christian scholar in the area of
social philosophy is a very great one: he must make an attempt to
outline policies for social reconstruction that are in accord with the
Biblical framework, and at the same time he must make use of a
vast quantity of scholarship which has been produced by non-
Christian thinkers. In other words, he must acknowledge that
common grace has enlightened the unregenerate scholar to the
extent that some of his endeavors may be useful to the Christian,
but at the same time the Christian must sift and choose from this
scholarship in the light of Reformed, Biblical standards. Clearly,
it is not a simple task, and some errors are bound to creep into the
work of even the most careful Reformed thinker, Yet part of the
heritage of the Reformation is the rejection of perfectionism, and
the fact that some errors are inevitable does not relieve us of the
task of working out the implications of our Christian position.

The Bible, in short, is absolutely fundamental in this work of
social criticism. Without it, the Christian is left without a basic
frame of reference by which he can evaluate the various proposals
for social change. Bearing this in mind, the reader may be able to
understand my hostile reaction to Troost’s starting point: “As we
saw in section 12, the Bible does not provide us with data, points
of departure or premises from which to draw logical conclusions
televant to modern society’s socio-economic problems, including
property relations” (p. 32). The question immediately arises: By
what standard are we to evaluate the validity of any particular
political or social proposal? If, as Christians, we cannot approach
the special revelation presented in the Bible in the hope of finding our
standards for social action, then where are we to go? It is Troost’s
position (and the position of many of bis fellow Calvinist scholars)
that the Bible gives us no data, no concrete recommendations, by
which we can judge political programs; the task of ushering in the
Kingdom of God is apparently to be accomplished without the
guidelines of special, concrete revelation.

Nevertheless, Troost can assert that “The message of the Bible
reveals something to us!” What is it which the Bible reveals? It
gives us the story of the coming kingdom, of “the re-establishment
of all things, to the total reconciliation, liberation and renewal of life
by the person and work of Jesus Christ through his cross and.
resutrection.” Even more than this, “The cross and the resurrection
promise to our practice of property relations a complete liberation
