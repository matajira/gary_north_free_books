224 An Introduction to Christian Economics

would still act as individuals standing before a righteous God, but
they would also be reminded that with power comes responsibility,
and this would include collective responsibilities, If such collective
responsibilities did exist, then temporal sanctions would again be
needed to enforce community laws, even over the non-members of
the visible, institutional church. It would therefore be likely that the
concept of property rights would be increasingly communal in per-
spective, as it had been in the Old Testament commonwealth.

This sense of communalism was given additional stimulus by the
historical situation in which fourth-century Christians found the
Roman Empire. When the Christians took possession of the political
sword, they found themselves confronted with an international econ-
omy which was in the process of disintegration, Economic arrange-
ments that had been specifically urban—-a money economy, im-
personal markets, freedom of contract—began to recede, especially
in the Western half of the empire. When social conditions began to
lose the older impersonality, a new impetus to local institutional
control was accented. With the reappearance of the idea of a
political community collectively responsible to God, and therefore
subject to God’s communal blessing or curse, the formal individualism
and voluniarism of the earlier Christian community—a community
which had been essentially an urban phenomenon——was altered.
Christian people were no longer the captives of a culture which they
regarded as apostate; they were now a part of a new kingdom, as in
the Old Testament, which possessed temporal authority. The ethic
of the Sermon on the Mount, based as it was on the assumption of
national or cultural captivity, began to be tempered by. the responsi-
bilities of power. It had always been difficult to assess just what men
owe to Caesar; when Caesar is a Christian, it becomes necessary to
determine the standards for ruling as well as the limits on obedience.3*

38 ‘This conflict has stemmed from the difficulty of applying the dual prin-
ciples of Peter—“We ought to obey God rather than men” (Acts 5:29)—and
Paul—“Let every soul be subject unto the higher powers" (Rom, 13:1a).
