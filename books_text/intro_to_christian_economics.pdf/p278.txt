266 An Introduction to Christian Economics

Republic, the Tatar ASSR, had its investment plan changed almost
five hundred times in 1961.2* Under these conditions, the task of
enterprise management would be impossible if it were not for
some ingenious (and often illegal} solutions worked out by factory
managers.

The basic solution has been the creation of a vast network of
“independent” supplies—a black market. This is the phenomenon
known informally as “blat.* Joseph S. Berliner, in his valuable study,
Factory and Manager in the USSR (1957), has described this process.
Since supply channels are often exasperatingly slow and frequently
deliver the wrong or inferior goods, managers must turn to alternative
sources of inputs if their production quotas are to be met (and their
bonuses and promotions received). For example, a plant may have
a surplus in any given year; this, in turn, is probably due to the fact
that the manager overstated his supply needs and understated his
plant’s productive capacity in the previous year, when the central
plans were drawn up. These additional goods may be traded to
some other firm for some future service or present loxury from that
firm. This aids not only those smaller firms that are on a lower
priority list for supplies, but it also helps the high priority industries
during periods of crisis. Certain “middlemen” with informal con-
nections are employed, usually under a bogus administrative title,
ag the agents for the blai operations. They are “pushers” whose ac-
tivities coordinate the underground facilities of supply and demand,
They are called the tolkatchi. Some firms employ only part-time
tolkatchi, especially the smalicr ones. In recent years, the government
has wisely removed the criminal sanctions that were once imposed
upon such activities of unauthorized exchange or resale of supplies.
In addition to this softening, the procedures for obtaining official
authorization to purchase extra supplies have been eased.2> The
State planners have, in effect, recognized the necessity of these “capi-
talistic” practices. Production goals.are sometimes more important
than official ideology. These practices go on as long as the conditions
of inefficient production and distribution remain. As Berliner says,
“The tolkatch thrives in an economic soil watered by shortages and
fertilized by unrealistic targets.”**

At this point, it would be wise to quote Alec Nove’s summary of

23. Alec Nove, “Prospects for Economic Growth in the USSR,” in Born-
stein and Fusfeld (eds.), The Soviet Economy, p. 318.

24. Joseph S. Berliner, Factory and Manager in the USSR (Cambridge,
Mass.: Harvard. University Press, 1957), chaps. 11, 12.

25. Berliner, “Blat Is Higher than Stalin,” in’ Abraham Brumberg (ed.),
Russia Under Khrushchev (New York:. Praeger, 1962), p. 173.

26,. Ibid., p. 175.
