Chapter VII
THE FALLACY OF “INTRINSIC VALUE”

[No matter how many times the economist explains that there
is no such thing as intrinsic value, he finds members of his
classes, at least the conservative ones, defending gold as
money because “gold has intrinsic value.” Gold does not have
intrinsic value, Nothing on earth, from an economic point of
view, has intrinsic value. Nothing, It just will not do any
good to tell you this, however. Sooner or later you will be
heard to say, “Gold has intrinsic value.” It is a shame that
the true statement, “Gold has historic value,” just doesrt
have the same Pzazzz.]

Vf people value something, it has value; if people do not value
something, it docs not have value; and there is no intrinsic
about it. Rt. Hon, J. Enoch Powell, M.P.

“Ideas die hard,” says an old proverb. Even in an age of rapid
change, such as our own, the slogans, clichés, and errors of earlier
times seera to persist; it often seems that the truths that once brought
peace, stability, and steady progress are the first things to be aban-
doned, while the errors persist undaunted. Henry Hazlitt once wrote of
John Maynard Keynes that the true things he said were not new, and
the new things he said were not true. Yet it is the new aspect of Keynes’s
“New Economics” that has fascinated today’s guild of economists.

The triumph of the slogan is understandable. We are limited
creatures. We cannot attain exhaustive knowledge of anything, and
certainly not of everything. As a result, we find ourselves at the mercy
of the expert; simultaneously, we live our day-to-day lives in terms of
ideas that we cannot be continually re-examining. Some things must
be accepted on faith or by experience; we have neither the time nor
capacity to rethink everything we know. Still, no intelligent person
dares to neglect the possibility that his opinion in some area or other
may be open to question, At times it is vital that we reconsider a
subject, especially if it is a barrier to clear thinking or effective action.
If our error is in a realm of life in which we claim to be experts, or
at least skilled amateurs, then the necessity of careful reasoning is
exceptionally important. The persistence of some erroneous line of

69
