Gold’s Dust 49

standard. National governments have monopolized the control of
gold for exchange purposes; they can now print more IOU slips than
they have gold. Domestic populations cannot redeem their slips, and
since March of 1968, very few international agencies have access to
governmental gold stocks (or so we are told). The governments
create more and more slips, the banks create more and more credit,
and we are deluged in money of decreasing purchasing power. The
tules of the game have been shifted to favor the expansion of cen-
tralized power, The laws of economics, however, are still in effect.

One can easily imagine a situation in which a nation has a tiny
gold reserve in its national treasury. If it produces, say, bananas,
and it limits its purchases of foreign goods by what it receives in
foreign exchange for exported bananas, it needs to transfer no gold.
It has purchasing power (exported bananas) apart from any gold
reserves, If, for some reason, it wants to increase its national stock
of gold (perhaps the government plans to fight a war, and it wants a
reserve of gold to buy goods in the future, since gold stores more
conveniently than bananas), the government can get the gold. All
it needs to do is take the foreign money gained through the sale of
bananas and use it to buy gold instead of other economic goods, This
will involve taxation, of course, but that is what all wars involve.
Tf you spend jess than you receive, you are saving the residual; a
government can save gold. That’s really what a gold reserve is—a
savings account.

This is a highly simplified example. It is used to convey a basic
economic fact: if you produce a good (other than gold), and you
use it to export in order to gain foreign currency, then you do not
need.a gold reserve. You have merely chosen to hoard foreign cur-
tency instead of gold. That applies to citizens and governments
equally well,

What, then, is the role of gold in international trade? Dr. Patrick
Boarman clearly explained the mechanism of international exchange
in The Wall Street Journal of May 10, 1965:

The function of international reserves is NOT to consummate
international transactions. These are, on the contrary, financed
by ordinary commercial credit supplied either by exporters or
importers, or in some cases by international institutions. Of
such commercial credit there is in individual countries normally
no shortage, or internal credit policy can be adjusted to make up
for any untoward tightness of funds. In contrast, international
reserves are required to finance only the inevitable net differences
between the value of a country’s total imports and its total ex-
ports; their purpose is not to finance trade itself, but net trade
imbalances,
