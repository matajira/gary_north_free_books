156 An Introduction to Christian Economics

subtle form of currency debasement, and far more dangerous, so
what he told his people would be true sevenfold today:

How is the faithful city become an harlot! it was full of judgment;
righteousness lodged in it; but now murderers. Thy silver is
become dross, thy wine mixed with water: Thy princes are rebel-
lious, and companions of thieves (Isa. 1:21-23).

Currency debasement is a sign of moral decay. It is accompanied
by corrupt governments and sinful leaders. But at least the average
citizen can tell when the silver coins are being debased; with paper
money such detection is difficult apart from economic signs in the
price level. The good money looks just like the unbacked money.
‘Yet Coogan would have no backing in metal for any of the paper.
In short, she wants a currency, in Isaiah’s words, of pure dross—no
silver at all!

Coogan wrote in the middle of the depression. Her recommen-
dations for economic well-being reflect most of the major errors of
her day, She calls for State-financed pension plans, reminiscent of
the old Townsend Plan.’ She wants currency debasement to obtain
full employment, an idea out of the Keynesian tool kit. All the
quackery of the 1930’s at least gets a brief hearing in Coogan’s
books, and by referring the reader to Soddy, she even brings in a bit of
Technocracy, for Soddy admits that he sces a great deal of truth in
the Technocrat system.®! She wants “equitable” prices set by Monetary
Trustees, managed currency, and virtual financial monopoly by the
State Treasury. Gold, which would resist the encroachments of the
State, is her bitter enemy. She would curtail bank money and bank
notes only. by stamping out the right of free coinage and the right

90. Ibid., pp. 255-256. Townsend also wanted to spend fiat money into cir-
culation, thus creating “effective demand.” See Alvin H. Hansen, Full Re-
covery or Stagnation (New York: Norton, 1938), pp. 92-93. As a Keynesian,
Hansen wants the government, in cooperation with the Federal Reserve System,
to make the necessary purchases, and. not have the money sent directly to pri-
vate citizens, since they are supposedly not intelligent enough to spend their
money on. those items that will promote rapid aggregate national economic
recovery, pp. 316-318. Therefore, we may have to come to live with full-time
deficit financing, pp. 301-302. Keynesianism is Social Credit with more -exten-
sive intervention into the economy by the government. It wants the government
to get the benefits of the counterfeit fiat money rather than the private con-
sumers, i.¢., the citizens who gain access to the counterfeit funds before the
new money increases everyone else’s cost of living. Keynesians oppose Town-
send plans—giving funds to consumers directly—except welfare “clients” may
receive funds. Ultimately, the Slate is to be an investment banker: Hansen,
p. 318.

91. Frederick Soddy, Wealth, Virtual Wealth and Debt (2nd ed.; Hawthorne,
Calif.: Omani, [1933] 1961), pp. 13-14. Anyone who has read Soddy’s writings
side-by-side with the publications of Technocracy is aware of the extraordinary
similarities, It makes you wonder if Howard Scott, the founder of Technocracy,
might have “borrowed” Soddy's ideas without actually footnoting their origin.
‘Who knows, and these days, who cares? A refutation of one constitutes a
refutation of the other.

 

 
