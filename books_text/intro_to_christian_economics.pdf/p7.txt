INTRODUCTION

Is there such a thing as a distinctively Christian cconomics? ‘Yes.
Are there explicitly Christian economic teachings that no secular
economist has written about? So far, probably not. ‘The uniqueness

- of Christian economics is that the Christian economist has specific,
concrete biblical revelation concerning the Hmits of economic theory
and practice. A secular economist may see the relationship between
monetary inflation and fraud, but he does nat stand with the authozity
of the Bible behind him, and he is, in the mid-twentieth century,
utterly unable to convince ninety-nine percent of his academic
colleagues (and no minister of finance) of the validity of hix critique.
Thus, the monetary theories of a Mises or a Rothbard He unused
in academic circles, The proponents of the economies of the full
gold-coin standard are unaware that their theory resty on certain
God-given external conditions, They simply accept these fimitacions
of nature as “given,” and they do not bother to inquire as to the
source of them. Such investigations, every secular economist would
tell us, are not relevant, are not scientific, cannot be demunstrated
by ethically neutral, rationalistic presuppositions. OF course, if we
are to judge by the state of the economics profession, nothing can be
demonstrated in this fashion, because none of them agrees with all
the rest on any issue. But God, @ priori, is irrelevant ta ccanontic
reasoning, even among the @ posterivrisis,

God has cursed the carth (Gen. 3:17-19}. ‘This is the starting
point for all economic analysis. ‘The earth no fonger gives up her
fruits automatically. Man must sweat to eat. Furthermore, among
the able-bodied, Paul wrote, “if any would net work, neither should
he cat” (II Thess. 3:10). All of the speculations of Marxist econu-
musts will not find a way for the post-revolution utopian werd
to avoid the curse of scarcity imposed by the first principle, and
none of the government welfare schemes of professional welfare
economists will escape the ethical limits of the second, ‘These are
two of the “givens” in the universe. All the Ph.D.’s in the work,
working ten hours a day (inconceivahlé for tenured Ph.Ds), wilf
not find any escape from these fimitations, The sheiety which

 

 

vil
