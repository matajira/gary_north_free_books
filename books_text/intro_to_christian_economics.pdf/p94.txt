82 An Introduction to Christian Economics

an increasing number of competing “priority” ends demands an
economic science for their solution.*?

Tt is this lack of economic science that has created the present
crisis in the Soviet economy: constant fluctuations back and forth
between regional and national economic planning, constant shortages
of key production goods, and a full-time quasi-legal network of black
market suppliers which keeps the planners’ errors from bringing the
whole economy to a grinding halt The two factors chiefly respon-
sible for the successes of the Soviet economy, however limited out-
side of the USSR’s military-industrial complex, have been its own
domestic “free” market—the black marketeers (folkatchi) and the
small private garden plots of the peasants—and the productivity of
the Canadian wheat farmers. J am reminded of an editorial cartoon
I saw years ago (I think Herblock did it), in which we find a Soviet
army officer saying to a commissar: “But Comrade, if we win the
whole world to communism, where will we buy our wheat?”

Fascinated with the propaganda value of Soviet growth figures
(themselves frequently suspect, especially in the “conclusion” sec-
tions**), American economist-propagandists have become obscssed
with the task of equaling or exceeding Soviet growth rates. They see
this as a necessity, in spite of the obvious mathematical fact that the
larger a base figure is, the larger aggregate increases must be to keep
it expanding at a constant rate, The Soviets, decades behind the
United States in economic output, can more easily enjoy high growth
rates, and even they have discavered the difficulty of retaining their
high rates as their economy expands (witness the falling off of their
growth rates since 1960).

Why this obsession with aggregate growth? One answer which I
have already mentioned is the fact that freedom is difficult to chart
statistically, We therefore see our propagandists appealing to other
results of the two societies in order to compare them. Another im-
petus to the “growth game” is the very methodology of contemporary
academic economics: Men whose concern for methodological rigor
far outweighs any other professional goal in their lives will have a
tendency to examine aspects of an economic system that ate sub-
ject to the “elegance” of mathematics. They will tend to search
diligently for “statistical handles” that are nicely “neutral,” and there-
fore acceptable to other professional economists as valid measure-

20. Jan S. Prybyla, cited in Gary North, Mars Religion of Revolution
(Nutley, N. J.: The Craig Press, 1968), p.

241. Gary North, “The Crisis in Soviet Economie Planning,” Modern Age
(Winter, 1969-70) [chap. 22, below].

22. Jasny, “Soviet Statistics,” The Review of Economics and Statistics
(1950), p. 92 ff
