40 An Introduction te Christian Economics

pay up, with interest. He gocs to his bank, takes out the $90, and
his bank has to call in the $81 it had loaned out. The $900 built on
the original $100 disappears, again as if by magic. This is the process
of demonetization of debt, and it is clear why there would be a drastic
decline in prices, and why a lot of banks would be closed, some of
them permanently.

The suffering imposed by depression is unfortunate, but it is the
price which must be paid for survival, If the consequences of runaway
inflation are to be avoided, then this discomfort must be borne.

The depression, lest we forget, is not the product of a defunct
capitalism, ay the critics invariably charge. It is the restoration of
capitalism. Free banking, even without the legally enforced one
hundred percent reserve requirement, can never develop the ram-
pant inflation described here.1? The inflation came as a direct result
of State-enforced policies, and the State must bear the blame. Sadly,
it never does. It accepts responsibility for the politically popular
“boom” conditions, but the capitalists cause the “busts.”

6. The Temptation to Return to the “Junk”

The analogy ends here, as far as I am concerned, with only one
unfortunate addition. The reformed addict, I am told, never com-
pletely loses his desire to return to the “junk,” The lure of the old
euphoria, the days of junk and roses, always confronts him. The
temptation to inflate once again is likewise always with us, and es-
pecially during the transition (depression) period. America’s 1929
depression is the best historical footnote to the unwillingness of an
economy to take its medicine and stay off of the “junk,”3

This much is certain, the deliberate inflating of a nation’s circulat-
ing media is an ancient practice which has generally accompanied a
decline of the national standards of morality and justice. The prophet
Isaiah called attention to the coin debasing of his day, including it in
a list of sins that were cammon to the society. They are the same
social conditions of our own era:

How is the faithful city become an harlot! it was full of judgment;
righteousness lodged in it; but now murderers. Thy silver is be~
come dross; thy wine mixed with water: Thy princes are rebel-
lious, and companions of thieves... (Isa. 1:21-23a).

Debased currency is a sign of moral decay. In the final analysis,

12. Mises, Human Action (3rd ed.; Chicago: Regnery, 1966), p. 444 ff.

13, Rothbard, America’s Great Depression (Princeton: Wan Nostrand,
1963), stands as the best book on the subject. It has been reprinted by Nash
Publishing Co, See also the important book, Banking and the Business Cycle,
by C. A. Phillips, T. F. McManus, and R. W. Nelson (New York: Macmillan,
1937). This has been reprinted, under Rothbard’s editorship, by the Arma
Press of New York.
