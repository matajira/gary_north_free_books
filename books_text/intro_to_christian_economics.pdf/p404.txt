BIBLIOGRAPHY

Most of the professional economic journals are unreadable, They
are primarily mathematical and statistical, imitating the methodology
of the physicai sciences. In part, this stems from what Prof, Fritz
Machlup has called “The Inferiority Complex of the Social Sciences”
(in Mary Sennholz [ed.], On Freedom and Free Enterprise: Essays
in Honor of Ludwig von Mises {[Priuceton: Van Nostrand, 1956]).
Such methodological pathologies as historicism, holism, behaviorism,
metromania, predictionism, mathematosis, and experimentomania,
Machlup argues, have infected the thinking of the younger scholars.
Pick up any issue of The American Economic Review or Economet-
rica and see for yourself. Yet it is publication in these two journals,
above all, that can elevate the prestige of any economics faculty, a
point made clear in the concluding remarks by Prof. John Siegfried
in the American Economic Association’s own journal, The Journal
of Economic Literature (March, 1972).

Yet it goes beyond mere intellectual inferiority. The crisis in
modern economic epistemology—and it is increasingly recognized
as a crisis by some important scholars (Saturday Review [Jan. 22,
1972}, the issue which is published annually for the Committee
for Economic Development})—-stems from what Herman Dooye-
weerd and C. 8. Lewis have recognized as the antinomy of the
nature-freedom scheme of post-Kantian thought. The science ideal—
prediction, rational control, mathematical precision in measuring all
things—relentlessly crushes all that stands before it, Max Weber, the
great German sociologist (who was the outstanding social scientist
of this century), predicted exactly this train of events, as rationaliza-
tion and bureaucratization rush onward. (‘Science as a Vocation”
[1918], in H. H. Gerth and C. Wright Mills [eds.], From Max Weber:
Essays in Sociology [New York: Oxford University Press, 1947].)
Lewis is correct: a full commitment to the science ideal brings with
it “the abolition of man” (sce Lewis’ book of the same name, Mac-
millan, 1947),

Much of this intellectual game is a hypocritical charade, John
Kenneth Galbraith blows the whistle on the players in his Economics,
Peace, and Laughter (New York: New American Library, 1972):

392
