x An Introduction to Christian Economics

practiced, either by the State or by a quasi-governmental central
bank. In the same way that a Cliristian would not have concerned
himself with the most efficient means in 1916 of exterminating
Armenians under Turkish rule, or with transportation economics
in transporting Jews to gas chambers in Germany in 1943, Christians
would, if consistent to their presuppositions, refuse to devote a
lifetime to discovering ways of making monctary debasement work
better, Christian economics grasps the fact that technical skills are
not neutral, that cach man is responsible before God for the use he
makes of his skills, that biblical revelation can, at crucial points,
determing what is and what is not a legitimate avenue of inquiry or
application for economists,

Man, for example, is not omniscient, nor can bis computers ever
make him omniscient. Therefore, any system of economics or
pscudo-economics that requires ommiscience in order to operate ef-
fectively is perverse. It rests on a fatal, ethically rebellious, assump-
tion: that man can remake himself through the application of
neutral science. Man, in short, can become God. Members of the
Union for Radical Political Economics, some 1,200 strong, who &a
argue for the abolition of all corporate profits or the abolition of
price tags for cighty percent of the goods in the American economy,
hold such a view of man, They see in the central planning com-
mission the operation of knowledge approaching God’s.* Christian
economics would simply reject, a priori, the possibility of a society
of such abundance that goods could become free—i.e., that at zero
price, the demand for a majority of goods and services would not
exceed supply. Genesis 3 denies this as an operating hypothesis.
To use such a hypothesis as an operating madel of economic behavior
is to pursue a demonic economics, an economics of “stones into
bread.”

Christian economics is a virtually unexplored field. The eroding
effects of secularism, intellectual dualism, pietism, and social anti-
nomianism have crippled the work of Christians in their extension
of the Kingdom of God. Not only have they failed to extend the
revelation of the Bible into the realm of society, they have even

3. Martin Bronfenbrenner, “Radical Bconomics in America: A 1970 Sur-
vey," Journal of Economic Literature, VIL (Sept., 1970); “The Unorthodox
Ideas of Radical Economists Win a Wider Hearing,” Wall. Stree? Journal
(Feb. 11, 1972); Business Week (March 18, 1972), pp. 72,74. The featured
celebrity of the latter articles is Dr. Howard Sherman, professor of economics,
University of California, Riverside. I studied under Dr. Sherman as a graduate
student. He was always fair enough in listesing to conservative views, al-
though they generally appalled him, He once told me that the favorite course
he ever taught was an adult university extension course in science-fiction
literature. That, somehow, seems to speak for itself.
