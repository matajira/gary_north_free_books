290 An Introduction to Christian Economics

was one of the favorite themes of nearly everyone associated in
any way with public educational institutions. Yet the myth was
shattered in one academic year, 1968-69." The glut of teachers at
all levels, from kindergarten to the graduate school, appeared almost
overnight. The teacher-job “gap” simply was swallowed up in the
outpouting of graduates in June of 1968; only in “special education”
—the euphemism for the handicapped, the culturally deprived, and
the retarded—is there a comparable gap, and the openings there
are being depleted by falling school revenues.

This glut is not strictly an American phenomenon, It is as serious
in the British Isles, perhaps worse. The British teaching certificate
is just that, a license to teach; it is not easily transferred to any other
oceupation, The English have overbuilt their institutions of higher
education, and the graduates are now reaping the whirlwind.

Previously sacrosanct fields like physics are now oversupplied. The
post-Sputnik era saw a seemingly endless barrage of propaganda in
favor of expanding our pool of available scientific talent. The “science
fairs” in the high schools, the federal scholarships, the televised
miracles of space travel all combined to convince American students
that the ticket to guaranteed security was the engineering degree and
the Ph.D, in physics, Easy Street has once again turned into a dead
end, as too many people crowded down its narrow path. Federal
grants from such agencies as NASA have fallen dramatically; federal
loans to students have begun to dry up. Budget cutting has removed the
fat from many federal science programs, to the dismay of those scien-
tists who have an ideological commitment to State-financed research.*

The extent of the glut in physics can be seen through a very
specific case. Heidelberg College in Ohio last year had an opening
for a teacher in physics. It received a total of 361 applications. Tiny
Dayton High School, in Dayton, Texas, received applications from
15 Ph.D.’s in physics, yet the school has only 455 students, and it
offers only a single course.° Industry has been less and less willing
to interview Ph.D.’s due to the highly specialized, inflexible nature
of Ph.D. training. The cut-backs in aerospace have hurt the market
for these trained specialists. An outstanding 40 percent of the 1969
graduates in physics were on post-doctoral fellowships in 1970.*

3, Newsweek (June 29, 1970) ceports that the first year in which a surplus
existed was 1967-68. This was not manifest at the time, however; it took a year
for the glut to register as a permanent phenomenon.

4. Science (March 12, 1972), p. 1092, Cf. Michael D. Reagan, Science and
the Federal Patron (New York: Oxford University Press, 1969), Reagan
favors such patronage, but he shows the problems inherent in such a relation-
ship. He also provides considerable economic data on the extent of the aid.

3. Time Gune 29, 1970).

6. Lhe Chronicle of Higher Education, IV (June 8, 1970), p. 8
