The Theology of the Exponential Curve 719

merely a present world, ever changing, never static; the aca-
demic positivists burrowing like boll-weevils in the thickets of
facts, mindless, deliberately, of purpose and meaning outside the
orbit of their own activity; the public prophets using pseudo
science to justify a repetitive, cyclical interpretation of history,
and the littérateurs preoccupied with evocation and exercise of
the imagination, The result is nihilistic and socially impotent.

AU are equally guilty I think of wilfully rejecting the one certain
judgment of value that can be made about history, and that is
the idea of progress. If this grcat truth were once more to be
frankly accepted, the reasons for it, and the consequences of it,
consistently and imaginatively explored and taught, history would
not only be an infinitely richer education but also play a much
more effective part in the culture of western society.‘t

It is a bleak, bleak picture that Plumb paints. The halls of ivy
have become empty shells—broken shells, like Humpty Dumpty.
“History properly so-called can be written only by those who find and
accept a sense of direction in history itself,” writes E. H. Carr. “The
belief that we have come from somewhere is closely linked with the
belief that we are going somewhere. A society which has lost belief’
in its capacity to progress in the future will quickly cease to concern
itself with its progress in the past.”!?

The Concept of Economic Growth

The social scientists who have retained their faith in planning have
not been hit so hard as those in the humanities by the death of the
idea of progress. Economists in recent years have become fascinated
with the possibility of continual economic expansion. The first signs
of the faltering of this faith have become evident in recent months,
specifically with regard to the question of ecology and the polhition
problem. In November, 1969, at a meeting of the United States Com-
mission for UNESCO, a call for total stability went forth to the
world. The New York Times News Service in late November ran 2
feature article in which Jerry Mander, a San Francisco advertising
Tan, was quoted as saying, “Beginning now, there should be national
preparations toward a no-growth economy.” Robert Anderson,
board chairman of Atlantic Richfield Oil Company, called for nega-
tive population growth. Ha! Lehrman, president of the Overseas
Press Club, said: “You've got to prove growth is evil. How do you
do it?” Mander admitted that “at this moment, we are totally un-
prepared, emotionally, psychologically, and technologically for the
emerging facts.” The propaganda campaign is beginning. If it takes

Ll. Ibid, p. 34.

12. E. H. Carr, What Is History?, cited in Rushdoony, Biblical Philosophy
of History, pp. 132-133.
