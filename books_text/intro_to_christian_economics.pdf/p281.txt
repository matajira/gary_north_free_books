Chapter XXTIT
THE MYTHOLOGY OF SPACESHIP EARTH

(“The Mythology of Spaceship Earth’ is self-expianatory.
The Spaceship Earth propaganda has intensified since the time
that I first published this piece in The Freeman (Nov., 1969).
The big problem is that everyone using the Spaceship Earth
slogan thinks that his group should be universally accepted
as the officers of the crew. There are a lot of peaple whe are
lching to be captain of the ship.]

The flight of Apollo XJ was probably the most stupendous tech-
nological achievement of the decade, (Unquestionably, it was the
most stupendous bureaucratic achievement of the decade: scheduled
for 1969, it actually took place in 1969!) Editorials in every paper
in America, I suppose, have lauded the flight as the monument to
the capacities of mankind to conquer nature and order our affairs, the
assumption being that the ability to fly a rocket implies the ahility to
organize a society, in theory if not in practice. The flight has brought
to the forefront that old cliché, “Man’s scientific wisdom has outrun
his moral wisdom”; we can go to the mtaon, yet somehaw we have
failed to solve the problem of mass poverty in the United States.

‘The gap between moral wisdom and scientific knowledge has been
a problem since the scientific revolution of the sixteenth century.
Immanuel Kant, writing in the late 1700's, struggled mightily with
this very question: How can man. bridge the intellectual chasm be-
tween scientific knowledge (the realm of Jaw and necessity) and
moral knowledge (the reaim of freedom and choice) without sacri-
ficing the integrity of one or the other? Hegel, Marx, and the modern
moral philosophers have all lived in the shadow of this dilemma,
and the crisis of modern culture reflects man’s failure to resolve it.
The responses to this dilemma, asa rule, take one or the other of two
forms, symbolized by Arthur Koestler as the Commissar on the one
hand, and the Yogi on the other.

The Commissar is enraptured with science and technology; he is
confident that scientific planning in proper hands can so alter man’s
environment as to bring about a new earth and a new mankind,

269
