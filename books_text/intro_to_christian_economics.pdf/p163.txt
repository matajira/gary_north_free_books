Gertrude Coogan and the Myth of Social Credit I51

Jf the general price level remains constant because of additional
paper money being inserted into the economy, then by definition
there is monetary inflation going on. In a highly productive free
market economy, Mises points out that there is “a general tendency
of money prices'and money wages to drop.”7? As more goods are
produced, and because the supply of money metals remains rela-
tively constant, prices and wages tend to fall in terms of money
metals. Real wages, that is, the quantity of goods that a given wage
will buy, will be rising because of the increased production. There-
fore, Coogan’s “stable price” scheme is definitely inflationary, for
prices ought to be declining.’® In the United States, for example, be-
tween 1867 and 1897, the wholesale price index fell (with 1929 as the
base year) from about 168 to 68, or 100 points, or some 60 percent.
Simultaneously, the country’s population almost doubled, from 37
million to 72 million, and real output went up by 400 percent. This
means that real per capita income doubled. The money stock
tripled, from $1.3 billion to $4.5 billion, but the velocity of moncy
(turnovers per unit of time) was cut in half, indicating that the
effective impact of the monetary inflation was reduced considerably.
What does this mean? It means that per capita output can rise
drastically in the face of falling prices. More important, it means
that if prices can fall by 60 percent in the face of a tripling of the
money stock, then if the general price level remains stable, we can be
fairly certain that the State is pursuing a policy of extensive monetary
inflation. (J am not using these figures to “prove” my point, but only
to illustrate it; all such index numbers are approximate and somewhat
arbitrary.)

Conservative economists of the 1920’s, as well as liberals, were
lured into making outrageously inaccurate statements about the
blessings of “‘stable prices” and the “fact” that there would never
again be a depression. Irving Fisher, perhaps the most prestigious
economist of the day, made one of these ridiculous statements in
September of 1929.8 Fisher, like Coogan, was a supporter of
“stable prices,” and it was he who, more than any other economist,
deserves the title of “father of the index number,” that magical tool
which supposedly enables the State’s planners to stabilize the econ-

71, Mises, Money and Credit, p. 417.

78. North, “Downward Frice Flexibility,” op. cit.

79. The money and income data come from Milton Friedman and Anna

Jacobson Schwartz, A Monetary History of the United States, 1867-1960 (Na-

tional Bureau of Economic Research, published by Princeton University Press,

1963), charts 3, 8 (pp. 30, 94-95). The population figures are in Historical Sta-

tistics, p. 7, Series A 1-3, “400 percent” is conceptually impossible: 4 X, really.
80. New York Herald Tribune (Sept. 5, 1929), cited in OA Yeah? (New

York: Viking, 1931), p. 37.
