Subsidizing a Crisis: The Teacher Glut 289

Advocates of the free market as a tool for efficient allocation of
scarce resources have long been critical of the way in which education
is financed in the United States. A host of studies are available
that deal with the lowering of quality, the uncreative uniformity,
and the spiraling costs of public education.! Only in recent months
have communities even contemplated the possibility of a system like
Milton Friedman’s voucher program, in which the parent would
receive the educational subsidy rather than the local public school.”
The obvious crises since 1965 in our public schools, coupled with
the realization on the part of black militants that educational plu-
ralism is advantagcous, have led to at least some rethinking of the
assumptions of American public education, With the realization
that education is not neutral, some former advocates of racial,
intellectual, and cultural integration have come to the conclusion
that “democratic education” has produced a generation of uprooted
graduates—drones and revolutionaries—who are not really very
different from Dustin Hoffman’s caricature.

This realization, however, has been a distinctly minority revelation.
The message has not come to the institutions of higher learning in
this country. They have gone on as before, tinkering occasionally
with the curriculum, adding a handful of courses like Black Studies
or Chicano Studies, but generally proceeding in a “business as usual”
fashion. Nevertheless, the violation of supply and demand that is
fundamental in any system of subsidized education has now resulted
in something wholly unforeseen by the bulk of American educators:
the perennial shortage of teachers came to an end, quite abruptly,
in 1968. The shock waves of that event are only now registering on
the bureaucratic structure of American higher education.

For how many years were Americans subjected to the perpetual
hand-wringing of professional educators over the teacher shortage?
How many news releases from the National Education Association
were printed, without any criticism, by the public news media? It

1. Cf. Benjamin A. Rogge, “Financing Higher Education in the United
States,” New Individualist Review, IV (Summer, 1965); available also from
the Center for Independent Education, Wichita. E. G. West, Education and
the State (London: institute for Economic Affairs, 1967). Roger A. Freeman,
“Crisis in American Education,” Christian Economics (Sept., 1970).

2. Milton Friedman, Capitalism and Freedom (University of Chicago Press,
1962), chap. 6; Robert L, Cunningham, “Education: Free and: Public,” New
Individualise’ Review, TH (Summer, 1963). Governor Reagan of California
mentioned the possibility of instituting a voucher system as an experiment; this,
however, was in a campaign speech. The Center for the Study of Public Policy,
located in Cambridge, Mass., has recommended the establishment of a 5-8 year
experiment of 12,000 elementary students; the plan would cost $6-8 million.
This was the conclusion in the Office of Economic Opportunity-financed study,
Education Vouchers.
