100 An Introduction to Christian Economics

some new rigidity. It has nothing to do with the fluid continuity of
flexible market pricing. Discontinuous “stability” is the plague of
politically imposed prices, as devaluations come in response to some
disastrous political necessity, often internationally centered, involving
the prestige of many national governments. It brings the rule of law
into disrepute, both domestically and internationally. Sooner or later
domestic inflation comes into conflict with the requirements of inter-
national solvency.??

For those who prefer tidal waves to the splashing of the surf, for
those who prefer earthquakes to slowly shifting earth movements, the
rationale of the political monopoly of money may appear sane. It is
strange that anyone else believes in it. Instead of the localized dis-
continuitics associated with private counterfeiting, the State’s planners
substitute complete, centralized discontinuities, The predictable mar-
ket losses of fraud (which can be insured against for a fee) are re-
garded as intolerable, yet periodic national monetary catastrophes like
inflation, depression, and devaluation are accepted as the “inevitable”
cosis of creative capitalism. It is a peculiar ideology.

The third problem seems to baffle many well-meaning free market
supporters. How can a privately established monetary system linked to
gold and silver expand rapidly enough to facilitate busincss in a mod-
ern economy? How can new gold and silver enter the market rapidly
enough to “keep pace,” proportionately, with an expanding number
of free market transactions? The answer seems too obvious: the
expansion of a specie-founded currency system cannot possibly grow
as fast as business has grown in the last century. Since the answer is
so obvious, something must be wrong with the question. There is
something wrong; it has to do with the invariable underlying assump-
tion of the question: today’s prices are downwardly inflexible.

It is a fact that many prices are inflexible in a downward direction,
or at least very, very “sticky,” For example, wages in industries
covered by minimum wage legislation are as downwardly inflexible as
the legislatures that have set them. Furthermore, wages in industries
covered by the labor union provisions of the Wagner Act of 1935
are downwardly inflexible, for such unions are legally permitted to
exclude competing laborers who would work for lower wages. Prod-
ucts that come under laws establishing “fair trade” prices, or products
undergirded by price floors established by law, are not responsive to
economic conditions requiring a downward revision of prices. The
common feature of the majority of downwardly inflexible prices is the
intervention of the political sovereignty.

12. Gary North, “Domestic Inflation versus International Solvency,” The
Freeman (Feb. 1967) [chap. 5, abovel.
