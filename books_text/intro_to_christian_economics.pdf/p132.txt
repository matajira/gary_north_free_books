120 An Introduction to Christian Economics

citizens all over the globe have the right to own gold and make con.
tracts in gold. But just because utopia has not arrived, there is no
reason to abandon the theory of voluntary exchange at unhampered
prices. The argument we hear so often today is this: “Given the
government’s monopoly over money, given the policies of deficit
financing through monetary inflation, given domestic legal tender
laws, we therefore need price controls over our international monetary
exchange.” Polylogism! The fact that we find ourselves in an in-
creasingly socialistic economy in no way disproves the theoretical
validity of free pricing—any time, any place, under any circumstance.
If the theoretical (and therefore the practical) validity of free pricing
is undercut in any way simply because of all the socialistic “givens”
that we operate under, then Marx was right, Hegel was right, the
German historical school of economics was right, institutional eco-
nomics is right, historicism is right, and economic theory is wrong.

There is a tendency, argues Mises, for one intervention by the
State into the economy to lead to another intervention. The disrup-
tions caused by the first intervention lead to cries for further politi-
cal intervention to solve them. The State takes control of moncy,
to “reduce the irrationality of the domestic money markets.” (And
to arrogate unto itself ultimate sovereignty.) Then it inflates the cur-
rency in order to increase its own influence in the affairs of men by
gaining access to scarce economic resources with the inflated cur-
rency. Then citizens refuse to accept the debased money. So the
State’s officials pass legal tender laws. The money, now artificially
overvalued, drives out both gold and silver. People prefer to trade in
the artificially overvalued money and either hoard the gold and silver
or send it abroad where it can purchase foreign goods cheaper than
the domestic inflated currency can purchase them. As domestic goods
climb in price due to the inflated paper currency, imports increase
and dollars flow out; foreign central banks then raise the price of their
currencies in relation to dollars. The United States Government real-
izes that this exposes its policies of domestic monetary inflation and
therefore presses for fixed exchange rates. Then foreign governments,
buried in dollars (at the artificially low price), begin to demand gold
Cheld by our government at an artificially low 1934 price). One
intervention leads to another, usually. But not always.

The exception came on August 15. Basically, the President had three
choices. First, balance the budget and stop the monetary inflation—
maybe even use the surplus of revenue over expenditures to reduce the
national debt. Unfortunately for political purposes, such an action
would have risked depression and high unemployment (given the previ-
ous policies of monetary expansion and the downward inflexible wage
