196 An Introduction to Christian Economics

will be over in 1971. We are about to see a new round of inflation,
this time on an unprecedented. level for peacetime America.

Five nations account for over 80 percent of the free world’s
production of copper: the U.S.A., Canada, Zambia, Chile, and the
Congo. Canada’s economy is so closely tied to ours that our domestic
inflation will tend to be “exported” to Canada; as our prices climb,
we will tend to import goods from Canada (especially raw materials
like copper), thus equalizing the two national price levels. Conditions
in Zambia, Chile, and the Congo are not easily forseen, but a safe
estimate would be for increased revolutionary pressures on the
three governments, and the likelihood of the nationalization of mining
industries where this has not already taken place, Both are bad for
production. With falling production and stable or increased demands
for copper by the industrial nations, the price of copper ought to
rise.

For these reasons, collectors would be wise to check the com-
modities section of their newspapers’ financial pages. Copper prices
(not, by the way, the less important “non-ferrous copper’ listing)
are listed there, and they should be checked at Jeast once a month.
Collectors should watch for any indication of an upward movement
in prices that continues for more than a few months.

There is, of course, the ever-present threat of price controls. A
Jarge industry like copper mining and production is always subject
to governmental pressures, both “voluntary” and involuntary, to
hold down selling prices, just as the steel industry is. The disastrous
effects of the employees’ strike of 1967-68 will serve as a warning to
company managers not to resist wage demands in the future, As a
result, the industry could easily be caught in a wage-price squeeze,
cutting profits to a minimum, or even producing losses. Price and
wage controls inevitably lead to shortages, rationing, and other
economic rigidities. Production is stifled.

If policies of monetary inflation are maintained, and if price
controls are imposed, the results will be the creation of black markets
of the controlled commodities, Thus, even if the official price of
copper is kept below the $1.55 level through the imposition of price
controls, there will be a market for pennies, Gresham’s Law, that
“bad money” or “soft money” will drive “bard” currency out of
circulation, stays in effect.

There will be a further inducement for the Treasury to abandon the
copper penny. As inflation continues, the penny carries less and
less overall weight in the economy. Even today, the penny is
primarily a tax coin, used either in sales taxes or as coin meter
parking devices. (These, it should be noted, are meters used to
