The Biblical Critique of Inflation 17

connection with the concern as permanent and even if it actually
behaves as financial theory would have stockholders behave, is
at one remove from both the functions and the attitudes of an
owner. As to the third group, small stockholders often do not
care much about what for most of them is but a minor source of
income and, whether they care or not, they hardly ever bother,
unless they or some representatives of theirs are out to exploit
their nuisance value; being often very ill used and still more often
thinking themselves ill used, they almost regularly drift into an
attitude hostile to “their” corporations, to big business in general
and, particularly when things look bad, to the capitalist order as
such. No element of any of those three groups into which I
schematized the typical situation unconditionally takes the atti-
tude characteristic of that curious phenomenon, so full of meaning
and so rapidly passing, that is covered by the term Property. . . .

Thus the capitalist process pushes into the background all those
institutions, the institutions of property and free contracting in
particular, that expressed the needs and ways of the truly “pri-
vate” economic activity. Where it does not abolish them, as it
already has abolished free contracting in the labor market, it
attains the same end by shifting the relative importance of ex-
isting legal forms-—the legal forms pertaining to corporate busi-
ness for instance as against those pertaining to the partnership or
individual firm—by changing their contents or meanings. The
capitalist process, by substituting a mere parcel of shares for the
walls of and the machines in a factory, takes the life out of the
idea of property. It loosens the grip that once was so strong-—
the grip in the sense of the legal right and the actual ability to do
as one pleases with one’s own; the grip also in the sense that the
holder of the title loses the will to fight, economically, physically,
politically, for “his” factory and his control over it, to die if nec-
essary om its steps. And this evaporation of what we may term
the material substance of property—its visible and touchable
teality—affects not only the attitude of holders but also that of
the workmen and of the public in general. Dematerialized, de-
functionalized and absentee ownership does not impress and call
forth mora} allegiance as the vital form of property did, Even-
tually there will be nobody left who really cares to stand for it—
nobody within and nobody without the precincts of the big
concerns.18

Limited liability laws have produced the era of the huge, imper-
sonal corporations that have produced unquestioned material pros-
perity, but at the same time these laws are now producing something
very foreign to free enterprise. The giant socialist bureaucracy seems
less threatening to men who have grown up in the midst of impersonal
economic structures. They no longer are willing to fight for private
Property if that property is depersonalized. The drift into socialism

18. Joseph Schumpeter, Capitalism, Socialism, and Democracy (New York:
Harper, 1950), pp. 141-142.
