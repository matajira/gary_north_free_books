Are Postmillennialists Accomplices of the New Age Movement? 279

Romans in a series of historic defeats, beginning with the fall of
Jerusalem and the destruction of the temple.3?

The postmillennial Christian Reconstructionists unquestionably
teach that there will be a future era in which the gospel heals the souls
of large numbers of people, and these healed people will then work to
subdue the earth to the glory of God. But this is the offense, in Hunt's
eyes. This optimism about visible manifestations of God's kingdom
on earth, he says, is what the New Age movement is all about.

Tell it to Jeremy Rifkin.

Conclusion

While Dave Hunt denies calling postmillennial Christian Recon-
structionists New Agers, there can be no doubt that he hints at this
supposed relationship. His followers have picked up the accusation,
and I have letters in my files that prove this.

We should not make eschatology the test of being a “fellow
traveller” of the New Age movement. The New Age movement’s
three key doctrines are all anti-Christian: 1) reincarnation, 2) the
divinization of man, and 3) techniques of “higher consciousness” as a
means to divinization. There are optimistic New Agers, and there
are pessimistic New Agers. Jeremy Rifkin is the most influential
New Age social philosopher, and he is self-consciously pessimistic,
and he self-consciously targeted premillennialists as those Christians
closest to his worldview. I could make a far better case for Dave
Hunt as a secret New Ager than he has been able to make concern-
ing me, But either argument, and either innuendo, would be equally
wrong, both morally and factually. Orthodox Christianity is inher-
ently opposed to New Age doctrines. The early Christian creeds
were statements of faith drawn up when proto-New Age theologians
began to mislead Christian believers.

What I do argue here is that theonomic postmillennialism is
more consistent in its opposition to Rifkin’s version of New Age so-
cial theory than either premillennialism or amillennialism is. I argue
that the worldview of Dave Hunt leads to a shortened view of time, a
minimal view of Christians’ authority in history and their responsi-
bility in history. Dave Hunt is a self-conscious retreatist and ex-
pounder of the escape religion. Where views such as his pre-

32. David Chilton, The Days of Vengeance: An Exposition of the Book of Revelation
(Ft. Worth, Texas: Dominion Press, 1987).
