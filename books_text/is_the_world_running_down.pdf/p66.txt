30 IS THE WORLD RUNNING DOWN?

traces this line of reasoning back to a 1927 scholarly paper by
physicist Werner Heisenberg.” Jaki, a Benedictine scholar and a
philosophical realist, rejects such speculation as “careless,” but he ad-
mits that Neumann’s book “is still regarded as perhaps the deepest
and most rigorous probing into the mathematical foundations of
quantum mechanics.”

The physicists have done with physical cause and effect what
they have also done with God: first, they have denied that anyone can
know the process of physical causation in subatomic physics, and
therefore the concept of physical causation is irrelevant to subatomic
physics; second, they jump (and not randomly!) to the conclusion
that a cause-and-effect relationship does not exist in the realm of sub-
atomic physics. Substitute “God” for “cause and effect,” and you can
see the transition from official agnosticism to atheism.

If this view of quantum mechanics is assumed by a significant
minority of modern physicists, then what does the quantum
physicist do with subatomic reality when that reality has no God ta
observe it? Does it disappear? Gribbin struggles with the unsatisfy-
ing intellectual alternatives left to man. He begins with atheism as the
only scientifically acceptable presupposition: no outside observer exists.
“By definition, the universe is self-contained. It includes everything,
so there is no outside observer who notices the existence of the uni-
verse and thereby collapses its complex web of interacting alterna-
tive realities into one wave function.”# In other words, there is no
God who observes what is going on inside the closed box of the uni-
verse, precisely because it is a closed box.

Problem: Who gives reality to the universe by reducing the sta-
tistically possible wave functions to a single wave function? Who
makes the world real? Gribbin is willing to consider even Bishop
Berkeley’s eighteenth-century philosophy, solipsism: the idea that I
know that I exist, but that it is impossible to prove that anyone else
exists. “I would prefer even the solipsist argument, that there is only

  

one observer in the universe, myself, and that my observations are
the all-important factor that crystallizes reality out of the web of
quantum possibilities — but extreme solipsism is a deeply unsatisfac-
tory philosophy for someone whose own contribution to the world is

39. Idem.
40. Zbid., p. 363.
41, Gribbin, Schrédinger’s Cat, p. 236.
