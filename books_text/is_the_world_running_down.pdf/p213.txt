Conclusion 177

mation and enslavement of nature. To preach the triumph of the
curse is to deny the triumph of Christ at Calvary. To construct a
methodology of science in terms of the historically irrevocable effects
of the second law of thermodynamics is to ignore a fundamental doc-
trine of the Christian faith.

This is precisely what Rifkin has done: dented the definitive, progres-
sive, and final universe-transforming power of Christ's resurrection in history.
Creation Science has denied the definitive and progressive universe-
transforming power of Christ’s resurrection in history, although
affirming the post-historic power of the resurrection. This denial of
the power of the resurrection has rendered them intellectually in-
capable of successfully refuting Rifkin.

Rifkin’s Appeal

Rifkin’s appeal is to those Christians who have given up on the
future, who have abandoned personal responsibility for working to
bring the future under the covenantal reign of Christ. That appeal
has been sporadically successful. There is a war on by the funda-
mentalist apocalyptists to defend their cherished cultural pessimism
and their equally cherished apocalypticism. Rifkin is a self-conscious
humanistic accomplice of Christian “pessimillennialists” in this work
of cultural erosion. He has added his cry of despair to the growing
chorus of Christian despair concerning the diminishing effects of the
gospel in history. He looks for the steady-state stagnation of
socialism to delay the judgment; “pessimillennialists” look for the
physical return of Christ to speed up the judgment. They have all
turned their backs on the idea of historical progress, one of Chris-
tianity’s most valuable contributions to the West — in fact, one of the
crucial foundations in the creation of the West.

Rifkin has offered a worldview in the name of Western science
{the second law of thermodynamics) that breaks with modern
Western science, and especially Western technology. He has also
offered this worldview in the name of a new “Reformation Christian-
ity” that breaks with Christianity. Rifkin argues that the coming
changes in the world economy will produce a change in religion.
“The first Protestant Reformation will not outlive the economic age
it grew up with.” This argument is suspiciously similar to the old
Marxist doctrine that religion is simply part of the ideological

10. Rifkin, Emerging Order, p. xv.
