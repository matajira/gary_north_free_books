294 IS THE WORLD RUNNING DOWN?

“colonies” after 1870 by “W.E.B.” (William E. Blackstone), popular-
ized at the turn of the century by C. I. Scofield, and codified by the
founder of Dallas Seminary, Lewis Sperry Chafer, in his often pur-
chased but seldom read Systematic Theology (8 volumes). Schnittger
has accurately described reasons for the evangelical retreat from
society:

Fourthly, there was the spread (specially through J. N, Darby’s teaching
and its popularization in the Scofield Bible) of pretribulationism. This por-
trays the present evil world as being beyond improvement or redemption;
and predicts, instead, that it will deteriorate steadily until the coming of
Christ Who will set up His millennial reign on earth. If the world is getting
steadily worse, and if only Jesus at His coming will put it right, the argu-
ment runs, there seers no point in trying to reform it.”

That is precisely how the argument runs: away from social respon-
sibility. It is nice to hear a pretribulationist at last face up to it, and
try to overcome it, His spirit is willing, but his system is weak.

Abandoning the 1830 Faith

It is quite possible for pretribulational dispensationalists to
become hard-core Christian Reconstructionists. All they have to do
is ignore the obvious fact that their view concerning the continuing
New Testament validity of Old Testament law has rejected every-
thing ever written by the founders and promoters of pretribulational
dispensationalism. These activist dispensationalists have already become
theonomists.

They are also steadily abandoning the number-one official doc-
trine of Scofield theology: the steady defeat of the church. Writes
Schnittger: “The Scriptures teach that the entire church age is char-
acterized by the simultaneous development of both evil and right-
eousness, rather than a steadily eroding church brought about by the
ever-expanding encroachments of opposition from the outside and
apostasy within. . . . Apparently the church will not be raptured in
defeat and impotence but at the apex of its development and influ-
ence.” Yet he turns around and says on the next page that “regard-
less of the efforts of the church, the world will not be Christianized.
The direct intervention and judgment of God in Christ will be neces-

31. Schnittger, Christian Reconstruction, p. 3.
32. Ibid., p. 16.
