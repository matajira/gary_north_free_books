66 IS THE WORLD RUNNING DOWN?

as most probable, and in our disillusioned generation it is easy to be-
lieve. From evolution, so far as our present knowledge shows, no ul-
timately optimistic phitosophy can be validly inferred,”62

To overcome this inherent, inescapable pessimism of modern
Western science, Jeremy Rifkin offers what he says is new hope for
the future, but without adopting the Christian doctrines of creation,
redemption, and resurrection. The quality of such hope we will ex-
plore in detail in subsequent chapters.

In summary:

1. The second law of thermodynamics has become a major scien-
tific foundation of modern pessimism.
2. Most scientists fail to speak out on major philosophical issues.
3. Three major views of the world govern all interpretations:
power religion, escape religion, and dominion religion.
4. Pessimism concerning the future is common to the escape
religion,
5. Humanistic pessimism is acknowledged in principle but ig-
nored as much as possible by the power religion.
6. Pessimism is denied by the dominion religion.
7. The pessimists want to escape God’s judgment, either in history
(through the “Rapture”) or at the end of time (atheism, mysticism).
8. Those who write on the second Jaw seldom mention its impli-
cations,
9. The second law teaches that the universe is becoming more
random, wearing out.
10. The universe is therefore headed for extinction.
11. This has been taught by the physicists who pioneered the laws
of thermodynamics.
12, The debate over the second law of thermodynamics is impor-
tant because of its effect on man’s concept of time and final judgment,
13. Some physicists have created incoherent explanations of the
universe in order to escape the implications of the second law.
14. The only atheistic alternative to the linear history of entropy is
cyclical history.
15. Cyclical history was the oudack of the pagan ancient world.
16. Rebellious men do not want (o think about the end of time, for
it points to the final judgment.
17. If the universe dies, then man dies.
18. If man dies, there can be no meaning to the humanist’s world.
19. The humanist is today without hope.

62. Bertrand Russell, “Evolution,” in Religion and Science (New York:
Oxford University Press, [1935] 1972), p. a1.
