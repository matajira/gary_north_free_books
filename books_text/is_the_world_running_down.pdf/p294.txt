258 18 THE WORLD RUNNING DOWN?

Dave Hunt is adamant: only in the hearts of believers and (maybe)
inside the increasingly defenseless walls of a local church or local res-
cue mission. As he says, in response to an advertisement for my Bib-
lical Blueprints Series: “The Bible doesn’t teach us to build society
but instructs us to preach the gospel, for one’s citizenship is in
Heaven (Col. 3:2).”2

It seems to me that he could have strengthened his case that we are
citizens of only one “country” by citing a modern translation of Philip-
pians 3:20. But this would only have deferred the question: Why can’t
Christians be citizens of two countries? After all, they are in the world
physically, yet not of the world spiritually: John 17;14-16. Christians
are, as Hunt (and all Christians) would insist, required to obey na-
tional laws, but also to obey the Bible. To be required to obey two sets
of laws is to raise the question of dual citizenship.

Hunt's dispensationalist gospel is a gospel of the heart only, Jesus
saves hearts only; somehow, His gospel is not powerful enough to re-
store to biblical standards the institutions that He designed for man-
kind’s benefit, but that have been corrupted by sin. Hunt’s view of
the gospel is that Jesus can somehow save sinners without having
their salvation affect the world around them. This, in fact, is the
heart, mind, and soul of the pessimillennialists’ “gospel”: “Heal
souls, not institutions.” Prison evangelist (and former Nixon aide)
Charles Colson has said it best (or worst, depending on your theol-
ogy): “The real trouble is that we Christians are not willing to accept
the gospel for what it is. It doesn’t tell us how to save anything but
our souls.”3

Hunt separates the preaching of the gospel from the concerns of
society. He separates heavenly citizenship from earthly citizenship.
In short, he has reinterpreted the Great Commission of Jesus Christ
to His followers: “All power is given unto me in heaven but none in
earth.” (A similar other-worldly view of Christ’s authority is held by
amillennialists.)* Christ's earthly power can only be manifested
when He returns physically to set of a top-down bureaucratic king-
dom in which Christians will be responsible for following the direct

2. Dave Hunt, CIB Bulleiin (Feb. 1987), fourth page.

3. Cited in Omega-Letter (March 1987), p. 11.

4, “There is no room for optimism: towards the end, in the camps of the satanic
and the anti-Christ, culture will sicken, and the Church will yearn to be delivered
from its distress.” H. de Jongste and J. M. van Krimpen, The Bible and the Life of the
Christian (Philadelphia: Presbyterian & Reformed, 1968), p. 27; cited by R. J. Rush-
doony, The Institutes of Biblical Law (Nutley, New Jersey: Craig Press, 1973), p. 14n.
