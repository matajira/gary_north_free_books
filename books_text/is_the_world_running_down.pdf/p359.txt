Time for a Change: Rifkin's “New, Improved” Worldview 323

support from the few remaining shocked troops of the Nirvana
Liberation Front, the mind-blown visionaries of social transforma-
tion who never quite recovered psychologically from the startling
news that a jury in a U.S. courtroom actually did free Angela
Davis. There are not many of them left.

Like charismatics, Rifkin wants revelation, but clearly not
Christian revelation. He wants to wait for the kind of higher con-
sciousness that is sought after by New Age avatars: “In an empa-
thetic time world, the mind places less emphasis on manipulative
knowledge and more emphasis on revelatory knowledge. Manipula-
tive knowledge gives us control but at the expense of wisdom. We be-
come skilled craftsmen learning how to reshape surfaces without
gaining any deep understanding of interiors. Manipulative knowl-
edge is always exercised at the outer margins of reality. Revelatory
knowledge is always experienced in the depths. . . . Revelation is
experienced by a giving over, a reaching out. The essential why of
things becomes revealed to us when we choose to surrender to
them... ."#

Watch out, Mr. Rifkin. The things to which you want to surren-
der may turn out to be more than impersonal aspects of the universe.

Conclusion

When I decided to publish this book, I asked my cover designer
to paint a split picture. On the left was an alarm clock with its back
open and a broken coiled spring hanging out. It was sitting on a
table in a dingy room with worn-out wallpaper. The clock’s face
would read five minutes to twelve. On the right hand side of the
cover was a high tech digital clock on a table in a freshly wallpapered
room. Its face would read twelve fifteen. The artist wied twice, but
the designs never conveyed my idea clearly, the idea of cosmic resto-
ration. Had I known of Rifkin’s decision to write Time Wars, I would
have stuck with the old design and asked the artist to try once again.
Rifkin literally followed my book cover’s original design: from a
world doomed by entropy, running out of time, to a high tech world
that has been visibly restored.

44, My favorite recollection of the “free Angela Davis” movement is a political
cartoon of a store filled floor to ceiling with posters: “Free Angela Davis!” and “Free
Sister Angela!” The store's owner is on the telephone, eyes bugging out, “They just
did what?”

45, Ibid., p. 209,
