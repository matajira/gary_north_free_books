42 IS THE WORLD RUNNING DOWN?

What distinguishes biblical dominion religion from satanic power re-
ligion is ethics. Is the person who seeks power doing so primarily for
the glory of God, and secondarily for himself, and only to the extent
that he is Ged’s lawful and covenantally faithful representative? If
so, he will act in terms of God’s ethical standards and in terms of a
profession of faith in God. The church has recognized this two-fold
requirement historically, and has established a dual requirement for
membership; profession of faith and a godly life.

In contrast, power religion is a religion of autonomy. It affirms that
“My power and the might of mine hand hath gotten me this wealth”
(Deut. 8:17). It seeks power or wealth in order to make credible this
very claim.

Wealth and power are aspects of both religions. Wealth and
power are covenantal manifestations of the success of rival religious
views. This is why God warns His people not to believe that their au-
tonomous actions gained them their blessings; “But thou shalt re-
member ihe Lorp thy God: for it is he that giveth thee power to get
wealth, that he may establish his covenant which he sware unto thy
fathers, as it is this day” (Deut. 8:18). It must be recognized that
God’s opponents also want visible confirmation of the validity of
their covenant with death, but God warns them that “the wealth of
the sinner is laid up for the just” (Prov, 13:22b). The entry of the
Hebrews into Canaan was supposed to remind them of this fact: the
Canaanites had built homes and vineyards to no avail; their ene-
mies, the Hebrews, inherited them (Joshua 24:13).

Those who believe in power religion have refused to see that long-
term wealth in any society is the product of ethical conformity to God's
law, They have sought the blessings of God’s covenant while denying
the validity and eternally binding ethical standards of that covenant. In
short, they have confused the fruits of Christianity with the roots. They
have attempted to chop away the roots but preserve the fruits.

2. Escapist Religion

This is the second great tradition of anti-Christian religion. See-
ing that the exercise of autonomous power is a snare and a delusion,
the proponents of escapist religion have sought to insulate them-
selves from the general culture—a culture maintained by power.
They have fled the responsibilities of worldwide dominion, or even
regional dominion, in the hope that God will excuse them from the
general dominion covenant.
