Mysticism us. Christianity 141

Until modern times, no government in the West has ever gone so
far in its disdain for all its citizens. The prevailing “ecological” view
appears to be based on the premise that Man is an excrescence in the
Universe. This is a mirror-reversal of the traditional Christian belief
that Man was created in the image of God for a special purpose, and
was given dominion on earth over ail other creatures and forms of life.

The governmentally sponsored “scientific” position has led to
amazing developments, A great dam project was first delayed and
then ruled out of existence because, the scientists said, the snail-
darter—a small, nearly unknown fish—would be rendered extinct
by the creation of the dam. Since then, snail-darters have been dis-
covered in embarrassing numbers in various locations, but so far as I
know the dam remains unbuilt.

Arguments against offshore drilling for oil center on presumed
ecological damage to the sea and the shore. But fish thrive on oil
spills, for oil is a natural substance. The ocean tides wash away oil
residues on shore. Some birds have, it is true, suffered from oil
spills. But so far, not to the extent that the seabird population has
been significantly affected. On the other hand, the loss of the oil ob-
tainable from our shores has cost the American people and industry
untold billions in jobs, products and quality of human life.

Some years back a debate arose over DDT. This insecticide,
made from coal-tar derivatives, is credited with eradicating malaria
in many parts of the world and with preventing the spread of typhus
at the close of World War II, Since typhus has killed more people
than battles, DDT was responsible for saving millions of lives. But
some said that DDT was deleterious because it builds up in the
system and resulted in creating thinner egg-shells in some species of
birds. This, it was claimed, would reduce the bird population to
dangerous levels.

Rachel Carson, a writer on biological subjects, afflicted with ter-
minal cancer, read such arguments and was overcome with dread.
In 1962 she wrote Silent Spring, which depicted a grim, lifeless world
without birds. Silent Spring helped spawn an environmental move-
ment that saw Man as a menace to all living things. A campaign was
launched against DDT. Despite a scientific report that said DDT
was beneficial, the U.S. Court of Appeals, in response to environ-
mental arguments, ordered an end to its use. Since then, the infesta~
tions and damage created by insects upon crops and humans alike
have spurted.
