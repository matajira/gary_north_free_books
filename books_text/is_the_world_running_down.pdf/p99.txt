The Pessimism of the Scientists 63

away into radiation, they maintain that, somewhere out in the remote
depths of space, this radiation may be reconsolidating itself again into mat-
ter. A new heaven and a new earth may, they suggest, be in process of being
built, not out of the ashes of the ald, but out of the radiation set free by the
combustion of the old. In this way they advocate what may be described as
a cyclic universe; while it dies in one place the products of its death are busy
producing new life in others.

This concept of a cyclic universe is entirely at variance with the well-
established principle of the second law of thermodynamics, which teaches
that entropy must for ever increase, and that cyclic universes are impossible
in the same way, and for much the same reason, as perpetual motion
machines are impossible. That this law may fail under astronomical condi-
tions of which we have no knowledge is certainly conceivable, although I
imagine the majority of serious scientists consider it very improbable,
There is of course no denying that the concept of a cyclic universe is far the
more popular of the two. Most men find the final dissolution of the universe
as distasteful a thought as the dissolution of their own personality, and
man’s strivings after personal immortality have their macroscopic counter-
part in these more sophisticated strivings after an imperishable universe.?”

Re-read that last sentence. It comes to the heart of the matter
concerning the fate of matter. The death of the universe is the psychological
equivalent of the death of God, for it points to the death of man, humanism’s
god. Man’s environment will have long since disappeared. Nothing
will carry on man’s work, man’s story, or man’s meaning. Man will
not be the judge of himself and the universe around him, The uni-
verse dies, and man must die with it. Man, the king of humanism, is
in fact nothing more than a cosmic parasite, and his host is dying. This
is bad news for all those men whose dream of autonomy from God
has led them to proclaim an autonomous universe, closed to God.

God alone could sustain the dreams of man by regenerating the
universe, even as He regenerates man. But regeneration points to
the final judgment, and autonomous man above all wants to avoid
the eternal judgment. Better the ultimate despair of the heat death of
the universe or the pseudo-hope of a cyclical universe which will de-
stroy today’s man, but which will open the possibility of eternally
recurring cycles of Big Bangs, thermodynamic dissipation, con-
tractions, and Big Bangs. Better eternal cycles than an eternity in
hell, says modern man. And for God-denying, God-defying men,
this conclusion is correct. It is not an available option, but it cer-

57, Jeans, The Mysterious Universe, pp. 179-81.
