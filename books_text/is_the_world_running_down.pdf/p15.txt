Preface xv

shown why and how six-day creationism leads to a fundamentally
unique worldview that encompasses things other than academic
topics like historical geology and biology.6 To win the battle with
Darwinism, which is above all a comprehensive worldview justifying
comprehensive power, six-day creationists must believe that the stakes
are far larger than mere laboratory experiments or one-evening
debates. Creation scientists must demonstrate to Christians that six-
day creationism really makes a difference in every area of life.

Only a handful of Christians are ever willing to sacrifice their
reputations and present associations for the sake of some rarified
theological doctrine. The Scientific Creation movement has not yet
persuaded Christians that its doctrinal position is anything more
than a rarified theological opinion developed by ivory tower special-
ists in the natural sciences. The only people who seem to understand
how much of a threat the six-day creation doctrine is to all of modern
secular humanism are the best-informed secular humanists on one
side and the Christian Reconstructionists on the other. The secular
humanists reject the conclusions of the Scientific Creationists, and
argue that the creationists’ official methodology (the appeal to scien-
tific neutrality) is a charade, while the Christian Reconstructionists
accept the creationists’ conclusions but reject their methodology as
self-deception rather than a charade,

Saul’s Armor

The Scientific Creationists’ case against Darwinism has been
narrowly focused and highly technical rather than a no-holds-barred
attack: theology, philosophy, ethics, psychology, economics, law,
and history. There is no single volume that has come out of Scientific
Creationism that summarizes the nature of the war between cre-
ationism and evolutionism in the main areas of modern thought.
There is not even a path-breaking scholarly monograph in any one
of these outside fields, This is not because there are not intellectually
competent people within the Creation Science movement. It is
because of the self-imposed methodological armor that Scientific

6. If six-day creationism could be used to locate oil and mineral deposits less ex-
pensively than the methodology of evolutionism does, we would begin to see the
abandonment of evolutionism, and also sce last ditch cfforts of university evolutionists
to explain the creationists’ success in terms of some other evolutionist theory. What we
need is for evolutionism to start drilling more dry holes than we do. Tf nothing else, we
could at least afford to fund a lot more creationist research projects
