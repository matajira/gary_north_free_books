Resurrection and Reconstruction 165

spiritual and cultural progress reshaped the West. For the first time
in human history, men were given a full-blown idea of progress,
which was above all a doctrine of ethical progress. This vision was
secularized by the philosophes of the Enlightenment, but that secular-
ized version of progress is rapidly fading from the humanist West.3
Belief in the universality of entropy (meaning inevitable decay) is only
one of the causes of this growing pessimism, but it is a powerful one.

In the twentieth century, “pessimillennialism” — premillennialism
and amillennialism—have been the dominant eschatologies. Those
who hold such views have self-consciously rejected the idea of visi-
ble, institutional, social progress. They insist that the Bible does not
teach such a hope with respect to the world prior to Christ’s per-
sonal, physical return in judgment.

I realize that there are premillennialists who will take offense at
this statement. They will cite their obligations under Luke 19:13:
“Occupy till I come.” (The original Greek actually says, “do busi-
ness,” not “occupy”: New American Standard Bible; also translated
as, “trade with this”: New English Bible.) But the leaders of the éradi-
tional premillennial movement are quite self-conscious about their
eschatology, and we need to take them seriously as spokesmen. For
example, John Walvoord, author of many books on eschatology, and
the long-time president of Dallas Theological Seminary, the premier
dispensational institution, has not minced any words in this regard.
In an interview with Christianity Today (Feb. 6, 1987), Kenneth Kant-
zer asked:

Kantzer: For all of you who are not postmils, is it worth your efforts to
improve the physical, social, political situation on earth?

Walvoord: The answer is yes and no. We know that our efforts to make
society Christianized is futile because the Bible doesn’t teach it. On the
other hand, the Bible certainly doesn’t teach that we should be indifferent to
injustice and famine and to all sorts of things chat are wrong in our current
civilization. Even though we know our efforts aren’t going to bring a
utopia, we should do what we can to have honest government and moral
laws. It’s very difficult from Scripture to advocate massive social improve-
ment efforts, because certainly Paul didn’t start any, and neither did Peter.
They assumed that civilization as a whole is hopeless and subject to God’s
judgment (pp. 5-1, 6-1).

3. Robert A. Nisbet, History of the Idea of Progress (New York: Basic Books, 1980),
ch. 9.
