Introduction 3

life, and they usually are unexpected. Not always, however, God
promised Israel the miracle of genetic near-perfection: no miscar-
riages of man or beast in Israel, just so long as the people were cove-
nantally faithful to God (Ex. 23:25-26). They knew in advance what
was possible, but they did not obey, and they did not receive the
blessing. Neither did their animals.

This book deals with miracles. It also deals with run-of-the-mill
activities that involve the inevitable wear and tear of life. In the Book
of Exodus, we find a notable verse that supports the thesis of this
book, Exodus 3:2: “And the angel of the Lorp appeared unto him
[Moses] in a flame of fire out of the midst of a bush: and he looked,
and, behold, the bush burned with fire, and the bush was not con-
sumed.” Why wasn’t it consumed? Because God sustained it. But it
was the oddness of a burning bush that was not being consumed that
caught Moses’ attention in the first place (Ex. 3:3). So the overcom-
ing of entropy had its part to play in God’s plan for the ages.

A similar comment applies to: 1) the manna of the wilderness
which fed the Israelites for almost four decades; 2) the daily refilling
of the oil pot of the widow of Zarephath, who fed Elijah for over
three years; 3) the widow who poured oil out of a single small pot
that filled a roomful of large containers; 4) Jesus’ turning of water
into wine; and 5) His feeding of thousands with a few fish and loaves
of bread. Most notably, it applies to every account of resurrection
from the dead, especially the resurrection of Jesus Christ.

If I were to come to you and insist that as a Christian, you have
an intellectual responsibility to seek to identify the constant, univer-
sal, uniformitarian “natural law” that made any or all of these mira-
cles possible, you would regard me as a fool. These were miracles;
therefore, the normal cause-and-effect relationships of conventional
physical science did not govern them.? This biblically reasonable
response is precisely what has long angered humanistic scientists.
They deny biblical miracles.

The idea that these miracles ever took place was rejected by all
nineteenth-century Darwinian scientists. Very few scientists have
ever affirmed faith in God’s miracles, and never in their scientific
papers does the subject of miracles come up. Miracles were under-

3. Sometimes I wonder if we make too much of a fuss trying to find the “mecha-
nisms” of the Genesis Flood, or how Noah could have fed the animals, or haw he
squeezed them into the ark.
