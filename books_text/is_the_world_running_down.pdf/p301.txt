Are Postmillennialists Accomplices of the New Age Movement? 265

But generally speaking, the premillennialist is more oriented toward
helping those who have been hurt by the system than by addressing the sys-
tematic evil, while the postmillennialist believes the system can be sanc-
tified. That’s the basic difference with regard to our relationship to society
(pp. 6-1, 7-D.

When dispensationalists are called pessimists by postmillennial-
ists —as we postmillennialists unquestionably do call them — they react
negatively. This is evidence of my contention that everyone recognizes
the inhibiting effects of pessimism. People do not like being called pessi-
mists. Walvoord is no exception. But his defense is most revealing:

Walvoord: Well, I personally object to the idea that premillennialism is
pessimistic. We are simply realistic in believing that man cannot change the
world. Only God can (il-I).

So, he objects to being called pessimistic. Well, what does he ex-
pect? Is this man totally self-deceived? Doesn't he read his own
Seminary’s scholarly journal, Brbliotheca Sacra? Listen to Lehman
Strauss’ dispensational assessment of today’s world, in an article ap-
propriately titled, “Our Only Hope”:

We are witnessing in this twentieth century the collapse of civilization.
It is obvious that we are advancing toward the end of the age. Science can
offer no hope for the future blessing and security of humanity, but instead it
has produced devastating and deadly results which threaten to lead us to-
ward a new dark age. The frightful uprisings among races, the almost
unbelievable conquests of Communism, and the growing antireligious phi-
losophy throughout the world, all spell out the fact that doom is certain. I
can see no bright prospects, through the efforts of man, for the earth and its
inhabitants.

Pessimism, thy name is Dallas Theological Seminary! Maybe
Dallas Seminary did not invent fundamentalist pessimism, but it
surely is the U.S. wholesale distributor.

Walvoord then insists: “We are simply realistic in believing that
man cannot change the world. Only God can.” Man cannot change
the world? What in the world does this mean? That man is a robot?
That God does everything, for good and evil? Walvoord obviously

13. Lehman Strauss, “Our Only Hope,” Bibliotheca Sacra, Vol. 120 (April/June
1963), p. 154.
