Preface xiii

limiting themselves to hobbling when they could sprint; they use
only humanism’s artificial legs, “for the sake of argument.” Van Til
writes:

A deductive argument as such leads only from one spot in the universe
to another spot in the universe. So also an inductive argument as such can
never lead beyond the universe. In either case there is no more than an infi-
nite regression, In both cases it is possible for the smart little girl 10 ask, “If
God made the universe, who made God?” and no answer is forthcoming.
This answer is, for instance, a favorite reply of the atheist debater, Clarence
Darrow. But if it be said to such opponents of Christianity that, unless there
were an absolute God their own questions and doubts would have no mean-
ing at all, there is no argument in return, There lies the issue. It is the firm
conviction of every epistemologically self-conscious Christian that no
human being can utter a single syllable, whether in negation or in affirma-
tion, unless it were for God's existence,>

Half a dozen of the most forensically skilled of the Scientific
Creationists have been tactically successful im many brief public
debates with Darwinists, but only because of the weak scientific case
for Darwinism and the weak debaters who have foolishly agreed to
show up. They have been public amputation sessions, not conver-
sion sessions. These one-night successes have strengthened the self-
confidence of Bible-believing Christians, and they have recruited a
few science students and even fewer faculty members; nevertheless,
Scientific Creationists and their associates have not yet begun to
offer a systematic, comprehensive alternative worldview to the domi-
nant Darwinian paradigm. They have failed to recognize clearly
that the heart of Darwinism’s hold on the thinking of the modern
world is not the evolutionists’ scientific case, which has been
remarkably weak from the beginning, but rather the very worldview
of Darwinism, for it conforms to the primary long-term goal of au-
tonomous man: to escape from God's judgments, historical and final.

By narrowing the focus of their chosen intellectual battleground,
Scientific Creationists have not yet successfully attacked the soft
underbelly of Darwinism: Aistortcal despair. Scientific Creationists, by
proclaiming the sovereignty of the entropy process, have also im-
mersed their own worldview in historical despair, They can offer
Darwinists and their followers only an escape from history: Jesus’

5. Cornelius Van Til, A Survey of Christian Epistemology, Vol. 2 of In Defense of
Biblical Christianity (Phillipsburg, New Jersey: Presbyterian and Reformed Pub.
Co., {2932] 1969), p. il.
