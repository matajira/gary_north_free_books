The Entropy Debate within Humanistic Science 101

“There is an acknowledgement that some form of expropriation is
always necessary. All things desire to live, and it is a law of nature
that for something to live, something else must die.””"

This creates an ethical dilemma, the kind of ethical dilemma that
the pantheist physician Albert Schweitzer faced: How can we affirm
life, if all life survives by imposing death? Can we set forth guide-
lines of ethical living in such a universe? Rifkin does not even try.
He hides behind the vague undefined words, “too much”: “But it is
also true that too much expropriation can result in destroying the
very life support systems we rely on for our future survival.”2! Whose
life support systems? Ours. Who must survive? We must. We are back
to the original presupposition of all humanism: man must prevail.

The consistent entropist should call for an end to all life. Only
inconsistency prevents this. But if the social entropist is unwilling to
be consistent and call a halt to the energy drain associated with all
living things, then why should he expect residents of the West to pay
any attention to him and cut back on their lifestyles? After all, it is
only a matter of degree, of comparative rates of “energy addiction.”
The lifestyle of the urban Westerner is only relatively more guilty
before the god of entropy than the lifestyle of the savage, or for that
matter, of the amoeba.

The philosophy of social entropy is innately a philosophy of death,
despite the fact that it officially affirms all life. It worships a physical
“law of entropy” as its governing (uniformitarian) principle. It wor-
ships an impersonal god of destruction. But this is equally true of
scientific, Darwinian entropy. Its uniformitarian principle is also an
impersonal consuming god. God is correct: “. . . all those who hate
me love death” (Prov. 8:36b).

Conclusion

John Maddox has come face to face with a religious impulse that
he does not understand. It makes no sense to him. His old fashioned
secular humanism blinds him to the reasons for the intellectual
power and appeal of a rival form of humanism. He is dealing with
people who virtually worship entropy the way that savages worship a
god of death. They want to sacrifice human progress on entropy’s
altar, in order to avoid a direct confrontation with the entropy pro-

20. Ihid., p. 95.
Ql. ddem.
