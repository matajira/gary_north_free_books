318 IS THE WORLD RUNNING DOWN?

trolled, doing only a few simple tasks.» As time goes on, production
increases and markets expand. Production becomes far more com-
plex, equipment becomes more expensive, and workers must receive
greater training. With greater skills comes greater responsibility, a
fundamental biblical principle (Luke 12:47-48). So, workers are
steadily given more freedom to make decisions as companies are
forced by growth and competition to decentralize the decision-mak-
ing process. The modern “knowledge society” is heading for even
greater decentralization, with more flexible work schedules.

The personal and socia} goal is self-discipline. This usually re-
quires external social and institutional discipline in a pagan, rebel-
lious society. As time goes on, people mature under this external dis-
cipline and become more responsive to other people’s economic re-
quests, as registered on a free, competitive market. Just as children
need direct external discipline in the early years, so do God-hating
societies require the discipline of long hours of work and tight factory
schedules. Once the clocks are internalized, and the Puritan work
ethic takes over a person’s life, the external restraints become in-
hibiting and counter-productive,

Self-discipline under God is inescapably self-discipline under the
constraints of time. We are temporal creatures. But self-discipline
under time does not mean we are slaves to clocks, any more than
self-government under law means that we are slaves to law. We are
masters of time because we respect time as a created thing. God is sov-
ereign, not time. Yes, if we lose this view of God, time can become a
slave master. Anything that man substitutes for God becomes his
slave master. But why single out time as the great villain? Why tar-
get the computer for vengeance? Only because Rifkin is an intellec-
tual Luddite. Like the saboteurs of old, who tossed wooden shoes
into machinery,! Rifkin tosses his wooden prose into everything
technological that has become a tool of visible progress in the West.

Computopia
Rifkin hates “computopia.” No wonder: he describes it in theo-
logical terms—the triumph of power-seeking man, Computopia is

30. Adam Smith, Wealth of Nations (1776), ch. 1,

31. Sabotage comes from the French word for shoe. A sabot is a wooden shoe, ac-
cording to the first definition given in the Oxford English Dictionary (1971). Odd fat
the OED gives considerable space to various definitions of sabot, but does not include
the words sabetage and saboteur. Strange.

 
