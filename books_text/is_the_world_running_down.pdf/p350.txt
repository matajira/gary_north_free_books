314 IS THE WORLD RUNNING DOWN?

producing capital.!® Traditions change for many reasons, but half
second or quarter-second improvements in time measurement surely
are not the primary locomotives of social change, except when they
lead to some major improvement in industrial production. This hap-
pened in industrial management studies at the turn of the twentieth
century, as Rifkin knows,” but he cannot find anything comparable
to these changes that have been produced by quartz watches. Yet
Rifkin subtitles his book, The Primary Conflict in Human History.

The Supposed Evils of Saving Time

As always, Rifkin is the social critic, He attacks the late-
nineteenth century time-management studies conducted by
Frederick W, Taylor. “Taylor believed that the key to making a
worker more efficient was to strip him of any capacity to make deci-
sions regarding the conception and execution of his task. In the new
scientifically managed factory, the worker’s mind was severed from
his body and handed over to the management, The worker became
an automaton, no different from the machines he interacted with,
his humanity left outside the factory gate.”2!

This, as you might expect by now, is sheer balderdash. Men are
not machines, and if you treat them as machines, you will reduce
their productivity. There is always an cconomic incentive in the free
market to treat people well, for this is what pays. This is the way that
Wayne Alderson took a nearly bankrupt steel fabrication plant and
made it profitable within two years: treating people as people. His
“Value of the Person” program became an industry objective because
it produced profitable results.22 Where managers and owners are
allowed to take a portion of the workers’ increases in productivity,
they will have an economic incentive to improve relations within the
factory or office. Only where they are not allowed to appropriate a
portion of any increased production, as under socialism, wilt this in-
centive be absent.

Rifkin is a propagandist. He either doesn’t know what he is talk-

19, Edmund 8. Morgan, The Puritan Family: Religion and Domestic Relations in
Seventeenth-Century New England (new ed.; New York: Harper & Row, 1966), pp.
58-59.

20, Time Wars, pp. 106-9.

21. Tbid., p. 109.

22. R.G. Sproul, Stronger Than Steet: The Wayne Alderson Story (New York: Harper
& Row, 1980).
