334

punctuated, 77
sunshine, 96, 192-93, 205
evolutionism (see Darwinism)

facts, 187, 208-9
failure of nerve, 137, 181-82
fairness, 215-16
faithfulness, 45
fact, 2n, 36, 187, 195
Fall
entropy, 124-28
power af, ix
thermodynamics &, xi
family, 157, 226
fear, 244
feedback, 158-59 (see negative feed-
back, positive feedback)
Ferris, Timothy, 13, 19
fig tree, 289-90, 290
final judgment
all religious, 113
entropy, 127
escape from, xiii, 54-55, 62-64,
131, 159
evolutionism, 112
march toward, 127
stalemate vs., 108
versions, 199
Finney, Charles, 229
Flood, xi, 207-8
fluctuations, 57
flux, 60, 195
Ford, Henry, 317
France, 226
Franklin, Benjamin, 308, 322
French Revolution, 226, 232-33, 249
“full bucket” analogy, 151-53
fundamentalism, 180-81, 212-13, 231,
272 (see also dispensationalism,
escape religion, pietism)
future-orientation, 98

gap theory, 270

garden, 82, 124-27

gas, In, 50, 56

gasoline, 128

General Motors, 137, 317

IS THE WORLD RUNNING DOWN?

Genesis Flood, xi, 269-70
Genesis Institute, 70
genetic engineering, 84, 204
Georgescu-Roegen, Nicholas, 68n
ghetto, 303
Gish, Duane, 73
gluttony, 302
gnosticism, 43-45
God
blessings of, 45, 128, 163
center, 240-41
character of, 199
closed box vs., 30, 92
curse, xxii-xifi, xxi, 37, 159,
170, 241-42
death of, 63, 107
delegates authority, 216
equal time for?, 195, 209-10
evaluator, 71
existence of, 60
glorious, 152-153
Holy Spirit, 132, 186, 298-300
intervention by, 33
Jesus as, 133
judgment, xiii, xxiii, 37, 49,
108, 127, 149, 282
karma vs., 169
kingdom of, 10, 111, 155-75, 260, 262
law of, 43, 97, 146, 184, 291-92, 295
love of, 219
Newton on, 32
observer, 30
one-many problem, 37
power of, 257-58, 263
redirects energy?, 1-2
regeneration, 63
sanctuary, 150
sovereignty of, 159
speaking in the name of, 216
this world’s, 107
trinity, 225
unchanging, 203-4
uniformitarianism of, 199
union with, 133
voice of, 216
whole counsel of, 239
wrath of, 219
see also covenant, ethics, law
