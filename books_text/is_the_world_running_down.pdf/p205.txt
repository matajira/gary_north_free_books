Resurrection and Reconstruction 169

resources all the time; this discovery process has been accelerating
for two centuries.” Per capita wealth continues to rise, exactly as God
promised the Hebrews.

What we are seeing in economic life is a reduction in the effects
of many degradative processes. This indicates that men can over-
come these effects through covenantal faithfulness to God, which
means obedience to God’s economic laws.® Western capitalism
stands as a historic testimony against Eastern mysticism, and also
against any social theory which asserts the dominion of entropy over
covenant-keeping man.

In short, God’s grace to covenant-keeping societies comes
through their obedience to revealed covenant law (Deut. 28:1-14). It
is not simply that God promises a miracle now and then, in order to
break “the steady downward pressure of entropy.” It is that God
promises to lift the cursed effects of entropy in response to covenan-
tal faithfulness. The “miracle” of zero miscarriages —a genetic reduc-
tion in entropy—is to become the continuing standard for the whole
society, God promised Israel (Ex. 23:26).

God is not some Eastern karmic force. For every benefit, He
does not impose a loss. Two people can trade voluntarily, and both
people involved can come out winners. There is no Eastern “balance
of karma,” any more than there is some sort of autonomous “balance
of nature.” We live in a world of covenantal law. The environment
responds positively or negatively to mankind in terms of a society’s covenantal
faithfulness? God promised that the hornets would go before the
Hebrews and drive out the perverse Canaanites, God sent plagues
on Israel’s enemies from time to time, and in response to the evil of
the Hebrews, He sometimes sent plagues on them.

What this should teach us is that the so-called “natural law” is
covenantal in the same way that miracles are. Natural law is governed
by God; better put, natural law is in fact the very process of God’s
government over nature. God responds in His ordinary government
of the universe to covenant-keepers and covenant-breakers: their

7. Julian Simon, The Ultimate Resource (Princeton: Princeton University Press,
1981), ch, 3, For a somewhat revised application of Simon’s thesis, see Gary North,
Moses and Pharaoh: Dominion Religion vs, Power Religion (Tyler, Texas: Institute for
Ghristian Economics, 1985), pp. 328-33.

8. Gary North, The Sinai Strategy: Economics and the Ten Commandments (Tyler,
Texas: Institute for Christian Economics, 1986).

9. Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas: Insti-
tute for Christian Economics, 1987).
