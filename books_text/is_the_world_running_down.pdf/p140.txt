5
CHRISTIANITY VS. SOCIAL ENTROPY

Man, being in rebellion against law and causality as infringements on his
becoming and on his ultimacy in process [evolution—G.N.], must
therefore be also in rebellion against clock time as the epitome of his slav-
ery. Glock time is a bondage whose ticking always and monotonously
moves in terms of an eternal decree external to and transcending man.
Man's rebellion against eternity therefore must be followed by a rebellion
against time. Time always beats to and echoes the stroke of eternity. . . .
Thus the rejection of God inevitably requires as its logical concomitant the
rejection of time, Man seeks either to arrest time, as from the days of the
Tower of Babel to Hitler's “thousand year” Reich and the United
Nations, by his own decree, or to flee from time by the clock, from history,
into mysticism, time lived. In either case, man as god ts saying, “Time
shall be no more.”
R. J. Rushdoony'

All parties to the debate understand the appeal of optimism. The
Scientific Creationists, because they are Christians, offer the hope of
individual salvation to man. The premillennialists implicitly (though
never explicitly) offer the hope of partially overcoming entropy dur-
ing Christ’s millennial reign. The amillennialists offer hope at least
beyond the grave, though not before,? Postmillennialists alone are.
the Christian theologians of hope for history.

Rifkin has no true eschatological hope to offer. He is a believer in
evolution,’ and he is also a believer in the entropy law, He claims

1. R. J. Rushdoony, The Mythology of Science (Nutley, New Jersey: Craig Press,
1967), pp. 77-78.

2. Gary North, Deminion and Common Grace: The Biblical Basis of Progress (Tyler,
Texas: Institute for Christian Economics, 1987), ch. 4.

3. “,,. it took nearly three billion years of natural evolution to create this tre-
mendous stock of energy.” The Emerging Order, p. 45.

104
