124 1S THE WORLD RUNNING DOWN?

the promised extension of life spans in the future? After all, Isaiah
taught that “There shal! be no more thence an infant of days, nor an
old man that hath not filled his days: for the child shall die an hun-
dred years old; but the sinner being an hundred years old shall be
accursed” (Isa. 65:20). Note: sinners are still in the picture. He was
not speaking of the world after the final judgment.

Entropy in the Garden of Eden

Animals died in the garden. This was not a curse as such. Man
exercised dominion over curse-free nature before the Fall. The bless-
ing of God was seen in the subordination of the world to man’s
dominion. Kline comments: “Similarly, the curse on man consists in
the reverse of this relationship —not in the mere presence of things
like death but in man’s falling victim to them. . . . When the sub-
human realm is consecrated to man, a state of beatitude exists; when
man is made subservient to or victim of the sub-human, a state of
curse exists.”

The Bible does not require us, therefore, to think of the character and
working of man’s natural environment before the Fall as radically different
than is presently the case, To be sure, the garden God prepared as man’s
immediate dwelling was a place eminently expressive of divine goodness
and favor, Nevertheless, the elements that could be turned against man
were already there in nature. Man’s state of blessedness is thus seen to be
primarily a matter of God’s providential authority over creation, control-
ling and directing every circumstance so that everything works together for
man’s good and nothing transpires for his hurt or the frustration of his
efforts. God gives his angels charge over the one who stands in his favor lest
he should dash his foot against a stone (Ps. 91:12). Blessing consists not in
the absence of the potentially harmful stone, but in, the presence of God’s
providential care over the foot, Adam’s world before the Fall was not a
world without stones, thorns, dark watery depths, or death. But it was a
world where the angels of God were given a charge over man to protect his
every step and to prosper all the labor of his hand.”

The resurrection of Christ in principle put an end to cursed en-
tropy, just as it put an end in principle to Satan. I say cursed entropy
because entropy—the normal, “natural” transition toward physical
randomness — existed prior to the Fall of man, just as the death of

16. Meredith G. Kline, Kingdom Prologue, 3 vols. (By the Author, 1981), I, p, 80.
17. Ibid., p. Bl.
