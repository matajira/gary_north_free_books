The Disastrous Quest for Scientific Common Ground 197

all scientists must build the foundation of science on the inescapable,
universal, irreversible law of entropy. Entropy is inescapable; it is
the same yesterday, today, and tomorrow (well, not quite the same,
as we shall see, but “usually” it is the same).

They then argue that the “closed box” aspect of the universe can-
not possibly be true, given the assumptions of the Darwinists. The
universe was not a self-created entity. It could not have evolved in
the random way that the Darwinians claim. Such a development is
not mathematically possible, according to the laws of probability.”
God must have created the world, for there is no way to explain the
coming of life in a world governed by the second law of thermo-
dynamics, When man rebelled, God cursed the universe. This curse
“from the outside” is the origin of the law of entropy, not nature
itself: so runs the argument.

Henry Morris has openly praised the methodology of unifermi-
tarianism: “But there is obviously no way of knowing that these proc-
esses and the laws which describe them have always been the same
in the past or that they will always be the same in the future. It is
possible to make an assumption of this kind, of course, and this is the
well-known principle of uniformitarianism. The assumption is reason-
able, in the light of our experience with present processes, and it is
no doubt safe to extrapolate on this basis for a certain time into the
future and back into the past. But to insist that uniformitarianism is
the only scientific approach to the understanding of all past and
future time is clearly nothing but a dogmatic tenet of a particular
form of religion.”

He meant this last sentence to apply to modern Darwinists who
reject miracles, but the best of the Darwinian scientists never held to
such a rigid definition of the doctrine, since they, too, need ways to
escape the limits of a fixed-rate system of geological and biological
change,” and today we find a scientific revolution going on in the

19. See Edward Blick’s estimates, in Henry M. Morris, et al. (eds.), Creation: Acts,
Facts, Impacts (San Diego, California: Greation-Life Publishers, 1974), p. 175.

20. Henry M. Morris, “Science versus Scientism in Historical Geology,” in A
Symposium on Creation (Grand Rapids, Michigan: Baker Book House, $968), pp.
12-13.

21. Harvard paleontologist George Gaylord Simpson writes: “Some processes
(those of vulcanism or glaciation, for example) have evidently acted in the past with
scales and rates that cannot by any stretch be called ‘the same’ or even ‘approximately
the same’ as those of today.” Simpson, This View of Life: The World of an Evolutionist
(New York; Harcourt, Brace & World, 1964), p. 132.
