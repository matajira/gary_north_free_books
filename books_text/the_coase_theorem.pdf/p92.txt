74 THE COASE THEOREM

life itself. . . .”°* But this is insufficiently rigorous by the stan-
dards of Chicago School economics. He forgot that the victim’s
ability to earn a living also involves costs. The producer must
eat, use public facilities of various kinds, and be a life-long
absorber of resources. So, what Becker really meant to say is
that the cost of murder is the net loss — discounted by the pre-
vailing rate of long-term interest, of course* - of the late vic-
tim’s lifetime earning potential, minus net lifetime expenditures
(also discounted). This raises a key question in our era of legal-
ized abortion, which may be a preliminary to legalized euthana-
sia (as it has been in the Netherlands): What if the dead victim had
been sick, dying, mentally retarded, or in some other way is a net ab-
Sorber of society's scarce economic resources? Must we not conclude
that the murderer has in fact increased the net wealth of soci-
ety? Remember Becker's rule: “society’s” estimation of net social
costs or benefits “excludes, among other things, the value
placed by society on life itself” On what economic grounds
could a legislator oppose the concept of selective murder, with
criminal indictments to be handed down in specific cases only
after a retrospective evaluation (by some committee or other) of
net costs and benefits?** Who is to say? After all, as he says,

34. Becker, “Crime and Punishment,” p. 9.

35. Richard A. Posner, Economic Analysis of Law (Boston: Little, Brown, 1986), pp.
170-81.

36. Becker ulso fails to mention the value of life to the late victim, which seems
a bit odd, given the fact that Becker also pioneered a subdivision in the economics
profession called human capital: Gary 8. Becker, Human Capital (New York: National
Bureau of Economic Research, 1964). Fortunately, Richard Posner has attempted to
rectify this gaping hole in Becker’s analysis. He does try to make an objective estima-
tion of the economic value of life to the victim, which he concludes is nearly infinite.
He uses a hypothetical example of rising economic payment that someone would
demand to induce him to get involved in death-producing activities: the more likely
death becomes, the higher the pay demanded. If death is sure, the price demanded
will approach infinity. (Why, then, do men volunteer for suicide missions in war-
time?) This is his surrogate for making a subjective posthumous estimation of life’s
monetary value to the late victim: Posner, Economic Analysis of Law, pp. 182-86. He
draws no important conclusions from this analysis, however, and does not include it
in his book’s index under “death” or “death,” for which there are no entries, or
