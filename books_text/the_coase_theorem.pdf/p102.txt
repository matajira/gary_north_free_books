84 ‘THE COASE THEOREM

a perceptive law review article, he warned the practitioners of
both economics and law that the great benefit which the free
market offers society is not its efficiency or its maximizing of
economic value. What the free market offers is its support for
“institutional alternatives which generate less social tension, less
evasion of postulated standards of conduct, more general ad-
herence to legal norms.”** Yct economists and legal theorists
argue that free market economic processes that exist only in an
imaginary zero-cost world can and do offer us a cost-effective
real-world model: just substitute voluntary market exchanges
for enforcement by the State of legal titles. Those who argue
this way are not only utopians, they are intellectual arsonists.
This is the mid-1960’s social philosophy of “Burn, baby, burn!”
applied not only to the adjacent field but to society itself.

The Social Costs of the Coase Theorem

There may be a journal essay by a free market economist
that has inflicted more damage on the case for economic free-
dom than Coase’s “Problem of Social Cost.” There may also be
a scholarly essay that has polluted academia’s moral environ-
ment favoring market choice more than Coase’s has. I cannot
imagine what that essay might be. (Becker's 1968 essay on
“Crime and Punishment: an Economic Approach” comes close,
but it is really only an application of Coase’s economic ap-
proach to law.)

Coase can always argue that his right to inflict such moral
damage is merely a factor of academic production. No doubt
this essay advanced his academic reputation after 1960. That is
what the Nobel Committee believed, in any case. But for every

53. Buchanan, “Good Economics — Bad Law,” ibid., p. 486.

64. Dahlman is overstating the case against traditional welfare economics when
he says that transction costs “are at the heart of the matter of what prevents Pareto
optimal bliss from ruling sublime. For if we could only eliminate transaction costs,
externalitics would be of no consequence. . Cart J. Dahiman, “The Problem of
Externality,” Journal of Law & Economics, XXII {Aprit 1979), p. 161.

 

 
