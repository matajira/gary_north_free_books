xii THE COASE THEOREM

Bentham did not end his discussion at this point. If he had
ended here, the felicific calculus, his theoretically essential intel-
lectual construct, would have been stripped of all of its real-
world content. Bentham needed this admittedly fictional aggre-
gation: “This addibility of the happiness of different subjects,
however, when considered rigorously, it may appear fictitious,
is a postulatum without the allowance of which all political
reasoning is at a stand... .”? Bentham saw clearly that social
science, meaning the science that undergirds policy recommen-
dations, must assume the ability of the policy-maker to add up
the utilities of different individuals, even though the science of
autonomous man says that this is an impossible task.

In the very next issue of the Economic Journal, Robbins back-
tracked. “But I confess that at first I found the implication very
hard to swallow. For it meant, as Mr. Harrod rightly insisted,
that economics as a science could say nothing by way of pre-
scription.”” This is exactly what it meant, and Robbins, too,
was aghast. But not for long. “Further thought, however, con-
vinced me that this was irrational.” Why irrational? Because
economists have always known that their prescriptions “were
conditional upon the acceptance of norms lying outside eco-
nomics... . Why should one be frightened, I asked, of taking
a stand on judgements which are not scientific, if they relate to
matters outside the world of science?” Robbins returned epis-
temologically to Bentham’s fiction of the common scale of utili-
ty, just as Harrod had.

Robbins asked rhetorically why any economist should be
frightened about such an appeal to standards lying outside of
economic science. There is a very good reason why an academic

9, Idem.
10, Robbins, p. 637.
11. Bid., p. 638.

12. In 1961, Robbins cited Halévy's citations from Bentham: The Theory of
Economic Policy in English Classical Political Econonyy (London: Macmillan, 1961), p.
180.
