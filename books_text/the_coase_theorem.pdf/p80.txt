62 THE COASE THEOREM

analysis.”** Economic analysis informs us that the reason why
smuggling exists is because of controls that inhibit voluntary ex-
change. In this case, the control is the economics profession's
ban on open discussions of this question: ethics as an inescapable
aspect of policy-making. Rothbard has called attention to the
epistemologically soft underbelly of the economics profession:
the myth of ethical neutrality in economics. This is another
reason why he is not going to win the Nobel Prize.**

33. Rothbard, “Introduction to the French Edition of Ethics of Liberty,” journal
of Libertarian Studies, X (Fall 1991), p. 14.

34, Gary North, “Why Murray Rothbard Will Never Win the Nobel Prize!”
Man, Economy, and Liberty: Essays in Honor of Murray N. Rothbard, edited by Walter
Block and Llewellyn H. Rockwell, Jr. (Auburn, Alabama: Ludwig von Mises Insti-
tute, 1988), ch. 8.
