20 THE COASE THEOREM

men know, and how can they know it?” They operate in terms
of an implicit though hidden dialecticism between objective and
subjective value theory.

Social Cost

Pigou also raised another issue concerning welfare econom-
ics, It is a variant of the earlier problem of wealth redistribu-
tion. It has become known in the economics profession as “the
problem of social cost.” Pigou argued that there are cases of
market failure® in which private benefits from a particular
activity impose costs on third parties. Pollution is the obvious
example, although there are many others, he said. The benefits
to the polluter are immediate and direct, but there is no mar-
ket-produced incentive for him to cease polluting as long as his
costs of operation are less than expected revenues.” Part of
these costs are borne by someone else, At most, the polluter
bears only part of the costs (stinging eyes, for example), but he
reaps all of the rewards (lower production costs). He continues
to pollute the environment. ‘Total costs in the community —
social costs — are therefore greater than the polluter’s personal
private costs. Followers of Pigou’s analysis frequently argue that
the State should redistribute this “stolen” wealth back to the
original owners, perhaps through a tax on polluters and tax
reductions for victims, so as to balance total social benefits (from
production) and total social costs.

There is a hidden problem with this line of reasoning, one
which was not discovered for almost half a century. Buchanan
points to it: “The Pigouvian norm aims at bringing marginal
Private costs, as these influence choice, mto line with social costs, as

25. Tyler Cowen (ed,), The Theory of Market Failure: AC
fax, Virginia: George Mason University Press, 1988).

 

tical Examination (Faiv-

26. Yes, yes, I know: “the present value of an expected future stream of income,
discounted by the prevailing rate of interest.” But sometimes ! prefer co write in
English.
