Introduction 7

economically relevant information in order for it to be effective as
a summary of past events. The individual who pays an accoun-
tant thinks he is getting something for his money. What is he
getting? A bunch of numbers on a page? Or information? The
individual must interpret the significance of this choice-inflenc-
ing information. There is no escape from subjectivism.

The Roads Untravelled

Consider your own situation. You are still reading this essay.
You still have faith in a positive future return on your present
investment of time. Let us consider a hypothetical possibility.
With the time you spend reading this essay (assuming you stick
with it to the bitter end), you might be able to think of an in-
vestment strategy that would make you rich, but because of
something you will read here, you will never think of it or have
the courage to risk it, On the other hand, you may also avoid
an investment that really would bankrupt you. Unlike the man
in the story of the lady and the tiger, you have the option of ig-
noring both doors; instead, you choose to read this essay. But
you could have opened a door. Which would it have been, the
lady or the tiger? You cannot know for sure. You will never
know. You can only guess. So, what is the true cost of reading
this essay? Life with the lady or a brief but colorful encounter
with the tiger?

If we take seriously the modern economist’s discussion of
costs and choices, we may find our world disturbing. We never
really know what our actions are costing us, assuming that it is
true that there is no way to relate our subjective evaluations
before we act with objective costs after we act. This disturbing
Jack of certainty can be relieved by an act of faith: “And we
know that all things work together for good to them that love
God, to them who are the called according to his purpose”
(Romans 8:28). But this providential word of encouragement is
hardly helpful to the modern humanistic economist.
