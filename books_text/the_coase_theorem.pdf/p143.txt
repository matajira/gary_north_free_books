neutrality myth, xvi
optimality, 68-69
policy-making, 18-19, 22,
30
predictions, 13
reason, 13-14
relativity, 16
science ?, viii
solutions, 88
tasks, 76
technical, 42
utopianism &, 41-42
value-free ?, 30, 31
welfare, 17, 19, 44
wicked, 31
efficiency
Buchanan on, 84
equilibrium, 65
equity &, 45, 89
myth of, 50-51
Rothbard on, 50-51
social, 33, 50-51
Egger, John, 1
envy, 93
epistemology
ethics &, 101
lack of interest, 19
only youl, 4
resentment toward, viii-ix
Robbins vs. Harrod, x-
xiv, 17-19
self-interest, ix-x
equations, 41
equilibrium, 53, 57-58, 64,
65, 67, 94
equity (see justice)
ethics
appeal to, xii-xiii

Index

125

economics 102-103,
109

epistemoloty &, 101

externalities, xiv, 25-26

(see also poliution)

&,

fines, 68-69

fire, 98-99

flux, 52, 95

free market, 93
freedom, 107, 116
Friedman, Milton, 13n

Galbraith, J. K., 88n

games, 29-30

genocide, 31, 55

Georgescu-Roegen, Nicholas,
16

God, 7-8, 31, 47, 58, 65, 76,
98, 117-18

Goodrich, Pierre, 110

grace, 76

gunnery sergeant, 43

Halévy, Elie, xi

hall of mirrors, 44-45

Hardin, Garrett, 46

harm, 30-32, 69n

Harper, F. A, 57n

Harrod, Roy, x-xiv, 17-19,
51-52

Heibroner, Robert, xiv, 109

Heraclitus, 16, 59, 94

Humanist Manifesto, 150

ideal type, 57
imputation, 8, 104-5
indeterminism, 108, 116
