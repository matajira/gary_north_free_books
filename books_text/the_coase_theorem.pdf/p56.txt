38 THE COASE THEOREM

judges use Coase’s theorem as a standard of judgment and a
legal precedent, property owners will experience no loss. Both
assumptions are implicit to Coase’s thesis, and both are categor-
ically incorrect. Coase begins with an unreal world in which
transaction costs are defined away, and from this he draws his
equally unrealistic conclusions.*

I say that his conclusion initially appears to be correct — that
in a zero-cost world, the outcome of the bargaining process
would be the same, the value of cattle vs. the value of crops.
Yet in a perceptive essay by Donald Regan, we learn that Coase
has no warrant for making this conclusion. Coase assumes that
the free market’s voluntaristic bargaining process will produce
the same economic results that a compulsory civil court’s deci-
sion would produce if it were to follow Coase’s concept of net
social cost, but why should we believe this? Regan says that
Coase offers no model of how this bargaining process would
inevitably produce such identical results ix the absence of specified
and. legally enforceable property rights. For example, sometimes a
bargainer makes economic threats of non-cooperation that must
be occasionally enforced in order to persuade the other party
that he should take such threats seriously, even if the actual
carrying out of the threat may injure the threat-maker in the
short run. How does Coase know what the short-run or long-
run outcome of a bargaining process will be? He doesn't.* This
is simply another way of saying that we cannot confidently

4. Writes Jules L. Coleman: “No term in the philosopher's lexicon is more
imprecisely defined than is the economist’s term ‘transaction costs.’ Almost anything
counts as a transaction cost, But if we are to count the failure to reach agreement on
the division of surplus as necessarily resulting from transaction costs (I have no doubt
that sometimes it docs), then by ‘transaction cost” we must mean literally anything
that threatens the efficiency of market exchange. In that case, it could hardly come
as a surprise that, in the absence of transaction costs s0 conceived, market exchange
is efficient.” Goleman, “Economics and the Law: A Critical Review of the Foundations
of the Economic Approach to Law,” Ethics, 94 (July 1984), p. 666.

5. Donald H. Regan, “The Problem of Social Cost Revisited,” Journal of Law &
Economics, XV (Oct. 1972), pp. 428-32.
