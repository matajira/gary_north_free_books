118 THE COASE THEOREM

God. As a by-product of a biblical defense of each man’s res-
ponsibility, we can and must provide the basis of.a reconstruc-
tion in economic theory. Instead of sneaking objective value
theory (continuity) into subjective economic theory through the
back door of statistics and index numbers, we must lay as the
foundation of economic science both biblical objectivism (God’s
law) and biblical subjectivism (man’s responsibility). Economics
must be gounded on an explicitly biblical epistemology. To
develop a consistent economic science, we must avoid the dia-
lecticism of both pre-modern and modern epistemology.’?

We must begin with a covenantal view of God and man.
God’s covenant has five assertions: the sovereignty of God, the
hierarchy of God’s authority, the permanence of God's ethical
standards, the judgment of God (temporally and eternally), and
the continuity through time and eternity of this covenant.”

First, we know that God is all-knowing. He can make inter-
personal comparisons of all our individual subjective utilities
(Luke 21:2-4). Second, we know that we are responsible to God.
through time. He exercises authority over us. Third, His law is
our permanent standard of ethical performance. Fourth, we
know that He is the subjective evaluator of all the minds and
spirits of every creature in history. We know that he properly
“weighs” the importance of every act, service, and commodity.
He has objective knowledge of all subjective realities. Fifth, we
know that He has a perfect plan for the ages that will be per-
fectly fulfilled. His people will inherit the earth (Psalm 37:9).

Applying all this to economic theory, we conclude that
Shackle’s “moments-in-being” are linked through time in terms
of God's sovereignty, authority, law, judgments, and plan. Man
is made in God’s image, so he can make sense of his world. He
is personally, covenantally responsible before God, who judges

19, Cornelius Van Til, A Christian Theory of Knowledge (Nutley, New Jersey:
Presbyterian & Reformed, 1969).

20. Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas:
Institute for Christian Economics, 1987), chaps. 1-5.
