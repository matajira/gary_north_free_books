26 THE COASE THEOREM

the production process. In other words, these victims are exter-
nal to the firm or production unit, but not external to its costs
of operation. Almost without exception, previous economists’
discussion of externalities had ended with a consideration of
what government measures are appropriate to reduce or elimi-
nate these externalities. The conclusions reached by most econ-
omists, based on Pigou’s analysis in The Economics of Welfare (4th
ed., 1932; originally published in 1920), were as follows, Coase
summarized: the producer of pollution (smoke, noise, etc.)
should (1) pay damages to those injured, or (2) have a tax im-
posed on his production by the civil government, or (3) have
his factory excluded from residential districts.'* Coase’s article
broke with this tradition.

Aaron Levine summarizes Coase’s breakthrough: “Assuming
zero transaction costs and economic rationality, Coase, in his
seminal work, demonstrated that the market mechanism is cap-
able of eliminating negative externalities without the necessity
of governmentally imposed liability rules.”’® Furthermore, the
theorem leads to the conclusion that “if transactions are cost-
less, the initial assignment of a property right will not deter-
mine the ultimate use of the property.”'® Free market econo-
mists of the Chicago School have increasingly sided with Coase.
(What is also remarkable is that traditional Jewish law had
adopted the basic features of the Coase theorem many centuries
earlicr; English law had not.” Why remarkable? Because Exo-
dus 22:5-6 is clearly on Pigou’s side."*)

14. Coase, “Social Gost,” p. 1.

15. Aaron Levine, Free Enterprise and fewish Law: Aspects of Jewish Business Ethics
(New York: Ktav Publishing House, Yeshiva University Press, 1980), p. 59.

16. Posner, Economic Analysis of Law, p. 7.

17. Yehoshua Liebermann, “The Coase Theorem in Jewish Law,” Journal of Legal
Studies, X (June 1981), pp. 203-303.

18. Gary North, Tools of Dominion: The Case Laws of Exodus (Tyler, Texas: Insti-
tute for Christian Economics, 1990), ch. 18.
