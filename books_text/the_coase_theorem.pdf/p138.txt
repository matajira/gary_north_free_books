BIBLIOGRAPHY

Christian Economics

Beisner, E. Calvin, Prospects for Growth: A Biblical View of Popula-
tion, Resources, and the Future. Westchester, Illinois: Crossway
Books, 1990.

. Prosperity and Poverty: The Compassionate Use of
Resources in a Free Society. Westchester, Illinois: Crossway
Books, 1988.

Chilton, David. Productive Christians in an Age of Guilt-Manipula
tors. Third edition. Tyler, Texas: Institute for Christian Eco-
nomics, (1985) 1991.

Griffiths, Brian. The Creation of Wealth. London: Hodder &
Stoughton, 1984,

. Morality and the Market Place. London: Hodder
& Stoughton, 1982.

Hodge, Ian. Baptized Inflation: A Critique of “Christian” Keynesian-
ism. Tyler, Texas: Institute for Christian Economics, 1986.

North, Gary. The Dominion Covenant: Genesis. Second edition.
Tyler, Texas: Institute for Christian Economics, 1987,
