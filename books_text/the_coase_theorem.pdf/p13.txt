Preface xiii

economist should be frightened: by appealing to ethical norms
outside of the science of economics, he negates every trace of
the scientific content of his policy recommendations and pre-
scriptions. He thereby reduces economic policy-making to the
level of -— gasp! - political science, or even worse, sociology.
The exchange between Robbins and Harrod took place over
half a century ago, yet the profession has politely buried all
traces of it in its collective (!) memory. A few quirky people on
the fringe of the profession resurrect this issue from time to
time,’ but the profession takes no notice. Nevertheless, just
like Dracula, it cannot be permanently buried. It will continue
to reappear, though perhaps only in the shadows, for as long as
methodological individualism remains the official philosophical
foundation of economic science. Given the rapid demise of the
appeal of socialism since 1989, this foundation seems secure.

The Coase Theorem

The problem of making interpersonal comparisons of subjec-
tive utility lurks in the shadows of the classic essay by Ronald
H. Coase, “The Problem of Social Cost.”'* The problem of the
impossibility of making scientific comparisons of interpersonal
subjective utility is the problem of social cost. Until it is dealt
with forthrightly by Coase and his disciples, the problem of social
cost will remain the bedrock problem of modern economic science. With
the widespread acknowledgment after 1988 of the economic
collapse of socialism in Eastern Europe and the USSR, and with
the substitution of concern over pollution — the economic issue
of “externalities” - as the justification for retaining political

13. Cf. Mark A. Lutz (economist) and Kenneth Lux (clinical psychologist), The
Challenge of Humanistic Economics (Menlo Park, California: Benjamin/Cummings,
1979), ch. 5. The obscurity of both authors and their publisher is indicative of the
problem: fringe critics. But their chapter is on target epistemologically and is a
thetorical delight to read.

14. R. H. Coase, “The Problem of Social Gost,” fournal of Law and Economics, 11
(Oct. 1980), pp. 1-44.
