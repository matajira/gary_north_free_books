What the State Can Do 305

it stays on schedule in the release of documents to scholars, Con-
gressmen, and the Government Printing Office. The staff, includ-
ing the Foreign Service Officers, must be cut by at least 50 percent
within two years. This may be too slow. Everyone over the age of
55 would be allowed to retire early—what the liberals did to the
FBI to weed out agents who came up through the ranks during the
J. Edgar Hoover era. Anyone who balks gets that coveted assign-
ment in Chad.

There is no legitimate excuse for employing 24,000 people. It
is bureaucracy run wild.

All Civil Service protection or other protection from firing
would be removed. Some protection against arbitrary firing could
be established, such as severance payments of half the annual
salary for anyone with over three years of continuous service. But
there would be no assured, lifetime employment. It would be run
like a business,

Let them go to the Civil Service Commission to complain. Let
them form a union. Pray that they strike. Nothing would serve
the interests of this nation better than a successful strike of all
State Department employees.

Shut it down. Wait. Then go out and hire the replacement
staff from the business community. Forget about a Foreign Ser-
vice examination. Just make sure every person hired is fluent in
the foreign language of his choice. (It is interesting that the
Wriston Committee recommended in 1954 that the foreign lan-
guage examination requirement be dropped for Foreign Service
applicants, which the State Department did in 1955.)?8 Require
applicants to take an exam in the culture of the nation in question,
with the selection of textbooks and the responsibility for writing
and grading the exams placed entirely in the hands of retired busi-
hessirien and missionaries who have come back from the nation in
question, plus suggested readings from the diplomats of that nation,

Finally, we need at least two separate and competing offices to
check the security risk status of Department employees. By hav-

28. Johnson, Adminisiration of U.S. Foreign Policy, p. 105.
