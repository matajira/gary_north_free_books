Introduction 9

Sodom (Genesis 18), and the way that Moses went before God in
the name of Israel (Exodus 32:11-13). This is the number-one as-
signment that God gives to heads of nations: to represent their na-
tions before God. Most leaders pay no attention to this argument.

Then you would tell him about the other task of the represen-
tative leader: to represent God before his people. God told
Joshua: “This Book of the Law shall not depart from your mouth,
but you shall meditate in it day and night, that you may observe
to do according to all that is written in it. For then you will make
your way prosperous, and then you will have good success”
(Joshua 1:8). The ruler is supposed to tell those under his author-
ity whatever God’s law requires. Without obedience to God's law,
a nation should expect God's cursings in history (Deuteronomy
28:15-68). It is his job to persuade the people to adopt God’s laws
nationally, and then enforce them.

But what if he replied, “I want to get my nation’s life back
together, but leave out all this Jesus stuff. That’s a lot of nonsense”;
what then?

This is exactly what every leader of every nation in the West is
saying: “Leave out all this Jesus stuff!” If we Christians who know
Christ, believe in His Bible, and have been chosen by God to
preach the healing gospel of Christ to the nations — the disciplin-
ing gospel (Matthew 28:18-20)— remain tongue-tied and silent be-
fore the nations, what should we expect? The national blessings of
God? Or God’s cursings on an international scale?

“This Jesus Stuff”

If we are going to discuss Christian principles of international
relations, then let us discuss them. Our goal is not to make hu-
manism work better, except as a temporary tactic to buy a little
more peace and time until a majority of voters become Christians
and then vote for politicians who will support the Christian recon-
struction of all aspects of civil government. If Christians are going
to attempt to reform today’s pagan, humanist imitation of Chris-
tian international relations, and if they attempt to do so in terms
of a worldview that is acceptable to paganism, then mankind will
