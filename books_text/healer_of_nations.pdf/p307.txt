What the State Can Do 293

in March of 1987, Congressmen Richard Armey of Texas intro-
duced legislation to get the Soviet Embassy off Mt. Alto~Mt.
“High”—in Washington, D.C., the highest point in the city. The
bill was so uncompromising—“Get them off, now!”—that the
House Foreign Affairs Committee, under pressure from the
Administration and the Speaker of the House, substituted a com-
promise amendment to get them off Mt. Alto within a year, but
which would grant the President authority to allow the Soviets to
keep their Embassy if this was seen by the President as being in
the national interest . . . of the United States, that is.

The U.S. Embassy staff was outraged at the increased work
load, for they had to wash cars and take out garbage. “It’s just so
difficult,” one American said. Another complained: “We are oper-
ating on a wing and a prayer.”

A wing, maybe. No prayer. That is the heart of the problem
with this nation’s foreign policy: no prayers.

Yet it gets worse. The man who served as U.S. ambassador to
Moscow, Arthur Hartman, in 1984 sent a cable to the State
Department. It attacked “right wing” Reagan extremists for using
anti-spy programs to subvert U.S.-Soviet Relations. Hartman
accused U.S. officials of “wrapping themselves in the mantle” of
counterintelligence in order to ruin U.S.-Soviet relations through
counterespionage efforts. Under the proposed plan, Hartman
claimed, the Soviet embassy would have had to hire U.S. work-
ers, which would have forced the Soviets to shut down their em-
bassy. It would be tantamount to breaking diplomatic relations,
he argued.!2

A Clean Slate

The bugging of the U.S. embassy is an old, oid story. A letter
to the editor from Leonard Saffir published in the New York Times
(April 4, 1987) is choice. It was titled: “How Porky and Bugs Out~
witted the Russians.”

12. Washington Times, April 13.
