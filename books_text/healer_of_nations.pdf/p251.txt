Conclusion 237

world by Soviet and Chinese Communism,> a humanist religion
that retains a satanic imitation of the five-point Biblical covenant
structure. First, Communism announces a god, the materialist
forces of dialectical history, and holy scriptures, the writings of
Marx, Engels, and Lenin. Second, it has hierarchy: the Commu-
nist Party, which imterprets and applies the infallible word of
Marxism-Leninism. Third, it has Communist law, Communist
economics, and Communist military strategy as the basis of world
conquest. Fourth, it has a doctrine of sanctions: the inevitable
world Communist revolution. Fifth, it has a doctrine of continuity:
the inevitable triumph of the working class through the forces of
dialectical history.®

The stronger humanist religion is on the offensive. The
weaker humanist religion is on the defensive. Christians in the
West have bet their futures on the competence of the weaker hu-
manism to defend them from the new Assyrians. Christians have
transferred political and cultural sovereignty to the soft-core hu-
manists by default, through their adoption of a limited gospel of
partial reconciliation. They have proclaimed that “Jesus is Lord,”
but only over individual hearts, Christian families, a handful of
churches, and underfunded Christian schools. Satan is the prince
over everything else, they believe.

But Satan, like Jesus, is not content with partial victory.
Satan, like Jesus, wants it all. The Communists articulate Satan’s
demands much better that the soft-core humanists of the West do,
So God is in the process of delivering His people into the hands of
these Assyrians, because Christians have already in principle and
in fact defaulted to the soft-core humanists. Until Christians start
taking the offensive by preaching the whole counsel of God, the
absolute sovereignty of God, and the gospel of comprehensive re-
demption, they will remain covenantal subordinates to the re-
treating humanists of the West. They will remain on the side of
the losers.

5, Jean Frangois Revel, How Democracies Perish (Garden City, New York:
Doubleday, 1984).
6. FN. Lee, Communist Eschatology (Nutley, New Jersey: Craig Press, 1974).
