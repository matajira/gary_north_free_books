What the Church Can Do 271

Why shouldn’t Christian families pay to bring in a young man
for training? He can study, live in a Christian home, and receive a
new vision of what Christian fellowship is all about. The Commu-
nists put them in dormitories; Christians should put them in
homes. Churches located near universities should work with local
Christian campus ministries to identify new foreign converts and
get them out of campus living quarters. The moncy the students
save in room and board can be put to better purposes, such as
supplying the people back home with food and literature.

Churches should put up money to have good Christian
literature “de-nationalized” and translated into foreign languages.
The cost of getting out 5,000 copies of a cheap paperback book is
getting lower and lower: under $5,000. Local churches could pool
money until an agreed-upon book is ready. Dominion Press is
willing to cooperate with churches by locating translators and
handling the production of Biblical Blueprints Books, as well as
removing ail royalty rights on books translated into Third World
foreign languages. (We have already done this with a church that
wanted to publish a Spanish-language version of Liberating Planet
Earth.) Contact:

Translation and Publication
Dominion Press
P.O. Box 8204
Ft. Worth, TX 76124

Christian mission efforts have been limited to saving souls and
ignoring cultures. This is understandable; that is how most
fundamentalist Christians view the gospel. Missionaries have
brought the ABC’s of the gospel to foreign pagans, but have not
had any vision of the KLM’s, let alone the XYZ’s of the faith. The
result has been that when even significant numbers of converts
have been brought into the churches, they have not been given
the tools to confront the domestic paganism at home and the
paganism imported from Communist nations. They have become
cannon fodder for their enemies.
