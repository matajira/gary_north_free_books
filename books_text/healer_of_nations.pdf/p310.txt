296 Healer of the Nations

fundamental freedoms for all without distinction as to race, sex, language, or
religion; and

4, To be a center for harmonizing the actions of nations in the
attainment of these common ends.

It asserts sovereignty over the whole world. Article 2 states:

6. The Organization shall ensure that states which are not
Members of the United Nations act in accordance with these Prin-
ciples so far as may be necessary for the maintenance of interna-
tional peace and security.16

Article 2 begins with this principle: “The Organization is
based on the principle of the sovereign equality of all its Mem-
bers.” Yet at the Yalta Conference of 1945, Roosevelt and Church-
ill agreed to allow the Soviet Union to have three votes in the
General Assembly: the two extra votes are for the Ukrainian SSR.
and the Byelorussian SSR.!” The United States pays one-third of
the total UN budget, the maximum required for any nation.#® So
much for “sovereign equality.”

Tt even requires an oath from its staff members that places the
UN above all other civil government loyalties:

I solemnly swear . . . to exercise in all loyalty, discretion, and.
conscience the functions entrusted to me as an international civil
servant of the United Nations, to discharge these functions and
regulate my conduct with the interests of the United Nations only
in view, and not seck or accept instructions in regard to the per-
formance of my duties from any Government or other authority
external to the Organization.

15. Charter of the United Nations (June 26, 1945), Chapter 1, Article 1
(emphasis added). Leland M. Goodrich and Evard Hambro, Charter of the United
Nations: Commentary and Documents (rev. ed.; Boston: World Peace Foundation,
1949), p. 584.

16. Idem.

17. G. Edward Griffin, The Fearful Master: A Second Look at the United Nations
(Boston: Western Islands, 1964), p. 88.

18. Charter, p. 185.

19. Cited by Arkady N. Shevchenko, Breaking With Moscow (New York:
Knopf, 1985), p. 220.
