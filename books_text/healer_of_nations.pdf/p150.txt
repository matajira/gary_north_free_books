136 Healer of the Nations

“real” demands. Appeasement has therefore been basic to Western
diplomacy throughout this century. Hitler bluffed his way from
German military weakness in 1933 to German control over much
of Central Europe by 1939, and the European professional
diplomats never learned to deal with him." Pipes concludes:
“Thus, for both constitutional reasons and reasons connected with
the peculiarities of totalitarian politics, the State Department is
not the proper agency to formulate and execute foreign policy to-
ward the Soviet Union or any other totalitarian state. These states
play by different rules and must be dealt with accordingly. Since
they employ Grand Strategy, to the extent that democracies are
capable of coordinated foreign policies, these must be undertaken
by the chief executive.”*

Summary

It is the God-assigned task of Christians to render righteous
judgments in history, in preparation for the day that we shall
judge the angels (1 Corinthians 6:3). Exercising righteous judg-
ment is a difficult task. When Adam failed to judge righteously
between the claims of Satan and the claims of God, he fell, and his
posterity lives under the curses that God applied in His righteous
judgment.

The worldwide task of preaching the gospel and subduing the
hearts, minds, and souls of men to Christ is a form of spiritual
warfare. It is a war aimed at attaining peace, but only on God’s
terms. There is no other way to attain peace. Peace is a blessing
from God, as are all good gifts (James 1:17). Thus, Christian na-
tions must wage peace. Sometimes this involves preparing for
war, But military warfare is always to be defensive; the sword of
the Lord is our offensive weapon: preaching. We are to trust God
to provide our national defense. A Department of Defense is legit-
imate; a Department of Offense isn’t,

13. A. J. P. Taylor, The Origins of the Second World War (2nd ed.; New York:
Fawcett, [1961] 1965).
14. Survival Is Not Enough, p. 275.
