188 Healer of the Nations

humanist puts his faith in other things: military equipment and
international agreements. Fearful men see the Babylon of our
day, the Soviet Union, and they want to make an alliance with the
Egypts of this world for protection. But Egypt does not offer legiti-
mate hope to God’s people. As God warned the Israelites through.
Jeremiah during a similar crisis:

“Do not be afraid of the king of Babylon, of whom you are
afraid; do not be afraid of him,” says the Lorn, “for ] am with you,
to save you and deliver you from his hand. And I will show you
mercy, that he may have mercy on you and cause you to return to
your own land.” “But if you say, ‘We will not dwell in this land, dis-
obeying the voice of the Lorp your God, saying, ‘No, but we will
go to the land of Egypt where we shall see no war, nor hear the
sound of che trumpet, nor be hungry for bread, and there we will
dwel?—Then hear now the word of the Lorp, O remnant of
Judah! Thus says the Lorn of hosts, the God of Israel: If you
wholly set your faces to enter Egypt, and go to sojourn there, then
it shall be that the sword which you feared shall overtake you there
in the land of Egypt; the famine of which you were afraid shall fol-
low close after you there in Egypt; and there you shall die’” ( Jere-
miah 42:11-16),

Free Market Internationalism

The major players in the Round Table-CFR-Trilateral Com-
mission networks are initiated from the top of the social and intel-
lectual hierarchy: academia, business, law, the military, and the
media. But the dominant interest is banking. Until the world
adopts a Biblical system of money and banking, there will be con-
tinuing economic and political problems with international fi-
nance capital. What is needed is a system of honest weights and
measures and a prohibition against all fractional reserve banking.
(See my book in the Biblical Blueprints Series, Honest Money: The
Biblical Blueprint for Money and Banking.)

Nevertheless, we must begin to reform this world, not a
millennial world. The primary solution to the growth of con-
spiratorial networks of influential people who control national
