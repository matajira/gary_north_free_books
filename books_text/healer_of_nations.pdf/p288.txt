274 Healer of the Nations

tional peace. The humanists want unity on anti-Christian terms,
yet Christians are unable to conceive of internationalism among
civil governments because they have been failures in establishing
what Jesus Christ set forth as a basic requirement for presenting
the gospel successfully: Church unity.

Under crisis, Christians can and must unify. The problem is,
after the crisis is over, Christians historically have gone back to
squabbling. They then turn over society to.humanists, for squab-
blers cannot successfully build anything. The common “lip” of
Christianity has turned into the confused “lips” of Babel. This is
the judgment of God against sinful Christians. Meanwhile, the
new Babylonians are building an international Tower.

Revival will give Christians another opportunity to build
God’s temple, so that the world will come to Zion in order to hear
God’s law and His message of spiritual and cultural healing. The
question is: When will this revival come? Before a world crisis?
During a world crisis? Or half a millennium after the crisis that
has led to a new dark age?
