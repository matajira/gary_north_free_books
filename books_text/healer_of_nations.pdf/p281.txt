Wheat the Church Can Do 267

mation at an Eastern Orthodox seminary, or study Church-State
relations at a Roman Catholic seminary? (All this assumes that
anyone would want to attend seminary.) Not many, I would
guess.

A hermetically sealed container is one that allows nothing to
escape because it allows nothing to get in, Christian traditions
have been very nearly hermetically sealed from each other. This
has been especially true of Protestantism, though Eastern Or-
thodoxy may be the king of ecclesiastical isolationism. Christians
do not know what other church traditions are. American Chris-
tians are astoundingly ignorant about Church history. They care
nothing about history. They are uninterested in the progress of
the Church in history. Why? Because they have no doctrine of the
Church in the future. They do not believe that the Church will
make a fundamental difference in the transformation of world civ-
ilization. They do not believe that the Church International has
served, is serving, and will serve as the world’s proper model for
nationhood. They are Christian isolationists. It is not surprising
that they are also nationalistic isolationists.

They do not look into Church history to find examples of suc-
cessful ventures in Church unity. They do not look across a border
—denominational or geographical —to learn how other Christians
are dealing with cultural; economic, and political problems. They
do not. expect to find Biblical solutions to real-world problems.
And because they do not expect them, they do not discover them.
They do not ask other Christians, “Can you shoot straight? Can
you shoot straighter than I can‘under all circumstances? Gan you
teach me how to shoot in a new situation?”

; Meanwhile, the enemy has now advanced to 300 yards, and is
closing in fast.

The Shooting Has Begun
What is happening to Russian Orthodoxy under the Soviets is
worse than what is happening to Eastern Orthodoxy under the
Turks, What is happening to Christians in South Africa —“neck-
lacing” —is a taste of things to come, (Necklacing is the term for
