80 Healer of the Nations

39. The answer is covenant: shared confession, the “unity of

lip.”
40. Empires are always at war with the kingdom of God.
41. Most humanists insist on the unity of the godhead: man.
42. Other humanists insist on total national isolation, the
“Jonah impulse.”

43. Paul was an internationalist.

44, The war goes on: no peace, only temporary cease-fire
agreements.

45. The goal of Christian international relations is: “All na-
tions under God.”
