90 Healer of the Nations

losophical first principles, how is it that such acts as murder and
adultery are almost universally condemned? Doesn’t this point to
the existence of natural law?

No, it points to man as the image of God. God says that He
has implanted the work of the law—not the law itself—into the
heart of every human being.

. .. for when Gentiles, who do not have the law, by nature do the
things contained in the law, these, although not having the law, are
a law to themselves, who show the work of the law written in their
hearts, their conscience also bearing witness, and between themselves
their thoughts accusing or else excusing them (Romans 2:14-15;
emphasis added).

The problem is, as men become more and more rebellious
against God, their consciences cease to function properly. Paul
also wrote:

Now the Spirit expressly says that in latter times some will
depart from the faith, giving heed to deceiving spirits and doc-
trines of demons, speaking lies in hypocrisy, having their own con-
science seared with a hot iron. , . (f Timothy 4:1-2).

Therefore, the problem is not a lack of logic—“right reason” — but
rather a lack of ethics. Man’s problem is moral, not philosophical. It
is not that rebellious men are stupid; it is that they are in rebellion.

Thus, the work of the law of God can restrain men from gross
evil during certain periods of history. But when God withdraws
His restraining common grace, men are increasingly evil, and in-
creasingly unwilling to listen to their own consciences. The fact
that they have been shown the work of the law becomes more and.
more irrelevant. Their knowledge serves to condemn them, but
not to restrain them.

Even the covenant people of Israel were required to listen as a
nation to the reading of God’s revealed law once every seven years
(Deuteronomy 31:10-13). If natural law is so natural, why would
God require the public reading of His law?

In principle, Christians do have the law implanted in their
hearts at the point of conversion (Hebrews 8:7-13; 10:16). But this
