220 Healer of the Nations

Subsidizing Evil

The first and most important point in understanding govern-
ment-operated foreign aid programs is that they are government-to-
government programs. They are not foreign projects that have been.
investigated by private charitable or profit-seeking organizations
in a wealthy country, and then financed by representatives of that
nation’s donors or investors. Instead, these projects are submitted
by a foreign nation’s political leaders or by a foreign bureaucracy,
for consideration by bureaucratic functionaries in another nation.
If approved by the financing nation’s bureaucrats, and if put into
the budget by the decision-making politicians, money collected by
taxation is then transferred to the foreign nation’s bureaucrats.
The money may or may not be spent on the agreed-upon project,
but the money will invariably strengthen the control of the recipi-
ent government’s bureaucrats at the expense of that nation’s pri-
vate sector, whether charitable or profit-seeking.

The bureaucrats on both sides of the border proclaim the
moral necessity of aid “with no strings attached.” What this means
is aid with no responsibilities to the taxpayers and voters of either
nation. It means no strings attached on bureaucrats in the recipient nation.
This is what every bureaucrat seeks, as surely as businessmen
want to begin projects without competitors. The free market does
not allow businessmen to achieve their goal; political coercion
does allow government bureaucrats to achieve it, at least for a
while.

P. T. Bauer, a Jewish scholar who converted to Roman
Catholicism late in life, is the Western economist who more than
any other scholar has pursued the implications of government-to-
government foreign aid. He became a member of the House of
Lords in Britain late in life. Bauer writes: “Foreign aid augments
the resources of recipient governments compared to those of the
private sector, thereby promoting concentration of power in the
recipient countries. This effect is much reinforced by the preferen-
tial treatment in the allocation of aid of governments engaged in
comprehensive planning, a criterion based, at any rate ostensibly,
