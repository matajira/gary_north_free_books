330 Healer of the Nations

Osgood, Robert, 203-5
‘overcoming (dominion), 252-55
ownership of the earth, 51
Oxford University, 182-83

paganism
covenants, 198
default, 250
natural law, 85-89
scattered, 69
unity, 78
see also humanism, one-State world
palace revolt, 72
Parliament, 54
passports, 50
patriotism, 236
Paul, 75, 262
peace
Biblical standard, 33
Christian warfare, 202
covenant &, 121
covenantal, 105
edification & 262
goal, 103-5, 199
final judgment, 75
gospel &, 20, 105, 118-19
institutions, 108
lure of, 127-28
righteousness &, 105
subordination, 199
treaties, It
war &, 125
peace movements, 104
peace offensive, 124
Pearl Harbor, x
Pentecost, 52, 70, 156
perfectionism, 39, 158
perjury, 163
Perkins, Dexter, 15
pessimism, 169-70
Pharaoh, 155, 219
Philippines, 228-29
pietism, 273

Pines, Burton Yale, 166
Pipes, Richard, 132, 135, 136
pledge of allegiance, 57-58
plowshares, 123-27
pluralism, 286
Biblical, 55
Christian, 77
humanism’s, 96, 245, 286
lie, 142
medievalism, 77-78
Schaeffer's, 201
polis, 88
politics, 98
Porky Pig, 294
pornography, 245
positive feedback, 253
power, 30
power religion, 86, 99, 236., 246
pragmatism, 175, 176, 204, 225
prayer, 19
precision through division, 262-63
President, 163, 177, 208, 214, 297
profit, 192
Progressive movement, 48
Protestant Reformation, 58, 164, 234
public schools, 85

rape, 37-38
rapture, xii
reason, 90
Reconstructionisis, 178
reconciliation, 46, 233
Red Cross, 189
redemption, 37, 45
Reformation, 58, 164, 234
Rehoboam, 59, 63
relativism, 204
reliability, 210
remnant, 29
Renaissance, 147-48
representation
centurion & Jesus, 161-64
covenantal, 199
