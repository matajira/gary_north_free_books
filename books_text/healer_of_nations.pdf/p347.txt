Subject Index

Thoburn, Robert, 94, 268
thrift, 193
time-table, 202
totalitarianism, 31, 106,148
‘Tower of Babel, 32, 44, 46, 66-69
trade
freedom, 279 |
missionary activity, 189
most-favored nations, 227-29
Nazis and U.S, 115 i
reduced tension, 169, j
USSR, 132; see also USTE
with military enemies, 195
with theological enemies, 208
U.S., 208

treaties i
between gods, 198
binding, 202-6
covenant, 18
missions &, 37, 173
peace, 75
two kinds, 298
‘unity of man, 111
veto of, 299
Washington's Farewell Address,
11-15
tribute, 28, 29
Trilateral Commission, 170, 177
trust, 107 :
Tsu, Sun, 206
tyranny, 36, 59, 63, 72, 151, 157,
United Nations
elitist, 71
incompetent, 123
liberals’ hope, 123-24
loyalty to, 296
seligious dream, 295
Rushdoony on, 70
sovereignty, 296
spies, 297
symbol of unity, 73
‘Tower of Babel, 70-71

214

333

voting (USSR), 296
warfare, 103
will to resist, 206
United States
apostasy of, 60
unity of, 32, 45-46
church, 160
confession, 68
creed, 164
godhead; 109
kingdom of God vs Satan’s, 75
kingdom of man, 108
Rockefeller on, 203
political, 67
program for, i
sacraments, 73
USSR
ambassadors, 170-71
“Babylon,” 188
encirclement by, 40, 118
KGB, 297
internationalism, 167
lies, 307
negotiations, xi
offensive, 246
Roosevelt &, 116-17
Satan's, 38, 40
surrogates, 227, 40
trade, 224-25, 227-78
treaties, 11
USTEC, 227, 132, 275
utopia, 19
utopians, 18-19

victory
covenantal, 121
goal of, 129-36
history &, 135
Jesus’, 2
promise of, 105
vision of, 130, 195, 206,
235
voting, 26
