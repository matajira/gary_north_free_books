IV. Judgment/Sanctions

4

NATIONS WITH RIVAL COVENANTS
ARE ALWAYS AT WAR

Put on the whole armor of God, that you may be able to stand
against the wiles of the devil. For we do not wrestle against flesh
and blood, but against principalities, against powers, against the
rulers of the darkness of this age, against spiritual hosts of
wickedness in the heavenly places (Ephesians 6:11-12).

The fourth point of the covenant structure is judgment. God
imposes dual sanctions in history: blessings and cursings. Nations
sometimes become God’s rods of judgment against other nations,
just as Assyria served as God's rod of affliction against Israel
(Isaiah 10:5).

Paul says that we wrestle against spiritual forces. There is evil
in the world, and for as long as there is evil in the world, covenant-
keepers and covenant-breakers will be in conflict. This conflict is
primarily ethical. It centers around the law of God (point three).
Each side attempts to extend its influence in history: covenant-
keepers by means of covenantal faithfulness to the law of God,
and covenant-breakers by means of power. There is a history-long
struggle between dominion religion and power religion.

Why are there wars? James writes: “Where do wars and fights
come from among you [Christians]? Do they not come from your
desires for pleasure that war in your members” ( James 4:1)? If this
is true within the Church International, how much more in the
world? Sin is the cause. We are in a spiritual war of good against
evil. It begins in the life of each person, Paul tells us (Romans 7).
It spreads to the institutions we are part of: family, church, state,

99
