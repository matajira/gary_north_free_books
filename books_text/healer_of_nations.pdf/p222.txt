208 Healer of the Nations

may not establish permanent legal pacts with them that possess
the five characteristics of the Biblical covenant: 1) common consti-
tution (source of authority); 2) common political or legal hierar-
chy; 3) common law-order; 4) common courts and common con-
fession of faith; and 5) common citizenship.

This does not mean that Christian nations are not allowed to
make temporary alliances for specific purposes with non-
Christian nations. Like a Christian who purchases the services of
non-Christian specialists, so can the Christian nation legitimately
purchase services from other nations, services such as military aid
ina crisis, There is a continual process of buying and selling going
on in international relations. This is normal. This is legitimate.
What is not legitimate is the creation of covenantal bonds, espe-
cially any on-going pact that is equal in authority to the constitu-
tion or fundamental law of a Christian nation, and which there-
fore takes precedence over the legislation of a Christian nation.

This is perhaps the most glaring weakness in the United States
Constitution: treaties are not explicitly said to be subordinate to
the Constitution. The President, with a two-thirds vote of those
present for a vote by the Senate, can establish covenantal bonds
with foreign nations that take precedence over national legisla-
tion. A treaty seems to become equal to a ratified amendment of
the Constitution. It is still a legally open question as to whether or
just how a ratified treaty is to be dealt with by the Supreme Court
or Congress if it is not in conformity to the Constitution.!! The
Supreme Court has never held a treaty to be unconstitutional. It
held that a treaty does supersede the Tenth Amendment, which
lodges in the states all powers not delegated to the United States.
Senator John Bricker of Ohio attempted in the early 1950's to add
an amendment to the Constitution that would have required the
provisions of any treaty to be conformable to the Constitution. It
was defeated in the Senate by one vote in 1954, and was never

il. The Constitution of the United States of America: Analysis and Interpretation
(Washington, D.C.: Government Printing Office, 1973), pp. 495-500.

12. Ibid., p. 495.

13. The case that stated this was Missouri. ». Holland (1920).
