124 Healer of the Nations

used, and improved. It may not be the last or the best hope of
mankind, but it is an indispensable instrument of the human
effort to muddle through the crises of the present and rise to the
challenges of the future.”

The Bible's goal is clear: international peace. Christian na-
tions are to “wage a peace offensive.” But how is this “peace offen-
sive” to be pursued? By a “convergence” between East and West,
North and South? By unilateral disarmament? By billions of dol-
lars of taxes in foreign aid programs?

The Bible is very clear concerning the nature of peace. It is
one of the blessings promised in the blessings section of Deuteron-
omy 28, the Bible’s key passage that deals with the sanctions of
God:

Now it shall come to pass, if you diligently obey the voice of the
Lorp your God, to observe carefully all His commandments which
I command you today, that the Lorp your God will set you high
above all nations of the earth. And all these blessings shall come
upon you and overtake you, because you obey the voice of the
Lorn your God: Blessed shall you be in the city, and blessed shall
you be in the country. Blessed shall be the fruit of your body, the
produce of your ground and the increase of your herds, the in-
crease of your cattle and the offspring of your flocks. Blessed shall
be your basket and your kneading bowl. Blessed shall you be when
you come in, and blessed shall you be when you go out. The Lorp
will cause your enemies who rise against you to be defeated before
your face; they shall come out against you one way and flee before
you seven ways... . Then all the peoples of the earth shall see
that you are called by the name of the Lorp, and they shall be
afraid of you (Deuteronomy 28:1-7, 10).

This was God’s covenantal promise to Israel as a lonely and
frequently besieged nation. But with the gospel of Christ spread-
ing throughout the world, this promise can be appropriated by a
federation of faithful nations. The promise of peace meant that

1. Inis L. Claude, Jr., Swords Into Plowshaves: The Problems and Progress of Interna-
tional Organization (2nd ed.; New York: Random House, 1959), p. 472.
