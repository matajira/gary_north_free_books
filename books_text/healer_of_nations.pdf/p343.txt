 

Subject Index 329

Napoleon, 13
nation
Christian, 125, 133, 198
covenant, 27
covenant blessings, 253
09, 24-25
Great Commission, 97
N. T. definition, 25
nationalism, 76, 169-70, 235
conservative, 56
nations
baptizing, 159-64
covenanting, 252
God's, 167
nationhood, 151
missionary bases, 169
morality, 203-4
rebellions, 125-26
six types, 207
subdued, 157
nations of mankind, 32-33
nation of nations, 169; sce also
confederation Christian order
nation-state
boundaries, 29, 31-32, 45, 51
Christian, 93
confederation, 51
covenant, 10, 172
discipling, 57
features, 34, 36
humanist, 30
imperfect, 39
pessimillennialism, 169-70
protection, 39
revival, 34
savior of, 57
sovereignty, 31
under covenant, 83
NATO, 203
naturalization, 146
natural law
Christians &, xii, 89, 94, 233
foreign policy &, 3

losing faith, 149
myth of, 86
origin of, 87-89
pagan idea, 85-89
Schaeffer &, 201
theocracy vs., 92
see also neutrality
Nebuchadnezzar, 23
nerve (failure of), 106, 245, 258
negotiation, xi, 132-33, 135, 137;
see also diplomacy
neutral court, 201
neutrality
covenantal, 92
international relations, 41
myth of, 6, 37, 47, 57, 92, 141,
198, 200, 215
state, 7
theology of, 5
new humanity, 46
New World Order, 63-73, 110, 204-5;
see also one-State world
Nimrod, 66
Nineveh, 74
Nisbet, Robert, 30
North, Oliver, ix, xiii
nuclear winter, 109

oath, 26, 28, 105, 197, 162-63
occultism, 285
occupy, 175, 179
oil, 156
old boy network, xi, 299
one-State world order
God vs,, 77
gospel failure, 169
impossible, 69
illegitimate, 44, 46, 47, 77, 146
humanist, 95
stages leading to, 65
see also empire
one-world Christian order, 94-95
OPEG, 224
