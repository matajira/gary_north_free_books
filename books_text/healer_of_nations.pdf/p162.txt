148 Healer of ihe Nations

marriage: rich children of businessmen could marry into the
nobility. The royal lines were sacrosanct, which is why European
royalty was related by marriage. The kings of England, Germany,
and Russia in World War I all had the same grandmother, Queen
Victoria. Cousins King George V and Czar Nicholas II looked
enough alike to be brothers. (As one of the three was rumored to
have said, “This war would never have started if Grandmother
had been alive.”)

The totalitarian State is geographical. Everyone is supposed to
vote in elections, but only the Party selects candidates. Member-
ship in the Party is through initiation. High Party officials bring
their children into the Party. Citizenship is largely fictional for the
masses outside the Party. Their purpose is to legitimize the social
order: “Vox populi, vox dei” (“The voice of the people is the voice
of God”). It is ritual participation.

Modern democracy is constitutional. The citizen must swear
allegiance to the constitution, either explicitly (naturalized citi-
zens) or implicitly (children of citizens automatically become
eligible to vote at a certain age). Those who have not sworn
allegiance may be residents of the nation, but they are not citi-
zens. Geography is not the deciding issue. For example, the
United States government requires U.S. citizens who reside out-
side the geographical boundaries of the United States and who
earn income from non-U.S. sources to pay income taxes to the
U.S. government. (There is a minimum income that is not taxed,
but high income earners are required to pay.) This is unique in
the world, yet fully consistent with the U.S. theory of citizenship:
constitutional (covenantal) rather than geographical.

Each system has exclusions. There is no escape from the pro-
cess of inclusion and exclusion. The only question is: What
should be the basis for inclusion or exclusion? The Biblical answer
is: the terms of God’s civil covenant.

Christian Civil Citizenship

The basis of inclusion in the institutional church is cove-
nantal: baptism, Communion, and outward conformity to the
