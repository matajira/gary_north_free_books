Missionaries Make the Best Ambassadors 169

this heavenly colonialism. They call ali nations to join a Chris-
tian commonwealth of nations. They are not to call other na-
tions to join under the headship of a particular nation, for that
would be an imitation of Satan’s colonialism. But there must
always be colonialism: God's or Satan’s. Colonialism is an inesca-
pable concept.

The Biblical standard is that nations must see themselves as
missionary bases, just as Israel served Jonah as a base of opera-
tions. The goal is spiritual conquest. If God’s vision for His king-
dom is international, with the goal being the establishment of a
covenantal “nation of nations” over time, then no nation can legit-
imately claim that it is the Christian nation, the only valid repre-
sentative of God’s kingdom on earth. Political conquest of one
Christian nation by another is illegitimate.

This is not to say that such attempts will never be made. Sin is
always a problem. But open borders do reduce tensions: free
trade and immigration allow men to achieve in international rela-
tions what residents of regions within a nation have achieved.
Warfare between states (provinces) has generally disappeared; the
same can be true in the future of nations that are in covenant
together. Sin will be subdued over time through the preaching of
the gospel and the conversion of men and nations.

Of course, anyone who believes that sin will increase because
the gospel fails in history will reject the idea that the inheritance of
righteous nations increases, while the rebellious nations are cut off
in history. The person who accepts such a view of the gospel’s
effects in history should expect either international anarchy or the
rise of a tyrannical empire, Nations will either collapse into anarchy
or be swallowed up by some gigantic world empire. But nations as
such will not survive.

This is why it is strange that millions of Christians who pro-
mote pessimistic eschatologies also tend to be highly nationalistic,
It is as if they are putting their earthly political hope in the nation-
state as the last, best institutional defense against either world
tyranny or world disintegration, despite the fact that their escha-
tologies ought to tell them the opposite: that of all institutions, the
