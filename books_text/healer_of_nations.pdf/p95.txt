III. Ethics/Dominion

3

GOD'S WORLD GOVERNMENT
THROUGH BIBLICAL LAW

Then the eleven disciples went away into Galilee, to the
mountain which Jesus had appointed for them. And when they
saw Him, they worshipped Him; but some doubted. Then Jesus
came and spoke to them, saying, “All authority has been given to
Me in heaven and on earth. Go therefore and make disciples of
all the nations, baptizing them in the name of the Father and of
the Son and of the Holy Spirit, teaching them to observe all
things that I have commanded you; and lo, I am with you
always, even to the end of the age.” Amen (Matthew 28:16-20).

The third aspect of the Biblical covenant is law, specifically,
Biblical law. Biblical law is the God-given tool of worldwide do-
minion for Christians.1 The kingdom-oriented goal of God’s peo-
ple in history is to work toward the worldwide manifestation of the
kingdom that exists now in heaven and in principle on earth. This
is why Christians are told to pray, “Thy kingdom come. Thy will
be done in earth, as it is in heaven” (Matthew 6:10, KJV).

The sign that God’s kingdom has come in history is that Chris-
tians are obeying His law. As Christians cbey God's law ever more
faithfully, the kingdom of God in history expands progressively into
every area of life. This is the principle of /eaven (Matthew 13:33).
God’s leaven replaces Satan’s leaven. God's kingdom progressively
replaces Satan’s kingdom as the dominant factor in world history.

1. Gary North, Tools of Dominion: The Case Laws of Exodus (Tyler, Texas: Insti-
tute for Christian Economics, 1988).

81
