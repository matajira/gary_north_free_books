What the Individual Christian Can Do 279

tools to the needy. They must also take great pains to make sure
that the gospel being preached by the missionaries is not a Marx-
ist version of liberation theology. (See my book in the Biblical
Blueprints Series, Liberating Planet Earth.)

Christians should not join pressure groups to restrict interna-
tional trade through tariffs, import quotas, and other State re-
straints on voluntary transactions.

Christians should not become advocates of closed borders to
those who are coming here to work. Obviously, revolutionaries
may accompany the immigrants, but trained revolutionaries are
going to get into a free nation anyway. The borders are not that
tight, and they cannot be made that tight. We are not Communist
nations.

Christians should not vote for State-financed welfare pro-
grams that will pay immigrants (or anyone else) not to work.
Paul’s warning is clear: “If anyone will not work, neither shall he
eat” (2 Thessalonians 3:10b). By voting for tax-financed welfare
schemes, Christians have helped to create a closed-borders men-
tality. To keep out immigrants who might go on welfare (though
few do), voters pressure legislatures to seal off the borders. A cen-
tury ago, immigrants were welcomed, or at least tolerated,
because they came to work, to serve the consumers. Socialism has
led to a world of passports, visas, and immigration quotas. Voters
are afraid that poor immigrants will vote themselves rich. This is
the grim heritage of democratic socialism’s version of the eighth
commandment: “Thou shalt not steal, except by majority vote.”

Christians must financially support churches that serve immi-
grants, This is the unique division of labor that foreign language
churches can offer. Because these will normally be segregated
liturgically and linguistically, no denomination will have the
funds and trained pastors to meet the needs of every immigrant
group. Yet the message of Acts 2 is clear: immigrants are to be
taught the gospel in their own tongue until they can master Eng-
lish, which the first generation should not be expected to do.
Thus, people of one denomination will have to accept the fact that
some other denomination will become the trainer and spokesmen
