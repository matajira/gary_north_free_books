226 Healer of the Nations

market rates, many of which are guaranteed by the U.S. govern-
ment-financed Import-Export Bank. Should we be surprised that
the head of the Import-Export Bank in 1972, the year that the
Department of Commerce authorized the sale to the Soviet Union
of the ball bearing machines that made possible the building of
MIRVed nuclear warheads, was Wall Street lawyer William J.
Casey, a former associate of Armand Hammer?’ And should we
be surprised that Mr. Casey was appointed head of the Central
Intelligence Agency (CIA) in 1981 by President Reagan? (Mr.
Casey died in May of 1987 of a stroke, not long before he was to
testify to Congress concerning his involvement in the controver-
sial secret program of supplying weapons to Iran.)

The Goods Are Gone Forever

Thus, the multinational banks have used their depositors’
money to continue providing foreign aid to insolvent nations that
cannot and will not repay them. This has allowed the various na-
tional governments to reduce the far more visible foreign aid
transfers. The depositors’ money was spent by the recipients; the
purchased goods of the capitalist world have been transferred to
the Third World, and now the loans and the entire banking sys-
tem are in jeopardy. The banks have made their money (so far);
the foreign governments have spent this money; exporting West-
ern businesses have profited from the purchases; and the eco-
nomic future of the depositors has been put into jeopardy. It was
inevitable that they be sacrificed, either as taxpayers or as
depositors.!? They had already sold their spiritual birthrights for a
mess of humanist pottage. They ignored God, and in their time of
financial crisis, God may ignore them.

If national governments had prohibited fractional reserve
banking, and if the U.S. government had not authorized a private
corporation (the Federal Deposit Insurance Corporation) to

16. Antony Sutton, The Best Enemy Money Can Buy (Billings, Montana: Liberty
House Press, 1985), p. 25.

17. On the magnitude and inevitability of the bankruptcy, sce Lawrence
Malkin, The National Debt (New York: Henry Holt, 1987).
