162 Healer of the Nations

banish the power of sickness and death (below Him). Jesus also
was a man under authority. The centurion recognized clearly that
Jesus’ covenantal subordination to God was the basis of His
power over sickness and death, just as the centurion’s covenantal
subordination to Caesar was the basis of his power over his troops
and servants.

We have come “face to face” (representationally through the
printed word) to the doctrine of representation: to speak in someone
else’s name before God, and to speak to men in God’s name. This is the
structural basis of human authority in God's world of plural, hier-
archical, institutional authorities.

Baptizing Covenantal Representatives

People are baptized as individuals. They are also baptized as
members of families. Fathers are baptized as covenantal represen-
tatives of their households, as those who proclaim and enforce
God’s law in their homes. The baptism of fathers is supposed to
lead, Biblically, to baptism of their households.? We see this in the
baptism of the family of the Philippian jailer. Paul and Silas spoke
to the jailer in the name of God (as God’s lawful representatives):

So they said, “Believe on the Lord Jesus Christ, and you will be
saved, you and your household.” Then they spoke the word of the
Lord to him and to all who were in his house. And he took them
the same hour of the night and washed their stripes. And immedi-
ately he and all his family were baptized. Now when he had
brought them into his house, he set food before them; and he re-
joiced; having believed in God with all his household (Acts
16:31-34).

There is another aspect of family representation that is impor-
tant. God sanctifies unbelievers because of their believing
spouses. This does not mean that He saves them eternally. It does
mean that He sets them apart covenantally, treating them as special
people, because of their marriage vows (covenantal oaths). “For

3. Ray R. Sutton, That You May Prosper: Dominion By Covenant (Tyler, Texas:
Institute for Christian Economics, 1987), pp. 89-91.
