Conclusion 247

The Israelites in the wilderness feared death more than they
loved the idea of winning the promised land. What did God give
them? Death in the wilderness. If we fear the wrath of men more
than God, then God will deliver us into the hands of fearfully
wrathful men. We must heed the warning of Jesus:

“And do not fear them who kill the body but cannot kill the
soul. But rather fear Him who is able to destroy both soul and
body in hell” (Matthew 10:28).

The Rockefeller Panel on foreign policy correctly assessed
what the West needs to survive: “Tenacity of purpose as well as ca-
pacity for sacrifice, sustained over a long period, will be needed to
meet the present challenge.”33 Unfortunately for the deal-doing
humanists, the West has run out of both: tenacity of purpose and
capacity for sacrifice.

Chambers’ Vision

Whittaker Chambers saw the crisis of the humanist West in
1925, He described in 1952 his 1925 “moment of truth” that led him
to join the Communist Party. He was sitting on a bench at Colum-
bia University in New York City. “I was there to answer orice for all
two questions: Can a man go on living in a world that is dying? If
he can, what should he do in the crisis of the 20th century?” I have
never seen the crisis of this century summarized more eloquently.

There ran through my mind the only lines I remember from
the history textbook of my second go at college—two lines of
Savinus’, written in the fifth century when the Goths had been in
Rome and the Vandals were in Carthage [St. Augustine died at
age 76 in 430 a.p. in North Africa during this Vandal conquest—
G.N.]: “The Roman Empire is filled with misery, but it is lux-
urious. It is dying, but it laughs.”

The dying world of 1925 was without faith, hope, character,
understanding of its malady or will to overcome it. It was dying
but it laughed. And this laughter was not the defiance of a vigor

33. Prospect for America, p. 48.
