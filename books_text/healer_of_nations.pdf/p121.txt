Nations With Rwal Covenants Are Always at War 107

They therefore lie: to themselves and to those whom they repre-
sent. Solzhenitsyn has described this process: “Many present and
former U.S. diplomats have also used their office and authority to
help enshroud Soviet communism in a dangerous, explosive cloud
of vaporous arguments and illusions. Much of this legacy stems
from such diplomats of the Roosevelt school as Averill Harriman,
who to this day [1980] assures gullible Americans that the Kremlin
rulers are peace-loving men who just happen to be moved by heart-
felt compassion for the wartime suffering of their Soviet people.
(One need only recall the plight of the Crimean Tatars, who are
still barred from returning to the Crimea for the sole reason that
this would encroach upon Brezhnev’s hunting estates. )”"?

The Sovereignty of God

God is high above all men and events. He controls men and
events. “The king’s heart is in the hand of the Lorn, like the rivers
of water; He turns it wherever He wishes” (Proverbs 21:1). “Woe
to him who strives with his Maker!” (Isaiah 45:9a). God is transcen-
dent. Yet He is also present with us. It is He who gives the victory.
“Some trust in chariots, and some in horses; but we will remem-
ber the name of the Lorp our God. They have bowed down and
fallen; but we have risen and stand upright” (Psalm 20:7-8).

We are in what appears to be the final stages of a conflict be-
tween a civilization that was once built on faith in God and
another that was self-consciously built on atheism. There should
be no doubt as to which civilization will win, if one of them trusts
in the strong arm of God. Our problem comes from the West’s
departure from this older faith. Our Communist opponents have
made a religion of chariots. They have bet everything on the power
of military hardware and the effectiveness of subversion. The Bible
says that such tactics cannot win the long-run battle. Sadly for us,
such tactics can and do win short-term battles when they are not
challenged by strategies and tactics built on faith in God.

10. Aleksandr Solzhenitsyn, “Misconceptions About Russia Are a Threat to
America,” Foreign Afairs (Spring 1980), p. 806.
