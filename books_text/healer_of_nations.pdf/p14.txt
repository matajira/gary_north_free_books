xiv Healer of the Nations

May God use Col. North’s testimony to begin a flanking
movement around the humanist leadership on both sides of the
Tron Curtain. May Oliver North turn out to be more than a two-
week celebrity. And may Christians begin to sort out the funda-
mental principles of international relations, and get them adopted
around the world, so that no future patriotic lieutenant colonel
will find himself $2 million in debt to lawyers because he did his
job well.
