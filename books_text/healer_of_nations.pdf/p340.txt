326 Hrealer of the Nations

covenant model, 43, 76
no escape, 7f
see also bureaucracy, elite
Hiram of Tyre, 21
Hiss, Alger, 289
history, 2, 135
Hitler, 69, 118, 135, 136, 168, 212
holiness, 163
Holy Communion, 10, 73-74
Holy Spirit
Christian nationhood, 34
healing, 156
power of, 53
world order, 77
see also revival
Hong Kong, 221
horses, 126, 127
humanism
anti-confessional, 48
Christian alliance, 6, 53, 217, 236
Communism &, 38
crisis of, 20
elites; see elite
empires; see empire
internationalism, 63-73
isolationism, 73-76
loss of faith, 242-48
man as god, 67, 70, 71, 88
man as sovereign, 106
nation-state, 30
occultism, 285
one-world order, 60, 65-66
pluralism, 142
political, xiii
power, 30
statist theology, 4
stronger-weaker, 237
theology of, 4-5
treaties, 15
vs, providence, 187
world democracy, 15!
Humanist Manifesto, 64
The Humanist, 178

humanists
Christian alliance, 177
foreign policy, 177
Foreign Service, 209
inheritance, 93
retreat of, xii
right to rule?, xii
Hunt, Dave, 97

idolatry, 69
immigration, 48-49, 146, 169, 279
immortality, 144
imputation, 141
incarnation, 31
inclusion, 25
inflation, 189
inheritance
default of, 93
kingdom of God;
see Matthew, 21:43
of earth, 217
international law, 94;
see also natural law
international relations
centralization of, 17
common law &, 3
crisis in, 10
justice & 12
private, 17
state monopoly?, 277-78
see also elite, foreign policy, Foreign.
Service, State Department
internationalism
by sacrifice, 205
Christian, 46-63, 73, 75, 94,
159, 230, 259-74
humanist, 63-73
interpretation, 31
“invisible government,” 16, 181
isolation, 13
isolationism
churches, 267
conservative, ix
