Businessmen Make the Best Diplomats 193

It is not enough to clothe people if they do not learn the eco-
nomic laws through which God says that men must clothe them-
selves: work, thrift, planning, personal responsibility, meeting the
economic needs of others, etc, It is also not enough to make
them wealthier if they do not honor God:

“You have sown much, and bring in little; you eat, but do not
have enough; you drink, but you are not filled with drink; you
clothe yourselves, but no one is warm. And he who earns wages,
earns wages to put into a bag with holes.” Thus says the Lorp of
hosts: “Consider your ways! Go up to the mountains and bring
wood and build the temple, that I may take pleasure in it and be
glorified,” says the Lorp. “You looked for much, but indeed it
came to little; and when you brought it home, I blew it away.
Why? says the Lorp of hosts. “Because of My house that is in
ruins, while every one of you runs to his own house. Therefore the
heavens above you withhold the dew, and the earth withholds its
fruie” (Haggai 1:6-10).

Who will tell them? Humanists in the Foreign Service? Of
course not. Christian businessmen must do it. This requires that
some Christians become skilled, successful businessmen. They
must learn fluency in a foreign language. They must learn the
ways of transmitting the practical gospel of Christ into a foreign
culture, by way of business skills that must also be imparted. I
think this is why Christians have been willing to walk away from
personal responsibility: a huge inferiority complex. They do not
believe that they can compete, because they do not believe that
covenantal faithfulness to God’s law works. In short, they are not
Christian pragmatists. They refuse to do business (pragmatenomai)
until He comes. They prefer to let State Department humanists
represent their nation, meaning the citizens of their nation, to the
world. And since these Foreign Service Officers are dedicated hu-
manists, this means that Christians are ready to allow these men
to represent this nation in the name of the God-denying theology
of humanism.

33. Gary North, Jeherit the Earth: Biblical Blueprints for Economics (Ft, Worth,
‘Texas: Dominion Press, 1987),
