232 Healer of the Nations

cause and effect in history.

7. When he is successful in this deception, the transfer process
is delayed.

8. This deception cannot go on indefinitely, for God eventu-
ally brings external judgment on the receivers.

9. Today, Satan has persuaded Christians to accept and even
vote for programs of compulsory wealth redistribution.

10. This wealth transfer subsidizes evil,

11. Government-to-government aid strengthens the control of
governments on both sides of the transaction.

12, Economic growth is therefore retarded in the recipient na-
tions.

13, The fastest growing underdeveloped nations have had free
markets and relatively little economic aid from foreign govern-
ments.

14, They have accepted the Protestant work ethic.

15. The concept of “Third World” is created by foreign aid pro-
grams.

16. The banking crisis is really a foreign aid crisis.

17. Government-protected banks have loaned depositors’
money to insolvent backward nations.

18. Bankers have trusted foreign governments rather than for-
eign private borrowers.

19. By making interest payments on schedule, foreign govern-
ments have borrowed ever-more money.

20. The principal can never be repaid.

21. The goods of the West have been transferred.

22. The economic future of the West is in jeopardy.

23. Christians will suffer because they allowed humanists to
represent them, as politicians, bureaucrats, and bankers.

24. Foreign aid should be limited to military aid.

25. The military services should finance it.

26. They should buy services they need from allied nations.

27. Foreign policy should distinguish between allies, neutrals,
and declared enemies that have not yet begun shooting at us.

28. Foreign aid should be private: charity and business invest-
ment.

29. The goal is to extend the kingdom through a program of
covenantal adoption.

30. Compulsory foreign aid is an attack on the redeemed family
of man.
