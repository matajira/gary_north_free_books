God Created the Nations A

nanted nation must reflect this commitment. The goal of Chris-
tians in politics should be to extend the visible kingdom of God in
the realm of political life, just as it is to be extended in the non-
political realms. The goal should be progressively to restrict the
influence of non-Biblical law. This is a bottom-up political process
that must begin with individual self-government under Biblical
law.

Clearly, to establish a godly foreign policy, Christians must
first establish Christianity as the religion of their nations. The
West needs a revival. So does the East. Any discussion of foreign
policy today that presupposes that we live in a world of neutral
nations has already given away the case for Christian international
relations. The assumption of neutrality leads to the erroneous con-
clusion that a nation’s foreign policy will be constructed either in
terms of the principle of humanist internationalism (empire or
alliances) or in terms of humanist isolationism. Both approaches to
foreign policy are wrong. What we need is Christianity, not the
myth of neutrality,

In summary:

1. God is transcendent over all the kingdoms of men,

2. Ail human kingdoms are temporary.

3. God is present with all men: the work of His law in each
heart.

4, God is present in a special way with His people: the king-
dom of God.

5. A nation is a group of people who are joined together geo-
graphically in a civil covenant.

6. The covenant has five parts.

7. A nation begins with a shared faith in God or some other ul-
timate sovereign agent.

8. A nation has five aspects: shared language, the authority to
impose taxes (and escape taxation), common laws within a shared
boundary, a common confession (oath), and citizenship require-
ments.

9. All citizenship requires exclusion.

10. The proper basis of political exclusion is the Biblical cove-
nant.
