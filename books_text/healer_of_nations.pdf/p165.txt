Ali People Are Citizens of Two Worlds 151

bearing the fruits of it” (Matthew 21:43). This transfer took place
at Pentecost and was confirmed by the destruction of the temple
in 70 a.p. This kingdom grows in history. After the final judg-
ment, the “nation” of Matthew 21:43—the Church International
— becomes identical to members of Christ’s international church.
The question is: To what extent does nationhood in history pro-
gressively reflect this definitive and final definition of nationhood?
Will there be high positive correlation (international theocracy)?
Will there be no correlation (random: permanent democratic
pluralism)? Or will there be a negative correlation (the triumph of
Satan’s world religion and empire in a one-State world)?

The historical optimists see the steady spread of the gospel,
the expansion of missionary activities, and a bottom-up restora-
tion of all things, as God’s kingdom is progressively manifested in
history. The political neutralists see political freedom in terms of
Western secular humanism’s official “party line”: world liberation
through world democracy, though without the triumph of the gos-
pel in history. The pessimists see an overnight imposition of world
theocracy under Jesus’ physical rule during the millennium, but
with political decline into tyranny prior to Christ’s coming. The
European Christian traditionalists, denying a visible millennium
in history, see only political decline into tyranny in history.?

Let us choose victory rather than defeat as our goal.

Summary

Because of the covenantal nature of all life, all men are under
one of two covenants: God’s or Satan’s. The cosmic conflict that
goes on between the forces of God and the forces of Satan is re-
flected in every covenantal institution, including national civil
governments. The goal of each side is to make manifest in history
the religious principles of its respective leader, either God or
Satan. Men seek to create either heaven on earth or hell on earth.
There is no third option, because there is no neutrality.

9. Gary North, Dominion and Common Grace: The Biblical Basis of Progress (Tyler,
Texas: Institute for Christian Economics, 1987}, ch. 4.
