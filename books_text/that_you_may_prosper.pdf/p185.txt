The Biblical Church Covenant 165

static: It is always either growing or dying. I have found that the big-
gest trouble-makers in the Church are those who never do anything con-
structive. They are not taking dominion. Consequently, they end up
out-of-sorts with the whole congregation, and either leave or find
themselves being disciplined. One either dominates for Christ, or he
is dominated.

The Two Tables

There is no neutrality, no safety zone between dominion and
being dominated. The Apostle Paul summarizes continuity in terms
of two tables. He says,

You cannot drink the cup of the Lord and the cup of demons; you can-
not partake of the table of the Lord and the table of demons (I Cor, 10:21).

 

There are only two tables in che world. Man either cats Christ's
meal or he eats Satan’s. Christ’s meal is a covenant of life. Satan’s is
a covenant of death. Christ’s meal is focused in the Lord’s Supper.
This table, as we have said time and again, is not magical. Grace is
not infused by the elements themselves. The acé itself of eating com-
munion with God’s people is covenant renewal.

But what of the other table, the table of demons? Where is it?
Paul leads us to believe that man imescapably eats with the devil if he
does not eat with Christ. Everything he does becomes a rite of cove-
nant renewal with the devil: politics, science, education, athletics,
and % forth, Every activity of the unbeliever, or perhaps the professed
believer who never communes, takes on a satanic sacramental char-
acter. Yes, the devil’s table becomes a life apart from Christ.

Self-conscious pagans speak of their work in a sacramental way.
Poltics, art, economics, or whatever are viewed as somehow infusing
the world with life. They become “sacraments.” If Christ’s table is
not the answer, something else is perceived as the source of life.
Often, a meal is connected with it. Consider a simple example: mod-
ern athletics. Remember, athletics in the ancient world, particularly
the Olympic Games, became a way of determining who was a god.

 

 

Today, there is almost always a superstitious ritual before the gamc.
Athletes, who don’t go to church on Sunday, have their last special
meal before the big game. This is not satanic in and of itself. But it
merely points to the fact that man has to find a substitute commun-
ion. If the last meal with the athletes is a substitute for Christ’s table,
it is demonic. If anything is a substitute sacrament, it is demonic.

 
