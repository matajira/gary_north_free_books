302 THAT YOU MAY PROSPER

death was simultaneously a circumcision and baptismal curse.

Although circumcision symbolized death, baptism symbolized a
greater death because it represented the death of Christ in a way that
circumcision could not. How? It was death unto life. Paul says, “Do
you not know that all of us who have been baptized into Christ Jesus
have been baptized into His death? Therefore we have been buried
with Him through baptism into death in order that as Christ was
raised from the dead through the glory of the Father, so we too might
walk in newness of life” (Rom, 6:3-4), Circumcision was death unto
death. Baptism was death unio life.

So, baptism has greater Azlling power. It represents the Holy
Spirit, whose Life is so powerful that instant death can come to the
one who betrays his baptism. Of the early church we read:

But a certain man named Ananias, with his wile Saphira, sold a piece
of property, and kept back some of the price for himself, with his wife’s full
knowledge, and bringing a portion of it, he laid it at the apostles’ feet. But
Peter said, “Ananias, why has Satan filled your heart to lie to the Holy
Spirit, and to keep back some of the price of the land? While it remained
unsold, did it not remain your own? And after it was sold, was it not under
your control? Why is it that you have conceived this deed in your heart?
You have not lied to men, but God.” And as he heard these words, Ananias
fell down and breathed his last; and great fear cane upon all who heard of
it... . And his wife came in, not knowing what had happened. And Peter
responded to her, “Tell me whether you sold the land for such and such a
price?” And she said, “Yes, that was the price.” Then Peter said to her, “Why
is it that you have agreed together to put the Spirit of the Lord to the test?
Behold, the feet of those who have buried your husband are at the door, and
they shall carry you out as well.” And she fell immediately at his fect, and
breathed her last (Acts 5:1-11).

How could Satan fill a baptized person’s heart? Because baptism
does not save anyone. Baptism represents the two-fold sanction of
the Holy Spirit. To be united to Christ means great life or horren-
dous death, depending on the faithfulness of the one baptized. There
was a similar incident with Achan in the Old Testament (Josh. 7).
But even in that case, the people carried out the death penalty ( Josh.
7:25), In the New Covenant, the Holy Spirit is more directly in-
volved in the death of those who break covenant.

Baptism is a sanction of death. Like circumcision, it killed the
“flesh,” but unlike the Old Covenant sacrament, it killed £y the Spirit.
