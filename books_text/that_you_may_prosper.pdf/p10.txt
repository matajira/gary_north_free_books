x THAT YOU MAY PROSPER

you shall have no other gods before Me.” The heart of the Covenant
Law is the Decalogue, which these words preface and introduce.

Sutton’s Book

The book you now hold in your hand is doubtless the clearest
exposition of Bible-as-covenant (that is, Bible as meant to be under-
stood) that you've ever read. That's because the author has spelled
out in no uncertain terms the implications of historic reformational
covenant theology in the light of current scholarship. The discovery
and application to biblical studies of the suzerain treaty parallels to
Sacred Scripture, contemporary to and confirmatory of the Pentateuch,
has been extremely enlightening and valuable for appreciation of the
orientation of the Word of God in its entirety.

Others, such as Meredith G. Kline, have published helpful sug-
gestions concerning archaeological and philological discoveries in
this area. But Ray Sutton has now both simplified and expanded
upon the rich lode made available through modern research. This he
does by citing the biblical reasons for historic successes and failures
in human history. In the realms of family, church, and state —cove-
nant institutions by divine design—only when there is conformity to
the biblical pattern and requirements of covenantal relationships is
divine blessing to be expected and experienced.

Whether you agree with every idea propounded in this volume
by its author, experienced pastor and enthusiastic Christian educa-
tor that he is, it will surely make you think. Its commanding logic
demands your interaction with the flow of reasoning and its often
surprisingly fresh suggestions will prove a stimulus and assistance to
your formation of judgments of your own, For example, not every-
one will readily accept the seemingly facile manner in which several
Books of the Bible are outlined on the same covenantal pattern read-
ily found in Pentateuchal Books like Exodus and Deuteronomy. At
least not the first time around. But when one becomes convinced of
the centrality and importance of the covenantal form and content
(structure and specifications) so vital to Holy Scripture as a whole
and in its parts, some such analysis of portions of the Bible makes
real sense.

Fresh insights into God’s Word are sure to be gained, to say the
least, through Sutton’s work. I found it to be so, after nearly half a
century of serious study and teaching of the Bible. Thinking through
this book will enable you to focus upon and relate by covenantal
