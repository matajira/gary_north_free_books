The Ten Commandments 217

2. Second Commandment: Hierarchy

The second commandment moves to the next principle of the
covenant, a hierarchy of obedience (“worship and serve”), In the
Deuteronomic covenant, the hierarchical section closes by associat-
ing rebellion to the sin of idolatry. Moses says, “So watch yourselves
carefully, since you did not sce any form on the day the Lord spoke
to you at Horeb from the midst of the fire, lest you act corruptly and
make a graven image” (Deut. 4:15-16). Thus, the second command-
ment follows the same pattern, connecting worship and submission,
“service” (Exod. 20:5).

The history of Israel’s redemption is also the backdrop. God for-
bids worship of any sort of idol. ‘The specific outline is “from heaven
above to the earth, to anything under the sea or earth.” It is possible
that the commandment is written this way to counter a “hierarchy”
among the Egyptian false gods. They worshipped life above, below,
and especiatly the Nile itself. Birds and animals of the Nile were
worshipped because it was believed that the “Great River” was a ser-
pent providing life to the world above, below, and all around. Clearly
this refers to the imagery of the serpent in the garden, a pagan hier-
archy. Even the way God condemns idolatry develops a certain
“false” hierarchy.

God’s hierarchy places all authority in Him. Anyone else only
has delegated responsibility. Transcendence is not shifted from God
to man, or to creation for that matter. Egyptian religion had a hier-
archy of authority that placed the Pharaoh in the center of the world.
He was half god and half man, a perfect “false” incarnation, He
mediated life to che world. The animals were simply “emanations”
from him, possessing a little “less” deity.

This created a pyramidal hierarchy with man at the top of the
pyramid. The pyramid structure is not itself inherently bad, since it
is the “mountain model” found throughout the Bible? The pyramid
was simply a cheap (or shall I say, rather expensive) copy of God’s
mountain dwelling, But God’s mountain-pyramid always has God
on top of the mountain, His hierarchy begins with God, not man, To
worship a “created” thing is to place creation at the top of the moun-
tain. The result: tyranny like that of Egypt.t

  

3. Richard J. Clifford, The Cosmic Mountain In Canaan and The Ole Testament (Cam-
bridge, Massachussetts: Harvard University Press, 1972), pp. 25-28.
4. R. J. Rushdoony, The One and the Many, pp. 36-37,
