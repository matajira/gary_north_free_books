Ethics 7

So, we begin with a mechanical explanation of cause and effect,
and end in collectivism. The Biblical covenant stands in stark con-
trast. God is personal. He created the world. He also created mathe-
matics. A formula may explain what happens in a physical sense,
but not why. The water cycle can be studied, for example, but this
does not explain why famines occur. God sends famines as a curse.
Everything that happens is the result of covenant-keeping or covenant- breaking.
Life is not mechanical.

2. Manipulative Cause/Effect

J call this false explanation of cause and effect “manipulative”
because force is often viewed as the basis of life. Brute power creates
cause and effect. The manipulation can be in the form of personal
theft or injury to another, or it can come in the shape of national theft
or murder. The conflict is always the same: right vs. might. Theories
of power believe that might creates cause and effect. When they are
implemented, as Shakespeare said through Ulysses in Troilus and
Cressida,

Then right and wrong
Should lose their names, and so should justice too,
Then everything includes itself in power,
Power into will, will into appetite,
And appetite, a universal wolf,
So doubly seconded with will and power,
Must make perforce a universal prey,
And last eat up itself.

Interestingly, Shakespeare observed that “right and wrong” dis-
appear when “power” rules. Man is not ruled by an ethical standard;
instead, he is left to the whims of a power-State.

The struggle of “right vs. might” is one of the basic movements of
the Bible, Fallen man tends to replace the covenant with force. But we can
rest assured that “right” prevails—that there is always an ethical
cause/effect relationship — because the Bible also tells the story of the
re-establishment of rule by covenant. The plan of redemption,
therefore, constantly clashes with the power-State. Finally, redemp-
tion wins out!

Consider Cain, He was driven to the east of Eden. The Biblical
text calls attention to the kind of society he established. The descen-
dant of Cain says, “I have killed a man for wounding me; and a boy
for striking me; if Cain is avenged sevenfold, then Lamech seventy-
sevenfold” (Gen. 4:23-24). The sons of Cain were destroyed, haw-
