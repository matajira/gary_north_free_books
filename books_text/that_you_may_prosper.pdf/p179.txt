10
THE BIBLICAL CHURCH COVENANT

To demonstrate the covenantal character of the Church, let us
turn to a section of Scripture that follows the five-fold structure,
Revelation, In the “letters” segment (Rev, 2-3), we see that even this
sub-section is drafted in a covenant format, Each letter is outlined accord-
ing to the five-point model. The first Church, Ephesus, is an example.

To the angel of the church in Ephesus write: The One Who holds the
seven stars in His right hand, the One who walks among the seven golden
lampstands, says this: “I know your deeds and your toil and perseverance,
and that you cannot endure evil men, and you put to the test those who call
themselves apostles, and they are not, and you found them to be false; and
you have perseverance and have endured for My name’s sake, and have not
grown weary. But I have this against you, that you have left your first love.
Remember therefore from where you have fallen, and repent and do the
deeds you did at first; or clse E am coming to you, and will remove your
lampstand out of its place — unless you repent. Yet this you do have, that you
hate the deeds of the Nicolaitans, which { also hate. He who has an ear, let
him hear what the Spirit says to the churches. To hirn who overcomes, 1 will
grant to eat of the tree of life, which is in the Paradise of God” (Rev, 2:1-7).

 

:1)

The covenant always begins by distinguishing God from man.
The preamble to this letter tells us that the words come from God,
“The One Who holds the seven stars.” In the previous chapter, we
are told Who this is.

True Transcendence (Rev.

And when I saw Him, I fell at His feet as a dead man. And He laid His
right hand upon me, saying, “Do not be afraid; I am the first and the last,
and the living One; and I was dead, and behold, { am alive forevermore,
and I have the keys of deaih and of Hades. Write therefore the things which

1, See Appendix 5.
159
