The Biblical State Covenant 189

ity of Old Testament penalties should still be instituted. As earlier
sections of this book indicate, the proper hermeneutic for determin-
ing what carries over into the New Testament is the principle: con-
tinue what is not changed in the New Testament, This would apply
to the penal sanctions of the Old Testament. The death penalty
offenses that should be extended into the New Testament are witch-
craft (Deut. 18:10-11), idolatry (Deut. 13:10), murder (Gen. ),
blasphemy (Lev. 24:11-23), homosexuality (Lev. 18:22-29), bestiality
(Lev. 18:23), rape (Deut, 22:25-27), adultery (Lev. 20:10), incest
(Lev. 20:14), incovrigibility of teenagers (Deut. 21:18-20), kidnap-
ping (Exod. 21:16), and some instances of perjury (Deut. 19:19-20).

Second, the New Testament penal sanctions are dissimilar to the
Old Testament. In some instances they can be more lenient. After the
shift from wrath to grace in history, “reformability” has greater possi-
bility. In the Old Covenant, before redemption comes in history, the
negative influence of wickedness is so great that it cannot be over-
come, It would seem that virtually all of the “bad clements” of society
would have had to be killed for this reason, But in the New Cove-
nant, the kingdom of God has a positive effect on wickedness. Some
of the wicked —indeed many more than could have been in the Old
Covenant—can be restored. We discover that former homosexuals,
for example, are in the Church of Corinth (I Gor, 6:11). No death
penalty is called for, Thus, we see how Paul can speak of the similar-
ity of Old Testament sanctions in Romans 1, and yet maintain the
possibility that not every convicted homosexual would have to be put
to death according to 1 Corinthians 6, In the New Covenant Age,
only the “unreformable” element would be put to death.

In some instances, however, the New Covenant sanctions are
stricter than the Old Testament. Paul allows in Romans 1:30-31 for
other offenses that can draw the death penalty: arrogance, unmerci-
ful, strife and others. Why does the New Testament speak this way?
Some of these offenses have historical precedent. For example, God
put Korah and family to death because they caused “strife” (cf. Rom.
1:29 & Nu. 16:1-50), But at first glance, ic might not seem possible to
commit an offense tied to “arrogance” (Rom. 1:30). Modern socicty,
however, presents some situations where the death penalty would be
appropriate. For example, as recently as Hitler’s reign of terror, we
find people committing horrible atrocities in the name of “Super-
race doctrine.” Their racial “arrogance” involved such things as fron-
tal lobotomies on Jews. According to PauY’s language, therefore, a

 

 

 

 

 
