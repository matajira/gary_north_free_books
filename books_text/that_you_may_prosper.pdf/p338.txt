318 THAT YOU MAY PROSPER

36-39 (Continuity): The final section of the articles has to do with
aspects of continuity. They begin with the consecration of bishops
and ministers (Article 36). Next, the role of the magistrate and the
status of one’s personal possessions (inheritance) are developed.
Finally, article thirty-nine concludes on the oath. This may seem like
a departure from the covenant structure, seeing the oath was dis-
cussed under thé sanctions point of covenantalism. But we should
keep in mind that instructions about the oath were also included in
the fifth point, concerning allegiance to the covenant head and the
reading of Scripture. Probably, the oath-article comes at the end
because the Articles deal with the errors of Anabaptism at this point.
And, closing on the oath emphasizes thc need to subscribe to the
Articles.

Whether the designers of the Thirty-Nine Articles were aware of
this obvious covenantal influence or not, I don’t know. I rather doubt
it. Nevertheless, the document easily follows the pattern. The real
value in understanding this covenant structure is theological. It
means that the theological system of thc Anglican Church is defi-
nitely Augustinian and Reformed. It is covenantal!

So what? Covenant theology keeps one from falling into Anabap-
tism at one end of the spectrum and Sacerdotalism? at the other end.
In Anglicanism’s case, it guards against the recurring Anglo-
Catholic trends. For example, the liturgy of the Anglican Church is
powerful. But unless it is understood as covenant renewal, it tends to
become a re-enactment (not of covenant cutting) of the death of Christ.
It becomes a magical ceremony where Christ is re-crucified. When
this happens, the true strength of the liturgy is lost. This was the
concern of the Reformers. This must be the concern of anyone who
truly believes in covenant theology. And, I hope this will always be
the concern of the Anglican Church, An understanding of the cove-
nantal influence and structure of its most central doctrinal standard
is the key to feeding and continuing this concern.

3. Roman Catholicism with its particular doctrine of transubstantiation,
