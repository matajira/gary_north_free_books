14
CONCLUSION: LITTLE BY LITTLE

“I will drive them out before you little by little, until you become
fruitful and possess the land” (Exod. 23:30).

We come to the end of our study of the covenant. The final ques-
tion to be answered is, “How do we establish a society based on the
concepts presented in this book?” I raise it because I do not want
there to be any confusion about how a Christian society is created. I
do not want the reader to leave this book thinking a covenantal cul-
ture comes from the top down, meaning by some “theocratic elite” for-
cing everyone to be a Christian, or believe a certain way. Nothing
could be farther from the truth.

God told the Israelites that their Biblical culture would come “lit-
tle by little.” It did not come suddenly, or overnight. It came gradu-
ally. The covenantal society that I have proposed can only come the
same way. That is, if it is to survive, it must come about from the bot-
tom up. Sure, the reader can implement the covenant structure in his
home and can seek to establish it in his church. But its fulfillment in
society-at-large will be much more difficult. It can only successfully
come about (and stick), if it takes hold at a grass-roots level through
evangelism.

The expansion of the Gospel from Jerusalem to Rome serves as
an example. Jesus says at the beginning of Acts, “You shall receive
My power when the Holy Spirit has come upon you; and you shall
be My witnessess both in Jerusalem, and in all Judea and Samaria,
and even to the remotest part of the earth” (Acts 1:8). This verse
summarizes the spread of the Gospel from one part of the world to
the rest. It began in Jerusalem, and ended up in Rome. The method
was Little-by-little evangelism, just like the land of Canaan.

Yes, Acts parallels the Book of Joshua. Joshua is the account of
the conquest of the land; Acts is the story of the conquest of the world.

202
