104 THAT YOU MAY PROSPER

does not say. They were nevertheless sons by declaration, and the lay-
ing on of hands was a judicial act that simply declared who the heirs
were, symbolizing a transfer from one generation to the next.

What happens to Joseph’s sons does not disrupt the cavenantal
progression from ratification to continuity that we sce in Deutcron-
omy, Nor should the Joseph situation confuse in the reader’s mind
ratification and the last step of covenantalism. The Deuteronomic
covenant places ratification before the final stage of covenantalism;
most of the time ratification and continuity are simultaneous to one
another, which may explain the Joseph account. At any rate, ratifi-
cation does provide an element of continuity, It (circumcision in the
Old Covenant and baptism in the New Covenant) initially estab-
lishes continuity. Ratification places legal claim on the recipient by
means of the “first” sacrament. It entitles him to an inheritance. It
officially guarantees a legacy even to the household, as we learned in
the last chapter. So it is obvious that the fourth and fifth points of the
covenant are inseparably bound. But the final point of the covenant,
what I am calling “continuity,” is a continuation of ratification. It is the
means whereby the claim is actually extended. It is a confirmation
process that usually involves the sccond sacrament (passover in the
Old Covenant and communion in the New Covenant) as we shall
see. In Deuteronomy, the continuity section begins with the laying
on of hands (via a sacred meal), and then it moves to a second ele-
ment that we also see in the Joseph account.

Second, the covenant transfer is confirmed by the actual conquest of
the land. After Joseph’s second-born becomes the heir, Jacob
describes how God will bring the nation to the Promised Land again
(Gen. 48:19-22). If there is no actual conquest of the land, Jacob’s
transfer is invalidated. This would mean his heirs are not the true
heirs, and he would not get to come back to the land, To the victors
go, not the spoils, but in this case, the inheritance.

Third, the inheritance is net confirmed on the basis of natural des-
cent. Man’s fallen nature disrupted the natural flow of things. Nor-
mally, the first-born would have been the recipient of the double por-
tion of the blessing, but the Fall of man morally disabled him, In the
Bible, the first-born is covenantally unable to obtain the inheritance:
Adam, Cain, Esau, Ishmacl, Reuben, and so forth. The constant
failure of the first-born goes back to Adam, and the constant success
of the second-born points forward to Jesus, the second Adam and
truc first-born.

This brief introduction takes us directly to the fifth point of cove-
nantalism in Deuteronomy (31-34). The obvious connection is that
