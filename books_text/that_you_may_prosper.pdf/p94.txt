74 THAT YOU MAY PROSPER

“We know Jesus and Paul but who are you?” With that, the evil
spirits viciously attacked the men. To these exorcists, power was in
an equation, in this case the name of Jesus. They discovered that
Jesus cannot be maneuvered by any formula. The exarcist’s use of a
formula also explains why science so easily slips into magic. Both tend
to reduce life to an equation. Atheistic science seeks to manipulate
the universe through an equation, and magic uses an incantational

formula.
Ethics, not elitist formulas, is the key to Biblical religion. David
says, “Thou dost not delight in sacrifice . . . with burnt offering.

The sacrifices of God are a broken heart” (Ps. 51:16). Then David
continucs on the ethical note, “By Thy favor do good to Zion, Build the
walls of Jerusalem, then Thou wilt delight in righteous sacrifices . . .
in whole burnt offering” (Ps. 51:18-19), Biblical religion has “ritual,”
or perhaps a better word would be “routine.” But the ritual of the
Bible follows the covenant. David calls for covenantal faithfulness to
make the routines of offering sacrifices legitimate. Without faithful-
ness, the sacrifices were useless.

Mediation By Magic

The problem of mediation is basic to the question of cause and
effect. Man cannot escape his position as God’s subordinate over the
world. Man calls on God’s power to assist him in his dominion tasks.
The question is: How does he calt on God? The Biblical answer is
that man calls on God as the covenant God who enforces the terms
of His covenant in history, But the magician, like the humanist, can-
not admit this. That places too much emphasis on the terms of the
covenant: ethics. So he seeks other ways of calling on God.

The heart of the humanist Renaissance was magic. It was a self-
conscious attempt to revive pagan humanism, including classical
magic. The fundamental assumption of magic is the chain of being
idea: “As above, so below.” God and the heavens are essentially like
man and his environment. So there must be a search for the appro-
priate means of intermediating between heaven and earth. This is the
magician’s quest. Frances Yates describes this rival worldview:

 

For the All was One, united by an infinitely complex system of relation-
ships. The magician was the one who knew how to enter into the system,

15, Ibid., p. 136. North discusses how Protestantism created a superior work ethic
by emphasizing routine.
