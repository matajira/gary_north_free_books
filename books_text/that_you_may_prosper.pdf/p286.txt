266 THAT YOU MAY PROSPER

grant (Neh, 2), the Holy City of Israel. Why would the king do such a
thing? Nehemiah had been his “cupbearer,” and had held all of the
king’s properties in trust. Nehemiah was given hts land because he had
been fazthful to the king. This is precisely what happens with the dis-
ciples. They become guardians of the cup, the Lord’s estate. For
their faithfulness, they reccive their own property and land. The
“cup of blessing” symbolized the entire cstate of the King of kings.

Jesus is quickly betrayed after the “cup of blessing” is given. The
“cup” actually turns into an ordeal of jealousy. Jesus notes a betrayer, or
“bastard,” in their midst. Judas is identified in terms of the cup when
Christ says, “But behold, the hand of the one betraying Me is with
Me on the table” (Luke 22:21), “that is the one for whom I shall dip
the morsel and give it to him” ( John 13:26), Christ served this first
communion by dipping the bread in the “cup of blessing,” signifying
the transferral of inheritance from one person to the next. Judas
quickly betrayed the Lord, “Satan having entered into him” immedi-
ately after he ate ( John 13:27).

In the last section of Revelation, the “bowls” are poured out on
the Old Covenant city and people. Even Satan meets the judgment
of these bowls (Rev. 20). As Jesus used the “chalice” to create contin-
uity with the faithful, and reveal discontinuity with the “bastards” of
the covenant, the continuity section of Revelation does the same.

The “chalices” create continuity between God and His people by
the destruction of the Old Covenant worshippers, the Great Whore
and the Beast. At the end of the continuity section, the new Temple,
City, and home of Gad’s elect appears, a city which invites those who
want to live there to come in (Rev. 22:17). No longer is the covenant
exclusively Jewish. The “nations” (Gentiles) are ready to come in, sit
down, worship, and eat at the new “Tree of life” (Rev. 15:4).

Conclusion

This completes our study of the patterns of the Deuteronomic
covenant. How appropriate that we end on the Book of Revelation.
John’s message was forged on the anvil of the Mosaic pattern, the
five parts of the Apocalypse perfectly matching the five sections of
Deuteronomy.

One matter remains to be considered in the covenant half of this
book. I have demonstrated that the Deutcronomic covenant con-
tinues into the New Testament and have noted the parallels. But,
does this mean there are no changes in the New Covenant? Are we
