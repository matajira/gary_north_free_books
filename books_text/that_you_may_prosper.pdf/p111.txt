Sanctions 91

ately he was baptized, he and all his household” (Acts 16:33).

Luke does not have to stop, because the New Testament builds
on the Old. God redeems the family, as well as individuals, by plac-
ing them under the sanctions. Remember, the covenant has terms of
unconditionality. The children may grow up and deny the covenant.
They may fall away. But of course, this is a possibility for adults, The
sanctions are familial. Cut off the children, and one effectively destroys
the future!

We come to the end of our discussion on the second aspect of cut-
ting a covenant, and we come to the third and final element.

 

 

Til. Witnesses

The last but not least important aspect of cutting a covenant is
the role of the witness. Notice that Moses calls two witnesses to
verify the ratification of the covenant. He says, “I call heaven and earth
to witness against you today, that I have set before you life and
death, the blessing and the curse, So choose life in order that you
may live, you and your descendants [seed]” (Deut. 30:19). Witnesses
make the ratification oficial. They testify to the fact that a covenant was
actually made, whether the recipients were serious or not, T add the
last statement because the attitude of the recipient when the cove-
nant is ratified is not of primary importance. God's disposition is of
main concern, Even though someone might not take his entrance
into the covenant very seriously, God does! He will make sure the
sanctions of the covenant are carried out one way or the other. These
witnesses testify to the fact that God’s oath has been consigned to the can-
didate, As a matter of fact, should the covenant be broken, the
witnesses are called forward to prosecute the one who has actually
broken the covenant, called a covenant lawsuit.

The “otlicialness” of entering the covenant raises an important
question. When is a person considered a Christian? When he says he
is a Christian or when he is baptized (ratifying the covenant by sanc-
tion, oath, and witnesses)? Notice that the question is not “When ts a
person a Christian?”, but when is he considered a Christian? This
distinction is quite important.

For one, certainly belief in Christ is the basis of personal salva-
tion, but any professing believer who will not be baptized and come
under the accountability of a local church is probably not a true
Christian. He is like a man and woman who say they love each other
but will not make their love “official” in a marriage ceremony. Most
