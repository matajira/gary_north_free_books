70 THAT YOU MAY PROSPER

seventeenth-century social philosopher Thomas Hobbes, for exam-
ple, discovered Euclid’s Geometry at the mid-point of his life. He was
not a mathematician, but when he read Euclid, he was impressed.
“Here was the first and only book he had ever found in which every
proposition was incontrovertible. Why were there no other books
like it? And why not write a philosophy of man and socicty, that
troublesome subject of controversy in all past ages, in a similar style
of step by step demonstration? This project Hobbes decided to make
his own,”9 He produced a mathematical explanation of the universe,
In the Epistle Dedicatory to De Cive he writes,

‘Thuly the Geometricians have very admirably performed their part. . . .
If the moral philosophers had as happily discharged their duty, I know not
what could have been added by human industry to the completion of that
happiness, which is consistent with human life. For were che naturc of
quantity in geometrical figures, the strength of avarice and ambition, which
is sustained by the erroneous opinions of the vulgar, as touching the nature
of right and wrong, would presently faint and languish; and mankind
should enjoy such an immortal peach that [unless it were for habitation, on
supposition that the earth should grow too narrow for her inhabitants] there
would hardly be left any pretence for war.”

This statement is revealing. Hobbes moves from a mechanical
view of cause and effect to udogianism, summarizing the effects of the
Enlightenment on the twentieth century. The mechanical view of
cause and effect led to a collectivist concept of government, as do all
utopian theories. One commentator on Bredvold makes this very
point:

As heirs of the Enlightenment, many social theorists in the West are un-
aware of how deeply they share with Communism a faith in materialistn
and the methods of physical science as applied to man. Many in the West
hold a Rousseauist belief in the natural goodness of man, combined with
the materialist principle of the omnipotence of environment [mechanical
cause/effect]. These principles are fundamental. . . . To the extent that the
philosophy of the Enlightenment has permeated Western Thought, our
statesmen are disqualified or at a disadvantage in waging an ideological war
with Marxian tyranny on moral grounds. The West cannot cndure half
positivist and half Christian.

9. Bredvold, p. 3l.

10. Cited in Bredvold, p. 32

11, Peter Stanlis, “The Dark Ages of the Enlightenment,” University Bookman
(Autumn 1962), p. 14. Brackets added.
