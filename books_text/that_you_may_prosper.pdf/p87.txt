Ethics 6?

was it? Noah promised his covenant-keeping son, Shem, that “Canaan
would be his servant” (Gen. 9:26), Moses then tells Israel how the
promise will come to pass.

When the Lorp your God shall bring you into the land where you are
entering to possess it, and shall clear away many nations before you, . . .
the Ganaanites. . . , Know therefore that the Lorn your God, He is Ged,
the faithful God, who kecps His covenant and His lovingkindness to a
thousandth generation with those who love Him and keep His command-
ments; but repays those wha hate Him to their faces, to destroy them; He
will not delay with him who hates Him, He will repay him to his face. There-
fore, you shall keep the commandment and the statutes and the judgments
which I am commanding you today, to do them. Then it shall come about,
because you listen to these judgments and keep and do them, that the Lorn
your God will keep with you His covenant and His lovingkindness which He
swore to your forefathers. And He will love you and bless you and multiply
you... . And the Lorp will remove from you all sickness; and He will not
put on you any of the harmful diseases of Egypt which you have known, but
He will lay them on all who hate you. And you shall consume all the peoples
whom the Lory your God will deliver to you (Deut. 7:1-16).

The formula for victory is simple: keep the covenant and domin-
ion will be the proper effect, The key to success is not a secret in the
Bible. It is revealed, and it is illustrated time and again,

There is no greater example of the fulfillment of what Moses said
than in the battle of Jericho, Instead of instructing Israel to fight
with weapons, God commanded them to march around the city,
symbolically circumcising it (Josh. 6), since circumcision is the ini-
tial application of the covenant (Gen. 17). God commanded them to
begin with Jericho in the same way they had been commanded to be-
gin with their personal lives. And through this ritual of faithfulness
at Jericho, God surrounded the city as He had surrounded the lives
of the Israclites. He then provided a living example of what happens
when the cnemics are encircled by God through the faithfulness of
His people. On the seventh day, the day of judgment in the Bible,
God caused the wails to fall down. Israel kept God’s Word, and God
kept His Word to Isracl by defeating their enemics.

At a time when the Church has a horrible self-image, because it
has wrongly been told that it will have to be raptured out of its own
unavoidable defeat, the lesson of Jericho needs to be taught. Jesus is
as emphatically clear as Moses and Joshua that the gates of hell will
not prevail against the Church (Matt. 16:18). The enemies will be
defeated, and the Ghurch will have dominion and blessing, if it will
keep God's covenant!
