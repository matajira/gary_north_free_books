Continuity 109

3. Discipleship

Moses addresses a third aspect of contirmation. He tells them to
read the law every seven years at the “remission of debts” (Deut.
31:10). It was for “their children, who have not known” the ways of
the Lord (Deut. 31:13). He had established continuity with the past
and confirmed them with the obligation of conquest in the present.
Now he prepares them for the future with the requirement of perpet-
ual instruction in the Jaw. The children and everyone were to be ed-
ucaied in the Lord. I call this process discipleship.

Civilization cannot be maintained by force. Societies are pre-
served by Gad’s power, self-discipline, and the day-to-day disciplin-
ing of the three institutional covenants: Family, Church, and State.
People nced to be instructed and disciplined in the ways of right-
eousness. Their habits have to be changed. This takes time. The
Biblical way requires constant evangelization, missions, and grass-
roots discipleship. Recognition of the past and the present are not
enough. Discipleship extends historically what has happened in the
past and is happening in the present. Without discipleship, the next
generation will simply lose everything worth having. Without rais-
ing up the next gencration, there can be no real dominion.

Overcoming Hlegitimacy

In Israel, anyone could become a king within ten generations.
The law said that the Moabite and the Ammonite had to wait ten
generations before they could enter the presence of the Lord (Deut.
23:3). Why did it take that long, when it took an Egyptian, the op-
pressor state, only three generations to become a ruler (Deut. 23:8)?
Because Moab and Ammon were bastards, the sons of the in-
cestuous relationship between Lot and his daughters. The rule ap-
plied to Moab and Ammon that applied to bastards in general: ten
generations of circumcised sons or faithful daughters before a
descendent could become a full citizen—a ruler (Deut. 23:2).

David was the descendent of Tamar, who committed adultery
with Judah. He was the tenth generation after Judah and Tamar’s
son, Perez (Ruth 4:18-22), which represcnts approximately six
hundred years, or about half a millennium. This indicates that it
takes time to change civilization and secure the inheritance of the
Lord. David represented ten generations of discipleship and future-
oriented preparation. It took those generations of parents a long
