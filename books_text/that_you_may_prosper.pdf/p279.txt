Revelation 259

3. Ethics (Reo. 4:1-7:17)

The third covenantal principle is consecration through obedience to the
Law, the stipulations of the Deuteronomic covenant. Revelation 4-7
follows these emphases, beginning with a scene around the throne of
God where the “Holy, holy, holy” response stresses consecration (Rev.
4), The next chapter specifically mentions the Ten Commandments,
a “book” written on both sides (Rev. 5:1). So the “ethical” concentra-
tion is obvious. Again, however, this section provides a “covenant
within a covenant,” the entire segment following the five points of
covenantalism.

A. True Transcendence (Rew 4)

Revelation 4 begins the ethical section with the first point of cov-
enantalism. John was commanded to “Come up here” (4:1). Worship
starts with the votum, “the call.” Everyone is called to come into God's
presence and offer true worship. Once John reaches heaven, he en-
counters the transcendent/immanent Lord. God is sitting on His
“throne,” transcendent (4:2). His presence (immanence) is also mani-
fested by His “holiness.” Everyone is singing, “Holy, Holy, Holy, is
the Lord God, the Almighty, Who was and Who is and Who is to
come” (4:8).

In the other covenants we have found that three events are used
at the beginning of the covenant to convey God's transcendence and
immanence: creation, redemption, and revelation, John witnesses
God as the mighty creator. The last verse of the chapter closes, “Wor-
thy art Thou, our Lord and our God, to receive glory and honor and
power; for Thou didst create all things, and because of Thy will they
existed, and were created” (4:11).

The Adamic covenant had begun the Bible with creation, and
now the last book of the Bible emphasizes this theme. Why? God is
getting ready to destroy the world through the judgments to come
in the next few chapters. He is not going to destroy the creation, but
instead He will judge it unto new life. The world will be re-made by
the application of the covenant.

B. Hierarchy (Rev. 5:1-5)

The hierarchical sections of the covenant emphasize God’s au-
thority and the “I-thou” relationship between God and His people.
God’s hierarchy requires submission. Often a “command/fulfillment”
