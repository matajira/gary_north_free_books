True Transcendence 33

Fallen man wants either a transcendent god or an immanent
god, but he does not want a personal God, for such a God is a cove-
nantal Godt. Covenant means law, law means obedience, and disobe-
dience means judgment. As I have said, fallen men above all want to
cscape God’s judgment, Why? Because they are disobedient.

Fallen man wants a substitute. He does not want a substitute
perfect man to serve as the sin-bearer of the world. This would point
to fallen man’s Jack of divinity and his need of salvation. So instead
of accepting Christ as man’s legal substitute, the ethical rebel substi-
tutes a false god who cannot execute legal judgment. The doctrine of
substitution is inescapable: fallen man simply wants a substitute god
of his own creation rather than a substitutionary atonement and
God’s imputation,

False Transcendence

Why do men want a transcendent God, but without a covenant
bond linking such a God to man? Answer: to substitute a distant god
of man’s creation for the all-too-present God of the Bible. The idea of
a god’s transcendence in an anti-Biblical sense means distance, not
God's absolute personal authority. Such a distant god is the god of
deism. Such a god is said to have created the world. He “wound it
up” long ago like the spring in a new clock. He then removed himself
completely from his creation. He allows it to work its way down over
time, He does not interfere with its activities. And, above all, he
does not judge it, He is so transcendent that he just does not care
what happens to it. In short, this god is impersonal. (By the way,
hardly anyone has ever really believed in the purely deistic
cightcenth-century god of the history books, since such a god is just
too far removed to be of assistance when people get in a crisis. He
does not hear prayers or answer them. He is just too useless to be
saleable.)

Guess who becomes the god of this world if this creator god of
deism is far, far away? You guessed it! Man does. Man becomes the
substitute god, The creator god is “on vacation.” He does not judge
kings, kingdoms, or bureaucrats. When it comes to exercising judg-
ment in history, he defaults. Man can spcak; deisim’s god is silent.
Man’s word therefore substitutes for god’s word. Man substitutes
himself for this substitute god. Man becomes the god of the system,
knowing (determining) good and evil.

So, without the transcendent, personal, covenantal God of the

 
