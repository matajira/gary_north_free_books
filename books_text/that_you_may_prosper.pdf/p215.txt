13

THE HISTORICAL STATE COVENANT

Both Family and Church are historically based on the covenant.
But what about the history of the State? In this chapter, we will see
that the original model we began with applies to the civil realm as
well. It also is a covenant. The historical example I have chosen is
perhaps one of the oldest covenants of our society, the Mayflower
Compact. From the word “compact,” which means “covenant,” it is
apparent what kind of document this is. But watch for the actual use
of the word covenant in the body of the document.

The Mayflower Compact
1620

In the name of God, Amen. We whose names are underwritten,
the loyal subjects of our dread saveraigne Lord, King James, by the
grace of God, of Great Britaine, Franc, & Ireland king, defender of
the faith, &c., haveing undertaken, for the glorie of God, and ad-
vancemente of the Christian faith, and honour of our king & coun-
trie, a voyage to plant the first colonie in the Northerne parts of Vir-
ginia, do by these presents solemnly & mutually in the presence of
God, and one of another, covenant & combine our selves togeather into
a civill body politick, for our better ordering & preservation & fur-
therance of the ends aforesaid; and by vertue hearof to enacte, consti-
tute, and frame such just & equall lawes, ordinances, acis, constitu-
tions, & offices, from time to time, as shall be thought most meete &
convenient for the general good of the Colonie, unto which we prom-
ise all due submission and obedience. In witness whereof we have
hereunder subscribed our names at Cap-Codd ye 11. of November, in
the year of the raigne of our soveraigne lord, King James, of Eng-
land, France, & Ireland the cightcenth, and of Scotland the fiftie
fourth.

Ano:Dom. 1620.

 

195
