Psalms 229

the wicked is contrasted with that of the righteous. In Deuteronomy,
we saw the same. The third section of the covenant recapitulated the
stipulations of the covenant, the Ten Commandments. Covenant-
keepers received life. Covenant-breakers end in death, The Psalmist
concludes the first psalm of the third book, “For, behold, those who
are far from Thee will perish; Thou hast destroyed all those who are
unfaithful” (Ps. 73:27).

We also saw in Deuleronommy that one of the ethical cause/effect
emphases was that only the son can fulfill God’s commandments (p.
61). Only the true son, or image-bearer, can implement the law and
conquer. In the third section of Deuteronomy, all three images of
sonship— prophet, priest, and king—are discussed. The third book
of the Psalms speaks of a true image-bearer to come. We find the
great sonship chapter, Psalm 80. Written by Asaph, Levite (the first-
born son of Israel (Ex. 32]), the psalm speaks of Israel as God’s true
son. Asaph writes, “O God of hosts, turn again now, we beseech
Thee; Look down from heaven and see, and take care of this vine
[srael}, even the shoot which Thy right hand has planted, and on
the son whom Thou hast strengthened for Thyself” (Ps. 80:14-15),

It could be argued that there are other psalms on “sonship” in
other sections of the Psalms. But Book III seems to bring out the
theme more clearly because it closes with probably the most power-
ful expression of the Davidic covenant as a covenant of “sonship.” It
is the only place in Scripture that specifically refers to David’s rela-
tionship to God as being a “covenant.” The psalm says,

He [David] will cry to Me, Thou art my Father, My God, and the rock
of my salvation. I also shall make him My first-born, the highest of the kings
of the earth. My lovingkindness T will keep for him forever, and My covenant
shall be confirmed to him. So I will establish his descendants forever, and
his throne as the days of heaven. If his sons forsake My law, and do not
walk in My judgments, If they violate My statutes, and do not keep My
commandments, then I will visit their transgressions with the red, and their
iniquity with stripes (Ps, 89:26-32).

This statement pulls the ethical emphasis together. David was
God’s son. His sons were expected to obey God’s stipulations, the
Ten Commandments, If they didn’t, there would be discipline, So,
the final Psalm of Book III closes on this note. It follows the basic
pattern that leads to the next principle of the Biblical covenant.
Psalm 89 is about the cutting of the covenant. The covenant-cutting
