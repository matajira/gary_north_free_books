7
DOMINION BY COVENANT

How is the covenant applied to society? Unfortunately, most re-
cent scholarship concerning the covenant has failed to ask this ques-
tion. There are only a handful of studies that even consider the social
ramifications, Edmund S. Morgan’s brilliant research on the Puritan
family being one of them.! Studies on the covenant suffer on two
counts: lack of basic Biblical work that pushes out into critical theo-
jogical matters, and failure to consider the historical and social out-
workings of its theology. My book is divided into two halves. Up to
this point, I have concentrated on the Biblical meaning of the term.
Now we should turn our attention to its application. To begin with,
covenant has enabled Christians, when they have been successful, to
dominate the earth. It is éhe model for dominion. Why? The domin-
ion mandates of both testaments are structured according to the cov-
enant: the cultural mandate in Genesis, and the Great Commission
in the Gospel.

The Cultural Mandate

The first dominion mandate is often called the “cultural
mandate” given ta Adam and Eve on the sixth day of creation.

And God blessed them; and God said to them, “Be fruitful and multiply,
and fill the earth, and subdue it; and rule over the fish of the sea and over
the birds of the sky, and over every living thing that moves on the earth.”
Then God said, “Behold, I have given you every plant yielding seed that is
on the surface of alt the earth, and every tree which has fruit yielding sced;
it shall be food for you; and to every beast of the earth and to every bird of
the sky and to every thing that moves on the earth which has life, I have
given every green plant for food”; and it was sa (Gen. 1:28-30).

1, Edmund S. Morgan, The Puritan Family (New York: Harper & Row, [1944]
1966).

124
