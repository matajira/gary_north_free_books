346 THAT YOU MAY PROSPER

peace offering, 86
purification (sin) offering, 85
reparative offering, 85
sin offering (purification), 85
whole-burnt offering, 85
sacrificial system, 84-86
‘Trinitarian formula, 88f.

Salt, 86

Sanctions
Cultural mandate, 126ff.
dual nature, 79
family and, 894.
households and, 891.
judicial, 78

fourth point of the covenant, 77ff.

promises, 78

summary, 121

witnesses, 91-93
Sanders, James A., 204n.2
Satan, 25n.5
Saul, 41-42
Schaeffer, Francis, 2
Schilder, Klaas, Ul1n.12, 125n.4
Schmemann, Alexander, 106n.9
Science and magic, 74
Scofield, C. L, 80n.3
Second-born, 100, 101-102
Self-maledictory vath, 84, 186
Seneca, 182
Septuagint, 24
Sermon on Mount, 135
Seth, 45
Seventy weeks, 234-35
Shakespeare, 71
Shiloh, 110
Sic et Non, xvi
Sinnema, Don, 66n.6, 106n.8
Situational ethics, 66
Skepticism, xvi
Skilton, John H., 235n.1
Skynner, A. G. R., 142
Smith, Joseph, 37, 152n.5
Snow, Lorenzo, 152
Social contract theory, 4n.3
Sodom, 117
Sang of Witness, 122
Sovereignty, 4+

S.PC.K.,5
Spinuza, 69
“Spirit Babies,” 153
Spiser, E. A., 150n.4
Stanlis, Peter, 70
State
authority, history of, 10
continuity, history of, 12
covenant history and, 9-12
ethics, history of, 11
interposition, 180
lesser magistrates, 180
oath, history of, 1
transcendence and, 10
State covenant
authority, 179
continuity (biblical), 190-93
(historical), 200-201
ethics (biblical), 180
(historical), 197-99
false continuity, 192
hierarchy (biblical), 179-180
(historical), 197
historical example, 195ff.
sanctions (biblical), 186-190
(historical), 199-200
totem pole and, 198
transcendence (biblical), 178-79
(historical), 196-97
Stranger in the land, 90, 294
Stones of the covenant, 87
Stuart, Moses, 256.6
Stoicism, 181ff.
Substitutionary atonement, 293
Succession, 104-105
Supreme Court, 10
Sutton, Ray R., xii, xiii, 8, 156
Suzerains, 83
Suzerain ‘Lreaties, 15
confirmation and, 105
communion meal and, 105
hierarchy, 43
Symbol
circumcision, 288
New Covenant, 86-87
Old Covenant, 86-87
Passover, 288
