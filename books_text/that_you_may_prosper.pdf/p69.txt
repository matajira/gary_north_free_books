Hierarchy 49

2. It is here that the servant of God encounters certain other
enemies whom he cannot defeat without submission lo the hierar-
chy. Actually, they are usually “lesser” enemies. Israel’s wilderness
experience is a perfect example. Forcign powers were brought down
on them. Until they repented (Deut. 1:41-46), the message was,
“Paganism will dominate you when you rebel against the Lord's sys-
tem of judgment.” When Israel failed with the “big” enemy, Canaan,
God withdrew them into a situation where they could practice before
“smaller” cnemics. The Lord riveted His point, as He often does in
the Bible, through a serics of instructive encounters. Some of the
enemies God commanded Israel to pass by without conflict, as in the
case of the “sons of Esau” (Deut. 2:8). Others God told them to fight
and destroy, as in the case of the Amorites (Deut. 3:8). Why were
they to fight some and not others? This leads us to point three.

3. Rebellion destroys unity among the brethren. God always
directed Israel away from their closest “brothers,” like the “sons of
Esau” and the “Moabites” (Deut. 2:1-25), When Israel learned not to
fight with the ones closest lo them, they became more unified. And
when they quit fighting among themselves, they were ready to fight
the real enemies, the Amorites and even Canaan (Deut. 2:24).

4. Marching comes before fighting. Israel wandered for thirty-
eight years (Deut. 2:14). They had to learn to march in military
order before they would be able to fight in harmony.* God drilled
them for over three decades. When they showed enough account-
ability to walk in formation, they were ready to begin to fight. The
people of God have to learn how to submit when they walk, before
they will be able to run.

5. Submission comes before privilege. After Israel absorbed the
lessons of accountability, they were given a covenant grant. This was
usually a grant given 10 a vassal who had demonstrated exceptional
faithfulness, Israel's grant was Canaan (Deut. 3:12),

6. The ways of rebellion are removed slowly. God waited until
the older, rebellious generation died (Deut. 1:35). This gave the
younger generation an extended course in wilderness training, ‘They
had to submit to God and each other to survive. Individualists do
not survive in the wilderness. Like Jesus in the wilderness, Israel
was prepared for any challenge. If they could live in the wilderness,
they could conquer in civilization.

6. James B. Jordan, The Sociology of the Church: Essays in Reconstruction (Tyler,
Texas: Geneva Ministries, 1986), pp. 215-6.
