8
THE BIBLICAL FAMILY COVENANT

A couple of years ago, I participated in a seminar on the family.
Its purpose: to draft a statement on the family from a Christian per-
spective that would he sent to politicians, showing them that a “tradi-
tional” view of the family exists in the consensus of our society. I re-
member the members of the group well: there were housewives;
there was a theological professor with admitted family problems who
was participating for his own sake; there was a family therapist;
there was the head of a leading family organization. T was the
minister.

For two days, we sat in a small room, trying to hammer out a
statement on the family, seeming to make little progress. What was
the problem? No one could answer the question: “What is the family?”

There were basically three options presented. One, the family is
a blood-bond, involving all those related by blood. Of course, this
raises the issue of “extended” vs. “nuclear” family. If the family is
related by “blood-bond,” then the family consists of all those near
and distant relatives who have any of the “clan’s” blood running
through their veins. The group was inclined to reject this view, but
on the other hand, it wanted to adopt it.

Second, the resident “family therapist” vehemently objected to
the “blood-bond’ definition, submitting the idea that marriage was a
social contract, along the lines laid down by Rousseau. Some were im-
mediately pulled this direction, thinking it was the solution to our di-
lemma. But then, one of the “down-to-earth” housewives said, “Wait
a minute. If marriage is simply arranged among various parties by
social agreement, what would stop someone from calling a social or-
ganization of homosexuals a ‘family’? She was correct, because
homosexual couples do call themselves a “family.”

Finally, after hours of listening to this fruitless debate, another
housewife — the housewives seemed to have more sense than anyone

137
