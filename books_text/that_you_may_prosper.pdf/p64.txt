44 THAT YOU MAY PROSPER

In the beginning, God created the world by His Word. What He
declared was visualized. If God had declared light, and the light had
not come into existence, then we could safely say that God could not
declare light. The idea is that what God says comes to pass, and if it
doesn’t, then God is revealed as a liar.

Enter Satan. Six days after creation, Satan tried to make God a
liar. His strategy was ingenious. He struck at God’s hierarchy by
taking the delegated authority given to Adam, and by actually con-
vincing the first man to give it ta Aim. He offered Adam divine au-
thority in place of a delegated authority. He told man he would be-
come like God, “knowing [determining] good and evil” (Gen. 3:5), if
he ate the tree of the knowledge of good and evil. Yet, he was trying
to convince Adam of something that was already true. Man was cre-
ated in the “image” of God (Gen. 1:26). In this sense, he already was
like God, a theomorphe. But the way to manifest God was not by
“knowing” (determining) good and evil; rather, it was by ruling as a
delegated authority. In the greatest deception of history, Satan had
succeeded in robbing man of what he possessed by offering him what
he could never have. By so doing, he effectively made the first man
obey his authority, which in turn made him the vice regent? of the
earth, and placed diabolical leaders in office. Adam’s disobedience
gave away God's visible sovereignty, as well as his own delegated au-
thority, and made Satan, not man, appear to be God.

The implication was devastating. Remember the progression of
the manifestation of sovereignty from verbal to visual and from dec-
laration to temporal demonstration (history)? God speaks, and man
obeys, God’s authority is over man, and man’s authority is over the
creation. Man mediates between God and the creation. But Satan
comes, disguised as a creature which was below man, and he also
speaks. Whose voice is the true voice of authority? Man is tempted
to respond to the creature and disobey the Creator. Man is still an
intermediary. He mediates betwcen the Creator and the creation.
This is his mescapable role. It is never a question of mediation or no
mediation; it is always a question of who is sovereign above man,
and who is subordinate beneath him. Satan sought ethical authority
(law-giver status) over man in order to manipulate God and God's
plan for the ages. God had declared Adam as His image, yet man
symbolically placed himself bencath the creation (a serpent) and

2. Sometimes written vice gerent.
