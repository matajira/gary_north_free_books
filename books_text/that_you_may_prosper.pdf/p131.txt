Continuity 1M

Dissotubility of the Covenant

The covenant can be terminated, or dissolved. The continuity of
all the covenants —God to God, God to man, and man to man—is in
fact dissoluble.©

The “primary” covenant—God to God—could be broken upon
covenantal death, Even though Jesus perfectly obeyed God, the
Gross is a picture of God the Father forsaking God the Son." Jesus
said, “My God, My God, Why hast thou forsaken Me” (Mati.
27:46). This is truly a great mystery, but Jesus was covenantally cut
off from the “land of the living.”” A kind of “covenantal death” oc-
curred prior to physical death.

The secondary level of covenant— God to man—can be broken
upon covenantal separation, The writer to the Hebrews warns the
believers not to forsake their covenant. He urges them to “press on to
maturity” (Heb. 6:1), the implication being that they were thinking
of departing from their covenant with God. Again, we take notice of
the following sobering words:

For in the case of those who have once been enlightened and have tasted
of the heavenly gift and have been made partakers of the Holy Spirit, and
have tasted the good word of God and the powers of the age to come, and
then have fallen away, if is impossible to renew them again to repentance, since
they again crucify to themselves the Son of God, and put Him to open
shame (Heb. 6:4-6).

10. There are three levels of covenants in the Bible. ‘Uhe primary level of covenants
is the God-to-God-covenant, This is the covenant among the Godhead, illustrated
in the Abrahamic covenant where God took a covenantal “oath” to Himself and used
the other members of the Godheud to “witness” (Gen. 15:1-21). The secondary level is
from God to man. When one is converted, he enters this covenant. Finally, there is
the tertiary level of covenants from man to man, falling into two categories: Self-
maledictory covenants which are binding until the “death” of those involved, and
contracts which should not be accompanied by “self-maledictory” oaths, oaths that
call down death to the one who breaks the covenant,

11. Klaas Schilder, Christ Crucified (Grand Rapids: Baker, [1940] 1979), pp.
371-426. As indicated by one of Schilder’s sermon titles, “Christ Thrust Away But
Not Separated,” Ghrist’s death did not do “violence” to the Godhead. Nevertheless,
the covenant was fractured such that God the Son incurred the full tury of the wrath
of God the Father.

12. Here, by the way, is a classic example of how physical death is based on “cove-
nantal” separation. ‘The physical death of one’s spouse causes the covenantal death
of the marriage, but only because all physical death is the result of the covenantal
violation of Adam and Eve (Rom. 5:11-12),
