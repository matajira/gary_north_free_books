The Biblical State Covenant 187

truth, the self-maledictory oath takes effect. He calls the wrath of
God down upon his head. In some cases of perjury, the death penalty
could be rendered. At any rate, the oath becomes the foundation of all
Judicial processes. The presumption is that God renders judgment on
the witnesses. They are not just testifying before men but the judge
of all men. It is not men who even render the judgment but only
carry out God's judgment through these witnesses sworn to His alle-
giance. Thus, if there is no cath, a conviction becomes merely the
“common consent” of a group of individuals who want to do another
man in. Atheism leads to chaos in the courts and the worst kinds of
injusti

Christianity has always seen the relationship between oath and
judgment. Justice is lost without the Biblical oath. In 1675, John
Taytor, yeoman from Guildford in Surrey, uttered these blasphe-
mous words:

 

Christ is a whore-master, and religion [Christianity] is a cheat, and pro-
fession (of Christianity] is a cloak, and they are both cheats, and all che
earth is mine, and I am a king's son, my father sent :ne hither, and made
mc a fisherman to take vipers, and I neither fear God, devil, not man, and
Lam a younger brother to Christ, an angel of God and no man fears God
but an hypocrite, Christ is a bastard, god damn and confound all your
gods, Ghrisi is the whore’s master.

He was charged with “blasphemy” and taken to the House of
Lords for trial. After considerable deliberation, he was then handed
over to the common-law courts. Never before had they been given
jurisdiction over blasphemy. In 1676, however, Taylor was taken be-
fore the greatest jurist of the day, Chief Justice Matthew Hale. Hale
found him guilty and said,

And... such kind of wicked blasphemous words were not only an
offence to God and religion, but a crime against the laws, State and Gov-
ernment, and therefore punishable in this Court. For ta say, religion is a
cheat, is to dissolve all those obligations whereby the civil societies are pre-
served, and that Christianity is parcel of the laws of England; and therefore
to reproach the Christian religion is to speak in subversion of the law.®

9, fournals of the House of Lords, XII, 688, May 11, 1675; 691, May 14; and 700-1,
May 20. Gited in Lonard W. Levy, Treason Against God (New York: Schocken, 1981),
pp. 312-314.

10. bid. Rex v. ‘Taylor, 3 Keble 607, 621.
