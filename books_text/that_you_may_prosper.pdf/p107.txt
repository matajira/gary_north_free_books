Sanctions 87

ham’s personal reception was the rite of cércumeision. Of course, cir-
cumcision was a “bloody” ordeal that also denotes cursing sanction in
symbolic form. In the New Covenant, the symbols of the sanctions
become baptism and communion. They too imply blessing and curs-
ing. In Appendixes 8 and 9, I discuss the sanctions of the Old and New
Covenants in detail. For now, however, it is important to understand
that all of these symbols manifest both sanctions. For exainple, the
Lord’s Supper is a visual manifestation of the finished work of
Christ, which mediates life and death. lt communicates life and
death. If partaken wrongly, it kills the recipient (I Gor. 11:27-34).

In each case, the covenant is actually received by use of some
symbol. In Deuteronomy, perhaps the most obvious symbol in-
volved in renewing the covenant is the use of the “large stones,”
coated with lime and etched with the actual covenant (Deut. 27:2-4).
The covenant was actually written on them. So they did more than
symbolize, they sealed the people and the land to Yahweh. But the
logical question is “Are these symbols real?” And, “What is the rela-
tionship between the symbols and thcir meaning?” The questions
being raised are one of the connection between “sign and thing signi-
fied.” There ave only three views: nominalism, realism, and cove-
nantalism.

Nominalism says there is no connection between the symbol and
what it means. There is no relationship between cause and effect.
The judicial ceremonies that Isracl engaged in have nothing to do
with reality. The signs and seals of the covenant really do not mean
anything. Using an analogy that David Chilton applies, “A kiss is
just a kiss.” When a nominalist sces a woman kiss a man, he says,
“That doesn’t mean anything. What they're doing is only symbolic.”
The kiss does not mean love, affection, or necessarily anything. The
absurdity of this is that if the nominalist is right, a man could just as
easily “slap” the woman with a glove— that is, if “symbolic gestures”
do not really mean anything.

The Bible disagrees. While man’s symbols may at times have no
meaning, Ged’s symbols have power because His word is behind
them. When He says He will bless those who are faithful to the cove-
nant sign, His word is true. When He says cursing will fall on the
one who violates the symbo! of the covenant, that too will happen. So,
God's special covenant signs really seal a person to God: incorporation.
The word comes from the Latin, meaning “one body.” Incorporation is
automatic upon the reception of the covenantal sign, but not auto-

  
