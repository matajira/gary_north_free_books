The Historical Family Covenant 149

A “well-intentioned” Christian couple sets out to have the “per-
fect” marriage. In the process, they elevate marriage to an idolatrous
position, make it uanscendent, and expect too much out of the mar-
riage. Then they have those “‘normal” problems adjusting. But
because they have an almost “deified” view of marriage, they become
disillusioned. Sometimes they end up getting a divorce.

How ironic! Modern society, in desperation, creates all kinds of
ways— T'V shows, books, “marriage encounters,” ctc. — to revive the
family institution. Even though these techniques might not be bad in
and of themselves, in the process, the family is placed in the position
of Ged. The family itself is worshipped. Maybe the spouse or even
the children end up at the center of some well-intentioned person’s
worldview. In either case, Biblical transcendence is lost. The family
itself becomes “god.” The contract above rejects such a notion. It
projects a high view of marriage and the family, but not so high that
it shatters the Creator/creature distinction.

Hierarchy
That what estate of hers the said Alice shall be found and committed to
the said Mr. Clarke, if it should so please the Lord to take the said Alice out
of this life before the said Thomas Clarke, he shall then have the power to
dispose of all the said estate sometimes hers so as it seems good to him... .

A Biblical covenant has representatives. The 1665 marriage does
something that might tend to go unnoticed, but is quite revealing
about the representative character of the relationship between these
two parties. The covenant is in the name of the man. Why? He rep-
resents both parties. According to the original language of the New
Testament (Greek), the very word for “family” is derived from the
word for “father.” Paul says, “For this reason I bow my knees to the
Father { Pater], from whom every family [Patria] in heaven and on earth
derives its name” (Eph. 3:14-15). Both Greek words come from the
same root. This representation of Ged by the father originated with
the first marriage between Adam and Eve. Adam “named” Eve and
declared her to be his wife. Moses says for this reason a “man shall
leave his father and mother and cleave to his wife” (Gen. 2:24). To
“name” something is to set up authority over it. Since the man was
given this responsibility, he represented God in a special way. So the
emphasis of the Bible is that the man is God’s representative in the
household.

Tn the event that the man dies, leaves, or divarces his wife, she
