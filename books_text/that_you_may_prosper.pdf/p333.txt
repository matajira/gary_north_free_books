Covenant By the Spirit 313

or bodily present, and something really happens in history. The cov-
enant has meaning and influence in history. Tts sacraments change
history because they are effectual. They are not mere symbols.
Christ attains special presence through the representative meal of
His people, The Biblical view thercfore abandons both realism and
nominalism. It is instead a covenantal view.

The Covenantal View of Christ’s Kingdom

The Biblical view of the kingdom is equally covenantal. It is also
a representative view. It is therefore also Aterarchical. In His kingdom,
Christ is spiritually present, and manifestations of this victorious
kingdom appear in history. The covenant has meaning and influence
in history, sacramentally and eschatologically. The kingdom of
Christ on earth is more than a symbol, yet He is not physically pres-
ent. He attains visible sovereignty through the historic victory of His
representatives, His people. He does not attain visible, historic defeat
through His representatives. The Biblical view therefore abandons
both realism and nominalism. It is instead a covenantal view.

Fundamentally, both premillennialism and amillennialism deny
the representative character of God’s people as Christ’s kingdom rep-
resentatives. Every Christian must affirm that all true authority is in
Christ. But if the Church’s visible authority is forever limited in his-
tary (pre-second coming), then the amount of submission required
of Christ’s hierarchy is also limited. We know the general principle
that with greater authority comes greater responsibility, yet modern pessi-
millennialism denies the increasing responsibility of the Church over
the affairs of history. Covenant-breakers grow more powerful as time
goes by; hence, the Church has progressively less responsibility be-
fore God to exercise responsible authority, As the disciples began the
Great Commission Covenant with their own submission at the feet
of Christ, so must the present-day Church. To the degree that this
Church believes in the full authority of Christ in heaven and on earth,
it will worship and submit to Him on earth!

As a curiosity, let me note a few oddities. First, those who hold
the doctrine of the real presence of Christ in the Lord’s Supper his-
torically have been amillennialists (nominalists) eschatologically.
They have explicitly denied the increasing visible manifestation of
Christ’s institutional rulership in history. Second, in this century,
those fundamentalists who have held to a “memorial only” view of
Christ’s sacraments (nominalism) have generally been realists with
