Continuity 113

referring to blood (Gen. 36:8). He had a “taste for game.” He liked
the taste of blood. Esau believed in power religion. Force was the
means of acquiring and sustaining the inheritance. And his warring
nature turned against him.

Esau was not a man of the covenant. His power-oriented way is
contrasted to Jacob’s “peace-loving” ways (Gen, 25:27). This means
that Jacob dealt with people according to the covenant. Esau was the
opposite. He dealt according to power, blood, and the desires of his
own stomach. He was nol past-oriented or future-oriented, One day
he went into the field to hunt. He came back and found Jacob cook-
ing a “red stew” (Gen. 25:30). Again the red is probably a reference
to “blood.” Esau was hungry and wanted something to eat immedi-
ately. The fact that it was a “blood” stew made him desperate. Of
course the stew was not a blood dish. It was a lentil stew (Gen.
25:34). I believe Jacob knew his brother was a bloody man and
would be reminded of blood when he saw it.

Jacob took advantage of the situation. He was a man of the cove-
nant. He knew and respected the covenantal transfer of inheritance.
He had to have the laying on of hands. But since he was not a power-
oriented person, he wanted to acquire the legal right of the first-born
by Esau’s choice. He told Esau that he could have some food if he
would give up his birthright. Esau agreed, The way Esau so quickly
agreed, as a matter of fact, proves that Esau did not regard his birth-
right very highly. He probably thought this covenantal oath to his
brother was worthless. He believed in power, and he perceived that
he could hang on to his inheritance by power and blood.

The conflict between Esau and Jacob does not stop here. Even
after Jacob lawfully obtained the birthright, Esau still wanted to
have the blessing of his father. He wanted to be a man of blood and
also have the benediction of the covenant. The two do not mix.
Rebckah and Jacob knew it. They also knew that Jacob needed
more than Esau’s oath, Jacob had to have his father’s confirmation.
So a plan was devised to obtain the blessing (Gen. 27:6-17). Jacob
dressed up like Esau and went into his father. He served a meal.
(Remember the covenant renewal process involves a meal.) But
Isaac was blind. He smelled and touched Jacob. He was not able to
tell the difference. Jacob received the blessing.

Esau was cursed. His father told him he would always be a man
of war (Gen. 27:40). As Jacob had been communicated, Esau was
excommunicated from his father’s table of blessings. From that point
