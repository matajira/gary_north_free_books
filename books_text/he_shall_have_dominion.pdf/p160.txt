114 HE SHALL HAVE DOMINION

ing the divine covenants, as well as a progressive development
in them. Thus, with the coming of the New Covenant in the
ministry of Christ, “the fullness of time” has been reached (Gal.
4:4).° And these concern rcdemption — a redemption, as we
shall see, that shall overwhelm the world.*!

The major competitor to covenantal theology among evan-
gelicals today is dispensationalism.” Dispensationalism allows
the historic, biblical covenants to play a large role in its theol-
ogy.”* Yet dispensational theology and covenantal theology
are, in the final analysis, “irreconcilable.”** Indeed, “reformed
covenant doctrine cannot be harmonized with premillenar-
ianism”*’ because the dispensationalist's “dispensations are not
stages in the revelation of the covenant of grace, but are dis-
tinguishingly different administrations of God in directing the
affairs of the world.” Thus, the major difference between
covenantal theology and dispensational theology is that coven-
antal theology traces a relentless forward moving, unified, and
developmental progress of redemption, generally understood in

30. “That the Covenant is a basic assumption throughout the New Testament is
evident from such passages as: Luke 1:72; 22:20; Matt. 26:28; Mark 14:24; Luke
24:25-27; John 6:45; Acts 2:39; 3:25; Rom. 11:27; 1 Cor. 11: 2 Cor. 3:6ff; Gal.
$:14-17; Eph. 2:12; Heb. 7:22; 8:6-13; 9:1, 15-20; 10:16; 12:24; 13:20. The basic
idea, nature, and purpose of the covenants made with Abraham, Israel, and David
are carried over into the New Covenant and require no explicit repetition in the New
Testament.” Roderick Campbell, Israel and the New Covenant (Tyler, TX: Geneva
Divinity School Press, [1954] 1981), p. 53n.

41. See Chapter 10 for the postmillennial significance of these covenants.

   

32. See Chapter 3 for a definition of dispensationalism.

33. The role of covenants in dispensationalism produces a strange anomaly in
the system: it results in a pandemonium of history-structuring devices. History is
divided by dispensations, while at the same time it is structured by covenants ~
covenants that do not always coincide with the dispensations! For instance, the Abrahamic
Covenant is considered unconditional and everlasting, but the dispensation of
promise (the Abrahamic era) is closed by the giving of the Law. Sce: Robertson, Christ
of the Covenants, pp. 202ff, 211.

34, Charles Lee Feinberg, Millennialism: The Two Major Views (3rd ed.; Chicago:
Moody Press, 1980), p. 87.

35. Ibid., p. 69.

36. Charles G. Ryrie, Dispensationalism Today (Chicago: Moody Press, 1965), p. 16.
