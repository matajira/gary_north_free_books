Introduction to Postmillennialism 67

tinues his earlier error, when he comments (1986): “Though
Augustinian amillennialism is generally followed in this modern
time... another form of amillennialism arose. B. B. Warfield
. .. taught that the Millennium is the present state of the saints
in heaven.”®

Of Allis, Pentecost writes: “Amillennialism today is divided
into two camps. (1) The first, of which Allis and Berkhof are
adherents. . . .”* Walvoord follows suit: “However, in view of
the evidence that many amillenarians consider it, as Allis does.
.. 7 Culver (1977), C. Feinberg (1980), Ryrie (1986), J. Fein-
berg (1988), and Lightner (1990) concur.

It is clear from Warfield himself,’ as well as other eschatol-
ogical writers’, that he was a postmillennialist. While express-
ly discussing the “premillennial” and “postmillennial” positions,
Warfield writes of his own view: “[T]he Scriptures do promise

ville: Thomas Nelson, 1990), p. 77.

5, Charles Caldwell Ryrie, Basic Theology (Wheaton, IL: Victor, 1986), p. 449.

6. Pentecost, Things to Come, p. 387.

7. John FE Walvoord, The Nations, Israel, and the Church in Prophecy, 3 vols. in 1
(Grand Rapids: Zondervan, 1988), 2:56. See also: Walvoord, The Revelation of Jesus
Christ, p. 286. Walvoord, “Revelation,” The Bible Knowledge Commentary: New Testament
Edition, Walvoord and Roy B. Zuck, eds. (Wheaton, IL: Victor, 1983), p. 978.

8. Culver, Daniel and the Latter Days, p. 24. Feinberg, Millennialism, p. 49. Ryrie,
Basic Theology, p. 449. John S. Feinberg, “Systems of Dicontinuity,” Continuity and
Discontinuity: Perspectives on the Relationship Between the Old and New Testaments, Fein-
berg, ed. (Westchester, IL: Crossway, 1988), p. 67. Lightner, Last Days Handbook, 91.

9. See: Warfield, “Jesus Christ the Propitiation for the Sins of the Whole World”
(1921), in Selected Shorter Writings — I, John E. Meeter, ed. (Nutley, NJ: Presbyterian
& Reformed, 1970), pp. 167-177. “Antichrist” (1921), sid., pp. 356-364. “The Impor-
tunate Widow and the Alleged Failure of Faith” (1913), in SSW (Nudey, NJ:
Presbyterian & Reformed, 1973), pp. 698-711. Warfield, Biblical and Theological
Studies, Samuel E. Craig, ed. (Philadelphia: Presbyterian & Reformed, 1952): “Are
There Few That Be Saved?” (1915), pp. 334-350; “The Prophecies of St. Paul”
(1886), pp. 463-502; “God’s Immeasurable Love” (n.d.), pp. 505-522. Warfield,
Biblical Doctrines (New York: Oxford University Press, 1929), pp. 663ff. Warfield,
“The Millennium and the Apocalypse,” Princeton Theological Review (Oct. 1904).

10. For example see: historic premillennialist Ladd, Crucial Questions, pp. 46-47.
Amillennialist Anthony A. Hoekema, The Bible and the Future (Grand Rapids: Eerd-
mans, 1979), pp. 1761.
