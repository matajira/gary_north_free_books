518 HE SHALL HAVE DOMINION

have to provide more compelling examples before this portion
of his argument can bear any weight. It collapses.

Hanko is concerned with the theonomic postmillennialist’s
position; he claims that it leads to “the bondage of law upon
law.” But is it necessarily wrong to be in “bondage of law upon
law”? Is it wrong, for example, to argue “you shall not kill”?
And “you shall not steal”? And “you shall not commit adul-
tery”? And “you shall not covet”? Is this an illegitimate “bond-
age” engendered by adding “law upon law”? Surely not!

Is it true that the civil laws are included “in the liberty
wherewith Christ has made us free (Galatians 5:1)”? Here an-
other attempt at exegesis fails Hanko. Contextual exegcsis
shows that the freedom Paul speaks of here — the freedom that
we have in Christ under the New Covenant — is a freedom from
the fulfilled ceremonial/symbolic laws, not the moral or civil laws.
We are still in “bondage” {i.e., under obligation) to civil author-
ity in the New Covenant era (Rom. 13:1-4; 1 Pet. 2:13-14).
Paul’s point in Galatians has to do with requiring the keeping
of ceremonial laws in order to gain salvation, Paul is condemning
the use of the Law (particularly the ceremonial law) by the
Judaizers as a means of redemptive merit before God (such as
in Acts 15).

This Judaizing approach to Law-keeping is clearly under
Paul’s scrutiny in Galatians. Paul’s concern is with a corrupted
“gospel” of salvation by works — a gospel that is under a curse
(Gal. 1:6-9). He is arguing “a man is not justified by the works
of the law but by faith in Jesus Christ, even we have believed in
Christ Jesus, that we might be justified by faith in Christ and
not by the works of the law; for by the works of the law no flesh
shall be justified” (Gal. 2:16). He warns the Galatians: “I do not
set aside the grace of God; for if righteousness comes through
the law, then Christ died in vain” (Gal. 2:21). He insists that
“no one is justified by the law in the sight of God” (Gal. 3:11),

The problem among those in Galatia was that “they have
become estranged from Christ, who attempt to be justified by
