Subject Index

prophecy and, 164-172,
349

rebellion of, 111, 314-315,
491

rejection of Christ, 215,
230-231, 314-315, 317,
341

Twelve Tribes, 212, 348,
352, 399, 405, 482

Israel of God (See:
Israel — Church as)

James the Just, 390

Jehovah (See also:

God — covenantal name,
12)

Jehovah’s Witnesses (See:
Cults — Jehovah’s
Witnesses)

Jerusalem
destruction, 161, 274, 320-

321, 338ff, 362, 471, 486
evil image, 379ff, 411
famous city, 379
gospel and, 200
holy city, 345, 407
Luke’s Gospel and, 202n
millennium and, 61, 63,

146
New, 362fF
pagan names, 382, 406,

41k
physical changes in

millennium, 146
prophecy and, 150ff, 164-

172
spiritual, 150, 362, 403
typology and, 201, 381

575
wife of God, 381-382, 402

Jesus Christ (See: Christ)
Jewish War with Rome, 347ff,

388-389, 399

Jews (See also: Israel)

conversion of, 53, 90, 128,
206, 230, 267n, 405,
487, 491

crucifixion of Christ, 347n,
382, 487

exaltation of, 193, 228-229

Gentiles and, 128, 167ff,
193, 204, 224, 499

judgment of, 267, 385

literalism and (See:
Hermeneutics — Jewish)

millennium and, 55, 75,
230

Nazis and, 428

race/generation, 339ff

rebellion against God, 84,
414

rejection of Christ, 230,
314-315, 341, 347, 501

Jigsaw puzzle, 15
Joachim of Florus, 87-88
John Baptist

as Elijah, 367i
forerunner of Christ, 156,
161, 214

Josephus, 343ff, 399, 403
Jubilee, 312ff 386, 517
Judaism, 115

Judgment

final, 2, 18, 52, 126, 244,
302

general, 73, 287, 291-292,
417-418
