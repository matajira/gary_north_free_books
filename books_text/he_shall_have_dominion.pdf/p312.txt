266 HE SHALL HAVE DOMINION

In 1 John 2:2, the force of the teaching does not depend on
syntactical features such as purpose clauses, but upon strong
redemptive terminology: “Ile Himself is the propitiation for our
sins; and not for ours only, but also for those of the whole
world.” The word “propitiation” (Ailasmes) is one of the more
potent redemptive terms available in Scripture.

In 2 Corinthians 5:19, another significant redemptive term
is employed: “reconciliation.” Reconciliation has to do with the
bringing back of a favorable relationship between God and
man. It speaks of actual relief from the consequence of sin (vv.
19, 21). Notice the emphasis on God’s action: Verses 18 and 19
say, “All these things were from God, who reconciled us . . .
namely, that God was in Christ recenciling the world to Him-
self.” Later in verse 21 it is said that “He made Him that knew
no sin to be sin on our behalf.”

This idea is also clearly represented in Romans 11:15: “For
if their being cast away is the reconciling of the world, what will
their acceptance be but life from the dead?” The argument
Paul is presenting in Romans 9-11] has to do with the racial
Jews’ place in the plan of God in light of God's calling of the
Gentiles. At this juncture Paul points to their casting away by
the judicial sentence of God. Though this judgment is lamenta-
ble to the Jews at present, says Paul, it is necessary in order to
effect “the reconciliation of the world.” And “The reconciliation of
the world, implies, of course, the conversion of multitudes of
men, and the prevalence of true religion.”**

Thus, in cach of the passages passing under our scrutiny, we

86. Charles Hodge, Commentary on the Epistle to the Romans (Grand Rapids:
Eerdmans, (1886), p. 365. Some amillennialists mistakenly complain that postmil-
lennialism’s view of Romans 11 claims “that the Jews will be saved as Jews.” They feel
the postmillennial view “neglects[s] Lhe New Testament truth that Jews who are saved
lose their national identity.” Herman Hanko, “An Exegetical Refutation of Postmil-
lennialism” (unpublished conference paper: South Holland, IL: South Holland
Protestant Reformed Church, 1978), pp. 12, 17. This is simply not true. The argu-
ment confuses the Jew as a racial entity with the Jew as religiously committed. Racial
Jews will be saved, when they forsake Judaism and become Christians.
