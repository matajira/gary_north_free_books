The Purpose of This Treatise 31

through the means of the Word of God: “Sanctify them
through thy truth: thy word is truth.”

All Christians, therefore, should “desire the sincere milk of
the word, that [they] may grow thereby” (1 Pet. 2:2). As we
grow in the knowledge of the Word of Truth, we should strive
to reach a level of understanding that would equip us to be
competent teachers of the Word (Heb. 5:12-14; contra John
3:10). None of us “knows it all.” Thus, the study of issues of
contemporary concern is always practically beneficial to the
Christian. And the labor of diligent and systematic study of
Scriptural issues is essential to the Christian’s pleasing God.

My concern in this work is with an evangelical audience.
Consequently, I will give only occasional and passing reference
to the various eschatological formulations by liberal theologians,
such as might be discovered in process theology, liberation
theology, and the like. This approach does not imply that a
study of the errors involved in rationalistic eschatological for-
mulations is unneeded.'® For a full-orbed Christian witness,
we should strive to understand and be able to respond to those
who would subvert doctrine within the church. Nevertheless,
due to space limitations, this will not be engaged in the present
work.

Hasty Postmortems

Many evangelical treatments of eschatology obscure the facts
of contemporary options, sometimes through ignorance, some-
times through overstatement. Whatever the reason, a great

9. John 17:17; cf. John 8:32; 15:3; Eph. 5:26; 2 Thess. 2:13; Jms. 1:21.

10. For helpful introductions to liberal eschatological views, see: Millard J.
Erickson, Contemporary Options in Eschatology: A Study of the Millennium (Grand Rapids:
Baker, 1977), chaps. 1-2, and Erickson, Christian Theology, 3 vols. (Grand Rapids:
Baker, 1985), 3:1155ff. Anthony Hockema, The Bible and the Future (Grand Rapids:
Eerdmans, 1979), pp. 288-315 (Appendix: “Recent Trends in Eschatology”). John N,
Oswalt, “Recent Studies in Old Testament Eschatology and Apocalyptic,” Journal of
the Evangelical Theological Society 24:4 (Dec. 1981) 289-302.
