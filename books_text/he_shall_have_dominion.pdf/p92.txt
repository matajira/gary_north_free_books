46 HE SHALL HAVE DOMINION

the stuff of which cults are made. It takes a certain arrogance to
claim to have discovered a vital teaching that the entire church
has overlooked for 1900 years.””? This, you understand, comes
from a man who defends an eschatological position, pre-tribula-
tional dispensationalism, whose origin which can be traced back
no earlier than the 1820s, and probably no earlier than 1830.

Vindication

One of the frustrating barriers that postmillennialists face in
the modern debate is the tendency by some to distort postmil-
lennialism. Many of the average Christians-in-the-pew have
such a flawed view of postmillennialism that it is sometimes
difficult to gain a hearing with them. Postmillennialism is
deemed to be utterly “this-worldly” in an unbiblical sense. It is
often considered an aspect of the “social gospel” of liberalism.
Or it is chought to throw out valid hermeneutical procedures to
bend and twist Scripture into a liberal system. Still others
wrongly assume postmillennialism involves a union of Church
and State. Again, popularizers of other viewpoints are generally
the source of the problem.”

Even worse, there are some fundamental misunderstandings
of postmillennialism, even by noteworthy theologians. And
some of these published errors have been in print for decades
without any attempt at correction. This deserves exposure
because it is the tendency of many simply to pick up on confi-
dent statements found in published works and promote them as
truth, Such errors will be dealt with in detail in later chapters.

Exhortation

Finally, a strong concern in producing this work is to issue
a challenging exhortation to evangelical Christians to adopt the

72. Hunt, Whatever Happened to Heaven?, p. 224.
73. See Part Five, “Objections,” below.
