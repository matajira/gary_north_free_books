138 HE SHALL HAVE DOMINION

and demands righteousness from kings (Dan. 4:1, 25ff). Arta-
xerxes commanded Ezra to appoint magistrates “beyond the
River” which would enforce God’s Law (Ezra 7:25ff). Ezra then
praised him for this (Ezra 7:27).

Most importantly, the moral justification for Israel’s expul-
sion of the Canaanites from the land rests upon the Canaanites’
breach of God’s Law (Lev. 18:24-27). In this passage, Israel is
threatened with the same punishment as the Canaanites if they
commit the lawless acts of the Canaanites. Again, we clearly see
a parity of standard employed in the judgment of pagan na-
tions, as in the judgment of Israel.** This comports well with
the universal call to submission to God’s will in Psalm 2.

Thus, we have seen that the spiritual, temporal, and geo-
graphical separation of pagan states from Israel did not effect
a separation of moral obligation. Because of this, the nations
around Israel were often judged for breaching God’s moral
standards, but never for breaching the Mosaic covenantal
form.** The same truth may be seen earlier in Abraham’s day
in the judgment of Sodom and Gomorrah, Genesis 19:15 (2
Pet. 2:9).

Are the Ten Commandments obliged upon pagans, despite
the Decalogue’s beginning with a distinct reference to Israel’s
redemption from pagan bondage (Exo. 20:1-3; Deut. 5:6-7)?
Dispensationalists answer; no. Are the Ten Commandments,
then, expressly for the covenant community? They answer: yes.

People from all nations are under obligation to God’s Law
today: Romans 1:32 (this speaks of the complex of sins preced-
ing, not any one particular sin); 2:12-15; 3:19; 12:19-13:10; 1
Timothy 1:8. This is expected in light of the coming of the
Messiah (Isa. 2:3-4). God’s Law in our era is considered to be

54. See also: Deut. 7:5-6, 16, 25; 8:11-20; 9:4-5; 12:1-4, 2Off.

55. They were judged for such things as slave trade, loan abuse, witchcraft, and
other non-ritual sins. Lev. 18:24-27; Deut. 7:5-6, 16, 25; 12:1-4; 19:29-32; Amos 1:6
(Exo. 21:16; Deut. 24:7); Nah. 3:4 (Exo. 22:18; Lev. 19:21); Hab. 2:6 (Exo. 22:25-27;
Deut, 24:6, 10-13); Hab. 2:12 (cf. Mic, 3:10).
