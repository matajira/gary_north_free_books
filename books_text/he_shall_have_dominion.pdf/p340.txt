294 HE SHALL HAVE DOMINION

The Eternal State

Temporal history comes to an end with the final judgment,
but life goes on in the eternal state. ‘The Bible does not tell us
as much about that estate, particularly our glorious heavenly
abode, as we might like to know. Berkouwer refers to its revela-
tion in this area as a mere “whisper,” according to Hoekema,
who agrees.’® Nevertheless, it is crystal clear that there is an
everlasting existence for man beyond the judgment, and that
the final estate is, in keeping with the covenantal sanctions
(Deut. 11:26-29; 30:1, 19), two-fold: eternal bliss for the just
and everlasting wrath for the unjust.

In 1 Corinthians 15, we learn that eventually, after Christ’s
enemies have been put down during history and disposed of at
the end of history, then God the Son will turn His kingdom
rule over to the Father. “Now when all things are made subject
to Him, then the Son Himself will also be subject to Him who
put all things under Him, that God may be all in all” (1 Cor.
15:28). This passage speaks of the kingdom being turned over
to the Trinity, not God the Father. The work of redemption is
no longer being prosecuted by the Mediator in eternity.”®

Heaven

The final state for the righteous will be in the glorious pres-
ence of God.” It will be an existence of holy perfection® and
impeccability (1 Thess. 4:17; Heb. 4:9; 12:23), Heaven is not a
state, but a place, for there reside Enoch (Gen. 5:22-24; Heb.
11:5), Elijah (2 Kgs. 2:1, 11), and Christ (Acts 1:9-10) in their
bodies. Not surprisingly there are more people who believe in

75, See: Hoekema, Bible and the Puture, p. 94n. He disputes the Enghsh transla-
tion of GC. C. Berkouwer’s The Return of Christ (Grand Rapids: Eerdmans, 1972), p.
63.

76. Shedd, Dogmatic Theology, 2:690ff.
77, John 14:1-3; cf. Job 19:27; Psa. 17:15; John 17:24.
78. Eph. 5:27; Rom. 8:21; Heb. 12:23; | John 3:2,
