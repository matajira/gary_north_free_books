Cultural Antinomianism 521

for sanctified living. Thus, this same Paul could say: “[W]e know
that the law is good if one uses it lawfully” (1 Tim. 1:8).

Hanko speaks of the Law of God as a “tyrant which followed
the Israelite wherever he went.””? But is this the conception of
the Law held by the saint redeemed by the grace of God, who
longs to honor and glorify His Savior? The Law, which Hanko
calls a “tyrant,” is deemed by the righteous man as a delight
(Psa. 1:1-2), an object of loving adoration (Psa. 119:97), a source
of moral strength (Psa. 119:97), the foundation of civil wisdom
and understanding (Deut. 4:6, 8), a blessing to be sought by the
redeemed (Isa. 2:2-3), a standard for holiness, justice, and
goodness (Rom. 7:12). Truly the Law becomes a “tyrant” to
those who wrongly use it as a means of justification and merit
(1 Tim. 1:8; Gal. 2:16; Jms. 2:10). The abuse of ceremonial
merit is indeed a “burden” (Acts 15:10, cf. 15:1). But we must
not confuse abuse with use.

Conclusion

Hanko’s pessimistic eschatology, when coupled with his
cultural antinomianism, leaves him shouting in vain against the
darkness. Light overcomes darkness; shouting only confuses.

What is that calling [of the Christian]? Not to change the
world. That is impossible. But we do have a solemn calling.
Negatively it is to condemn the world for her sin and for her
rebellion against God and against Christ. We must do this con-
stantly. ... And on the other hand, positively, we must witness
to the truth. We must witness to the fact that the Kingdom of
Christ is heavenly, that the Kingdom of Christ will come when
our Lord comes back again. We must stand in the midst of a
world which madly rushes down the road to destruction and
shout at the top of our voices, “Jesus Christ is King. . . !” We
must do that specifically in connection with the problems of life;
specifically in connection with each individual social problem that

22. Hanko, “An Exegetical Refutation of Postmillennialism,” p. 20.
