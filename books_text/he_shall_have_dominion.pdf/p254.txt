208 HE SHALL HAVE DOMINION

flow forth universal dominion: days of prosperity, peace, and
righteousness lie in the future. Particularly in Isaiah and
Ezekiel, “the catholicity of the Church’s worship is expressed by
all nations flowing to Jerusalem, and going up to the mountain
of the Lord, to the house of the God of Jacob; whereas in Mala-
chi, instead of them going to the temple, the temple is represented as
coming to them. ... [W]e must understand both representations
as designed to announce just the catholicity and spirituality of the
Gospel worship.”®?

These and many other such references refer to the inter
advental age, not to the Eternal State (as per the amillennial
view); for the following reasons.

First, numerous prophetic references speak of factors inap-
propriate to the eternal state, such as the overcoming of active
opposition to the kingdom (e.g., Psa. 72:4, 9; Isa. 11: 4, 13-15;
Mic. 4:3), birth and aging (e.g., Psa. 22:30-31; Isa. 65:20; Zech.
8:3-5) the conversion of people (Psa. 72:27), death (e.g., Psa.
22:29; 72:14; Isa. 65:20), sin (e.g., Isa. 65:20; Zech. 14:17-19),
suffering (e.g., Psa. 22:29; 72:2, 13, 17), and national distinc-
tions and interaction (e.g., Psa. 72:10-11, 17; Isa. 2:2-4; Zech.
14:16-17).

Second, though reduced to minority proportions, there will
be the continuance of the curse, despite the dominance of victory
(Isa. 65:25). Isaiah 19:18 may suggest a world ratio of five
Christians to one non-Christian.”°

Third, some prophetic language is indisputably applied to
the First Advent of Christ. Isaiah 9:6 ties Christ’s Messianic rule in
with His birth: “For unto us a child is born, unto us a son is

68. Psa. 22:27; 46:8-10; 47:3; 66:4; 67:4; 86:9, 67:2; 72:11, 17; 82:8; 86:9;
102:15; Isa. 2:2-3; 25:6-7; 40:5; 49:6, 22-29: 52:15; 55:5; 60:1-7, 10-14; 61:11; 66:19-
20; Jer. 3:17; 4:2; Dan. 7:14; Amos 9:11-15; Mic. 4:1-3; 5:2-4, 16-17; 7:16-17; Hab.
2:14-20; Hag. 2:7ff; Zeph, 3:10; Zech. 2:11; 8:22-23; 979-10; 14:16; Mal. 1:11; 3:1-12.

69, David Brown, Christ’s Second Coming: Will It Be Premillennial? (Edmonton,
Alberta: Still Waters Revival Books, [1882] 1990), p. 347.

70. Alexander, Isaiah, 1:357.
