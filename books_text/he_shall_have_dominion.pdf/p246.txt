200 HE SHALL HAVE DOMINION

The Psalm is a purely prophetic Psalm, having no reference
to David himself, as is obvious from Jesus’ teaching in Matthew
22:42-45 and in that David was not a priest (v. 4). And it clearly
anticipates Christ’s enemies being subjugated by Him. But He
does this while sitting at the right hand of God (“sit until”**),
not in arising, leaving heaven, and returning to the earth at the
Second Advent. That this Psalm is now in force, expecting the
ultimate victory of Christ is evident in both its numerous New
Testament allusions and in that He is already the Melchizede-
kan priest, mentioned in verse 4 (cf. Heb. 7). This peculiar
priest was one who was both king and priest, according to
Genesis 14:18, as is Christ.

His strong rod will rule from Zion, which portrays the New
Covenant-phase Church as headquartered at Jerusalem where
the gospel was first preached. He rules through His rod, which
is His Word (Isa. 2:3; 11:4). He leads His people onward into
battle against the foe (v. 3). The allusion to kings in verse 5,
following as it does the reference to Melchizedek in verse 4,
probably reflects back on Abraham’s meeting with Melchizedek
after his conquest of the four kings in Genesis 14. Because
“kings” is in the emphatic position in Hebrew, it indicates
Christ will not only rule the lowly, but also kings and nations
through His redemptive power, as in Psalms 2 and 72. His rule
shall be over governments, as well as individuals; it will be
societal, as well as personal.

Anticipation in the Prophets

The prophets greatly expand the theme of victory under the
Messiah. I will highlight several of the prophetic pronounce-
ments regarding victory. Due to space limitations only three of
these from Isaiah will be given a fuller treatment.”®

39. The Hebrew adverbial particle ‘d indicates duration. See: J. J. Stewart Per-
owne, The Book of Psalms, 2 vols. (Andover, MA: Warren F. Draper, 1894), 2:292-293.

40. For fuller helpful exposition see: Hengstenberg, Christology of the Old Testa-
