454 HE SHALL HAVE DOMINION

“How Can There Be a Kingdom With the King Absent?”

An objection to postmillennialism that recently has been
much used grows right out of the dispensational hermeneutic
of literalism. It has been asked: How can there be a kingdom
without a king? Hunt radically distorts and corrupts the post-
millennial vision in objecting to postmillennial doctrine: “The
growing acceptance of the teaching that a Christian elite has a
mandate to sct up the kingdom without Christ’s personal pres-
ence is genuine cause for concern.” Pentecost says: “[DJuring
this present age, then, while the King is absent, the theocratic
kingdom is in abeyance in the sense of its actual establishment
on the earth.” “Christ's kingdom is presently in abeyance.
The promised king came to His own and was rejected. David’s
throne is vacant. The king is ‘exiled’ in heaven. . . . Scripture
everywhere repudiates and disproves the doctrine that Christ is
now reigning as Prince of peace, seeking through the church to
extend His kingdom on earth by means of the gospel.”

What does Hunt mean by “a Christian elite”? Concerned
Christians who run for office and are elected? Dedicated Chris-
tians who establish Christian schools? Authors of best-selling
paperback dispensational books? He never quite says.

Why does he deny that Christians have “a mandate to set up
the kingdom”? Christ established it while on earth (as I noted
in Chapter 11). We are ambassadors of our king (2 Cor. 5:20).

What does he mean, we labor without “Christ’s personal
presence”? Does he denigrate the spiritual presence of Christ
now? Is not Christ personally present with us?”

Baker, 1965), p. 422.

12. Dave Hunt, Whatever Happened to Heaven? (Eugene, OR: Harvest House,
1988), p. 43.

13. Pentecost, Things to Come, p. 471.

14. Charles E. Stevens, “The Church of Christ and the Kingdom of Christ in
Contrast,” Prophecy and the Seventies, Charles Lee Feinberg, ed. (Chicago: Moody
Press, 1971), pp. 102-103.

15. Mate. 18:20; 28:20; Rom. 8:9-11; 2 Cor. 13:5; Gal. 4:19; Col. 1:27; 3:16; Heb,
