438 HE SHALL HAVE DOMINION

Though it is true that we should eagerly long for the Return
of Christ, it is neither true that this entails its imminence nor
that this is the only genuine spur to diligence. As far as the
imminent expectation of Christ’s Return goes, the Christian
should deeply long for personal release from this body of sin
and his entry into the glories of heaven, which comes at death.
Yet he labors where God has currently placed him by His prov-
idence: in the God-created world of time and space.

Furthermore, regarding imminence as a spur to holiness, we
all know that we could die this very minute, which is why we buy
life insurance policies. Our premium payments demonstrate that
we are statistically more certain that we will die in a relatively
short time (Psa. 90:4-6, 10; 1 Pet. 1:24) than we are certain that
Christ will return today. Upon exiting life through the door of
death, we will find ourselves in the presence of the Lord our
Judge, where we will give account (2 Cor. 5:8, 10). This ought
to spur us to live for Him, as is indicated in the whole motive
of the Parable of the Rich Barn Owner (Luke 12:16-21).

Still further, every Christian knows that he lives constantly
under the moment-by-moment scrutiny of Almighty God. We
cannot escape His presence during any moment of life, for
“there is no creature hidden from His sight, but all things are
naked and open to the eyes of Him to whom we must give ac-
count” (Heb. 4:13), This motivated David to live for God and to
praise Him for His greatness (Psa. 139). The certainty of our
absolute present openness to the Lord ought to move us to serve
Him more faithfully, even more so than the prospect that He
may possibly return today.”

No spur to holiness is lost by denying the imminency of
Christ’s Return. Besides, if anticipation of the appearing of
Christ at any moment is a major New Testament theme and
ethical spur, how can we account for passages that clearly ex-

35. J. A. Alexander, “The End is Not Yet,” Banner of Truth, No. 88 (Jan. 1971)
Lff.
