The Covenants of Redemption 117

Eve herself (Gen. 2:21-22; Acts 17:26), animals were created en
masse (Gen. 1:20-25). Even angels were created en masse as non-
procreative individuals (Matt. 22:30): a host.

The organic unity of the human race is vitally important to
the redemptive plan of God, as seen in Romans and 1 Corinth-
ians. Adam was the federal head of all mankind: a legal represen-
tative. In him, we are legally and judicially dead (Rom. 5:12-19;
1 Cor. 5:22). Christ is the federal head of all those “chosen out
of” (eklektos) mankind. In Him, we are legally and judicially
declared alive (Rom. 5:15-19; 1 Cor. 15:22). Christ became flesh
in order that He might attach himself to the unified race and
become its Redeemer (Phil. 2:5ff; Heb. 2:14).**

That God’s covenant has societal implications may be seen in
its being established with Abraham and his seed (Gen. 12:1-4).
The significance of Israel’s organic connection is illustrated in
her portrayal as a vine (Psa. 80:8-16; Isa. 5:1-7). In addition,
when God made covenant with Israel in the wilderness, it in-
cluded future generations (Deut. 5:3).

Because of this, God specifically promises covenant blessings
and warns of covenant curses running in communities of people.
Deuteronomy 28 and Leviticus 26 detail specifics of community
curses and blessings, transported from generation to generation
and expansively covering the broad community. This covenan-
tal factor is also demonstrated in Israel's history. For example,
the whole nation of Israel suffered defeat in war due to the
grievous sin of Achan (Josh. 7:1). They were learning corporate
responsibility through this “lesson” from God. Outside of Israel,
pagan communities were destroyed for their corporate evil.”

Neither may Christianity be properly understood in terms of

46. There is no corporate guilt for angels, but neither is there salvation for fallen
angels,

47. Josh. 2:10; 6:21; Exo. 20:16-18; Josh 8:1,2,24-29; 10:29-43; 1 Sam. 15:3. Cf.
Lev. 18:24-27. See: Kenneth L. Gentry, Jr., God’s Law in the Modern World (Phillips-
burg, NJ: Presbyterian & Reformed, forthcoming), ch. 6. Greg L. Bahnsen, Theonanty
in Christian Ethics (rev, ed.; Presbyterian & Reformed, 1984), Part 7.
