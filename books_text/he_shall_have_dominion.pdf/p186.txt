140 HE SHALL HAVE DOMINION

a summary case law from Leviticus 19:18 (Rom. 13:9b). Finally,
he concludes the thought regarding personal vengeance, which
he began in Romans 12:17-19: “Love does no harm {kakon,
“evil”] to a neighbor; therefore love is the fulfillment of the
law” (Rom. 13:10). This involves appropriate social conduct that
is incumbent upon all men, especially Christians — conduct that
avoids “carousing and drunkenness” and “sexual promiscuity
and sensuality” (Rom. 13:13).

His reference to God’s Law* in this context is most impor-
tant. Ultimately, God’s eternal vengeance is according to His holy Law
(cf. Rom. 2:3, 5-6, 12-15), which is encoded in the Mosaic Law.
Proximately and mediatorially, however, God’s temporal “min-
ister,” the civil magistrate, must mete out the “just reward”
(Heb. 2:2; cf. Rom. 7:12; 1 Tim. 1:8) for those for whom the
penalties of the Law were designed: evil-doers. Paul specifies
this even more particularly elsewhere: “The Law is not made
for a righteous person, but for the lawless and insubordinate,
for the ungodly and for sinners, for the unholy and profane,
for murderers of fathers and murderers of mothers, for man-
slayers, for fornicators, for sodomites, for kidnappers, for liars,
for perjurers, and if there is any other thing that is contrary to
sound doctrine.”®? And all of this was “according to the glori-
ous gospel of the blessed God which was committed to my
trust” (1 Tim, 1:9-11), not according to a passé example.

The theonomic position is that God’s Law is the standard for
justice in all areas of life, including criminal penology (if supported
by careful exegesis of the text of each penal sanction). This can
be legitimately deduced from the Romans 12-13 passage. In

58. Earlier he deemed this Law “established” (Rom. 3:31) and called it “holy,
just, and good” (Rom. 7:12).

59. A case may be made for Paul's generally following the order of the Ten
Commandments. H. D. M. Spence, “I and II Timothy,” Ellicott's Commentary on the
Whole Bible, Charles John Ellicott, ed., 8 vols. (Grand Rapids: Zondervan, rep. n.d.),
7:180. At the very least, it may be said that “the apostle now gives a summary of the
law of the Ten Commandments.” William Hendriksen, I and LI Timothy and Titus: New
‘Testament Commentary (Grand Rapids: Baker, 195'7), p. 67.
