Anticipation 205

(Jer. 23:5; 33:13), or as David himself (Jer. 30:9; Ezek. 34:23,
23; 37:24; Hos. 3:5). At His resurrection, He was raised up to
the throne of David (Acts 2:30-31), which represented the
throne of the Lord (1 Chr. 28:5; 29:23). Again, His reign brings
peace, for He is the “Prince of Peace” (Isa. 9:6). This peace
grows incrementally through history: Christ “extends its bound-
aries far and wide, and then preserves and carries it forward in
uninterrupted progression to eternity.”*’ His righteous rule
begins at the first coming of Christ (Luke 1:32-33).

Isaiah 11:9

Isaiah 11:1-10 speaks gloriously of the eschatological hape
begun with Adam, flowing through Noah, and expanded with
Abraham. The rod/branch from the stem/roots spoken of here
continues the thought of the preceding context. The collapse of
David’s house and of the Jewish government is set in contrast
to the fall of Assyria (Isa. 10). The remaining, nearly extinct
house of David, reduced to a stump, still has life and will bud
with a branch. That branch is Christ: He restores the house of
David in the New Testament,” hence the emphasis in the
New Testament on his genealogy from David (Matt. 1:1-17;
Luke 3:23-38°).

This coming of Christ (His First Advent as a stem or branch),
was with the fullness of the Holy Spirit (Isa. 11:2) and leads
to judgment upon His adversaries (v. 4, particularly first-centu-
ry Israel, Matt. 3:1-12; 24:2-34; Rev. 1-19). As in the other
prophecies surveyed, there is the promise of righteousness and
peace flowing after Him. Isaiah describes the peace between

57, Calvin, Commentary on the Book of the Prophet Isaigh, trans. William Pringle
(Grand Rapids: Eerdmans, [n.d.] 1948), 1:96. See later discussion of the principle of
gradualism in Chapter 12, below,

58. Mate. 1:17,18; Mark 11:10; Acts 2:34-36; 13:34; 15:16,

59, See also: Luke 1:27, 32, 69; 2:4; 9:27; 12:23; 15:22; 20:30-31; 21:9, 15; Rom.
1:3; Rev. 3:7; 5:5; 22:16.

60. Matt. 3:16-4:1; 12:17-21; Luke 4:14-21; John 3:34; Acts 10:38.
