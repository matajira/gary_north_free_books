226 HE SHALL HAVE DOMINION

suffer and then enter His resurrected, heavenly glory.”

In response to the Pharisees, Christ specifically declared that
the kingdom does not come visibly with temporal fanfare. “And
when he was demanded of the Pharisees, when the kingdom of
God should come, he answered them and said, The kingdom of
God cometh not with observation: Neither shall they say, Lo
here! or, lo there! for, behold, the kingdom of God is within
you” (Luke 17:20-21). Obviously a spiritual conception of the
kingdom is here demanded, in contradiction to an Armaged-
don-introduced, earthly, political kingdom.

This is why Christ went about preaching what is termed the
“gospel of the kingdom” (Matt. 4:23; 9:35; 24:14; Mark 1:14-15).
He proclaimed a redemptive, spiritual kingdom, Hence, being
exalted to His throne leads to a spiritual effusion of grace, not
the political establishment of an earthly government.”

The Jews made a major accusation against Jesus by saying
that He promoted a political kingdom in competition with
Caesar’s empire. This explains why Jesus was concerned to
discover the source of the accusation ~ He knew of the miscon-
ception of the Jews in this regard. His answer indicates that His
is a spiritual kingdom:

Then Pilate entered the Praetorium again, called Jesus, and said
to Him, “Are You the King of the Jews?” Jesus answered him,
“Are you speaking for yourself on this, or did others tell you this
about Me?” Pilate answered, “Am I a Jew? Your own nation and
the chief priests have delivered You to me. What have You
done?” Jesus answered, “My kingdom is not of this world. If My
kingdom were of this world, My servants would fight, so that I
should not be delivered to the Jews; but now My kingdom is not
from here.” Pilate therefore said to Him, “Are You a king then?”

34, Surely it cannot be denied that at the resurrection and ascension Christ
“entered His glory,” which was evidenced by Pentecost: John 7:39; 12:16; 12:23; Acts
3:13. He is now the “Lord of glory,” cf. Jms. 2:1; 1 Pet. 1:11; 2 Pet. 3:18; Heb. 2:9.

35. Luke 24:44-49; Acts 2:30-35; 3:22-26; 8:12; Eph. 4:8-11.
