548 HE SHALL HAVE DOMINION

to Russia and fsrael Since 1917, Tyler, TX: Institute for Christian
Economics, (1977) 1991. A premillennialist historian studies the
history of failed dispensational prophecy. He warns against
“newspaper exegesis.”

Woodrow, Ralph. Great Prophecies of the Bible. Riverside, CA:
Ralph Woodrow Evangelistic Association, 1971. An exegetical
study of Matthew 24, the Seventy Weeks of Daniel, and the
doctrine of the Anti-Christ.

Woodrow, Ralph. His Truth Is Marching On: Advanced Studies
on Prophecy in the Light of History. Riverside, CA: Ralph Wood-
row Evangelistic Association, 1977. An exegetical study of im-
portant prophetic passages in Old and New Testaments.

Zens, John. Dispensationalism: A Reformed Inquiry into Its Lead-
ing Figures and Features. Nashville, TN: Baptist Reformation
Review, 1973. Brief historical and exegetical discussion by a
(then) Reformed Baptist.

Theonomic Studies in Biblical Law

Bahnsen, Greg L. By This Standard: The Authority of God’s Law
Today. Tyler, TX: Institute for Christian Economics, 1985. An
introduction to the issues of biblical law in society.

Bahnsen, Greg L. Theonomy and Hts Critics. Tyler, TX: Insti-
tute for Christian Economics, 1991. A detailed response to the
major criticisms of theonomy, focusing on Theonomy: A Reformed
Critique, a collection of essays written by the faculty of Westmin-
ster Theological Seminary (Zondervan/Academie, 1990).

Bahnsen, Greg L. Theonomy in Christian Ethics. Nutley, New
Jersey: Presbyterian and Reformed, (1977) 1984. A detailed
apologetic of the idea of continuity in biblical law.

DeMar, Gary. God and Government, 3 vols. Brentwood, Ten-
nessee: Wolgemuth & Hyatt, 1990. An introduction to the fun-
damentals of biblical government, emphasizing self-government.
