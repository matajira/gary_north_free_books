Introduction to Postmillennialism 75

include premillennialism in their statements.” Not even the
second century Apostles’ Creed.” In fact, “early millennialism
was held mostly among Jewish converts. A few Apostolic Fathers
held it as individuals, but those who do not mention the millen-
nium had greater weight of authority and influence: Clement,
Ignatius, Polycarp.”** This is borne out by premillennialism’s
failure to receive creedal status. Even Tertullian and Irenaeus
(who were premillennial) record bricf creeds with no allusions
to a millennium.” What has happened to the evidence for
“pervasive” premillennialism?

Peters’ mistakes were powerfully analyzed and conclusively
rebutted in a 1977 Dallas Theological Seminary master’s thesis
by dispensationalist Alan Patrick Boyd. According to Boyd, he
“originally undertook the thesis to bolster the [dispensational]
system by patristic research, but the evidence of the original
sources simply disallowed this.” He ends up lamenting that
“this writer believes that the Church rapidly fell from New Tes-
tament truth, and this is very evident in the realm of eschatol-
ogy. Only in modern times has New Testament eschatological
truth been recovered.”*? As a consequence of his research,
Boyd urges his fellow dispensationalists to “avoid reliance on
men like Geo. N. H. Peters . . . whose historical conclusions
regarding premillennialism . . . in the early church have been
proven to be largely in error.”

36. Lightner, Last Days Handbook, p. 158.

37, A. Harnack, “Apostle’s Creed,” The New Schaff-Herzog Encyclopedia of Religious
Knowledge, 3 vols. (Grand Rapids: Baker, [L907] 1949), 1:242.

38. W. G. T. Shedd, A History of Christian Doctrine, 2 vols. (Minneapolis, MN:
Klock & Klock, [1889] 1978), 2:390-391. Papias' famous passage on the millennium
was taken from the Jewish Apocalypse of Baruch 29:1-8. See Geerhardus Vos, The
Pauline Eschatology (Phillipsburg, N}: Presbyterian & Reformed, [1930] 1991), p. 233.

89. Irenaeus, Against Heresies 1:10; 3:4; Tertullian, Virgin 1; Against Praexus 2; The
Prescription Against Heretics 13.

40. Boyd, “Dispensational Premillennial Analysis,” p. 91n.

41. Ibid., p. 92.
