Pragmatic Objections 447

T. Shedd, R. L. Dabney, J. II. Thornwell — adhering to the
doctrine of man’s inherent total depravity, this claim is ground-
less.” Lightner is a bit more reluctant to tar evangelical post-
millennialism with the Social Gospel brush, noting evangelical
postmillennialism “needs to be distinguished.”®

The postmillennial concern with social rightcousncss as well
as individual holiness causes lamentation even among some
non-dispensationalists. Peter Masters writes: “May the Lord
keep us all dedicated wholly to the work of the Gospel, and
deliver us from taking an unbiblical interest in social affairs
(especially out of frustration at the poor progress of our evan-
gelistic labours!).”™ In short, social responsibility = danger.

Social Gospel advocacy certainly picked up clements of evan-
gelical postmillennialism. But it reduced the supernatural trans-
formation wrought by regeneration into mere humanistic moral
effort. Immanent forces replaced transcendent ones as the
impetus to advance and the basis of hope. Though many of the
hopes of the two views are similar — the reduction of crime,
poverty, and suffering — the explanations, methodologies, and
goals are vastly different.

Furthermore, as Rushdoony argues, it is erroneous to assert
“that historical succession means necessary logical connection
and succession.” Evangelical postmillennialism is a vastly
different schema for history from Social Gospelism. Liberal H.
Richard Niebuhr traced the development of the Social Gospel
from: (1) Calvinistic postmillennialism, to (2) revivalistic Armin-

62. For helpful discussions of God’s common grace in the world in light of man’s
total depravity, see: Gary North, Dominion and Common Grace: The Biblical Basis of
Progress (Tyler, TX: Institute for Christian Economics, 1987); North and DeMar,
Christian Reconstructian, ch. 7.

63, Lightner, The Last Days Handbook (Nashvillc: Thomas Nelsan, 1990), p. 84.

64. Peter Masters, “World Dominion: The High Ambition of Reconstructionism,”
Sword & Trowel (May 24, 1990) 21.

65. Jean B. Quandt, “Religion and Social Thought,” p. 396.

66. R. J. Rushdoony, “Postmillennialism versus Impotent Religion,” Journal of
Chmistian Reconstruction 3:2 (Winter 1976-77) 123.
