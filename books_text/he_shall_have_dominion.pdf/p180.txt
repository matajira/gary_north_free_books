134 HE SHALL HAVE DOMINION

consistency requires that Matthew 5:21ff not be viewed as un-
dermining His teaching on the permanence of the Law of God.
Christ emphatically taught the Law’s continuing relevance.
Even the little tithes are important (Matt. 23:23). The Law is
the Golden Rule of service to God and man (Matt. 7:12; 22:36-
40). He even upheld the Law’s civil function (Matt. 15:3-6).

The New Testament Confirmation of the Law

The broader New Testament confirmation of the Law may
be illustrated from a number of angles.

The New Testament expressly confirms the Law. Christ based His
teaching on the Law.®® Even the details of the Mosaic case laws
are cited by the Apostles as binding directives.” Paul, the Apos-
tle of Faith, declares that faith confirms the Law (Rom. 3:31).
He even speaks of the perfection of the Law for the New Testa-
ment people (Rom. 7:12, 14).

Christian conduct is based on Law obedience. Law obedience
defines the Golden Rule of social conduct (Matt. 7:12) and
characterizes the conduct of love.” Keeping God’s command-
ments is deemed important to holy living,** in that it promotes
spirituality,” and evidences holiness, justice, and goodness.”*

Gospel preaching depends on the relevance of the Law. The Law of
God has a multiple usefulness for the Christian today. It defines
3° and then convicts men of sin,“ condemns transgres-
sion,” drives men to Christ,” restrains evil,’ guides sanc-

sin

33. Matt. 7:12; 12:5; 19:4; Luke 10:26; 16:17; John 8:17.

34. 1 Tim. 5:17 (Deut. 25:4), 2 Cor. 6:14 (Deut. 22:10), Rom. 10:6-8 (Deut.
30:11-13), Acts 23:1-5 (Exo. 22:28; Lev. 19:15; Deut. 25:2); 1 Cor. 14:34,

35. Matt. 22:36-40; Rom. 13:10; Gal. 5:14; Jms. 2:8.

36. 1 Cor. 7:19; 1 John 2:3,4; 5:3.

37. Rom, 7:12, 16; 8:3-4,

38. Rom, 2:13; 1 Tim. 1:8-10; Heb. 2:2; | Tim. 1:8-10; Heb. 8:10.

39. 1 John 3:4; Rom. 5:13; 7:7, Cf. Matt. 7:23; Titus 2:14; Rom. 3:20 (“iniquity”
is literally “lawlessness”).

40, Mart, 19:16-24; John 7:19; Acts 7:53; Rom. 7:7, 9-11; Jms. 2:9; 1 John 3:4.

41. Deut. 11:26, 28; Rom. 4:15; 7:10; Gal. 3:10; Jms. 2:10,
