264 HE SHALL HAVE DOMINION

world that God created and loves is His creation as i was de-
signed to be: a world in subjection to man, who in turn is in
subjection to God. Thus, God loves His created order of men
and things, not for what it has become (sinful and corrupted),
but for what He intended. This world order was designed to
have man set over it, to the glory of God (Psa. 8; Heb. 2:6-8).
This is why at the very beginning of human history man was a
cultural creature: Adam was to “cultivate” the world (Gen. 1:26-
28), beginning in Eden (Gen. 2:15).

It is important to understand that the New Testament often
speaks of the redemption of the “world” — the very system of
men and things of which we have been speaking. There are
several passages which speak of the world-wide scope of re-
demption. These passages are quite instructive in their eschatol-
ogical data. They clearly present Christ in His redemptive
labors; just as explicitly, they speak of the divinely assured
world-wide effect of His redemption.” In 1 John 4:14, we dis-
cover the divinely covenanted goal of the sending of the Son:
He was, in fact, to be the “Savior of the world.” Thus, in John
3:17 it is set forth very explicitly that “God did not send the
Son into the world to judge the world; but that the world
should be saved through Him.” John 1:29 views Him as in
process of actually saving the world: “the Lamb of God who
takes away the sin of the world.” Even more strongly pul is
1 John 2:2 where it is said that Jesus Christ is “the propitiation
for our sins; and not for ours only, but also for those of the
whole world.” Paul, too, applies the reconciling work of Christ to
the world (2 Cor. 5:19).

It undeniably is the case that these verses speak of a re-
demption that has the world in view. Consider John 1:29. Here
it is stated that Christ is presently in process of “taking away” sin.
“Taking away” here is the translation of a participle based on

85. See Warfield, “Jesus Christ the Propitiation for the Sins of the Whole World”
(1921), Selected Shorter Writings - I, ch. 23.
