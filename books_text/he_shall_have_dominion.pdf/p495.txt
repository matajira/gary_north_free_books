19

THEOLOGICAL OBJECTIONS

So he answered and said to me: “This is the word of the Lora to Zerub-
babel: ‘Not by might nor by power, but by My Spirit,’ says the Lorn of
hosts. ‘Who are you, O great mountain? Before Zerubbabel you shall
become a plain! And he shall bring forth the capstone with shouts of
“Grace, grace to it? ” (Zechariah 4:6-7)

In this chapter, we leave the experiential and historical
objections and enter into more serious objections: those related
to theological aspects of the eschatological question. (Of course,
I have already discussed a great deal of theology in this book!)

“Sin Undermines the Postmillennial Hope”

In the last chapter, I alluded to the Calvinistic doctrine of
the total depravity of man. It is clear that leading postmillennial
scholars have held strongly to this doctrine. In this section, I
will consider the implications of the doctrine that the human
race is composed of depraved sinners, whose sin affects every
aspect of their beings. We must appraise this in light of the
optimistic teaching that this world of depraved sinners will one
day experience universal righteousness, peace, and prosperity
before the Second Advent of Christ. The theological doctrine of the
depravity of man is frequently urged against the prospect of
