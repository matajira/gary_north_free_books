xxvi HE SHALL HAVE DOMINION

the dispensational camp has been willing to discuss in public
the implications of this radical alteration by Pentecost, or ex-
plain exactly why it has not, if correct, overthrown the dispen-
sational system. The dispensational system is in transition.’

Amillennialism

Amillennialism is the most widely held interpretation of
prophecy, primarily because Roman Catholics generally hold it,
although they rarely discuss eschatology. Lutherans also hold it.
Episcopalians, like Roman Catholics, have rarely emphasized
eschatology, so amillennialism has won by default. European
Calvinists (today, this means mainly Dutch Calvinists) have held
it for the last two centuries. They have been the major exposi-
tors of the amillennial system in the twentieth century.

The amillennialist believes that the next major eschatological
event will be the Second Advent of Jesus Christ at the final
judgment. The unified series of events which is called the rap-
ture by dispensationalists is identified by the amillennialist as
immediately preceding the final judgment. Like the premillen-
nialist and the postmillennialist, he believes in the coming of
Christ in the clouds, to whom the living and dead in Christ will
be raised. Like the postmillennialist but unlike the premillen-
nialist, he does not believe that this unified event will take place
a thousand years before the final judgment. It will take place
on the day of final judgment. That is to say, he denies that
there will be any eschatological discontinuity between today and
just before the Second Advent (final judgment). There will be
historical continuity for the gospel. Unlike the postmillennialist
but like the premillennialist, he insists that this is a continuity of
cultural decline and defeat for Christianity until Jesus comes again.

on leaven in the 1987 edition. He found that it was not what Gentry had quoted. He
called Gentry, who looked it up in the 1958 edition. The two versions differed.

10. Dr. Gentry writes a monthly newsletter, Dispensationalism in Transition, pub-
lished by the Institute for Christian Economics: P.O. Box 8000, Tyler, TX 75711.
