Pragmatic Objections 427

Church is subjected in many an erstwhile Christian country to
the point of virtual extinction, it is not at all surprising, that the
postmillenarian view should at present be at low ebb.”*

Adams notes that “the advent of two World Wars . . . virtual-
ly rang the death knell upon conservative postmillennialism as
well. . . . It is spurned as highly unrealistic because it predicts
a golden age around the corner in a day in which the world
nervously anticipates momentary destruction by nuclear war-
fare.” Hamilton writes: “The events of the past thirty years have
revealed the fallacy of such reasoning. World War I shattered
the hopes of the advocates of peace through international coop-
eration, in the Hague Peace Conference. The failure of the
League of Nations and the breaking out of World War II, have
given the final death blow to any hopes of the ushering in of an
era of universal peace and joy through the interplay of forces
new in action in the world.” Berkhof assumes that “the experi-
ences of the last quarter of a century” are inimical to postmil-
lennialism. Premillennialist Erickson follows suit: One problem
with postmillennialism “is its optimism concerning the conver-

. sion of the world, which seems somewhat unrealistic in the light
of recent world developments.”*

Neo-orthodox and liberal scholars also dispute postmillen-
nialism on this basis. In analyzing the decline of postmillen-

3. D. H. Kromminga, The Millennium in the Church (Grand Rapids: Eerdmans,
1945), p. 264-265. Herman Hanko, “An Exegetical Refutation of Postmillennialism”
(unpublished conference paper: South Holland, IL: South Holland Protestant
Reformed Church, 1978), p. 26.

4. Jay E. Adams, The Time Is at Hard (n.p.: Presbyterian & Reformed, 1966), p.
2. Floyd E. Hamilton, The Basis of Millennial Faith (Grand Rapids: Eerdmans, 1942),
p. 22. Louis Berkhof, Systematic Theology (Grand Rapids: Eerdmans, 1941), p. 719.
Millard J. Erickson, Contemporary Options in Eschatology: A Study of ihe Millennium
(Grand Rapids: Baker, 1977), p. 71. See also: William E. Cox, Biblical Studies in Final
Things (Nutley, NJ: Presbyterian & Reformed, 1966). Robert G. Gromacki, Are These
the Last Days? (Old Tappan, NJ: Revell, 1970), p. 179. William §. LaSor, The Truth
Abou Armageddon (New York: Harper & Row, 1982), p. 160. Leon J. Wood, The Bible
and Future Events (Grand Rapids: Zondervan, 1973), p. 38. Bruce Milne, What the
Bible Teaches About the End of the World (Wheaton, IL: Tyndale, 1979), p. 80.
