4

INTRODUCTION TO
POSTMILLENNIALISM

The Lorp said to my Lord, “Sit at My right hand, Till I make Your
enemies Your footstool.” (Psalm 110:1)

We do not hold with the philosophy of linguistic analysis
that problems of definition lie at the heart of all ambiguity.’
Yet often enough, carefully defining a theological position will
help correct many unnecessary misconceptions. Probably more
than any of the three other evangelical views, postmillennialism
has suffered distortion through improper definition by its op-
ponents. In this chapter, I will attempt to set forth a succinct
theological explanation of postmillennialism, as well as briefly to
engage the question of postmillennialism’s historical origins.

Confusion Regarding Postmillennialism

It is remarkable that there are some noted theologians who
do not appear to have an adequate working definition of post-

1. Ludwig Wittgenstein wrote in his preface to his Tractatus Logico-Philosophicus
that “what can be said at all can be said clearly.” Wittgenstein, Tractatus Logico-
Philosophicus, trans. by D. F. Pears and B. EF McGuinness (New York: Humanities
Press, 1961), p. 3.
