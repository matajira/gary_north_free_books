Theological Objections 459

(2 Cor. 4:5, 12, 14-15). Interestingly, Gaflin seems aware of
this fact, when he admits that “strictly speaking, [Paul’s state-
ments] are autobiographical.” That is the whole point: these
statements were autobiographical. They were not prophecies.
Furthermore, Gaffin’s comments are far too sweeping in
their assertions: “Over the interadvental period in its entirety,
from beginning to end, a fundamental aspect of the church’s
existence is (to be) ‘suffering with Christ’; nothing, the New
Testament teaches, is mere basic to its identity than that.” Is
suffering (persecution?**) throughout the “entirety” of the
intcradvental period a “fundamental” aspect of the church’s
existence? Is there absolutely “nothing . . . more basic” in the
New Testament? If we are not suffering (persecution?), are we
a true Church? Is Gaffin suffering greatly? Gaffin’s statements
are inordinately applied in an attempt to win points for his
pessimistic eschatological view. Surely they are overstatements.

Philippians 3:10

In Philippians 3:10, Gaffin’s second major reference, Paul
writes: “That I may know him, and the power of his resurrec-
tion, and the fellowship of his sufferings, being made conform-
able unto his death.” Of this verse Gaffin notes: “Paul is saying,
the power of Christ’s resurrection is realized in the sufferings of
the believer; sharing in Christ’s sufferings is the way the church
manifests his resurrection-power. Again, as in 2 Corinthians
4:10-11, the locus of eschatological life is Christian suffering”
(p. 213). But is Paul’s reference to suffering here contrary to

31. E E Bruce, I and 2 Corinthians in Ronald E. Clements and Matthew Black,
eds., The New Century Bible Commentary (Grand Rapids: Eerdmans, 1971), p. 194.
Philip E. Hughes, The Second Epistle to the Corinthians (New International Commentary on
the New Testament) (Grand Rapids: Eerdmans, 1962), p. 135.

32. Gaffin, “Theonomy and Eschatology,” p. 211.

33. If persecutional suffering is not in Gaffin’s mind here, then all other forms
of suffering are irrelevant to the argument contra postmillennialism,
