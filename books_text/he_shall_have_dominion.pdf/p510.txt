464 HE SHALL HAVE DOMINION

such temporary-typological elements and was expected only of
Israel, encoded numerous divinely ordained moral require-
ments, which are the perpetually obligatory commandments of
God. Moral requirements may be distinguished from the histor-
ical and redemptive trappings in which they are found.

Bahnsen clearly demonstrates that moral commandments
(justice-defining) are distinguishable from distinctive ceremonial
laws (redemption-expounding), as is evidenced in the Old
Testament itself. God contrasts the moral and ceremonial,
when He says: “For I desire mercy and not sacrifice, and the
knowledge of God more than burnt offerings” (Hos. 6:6).

Elsewhere we witness the same: “Then Samuel said: Has the
Lorp as great delight in burnt offerings and sacrifices, as in
obeying the voice of the Lorn? Behold, to obey is better than
sacrifice, and to heed than the fat of rams” (1 Sam. 15:22).
David writes: “Deliver me from bloodguiltiness, O God, The
God of my salvation, And my tongue shall sing aloud of Your
righteousness. O Lord, open my lips, And my mouth shall show
forth Your praise. For You do not desire sacrifice, or else I
would give it; You do not delight in burnt offering. The sacrifi-
ces of God are a broken spirit, A broken and a contrite heart;
These, O God, You will not despise” (Psa. 51:14-17). See also
Proverbs 21:3 and Isaiah 1:10-17.

2. God’s Law was in fact designed to be a model for the nations.
“Therefore be careful to observe them; for this is your wisdom
and your understanding in the sight of the peoples who will
hear all these statutes, and say, Surely this great nation is a wise
and understanding people. For what great nation is there that
has God so near to it, as the LorD our God is to us, for whatev-
er reason we may call upon Him? And what great nation is
there that has such statutes and righteous judgments as are in
all this law which I set before you this day?” (Deut. 4:6-8).*

45. Greg L. Bahnsen, Theonomy in Christian Ethics (rev. ed.; Phillipsburg, NJ:
Presbyterian & Reformed, 1984), ch. 9.

46. See also: 1 Kgs. 10:1, 8-9; Isa. 24:5; 51:4; Psa. 2:918; 47:1-2; 94:10-12; 97:1-2;
