Postmillennialism and Suffering 539

Why do the nations conspire and the peoples plot in vain? The
kings of the earth take their stand and the rulers gather together
against the Lorn and against his Anointcd Onc. “Let us break
their chains,” they say, “and throw off their fetters.” The One
enthroned in heaven laughs; the Lord scoffs at them. Then he
rebukes them in his anger and terrifies them in his wrath, say-
ing, “I have installed my King on Zion, my holy hill.” I will
proclaim the decree of the Lorn: He said to me, “You are my
Son; today I have become your Father. Ask of me, and I will
make the nations your inheritance, the ends of the earth your
possession. You will rule them with an iron scepter; you will
dash them to pieces like pottery.” Therefore, you kings, be wise;
be warned, you rulers of the earth. Serve the Lorp with fear and
rejoice with trembling. Kiss the Son, lest he be angry and you be
destroyed in your way, for his wrath can flare up in a moment.
Blessed are all who take refuge in him (Psa. 2).

Therefore, it is the unshakable confidence of the Suffering
Church that she one day will be the Victorious Church. Her
persecuted members will rule in the midst of the enemy: “To
him who overcomes and does my will to the end, I will give
authority over the nations - ‘He will rule them with an iron
scepter; he will dash them to pieces like pottery’ — just as I have
received authority from my Father. I will also give him the
morning star” (Rev. 2:26-28). Amillennialists do not agree.

In amillennialism’s eschatology of predestined historical
suffering, Christians are told to expect Christianity’s influence
to diminish steadily in history. They are expected to suffer
ever-greater persecution at the hands of rebellious covenant-
breakers. Christians are expected to prove their faith by experi-
encing ever-greater sickness and poverty, in contrast to the
message of the fundamentalists’ hcalth-and-wealth gospel. The
amillennialist elevates the instrumental function of suffering to
the level of a predestined eschatological goal. Amillennialism
preaches ever-greater suffering unto cultural defeat; postmillen-
nialist preaches ever-reduced suffering unto cultural victory.
