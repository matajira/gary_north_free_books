Pragmatic Objections 429

the defeat of Japan in Asia and Germany in Europe?
There are deeper responses to such objections, however.
These are basically three-fold.

1. Narrow Sampling

Such historical experience arguments involve too narrow a
sample. The better question regarding historical development
is: Have world circumstances and particularly conditions for the
Christian Church improved since Christianity’s inception in the first
century? That is, taking into account the big picture, the histori-
cal long run: Are Christians as a class today generally better off
than were Christians as a class of the first two or three centu-
ries? Are world conditions better today in Christian-influenced
areas than they were in the first century? Anyone who is cogni-
zant of the Roman persecutions against the early Church
should be quite aware that Christians today are in a much
better situation in most places on earth.

In debates on the subject, I point out the irony of this objec-
tion to postmillennialism in light of the circumstances of the
debate: “Here we are in a free land, sitting in our comfortable
Bible-believing church, dressed in our ‘Sunday best,’ holding
one of our many personal Bibles (the world’s largest selling
book!) debating whether or not there has been any advance in
the conditions of Christianity since its persecuted inception
2,000 years ago!” Ironically, the one who most vigorously be-
moans the decline of Christianity, Hal Lindsey, is the author of
a book published (before Bantam Books bought the rights) by
one of the nation’s largest Christian publishers, one of the
largest selling books of the last twenty years, selling thirty-five
million copies in fifty-four languages: The Late Great Planet
Earth"! 1. may be the case that we have witnessed a decline in
America over the last fifty or one hundred years, (It may even

9. As reported in Hal Lindsey and Chuck Missler, “The Rise of Babylon and the
Persian Gulf Crisis" (Palos Verdes, CA: Hal Lindsey Ministries, 1991), p. 64.
