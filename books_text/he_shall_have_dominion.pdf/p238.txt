192 HE SHALL HAVE DOMINION

Furthermore, the Promised Land served as a type of the
whole earth (which is the Lord’s, Psa. 24:1). It is, as it were, a
lithe to the Lord of the entire earth. As such, it pictured the rest
brought by Christ’s kingdom, which shall cover the earth (see
Hebrews 3-4). “Hebrews 11:8-16 shows that although Abraham
received the physical land of Canaan, he was looking forward to
the eternal city and Kingdom of God. Canaan is a type of the
new heavens and earth that began with the first advent of
Christ, in seed form (Gal. 4:26; Heb. 12:22-29}."1 In Psalm
37:11, the psalmist speaks of God’s promise to His people: “But
the meek shall inherit the land.” But Jesus takes this promise
and extends it over the entire earth in Matthew 5:5! Abraham
apparently understood the land promise as a down payment
representing the inheriting of the world (Rom. 4:13). Paul ex-
pands the Land promises to extend across all the earth, when
he draws them into the New Testament (Eph. 6:3). In several
divine covenants, we can trace the expansion of these Land
promises: Adam was given a garden (Gen. 2:8); Abraham's seed
was given a nation (Josh. 1); the New Covenant Church was
given the world (Matt. 28:18-20).””

But the fundamental blessedness of the Abrahamic Cove-
nant, like that of the Adamic Covenant before it, was essentially
redemptive rather than political. The seed line was primarily
designed to produce the Savior; the Land promise was typolog-
ical of the Savior’s universal dominion. The Abrahamic Cove-
nant involved a right relationship with God, as indicated in
Genesis 17:7: “And I will establish My covenant between Me
and you and your descendants after you in their generations,
for an everlasting covenant, to be God to you and your descendants
after you.” That which is most important in the plan of God is
the spiritual relation, rather than the relation of blood (John

16. W. Gary Crampton, “Canaan and the Kingdom of Ged — Conclusion,”
Journey 6 (Jan./March 1991) 19.

17, See my The Greatness of the Great Commission: The Christian Enterprise in a Fallen
World (Tyler, TX: Institute for Christian Economics, 1990), Part II.
