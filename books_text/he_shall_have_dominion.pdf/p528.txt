482 HE SHALL HAVE DOMINION

which speaks of the first century generation (Matt. 24:34). In
fact, the preceding context speaks of Jerusalem’s destruction
(Luke 17:22-37).

In the final analysis, it should be noted that no evangelical
millennial view supposes there will be no faith on the earth at
the Lord's return! Yet, to read the statements regarding Luke
18:8 and its supposed expectation of a negative answer, one
would be pressed to assert that Christianity will be totally dead
at His return!

Thus, it is clear that this passage is radically misunderstood
when urged against postmillennialism. Its standard is misinter-
preted. The Lord’s teaching regarding fervent prayer is
changed into a warning regarding the existence of the Christian
faith in the future. Its grammar is misconstrued, The grammar
that is indicative of concern becomes an instrument of doubt.
Its goal is radically altered. Rather than speaking of soon-com-
ing events, it is made to point to the end of history. Its final
result is overstated even if all the preceding points be dismissed:
No critic of postmillennialism teaches that “the faith” will have
vanished completely from the earth at Christ’s Return.

Luke 22:29-30 and Matthew 19:28

And I bestow upon you a kingdom, just as My Father bestowed
one upon Me, that you may eat and drink at My table in My
kingdom, and sit on thrones judging the twelve tribes of Israel.
(Luke 22:29-30)

So Jesus said to them, “Assuredly I say to you, that in the regen-
eration,” when the Son of Man sits on the throne of His glory,
you who have followed Me will also sit on twelve thrones, judg-
ing the twelve tribes of Israel.” (Matt. 19:28)

30. Note that Luke’s version of this thought substitutes the word “kingdom” for
“regeneration.”
