Characters 369

they would receive him. But they did not, which must mean
that Elijah is yet to come. The reason Christ could make refer-
ence to John the Baptist as he did was that John the Baptist
came in the spirit and power of Elijah (Luke 1:17). It therefore
seems obvious that there was a principle in relation to Elijah
which was also true of John the Baptist, and the reference
made by Christ was by way of application and not interpreta-
tion.”* In short, John might have been Elijah, but was not.
“Close, John, but no cigar!”

But Matthew 17 is unambiguously clear. In Matthew 11,
Christ is rebuking the spiritual obstinacy (11:16ff) of the crowds
that came to hear Him (11:7). He urges them to hear and
understand (11:15). He does not fear that they will derail prophetic
fulfillment by their unbelief! When He says, “He who has ears to
hear” (11:15), He does not imply the possible invalidity of His
observations on John, but alludes to the spiritual dullness of
those hearers who reject those observations (Matt. 13:9, 43;
Mark 4:9; Luke 8:8; 14:35). The reason why John came in the
“spirit and power of Elijah” (Luke 1:17), and why he should
have been received as “Elijah who was to come” (Matt. 11:14),
is because he was the literal fulfillment of the Elijah prophecy.

Neither may John Baptist’s denial of being Elijah (John 1:21)
be inimical to his fulfilling the prophecy.? His denial was with
regard to his being the actual corporeal return of Elijah from
heaven which was widely anticipated among the Jews. At one
place in the Talmud it is written: “But when God shall bring
[Elijah] to life in the body, he shall send him to Israel before
the day of judgment.”® This sounds dispensational to me!

4, Herman Hoyt, “A Dispensational Response,” The Meaning of the Millennium:
Four Views, Robert G. Clouse, ed. (Downer’s Grove, IL: InterVarsity Press, 1977), pp.
147-148. Cf. Louis A. Barbieri, Jr., “Matthew,” The Bible Knowledge Commentary: New
Testament, John F. Walvoord and Roy B. Zuck, eds. (Wheaton: Victor, 1983), p. 60.

5. John EF Walvoord, Prophecy Knowledge Handbook (Wheaton, IL: Victor, 1990),
p- 339.

6. See sampling of Talmudic references in: Lightfoot, Commentary on the New
Testament from the Talmud and Hebvaica, 2:243-247.
