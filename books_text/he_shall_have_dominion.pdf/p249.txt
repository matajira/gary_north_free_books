Anticipation 203

discipled in His ways and learn the strictures of holiness from
His Law (Isa. 2:3). The coming of the eschatological fulfillment
of redemption (Gal. 4:4) leads to the permanent establishment
of Christianity as an agency of gracious influence in the world
to salvation and sanctification. Evangelism is indicated in the
flowing river of people urging others to “come, go ye” to the
house of God (Isa. 2:3). With the overwhelming numbers being
converted to a saving knowledge of Christ and being discipled
in God’s Law, great social transformation naturally follows
(Isa. 2:4). “It is a picture of universal peace that Isaiah gives,
but it is a religiously founded peace.”*? The peace with God
(vv. 2-3) gives rise to peace among men (v. 4).

Amillennialist Hanko disposes of this postmillennial text as
treated by Boettner with an incredible sweep of the hand:
“Now it is true that Mount Zion has a symbolic and typical
meaning in Scripture. It is also true that the reference is often
to the Church of Jesus Christ — as Boettner remarks in connec-
tion with Hebrews 12:22. But one wonders at the tremendous
jump which is made from the idea of Mount Zion as symbolic
of the Church to the idea that ‘the Church, having attained a
position so that it stands out like a mountain on a plain, will be
prominent and regulative in all world affairs.’ There is not so
much as a hint of this idea in the text. The conclusion is wholly
unwarranted.” Having granted that Mount Zion is “symbolic
of the Church,” how can Hanko legitimately call the postmillen-
nial argument a “tremendous jump” with “not so much as a
hint” and “wholly unwarranted”? Hanko’s argument is merely
a loud denial rooted in his pre-disposition to amillennialism.
What we need here is careful exegesis, not loud assertions as a
substitute for exegesis.

50. See Gentry, Greatness of the Great Commission; North, Millennialism and Social
Theory.
51. E. J. Young, The Book of Isaiah (Grand Rapids: Eerdmans, 1965), 1:107.

52. Herman Hanko, “An Exegetical Refutation of Postmillennialism” (South
Holland, IL: South Holland Protestant Reformed Church, 1978), p. 6.
