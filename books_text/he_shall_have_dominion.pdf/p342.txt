296 HE SHALL HAVE DOMINION

of Tarsus (d. A.D. 396), Theodore of Mopsuestia (d. A.D. 429),
and others. Augustine states that in his day there were “mullti-
tudes who did not believe in eternal punishment.” Neverthe-
less, the doctrine was held by such church fathers as Barnabas
(ca. A.D. 120), Ignatius (d. A.D. 117), Justin Martyr (A.D. 110-
165), Irenaeus (A.D. 130-202), Tertullian (A.D. 160-220).5
Denial of hell was condemned by the Council of Constantinople
(A.D. 543).

Basically there are three false views of the afterlife of the
wicked promoted among biblical scholars: Universalism teaches
that the wicked will all be saved in the afterlife. Advocacy of
universalism range from Origen, “the first Christian Universal-
ist,”®> to modern writers such as Nels Ferré, D. 2 Walker,
William Barclay, and Thomas B. Talbot.*® F. D. E. Schleier-
macher was the most influential popularizer of universalism in
the nineteenth century, laying the ground work for its appear-
ance among twentieth-century Christians, where it is experienc-
ing “a significant resurgence in recent years.”*”

Restorationists argue that the wicked will be punished for a

83. Cited in Philip Schaff, History of the Christian Church, 8 vols. (5th ed.; Grand
Rapids: Eerdmans, [1910] 1985}, 2:612.

84. Martyr, Apology 1:8, 21; Irenaeus, Against Heresies 2:28:7; 3:4:1; Barnabus,
Epistle 20. For a list of early fathers holding to the doctrine, see: Harry Buis, “Hell,”
The Zondervan Pictorial Bible Encyclopaedia, Merrill C. Tenney, ed. (Grand Rapids:
Zondervan, 1976), p. 116. Hereafter cited as ZPEB.

85. Schaff, History, 2:611. Though many think Origen taught the final restoration
of Satan, this dacs not seem to be the case. See: Epistle to the Romans 1:8:9 (Opera
4:634) and Ad quosdam amicos Alexandria (Opera 1:5), as cited in Schaff, 2:611, n 3. For
a brief history of universalism, see: Richard J. Bauckham, “Universalism: A Historical
Survey,” Evangelical Review of Theology 15 (Jan. 1991) 22-35.

86. D. B Walker, The Decline of Hell (London: Routledge & Kegan Paul, 1964).
Nels Ferré, The Christian Understanding of God (New York: Harper, 1951), pp. 228ff.
William Barclay, A Spiritual Autobiography (Grand Rapids: Eerdmans, 1975), pp. 60ff.
Thomas B. Talbott, “The Doctrine of Everlasting Punishment,” Faith and Philosophy
7 Jan. 1990) 19-42. In addition see John A. T. Robinson, Jn the End God (2nd ed;
London: Collins, 1968), chaps. 10-11. Paul Knitter holds there are more true reli-
gions than Christianity. Knitter, Ne Other Name (Maryknoll, NY: Orbis, 1985).

87. Erickson, “Is Universalistic Thinking Now Appearing Among Evangelicals?”
Action (Sept/Oct. 1989) 4-6,
