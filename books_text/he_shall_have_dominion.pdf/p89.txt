The Purpose of This Treatise 43

Scripture that would lead to the postmillennial system. The best
postmillennialism can come up with is a position built upon an
inference.”®' Richard A. Young writes: “The primary weakness
of postmillennialism . . . is that it lacks exegetical support.”

After citing an optimistic, postmillennial conception of histo-
ry, amillennialist George Murray complains of the doctrine’s
absence in the New Testament. “One cannot but regret, howev-
er, that with the Bible in his hand, the writer did not produce
chapter and verse to prove his contention. The obvious reason
is that no such plain promise could be quoted from the New
Testament, for neither Jesus Christ nor His apostles gave the
slightest indication of any real rest for the church until she
enters upon the rest prepared for the people of God on the
other side of death.” Erickson largely agrees: “Perhaps more
damaging to postmillennialism is its apparent neglect of Scrip-
tural passages (e.g., Matt. 24:9-14) that portray spiritual and
moral conditions as worsening in the end times. It appears that
postmillennialism has based its doctrine on very carefully select-
ed Scriptural passages.”

Amillennialist Richard B. Gaffin also doubts the New Testa-
mental validity of postmillennialism, when he criticizes postmil-
lennial advocacy. “Briefly, the basic issue is this: Is the New
‘Testament to be allowed to interpret the Old — as the best, most
reliable interpretive tradition in the history of the church (and
certainly the Reformed tradition) has always insisted? . . . Will
the vast stretches of Old Testament prophecy, including its
recurrent, frequently multivalent apocalyptic imagery, thus be
left without effective New Testament control and so become a
virtual blank check to be filled out in capital, whatever may be

61. House and Ice, Dominion Theology, pp. 9-10.

62. Richard A. Young, “Review of Dominion Theology: Blessing or Curse?” Grace
Theological Journal 2:1 (Spring 1990) 115.

63. George Murray, Millennial Studies: A Search for Truth (Grand Rapids: Baker,
1948), p. 86.

64. Erickson, Contemporary Options in Eschatology, p. 72.
