194 HE SHALL HAVE DOMINION

The New Testament clearly informs us of the spiritual impli-
cations of the seed, in terms of the blessings for the nations.
Abraham has become “the father of circumcision to them who
are not of the circumcision only, but who also walk in the steps
of that faith of our father Abraham. . .. Therefore it is of faith,
that it might be by grace; to the end the promise might be sure to all
the seed; not to that only which is of the law, but to that also
which is of the faith of Abraham; who is the father of us all”
(Rom. 4:12, 16). “Know ye therefore that they which are of
faith, the same are the children of Abraham. And the scripture,
foreseeing that God would justify the heathen through faith,
preached before the gospel unto Abraham, saying, In thee shall
all nations be blessed. . . . If ye be Christ’s, then are ye Abra-
ham’s seed, and heirs according to the promise” (Gal. 3:7-8,
29). Thus, as we shall see in our next section, the Old Testa-
ment kingdom prophecies anticipate the sharing of the coven-
antal glory with others universally.**

Due to redemption, the curse of Genesis 3 upon all men is
countered by the Abrahamic covenant, in which begins the
“nullifying of the curse.”** The expectation of victory is so
strong that we may find casual references based on confident
expectation. The seed is promised victory in accordance with
the original protcevangelium. Abraham’s seed is to “possess the
gates of the enemy” (cf. Gen. 22:17 with Matt. 16:18).”* Gene-
sis 49:8-10 promises that Judah shall maintain the scepter of

24. Isa. 25:6; 45:22; 51:4-6; Mic. 4:Hf.

25. G. Charles Aalders, Genesis, trans. William Heynen, 2 vols., in The Bible
Student's Commentary (rand Rapids: Zondervan, [n.d.] 1981), 1:270. See also: Hans K.
LaRondelle, The Israel of God in Prophecy (Berrien Springs, MI: Andrews University,
1983), p. OL.

26, “Enmity” in Genesis 3:15 (‘ybah) is related to the verb (‘yh), In participial form
it “occurs repeatedly, alluding frequently to the very struggle between God's and
Satan’s people.” Abraham possesses the gates of his enemies (Gen. 22:17). Judah
overcomes his enemies (Gen. 49:8). God shatters His enemies (Exo. 15:6) and will be
an enemy to Israel’s enemies (Exo. 23:22). The Canaanites are Israel’s enemies
(Deut. 6:19). Robertson, Christ of the Covenants, p. 96n,
