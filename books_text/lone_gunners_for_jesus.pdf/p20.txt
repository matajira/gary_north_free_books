18 LONE GUNNERS FOR JESUS

legislate against abortion. The matter is judicial. The matter is
civil-political.

The Technology of Low-Cost Murder

Now come chemical abortifacients. This is what is going to
stop physicians from practicing abortion in the United States.
Price competition is the greatest threat to the local abortionist,
not some self-appointed revolutionary bringer of justice like
yourself. It is the fact that women will be able to walk into some
large store or local drug store and buy some pills - “morning
afler” pills — for $10.95 (or $39.95, on special, for a giant, econ-
omy-size bottle), take those pills, and kill their own infants.
Price competition is the looming treat to the killer physicians,
not the pro-life movement.

When such products are finally for sale, the pro-life move-
ment will be forced to come to grips with the judicial reality of
abortion, namely, that is it is legal. At that point, all of the pro-
life posturing had better end. We will have fewer picket lines in
front of physicians’ offices. It will do no good to picket a physi-
cian if he has been driven out of the death business because it
is so cheap to get home-based abortion out of a bottle. He is
not going to be practicing abortions. Mothers will be practicing
abortion. It will do no good to go out and shoot physicians.
The advent of cheap abortifacients is when the legalization of
abortion visibly becomes the real problem, when it becomes
discount abortion, mass-produced abortion.

The problem is the legalization of abortion, not some killer
physician on the corner. The physician on the corner who is
practicing abortion is a symbol, and it is good to challenge the
symbol in the name of the real issue, which is the legalization of
abortion. But that local physician is not the main problem. The
big problem is that modern technology is going to find a way to
have dirt-cheap abortions and make millions of dollars along
the way. We are going to get mass-produced abortions. We will
no longer worry about a comparative handful of abortions: a
