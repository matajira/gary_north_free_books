A SECOND LETTER TO PAUL J. HILL

Today is October 18, 1994. I have now received three letters
from you, dated October 7, October 10, and October 11. While
you remained silent in front of the initial jury that condemned
you, you have been quite lengthy in your responses to me.

You asserted in your October 7 letter: “I am a member of
the Trinity Church in Valpraiso FL where Archie Jones is a
member.” You are not a member; you were excommunicated
well over a year ago. (I will deal with this later.) But Dr. Jones
was my employee in 1980. James Jordan is a member of the
Trinity Presbyterian Church. He is still on my payroll, part
time. You said in your letter that you were a friend of Greg
and Gloria Keen in seminary. Greg Keen abandoned his wife in
1992 while a member of the same church as I was, which
promptly and properly excommunicated him, Pathways in your
past lead to me. Please regard this letter as a sign to the Chris-
tian public that says “Detour.”

Presbyterian Law

I begin with your October 10 letter, since it dealt with the
important issue of Presbyterian law. You quoted the Westmins-
ter Confession of Faith, as well as its Shorter Catechism (1646).
You were once ordained as a Presbyterian minister, which
required your public affirmation of these documents as judicial-
ly binding on you.
