82 CGHRISTIANITY AND CIVILIZATION

ment is accurate: Television and itinerant evangelists preach
traditional revivalistic Arminianism, make use of expensive
public relations machinery, and employ methods borrowed
from the entertainment industry. In a more profound sense,
however, revivalism today manifests itself in mainline Protes-
tant denominations. Revivalism has been institutionalized
and the result has been a de facto Baptistic ecclesiology and
church government in most American churches.

The most obvious effect of revivalism on local churches
has been its divisiveness. Following the First Great Awaken-
ing, for instance, many New Light Presbyterians and Con-
gregationalists separated from their churches and were later
absorbed or organized into Baptist churches.155 The methods
used to conduct revivals, moreover, directly undermined the
stability of the local congregation. Many of the preachers in
the first revivals were itinerants, a method of evangelism that
puts minimal emphasis on the local church. Church members
might be inclined to prefer nationally prominent revivalists to
their local pastor. 56

Individual churches were affected in more subtle ways as
well. Ideas of the very nature of the church have undergone
radical revision. Arminian revivalism hastened the disestab-
lishment of state churches in the late eighteenth and early
nineteenth centuries, and these two factors combined to pro-
duce a voluntaristic view of the church.45? The church came
to be viewed as an assembly of individuals, democratically
controlled, and undemanding, That membership in a church
requires submission to authority and a permanent and serious
commitment surprises, even angers many American Chris-
tians. The medieval and Reformation idea that church mem-

 

Finney, Moody, and Sunday on the obe hand and the evangelistic crusades
of Billy Graham on the other. McLoughlin, Afodern Revivatism, chapter 9.

155. See Goen, chaprers 2 and 6. Significantly, the ecclesiological reasons
for separation were usually twofold: First, separates, most of them New
Lights converted in the Awakening, protested against the reception of
unregenerate men into church membership, and secondly, they despised the
use of creeds as a test of orthodoxy. Goen, pp. 36-40. See Sutton, “The Bap-
tist Failure,” pp. 175-180, for a discussion and refutation of Anabaptist ec-
clesiology. It should be noted, however, that these radical Anabaptist
tendencies were modified by the Calvinistic elements of the Awakening.
Gaustad, p. 120.

156, For the effects of itineracy, see Tracy, pp. 424 ff.

157. Mead, The Lively Experiment, chapter VII.
