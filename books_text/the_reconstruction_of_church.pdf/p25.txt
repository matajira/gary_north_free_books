THE CHURCH: AN OVERVIEW 13

ment of Holy Baptisrn places a person into the Ghurch. It is a
sovereign act of God. When a baby is baptized, he does not
know what is happening to himself, but God is placing him in
His Church. We are to count and treat him as a Christian
from that day forward. The Bible tells the Church whom to
baptize, and the Church binds men to God on earth in accor-
dance with the heavenly revelation of the Bible.

The other sacrament is the Holy Eucharist (Thanksgiv-
ing), also called the Lord’s Supper, or Holy Communion, in
various churches. Christ is specially present at His Supper,
and He expects us to attend weekly. Sadly, most churches no
longer have weekly communion, though this is beginning to
change today. Special blessing and curse attach to the obser-
vance of the Lord’s Supper, for Paul says that those who eat
and drink in an unworthy manner (with self-conscious,
unrepentant sin), bring judgment to themselves (1 Corin-
thians 11:29-32). People who continue in unrepentant sin are
to be excommunicated from the Table, loosed from the bonds
of the Church, and regarded as heathen (1 Corinthians
539-13).

This, then, is the heart of the special power of the Church.
It is the power to determine who is to be counted as a Chris-
tian, by determining membership in the Church. It is the
power to baptize, to administer communion, and to excor-
municate. God promises to honor the decisions of His ap-
pointed officers, when they act in accordance with His re-
vealed Word. Indeed, in the Old Testament, God com-
manded anyone who would not submit to the decisions of the
court to be put to death (Deuteronomy 17:12). This shows us
how seriously He takes His earthly courts. Human govern-
ment is never absolute, for there is a Last Judgment which
will right all wrongs, but human government under God is
still a very serious thing.

One of the special powers of the Church officers is to form
a court to judge in matters of dispute between Christians (1
Corinthians 6:1-8). As we have just seen, even if the court
renders what we think is the wrong decision, we are still to
submit, awaiting God's perfect judgment at the last day.

Church government is not magical, as if only certain
special persons are empowered to perform the rites of baptism
and of the administration of the Supper. Rather, the special
power is governmental; These things may only be done under
