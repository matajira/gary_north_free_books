THE MARRIAGE SUPPER OF THE LAMB 223

eternal death. Adam provided the first discontinuity; Jesus
Christ provides the second: from spiritual death to spiritual
life, meaning eternal life.

We need not assume that the symbolism of circumcision is
limited to the symbol of the tokens of virginity. Vos argued
that it referred to the inability of man, by his own flesh, to
provide true life to heirs, that physical descent from.Abraham
js not the basis of life.2? But Vos was speculating; he provided
no exegesis to prove his case, although this may be one possi-
ble additional meaning of circumcision.

Rushdoony’s observation should be carefully considered
in light of my analysis of the relationship between circurnci-
sion, the bridegroom, and the tokens of virginity. He writes
that “the Hebrew word for bridegroom means ‘the circum-
cised? the Hebrew word for father-in-law means he who per-
formed the operation of circumcision, and the Hebrew word for
mother-in-law is similar. This obviously had no reference to
the actual physical rite, since Hebrew males were circumcised
on the eighth day. What it meant was that the father-in-law
ensured the fact of spiritual circumetston, as did the mother-in-
law, by making sure of the covenantal status of the groom. It
was their duty to prevent a mixed marriage. A man could
marry their daughter, and become a bridegroom, only when
clearly a man under God.”*4 This is accurate as far as it goes,
but we can go even farther. The in-laws were admitting that
their daughter needed the blood covering. They were admit-
ting that Israelites, as the bride of God Himself, were in need
of legal tokens of virginity. The true Bridegroom is the truly ctr-
cumcised man~-a man whose own blood provides the covering
for His bride.

As Rushdoony says, the root of the Hebrew word for
bridegroom is related to circumcision. The link has not been
taken seriously by most commentators, but traditionalism
should not blind us to the truth. Rushdoony’s insight is in-
novative. We should apply it more consistently to the sym-
bolism of circumcision. The irue Bridegroom is Jesus Christ, the
truly circumcised Son. It was the shedding of His blood that finally

22. Geerhardus Vos, Biblical Theology: Old and New Testaments (Grand
Rapids, Michigan: Eerdmans, [1948] 1975), p. 90. James Jordan provides
an extended theological discussion of circumcision in its various meanings
in his Law of the Covenant, pp. 78ff.

23. Rushdoony, Institutes, p. 344.
