166 CHRISTIANITY AND CIVILIZATION
Papa: Not if the elders keep things exciting enough on the
stage.

Nathan: Elders? What elders? You mean those men up there
on the platform are elders?

Papa: Sort of. But they don’t always call them that.

Nathan: Why aren’t they wearing robes and collars so you
know what they are?

Papa: They say elders shouldn’t wear special clothes.
Nathan: Why noi?
Papa; They think that there’s nothing special about clothing.

Nathan: Policemen and soldiers and judges wear special
clothes.

Papa: Weil, they think clothing isn’t special for elders. They
think elders should look like everybody else.

Nathan; Then why is that elder wearing a maroon suit with a
blue shirt, a green tie, and a white belt?

Papa: Well, it’s still a suit. The point is, he can wear anything
he wants.

Nathan: You mean an elder could wear a robe and a collar if
he wanted?

Papa: No. He can wear anything bui a robe and a collar.
Nathan: So they do think clothing is special!

Papa: Well... .

Nathan; There! Someone did it again!

Papa: Did what?

Nathan: He said “Amen.” See? That’s why this place needs a
liturgy book, Half the people don’t know when to say things.

Papa; I told you. They don’t do a liturgy here.

Nathan: Some people do. Hear that? Somebody just did it
again. If we had a book, we could all say it together. That
would keep some people from getting it wrong and saying it
while somebody else is talking.
