REVIVALISM AND AMERICAN PROTESTANTISM 57

emphasized emotion and experience and the excesses of the
Awakening produced a countervailing rationalism in its op-
ponents. Taylor and Beecher, it is true, erred in their rational-
ism, but the Puritan balance between feelings and intellect
reason and faith had already been upset by the first revival.
The Great Awakening was the catalyst that broke down the
already unstable Puritan theological system into its constitu-
ent parts and instigated the independent development of ra-
tionalist and pietistic traditions. It later became clear that be-
neath this dialectical relationship between rationalism and
pietism was a basic agrecment. Both rationalistic and pietistic
Christianity are hybrids of Biblical religion and humanism.

In other areas as well, the first Awakening marked a wa-
tershed in the decline of Puritanism. The effect of Neoplatonic
thought on Edwards was marked. In a personal journal, en-
titled Images or Shadows of Divine Things, Edwards recorded
scores of observations on the typological meaning of natural
phenomena. He distinguished between the “carnal,” “more ex-
ternal and transitory” portion of the universe which was
typical of the “more spiritual, perfect and durable part.”3?
Similarly, in the Middle Colonies, Freylinghuysen and the
Tennents encouraged an “existential indifference to the things
of this world.”53 It is true that evangelical preaching and writ-
ing in the immediate prerevolutionary period informed
churchgoers of the issues confronting the colonies and that
some evangelical preachers actually fought in the war, but
Edwards and many of his followers downplayed the Puritan
emphasis on political and social involvement. One of the
results of the revival, as Niebuhr has intimated, was the

 

revivalism of Dwight and Beecher, his roots in post-Awakening New Eng-
land, and the rise of a distinctly rationalist theology only after the Awaken-
ing suggest that his thinking was not unaffected by the revival. Foster im-
plies by the very structure of his book that Hopkinsianism and ‘Taylorism
emerged from the same revivalistic roots, and Whitney Gross traces a line
from Edwards through Taylor to Finney, Cross, The Bumed-Over District: The
Social and Intellectual History of Enthustastic Religion in Western New York,
1800-1850 (Ithaca: Comell University Press, 1950), p. 27.

32. Edwards, Images or Shadows of Divine Things, od. Perry Miller (New
Haven: Yale University Press, 1948), p. 27.

33, Heimert and Miller, Documents, p. xxii,

34. See Nathan O. Hatch, The Sacred Cause of Liberty (New Haven: Yale,
1979).
