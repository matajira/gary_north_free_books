THE RECLAMATION OF BIBLICAL CHARITY 317

Tt is estimated that there are now nearly three million
homeless “new poor” in America today.5? They crowd into
tent cities, living out of the backs of their cars, under bridges,
or, at best, in abandoned sub-standard shelters. In Pittsburgh,
homeless men sleep in caves above the Allegheny River.5@ In
Los Angeles, homeless men and women go door-to-door in
suburban neighborhoods, peddling fruit.5? In our nation’s
capital, homeless women sleep on Pennsylvania Avenue in
front of the White House.® In Houston, the state director of
the AFL-CIO iells the jobless to stay out of Texas: “There are
no jobs here,” he says, “and there are no beds,”6!

Though far from being a “Grapes of Wrath” situation, the
crisis is still a formidable one. On the East Coast, Baltimore
has nearly 10,000 homeless,®? Philadelphia has 8,000,583 New
York City has 36,000, and the nation’s capital has nearly
8,000.°5 In the Midwest, where unemployment has been
especially devastating, there are reportedly 8,000 homeless in
Detroit,5 and another 10,000 in the Harmmond metropolitan
area.§? The West Coast has suffered with more than 2,500
homeless new poor in Seattle®® and over 20,000 in the Los
Angeles/Orange County region.®? Because of the mass exodus
of workers from the post-industrial Midwest and Northeast,
the Sun Belt has been especially hard hit. Small cities like
Abilene and Humble struggle under the burden of 2,000
homeless new poor,’? while the Dallas/Fort Worth metroplex
and San Antonio face catastraphic conditions with nearly

37. Compare Newsweek, January 2, 1984, with Getschow’s article, and
Mike Hardin’s five-day series in The Columbus Citizen Journal, November
23-27, 1982.

58. Mary Ellen Hombs and Mitch Snyder, Homelessness in America
(Washington, D.C.: CONV Press, 1982),

59. Ibid., p. 126.

60. Ibid., p. 125.

61. Houston Post, June 26, 1983.

62. USA Today, 1983.

63. Source: Community for Creative Non-Vialence.

64, Source: Community Service Socicty of New York.

65. Source: Community for Creative Non-Violence.

66. Source: Coalition on Temporary Shelters.

87, New York Magazine, February 21, 1983.

68. USA Today, 1983.

69, Wall Street Journal, November 12, 1982.

70. Source: Humble Evangelicals to Limit Poverty.
