92 CHRISTIANITY AND CIVILIZATION

God. So, death in the case of the pagan is only ceremonially
acted out to avoid the real death. In this kind of ceremony—
the kind that is designed to escape responsibility to God—
death becomes a game in hopes that the reality will disappear.
Yet, death is a reality for Jesus Christ really faced death, and
not just ceremonially.

Therefore, the Christian mind understands that it is judg-
ment and not just death that must be faced. Death is the
penalty and result of judgment, Further, one must face this
judgment with Jesus Christ or he will truly die. Nevertheless,
life comes through judgment according to the Christian posi-
tion which is altogether different from the non-Christian view.

Paul emphasizes the need to face judgment and he brings
out this theme by his reference to the Feast of Unleavened Bread.
He wrote the first letter to the Corinthians during the Feast (I
Cor. 16:8)¢ which began the first day after the Passover Sab-
bath. It started with a firstfruits offering that was followed by
seven days of refraining from the use of leaven (Lev. 23:4ff.).
The firstfruits offering was the first stalk of wheat that was cut
down by the reapers. This imagery is a symbol of judgment
and being torn away from old leaven. The “cutting down” of
the sheaf comes by the hand of the reapers, and prefigures all
judgments to come--the destruction of Israel, Christ, and
later on the world (Matt. 3:12)

The Feast of Unleavened Bread began with this symbol to
indicate that leaven is a death which results from judgment.
During the next seven days, leaven was avoided because it
represented the corrupting influence of sin. By beginning this
period of feasts with a time of abstaining from leaven,
judgment-tearing was implemented by reformation of life. In
Scripture, the repentant see judgment as coming from the lov-
ing hand of God, and turn from their wicked ways. The
reprobate, however, rush head long into more self-conscious,
fermented, sin.

Paul alludes to this Feast, and the need for repentance
from fermented sin in a similar context of judgment in I Cor-
inthians 5, Here, he addresses the need for Church discipline,

4. W. J. Conybeare and J. 8. Howson, The Life and Epistles of Saint Paul
(Grand Rapids: Wm. B. Eerdmans, 1968), p. 381. Conybeare says, “He
[Paul] wrote during the days of unleavened bread, i.e, at Raster (I Cor, 5:7: see
the note on that passage), and intended to remain at Ephesus till Pentecost
(16:8, of. 15:32).
