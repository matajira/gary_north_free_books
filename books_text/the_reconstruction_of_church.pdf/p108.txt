96 GHRISTIANITY AND CIVILIZATION

I, Surname Definition and Calling.

Paul begins with surname definition and calling. His
name was defined in terms of calling. In an age of nominal-
ism, names become nominal or fortuitous. This has not
always been the case in Western Civilization. There was a
time when a man's name reflected his calling. If he were a
blacksmith, his name was called Smith. If he were a parson,
his name was called Parson. Any dictionary will confirm that
these names are popular today, but have no connection with
vocation.

Most important, however, Christianity taught man that in
Christ his vocation would be a reflection of his covenant with
the same. When Paul was converted, his name was changed,
Christian tradition in the Church has applied the name-
change-principle. At the point of baptism, an adult or child
would receive his Christian, or first name. His new name be-
fore his function name indicated that Christ’s calling took
priority over natural function. And, his natural abilities
should come under the use of the Christ.

So, outside of Christ, calling is only functionally deter-
mined. This ultimately leads to totalitarianism. Scripture says
that man’s function in this world is not accidental, instead, a
man’s calling is ordained by God.

The fact that a man was defined by his calling points to
definition which is beyond himself. In the Middle Ages, a
man was destined by birth to a certain occupation.’ Christi-
anity interpreted this as practical predestination. A man’s
definition was outside of himself, in other words, and thus
definition or calling preceded essence.6 Adam was first

5. Clothing and calling are directly related to one another. A man also
pointed out his calling by his clothing. The “unisex/uniclothing” movement
is an expression of totalitarian philosophy and ultimately an attack on the
Doctrine of Creation—the doctrine on which calling is established. Else-
where in this symposium I develop this concept in greater detail in an arti-
cle, “Calling and Clothing.’

6, Van Til has said this in so many words by his emphasis on the
Creator/creature distinction. Since God was before the created world, and
the created world therefore did not eternally exist, man receives definition
from God. He does not look to himself. James Jordan has made this obser
vation in his taped series on Christian World View which can be purchased
from Geneva Ministries, 708 Hamvassy, Tyler, Texas 75701,
