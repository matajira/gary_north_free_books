THE MARRIAGE SUPPER OF THE LAMB 219

the name of God. There had to be the covenantal mark on
Moses’ son to identify him as one of the Israelites. The land of
Egypt was under a curse because of the blood of the drowned
male children eighty years earlier. God was about to unleash.
the angelic avenger of blood against the firstborn of all those
dwelling in Egypt. This angelic destroyer was waiting for him
when he re-entered Egypt. There had to be a blood covering
in Moses’ family. His son had to be circumcised. This was
probably his firstborn son, although the Bible does not say so
explicitly.11 Hebrew males were bloodied on the eighth day
(Lev. 12:3). The females were not marked in any way,
although they are circumcised in some pagan cultures. !? The
question arises, why were the males singled out? Christians
baptize females. Females are to bear the invisible mark of the
Christian covenant. Yet they bore no covenantal mark in the
Old Testament era. Why not? Why did the covenantal sign
not apply to daughters?

From what has already been said about the tokens of vir-
ginity, the answer should be clear. The bridegroom had to
provide the blood for a faithless bride, if the bride was to sur-
vive. In marriage, the blood comes from the woman’s body.
Theologicaily speaking, the blood cannot come from the new
bride’s body, for she has been unfaithful, a harlot. Rahab is
the archetype. So the blood must come from the male. There
had to be blood to serve as a legal token of a consummated marriage be-
tween faithful couples. Israel circumcised all males as a sign. It
was, in part, an admission on the part of Israel, the bride, of
their need for the tokens of virginity. The bridegroom alone
could legally provide the tokens. The circumsision of the males of
Israel pointed to the blood that would be shed by the Messiah, the Bride-
groom who calls His bride into His chamber. Without the bloody
tokens of virginity, Israel, as an ethically faithless bride,
would perish. Circumcision was only for males, for only their
shed blood could testify to the bridegroom's provision of the

41. “At the time of the incident Zipporah evidently bad only this one son,
He was thus her firstborn son.” Hans Kosmala, “The ‘Bloody Husband’,”
Vetus Testamenium, X11 (1962), p. 20.

12. “Circumcision (Introductory),” in James Hastings (ed.), Encyclopedia
of Religion and Ethics, (2nd ed.; New York: Charles Scribner's Sons, 1932),
Vol. ILI, pp. 659, 667-68, Circumcision of females is sometimes called “in-
trocision.”
