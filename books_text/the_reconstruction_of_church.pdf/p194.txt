182 CHRISTIANITY AND CIVILIZATION

aeval Catholicism of P, it must be admitted that his basi
value-judgments are shared by very many who would ne
subscribe to his critical position. This no doubt partially ex
plains the continuing attraction of his views, however doubt
ful some of the intellectual arguments in their favour ma
be."*

The suppositions that primitive is pure, and that life stari
simple and complexifies, are evolutionary. Evolution says the
chaos generates life. Therefore, the random state of matte
raust be achieved before new life appears.

In philosophy, we call this irrationalism. In psychology
the irrational is expressed in the emotional. Therefore, Wel:
hausen leads us to believe that the early church did not hav
any set forms. They depended on the Spirit, which is contrar
to form, and had a purer expression of the whole man. H
was able to pour forth an emotional response to God becaus
there was no ritual to suppress him.

Records of early Christian practices do not confirm Wel
hausen’s presuppositions. Pliny the Younger wrote t
Emperor Trajan, “It was their habit on a fixed day to assemb!
before daylight and recite by turns a form of words to Chri:
as a god: and... they bound themselves with an oath, nc
for any crime, but not to commit theft or robbery or adultery
not to break their word, and not to deny a deposit whe
demanded. After this was done their custom was to depari
and to meet again to take food, but ordinary and harmles
food; and even this (they said) they had given up after m
edict, by which accordance with your commands I had forbic
den the existence of chubs.”#

It is obvious that the early Christians were binding ther
selves in some kind of covenant ceremony. They used the Te
Commandments. Not only does this tell us something abou
their view of the Law of God and its application for Chris
tians, it points out a ritual in the early church.

Hippolytus, a Roman Bishop, wrote in a.p. 210 that h
wanted the bishops to return to the form of worship that wa
observed in Apostolic times. He was bothered by new innovz

4. Gordon J. Wenham, Numbers (Downers Grove, Minois: InterVarsit
Press, 1981), p. 27.

5. Pliny, Epistles x. 96, tr. J. Stevenson, A New Husebius (Londor
S.P.C.K., 1968), p. 14.
