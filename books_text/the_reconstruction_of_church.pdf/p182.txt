170 CHRISTIANITY AND CIVILIZATION

Papa: They don’t believe in it. They think it’s wrong.
Nathan; But it tastes good.
Papa: Well, tasting good isn’t everything.

Nathan: But God made it for us to drink, especially at Com-
munion. It makes us happy, and it makes God happy too.

Papa: That's right.
Nathan: Does the Bible say it’s wrong?
Papa: Na.

Nathan: Then why do they say it is? And why do they drink
this yucky juice? And eat those crummy little cracker pieces?
No wonder they're so sad!

Papa: What?

Nathan: Well, look at them. Look how sad they all are. They
don’t look like they’re enjoying this, do they?

Papa: Well, no... .

Nathan: Well, they aren’t enjoying it a bit. But didn’t you tell
me that Communion is a special dinner with Jesus?

Papa: Yes.

Nathan: And when we come to Communion, the whole
Church is coming up to heaven, right?

Papa: Right.

Nathan: And when we go to heaven to be with Jesus and have
dinner with Him, we’re supposed to be happy, aren't we?

Papa: Sure.

Nathan: Well, why aren’t these people happy? Do they think
heaven is a sad place to be?

Papa: I think they're sad because they’re thinking about their

sins.

Nathan; But they've been forgiven, and now they're in
heaven! They’re supposed to be thinking about Jesus!

Papa: Oh, they're thinking of Him, too. They’re sad because
they're thinking about Him dying on the cross.
