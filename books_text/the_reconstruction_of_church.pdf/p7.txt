INTRODUCTION ix

writing, “Knowledge is basic to worship.”

Making application, Rushdoony states, “Thus, we are
plainly required to have Christian schools to teach every cove-
nant child the word of the Lard and to study every area of life
and thought in terms of Christian presuppositions. It is also
our duty to ‘teach all nations’ (Mart. 28:19), and all the inhabit-
ants thereof. The Great Commission is a commission i teach
and to baptize: it has reference to education as well as to wor-
ship, to the establishment of schools as well as churches.
‘Teaching is cited before baptizing. It is teaching which alone can
create a godly civil government and a faithful church.” Thus,
Rushdoony summarizes by writing, “The primacy of teaching
before church worship and national discipleship are asserted
by Scripture. The great missionary requirement of the days
ahead is Christian schools and institutions.”

Rushdoony makes the same assertion on pages 130-131:
“Thus, while wership has a very high place in God's plan,
priority belongs to instruction. The school is more essential to
Christian society than the church, although both are neces-
sary institutions.”

There are a number of observations to be made on this.
First, Rushdoony is right to call attention.to the importance of
teaching, but he errs in making it more important than wor-
ship. In fact, the reality is quite the reverse. Adam was not to
eat of the Tree of Knowledge until after he had eaten of the
Tree of Life.! An affirmation of God’s supremacy, an act of
worship, is the precondition of all true learning, “The fear of
the Lord is the first part of wisdom” (Ps. 111:10; Prov. 1:7;
9:10).

Second, given the prominence of the whole sacrificial wor-
ship system instituted at Mount Sinai, it is hardly credible to
maintain that it is not equal in importance to teaching as a
“necessary concomitant to a godly law order.”

Third, I do not see how the fact that many of the Levites
were scattered in Israel away from the sanctuary “makes clear
the importance of their teaching function over sanctuary du-
ties.” T could assert the opposite: The fact that there was only
one central sanctuary prover that it was more important than

1. See my essay, “Rebellion, Tyranny, and Dominion in the Book of
Genesis,” in Gary North, ed., Tactiss of Christian Resistance. Christianity and
Civilization No. 3 (Tyler, TX: Geneva Ministries, 1983).
