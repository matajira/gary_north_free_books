THE MARRIAGE SUPPER OF THE LAMB 215

pitied thee, to do any of these unto thee, to have compassion
upon thee; but thou wast cast out in the open field, to the lothing
of thy person, in the day that thou was born. And when I passed
by thee, and saw thee polluted in thine own blood, I said unto
thee when thou wast in thy blood, Live; yea, I said unto thee
when thou wast in thy blood, Live. I have caused thee to multiply
as the bud of the field, and thou hast increased and waxen great,
and thou art come to excellent ornaments: thy breasts are fash-
foned, and thine hair is grown, whereas thou wast naked and
bare. Now when I passed by thee, and iooked upon thee, behold,
thy time was the time of love; and I spread my skirt over thee,
and covered thy nakedness: yea, I sware unto thee, and entered
into a covenant with thee, saith the Lord Goo, and thou be-
camest mine. Then washed I thee with water; yea, I throughly
washed away thy blood from thee, and I anointed thee with oil. I
clothed thee also with broidered work, and shod thee with
badgers’ skin, and I girded thee about with fine linen, and I cov-
ered thee with silk. I decked thee also with ornaments, and I put
bracelets upon thy hands, and a chain on thy neck. And I] put a
jewel on thy forehead, and earrings in thine ears, and a beautiful
crown upon thine head. Thus wast thou decked with gold and sil-
ver; and thy raiment was of fine linen, and silk, and broidered
work; thou didst eat fine four, and honey, and oil: and thou wast
exceeding beautiful, and thou didst prosper into a kingdom. And
thy renown went forth among the heathen for thy beauty: for it
was perfect through my comeliness, which I had put upon thee,
saith the Lord Gop.

The sequel, as Ezekiel was sent to proclaim, was Israel's
subsequent faithlessness, a whoring after other gods (Ezk.
16:15-59). Nevertheless, God will remember His covenant
with Israel (Ezk, 16:60-63). There will be restoration; God
will bring the harlot back into His house (Hosea’s whole mes-
sage). But the initial wedding ceremony, as described by
Ezekiel, fits the symbolism of the Passover night better than it
fits the giving of the law at Sinai.6 This night established
Israel as His bride —a bride dressed in the finest clothing, for
all the rebellious world to see. In this case, the jewels were
supplied by the Egyptians.

Inside the blood-smeared hovels in Egypt, there was safety.

6. The phrase, “[ sware unto thee, and entered into a covenant with
thee,” refers to the original covenant between God and Abraham. Sinai only
re-confirmed the terms of the original covenant, as did the Passover. Paul
stresses the crucial importance of the Abrahamic covenant as over the cove-
nant at Sinai: Gal, 3:16-18,
