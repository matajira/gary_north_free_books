230 CHRISTIANITY AND CIVILIZATION

rainbow glory-fabric in heaven points to function and status.
Leaders in particular are surrounded by brilliant colors.

Parenthetically, even if the multi-colored clothing of the
rainbow does not establish a basic principle, the very least one
could observe is that created beings in the glory cloud have
different clothing and function. Angels are adorned one way,
the saints have white robes (Rev. 7), and Elders also have a
certain dress (Rev. 4:4).

Nevertheless, the rainbow clothing of officers in heaven
provides a point of contact between heavenly and earthly
principles. God surrounds the officers of heaven with a rain-
bow,? and reproduces it in the coat of Joseph, the patriarch
(Gen. 37:23). Let us consider the drama of the Book of
Genesis.

Originally, man’s flesh was his glory (I Cor. 15:38ff.), and
by obeying God, the glory of his flesh could be improved. I
agree with James Jordan’s thesis that the Tree of the Knowl-
edge of Good and Evil represented judicial rule.? References to

 

color, Interestingly, this is the color of the robe, and particularly the color of
the robe Christ wore (more reddish from the blood emphasis). Christ was
cast to Hell in His suffering on the Gross, yet He sits at the right hand of
God, next to the light, This deep purple color could indicate the color of ice
cold frozenness. Hell is not only portrayed as hot, but also extreme cold.
Notice the number of times that the Devil and demons come from the far
north (Jer. 1:14; 4:6; Is. 14:13; Job 26:6-7), The cold represents that
which never changes. Thus, on the other end of the color chart, purple is
next to the white because God never changes.

In the middle of the color chart are the colors of green and yellow, These
are the colors of the earth in the Bible. The earth is pictured by the color
chart as being between heaven and hell,

‘On the lower end of the color chart, toward hell, and next to the purple
color, are the colors of red and orange. These are the colors of judgment.
Man passes through judgment before he gocs to hell even though hell is a
form of judgment.

‘Thus, color reinforces the theology of the Bible, and as one might
suspect, joins with all of creation in pointing toward the God of Scripture.

2. One could conjecture that this “human” rainbow explains the various
colors in humanity, Humanity was created to surround the throne of God as
part of His Glory. The sea of humanity with all its many colors represents
this.

3, James B. Jordan, “Rebellion, Tyranny, and Dominion in the Book of
Genesis,” Gary North, ed., Tactics of Christan Resistance. Christianity & Civi-
ligation No. 3 (Tyler, TX: Geneva Ministries, 1983), pp. 38-80.

 
