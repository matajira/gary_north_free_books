THE CHURCH: AN OVERVIEW 17

endeavor to help the poor during the rest of the year. There
are also other kinds of diaconal services you might be asked to
help with, such as taking a turn in a picket line at a murder
store (abortion chamber), or writing a letter to government
officials protesting new laws which tax the Church, and the
like. We don’t have time to do everything, but we should
endeavor to help with the diaconal ministries of the Church,
for there is great reward in it, as Matthew 25:31-46 makes
clear.

Worship

As important as fellowship and service are, they are in the
general area of the Church's life. The special aspect of the
Church’s ministry is worship. Mainstream evangelicalism is
particularly weak in the area of worship, though this is begin-
ning to change. It is the job of the elders to appoint the times
and the format of formal worship.

People in our culture tend to view Church services as
something which they “attend.” They may sing a few hymns,
but for the rest they sit quietly while the pastor does all the
talking and all the praying. They don’t like it when new
hymns are picked, because they have to work at getting the
tune right. Worship is a time to sit passively and drink it in,
they think. This tendency in worship is called “quietism.”

The Bible is not quietistic in its view of worship, and in its
days of greatest strength, the Church has not been either. The
Bible commands us to praise God with musical instrurnents
and with the dance (Psalm 150). A good deal of effort is
necded to learn how to do this, and more effort is needed actu-
ally to do it.

We can call this “command performance worship.”
Whether the worship service is sparse and plain or rich and
ornate, the purpose of worship is not the entertainment of
man, but the entertainment of God. God is the Audience; we
are the performers in worship. We direct prayer and praise to
Him. We listen carefilly when He speaks to us through His
Word, as explained by the preacher. Too often, however, a
self-centered attitude is found in the man in the pew. He
comes to Church to get something, rather than to give of him-
self to God. With that attitude, he always goes away
unsatisfied. This is because man’s highest privilege and
