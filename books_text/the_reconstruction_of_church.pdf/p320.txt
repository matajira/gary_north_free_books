308 CHRISTIANITY AND CIVILIZATION

Thus comprehended, the Lord’s table, where we reap His
bounteous grace provisions, becomes a continual provocation
to missions. Thus comprehended, the Lord’s table becomes
an ever-present reminder of our earthly task.

Fourth, the Sunday School also must be utilized as a
dynamic prod for missions once again. Instead of being a di-
lapidated vehicle for watered-down moralisms, the Sunday
School could serve as an intensive training camp for dedicated
Kingdom activists. Rescued from banality, Sunday School
could be the platform from which strategies are plotted, tac-
tics are launched, and reclamation is begun.

Why sot start a weekly elective Sunday School class or
training union series to explore what Scripture has to say
about poverty and the appropriate Christian response to it?3?
If we start small and take our learners along in smooth, care-
fully plotted stages, it won't be too terribly long before we
have a whole slew of Christians chomping at the bit, raring to
jump headlong into the battle against hunger, homelessness,
and the welfare trap.

Fifth, special events and meetings must be held periodically
to stir the passion for, and instill the vision af, Biblical charity.
Since most churches already schedule special revivals or Bible
conferences or prophecy seminars or missions conferences
each year, why not devote some of the time to the problem of
poverty and its Scriptural solutions? Why not invite a speaker
or two who have actually begun the work of the Good Samari-
tan faith to detail the ins and outs, the ups and downs of their
ministries?

During the heyday of foreign missions, just before the turn
of the century, missionaries visited in our churches on a very
regular basis, sharing their experiences and inspiring many to
follow in their footsteps. Why not renew that old and venera-
ble tradition? But, this time, why not mix in a few “home mis-
sionaries” who are working with the poor as well as those
called by God to foreign fields? The distressing trend away

 

Papers and The Gentya Review, ave especially helpful in understanding the
proper place of liturgy and the sacraments in the church and for the for-
mulation of its ministries.

33. See the study guide for Bringing In the Sheaues, which has been de-
signed to translate the concepts of Biblical charity into a Bible study or Sun-
day School format. Entided Blueprint for Charity, it is available from
American Vision in Atlanta.
