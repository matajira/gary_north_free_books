THE LITURGICAL NATURE OF MAN 195

The central significance of the knee is clarified in a con-
crete reference. When Abraham causes his camel to kneel
with him (Gen. 24:11), both are made to rest. Thus, kneeling is
a gesture of resting.

At the beginning of time, God established the Sabbath day
on which He rested (Gen. 2:1f.). In the natural sense, rest is
sleep. In the redemptive sense, rest is faith and all that is asso-
ciated with it. When man rests in the Lord he acknowledges
complete dependence (Heb. 4:10). He indicates that his life
takes its orientation around the God of Scripture. Therefore,
bending the knee is a statement of faith.

This explains why bending the knee is a statement of faith
regardless of the object before whom the subject bows. Also, we
understand why the Hebrew noun for “knee” can also be trans-
lated “bless.” One who is resting in God is given His rest. This
is the kind of blessing that grows out of Biblical genujlection.

What is genuflection? From the Greek word for “knee,”
genu, we derive the word genuflection. Protestants have come
to fear this word because of the gestures associated with it. Ac-
tually, the Biblical concept of genuflection is simply resting in
the Lord through worship, But in the history of the church it
became perverted. How?

When worship ceases to be an act and shifts to being an
aid, genuflection becomes magical. Man gravitates toward
talismans of all sorts to stimulate himself. And this is where
we run into the point of contact between protestantism and
Rome.

The average evangelical goes to worship looking for an aid
to his life. As one man told me, “I go to worship to be made to
feel good.” This view is essentially no different from the
Roman Catholic concept of aids to worship. From the preach-
ing to singing, he wants to be stimulated. The modern evan-
gelical needs “aids,” and if he is not stimulated by various
aids, worship will be rejected. The irony about this view is
that the worship service designed to be an act—a liturgical
service — often is perceived to be unspiritual. The service with
the most stimulation, however, is believed to be the real thing.

Worship is active not passive, indicative not subjunctive,
and man should come to God resting instead of manipulating.
If he comes in faith, he will need no props. Worship ceases to
be a stage. The Biblical Christian realizes that he should not
live for stimulation, but for service to the Lord of heaven and
