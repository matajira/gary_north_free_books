INTRODUCTION

James B. Jordan

HE first volume in this annual series of symposia attempted

to analyze the fandamenta] problem of individualism as
it infects the Christian culture of one of the most apparently
Christian nations in the world today, the United States of
America. In our next two volumes, we probed the current
problem of statist attacks on the faith and on the Church,
which attacks are a direct result of the secular individualism of
American culture. In this fourth volume, we probe more
deeply into the religious problems which infect Western Cul-
tare as a whole and American Culture in particular. We in-
tend to continue this probing in the fifth volume (1986), which
will be on the topic of “Piety and Pietism.”

To say that the root of our problems is religious is to say a
great deal, but also to say rather little. Compared with the
heredity and environmental reductionisms popular in medern
(and in ancient) thought, a confession that human nature and
its problems are fundamentally religious is quite radical and
immeasurably important. If, however, this confession only
amounts to the notion that religious ideas underlie any given
culture, then the affirmation is far less radical. For to discuss
religion only in terms of ideas or doctrine is to reduce religion
to an ideology. For the Christian there is an equal ultimacy of
thought and practice, of saying and doing, of lip and life, of
preaching and sacramental practice. As a result, any discus-
sion of the restoration of Christian civilization may not simply
consist of how Christian doctrine differs from its challenging
counterfeits and antithetical adversaries in the areas of theol-
ogy and understanding— he it dogmatics, economics, politics,
or aesthetics; it must also include an examination of practice.

The practice of the Christian faith is most concentrated in
the activity of the Church. This is for the obvious reason that
it is in the Church that men devote themselves most rigorously

vil
