64 CHRISTIANITY AND CIVILIZATION

vistic assumptions which revivalism encouraged. Moreover,
they were historically linked with revivalism because revival-
ism was the major theological and ecclesiastical issue among
all Protestant churches. Nearly all theological innovations of
this period arose in support of or opposition to revivalism.

The revivalism of the 1820’s and 1830's maintained the es-
chatological optimism of the Great Awakening. In fact, the
scope of their vision broadened: The objective of the revival
was less to save individual sinners than to save the world.®
Finney believed that through the gradual spread of the Gos-
pel, the world would eventually reach a state of perfection.
Before Christ returned to establish His kingdom fully, there
would be an age of universal peace and prosperity. Bushnell
likewise awaited a future age of great prosperity.7° Increas-
ingly, the pietistic strain of the revivals expressed itself in a
heaven- and death-oriented eschatology, and William Miller’s
chiliasm gained some following.7! But the predominant escha-
tology before the Civil War was postmillennialism. As one
church jeader put it in a letter to Finney, “I want to see our
State evangelized.* He hoped that the state of New York “in its
physical, political, moral, commercial and pecuniary resour-
ces [w]ould come over to the Lord’s side.”72

The idea that the Gospel, and hence the preacher, could
and should speak to all of life, a leftover of the theocentric
world view of Puritanism, was held by the revivalists. Finney
wrote that a minister’s education should be “exclusively theo~
logical” in the sense that all disciplines should be studied “in
connection with theology.””3 Albert Barnes (1798-1870), a
Presbyterian, claimed that “Every subject, whether of busi-
ness or of morals, comes fairly within the province of the pul-
pit.”"* More and more, however, the churches advocating a
broad application of religious principles were those that had
strayed from orthodoxy. On their humanistic presuppositions

68. Cole, p. 77. Douglas writes that antebellum Christianity tended to be
cither pre- or postmillennial. Douglas, p. 221.

69. McLoughlin, Modern Revivalism, p. 105,

70. Cole, p. 232.

71, See Douglas, ch. 6; Cross, ch. 17. Miller predicted that Christ would
return to earth around 1843.

72. Quoted in Cole, p. 14.

78. Finney, p. 218.

74, Quoted in Cole, p. 237.
