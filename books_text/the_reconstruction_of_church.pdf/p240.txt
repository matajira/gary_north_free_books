228 CHRISTIANITY AND CIVILIZATION

only pastor wearing a clerical collar. The governor's aide
walked in rather disinterestedly. I could tell he did not want to
talk to these men, but I could also see that he did not appear
too concerned. And then, his eyes fell on me. Rather, his eyes
fell on my collar. He was almost shocked. His eyes raced up and
down from my collar to face, and face to collar. It seemed as
though this happened a thousand times in that brief second
when he first saw me.

The spokesman for the group was stating our purpose for
being there while the aide tried to gather his composure.
When the spokesman finished, to my surprise the aide recog-
nized me first as though he were thinking, “I know who just
spoke, but this guy with the collar on is the most important
leader.” I spoke my piece about why the fathers should be re-
leased, then other pastors spoke up. Each time, however, the
governor’s aide would look back at me to see if I approved. [
realized then that my collar was worth a thousand words,
Whether my Baptist friends would admit it or not, even
though I was not the leader of the group, J seemed to have become
the real power, at least in the eyes of the governor's aide. As we
walked out of the capitol, | thought, “What would have hap-
pened if all two hundred pastors had showed up wearing col-
lars? Would the governor still have refused to see us?”

Tam certain that the governor's aide only had a pragmatic
concern. He perceived that this issue was reaching into larger
denominations which have much more clout. He saw me
(wrongly in reality) as a “big gun” compared to the other men,
who dressed in business suits.

But even that proves a point. The collar represents some-
thing to the unbeliever. Whether evangelical protestants want
to recognize the point or not, the collar represents an office of
authority. But isn’t that the way it should be? Shouldn't the
person of the elder be sublimated? Shouldn’t people submit to
the ofice and not just the man? The answer is yes to all those
questions,

For too long, protestants have denied the undeniable. As
CG. 8. Lewis has said, “The modern habit of doing ceremonial
things unceremoniously is no proof of humility; rather it proves
the offender's inability to forget himself in the rite, and his read-
iness to spoil for everyone else the proper pleasure of ritual.”
The purpose of the collar is to cover the man and accent the
office or calling. Even unbelievers generally recognize this.
