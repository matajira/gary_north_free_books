QULTURE, GCONTEXTUALIZATION, AND THE KINGDOM 331

In the next section we attempt to expand on the practical
struggle of the Christian in seeking to confront pagan cultures
with the claims of the Kingdom of the Lord Jesus Christ. It is
not structured in the way intended by T.E.F. when it first put
forth this idea of contextualization, nor are some of the terms
used in the same way.

Contextualization as Kingdom Life-Style

God is bringing about His Kingdom, but not in the way
that those of the T.E.F. think. God’s Kingdom is not coming
by social action and a growing humanization of the nations of
the earth. It is coming as His people are totally governed by
His authority; and by this authority they are instructed to
promote justice, righteousness, mercy, and love on the earth.
it is at this point that the church is the “salt of the earth,”

In some ways this idea of “kingdom life-style” is modeled
after the idea of “possessio” as suggested by J. H. Bavinck in
his Introduction io the Science of Missions.'4 It is the Christian
who, in his life-style as a “servant of the King,” possesses the
culture. The Christian in the covenant community is the con-
textualizer. “The program for such a contextualized ministry
demands the development of a Christian mind (2 Cor. 10:3-5).
And that requires that we listen carefully to both Scripture
and culture, without either acculturating the Bible through
allegorizing it into models of Biblicizing our culture through
accommodation. It is not content with transforming political,
economic, social, and cultural spheres of life until those
spheres and their presuppositional framework has been sub-
jected to the judgment of the Word of God.”!4 This is the
essence of what I call “kingdom life-style contextualization.”

Some Christians emphasize the communication aspeci of
contextualization and others the aspect of indigenization. The
latter is that of making the church indigenous, whereas king-
dom life-style contextualization is the task of making the
whole counsel of God indigenous, and not only indigenous
but normative—a truly high goal, but one for which Jesus
taught us to pray, *... Your kingdom come, Your will be

13. (Phillipsburg, NJ: Presbyterian and Reformed, 1977), pp. 179-190.
14, Harvie Conn, Theological Reflections on Contertualizing Christianity: How
For Do We Go? (Unpublished paper, n.d.), p. 6.
