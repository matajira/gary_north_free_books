296 CHRISTIANITY AND CIVILIZATION

was the meal itself. At the other end of the ambo were silver
seats for the Bishop and other ruling officers of the church.
Between the altar and the “bema” (judgment) seat of the
Bishop was the pulpit from which the law, the prophets, and
the gospels were read every Lord’s day. And yes, contrary to
what protestants often believe, the word was preached in the
vernacular of the people.*”

How do we understand this ambo around which the nave
was ordered? Just as one might understand the axes of the
Gothic cruciform. The ambo was a figure of all the mountains
in the scripture where God ever manifested Himself to man,
and in particular a symbol of the mount of transfiguration,
where man is brought before the presence of Gad. It was the
ambo around which the congregation gathered to be fed by
word and rite in a liturgical dance before the throne of God.

It was all down front for 916 years after Hagia Sophia was
dedicated. The morning services went on uninterrupted ex-
cept for an occasional earthquake or two, which only shook
things up for a time. The church hardly missed a liturgical
heart beat through good times and bad. It was a sign of stabil-
ity, and hardly a soul seemed to take notice of the changes tak-
ing place down front. So for almost a millenium the ecclesias-
tical center of Byzantine culture performed morning mass,
distributing rite and word as Gad!s food for man to the accom-
paniment of the choir chanting the cadence of dawn into the
nave with words like: “The groom comes forth to his bride as
the sun goes forth from its chamber.” “The morning star has
risen in the east.” “Come Lord Jesus.”

And He came, as a wind driving the Islamic Turks before
Him, for the speck of the icon in the eye blinded the whole
man. But it was not the blue and gold stones shimmering on
the walls that caused it, Protestant churches don’t have such

37. The location of the ambo and the liturgical positions themselves seem
unclear to most scholars. Some sources speak of as many as four ambos,
while others speak of only one. The location of the ambo is most often
deseribed as generally in the eastern section of he nave, and some sources
speak of the altar’s being behind an iconostasis. It seems clear that
significant changes took place within the liturgy itself during the 916 years of
services; changes that expressed an increasingly magical view of reality.
What I have done here is 0 describe an ambo with liturgical positions that
were common in both eastern and western branchs of Christianity at ahout
the time Hagia Sophia was built.
