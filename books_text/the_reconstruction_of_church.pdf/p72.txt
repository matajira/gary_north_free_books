60 CHRISTIANITY AND CIVILIZATION

platonic overtones, many evangelical preachers were deeply
involved in the issues of the day. The democratic impulse
affected church polity, and made inroads into political and so-
cial theory, but full egalitarianism was avoided. Absolute ethi-
cal standards were retained, though often spoken of in terms
of natural, rather than divine law. The direct results of the
Awakening were not as significant as the fact that it had cre-
ated a climate ripe for error. Certainly, Puritanism, tottering
on the edge of a precipice, had been given an impolite shove,
while the revivalists who hastened its decline paraded them-
selves as its saviors. In general, however, the first revivals re-
tained much of the older faith. By the time of Finney, senti-
mental Calvinists must have viewed the earlier revivals with
more than a hint of nostalgia.

Finney and Antebellura Revivalism

“Religion,” Charles Finney (1792-1875) emphatically de-
clared in 1835, “is the work of man. It is something for man to
do. it consists in obeying God, It is man’s duty.”48 A revival “is
not a miracle, or dependent on a miracle, in any sense. It is a
purely philosophical result of the right use of the constituted
means.”45 In fairness to Finney, it must be added that he was
not always consistent with his central thesis. “God induces”
then to obey and conversion is a complex event that involves
the confluence of four forces: the minister, God, the truth, and
the convert himself.5° Moreover, Finney’s comments must be

48, Charles Grandison Finney, Lectures on Revivals of Religion, ed. William
G. McLoughlin (Cambridge: Belknap Press of the Harvard University
Press, [1835] 1960}, p. 1. McLoughlin’s introduction to Finney’s Lectures is a
superb summary of the revivalist’s thought. Other major works covering this
phase of revivalism are William McLoughlin, Modern Revivalism; Weisberger,
They Gathered at the River; and Cross, The Burned-Over District. Weisberger’s work
deals more extensively than the others with the revivals on the Kentucky fron-
tier. Cross’s work is important because it gives a detailed picture of the effects
of revivalism in a limited area. Charles C. Cole, The Sactal Ideas of the Northern
Evangelists, 1826-1860 (New York: Columbia University Press, 1954) is useful
for its portrait of the democratic social ideas that inevitably accompany
revivalism, An excellent account of the revivals of the 1840's and 1830's is
Timothy L, Smith, Revivalism and Social Reform: American Protestantism on the Eve
of the Civil Wor (New York: Harper and Row, [1957] 1965).

49. Finney, p. 13.

_ 30. Hbid., pp. 1, 195.
