34 CHRISTIANITY AND CIVILIZATION

church is fifteen years old, and has been plagued with internal
strife and splitting for years. The membership has been re-
duced to twenty. The church is located in what has become a
commercial district. Not too many blocks away is an older
residential area. It is to the residential area that the pastor
goes, hoping to attract some families to the church. He rings a
bell and an older lady answers the door.

“Hello. My name is Pastor Eager and I’m here to invite
you to our church just four convenient blocks away.” He
smiles a fresh smile. .

“My, how nice,” she replies, “but we attend First
Anesthetized and we're really happy where we are. How
many members did you say you have?” She is being conversa-
tional now.

“We have twenty, Ma’am,” says Pastor Rager, feeling just a
litde ashamed at the small number. But the lady is delighted.
A baby church. Women like babies.

“And where is your church. again?”

“It’s at the corner of Blank and Dash.” Now he is a little
proud. The church has some property. “We're that nice white
church with the six acres,” he beams.

“Amazing!” She cries. “You have gotten so much property
in such a short time!”

“Oh, no Ma’am. We've been there fifteen years.”

“I see.” And she does, she really does. Red lights flash
everywhere, The facts scream out, and there’s no perfuming
over the smell of death with new programs, revivals, and
campaigns. Nobody wants to associate with a loser, and there sim~-
ply is no excuse for a fifteen-year-old church having only
twenty members. No excuse at all.

Other people in the community are more aware. Word
gets around in fifteen years. So when it comes to friendship
evangelism, the folks in the church have exhausted that route
years ago.

Countless other troubled churches sit and wait across the
country. They wait for another pastor, another program,
another chance. The hope of renewal glimmers in the breasts
of the few who really care, and these continue to take tired
steps toward improving the situation, The others are about
business-as-usual. They are waiting for the rest to give it up
or become resigned, as they have become, to their interminable
private Bible studies every Sabbath morn at 11:00 a.M.
