THE MARRIAGE SUPPER OF THE LAMB 217

be brought against Rahab the harlot, for she had made a cov-
enant with the spies, as God’s representatives, and she placed
the sign of this covenant publicly in her window: a scarlet
cord. This was the mark of blood on the opening of her house.
The spies escaped through her window, which was part of the
defensive wall of Jericho—a symbol of the protection they
received in her house. The men of Jericho trusted in the
bloodless walls and bloodless gates of their city, and the walls
came tumbling down. But not the entire wall. One portion
survived intact. The house of the harlor, which bore the
tokens of ethical virginity, meaning the scarlet cord, was not
destroyed. The thin thread of the covenant supported her
wall, and it stood, while the rest of the walls collapsed. Inside
that house there was life, this time for the family of the former
harlot. It was the place of her new birth. She, too, marched
out into freedom, for she possessed the tokens of virginity.
Those who had no bloody tokens of virginity had their own
blood shed that day (Josh. 6).

So the symbolism of the bloody doorposts also testified to
an ethical discontinuity: from unrighteous maidenhood to right-
eous womanhood. It symbolized the protection of the for-
merly faithless bride by her bridegroom. The slain lamb pro-
vided the tokens of Israel’s ethical virginity. Those inside the
blood-marked homes received new life that night.3

Did Israel know that this was part of the meaning of Pass-
over? I believe that they did, because they knew of something
that happened to Moses on the way to Egypt, which James
Jordan has called a “proleptic Passover.”* It is that to which we
how turn our attention.

8. The blood in question was not symbolic menstrual bload. No mar-
riage could be legally consummated during the woman’s menstrual cycle. It
was illegal in Israel to have sexual relations during this time of the month
(Ley, 20:18). I arn not arguing here that the symbolism of the Passover had
anything to do with menstrual blood. Quite the contrary: it is we, as fallen
sinners, who come before God as people dressed in symbolic menstrual
sags. ‘Che familiar phrase in Isaiah 64:6, “all our righteousnesses are as filthy
rags,” is a softened translation, ‘I'he word translated as “filthy” literally
means “menstrual,” Shame is attached to such rags; the tokens of virginity,
on the contrary, were the opposite of shameful. To lack such a cloth was
shameful, and legal grounds for public execution.

9. See James B. Jordan, The Law of the Covenant: An Exposition of Exodus
21-23 (Tyler, TX: Institute for Christian Economics), Appendix F.
