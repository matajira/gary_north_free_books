268 CHRISTIANITY AND CIVILIZATION

jutting spires or steeple to speak of and was of a rather shallow
construction, lacking any overhanging eves, I remember once
thinking that if it had been shingled in dark green rather then
light gray one could easily have visualized a herd of sheep and
goats grazing on an old grass-covered mound in Scotland.
Contrary to Freudian opinion, the spires and steeples of the
old gothic churches were not phallic images, but grand old
symbols of the mountain of God. Calvary never tried to be so
grand, just functional. There were no exterior images, and
the main entry on the north face of the building can only be
described as plain and flat. It was an austere public face for a
church of this size. Gonspicuously lacking on this facade was
the traditional Greek veneer so popular among other powerful
baptist churches throughout the ghetto. So one could confi-
dently say that at least externally there was no ode to Greek
“democracy” played here. There was no impressive portico
with large fluted pillars, just a series of broad steps leading up
from Burton Street to a row of evenly spaced wooden doors.

The interior of Calvary Baptist Church was a perfect com-
plement to its earth mound exterior. It was all very warm,
solid, and simple. The beige walls were lined with tall rec-
tangular windows of plain glass on the eastern and western
sides of the building. Because the windows were clear, and
placed in line with the path of the sun, blinds were needed to
prevent the light from becoming a problem. I remember the
deacons haying to make minute adjustments to the window
blinds as the light changed on the east side of the church dur-
ing the morning service, and then on the west side during the
early evening service. The windows were in fact so tall that
they naturally led the eye up to a high ceiling which was also
beige in color. It was a sitaple unadorned canopy showing just
a hint of the massive structural beams supporting the roof; I
remember clearly that even hefore I understood the elaborate
symbolism of the gothic nave, whenever I looked up at the ex-
posed beam work I always thought of what it would have been
like in Noah’s Ark, Beams, floor, and furnishings were all rich
brown woods that seemed to go well together. As one entered
the church the narthex seemed a bit too shallow for the overall
size of the interior. Its narrow rectangular construction was
sliced even further by several broad steps that ran the full
length of the narthex. Proceeding up these steps one was
brought abruptly to the second level of the interior, to the
