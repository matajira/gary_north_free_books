THE LITURGICAL NATURE OF MAN 193.

not only bread and wine, but also grapes, flowers, a bird,
even property handed over in the form of a deed or voucher.
in a different vein, could the worshippers’ need to confess and
expiate be met by inviting them to act out a gesture of recon-
ciliation with their brother befure they present their gift, or by
inviting them to lay on the altar written confessions of sins or
requests for counsel or prayer?

“But offering needs to be rethought and rehabilitated in
other services than the Eucharist. Offertory processions and
actions are clearly suitable at Baptism and confirmation.
Underlying all is the pastor’s duty to enlarge his people’s un-
derstanding of offertory by teaching them its manifold mean-
ings, its history, and especially its connection with their
worldly life. The two chief secular realities with which war-
ship needs to be related in our day, it has been said, are mat-
ter and power, the world of economics, science and technol-
ogy, and the world of sociology and politics.”!?

The offertory pulls in everything that a man has, The
church has made this more involved, not to assuage man’s
guilt, but to convey the idea that the offering is a consecration
by giving up.

Pentecost was a time of remission. Passover was complete
at its end, But Pentecost was on the fiftieth day and thereby
tied into this numerical pattem. Pentecost took place on a
grander scale at the fiftieth year of Jubilee. Double sabbath
happened in the year as well as the week. The land was
returned and slaves were remitted.

Man needs to have regular cancellation of his debts to
God, The Absoiution of sin officially cancels debts. At this
time the elders bind and loose. As the Scriptures say,
whosoever sins you remit, they are remitted. Binding and
loosing go with remitting of sin.

The Feast of Tabernacles

Once a year the people of God were reminded of the
dwelling place given at the Exodus (Lev. 23:33). It was not a
natural dwelling. It came through God's redemption. The
dwelling was a new tabernacle made from the same tree which
symbolized Christ. Every year they returned to their new

12, Hoon, The Integrity of Worship, p. 234,
