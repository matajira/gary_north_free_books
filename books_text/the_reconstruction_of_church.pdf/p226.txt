214 CHRISTIANITY AND CIVILIZATION

Theologically, Jesus Christ is the bridegroom of the
church (Matt. 9:15). God’s promised restoration of Jerusalern
also pointed to His own role as a bridegroom:

For Zion’s sake will I not hold my peace, and for Jerusalem's sake
I will not rest, until the righteousness thereof go forth as bright-
ness, and the salvation thereof as a lamp that burneth. And the
Gentiles shall see thy righteousness, and all kings thy glory: and
thau shalt be called by a new name, which the mouth of the Lorn
shall name. Thou shalt also be a crown of glory in the hand of the
Lorp, and a royal diadem in the hand of thy God. Thou shalt no
more be termed Forsaken; neither shall thy land any more be
termed Desolate: but thou shalt be called Hephzibah, and thy
land Beulah: for the Lorp delighteth in thee, and thy land shall
be married. For as a young man marrieth a virgin, so shall thy
sons marry thee: and as the bridegroom rejoiceth over the bride,
so shall thy God rejoice over thee (Isa. 62:1-5).

In the case of the Gentiles of Moses’ day, they could see the
symbolic righteousness of redeemed Israel on the doorposts. The daor-
posts were covered with the blood of the lamb. We know today
that this imagery pointed forward in time to the blood of
Christ. Christ died to provide righteousness—a righteousness
acceptable before God —for His bride, the church. It was His
blood, not hers, that stained the tokens of her virginity. It was
His pain, not hers, that accompanied the blood. He displays the
tokens of His bride’s virginity for the enemies of God to see.*

God took Israel publicly into the bridal chamber. The blood
on the doorposis was God's announcement of the consummation of the
marriage. The most eloquent account of this marriage in the
Bible is found in Ezekiel 16:1-i4.

Again the word of the Lorn came unto me, saying, Son of man,
cause Jerusalem to know her abominations, And say, Thus saith
the Lord Gop unto Jerusalem; Thy birth and thy nativity is of
the land of Canaan; thy father was an Amorite, and thy mother
an Hittite, And as for thy nativity, in the day thou wast born thy
navel was not cut, neither wast thou washed in water to supple
thee; thou was not salted at all, not swaddled at all, None eye

5. This use of the imagery of the blood of the lamb obviously does not ex-
haust all the possible images. The lamb was also a sacrificial animal whose
blood covered sin. But the problem for the commentator is ta deal specific
ally with the problem of blood on the daorposts. This was a unique use of
blood in the Old Testament. Why doorposts? What did the sin covering
have to do with doorposts? We should not seek to evade this important ex-
egetical question,
