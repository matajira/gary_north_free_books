188 CHRISTIANITY AND CIVILIZATION

Western man takes a simple word like “progress” for
granted. He assumes that he will be making more money the
next year, The next president will be better. His children will
grow out of their phases. And he quotes the cliché, “time cures
all ills.”

Western man is unique. Other civilizations simply do not
think this way. Their view of time is static, Cultures like the
Polynesian world assume that life goes in a circle. What they
do at any one point in the year will happen the same time and
way the next year. Year by year, life does not change.

Why does Western man so naively think the future will be
better? Even when empirical evidence indicates it will not get
better, he has a sort of innate belief that it will. Why? The
western world was influenced by Augustine’s beliefs about
time and worship.

Augustine changed the world with his writings, Czy of
God, and Confessions. Both fucus on a sabbatic view of time and
life. In the City of God, Augustine spends considerable time
showing that history is moving in a septenary pattern toward
a future glory. Although he wanted to avoid a millenarian
viewpoint, he still held that history would progress through
seven time pericds. It would end in a.p. 1000.

This emphasis injected a concept of progress. Western civ-
ilization was rescued from the direction of the eastern church
and world. History, according to Augustine, was moving toa
doxological end.

Confessions also had a sabbatic therne. He stressed an in-
terior sabbath. City of God was more cultural in its orientation.
Confessions concentrated on inner rest. His famous statement
about being restless until finding rest in God captures the
theme.

In Augustine, the sabbath concept of time and worship
carries forward. History and eternity are converging together,
rather than being completely separated. Augustine stopped
here, but not before giving enough to the church and civiliza-
tion to begin the Middle ages. The world moved forward!

The difference between these early bishops points out the
relationship of time and worship. Without a fixed time of wor-
ship in history, the world became siatic. Pulling a concept of
sabbath, that is warship, into history, progress entered
Western Civilization. The week was no longer viewed as just
returning on itself. It was moving toward a sabbatic end.
