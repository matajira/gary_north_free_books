GLOTHING AND CALLING 229

If one does not think the collar is a symbol of authority
and office, watch how a minister is normally dressed in the
movies. He is just about always a Roman Catholic or Episco-
palian, not only because these are the two most influential de-
nominations in the Western World, but because ail their minis-
ters wear their calling.

These illustrations sound good, but sorneone commitied
to the Bible must ask, “Is clerical garb Biblical?” I believe the
answer to that question is “yes.” So, the primary purpose of
this essay is to demonstrate the Biblical propriety of distinc-
tively ministerial dress. A secondary purpose is to discuss spe-
cific ministerial clothing in worship and at everyday work. Fi-
nally, I want to address some of the practical questions and
issues. Let us begin with three lines of Biblical argument for
ministerial clothing.

Clothing and Glory

First, clothing is glory. The multi-colored garments of
Aaron were called “glorious” (Ex. 28:2,40). Clothing is not
primarily for protection, nor is its main purpose to prevent
nudity. As we will see in a moment, the human body is itself a
form of clothing.

Glory is the interpretative key for understanding clothing.
It takes us to the glory cloud around the throne of God. Ezek-
iel and John saw into this realm and recorded their visions
(Ezek. 1 & Rev. iff.). They saw many created beings, human
and angelic, dancing a liturgy around God’s eternal throne.
But perhaps more significant to our study of clothing is the
fact that these beings are engulfed by the refracting light of
Gad’s presence. This light refracts through people and space,
forming a glorious rainbow wall (Rev. 21:19ff.).

In other words, the rainbow wall consists of color and peo-
ple (Rev, 21:14). One who measures has a “gold” measuring
rod (Rev. 21:15), and gold is typically a color of the
priesthood. Other colors are specifically associated with the
apostles (Rev. 21:14,19). We could draw other conclusions
perhaps,’ but the mix of color and people indicates that the

1. Color tells the story of redemption. Tf one looks at a color chart, he will
find thac the two base colors which form polar opposites are white and black.
White represents God's presence in Seripture, and black refers to hell.

Next to these colors on either end of the color chart is a purplish (scarlet)
