318 CHRISTIANITY AND GIVILIZATION

15,000 each.?1 Even cities like Phoenix and San Jose have not
escaped. There, homelessness has claimed nearly 5% of the
entire population,”*

So, where are they? If there really is starving in the
shadow of plenty, why aren’t we more aware of it? Why are
the statistics so difficult to believe?

The fact is, the poorest of the poor are invisible. Or, at
least, very, very hard to see.

The invisibility of the poor is due in part to the suburbani-
zation of our culture. “We don’t go to their neighborhoods.
They don’t come to ours,” explains University of Houston pol-
itical scientist Donald Lutz. “The suburbanization process has
geographically stratified America. Thus, the poor are out of
sight, out of mind.” Except for the hard-luck human interest
stories that have become standard holiday fare, the poor
almost never cross our path. The poor are invisible because of
where they are.

But many of the poor are invisible because of who they are
as well.

Thirty-five percent of all those living below the official
poverty line in America are elderly.7? Despite Social Security
benefits, Medicaid, and Medicare, many of these elderly poor
suffer severe privation in one form or another. Some have
dropped out of the social care system, too immobilized by ill-
ness to travel the distance to the post office, or the grocery
store, or the benefits center. Alone, afraid, and afflicted, is it
any wonder that the elderly poor all too often are shuffled off,
by time and circumstance, beyond our line of sight? Invisible.

Another 45% of the poor in America are children.”* They
don’t form lobbying groups. They don’t march on Washing-
ton. They don’t picket the unemployment offices. They don’t
crowd into the public shelters on cold winter nights. They
don’t line the sidewalks of Times Square wrapped in tattered
rags that have known too many springs. Like most children,
they trot out each morning to meet the school bus. Like most
children, they spend their days walking the corridors of Amer-
ica’s public schools — except that they are poorly clothed, often

71. Dallas Times-Herald, July 13, 1982.

72. Newsweek, December 27, 1982.

73. Wall Street Journal, March 7, 1983.

74. Bread for the World, Background Paper #63.
