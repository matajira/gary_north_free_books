184 GHRISTIANITY AND CIVILIZATION

glorify you through your child Jesus Christ, through whom to
you be glory and honor, Father, Son, and Holy Spirit, in your
holy church, now and forever.

Amen,®

Therefore, the early Church had ritual, and the notion
that the early Church’s worship was simple —that is—erratic,
is pure mythology and wishful thinking. This kind of thinking
grows out of a basic evolutionary presupposition that chaotic
worship is the purest.

Form and Freedom

Using Wellhausen as representative of modern man, we
can go a step further in our analysis. For Wellhausen, form is
in a dialectic, or in conflict with freedom. Pagan man has
always wrestled with the relationship between them. But he
has never been able to resolve the perceived dialectic. Why?

As Van Til says, man sees life in terms of antinomies
because he does not helieve that Gad is absolutely sovereign.
The presupposition of antinomy produces conflict between
Jorm and freedom, In the following chart, we have attempted to
capture the conflict. The square represents form and the cir-
cle, freedom.

 

 
 
     
     
 

  

Law Freedom
Nature Grace
Predestination Freewill
Mind Emotion
Group Individual

 

 

 

rot fl”
| \
I ; i }
| | \
eee “Le?
POTN
i | }
Lowa LH

6. Apostolic Tradition 4, ed. Bernard Botte, La Tradition Apostolique de Saint
Hippolyte, Liturgiewissenschaftliche Queelen und Forshungen 39 (Minster:
Aschendorfische Verlagsbuchhandlung, 1963), pp. 10-17. Translation by
Leon Mitchell in Meaning of Ritual, pp. 78-79.
