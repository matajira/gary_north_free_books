146 CHRISTIANITY AND CIVILIZATION

of Christ to a T, she would desist from any discipline
altogether since the whole age between the two advents is a
period of God’s longsuffering. The exercise of church disci-
pline would be postponed until the parousia!

The Lord’s gracious procedure with the church of Thyatira
(Revelation 2:18-20) affords clear evidence of how He deals
with both theological and sexual fornication. The Lord’s indict-
ment reads: “Notwithstanding I have a few things against thee,
because you allow that woman Jezebel... to teach and to
seduce my servants to commit fornication. . . .” Both Jezebel
and the church are required to repent. The point of contention
here is not that the church did not formally disapprove of
Jezebel’s behavior, but that “you allow that woman, . . .” The
church permitted the fornication to continue. That is the crux
of the covenant lawsuit. To be sure, space was granted Jezebel
and the church to repent, but not outer-space. The Lerd
would have the church to repent quickly lest He come quickly
and remove the candlestick quickly! It matters little whether
the church disapproves of rotten limbs; what is at issue is
whether the church “allows” those members to continue as
members of the body of Christ, In terms of divine computa-
tion and assessment, the Lord sees the allowal of these
members to continue as members as tantamount to long-
sanctioning instead of longsuffering.

Moreover, why is God longsuffering toward the sinner
who is outside the church? The answer is so that He might show
mercy (1 Timothy 1:16). And this is exactly the reason for shed-
ding this bogus “forbearance” expressed toward the scan-
dalous “brother” who is within the pale of the visible church. If
the church wants an impenitent brother to repent he must be
excommunicated from the congregation, The church then has
a responsibility to divest herself of a cavalier longsuffering
that confuses longsuffering with long-sanctioning.

The above truths defuse such turtle cliches as “Haste
makes wasie” and “What is awesome must be done slowly.”
These ideas simply do not mesh with New Testament revela-
tion. They are more than often a cover-up for moral cowar-
dice. The Lord says to the church as Pergamos: “Repent or
else I will come unto thee quickly . . .” (Revelation 2:16). If
upon continuance in sin, the Lord will come quickly, then we
can expect that a quick response of repentance toward
previously tolerated members is required. In terms of the
