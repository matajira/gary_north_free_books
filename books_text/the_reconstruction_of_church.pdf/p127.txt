THE GHURGH IN AN AGE OF DEMOCRACY 15

Second, redemptive-historical preaching is concerned
with synthesis. Greidanus points out the difficulties in using
this term—in hermeneutics it refers to the “deeper sense” of
Scriptures, and in homiletics it is used in contrast to analytic
~—and explains a much different use of synthesis than Ameri-
cans are used to hearing, Synthetic interpretation means that
one should “pay close attention to the specific relationship (syn-
thesis) of elements within the text. A certain text may contain
many of the same elements as other texts, but in the synthesis
of these elements every text is unique. Consequently, every text
has a unique message, for the preacher should preach the text
as a unit and not as a collection of separate elements.”29

Whereas the first aspect of redemptive-historical preach-
ing focused on the continuity, this point concentrates on dis-
continuity. Greidanus, to my knowledge, does not make the
point, but this approach is consistent with the Biblical Doc-
trine of the Trinity. The Holy Trinity is one and many and has
continuity and discontinuity. Therefore, one would expect the
Revelation of God to reflect the oneness and manyness of the
Trinity.

Paul’s Christocentric approach to the Corinthians seems
to bear the same emphases as Greidanus. The Corinthians
were subject oriented. They centered their attention on the
man instead of the text. Paul attempted to correct this mis-
focus by reminding them that good preaching is in terms of its
attention to Christ, not the personal eloquence of the man.
For the Corinthians, it was a matter that touched the preser-
vation of the unity of the Body of Christ because their man-
centeredness was tearing them apart.

@

The flesh. The fifth reason for schism was the flesh. Paul
points this out in the third chapter of I Corinthians when he
says, “And I, brethren, could not speak to you as to spiritual
men, but as to men of flesh, as to babes in Christ. I gave you
milk to drink, not solid food; for you were not yet able to
receive it. Indeed, even now you are not yet able, for you are
still feshly” (I Cor. 3:1-3, NASV).

29. Ibid., p. 138.
30. Of. Calvin on Ezekiel, pp. 66-67, 334-36 (Ist vol. on Ezek.).
