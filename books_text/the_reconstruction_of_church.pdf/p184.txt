172 CHRISTIANITY AND CIVILIZATION

babies. He said so, didn’t He?
Papa: Yes.

Nathan: Look. These people have families, right? Don’t they
feed their babies? They don’t make their kids sit in a corner
and wait till they're grownups before they can eat. So why
shouldn’t God feed His children, too? It must be sad for the
kids to watch the rest of the family eating without them.

Papa: But they don’t think their children really are God’s chil-
dren.

Nathan: But they teach their children to pray, don’t they?
Papa: Sure.
Nathan: Who do they pray to?

Papa: “Whom.” Objective case. And don’t end your sentences
with prepositions unless you have to.

Nathan: Do their kids call God “Father”? Like in the Lord’s
Prayer? Wait a minute. You aren’t going to tell me they don’t
believe in the Lord’s Prayer, are you?

Papa: Sure, they believe in it. And many of them teach it to
their children.

Nathan: Well then. If they teach their children to say “Our
Father,’ then that means they think their children are God’s
children, too. Right?

Papa: Uh... sort of. But—

Nathan: But they don’t baptize them into Jesus. So how can
they be God’s children unless they’re in the Covenant?

Papa: Right. That’s why they don’t give them Communion.
Nathan: Is this as confusing to them as it is to me?
Papa: It might be if they thought about it much.

Nathan: Well, how are their kids supposed to become Chris-
tians, if their parents don’t bring them to be baptized?

Papa: When they get older, they're supposed to make up their
own minds.

Nathan: About whether or not to obey God? That's pretty
