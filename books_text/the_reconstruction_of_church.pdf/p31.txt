THE CHURCH: AN OVERVIEW 19

and never returning) is failure to attend worship regularly.
Similarly, the Bible says that “since we are receiving a king-
dom which cannot be shaken, let us have grace, by which we
may serve God acceptably with reverence and godly fear. For
our God is a consuming fire” (Hebrews 12:28-29).

God does not take it well when we despise His invitation
to worship Him. “Again, he sent out other servants, saying
‘Tell those who are invited, “See, I have prepared my dinner;
my oxen and fatted cattle are killed, and all things are ready.
Come to the wedding [feast].”’ But they made light of it and
went their ways.... But when the king heard, he was
furious. And he sent out his armies . . . and burned up their
city” (Matthew. 22:4-7).

God’s worship is formal. We should approach Him in awe,
realizing that we are surrounded by angels, though we do not
see them. The Bible says that in formal Lord’s Day worship
we draw into the presence of “an innumerable company of
angels in festival array, and to the assembly of the firstborn
ones whose names are registered in heaven |that is, Christians
on the earth], and to God the Judge of all, and to the spirits of
just men made perfect [that is, the Christians in heaven]”
(Hebrews 12:22, 23, literally). We present ourselves before the
throne of the King, in the presence of all His retainers. When
we come into the King’s presence, we dress in good clothing.
We act with sobriety. We are quiet when we arrive for wor-
ship. Hopefully, we kneel or stand to address our petitions to
Him. !2

The Church as a Whole Burnt Sacrifice

The Church has always limped in history, and it always
will. People look at the manifest weaknesses of God's Bride,
and they spit on her. Yet, while God avenges His saints, He
still keeps them limping.

God told Satan in the beginning that the righteous One,
Jesus Christ, would crush his head, but that in the process,
the heel of the Lord would be bruised (Genesis 3:15). Thus,

12. Sitting for prayer, along with the complete disregard of the psalter in
worship, is one of the weirdest features of the 20th century Church. At no
other time in the entirc history of the Church have people ever addressed
God sitting down during formal public worship.
