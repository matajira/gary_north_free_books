142 CHRISTIANITY AND CIVILIZATION

simply having one’s name removed from the official roll of
membership. If one will not hearken to the preached word of
the elders performed privately, why should we expect him to
listen to that same word publicly? So, it is not a case of shut-
ting him off from what he has never heard, nor is it a case of
not wanting him to repent. We are telling him that he is not
invited to the congregational worship service for the praise of
God, the preaching of the Word of God being the center of
that worship, as long as he insists on worshipping God on his
own terms.

Under the rubric of the nature of the church ~it ought to be
remembered that the visible church is best understood by its
synagogical counterpart.’ Much that is predicated of the syna-
gogue is also predicated of the church. For example, in the
theology of James the synagogue and the church were synony-
mous institutions (James 2:2), Jesus indicts the church at
Smyrna and correctly describes it as “a synagogue of Satan”
(Revelation 2:9). Those that claim they are Jews but are not,
Christ says he will “make of the synagogue of Satan” (Revela-
tion 3:9). To be excommunicated from the synagogue was equivalent to
being put out of the synagogue. ‘This was the fear uppermost in the
minds of the Jews, who—during the earthly pilgrimage of
Ghrist—“did not confess him” lest they should be put out of
the synagogue” (John 12:42). We are told of the parents of the
healed man who “feared the Jews: for the Jews had agreed
already, that if any man did confess that he was Christ, he
should be put out of the synagogue” (John 9:22). Christ warns
his disciples that they should not be scandalized when they are
“put out of the synagogue” (John 16:1-2). In another context,
the Apostle John condemns Diotrephes who did not receive
the brethren but instead “cast them out of the church” (3 John
10). The indictment of Diotrephes is itself indicted but not for
the spiritual-physical excommunication, but for casting them
out on an inequitable basis ~a basis stemming from a desire ta
pontificate and establish his own “personality cult.” The Greek
word here (3 John 10) for “casting out” (ekballo) is identical
with the verb used to describe the (bodily) casting out of Jesus
into the wilderness in Mark 1:12. So our point sticks: Excom-

7, For additional data on the synagogue-church equivalency see the Rev.
Douglas Bannerman's The Seriptural Doctrine of the Church (Grand Rapids:
Wm. B, Eerdrnang Publishing Co., 1955), pp. 1078.
