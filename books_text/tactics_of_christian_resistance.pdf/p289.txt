CITIZENS PARALEGAL LITIGATION TACTICS 243

one human institution, such as the civil magistrate, is to
neglect or ignore other legitimate authorities also ordained by
God.

CANON THREE: Civil authority under God has limited
power.

This third canon is an obvious corollary to number two,
but because of its importance to the civil order, it merits
special emphasis. One of the primary reasons behind the
American War for Independence was the assertion by both
the King and parliament of absolute sovereignty. The col-
onists opposed this usurpation on legal and moral grounds. In
its place they asserted a doctrine of limited power with regard to
the civil order.

In working this out in the constituting of our government,
the Founding Fathers developed three separate but related
political concepts. The first was a division of powers. Secondly,
civil government has a multiplicity of powers. And, thirdly, there
is a complexity of powers in the civil order. The statist, on the
other hand, asserts a doctrine of the simplicity of power. All
power is confined to the hand of the planner or governor, and
tyranny quite predictably results.

The assertion of the division, multiplicity, and complexity
of powers is particularly appropriate for our discussion of
resisting the IRS. The IRS is but one authority within our
complex legal system. We can legitimately resist it without
denying the authority of our system in general. As we shall
later sec, resistance to the IRS on an individual as well as ec-
clesiastical basis is perfectly lawful and orderly within the
framework of the Constitution, as well as within the
framework of Internal Revenue Code (IRC),

CANON FOUR: Authority or power in the Christian sense
is ministerial, not legislative.

Authorities in any area, whether church, state, or family,
are not given the responsibility of creating laws apart from
God’s ultimate law. Their responsibility is to administer God's
fundamental law to a particular sphere. In the case of civil

21, This Independent Republic, p. 33.
