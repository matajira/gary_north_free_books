THE NEW COMMUNITY 97

must be accompanied by a theology of disaster, and such a
theology must center on the Christian virtue of hope. The
apostle who suffered innumerable hardships, including
beatings and irnprisonment, wrote: “But thanks be to God,
who in Christ always leads us in triumph” (2 Cor, 2:14). Hope
is what enabled him to see the essence of the situation—
triumph — beyond the accident of disaster. It is the quality of
which optimism is the secularized and debased remnant. It is
rooted in the faithfulness of God, the firmest of all founda-
tions, instead of being a mere habit of thinking or, worse, the
outcome of historicist or other theories of inevitable progress.
Now that the prevailing fashion is to cry doom, it is needed all
the more.

Embarking on the Great Adventure

Biblical faith finds great power—as does its imitator,
Marxism — in the conviction that history is going its way. Or
rather, that since Christ is the Lord of history, it is going
history’s way. Final victory is not dependent upon how well its
work is done; rather it is assured regardless of all contingent
factors. “Thy kingdom come, thy will be done on earth as it is
in heaven,” is not a pious wish, but a certainty. We do not
question if we shall be able to bring such a happy state of
affairs into being, but rather what our role should be in its in-
evitable fulfilment. Since the world’s powers were “disarmed”
in Christ (Col. 2:15) their might is limited, despite the illu-
sions of invincibility they are able to project. The eschatology
of victory is a principal theme of the New Testament.

Yet, we live in a world of phenomena as well as eschaton,
and we must face the question of what good the gospel of
Christ is in the here and now. Ironically, those who seek their
ultimate value in the next world are the only ones able to do
much good in this one. Those who love with this present
world destroy it along with themselves. Charles Cochrane
concluded his study of ancient Rome by affirming that Chris-
tlanity was the synthesis that provided the only cohesion to
the fragmenting culture of Hellenism. That may be the role it
is presently preparing to assume again.

From the most homely of responsibilities to the most ex-
alted, Christian faith bas the capacity to infuse coherence and
grace where disintegration now takes place. About seventy-
