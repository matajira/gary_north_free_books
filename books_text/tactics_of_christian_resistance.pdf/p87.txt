REBELLION, TYRANNY, AND DOMINION 41

learned of his need of a mate, God provided her by means of
the covenant act of separation (2:21). The Bible also informs
us that the covenant act of separation continues each time a
man leaves his father’s house to marry (2:24). Now that the
woman had been made, God gave His basic “cultural man-
date” to the man and the woman together, telling them to im-
age His covenant action of filling as well (1:28).

We could say more about all this, but we are now in a
position to interpret God’s second course of wisdom-
instruction, which also used an animal, God would bring an
animal to Adam to teach him something about his guarding
task. From naming the animals Adam had learned that he
needed something. “Well, Lord, you have told me to serve the
garden, but I find I cannot do so. There is a problem. I find I
have a lack. I need a helper suited to me.” So, God provided a
passive Adam with something to make up the lack, So also
here. Frora encountering the dragon Adam would learn that
he needed something. “Lord, you have told me to guard the
garden, but I find I am naked. I lack any robe of judicial au-
thority. I am not empowered to deal with this sicuation.” So,
God would provide, when Adam was ready for it, what he
needed to deal with the invader. Let us now consider this in
more detail.

First we read that Adam and Eve were “both naked and
not ashamed.” It is a fundamental mistake of interpretation to
think that man’s nakedness was supposed to be a permanent
condition, and that clothing was simply introduced to cover
man’s sin. Not so. God is clothed in a garb of light, an envi-
ronment called “glory” in Scripture. The “glory cloud” is seen
as a palace, as a temple, as a’ society of angels and men
around Him, and in other forms as well.* The glory cloud is
God’s garment of regal.and priestly office. Man, as Gad’s im-
age, should also have such robes. The robe of office, however,
is not something man starts out with, but something he must
mature into, by acquiring wisdom based on righteousness.
The robe of office is for elders, not for young men. Moreover, it
is never seized, but is always bestowed.

God intended for man to learn about his priestly task,
which involves measuring (evaluating, witnessing) as a

4. On this see Meredith G. Kline, Zmages of the Spirit (Grand Rapids:
Baker, 1980).
