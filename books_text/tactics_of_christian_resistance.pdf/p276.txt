230 CHRISTIANITY AND CIVILIZATION

exempt.’ However, after the Fall of Jerusalem in a.p. 70,
Judaism for a time was no longer a “religio licita” and this
brought Christianity as well as Judaism into serious trouble
legally. The real problem was not that the Roman govern-
ment wanted to tax the Ghurch (they apparently did not), but
rather that they (from time to time) wanted Christians to
swear by the genius of the Emperor: i.e. to sacrifice to him as
being the world’s center of unity and final Lord over all (in-
cluding the Church), Rome was perfectly happy for the
Church to exist as long as the Church recognized that Caesar
is final Lord over the Church: the one who makes it “licita” or
licensed. Most Christians refused to do so and accepted
persecution and death in order to acknowledge Christ as Lord
over all (including Caesar).

This situation of conflict radically changed with the Chris-
tian Emperor Constantine, who made Christianity the official
state religion. Under the Constantinian settlement (and this
was thoroughly backed up by the later influential Theodosian
and Justinian Codes, which had so much authority in shaping
legislation in all the Christian countries of Medieval Europe),
the civil government had absolutely no authority to tax
churches, They did not actually make churches tax exempt:
they rather recognized that they already were such by virtue
of the fact that churches paid taxes (tithes and offerings) to
their head: Jesus Christ, while the state paid taxes to its tem-
poral head: Caesar and his successors.

Sometimes tithes were collected for the Church by the civil
authorities (as is still the case in West Germany), but these
tithes went to the local churches and/or to the Pope in Rome.
In these cases therefore the civil government collected taxes
(tithes) not from the Church, but from the papulace, and then
gave the tithes to the Church.

Now there was an exception in Medieval Europe during
the Crusades, in which the Church itself asked the civil authori-
ties lo tax Church income in order to finance this movernent.
This was known as the “Saladin Tithe.” C. W. Previte-Orton
in The Shorter Cambridge Medieval History (Cambridge: At The
University Press, 1971}, p. 618, explains:

The Crusades, un which depended so much of the moral leader-
ship of the West assumed by the Papacy, were financed in two
ways: by indulgences, which were commutations of the vow to go
on a crusade in return for moncy payment, a measure productive
