324 CHRISTIANITY AND CIVILIZATION

when a familiar Old Testament passage is remembered,
“Thine, O Lord, is the greatness, and the power, and the
glory, and the victory, and the majesty: for all that is in the
heaven and in the earth is thine; thine is the kingdom, O
Lord, and thou art exalted as head above all” (f Chronicles
29:11). Also, we are further reminded of the textual variant of
the Lord’s Prayer which is commonly learned, “for thine is the
kingdom, and the power, and the glory, for ever” (Matt. 6:13).
In each reference, “glory” is parallel to “kingdom.” Thus, we
can examine this concept to approximate the meaning of
kingdom.

Glory is the image of God which is reproduced as a pat-
tern in several places. #6 In Ezekiel 1 the glory-cloud appears to
the prophet with characteristics similar to the heaven into
which John was taken (Rev. 1). Looking into the cloud,
Ezekicl and John see a throne at the center, fire and lightning,
and the people of God with the angels worshipping at God’s
feet. This liturgical/governmental scene 1s the glory of God.

The glory pattern is also present in the tabernacle/temple,
which is a microcosmic picture of the world.” It has at its
center the ark-throne where God is, and around the walls of
the structure are pictures of trees representing people (Psalm
1). The same colors to be noted in Ezekiel 1 and Revelation 1
are also there. These holiness colors often indicate God’s pres-
ence. Therefore, the tabernacle/temple is both a glory pattern
and cameo of what the world ought to be--ordered space
around the throne of God. Where God is present, His glory
appears. The glory encompasses God as well as what sur-
rounds Him.

Applied to the immediate concern, this information in-
dicates that the Kingdom of God follows the same pattern.
This order exists around God’s presence. Scripture speaks of
the nations of God coming to His holy mountain (Isa. 2). The
seventh angel of Revelation depicts the same scene when he
says, “And there were great voices in heaven, saying, The
kingdoms of this world are become the kingdoms of our Lord,
and of his Christ; and he shall reign for ever and ever” (Rev.
11:15). Therefore, the kingdoms, as they come under the
Kingdom, become the glory of God. Now it is clear why the
words “Kingdom” and “glory? are found parallel to one

16. Meredith G. Kline, Images of the Spirit (Grand Rapids: Baker, 1980).
17, idem.
