xX CHRISTIANITY AND CIVILIZATION

Reconstruction (Summer, 1981), he outlined fourteen steps that
churches should consider if they operate a Christian school.
Two overall principles must govern these educational minis-
tries. First: What does the Bible say? Second, consistency. “In a
Christian school context, this means that the church must
treat the school the same way the other ministries of the
church are treated. A clever state prosecutor will attempt in
every way to distinguish the Christian school from other min-
istries of the church. Be consistent!”
Here are Whitehead’s suggested steps:

1. The Christian schoo] should never be separately incor-
porated from the church,

2, The financial statements of the day school should show the
day school as a ministry on the same level as other ministries of
the church. (Also, do not use the language of a commercial
business in the financial statements of church or school.)

3. The church treasurer should write all checks and control all
funds flowing in or out of all ministries.

4, Ifpossible, all ministries should use the same physical facilities.

5. Use the Bible in establishing the church constitution, using
Bible references throughout,

6, The school name should be about the sarne as the church’s
name, and the same doctrine should be taught.

7, The governing board of the church should govern all the
ministries, mcluding the school.

8. All fund raising should be done through the school’s parent
organization.

9. If parents “donate” money to the school, it may be held to
be tax-deductible only in amounts above what tuition for their
children would be.

10. Teacher contracts must be carefully warded. Is the teacher
the same as a minister? Is the minister not under contract, but
the teacher is?

11. Church officers, especially pastors, create a problem for the
day school if their children are enrolled in government (public)
grade or high schools,

12. Churches are automatically exempt from Federal taxation
under the Internal Revenue Gode. Why apply for exemption?

13. Some laws (Internal Revenue Code) treat the church as
separate from the school. This could become a problem,

14. The State is asserting the right to tax churches and their
ministries. This is where the battle must be joined.!

10. John W. Whitehead, “God vs. Caesar: Taking Steps to Protect
Church Schools,” The Journal of Christian Reconstruction, VIII (Summer, 1981),
Address: P.O. Box 158, Vallecito, CA 95251; $5.00.
