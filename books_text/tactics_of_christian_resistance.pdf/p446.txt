400 CHRISTIANITY AND CIVILIZATION

the name of the Lard Jesus, act up to the dignity of your call-
ing! No more sloth! Whatsoever your hand findeth to do, do it
with your might! No more waste! Cut off every expense which
fashion, caprice, or flesh and blood demand. No more
covetousness! Bui employ whatever God has entrusted you
with in doing good, all possible good, in every possible kind
and degree, to the household of faith, to all men! This is no
small part of “the wisdom of the just.” Give all ye have, as well
as all ye are, a spiritual sacrifice to him, who withheld not
from you his Son, his only Son: so “laying up in store for
yourselves a good foundation against the time to come, that
ye may attain eternal life.”
