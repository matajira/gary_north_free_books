140 CHRISTIANITY AND CIVILIZATION

movement at this time, let alone by those Christians who are
not connected with it. We understand this, This is why we do
not expect this preliminary phase of Christian resistance to
experience very much success. it will serve as a training
ground, just as the wilderness served the younger generation
of Israelites who were allowed to enter the promised land.
This is not to say that individual Christians who do not
affirm all four doctrines cannot be successful resisters. Lots of
people will develop skills necessary to resist bureaucratic
tyranny, including many non-Christians. But a Christian
resistance movement as a whole which is not motivated gener-
ally by all four beliefs will not achieve long-term, comprehen-
sive success. Such a movement will not be able to construct a
Christian social alternative, which must be the earthly goal of any
serious Christian resistance movernent, God will not honor
the cause of those who do not rely on Him utterly—not on
human decisions, human logic, human will (*heroic” or
“free”), or human efforts. We cannot defeat Satan by pro-
claiming even the partial sovereignty of man. After all, the
partial sovereignty of man is his most successful lie (Gen.

3:5).
