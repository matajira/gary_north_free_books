24 CHRISTIANITY AND CIVILIZATION

one blessing and glorious prophecy upon the other concerning
the children of Israel. So at first Balak was entirely frustrated
in his attempts to curse Israel and rob her of Jahweh’s favor
which constituted her supremacy. But Balaam was not so easily
frustrated in his pursuit of this world’s goods and he ulti-
mately was somewhat successful in having Abraham’s God
curse the children of Israel. Christ rebukes the church in
Pergamos for having “them that hold the doctrine of Balaam,
who taught Balak to cast a stumbling block before the children
of Israel, to eat things sacrificed unto idols, and to commit for-
nication” (Rev. 2:14). The result of this original application of
the doctrine of Balaam by Balak is recorded for us in
Numbers 25. And had it not been for the righteous and
courageous actions of Phinehas by which the plague of God
was stayed, the strategem would surely have succeeded in
destroying Israel and removing her as a military threat to
Moab. As Balak properly saw, the struggle was a spiritual
one, and it was not won by the captains of the host of Israel,
neither was it Jost by the armies of Moab. The victory belong-
ed to one righteous man, to Phinehas, who stood in the gap
that day for Israel.

Now any Biblical doctrine of resistance to tyranny should
take careful note of Balaam’s strategy. It should note that
while the enernies of America promote abortion, homosex-
uality, pornography, and sabbath breaking, etc., it will not
suffice to debate SALT and START and Pentagon defense
budgets, etc. The battle js still the Lord’s and the issue is still
spiritual. We must hold to the converse of the doctrine of
Balaam. We must promote that national righteousness which
will bring down the blessing of the Almighty. Phinehas was
only one man and God was willing to spare Sodom if but ten
righteous men could be found there. Let us repent and
reform, reforming our own lives, our families, our churches,
and our land, that we may be instrumental in bringing the
blessings of peace and liberty to us and to our seed after us.

Elypocrisy

Our Lord hated hypocrisy and it was preeminent in His
rebukes. There has been a great deal of hypocrisy in much of
what passes for principled resistance to tyranny and it really is
small wonder that so much of contemporary resistance to the
