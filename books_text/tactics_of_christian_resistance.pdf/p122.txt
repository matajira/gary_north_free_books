76 GHRISTIANITY AND CIVILIZATION

sphere. When an office-bearer steps outside his sphere, he is
not to be submitted to.

Secondly, we submit in terms of God’s law. If the state
orders us to commit evil, we must not submit; and this may
mean conscientious refusal to participate in foreign wars (as
opposed to defensive wars). If a father patriarchally orders his
children to remain under his authority, after they are married
and have children, they are not to submit (Gen. 2:24).

By itself, what we have described would be conducive to
anarchy. We would obey an office-bearer only when he gave
commands within his sphere, and only when such commands
did not conflict with the Scripture. There is, however, a se-
cond form of submission, which God requires of us. It is sub-
mission to power.

Properly speaking, office, authority, law, and power
should always be joined. In a sinful world, however, they
often are not. The Bible tells us to submit to power where it is
manifesied.©9 That is, we are not foolishly to contest it. Those
in a subordinate position are not able to confront an evil
power, and thus must live by being invisible to it, by deceiv-
ing it. We take note of and submit to officers because the law
of God tells us to. We take note of and submit to power
because the sovereignty of God puts it over us.

Practically speaking, this means that if the state passes a
sinful law, we do not submit to it unless the state puts genuine
power behind that law. We do not have to obey sinful laws,
because we do not submit to human law. If we can evade,
avoid, deceive, or compromise with the powers, we should do
so. If they close one Christian school, we open another. If they
lock the doors, we cut the lock when they leave. When they
come back, they can lock it again. If they want to station an
armed guard, then they can keep it locked.

Rape is a good analogy. If God sovereignly brings a rapist
into a woman's room, and she cannot overpower him (say,

59. This principle is recognized in secular law as well. “According to the
Declaration of Paris of 1856, a blockade to be binding must be effective, In
other words, a sufficient force must be maintained to prevent access to the
coast of the enemy. . . . A blockade may be considered effective if the forces
employed are such that any breach of blockade will bring considerable risk
to the ships involved, An inelfective or paper blockade is legally not
binding” William L. Tung, Intemational Lac in an Organizing World (New
York: Thomas Y, Crowell Co., 1968), p. 470.
