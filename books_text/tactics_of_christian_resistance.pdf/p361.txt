THE CHURGH AS A SHADOW GOVERNMENT 315

creeds, The Schleitheim Confession, such a position appears in
“The Sixth Article.”’ “We hold that the sword is an ordinance
of God, outside the perfection of Christ. Hence the princes
and authorities of the world are ordained to punish the wicked
and lo put them to death. But in the perfection of Christ, the
ban is the heaviest penalty, without corporeal death.”® Ac-
cording to this view, one government is outside of Christ,
while the other is “in the perfection of Christ,” meaning there
are two governments,

Calvin’s Rejection of Dualism

This dualistic approach to government was vehemently op-
posed by John Calvin. In his Treatises Against the Anabaptisis and
Against the Libertines he specifically attacks the statements just
quoted from the Schleitheim Confession. He says in the chapter on
the magistrate: (1) The calling of the civil magistrate is a voca-
tion created by God. (2) The Bible provides set guidelines for
magistrates in the Law of Moses. (3) Magistrates are to protect
the church. Therefore, although the state is separate from the
church, it can exist in the “perfection of Christ.” And this avoids
a dualistic view of the world which places the civil realm outside
of Christ. Calvin expresses these points in the following manner.

“Therefore, let us hold this position: that with regard to
true spiritual justice, that is to say, with regard to a faithful
man walking in good conscience and being whole before God
in both his vocation and in all his works, there exists a plain
and complete guideline for it in the law of Moses, to which we
need simply cling if we want to follow the right path. Thus
whoever adds to or takes anything from it exceeds. the limits.
Therefore our position is sure and infallible.

“We worship the same God that the fathers of old did. We
have the sarne law and rule that they had, showing us how to

7. Bruderlich Vereinigung etlicher Kinsler Goites, sishen Artikel betreffend, in Flug-
sohrifien aus den ersten fahren der Reformation, ed. Otto Clemen (Leipzig: Halle,
1907-1911), vol. 2, pt. 3, as well as in other sources. See John Calvin's
Treatises Against the Anabaptists and Against the Libertines, tr. by Benjamin Wirt
Farley (Grand Rapids, MA: Baker, 1982), pp. 76ff. See John H. Yoder’s
English translation in The Legacy of Michael Saider (Scottdale, PA: Herald,
1973), pp. 34-43. See also J. C, Wenger, “The Schleitheim Confession of
Faith,” Mennonite Quarterly Review 19 (1945);243-253.

8. Calvin, pp. 76ff.
