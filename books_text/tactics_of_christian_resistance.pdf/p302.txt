256 CHRISTIANITY AND GIVILIZATION

the income tax amendment was passed. Total Federal reve-
nues in 1913 were $344,424,000 —hardly sufficient to operate a
messianic State!)*

The second category of “indirect tax” is the tax on the acts and
events of a privileged person such as a corporation or licensed
attorney. A privileged person is a person who has asked for
and received a specific privilege from the Federal Govern-
ment. For instance, an attorney is licensed by the state and
exercises the privilege of being an officer of the court. A cor-
poration, although an artificial person, is also granted
privilege: the privileges of perpetual life and limited liability.
Each of these “privileged persons” is subject to indirect taxa-
tion. The sudject of the taxation is the exercise of privilege,
while the object of that taxation is the parameter “income.”

To sum up this section, we have seen that beth classes of tax-
ation, “direct” and “indirect” taxes, have very restricted jurisdic-
tions. A “direct tax” cannot be imposed upon persons or prop-
erty without apportionment among the states. An “indirect
tax,” on the other hand, while not subject to apportionment,
must be uniform throughout the United States, and can only
be imposed on privileged persons, acts, or events, not on acis
and events of common right. We have also seen that the “income
tax” is not a “direct tax,” since it does not meet the Constitu-
tional requirements. Rather, it is an “indirect tax,” and as
such, if must be in some way a tax on privilege.

Before we can use this important point in direct application
to our contention that most of the individuals of the United
States are not “persons liable” for “income tax,” we must first ex-
amine some further preliminary matters. As is universally rec-
ognized, the bureaucracy is exceedingly complex. To unravel
the deadly web which has been woven around us without
becoming hopelessly entrapped requires persistence, patience,
and a great deal of thought and careful study.

Faci, Law, and Determination

Article Three of the Constitution grants power to the Fed-
eral Judiciary. The first sentence reads, “The judicial power of

44. Historical Statistics of the United States, Colonial Times to 1957 (\Washing-
ton, D.C.: U.S. Department of Commerce, 1960), p. 713.

45, See Appendix B.

46. See section below on “income.”
