TOOLS OF BIBLICAL RESISTANCE 445

abortion forces, using traditional methods, had mustered only
2,000. In addition, more than 500 anti-abortionists showed
up at the hearing and swamped the chamber.

Ii was incredible. My letter didn’t mention the Governor,
but he was getting copies of all the public input directed to the
Board of Health — plus all the mail that was sent spontaneously
to him directly.

He told me later that in his term of office this issue had
produced one of the three top responses. The Board voted in
favor of the pro-abortion regulation, 5 to 4. The Governor
vetoed it, as well as two related bills that passed the 1981 ses-
sion of the legislature.

With what was then less than $6,000 in equipment, we
killed the abortion bills and raised $3,000 for the Virginia So-
ciety for Human Lite. (We had asked for money for the group
in the same letter in which we asked people to write to the
Board of Health.)

I started out with about 3,500 pro-life names (plus another
like number from pro-life groups in the state) in the 1980 proj-
ect. We have carried out the project another time, and now
my computer is able to disgorge over 20,000 labels and/or let-

- ters of pro-life people (some with volunteer contributor infor-
mation, but most simply having come on board by signing a
petition). This size list would be equivalent to nearly
1,000,000 names and addresses nationally.

We found that a list consisting of petition signers, that
would be worthless for fundraising for a national organization,
could accomplish fast turnaround legislative projects, pay for
the mailing, and produce some net money on top. We think
that this experience would be true for others, assuming the
issue is state or local, the group making the mailing is state or
local, and the project is a hot one, which clearly abortion
funding is,

But POLSYS™ works on other issues, too, and at the
county level as well. One evening an activist in the Fairfax
County Taxpayer Alliance called, She was concerned that the
petition drive to force a recent property tax increase into court
was not going well.

The petition process is almost totally unknown in
Virginia, but state law provides for automatic appeal into the
county court of a new property tax levy within thirty days — if
one percent of the voters of the county sign a petition for that
