THE CHRISTIAN MANIFESTO OF 1984 3

church and at prayer meetings, and lose all their zeal for the
Creator God and this His world. Here the church becomes
ather-worldly, unearthly, and irrelevant to real life. It becomes
progressively more and more alienated from the real issues and
foreign to God’s world. Lonely and defeatistic, it surrenders all
the other areas of life to the forces of antichrist, which ulti-
mately surround and destroy this pietism. What pietism pro-
duces, therefore, above all, is its own grave-diggers.

Consistent Christianity, however, will ultimately prevail. It
alone will stand fast against and overcome the pressure of an-
tichrist’s world system, for the gates of hell shall not prevail
against the true members of the Church of Christ. ‘The batile
may be long and protracted, fierce and bloody, but the ulti-
mate victory is sure. For true Christians shall overcome ail
opposition by the indwelling power of the Spirit of God. And
then, when Christ comes for His Own, He shall gather all His
elect from the four corners of the earth, and destroy His
enemies with the brightness of His coming. The triumph of
consistent Christianity and the fall of the system of antichrist
are both equally inevitable. History is on our side! Even so,
come, Lord Jesus!

Objectives

In what relation do consistent Christians stand to mankind
as a whole?

Consistent Christians are truly human and seek to oppose
Satan and all his inhuman works. They do not seek to form a
separate party opposed to the rest of mankind, but when they
stand up for the Kingdom of God and His righteousness,
while the progressive non-Christians are converted and become
Christians, the remnant, the non-progressive non-Christians,
reactionarily cling to their sins and sink even deeper into their
Satanic alienation from true humanity. Consistent Christians
are the vanguard of the true humanity, the new creation, and
seek to persuade af/ men to become consistent Christians.
Non-Christian men are inconsistent men. Only by becoming
consistent Christians do inconsistent men become consistent
men. Only by dedicating their lives to the Lord Jesus Christ,
that most human of ali men, can humanistic men become
truly human.

Consistent Christians seek to realize aii their objectives.
