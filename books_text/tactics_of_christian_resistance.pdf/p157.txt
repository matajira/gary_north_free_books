APOLOGETICS AND STRATEGY Ut

ation. The Muslims of the seventh and eighth centuries,
A.D., believed in predestination fervently, and conquered
North Africa and even parts of Europe by means of the motiy-
ation which this doctrine provided, even in the perverted form
held by Islam. Marxists in our day have relied on “the in-
evitable forces of history” to undergird their efforts. Modern
science, until the advent of quantum mechanics, also rested
on a version of comprehensive, though impersonal, cause and
effect. Mao was wrong: power does not flow from the barrel of
a gun. Power flows from faith in the inevitability of a cause. In the
Christian world today, only the Calvinists possess this faith in
the inevitability of Christ’s cause.

Il. A Presuppositional Apologetic

Herbert Schlossberg is correct when he says that
apologetics should not be apologetic. We are not saying “we're
sorry” to anyone about our faith and our God. We have not
been assigned the task of designing an intellectual defense of
the faith which is solely a defensive faith. We defend faith in
the living God of history. We tell man of God’s totally suc-
cessful offensive campaign against Satan and his followers.
This message of God's total victory is, in fact, precisely what
offends. Fallen men do not like to hear about a God who shows
no mercy beyond man’s grave—a God who also intends to
smash the idols of humanity, in time and on earth. The exist-
ence of heil testifies to the inescapable fact that God takes no
prisoners. There are no POW camps in God’s post-~judgment
plans. God demands unconditional surrender, in time and on
earth,

One of the failures of Christianity for two thousand years
has been its defense of the faith in terms of the intellectual
categories of fallen, would-be auionomous man. The early
church apologists used Platonic thought to defend the truths
of the faith, and then the neo-Platonists made great inroads
into the church. The medieval scholastics used Aristotle to de-
fend the faith, and then natural law rationalists made great
inroads into the church. Since Kant, traditional Christian

25. The Bible teaches that God is the sustainer of all things—provi-
dence—because He created all things in terms of an eternal decree:
predestination.

26. Gary North, Unconditional Surender: God's Program for Victory (2nd ed.;
Tyler, TX: Geneva Divinity School Press, 1983).
