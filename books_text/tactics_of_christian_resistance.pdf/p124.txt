78 CHRISTIANITY AND CIVILIZATION

problem, however, is that it is precisely the direct government
of Ghrist over his Church which is at issue, and this is the
heart of the gospel. Thus, no compromise is possible.

Christians should be careful about hiring lawyers and try-
ing to fight matters out in civil court, at least when it involves
the church directly. (A Christian school not run by a church is
another matter.) The issue is jurisdiction, pure and simple. A
lawyer will almost always de facto grant jurisdiction to the
statc. In spite of personal integrity, lawycrs are officers of the
court, and have a vested interest in working within the sys-
tem. The church is outside the system, I do not say that all
lawyers are committed statisis at heart, but that Christians
must be careful in employing them. There may be a place for
going into court precisely to make the point about jurisdic-
tion, but a church officer must be careful not lo compromise
the integrity of the church in any way.

It should also be noted that many times a judgment call on
affairs such as this is a very clase matter. God promises to give
us wisdom in the midst of the situation, not in abstraction, as He
says in Matthew 10:19, “When they deliver you up, do not
become anxious how or what you will speak; for it shall be
given you in that hour what you are to speak.” We must he
careful about judging other Christians in abstraction. In the
midst of the situation, a man may determine that the state in-
tends to use maximum force, and may choosc to let his church
be shut down, and flee to start another. In another situation, a
man might force the state actually to use force before he
finally capitulates. The principle is the samc, though there is
variance in application.

Two areas we might briefly adelress before concluding this
appendix are the draft and taxes. 1 Samuel 8:104f. makes it clear
that it is sinful for the state to drafi men into an army for ag-
gression or even as a standing army (as opposed to a ready mi-
litia), and it is also clearly sinful for the state to claim more than
9.99% of income as a tax, since to do so is to make the state
preeminent over God. Thus, it seems that Christians should
not obey calls for the draft, and should pay no more than
9.99% in income taxes. Biblically speaking, property and capi-
tal taxes are wholly wrong, so Christians should not pay them.
If Christians respond to draft calls, or pay their full income
and property taxes, it is out of submission to power, not to law.
If the state is prepared to kill or imprison men for draft or tax
