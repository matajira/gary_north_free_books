388 CHRISTIANITY AND CIVILIZATION

canon law, its own military strength and financial power, its
own political connections and official authority.”

It would not, of course, have given way under pressure
alone had it not been seriously weakened from within by the
corruption of centuries. The Renaissance, which represented
a turn toward the world and away {rom religion, was held by
Burckhardt to be the declining stage of Christianity. If Protes-
tantism had not arisen, it is conceivable that Christianity
would have completely collapsed, 4 la the religion of the
Greeks and the Romans, with similar results, some centuries
ago.

As it was, the theories and pattern of the Renaissance re-
mained a part of European thought, to resume a fashionable
appeal during the Enlightenment. And although the
Enlightenment, which Peter Gay described as “the new
Paganism,” led directly to the massacres of the French
Revolution, the 19th century, once again, with the sanction of
Darwinism, resumed the underlying themes of the Renaissance.
We today, sated with scenes of vice and the promotion of
decadence, are well aware of what it must have been like to
have seen Italy through Luther’s eyes; something not too
different from modern Paris, with its sex shows for tourists, or
New York, with its endemic viciousness. The anti-Christian
nature of the Renaissance and the Enlightenment, and of
Darwinism, Freudianism, and other mind-sets of today pose
the same problems and challenges to us that the early
Calvinists confronted in their day.

‘They used, as is clear, the “wisdom of the serpent” in some
of their responses, It would be amusing, if it were not tragic,
that those who advise some similar ruses today are held to be
violating assumed regulations of unnatural purity, drawn up
by persons who hold themselves to be holier than God
Almighty. One diligent worker for the Gospel for instance,
known as Brother Andrew, managed to smuggle a large
number of Bibles into Communist China past the noses of the
commissars, by printing them in small red-covered books,
which resembled the notorious sayings of Mao-Tse-Tung.
Brother Andrew was actually criticized by some individuals
for this, on the grounds that his tactic was dishonest.

The world will not long respect nor harbor such unworldly

25. Was the Protestant Reformation a Revolution? passim. pp. 53-77.
