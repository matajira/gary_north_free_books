226 CHRISTIANITY AND CIVILIZATION

which should be well-known to those who worked with com-
munications in Vietnam). Other popular units are low power
backpack rigs such as the PRC-10s, low band mobile units like
the RT-70, and hand-held PRC-6s.

Frequency Selection

Selecting the array of frequencies is no mean trick. Some
frequencies are for monitoring only, and others are used for
two-way. Some are for short range, others for varying
distances which could reach half way around the world—
depending upon the client and the purpose of the commun-
ication. Many problems and influences come into play, espe-
cially in determining transmission frequencies. These include
propagation factors combined with the transmission modes,
antenna types, and amount of security required. Security is a
problem in selecting any type of communications system
which is intended to be operational over a reasonable period
of time without creating more problems than anyone really
wants.

If the transmissions are to be for long range work, then
they will have to lie between 2 and about 25 MHz; that’s the
rub! These frequencies are already crowded with broadcasters
and a mnyriad of other users who jealously guard them from
appropriation by outsiders. Doing it without creating in-
terference to some other (authorized) cornmunications or
broadcasting system is one of the most important things to
keep in mind. There is, of course, the option to camouflage
the communications operations to make them appear to belong
on whatever frequencies have been selected. It’s tricky, but it
is commonplace. Actually, there are many frequencies which
have been used (and are now being used) for such operations
without bothering anyone or calling undue attention to those
who operate the systems.

 

The battery operated unit can operate AM between 230 and 250 MHz and
FM from 38 to 42 MHz with 1/5 watt on AM, 1/2 watt on FM. The whole
thing can fit into a person’s pocket and yet has a 20 mile operating range.
This set was used in Vietnam, most especially by che CIA and related agen-
cies, The going value for the URC-68s in good operating condition is about
$150 and they are still in demand for use by all manner of paramilitary
groups, few of which are particularly concerned with being licensed by the
FCC, — Editor, Pop, Comm.
