ABOUT GHRISTIANITY AND CIVILIZATION 475

and enjoy life. We may contrast this attitude with that of Job, whe
was the richest man in the East (Job 1:3), who used his wealth
wisely and did not give it up foolishly ( Job 31:13-23), and who was
rewarded by God with even more riches ( Job 42:10-17). The same
may be said of Abraham, the father of true believers (Gen. 13:2).

We at Geneva Divinity School assert the Biblical doctrine of
justification by faith, and confidence before God. What the modern
church needs is not more guilt but release from guilt, and confident
service before the face of God.

Statement of Belief

Geneva Divinity School is committed to the absolute autharity
and inerrancy of the Holy Scripture of the Old and New
Testaments. We affirm that these contain all that is necessary for
faith and life, so that all aspects of society must be governed by the
Word of God, and all men are held accountable to Scripture by
God.

We affirm the historic Christian faith, as summarized in the
Apostles’ Creed, the Nicene Creed, and the Athanasian Creed. Fur-
ther, we affirm the theology of the Reformation, as summarized in
the Belgic Confession, the Heidelberg Catechism, the Canons of
Dordt, the Westminster Confession of Faith, and the Westminster
Larger and Shorter Catechisms. ,

Finally, we affirm that human reason is subject .o the Scriptures
at all pomts, and we look forward to the advance of the gospel,
amidst tribulation in the world until all nations have received the
blessings of the Kingdom of Christ. ~
