CITIZENS’ PARALEGAL LITIGATION TACTICS 253

ernment wishes to collect $1,000.00," then it would directly
tax Delaware and New Hampshire for $200 each, and Pennsyl-
vania and New York for $300 each. It would be the responsibil-
ity of each state to collect enough money to pay the apportioned
direct tax to the Federal Government. The individual states
probably would do that by some form of indirect taxation,
such as sales tax, but however it is collected, the Constitution
does not allow the Federal Government to gain jurisdiction
and directly tax the individuals of the United States.

At this point it would be proper to ask: How did we get
into the situation of paying income taxes to the Federal Gov-
ernment? The income tax certainly looks like a direct tax on
our “income.”9 If the Constitution does not grant the Federal
Government power to collect a direct tax from the individuals
of the United States, then why do they collect an “income”
tax?

During the last decade of the nineteenth century, statism
began a serious effort to void some of the Constitutional
limitations placed upon the Federal Government by our
Founding Fathers. One of the limitations that was a particu-
lar nuisance to the ideals of the statists was the clause in the
Fifth Amendment which reads, “No person shall be deprived
of property without due process of law, nor shall private prop-
erty be taken for public use, without just compensation,”
The statists saw possibilities for the widespread political
redistribution of wealth, and they began to agitate for a form
of taxation that supposedly would make the rich pay more,
and the poor pay less. In order to do that, they reasoned that
they must not only get around the clause in the Fifth Amend-
ment regarding “just compensation,” but they must also
somehow negate the apportionment limitation on direct taxes
in Article I, Section 2.

38. I believe in a very limited government!

39. “Income” is a legal-fiction term. See discussion below.

40, It is with this clause that the doctrine of eminent domain is derived.
‘You can sec that although it is possible to implicitly derive a doctrine of emi-
nent domain, it is alsa just as possible to derive a doctrine that would not
grant the ultimate ownership of all land to the state, but would grant them
what might be called an “easement” on selected pieces of property. That is, if
the Federal Government needed to use a particular piece of property for a
highway or some other good and necessary use, then it could do so, but it
must first negotiate with the owner, and then pay for the property. The Fed-
eral Government would not, however, have a ‘right” co all property.
