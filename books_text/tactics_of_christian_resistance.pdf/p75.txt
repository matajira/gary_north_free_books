BIBLICAL TACTIC FOR RESISTING TYRANNY 29

ourselves that there are many able, godly, and faithful ministers
in the land; yet, being convinced that we are called to humble
ourselves, and to justify the Lord in all the contempt that he hath
poured upon us—that they who shall know our sins may not
stumble at our judgments,~we have thought it our duty to
publish this following discovery and acknowledgment of the cor-
ruptions and sins of ministers, that it may appear how deep our
hand is in the transgression, and that the ministers of Scotland
have no small accession to the drawing on of these judgments that
are upon the land.

Only in this following acknowledgment we desire it may be
considered, That there are here enumerated some sins whereof
there be but some few ministers guilty, and others whereof more
are guilty, and not a few which are the sins of those whom the
Lord hath kept from the more gross corruptions herein men-
tioned; and that it is not to be wondered at if the ministry of
Scotland be yet in a great measure unpurged, considering that
there was so wide a door opened for the entering of corrupt per-
sons into the ministry, for the space of above thirty years under
the tyranny of prelates, and that also there hath been so many
diversions from, and interruptions of endeavours to have a purged
ministry in this land.

Now, when if ever in our day have you beard of a group of
Reformed, or Evangelical, or Fundamentalist ministers
publically in great detail (versus pious platitudes and empty
general confessions) confess their own sins and acknowledge
that they have greatly contributed to bringing down the
displeasure and judgments of God upon the land? Is this why
reformation lags and tyranny spreads itself like a green bay
tree? Let us labor and pray that in our day also we should be
blessed with so godly a response to God’s present chastise-
ments on our land.

History

The Bible is to be our only rule of faith and practice, We
take our cosmology from Moses and not from the pro-
nouncements of secular science. By faith we believe that the
worlds were framed by the word of God. We are not em-
piricists. But while we may reject science, falsely so called, yet
we do not hesitate to strive hard to establish thorough correla-
tion between special revelation, the inscripturated word, and
general revelation, the testimony of the creation. And neither
should we hesitate, in the light of the principles we have been
