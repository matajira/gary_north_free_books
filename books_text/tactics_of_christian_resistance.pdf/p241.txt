TERMITE TACTICS 195

Enemy’s plan to spread vicious anti-Castro propaganda on
the Purdue campus, she detected the first hints of interest. A
cocked eyebrow here, a sideways glance there. She had them!
Now, for the clincher. She announced that she had informa-
tion from an unimpeachable source that the entire CUBA
WEEK program was funded by the CIA and the leader of the
sponsoring group was a paid CIA agent.

Many of my fellow students found the charge mildly in-
teresting, particularly those who knew the alleged CIA agent,
namely, me. I like to think I have as much of a sense of humor
as the next running dog, but this was a bit much. After all,
student organizations at Purdue have all of their funds held in
trust by the University administration and every financial
transaction must be approved in writing by a faculty sponsor.
Like most student organizations, our little conservative group
was always strapped for money, Surely an organization like
the CIA could afford to keep up appearances in a style
somewhat grander than ours. Still, it was an opportunity to
have a little fun and we never passed up one of those.

A, The Haunting

It was about 6:00 p.m., the dinner hour at Purdue, but
Mizz Baxter was still working late in her office. She was
somewhat surprised when three students stepped into her
office and asked for'a moment of her time (I took along two
friends who wouldn’t have missed it for the world).

“Mizz Baxter?” I asked.

“Yes?”

“My name is Wayne Johnson, the person whom you identi-
fied in your classroom today as a paid CIA operative,” I said.

Mizz Baxter had clearly been caught off guard, but her
recovery was almost immediate. She would not be tested and
found wanting. . ..

“Are you here to deny it?” she asked testily.

I couicn’t resist. Actually I had come to deny it, but sud-
denly I realized how ridiculous this entire conversation was
bound to be. .

I narrowed my eyes, and spoke as:raspy as I could, without
laughing. “No,” I said slowly. “'mvhere to find out who the
leak is in our organization.”
