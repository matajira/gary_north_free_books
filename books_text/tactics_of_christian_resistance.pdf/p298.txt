232 CHRISTIANITY AND CIVILIZATION

unless in proportion to the census or enumeration herein-
before directed to be taken.”

On the basis of these Sections, the courts have ruled that
Congress has been granted the power to lay and collect two
different types of taxes: Direct and Indirect taxes. A direct tax
is one that is imposed directly upon property according to its
value (ad valorum). It is one that is imposed on existence.%”
Every year, if you own a home or sorne other form of real
property, you pay property taxes. Those taxes are direct
taxes. They are, however, levied by the individual states, and
not by the Federal Government. The reference to “taxes” in
Article I, Section 8, paragraph 1 refers to direct taxes which
are subject to apportionment among the several states (Article
I, Section 2, paragraph 3). There is a further limitation on
direct taxes in Article I, Section 9, paragraph 4. Not only
must direct taxes be apportioned among the several states,
but they must be proportioned according to the census or enu-
meration directed to be taken.

Now, what does all of that mean? In granting to Congress
the power to lay and collect a direct tax, “We, the People” did
not grant to Congress the power to collect money directly from
the unincorporated individuals of our nation. Rather, we
granted to Congress the power to lay and collect a tax from the
state governments. That is what “apportioned among the several
states” means. Further, not only must the tax be levied and
collected from the several states, but the amount of the tax
must be according to the number of people residing in that
state (“in proportion to the census or enumeration herein-
before directed to be taken”).

For example, if there are 20 million people in the state of
Delaware, and 30 million people in the state of Pennsylvania,
and 30 million people in the state of New York, and 20 million
people in the state of New Hampshire, and the Federal Gov-

36. Pollock v, Farmer’s Loan and Trust Co,, 167 U.S, 429, 15 §.Ct. 673, 39
L.Ed. 759 (1893). In the body of the opinion, Mr. Chief Justice Fuller, who
wrote the opinion, states, “In the matter of taxation, the Constitution
recognizes the two great classes of direct and indirect taxes, and lays down
two rules by which their imposition must be governed, namely: The rule of
apportionment as to direct taxes, and the rule of uniformity as to duties, im-
posts, and excises.”

37, Henry Campbell Black, M.A., Black's Law Dictionary, Fifth Edition.
(St. Paul, MN: West Publishing Co.), p. 413.
