BIBLICAL TACTIC FOR RESISTING TYRANNY 25

democratic totalitarianism of the welfare state has been an ex-
ercise in bitter frustration. Without divine blessing, any such
movement is condemned to futility, and God will never bless
hypocrisy.

As we have seen, reformation is the stock of the tree from
which liberty sprouts. But too many conveniently have forgot-
ten the fundamental truth that reformation begins at home. It
is the essence of hypocrisy that the focus is always on another's
sins and never on its own. When we are exclusively concerned
with the wood in everyone else’s eyes and oblivious to the
wood in our own, then our basic attitude has already
degenerated to crass hypocrisy, irrespective of how efficiently
we can apply God's law to anyone and everything else. Our
Lord brought this out so clearly in the parable of the Pharisee
and the Publican. The former, in spite of his extensive knowl-
edge of the scriptures, was an offensive hypocrite because all
he could see was the sin of this despised tax gatherer, this cor-
rupt tool of the pagan Roman State. The latter saw his own
sin and confessed it before God with penitential tears. The
scriptures leave no doubts about which man left Gad’s house a
justified man with peace and liberty in his heart.

Now it is the patent stock in trade of “Christian Conser-
yatism” perpetually ad nauseum to call everyone else to
repentance.* It is always the liberals, the socialists, the com-
munists, the atheists who are called to repentance. It is the
sins of the pornographers, of the National Council of
Churches, of radical politicians, that are constantly kept in
view. By comparison, the membership of such groups are con-
stanily preening themselves as a righteous elite standing for
“God and country.” One prominent anti-communist
evangelist actually pleaded with his following to ignore his
homosexuality so that they could get on with the main task of
saving the nation from communism. God does not give us
knowledge of His will primarily so that we can be better
judges of other people’s sins but rather that we might more
and more conform ourselves to the Lord Jesus Christ, the
perfect Servant of Jahweh. Knowledge brings responsibility;
therefore, judgment begins with the House of God. Unless we
stop acting and assuming and actually demanding that it start
with atheists and communists, etc., that is exactly where it

 

4, We might call this a doctrine of Selective Depravity.
