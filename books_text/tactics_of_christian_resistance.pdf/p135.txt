THE NEW COMMUNITY 89

Contrary to the sense of affliction and defeat that marks so
much of the contemporary church in the West, the tone in the
New Testament was one of victory. If we turn away from the
Weltschmerz adopted too uncritically from the larger society,
and look instead at the emerging new churches of Asia,
Africa, and Latin America, we see something akin to the
first-century exemplars. Considered on a worldwide basis, the
twentieth century is a great period of Christian expansion,
and the number of new converts to the faith has been
estimated reliably as exceeding fifty thousand per day.? If cur-
rent trends in East and West should continue, we may expect
that some of what are now the poor, backward countries of the
world will become the economic, political, and social leaders
of the twenty-first century, while the neo-pagan West con-
tinues its slide into impotence.

Toward the Triumph of Justice

Few ironies are more bitter than the fact that the strongest
declamations against injustice come from the apologists of
Marxism, an ideolagy responsible for the death or enslave-
ment of countless millions. The Christian churches of the
West in recent years have addressed this issue largely fram the
perspective of the great depression of the 1930s. Reinhold
Niebuhr in the United States and Archbishop William Tem-
ple in Great Britain, persuaded that the business cycle and the
hardships of the thirties were caused by the unbridled play of
market forces, led the movement to make state domination of
the economy normative. ‘This, they said, would bring into be-
ing the “just social order.” Their vision for the messianic state
was not far from what we have today. In this sense, if in no
other, they may be said to have succeeded.

Notwithstanding the errors of some of the church’s leaders
in this respect, the doing of justice in society is one of the ma-
jor themes in the biblical writings. How the weak are treated
is a test for any society, because it requires selfrestraint for
the powerful to do justice. “Ifa king judges the poor with equity,”
declared the ancient wisdom literature, “his throne will be
established forever” (Prov. 29:14). The king who does so does

7, Cambridge University historian Edward Norman discussed this issue
in the 1978 Reith Lectures, published as Christlantty and the World Order
(Oxford: Oxford Univ. Press, 1979).
