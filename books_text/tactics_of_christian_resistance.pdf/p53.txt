THE CHRISTIAN MANIFESTO OF 1984 7

geopolitically relatively insignificant Africa (where only 8 per-
cent of the world's present population is located) should im-
mediately be revised.

Tactics

The tactical reformatory Christian leadership will have to
begin in the nominally Christian nations like the U.S.A., the
Republic of South Africa, Northern Ireland, the Netherlands,
Finland, and, to a lesser but ever-increasing extent, South
Korea and Nationalist China (Formosa, Taiwan). The tactics
whereby Christians must obtain and maintain religious,
political, economic, and cultural power will vary in each of
the main strategic areas (viz., the U.S.A., the U.S.S.R.,
Europe, Red China, Japan, Israel, and Panama), in spite of
many points of contact and overlap.

If Christianity recedes in the U.S.A. it will probably recede
in the other strategic areas too, as far as can humanly be fore-
seen, But conversely, too—if Christianity does not soon pros-
per in the other strategic areas, it will probably also soon
recede in the U.S.A., at least for the foreseeable future. Let
us then deal with all these strategic areas, shortly, one by one,

i. In the U.S.A., the influential areas which must be con-
trolled by Christians are: the West Coast (Seattle through Los
Angeles), the Great Lakes (Duluth through Buffalo), the East
Coast (Boston through Washington), and the South Coast
(Houston through Atlanta). In the next few years, these are
the areas where U.S. history will be made; and of these areas,
control of the Great Lakes and the East Coast will be
decisive—for this is where the huge central and eastern
megapolis will arise. The U.S.A. still has the greatest
available number of Christians in the world at present, and
they must be increased and deployed to maximum utility both
in the U.S. and elsewhere. Geopolitically, the U.S. must man
its western border in the region of Southeast Asia (not just in
California), and its southern border south of Panama, keep-
ing these borders under constant surveillance. U.S, Chris-
tians must get involved in churches, politics, education, com-
merce, and international affairs— for Christ's sake!

2. The U.S.S.R. covers by far the largest Jand-mass on
earth —as big as the U.S.A., Canada, and Red China all put
together. Potentially, it has vast mineral resources — especially
