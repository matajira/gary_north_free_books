234 CHRISTIANITY AND CIVILIZATION

trouble with the bar association (or the Internal Revenue Ser-
vice), the amateur may be in a better position to defend his
own interests than to pay a lawyer to do it.

I point all this out as a motivation device. This essay will
introduce Christian citizens to a new realm of responsibility
that few have ever even considered. The escalating confronta-
tion with bureaucracy, as Gary North calls it elsewhere in this
volume, is forcing Christians to defend their interests, and
God's interests, with only minimal aid from attorneys.

T focus on recent developments in the field of tax law. Ido
this deliberately. If the IRS, generally the most feared of all
Federal agencies, has been stymied time and again by a par-
ticular series of tactics— tactics that have been adopted only by
dedicated paralegal (non-professional, non-licensed) defend-
ers of thetr own cases—then Christians have a reasonable hope in
dealing with the far less powerful bureaucratic agencies that are likely to
interfere with their God-given duties.

Christianity us. Secular Humanism

Secular humanism and Christianity cannot co-exist with-
out conflict. The State! cannot be sovereign over all of life and
simultaneously not control the Church and her schools. Just
as the State has sought to control every other aspect of life, to
offer “cradle to grave security,” so it is seeking to control the
Church. The beast desperately wants to control every aspect
of the Church’s life, from her finances to her methods of
evangelism, from advertising to publishing, from selection of
her ministers, teachers, and officers to the use of her facilities.
The messianic State is asserting a usurped sovereignty over
the Church, and it is using the IRS to enforce that sover-
eignty.

On January 9, 1978, then-IRS Commissioner Jerome
Kurtz in a speech in New York City unveiled the new IRS
policy with regard to churches. He outlined the “14-point
criteria” the IRS would implement to determine whether a
church is a church. Although Kurtz emphasized that the “en-
forcement program” primarily was aimed at tax-exempt
religious educational ministries that “discriminate in their ad-

. In this essay, I shall capitalize “state” when I refer to the modern, mes-
sianic civil government, and I will use the lower-case “s" when referring to
the civil government of regional units known as states.
