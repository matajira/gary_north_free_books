LEVERS, FULCRUMS, AND HORNETS 419

One and the Many. The POLSYS computerized program for
local political action, which uses the inexpensive Radio Shack
TRS-80 Model IIT or 4 computers, is ideal for such a training
program. The developers of POLSYS, Frank Slinkman and
Larry Pratt, could be brought to the CBN studios to video-
tape a training program in the use of POLSYS. This cassette
tape would go to all regional units of the Freedom Council.i3

What is needed is a generation of trained, dedicated political
activists who can learn leadership skills at the local level, They
can serve in local governments in order to build political
buffers to the illegitimate extension of Federal power. We need
men who will, if necessary, serve initially as dogcatchers, so
that they can later serve as county commissioners, state legis-
lators, and Congressmen. Not everyone needs to start at the
top. Most people shouldn’t. Elders need to serve first as dea-
cons; this principle is a good one for political action.

First approach: a local church uses the POLSYS program
for development of its own door-to-door evangelism-survep pro-
gram It thereby builds up a local data base. This data base
could be rented later on to political candidates who want to
use a direct-mail campaign. By renting the. list, the church
avoids suspicion about “mixing church and State.” But the
church is under no obligation to tell every local candidate that
it rents its list or even has a list to rent. Presumably, the elders
will want to screen the candidates very carefully.

Second approach: if local churches are unwilling or unable
lo adopt this sort of evangelism strategy, then focal units of the
Freedom Council can do it. These surveys become sources of
names for the creation of local data bases, as well as a national
data base. Again, we are honoring the One and the Many
principle: a national strategy that offers incentives, benefits,
and responsibility to local branches.

Third approach: a local Christian businessman uses the
POLSYS program, or other data base program (Datafax,
etc.) for business-related mailings. He can deduct the cost of
the computer and the program as business expenses. At some
point, he rents the list to political action committees or can-
didates. This transfers power to him, as the man who controls

 

13. Gary North, “A Politic Move,” Deskiop Computing (June, 1982).
14. Gary North, “Bread-and-Butter Neighborhood Evangelism,” The Jour-
nal of Christian Reconstruction, VIL (Winter, 1981).
