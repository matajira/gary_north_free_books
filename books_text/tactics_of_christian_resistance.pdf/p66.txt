20 CURISTIANITY AND CIVILIZATION

both Joseph and Daniel should be carefully appreciated. Both
these men were knowledgeable and godly men who rose to
positions of great power, influence, and authority in essentially
pagan empires. The scriptures totally vindicate both men
from any charge of sin with respect to their public actions as
recorded in sacred history. Yet there is no evidence at all in the
sacred record that either man ever attempted any reformation
whatsoever in the nations that each respectively ruled as a
result of their faithful commitment to Jahweh, the God of
Israel. To have attempted such a reformation would obviously
have exceeded their constitutional authority and would prob-
ably have been to court martyrdom. Martyrdom has its place
when we are called to it by the Gad who is sovereign over all
life, but we have no right to court it foolishly, especially by in-
sisting on casting our pearls before swine.

Both these men owed not only their positions but their
lives to remarkable dispensations of divine providence, and it
would have been a presumptuous tempting of the Most High
to have embarked on a rash and suicidal course of building
castles in the sky. Both men were models of faithful ministers,
wise stewards, and diligent servants bringing honor to their
God and submitting graciously to those powers and authori-
ties that God in His providence had chosen to place them
under. They performed the functions of their office with such
honesty and truth, such equity and justice, such remarkable
intellect, wisdom, and nobility of character, that they brought
honor to themselves and the God they served. They were liv-
ing examples of Paul’s admonitions in Romans 13 and in |
Timothy 6. God was greatly pleased with them and we should
be also. In short, there is no Biblical requirement to abuse
one’s legitimate authority in a vain attempt to promote a
reformation by authoritative fiat without a shred of constitu-
tional basis or national consensus.

Authority

While we are on the aforementioned subject, it is fitting to
examine carefully the nature of authority. As we have seen,
the matters of resistance to tyranny and of godly reformation
are closely related to questions of authority. And here we must
carefully distinguish between the revealed and secret wills of
God (Deut. 29:29). God's revealed will is that the murderer
