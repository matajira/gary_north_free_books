464 CHRISTIANITY AND CIVILIZATION

qualification, That staffer had worked for the other side and
he knew what the bills were, and what the amendments were,
and what section 5205 of this means, and what the Smith
Amendment to that means, and so on. He could make a case
for himself, saying: “I am not biased by any one fact or world
view, I just know the facts.” That’s the problem: if you're a
Senator, say on the Interior Committee, and you are dealing
with oil reserves, or deep sea mining, you need a staffer who is
a conservative and who also knows deep sea mining or what-
ever, and has the Capitol Hill experience so he knows how to
write a bill and how to draft a committee report and amend-
ments and all of this kind of thing. You're talking about a per-
son with extensive qualifications. The point is: There is a
shortage of qualified conservative staff.

Liberals have a system for developing competent staff.
This is the intern system. It goes back to the 60s, when liberals
would take the best and the brightest, the most ambitious col-
lege students from all over the country and bring them into
Washington to work in the summers or during a semester,
sometimes receiving college credit for it, in a Congressional
office. The zealous, enthusiastic, energetic kids don’t think
anything of working 12 hours a day and welcome the oppor-
tunity, It gives them a boost in their career. The intern system
has not been effectively supported and utilized by conser-
vatives. There has been one program in Washington in which
Christian colleges try to cooperate, but it is not a very large
program, and it does not outreach to conservative students.
Conservative legislators, when they get interns, like them to
be conservative, and they also like them to be intelligent. In-
ternships are a way of learning to become a Capitel Hil
staffer, so bear that in mind. At one point, Jacob Javits, a
former liberal Senator from New York, had on his staff, all
told, something like 50 lawyers —that doesn’t count legislative
aides or professional staff researchers or anything of that sort,
just Jawyers. And they were there cranking up bright ideas.
You can understand how important the pressure of the staff is.

Bureaucracy

Next is a source of pressure you cannot overlook. That’s
the bureaucracy. In politics, as in most walks of life, knowledge
és power. Information is power. And who controls information,
