POLITICAL RESISTANGE TACTICS 453

cut philosophical ameridment, without a major campaign,
you will get 25 votes in the Senate. That's up from only 12 or
i4 a couple of years earlier.

Opposite them are the liberals, the people who are com-
mitted to bigger government, unilateral disarmament, more
government regulations, and are opposed to traditional
values. Today, if, say, Howard Metzenbaum sponsors an
amendment, and there’s no national campaign to that amend-
ment, he will get, as it turns out, about 25 votes. Now,
sometimes you will get 29 here or 29 there, but this is an
average.

What all this means is that one half of the United States
Senate is available—up for grabs by whoever can exert the
strongest pressure. The Senators have preferences rather than
principles. A lot of these folks here in the mushy middle would
love it if they could get elected and for six years would never
have to cast a roli call vote. That way they could say one thing
to a group on one side, “I am really with you,” and then they
could come over to the other side and say to those who are on
the opposite side of the first group, “I’m really with you,” and
nobody would ever know because you could never pin them
down. Even as it is, with roll call votes, it is very difficult to
pin them down. We almost have to be there to outsmart them
and out-talk them because they'll be able to rationalize what-
ever they've done and tell us, “You really don’t know what you
are talking about.” But nevertheless, one half of the United
States Senate is uncommitted in the philosophical sense, and
as I say, they tend to go one way or the other. But in the true
philosophical sense, in terms of whether they have a world
vicw, these Senators are uncommitted. And they are the peo-
ple that you want to focus your attention on in a legislative
battle.

For practical purposes, I divide these legislators . into
categories of Saints, Saveables, and the Sinners. I’m going to
recommend something that is bad theology but good politics.
In a theological context, you ought to spend a lot of time with
the sinner to try to convert hirn, because there is more rejoic-
ing in heaven over one sinner who returns than there is over
all the just, but in political terms, it tsa mistake. Because, ina
political context, if you spend a lot of time with the committed
leftwingers, you're going to expend enormous amounts of
energy, time, and effort, and you're going to get virtually no
