THE CHURCH AS A SHADOW GOVERNMENT 343

Historically, the church viewed itself as a court. Paul says
to the church at Corinth, “Dare any of you, having a matter
against another, go to law before the unjust, and not before
the saints? Do ye not know that the saints shall judge the
world? And if the world shall be judged by you, are ye unwor-
thy to judge the smallest of matters? Know ye not that we
shall judge angels? How much more things that pertain to this
life? If then ye have judgments of things pertaining to this life,
set them to judge who are least esteermed in the church. I
speak to your shame. Is it so, that there is not a wise man
among you? No, not one that shall be able to judge between
his brethren” (I Cor. 6:1-5).

The early church took Paul’s injunctions so seriously that
their church buildings were laid out as a court. The area just
outside the door was a courtyard with a garden because they
saw the church as guarding the new garden of God.* Just in-
side the door, the vestibule, was for the hearers, catechumens,
and third class of penitents. At the center of the main
meeting area was the table where the Lord’s Supper was
served, and it was called the Tribunal, or Bema.* Since the
sacrament was viewed as the locus of discipline, as was the
rod in the home, and as was the sword in the hand of the
magistrate, the court was held here. Every Lord’s Day, when
the church mct for worship, court was in session. Judgment
was rendered through preaching of the Word of God and the
administration of the sacraments. People were admitted to the
Table and communion of Christ. Sometimes they were
dismissed. Furthermore, each Saturday was a time when the
Elders met, and people could appear before them to settle
their disputes before the Lord’s Supper was observed the next
day.

"prom Scripture and its application in history we begin to
see the complexity of this court. Although it was judicial in

 

air come down and rip me and my family to pieces if I am found to be un-
faithful.” Perhaps the word would not be used so capriciously if its true
Meaning were recognized!

44, Henry R. Percival, The Seven Ecumenical Councils, vol. XIV of The
Nicene and Post-Nicene Fathers, ed. by Philip Schaff and Henry Wace (Grand
Rapids: Eerdmans, 1974), pp. 254.

45. Idem.

46. Ider.

47. Idem.
