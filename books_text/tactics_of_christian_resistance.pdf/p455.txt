LEVERS, FULGRUMS, AND HORNETS 409

ment Southern whites used in 1962, the “outside agitators”
argument. Television coverage made the difference for King.
Tt could make the difference for Christians who want to recap-
ture the nation.

‘The existence of satellites and cable channels has at last
broken the hold of the major TV networks. The level of infor-
mation provided by a 30-minute interview is far greater than
that provided by a 2-minute news snippet. News snippets are
designed to hold an impersonal audience's attention long
enough to sell a percentage of them some soap. There is no
dedicated group of viewers who are emotionally committed to
an anchorman. On the other hand, there are millions of
viewers who are personally committed to one or another of the
electronic churchmen. Thus, they will sit in front of the screen
and listen to a lengthy interview, and even try to understand
it. This puts a major educational tool into the hands of Christian
leaders—~a tool which the humanists cannot match on televi-
sion because of the “least common denominator” principle
which governs the Nielson rating wars.

The lever of television gives the local Christian soldier
hope. He knows there is a potential army of supporters
behind him, if he gets in a difficult situation. His supporters
can be mobilized rapidly and inexpensively if a particular
electronic churchman gets his case before the viewers. The
problem of anonymity which the local Christian pastor faces
in any confrontation with the bureaucrats can now be over-
come overnight. This is what Martin Luther King discov-
ered, and it led to the creation of a successful resistance move-
ment in 1956. Bureaucrats run from adverse publicity the way
cockroaches run from light. This weakness must be exploited by
Christian activists.

The very existence of the CBN satellite in itself is a
mobilization tool of great importance. It is safe to say.that few
men are willing today to take the risks necessary to stand up
to the various state and Federal bureaucracies. The very pres-
ence of the satellite gives CBN an important edge in geiting its people in-
valved in Christian activism. Without a means of publicizing a
crisis, few pastors will take a stand. The CBN-mobilized
leaders could easily take positions of leadership locally that
other pastors would not dare to take, since they would not
have the potential back up of the CBN Satellite. The satellite
is like a howitzer on a batiefield in which Christians have
