xviii CHRISTIANITY AND CIVILIZATION

You will also receive a copy of Ed Rowe’s paperback book,
The Day They Padlocked the Church. This book can be ordered
separately, however, for $3.50:

Huntington House
1200 N. Market, Suite G
Shreveport, LA 71107

The humanist opponents of Christian education are blind
to the underlying issues involved in the battle over Christian
education-- willfully blind, in my view—~or else simply dis-
honest. They have become convinced, as so many naive
Americans are, that the State is responsible for the education
of children. The Bible teaches that parents are responsible,
not the State. It is only because madern men believe that the
State has taken on the functions of the family that people can
believe that the State must supervise education. But this
religion of the “State as pseudo-parent” is widespread. The
State has taught it in its very own established church, the
public school system.#

Conservatives are not immune to this error. I received a
letter from a subscriber to my economics newsletter, Remnant
Review, who protested against my insertion of a promotional
flyer telling the story of Pastor Sileven’s crisis. My letter is
quite conservative. Amazing— one of Sileven’s opponents was
a subscriber! Here, believe it or not, is a “conservative’s” view
~a very, very high official of the public schools in a Nebraska
county:

The State of Nebraska requires public and private school teach-
ers to be certified which requires a B.S. or B.A. degree in educa-
tion. The Catholic and Lutheran private school systems adhere
to this policy and they state that although religion is an added
part of the curriculum, qualified teachers still are needed to teach
the three R’s.

To someone living in Florida or Oregon and reading the news-
papers about Everett Sileven’s church, one would think that anti-
religious idiots existed in the State of Nebraska. The public pulse
columns of the Omaha World Herald were full of letters to the
editor from all over the country complaining about the Sheriff
hauling Sileven out of a church and not one of the letters stated
anything about the school, sponsored by the church, not using
certified teachers, which was why Sileven, as head of the school,

8. R. J. Rushdoony, The Messianic Character of American Education (Nutley,
NJ: Craig Press, 1963).
