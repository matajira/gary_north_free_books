254 CHRISTIANITY AND CIVILIZATION

Although a great deal more could be said, sulfice it to say
that in the infamous year of 1913, the Sixteenth Amendment
to the Constitution was passed. It reads, “The Congress shall
have power to lay and collect taxes on incomes, from what-
ever source derived, without apportionment among the
several States, and without regard to any census or enumera-
tion.” The statists gat their way, an unapportioned direct tax
graduated according co “income” was declared law. Or at least
that is the way it seemed.

As might be guessed, there was a great deal of debate and
bitter political fighting leading up to the passage of the Six-
teenth Amendment, And, as is often the case in a hotly
debated law, the Sixteenth Amendment was immediately
challenged in the courts.

The case that is most important for our consideration is
Brushaber v. Union Pacific Railroad (1916).*1 In this case a
stockholder, Mr. Brushaber, objected to the voluntary paying
of income tax by the corporation, Union Pacific Railroad
Company. The Supreme Court used this case to clarify the
confusion surrounding the Sixteenth Amendment. The major
objection was that the Sixteenth Amendment was unconstitu-
tional because it caused one portion of the Constitution to
conflict with another portion of the Constitution. As it then
stood, Article I, Section 2, paragraph 4 (the “apportionment”
clause) conflicted with the Sixteenth Amendment.

In the Brushaber decision, the Supreme Court ruled that:

1) In spite of the explicit wording of the Sixteenth Amend-
ment, Ho new power to tax was granted that did not already exist
under Article 1, Section 8.

2) The “income tax” was not in substance an unapportioned
direct tax, for to be so would cause one provision of the Constitu-
tion to conilict with another provision that had nat been lawfully
vepealed.

3) In order to avoid being an unapportioned direct tax, the
Court miled that the requirement that “incorne” be separated
from “source” was of principle importance and could now be
established by legislation. In other words, to tax a “source” would
be a direct tax, but to tax “income” is to tax the use of a “source,”
whence arises ruling #4.

4) Regardless of its form, the “income tax” is in substance
deemed to fall into the class of taxes known as “indirect” taxes,
along with imposts, duties, and excises, which were never consti-
tutionally subject to apportionment.

41, 240 U.S. 1 (1916).
