A CHRISTIAN ACTION PLAN 3il

by the Supreme Court school prayer decisions is beyond cal-
culation. President Roosevelt did not hesitate to use power to
force the Supreme Court to acquiesce to New Deal legislation.
Chrisnans should not hesitate to use the lawful power at their disposal
to secure reversal of onerous Supreme Court decisions.

(4) Christians must take action in education.

The courts and ill-advised federal regulations have often
made a mockery of education. Eliminating prayer removed
moral restraints; busing tends to remove neighborhood
restraints. Many schools have become undisciplined jungles.

Textbooks used in public schools often tend to destroy
long-established moral values. Parents have every right to insist on
quality moral education for their children. They should fight for it in
public schools, and if good public education is dented them, they musi
do everything possible to establish an alternalive private system of educa-
tion where Christian values can be taught.

(5) Christians must become aware of the awesome power

of the media io mold our moral and political consensus.

Christians need to do everything in their power to get invalved in
media (radio, television, newspapers, magazines). Where pos-
sible, Christians should seek to establish or purchase newspapers,
magazines, radio stations, and television stations.

Christians should learn motion picture techniques, produce drama,
write music, publish books — anything io produce a climate of righteous-
ness and godliness. They must dispel the sense of nihilism and
lack of meaning that is so evident in much that passes for art
these days.

(6) Christians should seek positions of leadership in

major corporations and benevolent foundations.

It has been said that money is the “mother’s milk” of
politics. It also is the essential nourishment of education, en-
trance into the media, the arts, and wide-scale evangelism.
Christians should learn the ways of finance: stocks, bonds, banking,
commodities, real estate, taxes. More than anything, they should learn
and apply the principles of God's kingdom dealing with the acquisition
and use of wealth. When they have accumulated material
resources, they should recognize the enormous good they can
accomplish with that wealth in unity with other members of
the body of Christ throughout the world.
