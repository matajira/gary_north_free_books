COMPUTER GUERRILLAS 224

states in pushing for alternative energy and conservation,
mainly at the prodding of the EDF and its partner ELFIN.

In 1980 EDF successfully used an ELFIN model to block
construction of a proposed $5 billion complex that called for
two coal-fired plants in Utah and Nevada. EDF contended
that the power needed could be provided less expensively
through a combination of conservation and available alter-
native energy sources such as wind and geothermal power.
The utilities withdrew from the project before the Public
Utility Commission (PUC) reached a decision, citing changed
projections of power demands, but according to the PUG the
EDF model was a decisive factor in killing the project.

ELFIN has also figured prominently in half a dozen other
significant utility or legislative hearings in Florida, Mllinois,
Arkansas, Michigan, and most recently in New York, where
EDF tried to halt construction of the partially completed Nine
Mile Point No. 2 nuclear plant near Oswego.

The Nine Mile challenge was a departure for EDF. “Until
Nine Mile we had basically been looking at plants that hadn’t
started,” Kirshner explains. “We wanted to avoid committing
good money to them until it was clear they were needed.” As it
turned out, EDF lost on that one; in February of last year the
New York Power Commission decided to go ahead with the
plant. However, the model did have some effect, The Power
Commission included some of EDF’s suggestions in its condi-
tions for building the plant.

Irrigation Plans via Micro

ELFIN is small by mainframe-computer standards, but
it’s still too large to run on a microcomputer. EDF és starting
to do some other projects on its North Star microcomputer,
however. The group bought the machine last year for word
processing, but it’s now being groomed as an attack computer
as well. One current project is an analysis of alternative
methods of farm irrigation in both the central plains of Texas
and California’s Central Valley. This is part of a project to
find ways to irrigate with less water; it ties in with a six-state
federal study that predicts farming may die out in parts of the
U.S. because of dwindling supplies of ground water.

Zack Willey says the result of this work— irrigation plans
that will help deal with the salt problem and eliminate the
