REBELLION, TYRANNY, AND DOMINION 65

threaten God's people and oppress them for 400 years (15:13),
but that God's covenant was as sure as His Person, and would
in time be established. It meant that Pharaoh and Abimelech
and Laban were birds of prey, but that God’s people would ac-
quire possessions and wisdom and would come out in wealth
and power and authority (15:14). It meant that Abram and
those who have like faith (Romans 4; Hebrews 6) must exer-
cise patience during the “probationary forty.” If they try to
seize it (land, power, glory, dominion, office), they will lose it
all.

Abram had the power to score a temporary victory over
Chedorlaomer. He was wise enough to realize he did not have
the power to maintain such dominion, and quickly retired
from the field of battle after rescuing Lot. He was smart
enough to wait.

Earning the Robe: The Example of Joseph

Reuben was Jacoh’s eldest son. Reuben could not wait to
inherit the robe, so he lay with his father’s wife (Gen.
35:22).45 For this revolutionary act, he lost his preeminence
(49:3-4). Simeon and Levi were passed over because of their
revolutionary actions, and so the blessing of rule came to
Judah, the fourth son (Gen. 49:5-12). Because of Joseph’s
faithfulness, however, Jacob early on elevated him over the
other brothers in some capacities. Jacob made for Joseph a
full-length (not multicolored) robe, and invested him with au-
thority over his brethren when he was only seventeen
(37:2,3,14). This may not have been a wise move, as the se-
quel perhaps shows, but it was prophetic, as God’s double-
witness dream showed to them all (Gen. 37:2-10).

44, In this case 400. See footnotes 9 and 13 above.

45. Taking the concubine of one’s predecessor was a perverted way of
claiming to be the new lord of the bride, Absalom did it publicly to David (2
Sam, 16:20-23). Adonijah tried to do it to Solomon (1 Ki. 2:13-25). This act
is forbidden explicitly in Deuteronomy 22:30 as an uncovering of the wing
of the father's garment, and is one of the particular curses of Deuteronomy
27, in v. 20. The “wing” is the extended corner of the rabe (Deut. 22:12) and
signifies the extension of a man’s dominion to the four corners of his life,
analogous to the four corners of the world which are overshadowed by the
four wings of the cherubim. For the seed to rise up and attack the bride is an
extvemely grotesque perversion of man’s symbolic imaging of the life of
God, and makes the seed into the serpent.
