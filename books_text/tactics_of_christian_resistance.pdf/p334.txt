288 CHRISTIANITY AND CIVILIZATION

thing to a real conspiracy that I have come across.

How can the right of jury nullification, or jury veto, be
used to initiate political reform? Let us suppose that a state
government passes a law making it illegal to operate a Chris-
tian school without a license. Then officials begin to prosecute
parents that send their children to Christian schools, ministers
that have a Christian school in their church, and the
employees of Christian schools. The District Attorney gets an
indictment on the Rev. John Smith on a charge of operating a
Christian school without a license. A jury is seated, and the
evidence and arguments of the case are presented. The judge
then instructs the jury that if Rev. Smith is operating a Chris-
tian school then he is guilty of breaking the law requiring
school licensure. He tells the jury that they are bound to
uphold the law to the “nth” degree. If the facts of the case as
alleged by the prosecution are true, then they must convict
Rey. Smith.

The jury retircs to deliberate. It just so happens that
seated on this jury is John “Concerned Citizen.” He providen-
tially knows about the right of jury nullification. John tells his
fellow jurors that they have the right to judge both the law AND the
Jacis. If they believe that the law is unjust, then they don’t
have to convict, regardless of what the judge said. The jury
decides that even though they can’t deny Rev. Smith is
operating a Christian school, he is not gutly of operating a
Christian school without a license. They decide to vefo a bad,
unjust law,

Let us expand the situation a bit. Suppose the government
is prosecuting churches all across the country. But because
juries have been alerted to the fact that they can nullify an un-
just law, the government fails to get a single conviction of any kind!
Just think of the message that “We, the People” would send to
Capitol Hill and the Justice Department! Back off the
Churches and Christian schools .. . or else!

If we look at the “vote” of a grand jury, the significance of
jury nullification becomes even more awesome. A grand jury
is a jury of inguzry. It is called a “grand jury” because it consists
of more jury members than a trial jury, usually not less than
12 and not more than 23 members. The grand jury is called to
investigate an alleged crime. It is their duty to determine
whether “probable cause” exists; that is, whether or not there
is enough evidence to indict an individual of a crime. The
