slit GHRISTIANITY AND CIVILIZATION

rebels will not enjoy the opportunity to call the power of God
into question (Psalm 2).

‘After the failure of the Puritan revolution under Cromwell
in 1660, one of the discouraged radical supporters of the
revolution was reported to have said, “God spat on our
dream.” God spits on the dreams of many people. If we
elevate our little dreams and schemes above the honor of God,
then we can prepare ourselves for damp faces. But if we stand
as Moses stood, on the honor of God’s name, and we conduct
ourselves accordingly, then God will spit on the dreams of our
enemies, for they are His enemies.

A Warning

If you or your church should decide to take a stand and
fight on a fundamental principle, such as the sovereignty
(legal jurisdiction) question regarding taxes or Christian
education, understand that there is no large, well-funded
organization which is likely to assist you. The few large ones
are swamped with requests to defend this or that group, free
of charge. These groups seldom can devote much time to the
cases they take on, and they are losing case after case. Yet the
smaller organizations are, for the most part, letterhead
organizations. They exist only as hopes and dreams.

When I first started to edit this issue, I contacted at least
five organizations that are involved in one facet of resistance
or another. I asked the directors to produce a 20-page, double-
spaced, typewritten essay on just what services their groups
offer, and what kinds of tactics they recommend. I received
back a few previously published pamphlets that did not really
deal with the issues I raised. I did not get a single essay that
described what, precisely, each group actually does to support
those churches or schools involved in resistance. My conclu-
sion: They aren't really ready to help you~not if they are
unable to get out what is in effect free advertising for their
own operations. Thus, you are on your awn, institutionally
speaking.

Those of us who are associated with the publication of
these two issues of Christianity and Civilization understand
that this is a pioneering effort. As far as we are aware, these
two issues constitute the only explicitly Christian materials
published in English in this century that deal with the ques-
