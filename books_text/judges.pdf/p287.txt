Samson: Blindness, Judgment, Restoration 273

Proverbs, whose “feet go down to death” (Prov. 5:5).

Here is the secret of Philistia’s domination of Israel. It was
not because they were stronger, for they were not. It was because
of Israel’s sin, and God’s giving Israel over to them. It is clear
from the passage that the Philistine, their warriors and their
magic, are utterly powerless. The only reason they have ascended
to rule over God’s people is that God’s people are faithless.

The Philistine blinded Samson so that, if his strength
returned, he still would not be able to fight. His blindness also
serves as an outward sign of his spiritual condition. Samson,
like Israel, and like Eli before him, has been blinded by sin.

He is put to work grinding in the mill. This is a sign of the
victory of Dagon, the Philistine god of grain. The Philistine
assumed that the fertility of their culture came from Dagon, the
god of grain and fertility. The text, however, hints of something
else. Grinding is put in parallel with sexual relations in Job
31:10, because of the connection between human and agricultural
fertility (compare Is. 47:2; Jer. 25:10), There is an eye for eye,
tooth for tooth justice in this. God’s principle is, as we have seen,
“If you like the gods of the nations, then you will doubtless enjoy
being dominated by the cultures of the nations.” The same thing
is true here. Does Samson (Israel) enjoy “grinding” with Delilah
(Philistia)? Well then, let him be put to work grinding!

Also, though, when Samson is grinding for the Philistine,
we see that their agricultural fertility and prosperity are actually
built upon the slave labor of Israel. Samson’s grinding is a pic-
ture of Israel’s condition, and it is also one more slap at idolatry.
It is not Dagon but the Lord who gives prosperity to Philistia,
and the only reason God gives prosperity to Philistia is because
He is punishing His people by making them slaves of Philistia.

Repentance and Vengeance

22, However, the hair of this head began to grow again after
it was shaved off.

This is so obvious that we need to ask why the Spirit bothers
to record it here. Do we need to be told that hair grows back
after it is shaved off? The point is that in his humiliation, Sam-
son begins to repent and return to the Lord. Simultaneously
God gives Him back his miraculous strength, and the sign of
