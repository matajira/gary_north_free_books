124 Judges

The NASV does not have the sense of the Hebrew here, for
it indicates that two bulls were to be used. Rather, it was the
“second” bull that did all the work. The second or younger bull
points to the theme of the replacement of the firstbom. The fact
that the bull was seven years old ties to the oppression of Mi-
dian, which had lasted seven years, corresponding to seven years
of apostasy for Israel. The Levitical law required that a national
sin be atoned for by a bull (Lev. 4:13-21). The seven years of this
bull atoned for the full week of Israel’s sin. It cancelled out the
defiled first week, and made possible the resurrection of the
eighth day, and a new week in righteousness for humanity, led
by the Son, the Younger Brother.

The bull that destroys Baal is the same as the bull that is sac-
rificed, for both actions picture Christ, whose death destroyed
Satan forever. This was a Whole Burnt Sacrifice, signifying total
judgment and devotion to destruction.

Gideon obeyed God. Because of fear, he did it at night.
(Also, if he had done it in broad daylight, he probably would
have been stopped.) He got a total complement of ten men to
help him, but a secret shared by ten men is no secret, and so the
town found out that Gideon was the one who tore down the
altar of their beloved Baal.

28. When the men of the city arose early in the morning,
behold, the altar of Baal was torn down, and the Asherah which
was beside it was cut down, and the second bull was offered on
the altar which had been built.

29. And they said to one another, “Who did this thing?” And
when they searched about and inquired, they said, “Gideon the
son of Joash did this thing.”

30. Then the men of the city said to Joash, “Bring out your
son, that he may die, for he has torn down the altar of Baal, and
indeed, he has cut down the Asherah which was beside it .“

31. But Joash said to all who stood against him, “Will you
contend for Baal, or will you deliver (yasha‘) him? Whoever will
contend for him shall be put to death by moming. If he is a god,
let him contend for himself, because someone has torn down his
altar.”

32. Therefore on that day he named him Jerubbaal, that is
to say, “Let Baal contend against him,” because he tore down his
altar.
