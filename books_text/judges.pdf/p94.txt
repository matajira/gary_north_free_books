76 Judges

early 1960s. As presented in the film, the throne of the king of
Spain had behind it a large frescoed tree. Apparently the
association of trees with thrones of judgment was not uncom-
mon in the Western world either.)

Important people are frequently seen sitting under trees in
Scripture, and indeed the Tabernacle, the central manifestation
of the gate of heaven, was pitched under a great tree (Josh.
24:26). Palm trees in particular are used to represent the right-
eous (Ps. 92:12). As they picture God’s people arrayed in His
presence, they are found all over the walls of the Temple (1
Kings 6:29-35; Ezk. 40:16- 41:26). Finally, it is the bride who is
compared to a palm tree in Canticles 7:7-8.

Thus, the palm tree is a fitting symbol for Deborah herself.
She constituted a gate to heaven for her people, rendering
judgments for them, and raising up a godly generation. As a
picture of the True Judge, she provided leaves for healing, the
fruit of the Word for eating, and shade for protection.

We have mentioned that Bethel, the house of God, was the
place where God’s ladder had touched earth in Jacob’s vision. It
remains to note that the other place mentioned in connection
with Deborah is also important. Ramah is between Bethel and
Bethlehem-Ephratha. It was while making a journey from
Bethel to Ephrath that Rachel died giving birth to Benjamin
(Gen. 35:16), and Jeremiah 31:15 identifies the place of her death
as Ramah. The death of Rachel in childbirth is the climactic
fulfillment in Genesis of the prophetic judgment levied against
the woman in Genesis 3:16: “I will greatly multiply your pain in
childbearing; in pain you shall bring forth children.” When
Rachel names her son Ben-Oni, “Son of Sorrow,” she is confess-
ing her faith and her acceptance of the conditions of Genesis
3:16. The mother suffers to bring forth the child, but her sorrow
is tamed to joy when he turns out to be the deliverer.

Rachel died and did not get to see Benjamin grow up.
Deborah, however, lived to see the triumph of her sons and
daughters. That greater Deborah, Mary (whose name means
“Bitter”), lived to see her son, Jesus, triumph over all
humanity’s enemies. (Notice that the praise heaped upon Jael,
Deborah’s twin in helping protect the seed, is applied to Mary in
the New Testament: Jud. 5:24; Luke 1:28.)

Bethel, the place of the Church, the Mother of Israel; and
