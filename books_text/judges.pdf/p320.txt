The Destruction of Benjamin 307

23. And the sons of Israel went up and wept before the Lorp
until evening, and inquired of the Lorn, saying, “Shall we again
draw near for battle against the sons of my brother Benjamin?”
And the Lorn said, “Go up against him.”

24. Then the sons of Israel approached the sons of Benjamin
the second day.

25. And Benjamin went out to meet them from Gibeah the
second day and cut down again 18,000 men of the sons of Israel;
all these drew the sword.

Israel arrayed itself for battle “in the place where they had
arrayed themselves the first day.” We see no repentance, no con-
fession that they also are guilty. Thus, they once again are
defeated. Their total loss is 40,000, a tithe of their whole army.
The tithe is what the Lord claims, as a token of claiming every-
thing. In taking this tithe, the Lord was judging them all.

Repentance

26. Then all the sons of Israel and all the people went up and
came to Bethel and wept; thus they remained there before the
Lorp and fasted that day until evening. And they offered bumt
offerings and peace offerings before the Lorn.

27. And the sons of Israel inquired of the Lorn (for the Ark
of the Covenant of God was there in those days,

28. And Phineas the son of Eleazar, Aaron’s son, stood be-
fore it to minister in those days), saying, “Shall I yet again go out
to battle against the sons of my brother Benjamin, or shall I
cease?” And the Lorm said, “Go up, for tomorrow I will deliver
them into your hand.”

Now Israel has gotten the message. First they fasted until
evening. The new day began in evening, and fasting for the re-
mainder of the day was a confession that they were cut off from
God’s blessings of food. Next they sacrificed burnt offerings,
which symbolized that they were guilty, and deserved to be burned
up by God’s fire, but that they trusted in the Substitute. Then
they sacrificed peace offerings, which symbolized communion re-
stored. They did this at evening. Since the new day began at even-
ing (as in Genesis 1, “evening and morning”), this is the third day,
the day of judgment, resurrection, and new beginnings.

We are told that Aaron’s grandson was ministering in those
