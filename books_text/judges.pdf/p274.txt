Samson: The Mighty Bridegroom 259

the Lord, and that seeks to turn Samson in. The cowardly
Judahites take 3000 men along to deliver Samson into the hands
of only 1000 Philistine, as we see in verse 15. Clearly, Judah is
unfit to produce a true king at this stage in history. We might
bear in mind that by this time the Ark has surely been taken cap-
tive by the Philistine. In their superstition, the men of Judah
think that there can be no deliverance without the presence of
the Ark,

We are reminded of another story when a young deliverer
was betrayed by fellow Israelites. In another Egypt, the real one
this time, Moses sought to deliver his people, but they would
have none of it. As a result, Moses had to flee (Ex. 2:11-15).

Philistine Asses

14, When he came to Lehi [Jawbone], the Philistine
shouted as they met him. And the Spirit of the Lorp rushed
upon him mightily so that the ropes that were on his arms were
as flax that is burned with fire, and his bonds were melted from
his hands.

15. And he found a fresh jawbone of a donkey, so he
stretched out his hand and took it and smote 1000 men with it.

16. Then Samson said,

“With the jawbone of an ass,
“One heap, two heaps,

“With the jawbone of an ass,
“Lhave smitten a thousand men.”

17. And it came about when he had finished speaking, that
he threw the jawbone from his hand; and he named that place
Ramath-Lehi [Jawbone Hill].

Whose fire is stronger? God’s fire, of course. The fresh, new
ropes snap as if they were but flax “that is bumed with fire.”
God’s fire frees Samson, and His fire defeats the Philistine once
again.

In battle, the Nazirite comes into contact with death, as we
noted in the previous chapter of our study. Thus, there is
nothing objectionable about Samson’s using a fresh jawbone of
an ass as a weapon. Ordinarily he would not have touched it,
but this is battle. (The fresh jawbone mayor may not have had
meat still on it, but it was not yet dried out and brittle.) The
Philistine had an iron monopoly in Israel, and so the Israelites
