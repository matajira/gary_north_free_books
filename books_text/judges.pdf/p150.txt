Gideon: God's War Against Baal 133

5. So he brought the people down to the water. And the
Lorp said to Gideon, “You shall separate everyone who laps the
water with his tongue, as a dog laps, as well as everyone who
kneels to drink.”

6. Now the number of those who lapped, putting their hand
to their mouth, was 300 men; but all the rest of the people kneeled
to drink water.

7. And the Lorp said to Gideon, “I will deliver (yasha‘) you
with the 300 men who lapped and will give the Midianites into
your hands; so let all the other people go, each man to his place.”

8. So the 300 men took the people provisions and their
trumpets into their hands. And he sent all the other men of
Israel, each to his tent, but retained the 300 men; and the camp
of Midian was below him in the valley.

Gideon is called Jerubbaal, the Baal-Fighter. He is going to
war against Baal, in the confidence God had given him the night
before. They rose early, with the sun. The rising of the sun is a
picture in Judges of the strength of God’s righteous people.

Deuteronomy 20:8 commands that when the army is sum-
moned, those who are fearful should be sent home. The Lord
reminds Gideon to implement this law now. Holy war cannot be
fought except by men of faith, who have confidence in the Lord
and are consequently basically unafraid. Moreover, the Lord is
showing Israel that ultimately He alone is the Deliverer; they
have no active part except to mop up after the battle has
definitively been won. Twenty-two thousand men departed, and
thus the place came to be called the Spring of Harod (“Fearful,
Trembling”).

In the second test, it was those who were single-minded who
were chosen. Lapping as a dog laps is explained in verse 6 as tak-
ing water in the palm and bringing it to the mouth. They used
their hands the way a dog uses its tongue to scoop up water.
These men were so conscious of the holy war that they did not
kneel down to drink, but remained standing and alert. They
were wholly consecrated to their task, single-minded. God’s wars
can only be fought by such men.

Gideon’s band numbered 300; the enemy 135,000. This is a
ratio of 450:1, in favor of the enemy. Not good odds, humanly
speaking. God’s plan, as revealed in verse 18 and following, re-
quired each of the 300 men to have his own torch, trumpet, and
