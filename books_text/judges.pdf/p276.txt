Samson: The Mighty Bridegroom 261

The deliverance from Philistia is a new exodus, as 1 Samuel 1-7
makes clear; here that motif is only hinted at, but water from
the rock is a clear reminder to Israel of Moses in the wilderness.
The Philistine oppression lasted 40 years, the same as the wilder-
ness wanderings. If Israel would follow the lead of their new
judge, Samson like Moses would give them water, and lead them
to the restoration of the promised land.

It is God’s way to humble us immediately after a great vic-
tory, lest we be proud and trust in our own strength. It is, thus,
of the grace of God that he humbles Samson right after his vic-
tory. Samson is reminded that God alone is the Source of his
strength. He would die, were it not for God’s grace to him.

Samson inaugurates his ministry with a glorious series of vic-
tories. I think that there is nothing to criticize in these early ac-
tions of Samson. They were followed up by 20 years of wise and
dynamic leadership and judging. At the end of those 20 years,
however, Samson fell into sin, a sin that was a picture of Israel’s
sin,

It was the sin of whoring after Philistine culture, signified by
whoring after Philistine whores. Maybe we should see Samson
already falling into this sin in the story we have just considered.
It might be that this story shows Samson (Israel) wrongly look-
ing to marry Philistine culture. If we take such an approach,
then we see God showing Israel the faithlessness of the
Philistine as Samson’s wife refuses to trust him, and the cruelty
of the Philistine as they murder her and her father. We might
see Samson “defiled” at the beginning of the story by scraping
honey out of the carcass of the lion with his hands, and see him
hurling away this defilement when he throws away the fresh
jawbone. Between these two actions, we might see Samson mak-
ing a series of mistakes, which God graciously delivers him
from, and from which he repents.

This is a possible interpretation, but I do not think it is the
correct one. I have attempted to show how each of Samson’s ac-
tions can be seen as morally and spiritually sound. The overall
reason why I think we need to take this approach is that nothing
in the passage says that Samson was in sin, or that he was
stupid. Rather, what the passage says is that the Lord blessed
him, that the Spirit stirred him up, and that his desire to marry
the Philistine girl was from the Lord (13:24, 25; 14:4). Unless we
