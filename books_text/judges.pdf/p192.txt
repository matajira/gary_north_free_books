Abimelech: Israel% First King 175

literature of the ancient world will show. In Baalism, it is ulti-
mately man who is most important, not the gods.

The Doom Implemented: Abimelech

50. Then Abimelech went to Thebez, and he camped against
Thebez and captured it.

51. But there was a strong tower in the center of the city, and
all the men and women with all the leaders of the city fled there
and shut themselves in; and they went up on the roof of the
tower.

52. So Abimelech came to the tower and fought against it,
and approached the entrance of the tower to burn it with fire.

53. But a certain woman threw an upper millstone on Abim-
elech’s head, crushing his skull.

54. Then he called quickly to the young man, his armor
bearer, and said to him, “Draw your sword and kill me, lest it be
said of me, ‘A woman slew him.’ “So his young man pierced him
through, and he died.

55. And when the men of Israel saw that Abimelech was
dead, each departed to his place.

Continual sin destroys the mind of man, and Abimelech has
now become a fool. His wrath knows no bounds. He is deter-
mined to destroy everything in his reach. If I can’t have it,
nobody can, is his attitude. So, he goes to fight Thebez. He tries
the same tactic here as had worked at the temple: of Baal, but
this time he is killed. Even though he tries to die honorably, his
humiliating death at the hands of a woman is enshrined in Scrip-
ture for all generations to laugh at (cp. Jud. 4:9).

Once again the head of the serpent is crushed. Once again it
is a woman, the protectress of the covenant and the guardian of
the seed, who crushes the head. Abimelech is thus equated with
Sisers.

There is more, though. Stoning was the prescribed mode of
capital punishment in the Old Testament. Abimelech is stoned
to death, in fit recompense for the murder of his brothers. Just
as he had slain them on one stone, so he is slain by one stone —
eye for eye, tooth for tooth.

Finally we should note that this is no mere stone. The Holy
Spirit takes the trouble to pen the fact that it was an upper mill-
stone. To keep it from being stolen, this woman had removed the
