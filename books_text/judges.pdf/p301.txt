288 Judges

besides? So how can you say to me, What is the matter with
you?? »

25. And the sons of Dan said to him, “Do not let your voice
be heard among us, lest fierce men fall upon you and you lose
your life, with the lives of your household.”

26. So the sons of Dan went on their way; and when Micah
saw that they were too strong for him, he turned and went back
to his house.

A little community of apostates had formed around Micah’s
shrine, and they joined Micah in pursuing the Danites. They
didn’t want their gods taken away.

There is a lot of humor and irony here. Micah, the thief, is
himself robbed. Even more ridiculous is what Micah himself
says, “You have taken away my gods which I made.” Man-made
gods aren’t very loyal to their makers, it seems. Here again is the
heart of Baalism: gods in the control of men.

The Danites remind us of a motorcycle gang. “Hey, keep
your voice down,” they say. “There are some real tough guys
back in the camp behind us, and if they hear what you say, we
might not be able to restrain them. You’d better go your way.”
Micah gets the message.

27. Then they took what Micah had made and the priest who
had belonged to him, and came to Laish, to a people quiet and
secure, and struck them with the edge of the sword; and they
bumed the city with fire.

28. And there was no one to deliver them, because it was far
from Sidon and they had no dealings with anyone, and it was in
the valley which is near Beth-Rehob. And they rebuilt the city
and lived in it.

Beth-Rehob means “House of Rooms” or “Roomy Place.”
The Danites have found a wide open space, it seems. They have
renounced the Lord as their Yasha‘ (Giver of a wide, open
space), and have made their own.

They burned the city with fire. This is a parody of the hor-
mah burnings in Joshua and elsewhere in Judges. The fire this
time was not from the altar of the Lord, but was strange fire.

Again the passage stresses the peacefulness of these Ca-
naanites. This is not to say that they were not to be driven out,
