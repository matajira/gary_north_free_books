182 Judges

Abdon must have been an old man when he became a judge,
for he already had 40 sons and 30 grandsons (not nephews, as
the AV has it), Abdon means “servant,” but he obviously was
not as much a servant as he should have been, because he also
was a polygamist. He extended his dynastic activity to his grand-
sons, a progression beyond what we have already seen. The
number 70 comes up here, as it did in connection with Gideon
(Jud. 8:30). It brings us full circle in the treatment of the theme
of the drift toward statism.

This is as good a place as any to comment on the death and
burial notices given for the judges. Beginning with Gideon, we
are told of the burial place for each of the judges, except
Abimelech. Why does the text tell us that each judge died? Did
we have any doubt about it? Obviously not. We have to look for
a theological reason, and it is not far to find. By calling atten-
tion to the deaths of these judges, Scripture reminds us that the
deliverances wrought by them were only temporary. Death still
had the last word, and thus men were still not really delivered
from the curse of original sin. This points to the need for a final
Deliverer, who would save us from death itself. It points to the
resurrection. .

Why mention the burial sites? Because tombs were memori-
als. They were memorials that reminded people of the curse of
death for sin, and they were memorials of the sure and certain
hope of the coming Deliverer who would raise men from the
dead. Thus, the Old Testament always pays attention to the
burial sites of prominent persons. Where the text does not men-
tion a burial place, as with Abimelech, this indicates by way of
contrast that there was no memorial for him.* We shall note the
peculiarities of Jephthah’s burial when we come to it.

Summary
It is apparent from these notices that the last three judges
were old men when they began to judge, and probably Tola and
Jair were getting along in years also. In contrast to Gideon,
these were important men in their communities, probably

4, That is, by way of contrast in this context. The context is the second half
of Judges. Death and burial are not recorded for Ehud and Deborah, but that
stands outside this context, It is beginning with Gideon that the text begins to
call attention to burial sites.
