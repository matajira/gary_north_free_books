The Levites’ Failure: Moral Depravity 295

What had been a pleasant stay is turning into a hassle. We want
the Levite to go ahead and get back where he belongs. There is
something not quite right about remaining too long in the
father-in-law’s house (compare Gen, 2:24). If there is a message
for Israel here, it is that they are being dragged down by too
much familiarity with the house of their father-in-law, which is
to say, too much familiarity with the house of that paganism
from which they had been delivered. The emphasis is on “eating
and drinking” with the father-in-law (vv. 4, 5, 6, 8). Again, this
is not morally wrong, but in terms of the typology it contains a
warning not to be too familiar with one’s unsavory past.

I mentioned in the previous chapter of this study that Bethle-
hem in Judah is presented in a rather unsavory fashion in
Judges and Ruth 1. Here, the bread of Bethlehem is shown
seducing and preventing this Levite from getting back to his ap-
pointed tasks.

Thus, the writer has skillfully created in us an air of tension.
The Levite really does have to be going, now. He needs to get on
home, where he belongs. He has been delayed from his holy
calling long enough.

There is one other matter to note in this section, which has
to do with its numerology. The girl stayed with her father 4
months (v. 2), and then they left on the 5th day (v. 8ff.). It might
not occur to us to add 4 to 5, but it would have occurred to the
ancient reader to do so. We wind up with 9. In some contexts
this might not have any meaning, but here it does. The girl has
played the harlot. At exactly nine periods of time later, she is
killed, raped to death. This is a horrible inversion of what
should have taken place. She should have been faithful to her
husband, and after nine months borne him a child. This is a pic-
ture to Israel of the consequences of sin. The fruit of faithful-
ness is life, but the fruit of faithlessness is death.

Bypassing Jebus (Jerusalem)

10. But the man was not willing to spend the night, so he
arose and departed and came to a place opposite Jebus (that is,
Jerusalem). And there were with him a pair of saddled donkeys;
his concubine also was with him.

11. When they were near Jebus, the day was almost gone;
and the servant said to his master, “Please come, and let us tum
