Jephthah: Reaching for the Crown 189

We read the terrifying phrase “the anger of the Lorp burned
against Israel.” So great was His anger that He permitted the
Ammonites to “shatter” Israel. The word for shatter is used only
one other time in the Bible, in Exodus 15:6 where it refers to
God’s shattering of the Egyptians!

10. Then the sons of Israel cried out to the Lor, saying,
“We have sinned against Thee, for indeed, we have forsaken our
God and served the Baals.”

11. And the Lorn said to the sons of Israel, “Is it not so that

(1) when the Egyptians,

(2) and when the Amorites,

(3) and when the sons of Ammon,

(4) and when the Philistine,

12. (5) and the Sidonians,

(6) and the Amalekites,

(7) and the Maunites
oppressed you, and you cried out to Me, then I delivered
(yasha‘) you from their hands? [This is a literal translation, not
the NASB, for verses 11 and 12.]

13. “Yet you have forsaken Me and served other gods; there-
fore I will deliver (yasha‘) you no more.

14. “Go and cry out to the gods which you have chosen; let
them deliver (yasha‘) you in the time of your distress.”

Here we have superficial repentance. The children of Israel
have become experts at sinning and repenting. Like the followers
of the modern cult leader R. B. Thieme, they have learned the
“rebound technique”: Just name your sin and God will automat-
ically forgive it, just like that. God is an impersonal computer, it
seems; type in the right message, and He will spit out forgiveness.

God rejects their “easy believism” theology. They find that
He is a Person, and that He is offended. He calls attention to
seven deliverances in the past, which they have respected by now
going into seven-fold apostasy!

Egyptians Exodus 1-14

Amotites Numbers 21:21-35

Ammonites Judges 3:13

Philistine Judges 3:31

Sidonians Judges 4-5

Amalekites Judges 6:3 (Ex, 17:8-16; Jud. 3:13)
Maunites Judges 6:2
