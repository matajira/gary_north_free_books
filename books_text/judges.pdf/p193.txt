176 Judges

small upper millstone and carried it with her. In the providence
of God, it is an implement of work that kills the man of
blood. We are back, then, to the by now familiar distinction be-
tween good trees and brambles, between horns and carpenters.
Ultimately, it is the craftsmen and the millers who will overcome
the hors and bramble men.

Some readers may think we are drawing too much out of this
verse (v. 53). But let the reader consider that the Spirit might
just as easily have written, “But someone threw a stone and it hit
Abimelech so that he was dying.” That is not, however, what the
Spirit chose to write. He calls our attention to the woman, to the
millstone, to the crushing of the head. We therefore are obliged
to take note of these details.

Conclusion
56. Thus God repaid the wickedness of Abimelech, which he
had done to his father, in killing his seventy brothers.
57. Also God returned all the wickedness of the men of
Shechem on their heads, and the curse of Jotham the son of
Jerubbaal came to them.

These two verses make it plain that God was at work aveng-
ing the family of Gideon Baal Fighter against Abimelech and
the men of Shechem, returning their wickedness upon their
heads. Every single person involved in the crime was killed
(9:43, 45, 49, 54). We may rejoice with trembling to see how
God avenges the blood of His saints. God emerged victorious
over Baal. Baal was destroyed by his own followers. The wicked
destroyed themselves, and the children of Israel were able to go
home and leave off this unnecessary and fratricidal war. The am-
bitions of men who would not leave their fellows alone to work
out their own affairs under God were responsible for this blood-
shed, though it was almost all their own blood that was shed.
All this, however, blossomed from the seemingly minor com-
promises made by Gideon, the faithful warrior of God. Let each
of us pray that we do not make similar compromises, for it is
our children who will pay if we do.
