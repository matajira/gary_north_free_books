30 Judges

God during the Conquest: His making the sun stand still in the
heavens. This is particularly noted herein Judges 2:9 to point up
the greatness of the works God had done. Surely parents would
tell their children about it.

The Failure of the First Generation

10. And all that generation also were gathered to their fath-
ers; and there arose another generation after them who did not
know the Lorp, nor yet the work which He had done for Israel.

The sad sequel, however, is that they did not. The first gener-
ation failed to teach their children about God. The children
grew up ignorant of the two things specified in Deuteronomy 6.
They did not know the Lord, and they did not know His works
on behalf of Israel. What does this mean?

In the first place, it means that the older generation was too
busy doing what they supposed to be God’s will, with the result
that their children were not taught. How often is this the case!
Scripture makes it plain that there is no more important task
any man or woman has than teaching his or her children about
the Lord. The very last verses of the Old Testament tell us that
the whole purpose of the Messiah’s work can be summed up as
restoring family life under God. Satan loves to see Christians
who think that the Kingdom cannot wait, and they they must be
busy. Satan has time (he thinks); and he is willing to wait, in the
confidence that the next generation will be his. The older gener-
ation worked hard to occupy that part of the land they had con-
quered, but all their labors came to naught because they did not
train their children, and the land was conquered by enemies.

This sad story happens over and over in the book of Judges.
Israel’s national disasters were a direct result of family disasters,
parents who did not understand God’s priorities. Busy-busy
Christians and their rebellious children: a story common to all
ages of the Church. And is this now why so many preacher’s kids
and missionary’s kids tum out bad? And how often is this simply
the result of parental egotism? “'m important and my work is
important, and I don’t have time for my children.” Parents with
such attitudes will pay dearly in old age, and so will society.

In the second place, it means that the children did not under-
stand the reality of the war between God’s people and God’s
