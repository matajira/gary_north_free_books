Samson: The Mighty Bridegroom 257

actually the stronger party, and the true judge and ruler of this
land. By acting against him, the Philistine had assaulted a
leader, The Philistine had unlawfully stolen 30 changes of
garments from Samson, which was now the value of his wife. By
giving his wife to another, they had robbed Samson of some-
thing worth “30.” His vengeance, then, is strictly according to
law, five-fold, involving 150 teams of foxes. Thus, while the
number 300 points to the non-final character of this judgment,
the pairing of the foxes and the resultant number 150 shows that
it was strictly according to the law of “eye for eye, tooth for
tooth.”

Vengeance on the Philistine

6, Then the Philistine said, ‘Who did this?” And they said,
“Samson, the son-in-law of the Timnite, because he took his
wife and gave her to his companion.” So the Philistine came up
and burned her and her father with fire.

7, And Samson said to them, “Since you act like this, I will
surely take revenge on you, but after that I will quit .“

8. And he struck them ruthlessly [shoulder on thigh] with a
great slaughter; and he went down and lived in the cleft of the
rock of Etam.

A heap of corpses lying on each other, with their limbs cut off
at shoulder and hip and piled up, is a grotesque but powerful
image. Samson killed a large number this time.

Samson’s wife had feared the fire of Philistia more than she
trusted the strength of her lord and husband, who represented
the Lord God of Israel. By siding against God, she hoped to
avoid being burned. She received the very judgment she had
hoped to escape.

There is a contest of fires in these two chapters. The
Philistine threaten fire against Samson’s wife, and eventually
they bring it upon her. Samson’s anger bums, and he brings fire
to the wheat of the Philistine. Fire is the sign of judgment,
man’s or God’s (as we saw in Judges 1:8 and 17). Whose fire is
stronger? As the fires go back and forth, the battle escalates.
Whose fire will win? The answer will come at the end of the
story.

How does this square with the lex talionis? The phrase
