210 Judges

a willing substitute could they hope to have life and fruitfulness
in the earth,

Thus, when the women of Israel came to talk with her four
days in the year, they were confessing that they deserved four
“days” of barrenness, and they were confessing that she had
taken their curse upon her, as their substitute. This is the first
way in which Jephthah’s daughter was a continuing sign to
Israel. She was a sign that they deserved perpetual sterility for
their sins, and deserved to be in mourning, but that God had
given them fertility because Someone had taken upon Himself
the curse that they deserved. Jephthah’s daughter can indeed be
a type of Christ, because as He represented the Church, He
stood in union with the Bride, and thus assumed a feminine
role.

But there is a second aspect to the ministry of Jephthah’s
daughter. She served as a type of the Old Covenant church,
waiting for the Lord to come and consummate the marriage.
The Bride of the Lord had to be “most holy,” lest she die (Dt.
22:13-21), and a picture of that is virginity. The high priest,
representing the Lord to the people, had to marry a virgin (Lev.
21:13). Paul compares Satan’s seduction of Eve with loss of
virginity in 2 Corinthians 11:3, and says that the Church has
been reconstituted a pure virgin by the work of Christ, and thus
a fit bride for Him. (In a sense, the New Covenant Church is
already married to Christ, and bears fruit for Him; but in
another sense, she is still a virgin, awaiting His final coming.)

Israel did not want to wait for the coming of their true Lord,
King, and Husband. They kept wandering off to fomicate with
the Baals (and “baal” means lord, husband), and they kept
wanting to go ahead and set up a human king. God kept telling
them to trust Him, and to wait. Jephthah, like the rest of Israel,
was impatient. With good motives, desiring to lead God’s peo-
ple in righteousness, Jephthah wanted to establish a kingdom
and a dynasty. God tured him down. The perpetual virginity of
Jephthah’s daughter was a sign to Jephthah and to all Israel that
the Lord alone was King and Husband, and that He had not yet
come to marry the Bride and establish the Kingdom. Year after
year, the presence of Jephthah’s daughter at the Tabernacle door
was a reminder of this fact to Israel. If they understood her
message aright, Jephthah’s daughter was the gospel for Israel.
