Jephthah: Reaching for the Crown 205

notion of the sacrifice of the firstborn as a foundation for the
city. Now we come to several other strange notions, which we
also find hard to understand, among them the idea of going up
to the mountains to mourn, and especially the idea of virginity
and the sacrifice of virginity. We shall have to try to understand
these matters as best we can.

First, after the victory Jephthah’s daughter came out to him
dancing. It was customary for the women to dance after a great
victory, as we see in the dance of Miriam (Ex. 15:20) and in the
dance of the Israelite women after the death of Goliath (1 Sam.
18:6). Apart from general joy and relief at the defeat of the
threat, there is a very specific reason for these celebrations,
which is revealed to us specifically in Jeremiah 31:4, “Again I
will build you, and you shall be rebuilt, O virgin of Israel! Again
you shall take up your tambourines and go forth to the dances
of the merrymakers.” The defeat of the enemy brings about
peace and safety from rape, and makes possible the building of
a house, with children. Jephthah’s daughter, like her father, an-
ticipates the building of his house.

Second, the passage stresses that she was Jephthah’s only
child. Any possible dynasty had to come through her. We know
that Jephthah was interested in a dynasty because he was anx-
ious to be made head of Gilead (11:9). The fact that it was she of
all people who met him caused him anguish, for he realized that
all his personal hopes were now dashed forever.

Third, Jephthah’s mourning was not some mild regret, as
those who defend him to the hilt want to make it out. He tore
his clothes, an action which has a specific symbolic meaning in
the Bible. A covenant is a bond, joining two or more people.
When those two people are ripped apart, the covenant is torn.
Thus, for instance, in marriage man and wife are one flesh, and
encircled by one garment (Ruth 3:9; Ezk. 16:8). The garment
symbolizes the covenant. Now, death definitively rips two peo-
ple apart. We know that what causes mourning and grief is the
intense feeling of loss, of separation, caused by death. Death,
then, rips apart the covenant that exists, whether formally or
informally, between people (the close tie of marriage, for in-
stance, or the looser ties of friendship). Thus, ripping the gar-
ment as a sign of the rending of a covenant is most appropriate
for mourning (for instance, 2 Sam. 1:11; 13:31; Ezra 9:3).
