270 Judges

They want to afflict him. This seems a strange way to put it,
and it is used three times in this chapter (w. 5, 6, 19). It is the
term usually used for the fiction of the people Israel by a for-
eign power, such as the affliction in Egypt. The use of this term
here brings out Samson’s identification with Israel, and
Philistia’s identification with Egypt.

6. So Delilah said to Samson, “Please tell me where your
great strength is and by what means you maybe bound to afflict
you.”

7. And Samson said to her, “If they bind me with seven
fresh cords that have not been dried, then I shall become weak
and be like any other man.”

8. Then the lords of the Philistine brought up to her seven
fresh cords that had not been dried, and she bound him with
them,

9. Now she had men lying in wait in an inner room. And
she said to him, “The Philistine are upon you, Samson!” But he
snapped the cords as a string of tow snaps when it touches fire.
So his strength was not discovered.

10. Then Delilah said to Samson, “Behold, you have deceived
me and told me lies; now please tell me, by what means you may
be bound.”

11. And he said to her, “If they bind me tightly with new
ropes which have never been used [with which work has not been
done], then I shall become weak and be like any other man.”

12. So Delilah took new ropes and bound him with them and
said to him, “The Philistine are upon you, Samson!” For the
men were lying in wait in the inner room. But he snapped the
ropes from his arms like a thread.

13. Then Delilah said to Samson, “Up to now you have de-
ceived me and told me lies; tell me by what means you may be
bound.” And he said to her, “If you weave the seven locks of my
head with the web.”

14. And she fastened it with the pin, and said to him, “The
Philistine are upon you, Samson!” But he awoke from his sleep
and pulled out the pin of the loom and the web.

Delilah tried and failed three times to find out Samson’s
secret. Each time she had men hidden in another room, but they
never showed themselves. She said “The Philistine are upon
you,” but they remained hidden. After all, if Samson had seen
