Samson: The Mighty Bridegroom 247

went. When he came to his father and mother, he gave some to
them and they ate it; but he did not tell them that he had scraped
the honey out of the body of the lion.

What happens here is a picture of the attack of Philistia
against Israel. The lion signifies strength and often represents
mighty powers that attack Israel. Just as David kills a lion be-
fore killing the uncircumcised Philistine Goliath, so Samson
fights a lion first (1 Sam. 17:33-37). Samson kills the lion of
Philistia. Once the lion is dead, the land once again becomes a
land of milk and honey. It is the death of the lion that makes
this possible.

(While in a general way it is obvious that the lion’s attack
pictures the Philistine/Egyptian assault on Israel, we can see
that the author of this narrative has made it fairly explicit by
means of literary parallelism. Note this comparison between
Judges 14:5f. and 15:14:

14:5f. And behold, a young lion came roaring toward him.
And the Spirit of the Lorp came upon him mightily,
So that he tore him as one tears a kid. .. .
15:14 The Philistine shouted as they met him.
And the Spirit of the Lorp came upon him mightily,
So that the ropes that were on his arms were as flax that
is bumed with fire... .

In both cases, they roar/shout as they meet Samson, the Spirit
tushes on Samson, and Samson tears something apart. Clearly
the roaring of the lion parallels the shouting of the Philistine.
In addition, as noted above, the lion guarding the entrance into
Philistine territory proper is parallel to the Sphinx that guards
Egypt. Samson’s destruction of the lion is his destruction of the
guardian of Philistia/Egypt, and lays Philistia/Egypt open to
invasion by God’s people.)

Deuteronomy 33:22 says of Dan, “Dan is a lion’s whelp, that
leaps forth from Bashan.” Just as Samson is the true sun to
replace Beth-Shemesh, so he is the true lion who defeats the
false lion of Philistia. He is a picture of the Most Perfect
Danite, the very Lion of God, Jesus Christ.

If we assume that Samson used his hands to scrape the
honey out, then we have to assume that he made himself
