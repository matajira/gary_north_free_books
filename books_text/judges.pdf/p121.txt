The Song of Deborah 13

The Kishon river flooded, swamping the chariots of Sisers,
Thus, the Lord showed His power over the dreaded iron
chariots. God used Baal’s own weapons, weather and water, to
destroy Baal’s army. The Kishon was particularly appropriate
for this work, since it is a very swiftly flowing river. The theme
of the river purging the land has been seen once already in
Judges (3:28), and will recur again (7:24; 12:5).

Why the Kishon is called an ancient torrent is not im-
mediately clear. The reference, however, is almost certainly to
the Flood and to the destruction of Pharaoh at the Red Sea.
God has an ancient torrent that sweeps away His enemies. The
sprinkling of water was used for the cleansing of the righteous,
and floods of water to cleanse the land of the defilement of Ca-
naanites. The Kishon was later used to sweep Baalists out of the
land again, in 1 Kings 18:40, under Elijah’s orders. Still later,
Josiah pointedly destroyed Asherah idols at the Kishon.

Deborah interrupts the duple rhythm of her Song at this
point to sing out, “O my soul, march on in strength.” It is man’s
part to march for God, but it is God Who gives the strength
(compare Ex. 15:2; Ps. 44:5),

The enemy tried to escape, either on foot or on horseback,
cutting the horses free of the harnesses to the chariots. This at-
tempt failed. The horses were trapped in the mud, and their
thrashing hooves slew many Canaanite soldiers. The beautiful
war machine, the valiant steeds (mighty ones), turned into a
liability rather than an asset under the vengeful providence of
the Omnipotent.

Stanza 3

The third stanza is the aftermath. It passes out curses and
blessings, and closes with rejoicing at the destruction of the
wicked. Meroz is cursed and Jael blessed. Deborah laughs at the
grotesque death of Sisers, and at the coming sorrows of the evil
anti-mother, the mother of Sisers. .

23a. “Curse Meroz,” said the angel of the LoRD,
“Utterly curse its inhabitants; *

23b. “Because they did not come to the help of the Lorp,
“To the help of the Lorn against the warriors.”
