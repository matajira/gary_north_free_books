18 Judges

times called “suzerainty treaties.” However they are termed,
Israel had been forbidden to enter into any treaties or covenants
with the Canaanites (Ex. 23:32). Thus, what we have here is
almost certainly not only a failure to follow out God’s com-
mands, but a direct violation of them.

The Response of Israel: The Other
‘Tribes’ Progressive Failure (1:30-36)

The First Degree of Compromise
36, Zebulun did not drive out the inhabitants of Kitron, or

the inhabitants of Nahalol; so the Canaanites lived among them
and became subject to forced labor.

The first degree of failure is for Canaanites to continue to
live among the Israelites. We have already seen this in the case of
Manasseh and Ephrairn. Here it is noted of Zebulun as well.

This section, concerning the rest of the tribes, deals with
Zebulun, Asher, Naphtali, and Dan. The first three tribes are
the northemmost. Perhaps they are selected to show that the
failure of Israel to follow through was comprehensive, stretch-
ing throughout the land.

Another aspect is that Joseph and Benjamin, the sons of
Rachel, have already been mentioned. So have Judah and
Simeon, sons of Leah. To round out the family, we have Zebu-
lun (son of Leah’s old age), Asher (son of Zilpah), and Naphtali
and Dan (sons of Bilhah). Thus the descendants of all four
wives are mentioned.

The Second Degree of Compromise

31. Asher did not drive out the inhabitants of Acco, or the
inhabitants of Sidon, or of Ahlab, or of Achzib, or of Helbah,
or of Aphik, or of Rehob.

32. So the Asherites lived among the Canaanites, the inhabi-
tants of the land; for they did not drive them out.

33. Naphtali did not drive out the inhabitants of Beth-
Shemesh, or the inhabitants of Beth-bath, but lived among the
Canaanites, the inhabitants of the land; and the inhabitants of
Beth-Shemesh and Beth-Anath became forced labor for them.
