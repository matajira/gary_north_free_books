Gideon: God’s War Against Baal 9

the Israelite social order and led to oppression and statism. The
same point is made here.

Gideon was from a poor family, and thus the spoils of war
were attractive to him. There is nothing wrong with taking the
spoils of war, as delineated in Deuteronomy 20:14. If the people
wanted to thank Gideon, they were entitled to give him gifts.

Gideon asked for one ring from each man. These were
almost certainly earrings, because it was women who wore nose-
tings in the ancient world. The Ishmaelite men wore earrings,
but the Israelites did not. When they came out of Egypt the
Israelites had contributed their earrings to make a golden calf
(Ex. 32:2-4), They had “played the harlot” with it, and had been
punished by God for their idolatry. As a result, when they
repented they forswore the wearing of earrings and like or-
naments (Ex. 33:4-6). Thus, the earrings of the spoils were of no
use to anyone as earrings; they would have to be made over into
something else.

Israel had spoiled not only the gold of Egypt, but also the
philosophy of Egypt. The Bible emphasizes the goodness of tak-
ing over spoils from the pagans, but warns against taking their
philosophy. It is easy for Christians to fall on either side of this
matter. Some are too ready to adopt the thinking of the world.
Others, in reacting against vain philosophies, reject the good
things of the Lord along with them. The Biblical position is to
take the goods of paganism, and build God’s tabernacle out of
them, not a golden calf.

Gideon used this gold, and the other spoils given to him, to
make an ephod. We are told that all Israel “played the harlot”
with it in Ophrah, where Gideon placed it. The parallel between
this incident and that of the golden calf must not be missed. As
Gideon drifts into a de facto though not de jure (in fact, though
not in law) humanistic kingship, the golden calf type of image
worship also creeps into society. Later on, the blatantly Baalistic
humanistic king, Jeroboam I, would openly reintroduce golden
calf worship (1 Kings 12:28-29). Jeroboam I put one of his
golden calves at Dan, which had for a long time been a center of
ephod worship Gud. 17-18).

Thus, there is a complex of connections, or principles, at
work here. When men are not satisfied with God and His wor-
ship, they begin to set up their own ways of doing things. In the
