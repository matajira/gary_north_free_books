The Salvation of Benjamin 323

tive comes in the third section, below.

This is a convenient place to take note of the numerology
that shows up here in the amazing providence of God. The
survivors of Benjamin numbered 600, six being the number of
humanity in its inadequacy. Just as Adam was created on the
sixth day (but did not attain to rest on the seventh), so Benjamin
is re-created in terms of the number six.

Four is the number of the earth, with its four corners.
Jabesh-Gilead rendered 400 virgins as a sign that Benjamin
would now be able once again to take dominion over its land.
Moreover, given the parallels between the earth/garden and the
woman, and that Adam was to guard both, we can see here 400
new Eves for the restored Adams of Benjamin. The fundamen-
tal imagery is of a new creation, and of new helpers meet for the
new Adams of Benjamin, in their restored dominion.

The proclamation of peace to Benjamin (v. 13) was possible
only on the basis of the peace sacrifice. At the same time, cor-
relative to that, Israel was able to offer Benjamin a practical
means of restoration. That Benjamin accepted the Lord’s proc-
lamation of peace to them, means that they repented and could
be accepted back into Israel.

Four hundred women were not enough for 600 men. Thus,
we are set up for the most remarkable sign of God’s favor to
Benjamin.

The New Birth

15, And the people were sorry for Benjamin because the
Losp had made a breach in the tribes of Israel.

16. Then the elders of the congregation said, “What shall we
do for wives for those who are left, since the women are
destroyed out of Benjamin?”

17, And they said, “There must bean inheritance for the sur-
vivors of Benjamin, that a tribe may not be blotted out from
Israel.

18. “But we cannot give them wives of our daughters.” For
the sons of Israel had sworn, saying, “Cursed is he who gives a
wife to Benjamin.”

19. So they said, “Behold, there is a feast of the Lorn from
year to year in Shiloh, which is on the north side of Bethel, on
the east side of the highway that goes up from Bethel to
Shechem, and on the south side of Lebonah.”
