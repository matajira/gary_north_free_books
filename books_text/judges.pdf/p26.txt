8 Judges

Caleb have the pick of the land, Caleb chose the land of the
giants (Josh. 146-12), and gave as his reason that he had been
thinking about it all those years (v. 12). Indeed, it was Caleb who
led the conquest of Hebron (Josh. 14:13-15; Jud. 1:20).

Now we ought to note that Caleb was not a racial Israelite,
but a convert from the Kenizzites (Gen. 15:19; Josh. 14:6). This
is remarkable in itself, showing the plenteous grace of God.
Like Uzziah later on, Caleb the convert was a better soldier of
God than were many who had been bor into the kingdom.
Caleb’s father was Jephunneh (Josh. 14:6). Jephunneh had a
younger son, Kenaz, named apparently for the tribal ancestor,
and his son, Caleb’s nephew, was Othniel.

Caleb offered to marry his daughter Achsah to whoever con-
quered Debir. This was a shrewd move, ensuring a worthy son-
in-law for himself and a worthy husband for Achsah. Caleb
knew that only in the strength of the Lord could a man conquer
Debir, and so he assured a Godly husband for his daughter.

And so, in a vignette, we have a great love story. Othniel, to
win the bride, destroyed the city. No Medieval dragon-slayer
ever did more for his princess. And of course, this romance is
but an emblem of the gospel, for it was the Greater Othniel who
conquered the wicked word of this world, in order to win His
holy bride.

The destruction of Debir is one more revelation of what it
means to conquer Canaan. The words, the philosophy, of the
Canaanites must be destroyed, and replaced with the Word of
God. In America today, as I write this, it is more and more the
case that the City of Books is in the hands of the heathen. A few
years ago the United States Supreme Court passed the Thor
Power Tool Decision. The effect of this decision is that
publishers must pay a tax on their book inventories each and
every year, so that books are nowadays published in small
amounts, and sold off as soon as possible. The only kinds of
books that stay in print year after year are, for the most part,
trash. This action on the part of the Federal Government is a
tremendous attack on the true freedom of the press, yet there
has been little outcry against it from the establishment. Re-
cently, the Presbyterian and Reformed Publishing Company of
Phillipsburg, N. J., had its non-profit status removed (since the
government decided it was making “too much money”). The
