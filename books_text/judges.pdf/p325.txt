312 Judges

Solomon. The men of Gibeah had not only raped this woman,
they had “gleaned” her, so that she was not only harmed but killed.
Just so, Benjamin is not only to be defeated, but “gleaned,” so
that it is destroyed. We may say that if the men of Gibeah had
only raped her, then Benjamin would have been defeated but
not completely destroyed.

All of this is important, as we shall see, in understanding
what happens after the battle.

The progressive diminution of Benjamin, as it is described,
creates for us a sense that the entire tribe is being liquidated.
First 18,000 are killed, then 5000 are “gleaned,” and then 2000
are smitten. Obviously, holy war is being conducted, and will
not stop until no Benjamites are left.

48. The men of Israel then tumed back against the sons of
Benjamin and struck them with the edge of the sword, both the
entire city with the cattle and all that they found; they also set on
fire all the cities which they found.

We have now to discuss more fully whether this action was
right or wrong. Did Israel break out of bounds and go too far?
Is what we read in Judges 21 a punishment for Israel because
they exceeded the Lord’s requirements? Or do we have to look
in another direction to understand what is going on here?

The problem is that there is no explicit statement of evalua-
tion from the Lord in this passage. Commentators generally feel
that this entire story is designed to show social anarchy, the
result of not having the Lord as King. We don’t have to disagree
with this general assessment to point out that Israel as a whole
had repented before the battle of the third day (Jud. 20:26) and
was acting under the guidance of the Lord.

Clearly, to make a moral assessment of the slaughter of Ben-
jamin, we need to take a close look at the laws of war in
Deuteronomy. These are the commands of the Lord. We can
then ask if Israel was obeying them properly or not. First of all,
Deuteronomy 13:

12. If you hear in one of your cities which the Lorp your
God is giving you to live in, anyone saying that

13. Some sons of Belial have gone out from among you and
have seduced the inhabitants of their city, saying, “Let us go and
