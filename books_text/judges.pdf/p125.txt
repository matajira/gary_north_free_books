The Song of Deborah 107

clesiastes 3:8, “A time to love, and a time to hate; a time for war,
and a time for peace.” We do not live in a static, changeless con-
dition. There are appropriate ways to think and feel about
various situations and conditions of life. There are times to
hate: “Do I not hate those who hate Thee, O LoRD? And do I
not loathe those who rise up against Thee? I hate them with the
utmost hatred; they have become my enemies” (Ps. 139:21-22).
We may well rejoice when the rapists, and the Hitlers, Stalins,
and Idi Amins of this world are dead. Until they die, we extend
the gospel to them, thereby loving God’s and our enemies. Once
they are dead, however, let us rejoice in their destruction!

31. Thus let all Thine enemies perish, O Lorp;
But let those who love Him be like the rising of the sun in
its might. [End of the Song.]
And the land was undisturbed for forty years.

The final prayer is addressed to God: In this manner let all
Your enemies perish. In what manner? By having their heads
crushed. All the enemies of God are arrayed under Satan’s ban-
ner, and they are all to receive the same curse as was placed on
him in Genesis 3:15. A man might live with a bruised heel, or a
crushed hand, but not with a crushed head. Total elimination is
prayed for here.

Even as God’s enemies are being destroyed, God’s people will
rise in history, in glory and power, just as the sunrise. Deborah
refers here to Genesis 32:31, where the sun rose as Jacob crossed
into the holy land, after wrestling with God all night. Deborah
prays that all Israel will be like their father, able to wrestle with
man and with God, and prevail. Her prayer receives an im-
mediate fulfillment in the next story in Judges, where Gideon,
after finding the strength to pursue the enemy all night (Jud. 8:4),
returns from battle at the rising of the sun (8:13, literal transla-
tion). A further fulfillment is in Samson, whose name means
“Sun.” All this is fulfilled fully in Revelation 1:16, where the face
of Christ is seen as “like the sun shining in its strength.”

Summary and Conclusions

The Song of Deborah is built upon contrasts. There are, ob-
viously, the two mothers, and the two seeds raised up by them.
