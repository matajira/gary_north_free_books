Samson the Nazirite 231

how to raise up the Seed. Similarly, God appeared to Rebekah,
not to Isaac, to give instruction about the primacy of Jacob over
Esau (Gen. 25:22f.).

Manoah’s wife was barren, The salvation of the world is to
come through the Seed of the woman, but because of sin, the
woman is barren. How can the Seed be bom? Only by means of
a miracle. Thus, all three wives of the patriarchs in Genesis were
barren. God had to open the wombs of Sarah, of Rebekah, and
of Rachel. The child born on the other side of the miracle, in
each case, was the Seed (note especially how Joseph delivers his
older brethren).

All three mothers of permanent Nazirites in the Bible were
barren. The mother of Samson we have before us. Samuel’s
mother was barren, and a miracle was needed (1 Sam. 1:6, 11).
The mother of John the Baptist was both barren and past
menopause (Luke 1:7, 15). In each case, a son who is set apart
for extraordinary work, and given especial power of the Spirit
(signified by long hair), is born of a barren woman.

The fulfillment of this theme comes when the Ultimate Seed
and Nazirite, Jesus Christ, is born of the ultimately barren
womb, the womb of a virgin. The genealogy of Matthew 1:1-17
points to the impotence of the natural line of Adam. The
genealogy issues in Joseph, who is not the father of Jesus.3 The
genealogy shows the need for a total miracle, a wholly new be-
ginning, for the salvation of the world. This is also the meaning
of the births of Samson, Samuel, and Obed (which may well
have been the same year).

Why didn’t Gabriel appear to this woman, as he did to Mary?
It is because it is important for the “angel of the Lord” to be the
one bringing the message. This is because the angel of the Lord is
the Captain of the Lord’s hosts (Josh 5:15 - 6:2). This Divine war
captain is also the One Who appeared to Gideon (Jud. 6:11ff.).
God is going to war, and He is raising up yet another Joshua,
another Savior who brings yasha‘ to His people.

The child is to be a Nazirite, and this is specified in terms of
alcohol, avoiding all ceremonial death (unclean food), and long

3, Numerologically, this genealogy is 42 generations long, seven sixes, a ful-
ness of human inadequacy. Of course, this genealogy also shows that Jesus in-
herited the official status of Isaac and Solomon, the seed/sacrifice and the
son/king, from Joseph.
