Irenceus, Bishop of Lyons 499

(i.e, John) or “the Apocalypse”? Which of these two antecedents
“was seen” “almost” in Irenaeus’s time and near “the end of the reign
of Domitian”? Swete records for us a significant observation from
master expositor F. J. A. Hort: “Dr. Hort, it appears, in his lectures
on the Apocalypse referred to an article by M. J. Bovan in the Revue
de Theologie et de Philosophie (Lausanne, 1887), in which it was sug-
gested that the subject of éwpd@n in Iren. v. 30.3 is not 4) dnoxdAuyas
but 6 ujv dnoxédAvynv éopaxK6t08s, i.e. 6 ‘Iadvvys.”" Such is all
the more significant when we consider the observations of the first
English translators of Irenaeus:

The great work of Irenaeus, now for the first time translated into
English, is unfortunately no longer extant in the original, It has come
down to us only in an ancient Latin version, with the exception of the
greater part of the first book, which has been preserved in the original
Greek, through means of copious quotations made by Hippolytus and
Epiphanies. The text, both Latin and Greek, is often most uncer-
tain....

Trenaeus, even in the original Greek, is often a very obscure writer.
At times he expresses himself with remarkable clearness and terseness;
but, upon the whole, his style is very involved and prolix.'®

 

few expositors have called into question the proper understanding of Irenaeus's 4o-
penevos. Guericke is bothered by the absence of the definite article beforeAopenavod.
Stuart relates his argument thus: “Guericke suggests, that when Irenacus says, ‘the
Apocalypse was seen not long ago, but almost in our generation, 1pd¢ 0 téAet tH¢
Aopenavos dpyric; that the adjective Aopettavod, (for adjective it may be, and if a0, it
ig one which ig generis communis, and not the proper name of Domitian), belongs, in
accordance with the Greek formation, to the name Domitius, and not to Domitian which
would make an adjective of the form Aoprmanxés. Ifit were a proper name, he says it
should be written 106 Aopeudvou. Now Nero's name was Domitius Nero, and not
Demitianus, which is the name of the later emperor” (Stuart, Apocalypse 1 :282-283n). If
such a re-interpretation of the phrase is permissible, and if we interpret the first portion
of the sentence from Irenacus along the common lines, then this would make Irenaeus
testify that the Apocalypse was written near the end of the reign of Nero.

This particular approach to the Domitian identity is very rarely held even among
convinced early date advocates. Farrar says that “no scholar will accept this hypothesis*
(Farrar, Early Days, p, 407), (This must be an overstatement, since Guericke was a
reputable scholar.) Stuart doubts its validity, as did Macdonald. Not only does it seem
abundantly clear that [renacus intended the Emperor Domitian by this reference, but
the argument above is much stronger, more widely held, and to be preferred.

17. Swete, Revelation, p. cvi. Although it should be noted that Swete comments that
Hort did not accept Bovan's argument calling for such a re-interpretation of Irenacus.

18, ANF 1311-312. The translation and introductory remarks were by Alexander
Roberts and W. H. Rambaut, according to the first edition of the translation: The Writings
of Frenaeus, vol, 1 (Edinburgh: T. & T. Clark, 1880).
