Select Bibliography I: Modem Writings 369

vols. Edinburgh: T. & T. Clark, 1911.

Momigliano, A. The Augustan Empire, 44 B, C.-A.D. 70, Cambridge
Ancient History, vol. 10. New York: Macmillan, 1930.

Mommsen, Theodor. The Provinces of the Roman Empire. London: 1886.

Morgan, Charles Herbert, et al. Studies in the Apostolic Church. New
York: Eaton and Mains, 1902.

Morris, Leon. The Gospel According to John. Grand Rapids: Eerdmans,
1971.

Morrison, W.D. The Jaws Under Roman Rule. New York: Putnam,
1890.

von Mosheim, John Laurence. History of Christianity in the Firsi Three
Centuries. New York: Converse, 1854.

Moule, C. F. D. The Birth of the New Testament. 1st ed. Cambridge:
University Press, 1962.

The Birth of the New Testament. 3rd ed. New York:
Harper & Row, 1982.

Moule, H. C. G. Studies in Colossians and Philemon. Grand Rapids:
Kregel, (1893) 1977.

. Studies in Philippians. Grand Rapids: Kregel, (1893)
1977,

Moyer, Elgin S. Whe Was Who in Church History. Chicago: Moody,
1962.

Miller, Jac J. The Epistles of Paul to the Philippians and to Philemon. New
International Commentary on the New Testament. Grand Rapids:
Eerdmans, 1955.

Muilenburg, James. The Literary Relations of the Epistle of Barnabas and
the Teaching of the Twelve Apostles. Marburg: Yale, 1921.

Myers, J. M. 1 and 2 Esdras: Introduction, Translation, and Commentary.
The Anchor Bible. Garden City, NY: Doubleday, 1974.

Nestle, Eberhard. Introduction to the Textual Criticsm of the Greek New
Testament. Translated by William Edie. London: Williams and
Norgate, 1901.

Nestle, Eberhard, Erwin Nestle, and Kurt Aland, eds. Novum Testa-
mentum Grace. 25th ed. Stuttgart: Wirtembergische Bibelanstalt,
1963.

Peters, Ted. Futures: Human and Divine. Atlanta: John Knox, 1978.

Pierce, Robert L. The Rapture Cult. Signal Mountain, TN: Signal
Point Press, 1986.

Pink, Arthur W. The Redeemer’s Return. Ashland, KY: Calvary Baptist
