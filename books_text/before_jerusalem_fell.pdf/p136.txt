122 BEFORE JERUSALEM FELL

dieck, for instance, sees verse 7 as the “principal theme” that ex-
presses”the fundamental idea . . .ofthewhole book.” Weiss views
it as “a motto for the whole book.”5 Justin A. Smith comments that
“if any one theme can be named as the absorbing and comprehensive
one in this book, it must be given to us in the words (1:7), Behold,
he cometh with clouds.’ © Of Revelation 1:7, 8, Terry observes that
“these two verses contain, first, a solemn declaration of the great
theme of the book.”’ Russell argues that this verse is “the keynote of
the Apocalypse” that “is the thesis or text of the whole.”® T. D,
Bernard in his Bampton Lectures at Oxford University calls this
verse “the first voice, and the keynote of the whole.”? Donald W.
Richardson states of this verse: “The Coming of the Lord is the
dominant note of the book.”!° Chilton concurs: “Verse 7 announces
the theme of the book.” }!

That these observations as to Revelation’s theme are correct
should be evident in the emphasis placed on His coming that is a
constant refrain in the personal letters to the Seven Churches (Rev.
2:5, 16, 25; 3:3, 11, 20) and elsewhere (Rev. 16:15; 22:7, 12, 20), As
Dissterdieck observes: “He (Christ) cometh; this is the theme of the
Apoc., which is expressed here not in indefinite generality, but di-
rectly afterwards its chief points, as they are further unfolded in the
book, are stated.”!* The thematic idea is not only introduced early
in the work (Rev. 1:7); and it not only closes it (Rev. 22:20); but it
is also presented dramatically with an attention-demanding “Be-
hold!” at its initial appearance. Clearly something of tremendous

8, Friedrich Diisterdieck, Critical and Exegetical Handbook to the Revelation of John, 3rd
od,, trans, Henry B, Jacobs (New York: Funk and Wagnalls, 1886), p. 28.
4. Ibid.
5, Bernhard Weiss,A Manual of Introduction to the New Testament, trans, A.J. K.
Davidson, vol. 2 (New York: Funk and Wagnalls, 1889), p. 71.
6, Justin A. Smith, Commentary on the Revelation, An American Commentary on the
New Testament (Valley Forge: Judson, [1884] rep.7.d.), p. 18.
7. Milton 8, Terry, Biblical Apocalyptics (New York Eaton and Mains, 1898), p. 280.
8 J. Stuart Russell, The Parousia: A Study of the Naw Testament Doctrine of Our Lord’s
‘Second Corning, 2nd ed. (Grand Rapids: Baker, [1887] 1983), p. 368.
9, Thomas Dehany Bernard, Progress of Doctrine in, the Naw Testament (Grand Rapids:
Eerdmans, [1864] 1949), p. 213.
10. Donald W. Richardson, The Relation of Jesus Christ (Richmond, VA: John Knox,
1964), p. 28,
11. David Chilton, The Days of Vengeance: An Exposition of the Book of Revelation (Ft.
Worth, TX: Dominion Press, 1987), p. 64.
12, Diisterdieck, Revelation, Dp. 105.
