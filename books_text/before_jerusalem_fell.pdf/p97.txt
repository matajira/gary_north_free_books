Clement of Alexandria 81

now as then obtain a Pope’s sanction. Nero, after Judas, becomes the
most accursed of the human race. “The first persecutor of the Church
must needs be the last, reserved by God for a final and a more awful
vengeance.”7 |

Truly, “the picture of him as the incarnation of evil triumphed as
Christianity triumphed.”7* The references to the Nero-Antichrist
designation can be found in the following: the Sibylline Oracles,
Tertullian, Lactantius, Jerome, Augustine, and Sulpicius Severus.”

The First Century Persecutions

Fourth, the persecution of Christians under Domitian (if we may
call it a persecution) was much less severe than that under Nero
— although it certainly was a tyrannical outburst.” Lightfoot speaks
of the Neronic persecution in comparison to the Domitianic thus: “the
earlier and more severe assault on the Christians [occurred] in the
latter years of the reign of Nero.”7° In fact, “early evidence is lacking
for any general religious persecution during Domitian’s reign. Though
the emperor was a violent man, his violence was directed not against
Christians or any other group but against carefully selected individu-
als whom he suspected of undermining his authority. »76 Ag Edmund-
son puts it, Domitian’s persecution was “not a general persecution
at all, but a series of isolated acts directed chiefly against a few
influential persons, including members of his own family.”7’Hort
speaks of the Domitianic persecution in contrast to the Neronic by
noting that the dramatic language of Revelation “does not fit the
short local reign of terror under Domitian. Nero affected the imagina-
tion of the world as Domitian, as far as we know, never did.”7"Late
date advocate G. E. Ladd states that “there is no evidence that during

71, Henderson, Nero, pp. 420-421,

72, Griffin, Nero, p. 15.

78, Sibyliine Oractes 5:33; 8:71; Tertullian, Apologia 5:4; Lactantius, The Deaths of the
Persecutors 2; Jerome, Daniel (at Daniel 11:28), and Dialogues 214+ Augustine, The City
of God 20:19; and Sulpicius Severus, Sacred History 2:28, 29,

7A, The evidence supportive of this will be examined more fully in Chap. 17.

75, Joseph B, Lightfoot and J. R, Harmer, eds., The Apostolic Fathers (Grand Rapids:
Baker, [1891] 1984), p. 3.

76. Glenn W. Barker, William L. Lane, and J. Ramsey Michaels, The New Testament
Speaks (New York: Harper & Row, 1969), p. 368.

77, Edmundson, Church in Rome, p. 168,

78, Hort, Apocalypse, xevi.
