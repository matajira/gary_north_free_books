342 BEFORE JERUSALEM FELL

Clearing Up a Misconception

It maybe that the following statement by House and Ice is poorly
phrased, but as it stands, it definitely leaves an erroneous impression
that needs correction:

The preterist, postmillennial viewpoint of the Christian Reconstruc-
tion movement, as expounded by David Chilton in The Days of Venge-
ance, stands or falls on whether or not the final book of the Bible was
written before A.D. 70, Fellow postmillennialist and pre-A.D, 70
preterist Kenneth L. Gentry notes this major weakness when he says,
“if it could be demonstrated that Revelation were written 25 years
after the Fall of Jerusalem, Chilton’s entire labor would go up in
smoke, *°

When they state that the particular pretenstic approach of Chil-
ton (with which I agree) stands or falls on the early date of Revela-
tion, I concur. But when they add that I note “this major weakness,”
the impression that clearly remains is that I am suggesting that the
major weakness of this preterist view of Revelation is that it has to depend
on an early date — as if I deemed the evidence for an early date as
being weak!'? Such was not the intention of my statement at all. I
was reviewing Chilton’s book, and I merely pointed out that I believe
that a major weakness of his book — not the preterist view as such — is
that it does not deal in more depth with the dating question. How-
ever, I did note that Chilton’s book is subtitled: “An Exposition of the
Book of Revelation.” |! It is an expository, not a critical, commentary.
The “major weakness” statement was regarding what Chilton left
out of his book (a thorough inquiry into the question of Revelation’s
date), not preteristic postmillennialism or early date advocacy.

The Problem of Partial Citation

In defense of Chilton, it should be noted that an imprecise
statement by House and Ice leaves the impression that Chilton has
created de nove a faulty argument for the early date of Revelation.
Their statement reads:

9. Ibid, D. 249, citing Kenneth L. Gentry, Jr., “The Days of Vengeance: A Review
Article,” The Counsel of Chalcedon (June 1987): 11.

10, I clearly state my convictions regarding the early date in the article they cite:
“Days of Vengeance,” p. 11.

11 Bid, p. 11.

 
