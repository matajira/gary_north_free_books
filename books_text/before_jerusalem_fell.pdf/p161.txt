The Identity of the Sixth King M7

in regard to this passage. The problem is that John introduces the
passage in such a way as to appear to suggest the exceeding difficulty
of the interpretation of the matter. After the vision is shown to (Rev.
17: 1) and seen by (Rev. 17:3) John, the angel speaking to him says
(v. 9a): “Here is the mind which has wisdom” (the Greek of the
statement is: Ode 6 vobc 6 Zywv oopiav. Then follows our text.
Despite the fact that there are no lexically difficult words involved,
this phrase has generated extensive debate among commentators.

We will consider the reservations of two commentators by way
of illustration of the false perceptions regarding the alleged interpre-
tive problem. Regarding the matter, dispensationalist Walvoord notes:
“The explanation of the beast introduced by the unusual phrase ‘here
is the mind which bath wisdom’ anticipates the difficulty and com-
plexity of the revelation to follow. The reader is warned that spiritual
wisdom is required to understand that which is unfolded.”] Post-
millennialist H. B. Swete urges caution on the same basis: “What is
to follow will put to the proof the spiritual discernment of the hearer
or reader.... As Arethas points out, the wisdom which is de
manded is a higher gift than ordinary intelligence. . . . The inter-
pretation now begins, but (as the reader has been warned) it is itself
an enigma, for which more than one solution may be found.”2

Despite the asseverations of these commentators, it would seem
that those who allege that the phrase introduces an ambiguity are in
essence turning the statement on its head. In point of fact, the context
is extremely clear: the express purpose of the statement is to provide
an elucidation of the matter. Let us consider the situation carefully.

In verses 1-6 of Revelation 17, one of the seven angels appears
to John for the purpose of showing him the judgment of the “great
harlot” (v. 1). When the angel “carried” him “away in the Spirit,”
John “saw” the woman on the beast (v. 3). This was a revelatory
vision-experience, such as the opening verse of Revelation indicated.
John would receive (Rev. 1:1, “signified”). By definition revelatory
visions are symbolic representations of prophetic truths or events.
The visions as such are the more difficult portions of Revelation, by

1. John F. Walvoord, The Revelation of Jesus Christ (Chicago: Moody, 1966), p. 250.
2, Henry Barclay Swete, Commentary on Revelation (Grand Rapids: Kregel, [1906]
1977), pp. 219-220.
