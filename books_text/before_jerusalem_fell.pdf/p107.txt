Additional External Witnesses 1

mouth of the beasts proceed fiery locusts, Vs. iv. 1, 6, [Rev] 9:3:
whereas the foundation stones of the Heavenly Jerusalem bear the
names of the Twelve Apostles, [Rev] 21:14, and those who overcome
are made pillars in the spiritual temple, [Rev] 3:12, in Hermas the
apostles and other teachers of the Church form the stones of the
heavenly tower erected by the archangels, Vis. iii. 5. 1. The faithful
in both are clothed in white and are given crowns to wear, [Rev] 6:11
etc,, 2: 10; 3:10; Hermas, Sire, viii, 2. 1, 3."

Westcott and Hug agree,” and Swete comments that “it is hardly too
bold to say with Bishop Westcott that ‘the symbolism of the Apoca-
lypse reappears in the Shepherd.’””°

In more recent times noted critics concur in this assessment; we
mention but a few. Patristics scholar, Edgar J. Goodspeed, states
that Hermas is “clearly acquainted with the Revelation of John.”2’
John Lawson and Guthrie agree. 28 Mounce also leans in this direc-
tion: “While such parallels [between The Shepherd and Revelation]
may indicate nothing more than that both books drew from a com-
mon apocalyptic tradition, the possibility that Hermas may have
known the Apocalypse is by no means precluded.”

If a date in the A.D. 80s be given to The Shepherd (as is most
plausible), and if the apparent allusions to Revelation in it are
expressive of its dependency upon Revelation (as certainly seems the
case), then Revelation influenced the writing of The Shepherd in the
late A.D. 80s. The Shepherd was certainly written somewhere around
Rome, for it mentions Clement (undoubtedly the Clement of Rome
because of the recognition his name is expected to carry, cf. Vision
2:4), For John’s Revelation to have been written, to have been copied
(aboriously by hand), to have made its way to Rome by the 80s, and.
to have influenced the writing of another work, would be strong

24, R. H, Charles, The Revelation of St. John, 2 vols, International Critical Commentary
(Edinburgh: T. &T. Clark, 1920) |:xcvii.

25, B.F. Westcott, A General Surey of the History of the Canon of the New Testament, 3d
ed. (London: Macmillan, 1870), p. 181; Johann Leonhard Hug, Introduction io the New
‘Testament, trans, David Fosdick, Jr. (Andover: Gould and Newman, 1836), p. 659.

26. Henry Barclay Swete, Commentary on Revelation (Grand Rapids: Kregel, [1906]
1977), p.cx. He not only mentions Westcott in this regard, but Lardner as well.

27. Goodspeed, Apostolic Fathers, p. 97.

28, Lawson, Apostolic Fathers, p. 220; Donald Guthrie, New Testament Introduction, 8d
ed, (Downers Grove, IL: Inter-Varsity Press, 1970), pp. 931-932.

29. Robert H. Mounce, The Book of Revelation. New International Commentary on the
New Testament (Grand Rapids: Eerdmans, 1977), p. 87.
