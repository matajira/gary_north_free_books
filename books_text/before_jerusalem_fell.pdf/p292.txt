The Role of Emperor Worship 279

comment Robinson retorts: “If that is not contemporary, we have
nothing.” (We will provide more on the persecution motif in Reve-
lation in the next chapter.)

Indeed, it is not until Trajan, the second emperor after Domitian,
that hard evidence supportive of a persecution based upon the legal
enforcement of the emperor cult upon Christians can be found.”
This is contained in the famous correspondence between Pliny the
Younger and the emperor Trajan regarding the proper handling of
Christianity in Bithynia, Asia Minor, in about A.D. 11393 - over a
decade later than and two emperors after Domitian. The significance
of this evidence is that by Traj an’s day “it is treated as a stock test
of loyalty.”9*

The dogmatism necessary for supporting a late date for Revela-
tion on this matter is without foundation. Peake’s reserve, as a
capable late date advocate, should be noted and applauded: He
understands Domitian’s demand for emperor worship from Chris-
tians as a cause of the persecution as merely “possible.”9*Robinson
notes that “while the evidence from the imperial cultus does not rule
out a Domitianic dating, it does not establish it either.”

Nero and the Emperor Cult

But apart from these matters, there is slight documentary evi-
dence that suggests that the Neronic persecution was related at least
in part to the imperial cult. Tacitus records the rationale for the
justification of the persecution.°’ He notes that Nero turned to the
Christians in a desperate search for a scapegoat in order to turn
suspicions for the burning of Rome from himself He chose them
because Christians, as such, were “detested” by the populace. Sue-

91, Robinson, Redating, p. 237 n, 86,

92, Ibid,, p. 286, Moyer notes that “these letters are the earliest account of Christians
to be given by pagan writers” (Elgin 8, Moyer, Who Was Who in Church History (Chicago:
Moody, 1962], p. 385). This correspondence antedates the writings of Tacitus and
Suetonius.

98, Pliny, Epistles 10:96, Fuller reference to the significance of this correspondence
will be made in the next chapter of our inguiry.

94, Robinson, Redating, p. 236, See also Merrill C, Tenney, New Testament Times
(Chicago: Moody, 1965), p. 381.

95, Peake, Revelation, p. 121,

96, Robinson, Redating, pp. 237-238.

97. Annals 15:44.
