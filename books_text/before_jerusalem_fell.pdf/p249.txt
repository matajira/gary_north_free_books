The Looming Jewish War 235

prophesied by Jesus. The Jerusalem holocaust was coming in that
generation and would render the land valueless. 1 Thessalonians 2:16
speaks of the Jews who “always fill up the measure of their sins” and
upon whom “the wrath has come... to the utmost. ” Hebrews
12:18-29 contrasts Judaism and its fulfillment, Christianity, and notes
that there is an approaching “shaking” of the old order coming.
There are many other Scriptural indications that point to something
dramatic and earth-shaking that was coming upon the world and
that would be felt in reverberations even beyond Judea.*

Thus, Revelation 7 is strongly indicative of a pre-fall Judea. After
the Jewish War “Palestine was proclaimed a Roman province, and
a great part of the land became the personal property of the emperor.
But the country was in ruins, its once flourishing towns and villages
almost without inhabitants, dogs and jackals prowling through the
devastated streets and houses. In Jerusalem, a million people are
reported to have penshed, with a hundred thousand taken captive
to glut the slave markets of the empire. “g The evidence for the
awfulness of the destruction is not based solely upon documentary
testimony from Josephus, but it is also well-evidenced archaeologi-
cally:

The recent excavations have provided striking evidence of Titus’s
destruction. ... In the destruction of these buildings, walls were
razed, paving stones torn up, and the drain clogged with material
firmly dated to the last part of the century by the pottery. In the drain
were human skulls and other bones, washed down from the ruined.
city higher up the slope.

Even more dramatic were the finds in Site N, the area in which the
fine street of Herod Agrippa was uncovered. Reference has already
been made to the collapse of the staircase leading east from the street
(p. 165). The tumble of stones was remarkable even for Jerusalem
where tumbles of stones are a phenomenon all too common in excava-
tions. The magnitude of the disaster perhaps made a special impact
owing to the excellence of the destroyed buildings as shown by the
magnificently-dressed stones, and the period of the collapse was very
precisely pin-pointed by the discovery at its base of a hoard of coins
of the First Revolt, hidden by defenders who could not recover them

8. E.g., Rem, 18:11, 12; 1 Cor, 7:26, 29-81; Col, 3:6; Heb, 10:25, 87; James. 5:8, 9;
1 Pet. 45, 7; 1 John -18,
9, Rufus Learsi, Israel: A History of the Jnvish People (New York World, 1949), p. 178.

  

 
