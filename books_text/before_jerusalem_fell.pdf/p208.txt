194 BEFORE JERUSALEM FELL

bets can be found in the Roman numeral system. In Roman numerals
the letter I possessed the numerical value of 1; V was 5; X was 10;
C was 100; D was 500; and so forth. The Greek and Hebrew lan-
guages operated similarly, although their numerical equivalents fol-
lowed the alphabetic order and employed the entire alphabet.?

Because of the two-fold use of letters as both alphabets and
numbering systems, cryptogrammic riddles were common in ancient
cultures. Cryptograms involved the adding up of the numerical val-
ues of the letters of a word, particularly a proper name.’ In Greek
these riddles were called oo wépra (‘numerical equality”); in Rab-
binic Hebrew such cryptograms were known as “gematria” (from the
Hebrew word for “mathematical”).4 By the very nature of the case
cryptograms almost invariably involved a riddle. This can be seen
in that the word very simply could have been spelled out, and also
in that any particular arithmetical value could fit a number of words
or names,

Zahn provides us an example of a cryptogram discovered in
excavations from Pompeii, which was buried by volcanic eruption in
A.D, 79. In Greek the inscription written was: PUG ij¢ dpiOpds
€ (1 love her whose number is 545”).

The name of the lover is concealed; the beloved will know it when she
recognises her name in the sum of the numerical value of the 3 letters
Ppe,ie., 545(@ = 500 + p= 40 + € = 5). But the passing stranger
does not know in the very least who the beloved is, nor does the 19th
century investigator know which of the many Greek feminine names
she bore. For he does not know how many letters there are in the
name which gives us the total of 545 when added numerically,°

2, For Greek, see W. G. Rutherford, The First Greek Grammar (London: 1935), pp.
143ff. For Hebrew see E. Kautzsch, od,, Gesenius’ Hebrew Grammar, 28th od., trans, E,
Cowley (Oxford: Clarendon, 1946), p. 80. See individual alphabetic entries in G.
Abbott-Smith, A Manual Greek Lexicon of the Nero Testament (Edinburgh: T. & T. Clark,
1987), ad. 10C.; and Joseph H, Thayer, A Greek-English Lexicon of the Naw Testament (New
‘York: American Book, 1889), ad, 100,

8, Irenaeus mentions this phenomenon in his Against Heresies §:30:1 (although this
statement is probably by a later copyist): “numbers also are expressed by letters.”

4, J, Massyngberde Ford, Ravlation. Anchor Bible (Garden City: Doubleday, 1975),
p. 228,

5, Cited in Oskar Ruble, “dpi0jéa” in Gerhard Kittel, od., Theological Dictionary of
the New Testament [TDNT], trans, Geoffrey W. Bromiley, vol. 1 (Grand Rapids: Eerd-
mans, 1964), p. 462, See also Miller Burrows, What Mean These Stones? (New Haven:
American Schools of Oriental Research, 1941), p. 270.
