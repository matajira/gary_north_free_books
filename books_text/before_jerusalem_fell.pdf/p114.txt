98 BEFORE JERUSALEM FELL

tradition teaches, condemned John, who bore testimony, on account
of the word of truth, to the isle of Patmos. John, moreover, teaches
us things respecting his testimony [i.c., martyrdom], without say’ing
who condemned him when he utters these things in the Apocalypse.
He seems also to have seen the Apocalypse . . . in the istand.”>>

Needless to say, early date advocates find the use of Origen
questionable, in that it is not at all clear that he had in mind
Domitian as “the King of the Remans.” Indeed, late date advocates
are sometimes less than convincing themselves. Swete observes of the
witness provided by Origen and Clement of Alexandria: “It will be
seen that the Alexandria testimony is not explicit; the Emperor who
banished John is not named either by Clement or Origen. But in the
absence of evidence to the contrary they may be presumed to have
followed in this respect the tradition of South Gaul and Asia Mi-
nor.”5’ Charles argues similarly: “Neither in Clement nor Origen is
Domitian’s name given, but it may be presumed that it was in the
mind of these writers.”5°

Early date proponent Hort states of this situation: “The absence
of a name in both Clement and Origen certainly does not prove that
no name was known to them. But the coincidence is curious.”
Stuart sees the absence as more than “curious” and more than merely
lacking the character of proof for late date advocacy:

This remarkable passage deserves special notice. We cannot suppose
Origen to have been ignorant of what Irenaeus said in V. 30....
Yet Origen does not at all refer to Irenaeus, as exhibiting anything
decisive with regard to which Roman emperor it was who banished
John. He does not even appeal to tradition, as according with the
report of Irenaeus. Moreover he notes expressly, that John has not
himself decided this matter in the Apocalypse. . . . If now he re-
garded the opinion of Irenaeus as decisive in relation to this subject,
how could he have failed, on such an occasion, of appealing to it?. . .
We cannot well come to any conclusion here, than that Origen knew
of no way in which this matter could be determined.

56, Ongen, Matthew 16:6, Citation can be found in Charles, Revelation 1 :xciii; Swete,
Relation, D, xeix; Stuart, Apocalypse 1:71,

57. Swete, Revelation, D. mix n, 2,

58, Charles, Revelation 1 =xcii.

59. Hort, Apocalypse, D. xv.

60. Stuart, Apocalypse 1271,272,
