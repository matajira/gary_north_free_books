38 BEFORE jrrusstem FELL

Moses Stuart, Commentary on the Apocalypse, 2 vols. (Andover: Allen,
Morrill, and Wardwell, 1845).

Swegler. [1]

Milton S. Terry, Biblical Hermeneutics (Grand Rapids: Zondervan,
[n.d.] rep. 1974), p. 467.

Thiersch, Die Kirche im apostolischen Zeitalter.[ 1]

Friedrich August Gottreu Tholuck, Commentary on the Gospel of John
(1827), [1]

Tillich, Introduction to the Naw Testament. [1]

Charles Cutler Torrey, Documents of the Primitive Church, (ch. 5); and
The Apocalypse ofJohn (New Haven: Yale, 1958).

Cornelis Vanderwaal, Hal Lindsey and Biblical Prophecy (St. Cath-
arine’s, Ontario: Paideia, 1978); and Search the Scriptures, vol. 10
(St. Catharines, Ontario: Paideia, 1979).

Gustav Volkmar, Commentar zur Offenbarung (Zurich: 1862). [3]

Foy E. Wallace, Jr., The Book of Revelation (Nashville: by the author,
1966) .

Arthur Weigall, Nero: Emperor of Rome (London: Thornton Butter-
worth, 1930).

Bernhard Weiss, A Commentary on the New Testament, 2nd cd., trans. G.
H. Schodde and E. Wilson (NY: Funk and Wagnalls, 1906), vol. 4.

Brooke Foss Westcott, Tze Gospel According to St. John (Grand Rapids:
Eerdmans, [1882] 1954).

J.J. Wetstein, New Testament Graecum, vol. 2 (Amsterdam: 1752).

Karl Wieseler, Zur Auslegung und Kritik der Apok. Literatur (Gottingen:
1839) ©

Charles Wordsworth, The New Testament, vol. 2(London: 1864).

Herbert B. Workman, Persecution in the Early Church (London: Oxford,
[1906] 1980).

Robert Young, Commentary on the Book of Revelation (1885); and Concise
Critical Comments on the Holy Bible (London: Pickering & Inglis,
n.d.), p. 179.

C.F. J. Ziillig, Die Offenbarung Johannis erklarten (Stuttgart: 1852). [3,
6]

60, Cited in Hayes, John, p. 246
