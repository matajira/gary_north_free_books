17

THE PERSECUTION
OF CHRISTIANITY

Another argument prevalently employed by late date advocates
is that which, as Morris notes, discovers “indications that Revelation
was written in a time of persecution” — a persecution that accords
“much better with Domitian. ”' Both Morris and Guthrie list this as
their second arguments for the A-D. 95-96 date; Kimmel cites it first.
This line of reasoning is given considerable attention by many mod-
ern late date scholars, including Morris, Guthrie, Kimmel, Mounce,
Barnes, Hendrickson, and Beasley-Murray, for example.* Kimmel
is quite confident that “the temporal scene which Rev. sketches fits
no epoch of primitive Christianity so well as the time of the persecu-
tion under Domitian. 73

It is indisputably clear from the perspective of Revelation’s self-
witness that imperial persecution against the faith has begun. We
will cite two clear samples of references to this persecution by way of

1, Leon Morris, The Revelation of St John (Grand Rapids: Eerdmans, 1969), p. 36,
See also James Moffatt, The Revelation of St, John the Divine, in W. R, Nicoll, ed.,
Englishman's Greek Téstament, vol, § (Grand Rapids: Eerdmans, rep, 1980), pp. 817-820
(though his approach is much different, op. p. 818). R, H. Charles, The Revelation of St,
John, 2 vols. International Critical Commentary (Edinburgh: T, & T. Clark, 1920)
L:xciv-xev. Robert H. Mounce, The Book of Rexelation. New International Commentary
on the New Testament, pp. 38-84, Donald Guthrie, New Testament Introduction, 8rd od.
(Downers Grove, IL: Inter-Varsity Press, 1970), pp. 951-958. Werner Georg Kimmel,
Introduction to the New Testament, 17th ed., trans, Howard ©, Kee (Nashville: Abindgon,
1979), p. 467.

2, Morris, Revelation, p. 36; Guthrie, Introduction, p, 951; Mounce, Revelation, p. 38;
Kimmel, Introduction, p. 467; Albert Barnes, Barns’ Notes on Naw Testament, 1 vol, od.
(Grand Rapids: Kregel, rep. 1962) p. 1582; William Hendriksen, More Than Conquerors
(Grand Rapids: Baker, 1967), p. 20; G. R. Beasley-Murray, “Revelation,” in Francis
Davidson, od,, New Bible Commentary (Grand Rapids: Eerdmans, 1954), p, 1168,

8, Kimmel, Jntroduciion, p, 468,

 

285
