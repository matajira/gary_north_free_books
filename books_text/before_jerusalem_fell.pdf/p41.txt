24 BEFORE JERUSALEM FELL

to exclude the possibility that the extant book is a second edition of
an earlier work, or that it incorporates earlier materials.” '

As tempting as delving into this question is, we will by-pass it,
with only occasional reference in later portions of this study. The
reasons for by-passing this particular matter are not merely mechani-
cal; that is, they are not totally related to the difficulty of the topic
or the bulk of research that would be generated herein (although the
latter consideration is certainly legitimate). Rather the rationale for
omitting discussion of the matter is more significant and is of a
theological nature. The primary reason for its exclusion is due to the
obvious difficulty of maintaining the composite and discordant nature
of Revelation while defending its canonicity and its revelational qual-
ity. How can we maintain a coherent theory of Revelation’s inspira-
tion if it has gone through several editions under several different
hands? The problem is virtually the same with the more familiar
questions related to such books as the Pentateuch and Isaiah, for
instance. This is why almost invariably those who have argued for its
composite nature are of the liberal school of thought. A secondary
reason is due to the intention of the present writer. This treatise is
written with an eye not to the liberal theologian, but to the conserva-
tive. The plea for a hearing in this research project is toward conser-
vative theologians who stand with the author on the fundamental
theological issues, such as the inspiration and inerrancy of Scripture.
The debate engaged is an “intramural” debate among evangelical.

Survey of Scholarly Opinion

In virtually all of the popular literature on Revelation and in
much of that which is more scholarly, the assumption often is that
informed scholarship unanimously demands a late date for Revelation.
The impression, if not the actual intent, is given that a scholar’s
adherence to an early date for Revelation is due either to an ostrich-
like avoidance of the facts or to his not being abreast of the literature.
For example, Barclay M. Newman, Jr., states: “Among present-day
New Testament scholars it is almost unanimously agreed that the
book of Revelation was written at a period late in the first century,
when the churches of Asia Minor were undergoing persecution by

19, Swete, Revelation, p, eiv. It should be noted that Swete opts for the Johannine
authorship as the most preferable, See above comments,
