12
THE ROLE OF NERO CAESAR

In an earlier section we demonstrated that the reference to the
seven kings in Revelation 17 indicated that the sixth king was pres-
ently ruling when John wrote the book. There we showed that the
sixth king must have been Nero Caesar, in that he was the sixth
emperor of the Roman Empire. At this point we turn to a further
consideration of evidences of Nero’s appearance in Revelation.

The Gematria "666"

One of the best known features of Revelation among the general
Christian populace today is also one of its most misunderstood. That
feature is the gematria riddle in Revelation 13.! There is a widespread
awareness of and interest in this intriguing passage of Revelation
13:18, which says: “Here is wisdom. Let him who has understanding
calculate the number of the beast, for the number is that of a man;
and his number is six hundred and sixty-six. ” In order to gain a
proper conception of this verse, a little historical and cultural back-
ground will be necessary.

Ancient Numerical Riddles

In ancient days alphabets served a two-fold purpose. Their first
and foremost design was, of course, their service as letters from which
words were composed in written communication. But in the second
place, letters were also assigned numerical values and thus served as
numerals. The most familiar example of this dual function of alpha-

1, Mounce suggests that “no verse in Revelation has received more attention than
this one with its cryptic reference to the number of the beast” (Robert H, Mounce, The
Book of Revelation. New Intemational Commentary on the New Testament [Grand Rapids:
Eerdmans, 19771, p. 263).

193
