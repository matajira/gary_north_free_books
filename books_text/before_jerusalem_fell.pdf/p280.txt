The Role of Emperor Worship 267

western provinces, so that Philo could say, that everywhere honors
were decreed to him equal to those of the Olympian gods. “3°

In one respect Octavian had long been unique: since 42 B.C. and the
consecrations of Divus Julius he had been the son of a god, “Divi
filius.” After Actium his birthday was celebrated as a public holiday;
libations were poured in his honour at public and private banquets;
from 29 B.C. his name was added to those of the gods in hymns; two
years later he received the title of Augustus; his Genius, perhaps in
12 B. C., was inserted in official oaths between the names of Juppiter
and the Di Penates; in A.D. 13 an altar was dedicated by Tiberius in
Rome to the Numen Augusti.®

Accordingly Suetonius noted of the emperor Claudius that he used.
as “his most sacred and frequent oath ‘By Augustus. “3°

Interestingly, late date advocate Moffatt has an excellent sum-
mary of the cult as it existed in focus on Augustus:

Since the days of Augustus, the emperor had been viewed as the
guardian and genius of the empire, responsible for its welfare and
consequently worthy of its veneration. It was a convenient method of
concentrating and expressing loyalty, to acknowledge him as entitled
to the prestige of a certain sanctity, even during his lifetime. . . . Its
political convenience, however, lent it increasing momentum. Gradu-
ally, on the worship of the Lares Augusti in Italy and the capital...
and on the association of the imperial cultus with that of dea Romain
whom a temple had been erected at Smyrna as far back as 195 B.C.),
the new canonisation rose to its height, never jealous of local cults,
but thriving by means of its adaptability to the religious syncretism
of the age. It was the religious sanction of the new imperialism. It had.
temples, sacrifices, choirs (as at Smyrna), and even a priesthood (the
“Socales Augustales”) of its own.

For obvious reasons the cult flourished luxuriantly in the prov-
inces, particularly in Asia Minor, where the emperor was often re-
garded as an incarnation of the local god or named before him... .
The cultus, attaching itself like mistletoe to institutions and local rites
alike, shot up profusely; polytheism found little trouble in admitting
the emperor to a place beside the gods, and occasionally, as in the
case of Augustus and Apollo, or of Domitian and Zeus, “the emperor

82, Beckwith, Apocalypse, p. 198,
88, Scullard, Gracchi fo Nero, p. 242.
84, Claudiua, 11,
