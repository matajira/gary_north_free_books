70 BEFORE JERUSALEM FELL

which his irregular birth foretold, by a youth made unhappy by
lameness, a lifetime passed amidst warfare and ever exposed to the
approach of death, by the misfortune caused to the world by his whole
progeny but especially due to his two daughters who became the
mothers of the emperors Gaius Caligula and Domitius Nero, the two
firebrands of mankind. ... Nero also, who was emperor shortly
before and whose entire rule showed him the enemy of mankind.’

Apollonius of Tyana (b. 4 B.C.) says that Nero was “commonly
called a Tyrant”: “In my travels, which have been wider than ever
man yet accomplished, I have seen many, many wild beasts of Arabia
and India; but this beast, that is commonly called a Tyrant, I know
not how many heads it has, nor if it be crooked of claw, and armed
with horrible fangs. ... And of wild beasts you cannot say that
they were ever known to eat their own mothers, but Nero has gorged
himself on this diet.”8

Roman historian Tacitus (A.D. 56-117) spoke of Nero’s “cruel
nature”? that “put to death so many innocent men.” 10 He records a
senate speech that discussed the wrongs of Tiberius and Gaius,
noting that “Nero arose more implacable and more cruel” and that
the senate under Nero “had been cut down.” !! Suetonius (A.D.
70-130) speaks of Nero’s “cruelty of disposition” evidencing itself at
an early age.'? He documents Nero’s evil and states: “Neither dis-
crimination nor moderation [were employed] in putting to death
whomsoever he pleased on any pretext whatever.” ' Juvenal(c, A.D.
60- 138) speaks of “Nero’s cruel and bloody tyranny.” * He laments
Nero’s heinous sexual exploits with handsome young men: “No mis-
shapen youth was ever unsexed by cruel tyrant in his castle; never
did Nero have a bandy-legged or scrofulous favourite, or one that
was hump-backed or pot-bellied!”

7, Pliny, Natural History 7:45.

8. Philostratus, Lif of Apollonius 438, Cited in John A. T. Robinson, Redating the New
‘Testament (Philadelphia: Westminster, 1976), p. 285, from J. 8, Phillimore (Oxford, 1912)
2:38.

9. Tacitus, Histories 4:3.

10. Hid, 4:7.

11. Ibid, 442,

22, Suetonius, Nero 7:1
18, ibid. 37:1.

14, Juvenal, Satires 7:25
15, Satires 10:306ff.
