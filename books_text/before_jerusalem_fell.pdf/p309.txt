296 BEFORE JERUSALEM FELL

Hort concurs: “The whole language about Rome and the empire,
Babylon and the Beast, fit the last days of Nero and the time
immediately following, and does not fit the short local reign of terror
under Domitian. Nero affected the imagination of the world as Domi-
tian, as far as we know, never did.”4° The gruesome cruelty of Nero’s
persecution has already been noted from Tacitus: Christians were
“wrapped in the hides of wild beasts, they were torn to pieces by
dogs” and were “fastened to crosses to be set on fire.”

Thus, the sheer magnitude and the extreme cruelty of Nero’s
persecution of Christianity are most suggestive of its suitability for
fulfillment of the role required in Revelation. Athough the debate is
involved and inconclusive “there is some reason to believe that there
was actual legislation against Christians in Rome under Nero. 750
Demonstration of this fact, however, is not necessary to establishing
our argument.

48, Hort, Apocalypse, p. XVI,

49, Tacitus, Annals 15:44.

50. C.F, D, Moule, The Birth of the New Testament, 8rd ed. (New York: Harper &
Row, 1982), p. 154, Though there has been intense debate as to the question of Nero's
persocution’s basis in legislative action, there is good evidence to suggest it was 50: (1)
‘Tertullian speaks of the “Neronian institution,” Afol. 5:3; Sulpicius Severus indicates the
same, Chron, 11:29:8, (2) Suetonius strongly implies such, Nero 16, (2) 1 Pet. 415 is more
easily understock in such a situation. See especially Jules Lebreton and Jacques Zciller,
History of the Prinztive Church, trans, Ernest C. Measenger, vol. 1 (New York: Macmillan,
1949), pp. 874-881, See also Moule, Birk of Naw Testament, pp, 154; and John A, T.
Robinson, Redating the New Testament (Philadelphia: Westminster Press, 1976), pp. 234ff

Many of the passages that the persecution is declared to exist in probably refer to
either the Jewish persecution of Christianity or to the Roman overthrow of Jerusalem,
according to a number of early date advocates, including the present writer.

Others who argued that the legal proscription of Christianity was as early as under
Nero's latter reign include:

8, Angus, “Roman Empire," Integration Standard Bible Encyclopedia {1SBE] (Grand
Rapids: Eerdmans, 1915)4:2607. Angus cites Mommsen and Sanday as adherents.

E, G, Hardy, Christianity and Roman Government (New York Burt Franklin, ‘te7t]
1894), p. 77.

J. L, Ratton, The Apocalypse of St. John (London: R, & T. Washbourne, 1912), p. 14.

J, Stevenson, od., A New Eusebius: Documents Mlustrative of the History of the Church to A.D.
337 (New York: Macmillan, 1957), p. 8.

Edmundson, Church in Rome, pp. 125

Peake, Revelation, p. 111,

Workman, Persecution, DD. 208

Henderson held this view and cited the following authors: B, Aubé, Gaston Boissier,
Theodor Keim, J. B. Bury, Charles Menvale, F. W. Farrar, Henry Fumeaux, A. H.
Raabe, Ernest Renan, and Pierre Batiffol; Henderson, Nero, p, 485,
