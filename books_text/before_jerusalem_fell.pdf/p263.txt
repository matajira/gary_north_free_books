The Looming Jewish War 249

ity and iniquity of Jerusalem during these final days. The cruelty
especially of the seditious leaders of the revolt (the sicarii, or zealots)
increased rapidly as the final pall of doom settled over the exhausted,
terrified, starving, dying, and doomed masses:

The madness of the seditious did also increase together with their
famine, and both those miseries were every day inflamed more and
more. ...”

It is therefore impossible to go distinctly over every instance of these
men’s iniquity. I shall therefore speak my mind here at once
briefly: - That neither did any other city ever suffer such miseries,
nor did any age ever breed a generation more fruitful in wickedness
than this was from the beginning of the world. .. .‘1

And here I cannot but speak my mind, and what the concern I am
under dictates to me, and it is this: — I suppose that had the Remans
made any longer delay in coming against those villains, the city would
either have been swallowed up by the ground opening upon them, or
been overflowed by water, or else been destroyed by such thunder as
the country of Sodom perished by, for it had brought forth a genera-
tion of men much more atheistical than were those that suffered such
punishments; for by their madness it was that all the people came to
be destroyed .52

Surely such barbarous conduct against their own families and
friends is evidence of the fulfillment of Jesus’ prophecy of covenantal
curse in Matthew 12:40.53 Had not Jesus spoken to the leaders of the
Jews and said they were of their father the devil (John 8:44)? Stier is
not amiss in his summary of the condition of the Jews who set
themselves “against the Lord and His anointed” (Acts 4:25ff) in the
first century: “In the period between the ascension of Christ and the
destruction of Jerusalem, this nation shows itself, one might say, as
if possessed by seven thousand devils. “5‘This condition became even
more dramatically evident in the final days of the defense of Jerusa-
Jem, as Henderson rightly observed: “Meanwhile that unhappy city
during all this year of grace had been prey to the most bloody

  

1:4-5; 52124
54, In Reden Jew 2:187, Cited in Russell, Parcusia, p, 412n.

 
