404

Music, 273.

Mystery, 11, 105, 146, 148.

Mystery of iniquity, 80.

Mythology), 14,25, 802n, 808, 804,806,
815.

Nation(s), 6, 71n, 128, 148, 148, 165,
171, 174, 176, 176, 180, 184,222, 288,
242,249,250, 258n, 276,

Nazarene, 226,228.

Neapolitanus, 250.

Near, (See: Revelation - expectation).

Nero (emperor) (See aéso: Beast), 15, 18,
26,28, 78, 79,84-85,99, 106, 108,
116, 182, 153, 167, 193-219, 230,240,
270,298,294 835,852,
adoptive name, 104,

Apollo fascination of, 271-275.

appearance, 217.

beast (See: Beast),

birth/childhood, 215-216,217.

burning of Rome and (See: Rome —
burning of).

character, 69-88, 160, 172, 195, 198,
209,212-215,275,805.

death, 23,50,55,60, 74, 77,80, 144,
219,241,242,254-255, 295,300,
802,805>306,311,818, 816,326,

emperor, 77, 158, 160, 164, 199, 251,
252,274,288,296,299, 306,827,
335,847,850.

family, 69-70, 75, 78,218,216,217,
272,

god, 75, 77, 78, 207.

Jobn and, 54,99, 104,

legend regarding (Redivious), 74-77,
218,260,300-317,

life, 69f7,271f, 806,

Lucius Domitivs Ahenobarbus.

matricide, 70, 78, 75, 76, 78, 195, 218,
214,276.

pretender, 75-76, 180,

Redivivus (See: Nero — legend.
regarding),

Quinguennizon Neronis, 271,

revival (See ads: Nero — Legend.

BEFORE JERUSALEM FELL

regarding).
worship of, 264, 270-284,
Nerva, 56, 77, 160.
New Covenant (See alio: Covenant), 148,
174,280.
New Israel, 181,221, 228, 227, 230,
New Testament, x, 4, 11, 13, 14, 17, 18,
21,22,24,64,66, 106, 114, 119, 128,
125, 127, 155, 166, 170, 178, 178, 182,
188,208,200,210, 226n, 227n, 234,
288,824,827,838,
dating of, 20,25,27-28, 166-167, 181,
182,287,841,
Nicolaitan(s), 61, 329m, 380n,
Number(s) (Gee aéso: Alphabet;
Gematria; Six hundred, sixty-six),10,
168, 198-196,204,215,224, 238,254,

Oath, 266,267.

Occult, 216.

Octavian (Bee: Augustus).

Offerings, 176,850.

Oil (burning oil), 54,95,96-97, 105.

Old Covenant, (See alao: Covenant), 144,
14,

Old Testament, 14n, 17,29, 117, 128,
170, 185, 196,220,

Olivet Discourse, 135n, 175,288, 849.
omitted in John’s Gospel, 130-181, 242,

2420,

Olympian, 267,269,275.

Omen, 808.

One hundred, forty-four thousand, 168,
174n, 282,

One thousand (See: Thousand).
Orthodoxy, 5,7, 17, 18,21, 28n, 24,26,
27, 28,59,60, 118, 114, 188, 136n,

142, 145, 168,182n, 210, 226n, 238,
245,288,802,815,820, 888.

Otho (emperor), 119n, 144, 158, 157,
158, 160, 161, 191, 315,351.
admiration of Nero, 808-809.

Palace/castle, 61, 70, 72,218,274.
Palestine, 101, 155,211, 226n, 285, 287,
252,
