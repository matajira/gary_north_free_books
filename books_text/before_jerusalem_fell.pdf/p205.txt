The Contemporary Integrity of the Temple 191

away, and Otho, and Galba, and Vitellius. And Vespasian rose to
the supreme power, and destroyed Jerusalem, and desolated the holy
place. And that such are the facts of the case, is clear to him that is
able to understand, as the prophet [ie., Daniel] said.” He mentions
it again several pages later in the same book and chapter, again
relating it to Daniel’s prophecy.’ Thus, Clement ties the fall of
Jerusalem to God’s divine intervention in judgment upon Israel by
prophetic decree.

In Miscellanies 4:15 he quotes The Preaching of Peter, which ties the
fall of Jerusalem into the rejection of Christ by the Jews: “Whence
also Peter, in his Preaching, speaking of the apostles, says: But we,
unrolling the books of the prophets which we possess, who name
Jesus Christ, partly in parables, partly in enigmas, partly expressly
and in so many words, find His coming and death, and cross, and all
the rest of the tortures which the Jews inflicted on Him, and His
resurrection and assumption to heaven previous to the capture of
Jerusalem. As it is written, These things are all that He behooves to suffer,
and what should be afier Him. Recognizing them, therefore, we have
believed in God in consequence of what is written respecting Him’ ””
In quoting this earlier work, Clement provides a double indication
of the significance of the fall of Jerusalem, his own and that from
Peter’s Preaching.

Other early references to Jerusalem’s fall include the following:

Tertullian (d. 220):
Apology, chapter 21 (ANF 3:34}, chapter 26 (ANF 3:40);
An Answer to the Jews, chapter 3 (ANF 3: 154), chapter 8 (ANF
158ff.), chapter 18 (ANF 3:168ff.);
Against Marcion 3:23 (ANF 3:341{f.), 439 (ANF 3:415ff.).

The Recognitions of Clement (dated c, pre-211) 1081:44 (ANF 8:94).

The Clementine Homilies (dated c. first part of third century) 1093:15
(ANF 8:241).

Lactantius (A.D. 260-330):
The Divine Institutes 421 (ANF 7:123-124);
The Epitome of the Divine Institutes 46 (ANF 7.241).

106. ANF 2:334.

107, ANF 2:510.

108, Thomas Smith, “Recognitions of Clement,” in ANF 874,

109, M. B, Riddle, “Introductory Notice to the Pseudo-Clementine Literature” in
ANF 8:70,
