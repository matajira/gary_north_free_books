The Role of Nero Caesar 209

represented by the letters of the name Jésous, the Greek name ‘Jesus’,
it comes to 888, Each digit is one more than seven, the perfect
number. But 666 yields the opposite phenomenon, for each digit falls
short. The number may be meant to indicate not an individual, but
a persistent falling short. ”©? He knows that Jesus is an historic
individual and that His name is symbolic, too. Does not Nero become
typical of the antichrist in Christian history, largely due to his being
the first of the secular persecutors of Christianity? Though he is a
specific individual, he also becomes a symbol of Rome’s persecuting
wrath, as in the Ascension of Isaiah 4:1 ff and the Sibylline Oracles
8:65ff. Bo Reicke even suggests that 666 became a political slogan
used for the cruel and tyrannical persecution introduced by Nero.”

The Hebrew Spelling Problem

The third objection to the Nero referent is that Nero’s name is
precluded on the grounds that (a) John writes to Gentile churches,
which suggests the need for using Greek letters, and (b) the process
of the deriving of the name “Nero” from “666” requires too many
elaborate intricacies. This is the second most substantial argument
against the Nero theory. Careful reflection upon this objection, how-
ever, dispels its force, especially when we consider it in the light of
the positive evidence set forth heretofore in its favor.

First, although John wrote in Greek, Revelation has long been
recognized as one of the more “Jewish” books of the New Testament.
“More than any other book in the New Testament, the Apocalypse
of John shows a Jewish cast. ”’! Indeed, one of the arguments that
historically has been granted the most weight for its early date (as
per Westcott and Hort) is that its language is so intensely Hebraic
in comparison to the Gospel’s smoother Greek. Harendberg, Bolton,
Torrey, and others suggest an Aramaic original for Revelation be-
cause of this .”In Charles’s introduction to Revelation, he included

69, Morris, Revelation, p. 174.

70, See reference in Sweet, Ravlation, p. 218n.

71, Gustav Kruger, History of Early Christian Literature in the Firet Three Centuries, trans,
C. R, Gillett (London: Macmillan, 1897), p. 35.

72, See diseussion in Bernhard Weiss, A Manual of Introduction to tf w New Testament,
vol, 2, trans. A. J. K. Davidson (New York: Funk and Wagnalls, 1889) p. 75; Torrey,
Apocalypse, pp. x, 27-58; Werner Georg Kiimmel, Introduction to the New Testament, 17th
ed., trans. Howard C. Kee (Nashville Abingdon, 1978), p. 465; J. Schmid, in Theologische
Revue 62 (1966): 306.
