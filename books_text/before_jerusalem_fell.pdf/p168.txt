154 BEFORE JERUSALEM FELL

view first. Then we will comment upon both the objections to the
above proposed view and the deficiencies of the opposing views.

It is true that the Roman empire was officially established as an
empire under Augustus, and that there are some scattered lists of the
emperors that seem to begin the enumeration with Augustus. Never-
theless, it seems patent that the enumeration of the “kings” should
most logically begin with Julius Caesar. As Stuart observed: “At
most, only an occasional beginning of the count with Augustus can
be shown, in classic authors. The almost universal usage is against
it »26

For instance, as we consider TacitUs’s statements in Annals 1:127
and Histories 1:1,28 we discover that in regard to information relevant
to our inquiry he really only states two things of consequence regard-
ing Augustus as emperor. One is that Julius refused to be called
“king,” while Augustus accepted such a designation. The other is
that the empire was established on an uninterrupted foundation with
Augustus (upon Julius’s death the empire was involved in a power
struggle for twelve years). Here, then, we do not have a denial of
Julius’s role as the first “king” of the empire at all. Neither do we
have a denial of his role as the first ruler of what shortly would
become the Roman Empire.

The same is true of the statement of Aurelius Victor (4th century)
in his Abbreviated History of the Caesars. He, too, speaks of the uninier-
rupted state of rule in Rome. In his Epitome (1:1) is another example
of the idea of permanency, along with formal usage of the titles
Imperator and Augustus. Nothing he writes precludes the understand-
ing that Julius was the first of the Roman Emperors. Other such
references are much later than even Victor, and are thus too far
beyond the era in which John wrote to be of much value. The
determination should be based upon relatively contemporaneous
authorities current in his day.

As a matter of historical fact, we must note that Julius did claim

26, Stuart, Apocalypse 2:276.

27, Annals 1:1 states: “Neither Cinna nor Sulla created a lasting despotiam: Pompey
and Crassus quickly forfeited their power to Caesar, and Lepidus and An tony their
swords to Augustus, who, under thestyle of Prince’ gathered beneath his empire a world
outworn by civil broils.”

28, Histories 1:1 notes: “After the battle of Actium, when the interests of peace required
that all power should be concentrated in the hands of one man. . . “
