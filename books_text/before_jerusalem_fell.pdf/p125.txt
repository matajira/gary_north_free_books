Additional External Witnesses 109

and Origen, the second and third most significant witnesses to the
Domitianic date, are more in the mind of the modern reader than in
the script of the ancient texts. The important references from both of
these two fathers not only lack the name “Domitian,” but are more
easily understandable if dealing with Nero. In the case of Clement
particularly, it would appear that a Neronic date would be de
manded, and not simply suggested. That these two witnesses were
ever deemed notable examples of the late date witness is quite
remarkable. Andreas clearly supports a Domitianic banishment, but
in doing so he must debate a plurality of competing exegetes prior
to and during his own era who hold to a Neronic date. Victorious is
a sure witness, but alone in unambiguous testimony among the major
references.

There are some witnesses that may hint at a pre-A.D. 70 dating
for Revelation, such as The Shepherd of Hermas and Papias. Yet,
other sources are even more suggestive of a Neronic banishment: the
Muratorian Canon, Tertullian, and Epiphanies. Others seem to
imply 40th dates for John’s banishment: Eusebius (cf. Ecclesiastical
History with Evangelical Demonstrations) and Jerome. These at least
suggest either an early competition between theories, or a double
banishment of John, once under Nero and later under Domitian.

On the other hand, undeniably supportive of a Neronic date are
Arethas, the Syriac History of John, the Syriac versions of Revelation,
and Theophylact.

Obviously, then, there was no sure, uniform, and certain tradi-
tion in the early centuries of the Church on this matter. All that is
certain is that John was banished to Patmos and there wrote Revela-
tion. In the matter of details, there is confusion and contradiction
that betrays the possibility of various hypotheses floating about,
rather than firm convictions. This is possibly why neither Clement.
of Alexandria nor Origen ventured to explicitly name the emperor
of the banishment. They surely knew of Irenaeus’s statements, yet
they neglected to refer to them on this matter. All things considered,
however, even the external evidence leans toward a Neronic date.
