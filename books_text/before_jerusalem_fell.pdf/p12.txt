xii BEFORE JERUSALEM FELL

ments was fulfilled in A.D. 70. There are therefore no major eschato-
logical discontinuities ahead of us except the conversion of the Jews
(em. 11) and the final judgment (Rev. 20). Therefore, neither the
church nor living Christians will be delivered from this world until
the final judgment. The so-called Rapture will come only at the end
of history. There is no “great escape” ahead. This interpretation of
Bible prophecy especially appalls dispensational premillennialists.
They want their great escape."

The amillennialists are unhappy with the book for a different
reason. They affirm preterism’s view of the future’s continuity — on
this point, they stand with the preterists against premillennialism — but
they reject the postmillennial optimism of Chilton’s book. If preter-
ism is true, then most of the prophesied negative sanctions in history
are over. Covenant theology teaches that there are positive and
negative sanctions in history. If the prophesied (ie., inevitable) nega-
tive sanctions are behind us, then the church has no legitimate
eschatological reason not to expect God’s positive sanctions in history
in response to the preaching of the gospel. There is no legitimate
eschatological reason not to affirm the possibility of the progressive
sanctification of individual Christians and the institutions that they
influence or legally control. But amillennialism has always preached.
a continuity of external defeat for the church and for the gospel
generally. The victories of Christianity are said to be limited to the
hearts of converts to Christianity, their families, and a progressively
besieged institutional church. Amillennialism’s continuity is the con-
tinuity of the prayer group in a concentration camp; worse: a sen-
tence with no possibility of parole.

8, Daye Hunt, Whatever Happened to Heaven? (Eugene, Oregon: Harvest House, 1988),

9. I realize that certain defenders of amillennialism like to refer to themselves as
“optimistic amillennialists.* I had not heard this term before R. J. Rushdoony began to
publish his postmillennial works, I think the postmillennialists’ legitimate monopoliza-
tion of the vision of earthly eschatological optimism has embarrassed their opponents,
What must be understood from the beginning is that there has never been so much as
an article outlining what this optimistic amillennial theology would look like, let alone a
systematic theology. There has been no published Protestant amillennial theologian in
four centuries who has presented anything but a pessimistic view of the fucure with
respect to the inevitable cultural triumph of unbelief. It is my suspicion that any
“optimistic amillennial” system would simply be a variety of postmillennialism. [ believe
that the term “optimistic amillennialist” "peters to a postmillennialist who for employment
constraints or time constraints — it takes time to rethink one’s theology — prefers not
to use the word “postmillennial” to describe his eschatology.
