310 BEFORE JERUSALEM FELL
Nero’s spirit and making all the priests and people attend.

Rome as “Nero Redivivus”

An even more compelling view, however, is available to the
interpreter, one that is certainly to be preferred above either the Nero
Redivivus or the approach just mentioned. The present writer is
convinced that an extremely strong case can be made for an interpre-
tation that meets all the requirements of the case and avoids the
potentially rocky shoals of the implementation of a legend. The
interpretation to be given is most aprepos, not only in regard to one
of the major events of the first century, but also to the theme of
Revelation.

As we set forth this interpretation, it will be necessary to recall
that John allows some shifting in his imagery of the Beast: the
seven-headed Beast is here conceived generically as the Roman Em-
pire, there specifically as one particular emperor. It is impossible to
lock down the Beast imagery to either one referent or the other.? At
some places the Beast has seven-heads that are seven kings collec-
tively considered (Rev. 13: 1; Rev. 17:3, 9-10). Thus, he is generically
portrayed as a kingdom with seven kings that arise in chronological
succession (cf. Rev. 17:10- 11). But then again in the very same
contexts the Beast is spoken of as an individual (Rev. 13:18), as but
one head among the seven (Rev. 17: 11). This feature, as frustrating
as it may be, is recognized by many commentators. It has already
been demonstrated that the sixth head (Rev. 17: 10) that received the
mortal wound (Rev. 13:1, 3) with a sword (Rev. 13:10, 14) and that
was mysteriously numbered “666” (Rev. 13: 18) is Nero Caesar, the
sixth emperor of Rome who died by a sword from his own hand.”

Recognizing this shifting referent takes one a long way toward.

89, It is very interesting to note a related and remarkable feature in the Johannine
methodology, John frequently gets his point across with double-meaning terms, Under
his brief discussion of “Johannine Theology” Gundry writes of John's record of Jesus’
teaching that the words “often carry second and even third meanings, ‘Born again (or
anew) also means ‘born from above’ (3:34), and the reference to Jesus’ being ‘lifted up"
points not only to the method of His execution, but also to His resurrection and
exaltation back to heaven (12:20-86, especially $2) “ For an interesting discussion of this
feature of John’s style see Leon Morns, The Gospel According to John (Grand Rapids
Eerdmans, 1971), “Introduction,” and ad, 26C.

4s), Chap. 10,
