7

THE ROLE OF
INTERNAL EVIDENCE

We come now, at last, to the presentation of the major arguments
for the early date of Revelation. The evidences analyzed herein
should be considered the fundamental arguments of early date advo-
cacy. Following the presentation of the positive internal evidence,
will be given an analysis of and rebuttal to the four leading objections
to the early date from the internal evidence.

The Significance of Internal Evidence

As observed previously, it has often been the case that the exter-
nal witness to Revelation’s date has been a major — perhaps the
major — stumbling block to the acceptance of an early date. Hence,
our lengthy survey and analysis of the external evidence. Working
from biblical presuppositions as to the nature and integrity of Scrip-
ture, the convictions of orthodox, conservative Christianity must
recognize that the essential and determinative evidence ought to be
drawn from the internal testimony of the scriptural record itself, when
it is available. In this regard, the argument put forward by Ned B.
Stonehouse for a change of terminology in the field of Biblical Intro-
duction is very much to the point before us (even though his original
considerations were with questions related to the Synoptic Problem).
Stonehouse calls for an abandonment of the internal/external no-
menclature in the field in favor of a self-witness/tradition distinction:

Tn using the terms “tradition” and “self-witness,” it may be well to
point out, I am deliberately abandoning the older terminology em-
ployed in my undergraduate course of studies, namely, “external and
internal evidence.” Since I have exactly the same contents in view in
my distinctions as my teachers had in theirs, the difference being
pointed up hardly involves a serious dispute with them. There is

118
