214 BEFORE JERUSALEM FELL

hides of wild beasts, they were torn to pieces by dogs, or fastened to
crosses to be set on fire, that when the darkness fell they might be
burned to illuminate the night... . Whence it came about that,
though the victims were guilty and deserved the most exemplary
punishment, a sense of pity was aroused by the feeling that they were
sacrificed not on the altar of public interest, but to satisfy the cruelty
of one man.

Apollonius of Tyana (b. 4 B. C.) specifically called Nero a “beast”:
“Tn my travels, which have been wider than ever man yet accom-
plished, I have seen many, many wild beasts of Arabia and India;
but this beast, that is commonly called a Tyrant, I know not how
many heads it has, nor if it be crooked of claw, and armed with
horrible fangs... . And of wild beasts you cannot say that they
were ever known to eat their own mother, but Nero has gorged
himself on this diet.”™ It is important to understand that “the context
shows that he is thinking of a beast of prey with claws and teeth, a
carnivorous animal, like a lion or panther.”9*In Sibylline oracles
8:157 (dated about A.D.175)™ Nero is fearfully designated a “great
beast” (@rjp péyac). In this section of the Oracles we read “then
dark blood will pursue the great beast.”*”

Lactantius, speaks of him as “an execrable and pernicious ty-
rant” and a “noxious wild beast. “9*Eusebius writes of him as one
possessed of “extraordinary madness, under the influence of which,
the] . . . accomplished the destruction of so many myriads without
any reason.”® Henderson records the assessments of several scholars
regarding Nero’s, character: Diderot and Marivale call him “the
Monster.” ' Renan speaks of him as “the first in that long line of
monsters.” Duruy claims he “has no equal in history, to whom no
analogy may be found save in the pathological annals of the scaffold. ”
De Quincey calls him “Nero the Arch Tyrant.” Menvale and Beule
state that he “was the last and most detestable of the Caesarean

98, Annas 15:44.

94, Philostratus, Lift of Apollonius 438,

95. Foerster, “Onpiov,” TDNT 3:134,

96, Collins, “Sibylline Oracles, OTP 1:416,

97. This reference is clearly speaking of Nero as has been noted by Collins, “Sibylline
Oracles,” OTP 1:421, and Foerster, “Ogpiov,” TDNT 3.134.

98, Lactantius, Of the Manner mn Which the Persecuiors Died 3 (see ANF 7:302).

99, Eusebius, Eeclesiastical History 2:25:2

100, Henderson, Nero, p. 18.
