Select Bibliography Z: Modern Writings 365

Gregory, George. Dr. Gregory’s History of the Christian Church from the
Earliest Periods to the Present Time, Edited by Martin Ruter. Cinci-
natti: Roff and Young, 1832.

Griffin, Miriam T. Nero: The End of a Dynasty. New Haven: Yale,

1984,

Guignebert, Charles. The Early History of Christianity. New York:
Twayne, n.d.

Gundry, Robert H. Sursey of the New Testament. Grand Rapids: Zon-
dervan, 1970.

Guthrie, Donald B. New Testament Introduction. 8rd ed. Downers Grove,
IL; Inter-Varsity Press, 1970.

Gwatkin, Henry Melville. Zarly Church History to A.D. 313. Vol. 1.
London: Macmillan, n.d.

Hammond, N. G. L., and H. H. Scullard. Oxford Classical Dictionary.
2nd ed. Oxford: Clarendon, 1970.

Hardy, E. G. Christianity and the Roman Government. New York: Burt
Franklin, (1894) 1971.

Harnack, Adolf The Mission and Expansion of Christianity in the First
Three Centuries, 2 vols. New York: Putnam’s, 1908.

Harris, R. Laird. Inspiration and Canonicity of the Bible. Grand Rapids:
Zondervan, 1969.

Harrison, R. K. Introduction to the Old Testament. Grand Rapids: Eerd-
mans, 1969.

Hastings, James, ed. Dictionary of the Bible. 5 vols. New York: Scrib-
ner’s, 1898-1904.

Hayes, D. A. John and His Writing. New York: Methodist Book
Concern, 1917.

Henderson, B. W. Five Roman Emperors, Cambridge: University Press,

1927,

. The Life and Prim”pate of the Emperor Nero. London:
Methuen, 1903.

. The Study of Roman History. 2nd ed. London: Duck-
worth, 1921.

Hendriksen, William. Colossians and Philemon. New Testament Com-
mentary. Grand Rapids: Baker, 1964.

Hill, David. New Testament Prophecy. Atlanta: John Knox, 1979.

Hodges, Zane C., and Arthur L. Farstad, eds. The Greek New Testament
According to the Majority Text, Ind ed. Nashville: Thomas Nelson,

1985.
