The Condition of the Sewn Churches 319

The data discerned from this perspective is almost universally
employed among late date advocates. Although there is a wide
variety of approaches constructed from the material of the Seven
Letters, only the more solid evidences will be tested at this juncture.
We will show that none of the arguments is detrimental to early date
advocacy. In keeping with the approach utilized throughout this
section of our work, we will follow the order found in Morris’s work
on Revelation.

The Wealth of the Church in Laodicea (Rev. 3: 17)
Revelation 3:17 reads:

Because you say, “I am rich, and have become wealthy, and have
need of nothing,” and you do not know that you are wretched and
miserable and poor and blind and naked.

Morris notes that in the Laodicean letter “we are told that the church
in Laodicea was ‘rich, and increased with goods’ (iii. 17). But as the
city was destroyed by an earthquake in A.D. 60/61 this must have
been considerably later."5 Mounce and Kimmel also endorse this
observation, a major component of the complex of evidence derived
from the Seven Letters.®

It is true that Laodicea was destroyed by an earthquake about
this time; the evidence for both the fact of the earthquake and its date
are clear from Tacitus.7 The idea behind the argument is that such
a devastating event as an earthquake must necessarily have severe
and long term economic repercussions on the community. And in
such a community, the minority Christians could be expected to have
suffered, perhaps even disproportionately. If Revelation were written
sometime in the period from A.D. 64-70, it would seem to Morris,
Mounce, and others, that the time-frame would be too compressed
to allow for the enrichment of the church at Laodicea, as is suggested
in Revelation. But by the time of Domitian a few decades later, such

3. Morris, Revelation, p. 87.

6 Mounce, Revelation, p. 35 and Kimmel, Zatroduction, p. 469,

7, Tacitus, Annals 1427, Most scholars accept the dating from Tacitus, Eusebius
(Chronicle 64) and Orosius speak of it as occurring after the fire that destroyed Rome in
AD. 64, according to C, J, Hemer, A Study of the Letters to the Seven. Churches of Asia with
Special Refirence to Their Local Background (Manchester: unpublished Ph.D. dissertation,
1969), p. 417; cited in Mounce, Ravlation, P. 128, n, 81,
