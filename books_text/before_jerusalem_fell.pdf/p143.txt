The Thm of Revelation 129

And in this respect the fact that a city was in heathen possession
exercised a decisive influence. Thus the environs of Ascalon, the wall
of Caesarea, and that of Acco, were reckoned within the boundaries
of Palestine, though the Cities themselves were not, Indeed, viewing
the question from this point, Palestine was to the Rabbis simply ‘the
land, all other countries being summed up under the designation of
‘outside the land.’ “2

That such is the referent in Revelation 1:7 seems to be addition-
ally indicated by the fact that the verse is a blending of Daniel 7:18
and Zechariah 12:10. The Zechariah 12:10 passage indisputably
refers to the land of Israel: “And I will pour out on the house of David
and on the inhabitants of Jerusalem, the Spirit of grace and of
supplication, so that they will look on Me whom they have pierced;
and they will mourn for Him, as one mourns for an only son, and
they will weep bitterly over Him, like the bitter weeping over a
first-born, In that day there will be great mourning in Jerusalem, like
the mourning of Hadadnmmon in the plain of Megiddo. And the
land will mourn, every family by itself.”

Furthermore, in Jesus’ teaching there is a recurring emphasis
upon the culpability of the generation of Jews then living. In Mat-
thew 28 He calls down a seven-fold woe upon the scribes and
Pharisees, those who “sit in the chair of Moses* (Matt. 23:2).In this
woeful passage He distinctly and clearly warns (Matt, 28:32-88):

Fill up then the measure of the guilt of your fathers. You serpents, you
brood of vipers, how shall you escape the sentence of hell? Therefore,
behold, I am sending you prophets and wise men and scribes; some
of them you will kill and crucify, and some of them you will scourge
in your synagogues, and persecute from city to city, that upon you may
Jail the guilt of all the righteous blood shed on earth, from the blood of
righteous Abel to the blood of Zechariah, the son of Berechiah, whom
you murdered between the temple and the altar. Til» I say to you, all
these things shall come upon this generation. O Jerusalem, Jerusalem, who
kills the prophets and stones those who are sent to her! How often I
wanted to gather your children together, the way a hen gathers her
chicks under her wings, and you were unwilling. Behold, your house
is being left to you desolate!

24, Alfred Edersheim, Skeishes of Javish Social Life (Grand Rapids: Eerdmans, [1876]
1972), p. 14,
