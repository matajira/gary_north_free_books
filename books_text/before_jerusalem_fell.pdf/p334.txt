The Condition of the Seven Churches 321

material but spiritual... . [T]he Laodiceans felt they were secure
in their spiritual attainment .* !°

The Ease of the Recovery

In addition, there is the impressive historical evidence of the
situation that tends to undermine the rationale of the argument, even
if material riches are in view. Most ruinous to the entire argument is
the documented fact of Laodicea’s apparently effortless, unaided, and
rapid recovery from the earthquake. Tacitus reports that the city did
not even find it necessary to apply for an imperial subsidy to help
them rebuild, even though such was customary for cities in Asia
Minor. As Tacitus records it, Laodicea “arose from the ruins by the
strength of her own resources, and with no help from us. ” This is
as clear a statement as is necessary to demonstrate that Laodicea’s
economic strength was not radically diminished by the quake. De-
spite the quake, economic resources were so readily available within
Laodicea that the city could easily recover itself from the damage.
Interestingly, both Morris and Mounce make reference to this state-
ment by Tacitus, despite their using the argument to demand a late
date.

Furthermore, it would seem that the time element would not be
extremely crucial for “earthquakes were very frequent thereabouts,
and rebuilding doubtless followed at once. ” '3 The quake occurred in
AD. 61; if Revelation were written as early as A.D. 65 or early A.D.
66 (as is likely), that would give four years for rebuilding. We must
remember that the recovery was self-generated. Simple economic
analysis demands that for the resources to survive, rebuilding would
have to be rapid.

The Epicenter of the Quake
In addition, who is to say that the Christian community was

10, Mounce, Revelation, p. 126, This is not the first time that Mounce employs an
argument in his introduction that he fails to follow through adequately in his commen-
tary. See our observations in Chap. 18 on his contradictory treatment of the NerRedivivus
myth, It would seem most reasonable to expect that if the argument in his introduction
is to be given weight, it must not be allowed to shift its meaning in the commentary.

11, Tacitus, Annals 1427,

12, Morris, Revelation, p, 87; Mounce,Revelation, p. 128,

18. F. J. A. Hort, The Apocalypse of St.John: F-if! (London: Macmillan, 1908), p. xx.
See Strabo (64 B.C. - A.D. 18), Geographic 12:8: Dio Cassius, Roman History 54:30.
