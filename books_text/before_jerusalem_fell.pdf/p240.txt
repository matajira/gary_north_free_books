226 BEFORE JERUSALEM FELL

It should be self-evident that the cataclysmic events of A.D. 70
played a dramatic role in the life of both the Church and Judaism in
terms of their inter-relationships. Unfortunately, this event is too
often overlooked by many.20 But was not Christianity born in Jerusa-
lem (Acts 2) in obedience to Christ’s commands (Luke 24:44-53;
Acts 1)? Was it not headquartered there in its earliest period (Acts
8:1; 11:2; 15:2; Gal. 1:17, 18; 2:1, 2)? Yet when the dust settles after
the Fall of Jerusalem, we no longer find a Christian concentration
on Jerusalem. Indeed, in A.D. 80 Gamaliel II caused the Jewish daily
prayer (Skemone Esre) to include a curse on the Christians: “Let the
Nazarene [sc. Christian] and the Menim perish utterly.”?! Indeed,
it is impossible for us nowadays to realize the shock of A.D.70 toa
community in which Jewish and Gentile members alike had been
reared in the profoundest veneration of the immemorial sanctity of
the Holy City and the Temple.”2*

Certainly the breach did not come overnight. Since its inception
Christianity had been persecuted almost exclusively by the Jews
throughout the period of the Acts.*Yet many converts were being

20. Few New Testament acholars have really come to grips with the significance of
Jerusalem's fall. 8. G. F. Brandon states: “Attention has already been drawn to the
curious neglect scholars have shown towards the subject of the significance of the
destruction of Jerusalem for the infant Christian Church” (Brandon, The Fall of Jerusalem
and ike Christian Church: A Study of the Effects of tke Javish Overthrow of A.D. 70 on Christianity
[London;: SPCK, 1957], p. x). Since the publication of Robinson's persuasive Redating the
Nau Testament (1976), however, this calamity is difficult to overlook.

21, See Torrey, Apocalypse, p. 82; H, Daniel-Reps, The Church of Apostles and Martyrs,
trans. Audrey Butler (London: Dent, 1968), p. 48.

22, B, H, Streeter, The Four Gospels: A Study of Origins (Landon: Macmillan, 1924), p,
516,

28, We maintain this in spite of the confident assertions by Brandon that “the
Palestinian Christians stood well in the estimation of their fellow countrymen and were
subjected to no concerted persecution by the popular leaders and the people” and “the
Palestinian Christians were not an outcast body from the national life of Israel, but rather
they enjoyed a certain measure of sympathy from the Pharisees* (Brandon, Fall of
Jerusalem, p. 100), His argument is primarily based on an alleged incongruity and
confusion in the record of the Acts which he discovers by comparing other ancient.
records of the era (see his chap. 6).

Moule ig surely more in line with the realitv of the situation when he writes: ‘<so far.
then, as our only New Testament narratives go, there is no predisposition to expect other
than Jewish origins for persecution. And if it is objected that the Acts is biased in this
respect, because it is a studied apologia to the Roman government, the burden of proof
rests with those who try to discredit its reliability here” (Moule, Birth of Nav Testament,
8rd cd., p. 159). He then proceeds to defend this evangelical position with considerable
expertise (pp. 159).
