Sic et Non: Judicial Agnosticism 229

God would no longer wink at such ignorance, Paul an-
nounced. But Barker wants to make such cursed ignorance the
basis of our appeal to the natural man until such time as we
Christians are a majority, i.e., in a “non-missionary” situation.
Therefore, natural law is that to which we must appeal. There it is,
in black and white. This is Westminster’s confession, in the
words of the book’s co-editor.

Barker states: “This is the way Paul operated in the Roman
Empire and the way any Christian must operate in a minority
situation.” Let us explore this “minority situation” idea. Ques-
tion: To what should Christians appeal when we are no longer
a-minority? This distinctly postmillennial question is the one
that Barker and his pluralist and amillennialist colleagues
steadfastly refuse to answer in print. If he says “theonomy,”
then he has given up his pluralist theology. Christian pluralism
then loses its status as a scrious political philosophy; it becomes
merely a tactic, a pragmatic con job to fool the covenant-break-
ers until such time as we Christians get the votes. On the other
hand, if he says “natural law,” then he is trapped: his appeal to
our present minority status as the basis of our need to appeal
to natural law is revealed as a rhetorical con job to fool the
followers of Van Til. So, he is trying to fool either the pagans
or the Vantilians. I think it is the latter.

My assessment of his real judicial commitment is this: he has
no intention of ever appealing to theonomy; he is a defender
of natural law theory. With respect to our present minority
status and our supposed need to appeal to natural law when
trying to persuade pagans, it is just another case of sic et non.
Our minority status is supposedly permanent, so natural law is
to be our permanent guide.

Why go on with this? But I will. We need specific, detailed
answers. Whose version of natural law? Not in the U.S. Consti-
tution, surely, which places all judicial sovereignty in “We, the
People,” but none in a higher law. Where have these principles
been stated and defended? What societies have adopted this
