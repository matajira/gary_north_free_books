326 WESTMINSTER’S CONFESSION

and they resent the restraints God’s that law unquestionably
imposes on them and thcir intellectual allies, trendy humanists,

Politically liberal antinomians can easily enlist the support of
instinctively conservative antinomian pietists, who also reject
God’s law. Representatives of both groups — fundamentalist
conservatives like Dave Hunt and neo-evangelical liberals like
Rodney Clapp — can march arm in arm against the perceived
threat of covenant theology. Modern pietistic Christians are
petrified at the prospects of a worldwide Christian revival. If it
comes, Christians will be called upon to provide specific biblical
answers to questions in every area of life. Neither their theol-
ogy nor their training has provided pietists with the necessary
tools of leadership. Certainly their hostility to biblical law leaves
them without the required intellectual resources. So they deny
that it will ever happen. Better a world without personal bibli-
cal responsibility than a world where billions of people are
converted to faith in Christ. Better to see multitudes in hell
than Christians in power. Better pessimillennialism than post-
millennialism.

Mr. Clapp ridicules the complexity of Reconstructionist
discussions of biblical law. He insists on a simple world for
Christians, a world of few disagreements or intellectual diffi-
culties. But should we expect disagreements in a Christian
society? Of course. Through disagreements comes progress.
Look at the history of the creeds. Look at the Reformation.
Why else did Dr. Luther nail his 95 points of disputation on
the church door, if not to elicit formal public debate? People
daily hammer out the truth through publishing, experimenta-
tion, and market competition. Yet here is Mr. Clapp, horrified
by the thought of Christians having to exercise such awesome
responsibility. Better to leave such matters to the humanists, he
implies. Better to leave it to the 600,000 humanist lawyers in
the U.S, Better to “live by the spirit.” That way, tyranny will
have no Christian opponents. In such a world, Christians will
be able to remain culturally irrelevant, all nicely covered by a
