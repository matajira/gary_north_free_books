Honest Reporting as Heresy 341

Four years later, Clapp wrote a book review of Lesslie New-
bigin’s The Gospel in a Pluralist Society. It appeared in Christianity
Today (Jan. 14, 1991). His review was generally favorable. It
was also judicially schizophrenic, in the Westminster Seminary
way: sic et non.

Newbigin asks some of the same questions that we Recons-
tructionists have been asking. The problem, according to New-
bigin, is that the Church has accepted humanism’s dualism
between facts (external) and values (internal). Clapp agrees:
“Privatizing faith meant, in reality, trivializing it. No longer did
the church forthrightly proclaim Christ as public truth, the
most important fact and value of existence. Instead, Christianity
was relegated to the same realm as mere opinion of prefer-
ence” (p. 36). A privatized faith is fundamentalism’s faith, and
we all know what Christianity Today thinks of fundamentalism!

Now, most Christians might say they never assented to any
such arrangement. But right up to the present, many Christians
continue to interpret and understand the faith in individualistic
and privatistic terms, which is a distortion of the biblical witness,
Though not intended to, it undermines the social and indeed
cosmic breadth of God’s saving kingdom,

If this does not sound like a theonomic analysis, then I have
misread it. Clapp sees where this is headed, and he pulls back.

Along these lines, it is a disappointment that Newbigin can
still hope for some form of Christian society. Incredibly, given
centuries of Constantinian rapprochement, he suggests this is a
“question that has not been seriously followed up” (p. 38).

Clapp’s enemy is still the same: Christendom. This is why he
was cited so often by the authors in Theonomy: A Reformed Cri-
tique: they share a common enemy.

They all face the same problem: If not biblical law, then
what? If not biblical covenantalism, then what?
