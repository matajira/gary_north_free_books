350 WESTMINSTER’S CONFESSION

amillennial followers to conclude that their view of the future
of Christianity was also his.

Calvin’s Pessimism

An example of this pessimism is his discussion of salvation
and peace, two promised blessings for believers in Christ. What
is implied by this promise?

Hence these things are connected together, salvation and peace,
not that we enjoy this joyful and peaceful state in the world; for
they greatly deceive themselves who dream of such a quiet state
here, as we have to engage in a perpetual warfare, until God at
length gathers us to the fruition of a blessed rest. We must,
therefore, contend and fight in this world. Thus the faithful
shall ever be exposed to many problems; and hence Christ
reminds his disciples, “in me ye have peace; but in the world” —
what? Sorrows and troubles.$

Yet personal sorrows and troubles do not deny the possibili-
ty of kingdom expansion and victory in history. For example,
we all recognize that individual inventors have many troubles
and frustrations; this does not deny the possibility of technolog-
ical progress. What about personal spiritual progress? It is not
only possible; it is mandatory, Calvin taught.* “No one shall set
out so inauspiciously as not daily to make some headway,
though it be slight. Therefore, let us not cease so to act that we
May make some unceasing progress in the way of the Lord.
And let us not despair at the slightness of our success. . . .”°

The question, then, is the compound growth of righteousness.
Can it outpace the compound growth of wickedness in history?

8. Calvin, Commentaries on the Book of the Prophet Jeremiah and the Lamentations
{Grand Rapids, Michigan: Baker Book House, [1563] 1979), IV, p. 285: Jer. 39:16.
4, John Galvin, Institutes of the Christian Religion (1559), translated by Ford Lewis
Battles (Philadelphia: Westminster Press, 1960), ITI:XX:42.
8. Ibid., WEVLS.
