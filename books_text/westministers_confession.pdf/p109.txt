A Positive Biblical Confession Is Mandatory 85

posumillennialists. There is also no doubt about where contem-
porary dispensationalists think Christians should and must be
prior to the Rapture, the Great Tribulation, and the Second
Coming: cleaning out the cesspools of humanism’s civilization.
The best we can hope for, in their view, is an electric suction
pump instcad of a hand-driven one, or worse (visions of gaso-
line tank siphon hoses), But Westminster Seminary is neither
theonomic nor dispensational. Where, then, do its faculty
members think that Christians should be in the cultural and
political hierarchy? And how do they think the covenantal
Church, State, and family should get us therc?

Jerusalem or Athens?

Since 1973, the Westminster faculty has faced a growing
theological challenge: thconomy. The theonomists claim that
there is a workable alternative to the judicial pluralism of the
modern world: biblical law. That our world is suffering from
the devastating effects of moral pluralism no Calvinist doubts.
That this moral pluralism is the product of an underlying
theological pluralism, no Calvinist doubts. Men reject the God
of the Bible, and so He turns them over to their own evil imag-
inations (Rom. 1:18-22). But the question that the theonomists
have raised needs to be answered: If the long-run intent of the
gospel of Jesus Christ is to eliminate theological pluralism and
moral pluralism, why isn’t it inescapably also the long-run goal
of the gospel to eliminate judicial pluralism? Furthermore, if
judicial pluralism goes, then so will political pluralism.

It is this final step that the critics of theonomy all see as a
necessary outcome of the transformation of pluralism. But if
this is a legitimate long-run goal of the gospel, then Christiani-
ty is in ultimate conflict with the right wing of the Enlighten-
ment, meaning all forms of democratic theory that promote
universal suffrage without respect to creedal confession. Ulti-
mately, it means the rejection of the political polytheism of the
