190 WESTMINSTER’S CONFESSION

to speak prophctically and without qualifications. “Thus sayeth
the Lord, more or less, give or take a little, on the average!” is
their battle cry against the pagan, apostate academic world that
granted them their academic degrees and has accredited their
programs.* Having been forced by the seminary’s hiring poli-
cies to submit themselves for-anywhere from seven to ten years
to secular humanist higher education (B.A., M.A., Ph.D.), they
are not used to conducting offensive, head-on, academic con-
frontations. They have been trained to lie low, to de-emphasize
their unique Christian outlook. They have been trained to seek
a common-ground compromise. Van Til was an exception, but
his precedent of total confrontation with humanism and philo-
sophical autonomy has not been followed on campus: at West-
minster or anywhere else.

Seminary faculty members do not choose to bite the hands
that fed them their academic certification. They even seek out
continuing approval from their covenantal enemies by submit-
ting the seminaries to formal accreditation. They cannot deal
with the idea that it is Christians, and only Christians, as the
exclusive covenantal agents of God’s kingdom in history, who
are supposed to do the certifying. They cannot seem to shake
loose a deeply rooted Christian inferiority complex. “Tell us
that we meet your standards, we beg of you! Tell us what to
do, and we will do it, We know that your academic standards
are entirely neutral, so we will submit!” Yet it is to them that
Christian laymen turn for counsel on how to fight the good
epistemological fight of faith.

Certification as Initiation

The academic certification issue has been at the heart of the
retreat of Christianity for about eight centuries. The legally
independent universities of Europe® steadily became the

2. They have placed themselves under a hostile authority.
3. This independence can be traced back to 1242, when the Pope granted new
