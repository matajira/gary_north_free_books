Sic et Non: Judicial Agnosticism 207

today: “Therefore, Westminster could not hire Bahnsen, and
Shepherd had to be fired.”

Vernon Sheridan Poythress

Dr. Poythress is basically “Me, too, John,” with minor quali-
fications. He writes as though he thinks that Frame is just too
hard core. Poythress wants to smooth over all differences,
Sometimes I am tempted to call him Vernon Sheridan Pang-
loss.

Dr. Poythress loves to play with texts. He rolls them around
in his word processor the way children roll around bright
stones in their fingers. Then they drop them into a box and
forget about them. So does Poythress. For example, he cites
Leviticus 19:19: “Ye shall keep my statutes. Thou shalt not let
thy cattle gender with a diverse kind: thou shalt not sow thy
field with mingled seed: neither shall a garment mingled of
linen and woollen come upon thee.” He spends seven precious
pages discussing how this text might be interpreted in different
ways by theonomists and intrusionists, but he never suggests
how it should be interpreted by faculty members.

He then invokes Frame’s familiar tripartite division of ethics
(and just about everything else) into normative, personal, and
situational: three more pages gone. No conclusions. Then he
cites Deuteronomy 4:6-8:

  

Keep therefore and do them; for this is your wisdom and your
understanding in the sight of the nations, which shall hear all
these statutes, and say, Surely this great nation is a wise and
understanding people. For what nation is there so great, who
hath God so nigh unto them, as the Lorp our God is in all
things that we call upon him for? And what nation is there so
great, that hath statutes and judgments so righteous as all this
law, which I set before you this day?
