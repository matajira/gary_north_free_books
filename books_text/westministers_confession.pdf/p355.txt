Honest Reporting as Heresy 331

between the false liberation theology of Marxism and the true
liberation theology of Christian Reconstruction.

He is partially correct when he writes that “North evidences
a glee for polemical bloodshed. . . .” Because I want to become
a universally respected elder statesman (whose books then sell
like hotcakes, Iam promised), and because I am growing ma-
ture (read: stodgy), these days I only exercise this taste for
blood as an appetizer. The “old Gary North” only appears in
the introductions I write to other Reconstructionists’ books that
I publish. (And in an occasional essay like this one.)

He is undoubtedly correct when he writes that “The Recons-
tructionists are frequently criticized for not adequately appreci-
ating the historical and cultural distance between nomadic,
agricultural Israel and modern technological America.” We are
criticized this way by blatant Darwinian relativists who are masquer-
ading as Christians. 1 am devoting thousands upon thousands of
pages to show just how relevant Old Testament law is in
today’s economic world. These full-time antinomian skeptics
who say such nonsense — post-Mosaic Israel was never nomad-
ic, for example ~ are trying to run from the law of God, who is
utterly hostile to their recommended humanistic policies of
socialism, Keynesian interventionism, and liberation theology.
Furthermore, they have not done their homework. They have
not shown just exactly how God’s laws against theft, debt, in-
flated fiat money, false weights and measures, and similar evils
have been annulled by the gospel of Christ. Yet they whine
endlessly about “oppressive capitalism.” They are simply apolo-
gists for humanism’s economic whoredom.

Finally, Mr. Clapp is unfortunately correct when he writes:

The Reconstructionists are also a distinct minority in their
conviction that Israel was not the only nation God intended to
be a theocracy. In a paper criticizing Bahnscn's Theonomy, Co-
lumbia (S.C.) Graduate School theologian Paul Fowler states the
commonly accepted interpretation that “God set Israe] apart to
