130 WESTMINSTER’S CONFESSION

world ever since as the chief enemy of human liberty. It appears
in the world to-day.*

In his cross-examination session by members of the commit-
tee, he observed ~ prophetically, it has turned out: “I think we
are having to-day a very marked intellectual as well as moral
decline through the gradual extension of standardization in
education.”** What is really interesting is that one proponent
of the bill kept pressing Machen to admit that the Bureau of
Education had done some good things in administering the
schools in Washington, D.C.” Machen did not take the bait.
Today, there are few if any Congressmen who send their chil-
dren into the hell-holes of the Washington, D.C., public school
system.

One of the Senators asked him a perceptive question. It is
the educational question of questions in the United States. It
has been the question ever since the decision of the Puritans in
Massachusetts to pass a compulsory school attendance law in
1642 — a precedent used by Horace Mann two centuries later
to make America a unitarian nation:

I am just wondering whether there is any such thing as
moral conduct in the United States Congress or among the
citizens of the United States apart from a distinctly religious
basis. I am just wondering whether the public schools have any
function in the way of teaching morality which is not distinctive-
ly religious in its basic idea.

Here is Machen’s reply:

I myself do not believe that you can have such a morality
permanently, and that is exactly what I am interested in trying

25. Ibid., p. 101.
26, Tbid., p. 114.
27. Ibid., pp. 116-18.
