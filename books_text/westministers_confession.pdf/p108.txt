84 WESTMINSTER’S CONFESSION

moral pressure of searching for judicial consistency on God’s
part, justifying it theologically, and, most threatening of all,
publicly pressuring the civil government to honor this consis-
tency in its laws and espccially its sanctions. In short, they feel
themselves hard-pressed to survive on the fringes of Western
civilization, let alone to move toward its judicial center in the
name of Jesus Christ.

When the Bough Breaks

But there is this lurking problem: What presently unifies the
judicial foundation of Western civilization? What moral base
provides the continuing legitimacy, stability, and public faith in
Western civilization? Christians, as members of the Church
Militant, are in history, like it or not. They want out, but they
cannot lawfully get out except on God’s terms. They are not
immune to the disruptions that are now escalating in the
world. If God is not the source of these disruptions, then what
is? If Christians no longer believe that God brings sanctions in
history, then what hope can Christians offer to a world in
crisis?” Dispensationalism offers the Rapture,’ but Rapture
fever, like all other fevers, produces the familiar symptoms of
uncontrollable shaking and hallucinating in its victims, grim
afflictions that are only marginally more debilitating than amil-
lennialism’s pre-parousia paralysis: God’s frozen people.

If Christians are in the world, then they are supposed to be
either on top of things under God or at the bottom of the heap
under Satan. There is no doubt where the theonomists think
Christians should be, if the Church were to become covenan-
tally (judicially) faithful to God for a few generations — not only
an historic possibility but an inescapable future reality, say the

1, Gary North, Millemialism and Social Theory (Iyler, ‘Texas: Institute for
Ghristian Economics, 1990), ch. 8.

13. Dave Hunt, Whatever Happened to Heaven? (Eugene, Oregon: Harvest House,
1989).
