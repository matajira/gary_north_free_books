10 WESTMINSTER’S CONFESSION

long. They have begged humanists and theological liberals for
academic accreditation. They have sent their ministers to secu-
lar universities, They have sought to remain in theologically
liberal denominations as minority tokens. They have lost their
faith in the victory of Christianity in history, let alone the victo-
ry of Calvinism. They have seen themselves as minority status
citizens in a world forever controlled by their enemies. Thus,
they have sought to avoid confrontations. They have become
psychologically irenic.

Not so Luther and Calvin. They were not in the least inter-
ested in gaining the positive sanctions of the Roman Church.
They had no interest in irenic debates. They wanted to identify
areas of disagreement, not areas of agreement. They adopted
a highly confrontational rhetorical style. But the average Cal-
vinist or Lutheran knows almost nothing of the rhetoric of the
Reformation. Christians rarely study Church history. Protes-
tants do not even study the history of the Reformation. Per-
haps they may have sat through a Sunday school series twenty
years ago that surveyed the Reformation. Maybe they have
read a 140-page book on the Reformation written by a non-
confrontational seminary professor whose rhetorical model is
modern academia. If the Reformation had becn run by today’s
seminary professors, it never would have begun.

The typical Calvinist has never read Calvin’s Institutes. It sits
on his shelf unread. “Someday, T'll read it,” he vows to himself,
but he knows he never will. “Anyway, our pastor has read it.
He knows.” Hal If every Calvinist pastor in America who has
not read the Institutes cover-to-cover had to resign on a Thurs-
day, there would be a lot of empty pulpits the next Sunday.
The whole of the Instituées is not assigned in any Calvinist semi-
nary that J know of. Calvinists simply do not know the history
of their movement. They do not know what the Reformers did
in order to Icave the legacy of the Reformation to their spiritu-
al heirs. They have never read the rhetoric of the Reformation.
They accept the Reformers’ legacy but reject their methods,
