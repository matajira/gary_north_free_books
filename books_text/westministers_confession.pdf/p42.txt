18 WESTMINSTER’S CONFESSION

penalty.* He also opposed civil laws against prostitution, adul-
tery, incest, rape, and sodomy. After all, these are all natural
urges and practices; they are found in nature. Marriage and
monogamy are not normal in nature. “Can we possibly imagine
Nature giving us the possibility of committing a crime which
would offend her?”*?

When a man chooses between natural law and biblical law,
let him understand well in advance just what the theoretical
implications of his choice really are. The faculty of Westminster
Theological Seminary has not yet understood these implica-
tions, yet it has long since made its collective choice.

Conclusion

The Calvinist or Reformed Protestant world today is an
exceedingly narrow one — one might even say institutionally
incestuous, which is the fate of newly developing movements
and also fading ones. The Reformed world, being small and
few in number, can muster only a few scholars, and fewer
published ones. But Calvinism’s influence in the American
Protestant world has been way out of proportion to its numbers
ever since 1800, when Baptists and Methodists began to out-
strip the Calvinists on the growing mission field of the Western
United States.

Why this disproportional influence? One reason is that the
various non-Reformed camps (Lutherans excepted) did not do
the work of detailed biblical scholarship until after World War
II, when neo-evangelicalism appeared. The fundamentalist
world still relies heavily on non-fundamentalists to defend itself
against the higher critics of the Bible. So, as defenders of the
faith, especially against German liberalism, Reformed scholars
were the watchmen on the American churches’ watchtower.
After 1900, this watchtower meant primarily Princeton Semi-

38, Ibid., p. 247; cited in ibid., p. 65.
39, Ibid., p. 258; idem.
