The Question of Millennialism 173

and inside the Church and family? None. There is none. They
simply refuse to discuss what they have done. They assert, as
Gaffin asserts, that any concept of covenantal progress in histo-
ry outside the Church and family is biblically illegitimate. (His
language is so strong in this regard that he could become as
confrontationally rhetorical as I am, if he would just work at it.
He has clearly displayed the basic talent; now he just needs to
develop it.)

Gaffin’s problem is that he holds to the theology of Eastern
Orthodoxy with respect to history: moral progress only through
suffering. No Calvinist amillcnnial theologian has articulated this
position any more clearly. He has developed an entire world-
view based on this presupposition. He calls this his most sub-
stantial reservation against postmillennialism. It has taken
seventeen years of theological pressuring since Rushdoony’s
Institutes of Biblical Law was published to get so forthright a
statement out of a Calvinist amillennialist. No one has demon-
strated more visibly the accuracy of Rushdoony’s judgment:
amillennialists are premillennialists without earthly hope.

Personal Moral Progress Only Through Suffering

Gaffin calls amillennialism inaugurated eschatology, a variant of
realized cschatology. Understand, this is the equivalent of
definitive eschatology. There would be nothing wrong with it if it
had the necessary complement, progressive eschatology. But he is
appalled by the very thought of progressive eschatology, for it
would necessarily deny the heart of his ethical system: personal
maturation through suffering. We need persecution in history.

The inaugurated eschatology of the New Testament is least
of all the basis for triumphalism in the church, at whatever point
prior to Christ’s return. Over the interadvental period in its
entirety, from beginning to end, a fundamental aspect of the

10. Ibid., p. 210.
