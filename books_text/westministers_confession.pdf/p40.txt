16 WESTMINSTER’S CONFESSION

never daring to suggest what it should be. It is the offense of
Christian Reconstruction that in the name of the original West-
minster Confession, we proclaim an exclusively biblical ideal,
based on the idea that the general equity of civil law can be progres-
sively achieved in history only through a self-conscious application of
biblical law (theonomy). If Van Til was correct about the corrupt-
ing effects of sin on fallen man’s ethical sense, and if he was
correct about the illegitimacy of natural law theory, then there
can be no other interpretation of the general equity clause of
the Westminster Confession (XIX:4). But Westminster Semi-
nary has played a game of sic e¢ non with Van Til’s legacy,
saying “yes” to his rejection of natural law but “no” to the
theonomists’ application of it to civil law and the Confession’s
general equity clause. Westminster’s confession is a confession in
conflict. Westminster Seminary is inherently a house divided
against itself. So is any form of Christianity that adopts West-
minster’s new judicial confession.

Christian Reconstructionists paraphrase Van Til: “Christian
society is not one possible working model among many; it is
the only possible working model. Every other model is wrong
and will be judged wanting by God in history.” To silence this
positive confession,** Westminster’s faculty decided to write
Theonomy: A Reformed Critique.

Sadism and Natural Law Theory

Because this is an introduction, I need to warn the reader
well in advance: this book is about natural law theory and its
implications for applied theology. Make no mistake about it: ail
Christian theology is applied theology. This may not be apparent in
all cases, but it is always the case. There is no neutrality in life.
Christianity is a way of life. Every religion is a way of life, and
every way of life is grounded in some religion.

84, Note: it is a positive judicial confession, not a positive magical confession.
