176 WESTMINSTER’S CONFESSION

And it shall come to pass in that day, that the remnant of
Israel, and such as are escaped of the house of Jacob, shall no
more again stay upon him that smote them; but shall stay upon
the Lorn, the Holy One of Israel, in truth. The remnant shall
return, even the remnant of Jacob, unto the mighty God. For
though thy people Israel be as the sand of the sea, yet a rem-
nant of them shall return: the consumption decreed shall
overflow with righteousness. For the Lord Gop of hosts shall
make a consumption, cven determined, in the midst of all the
land. Therefore thus saith the Lord Gop of hosts, O my people
that dwellest in Zion, be not afraid of the Assyrian: he shall smite thee
with a rod, and shall lift up his staff against thee, after the manner of
Egypt. For yet a very little while, and the indignation shall cease,
and mine anger in their destruction. And the Lorp of hosts shall
stir up a scourge for him according to the slaughter of Midian at
the rock of Oreb: and as his rod was upon the sea, so shall he
lift it up after the manner of Egypt. And it shall come to pass in
that day, that his burden shall be taken away from off thy shoul-
der, and his yoke from off thy neck, and the yoke shall be de-
stroyed because of the anointing (Isa. 10:20-27). (emphasis
added)

After the manner of Egypt. Every covenant-keeper is supposed
to remember what happened to Egypt after that nation broke
the Israelite vessels: destruction in history. But such a message
of reversed roles, of victory, Gaflin says is strictly limited to Old
Testament history; it has nothing to do with the history of the
Church of the resurrected Christ. How do we know this? Be-
cause of Philippians 3:10: “That I may know him, and the
power of his resurrection, and the fellowship of his sufferings,
being made conformable unto his death.”"* He then spends
several pages explaining Christ’s sufferings and His death. He
defines Christ’s resurrection in terms of His suffering. Here is with-
out a doubt the heart of the amillennial message, a message of
incomparable pessimism: “By virtue of union with Christ, Paul

14, bid., p. 212.
