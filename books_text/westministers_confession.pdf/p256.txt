232 WESTMINSTER’S CONFESSION
Division in the Ranks

Davis understands that theonomy is a divisive issue. If we
theonomists continue to argue that ours is the only correct
view, it will be impossible for others in the creedal churches to
work with us.’ Why? It all depends on how we press our
case. If we teach, convert the best and the brightest to our
position, and wait for God’s covenantal sanctions to transform
people's thinking, what is wrong with this? We have time. We
are postmillennialists. We can afford to wait. A few victories,
and pessimillennialists will switch. Pessimillennialism exists
primarily to justify failure; when Christianity starts to win,
pessimillennialism will be abandoned by younger activists. This
is how paradigm shifts work. Theonomists can bide their time.

He observes: “Theonomists appear to be committed to ‘con-
servative’ politics. If that is so, is theonomy really the political
position supported by the Bible?" He does not answer his
question, of course. If he were to say yes, he would have to
account for the long presence of his mentor, Paul Woolley, on
the faculty. If he said no, he would alienate a lot of donors,
since few church members in the pews support the political
ideas of Paul Woolley. So he prudently refuses to answer. We
have seen this strategy before:

And when he was come into the temple, the chief priests and
the elders of the people came unto him as he was teaching, and
said, By what authority doest thou these things? and who gave
thee this authority? And Jesus answered and said unto them, I
also will ask you one thing, which if ye tell me, | in like wise will
tell you by what authority I do these things. The baptism of
John, whence was it? from heaven, or of men? And they rea-
soned with themselves, saying, If we shall say, From heaven; he
will say unto us, Why did ye not then believe him? But if we
shall say, Of men; we fear the people; for all hold John as a

102. Ibid., p. 394.
108. Ibid., p. 396.
