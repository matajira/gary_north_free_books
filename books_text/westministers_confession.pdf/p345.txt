Honest Reporting as Heresy 321
Republican Civil Government

Ah, yes, a republic. You know: that system of representative
government which the authors of The Federalist offered as an
alternative to classical democracy, since direct democracy was
greatly feared by voters in the early American Republic, Madi-
son wrote in Federalist No. 10:

Hence it is, that such Democracies have ever been spectacles of
turbulence and contention; have ever been found incompatible
with personal security, or the rights of property; and have in
general been as short in their lives, as they have been violent in
their deaths.

“Democracy as Heresy,” Mr. Clapp? Dirty pool, Mr. Clapp?
Snookering your readers, Mr, Clapp? Slander by headline, Mr.
Clapp? It is an old technique: readers tend to remember head-
lines more easily than buried evidence indicating that the head-
line is a fraud. It is a corrupt technique, but it works.

Reconstructionists do indeed want a decentralized republic
whose primary charter is the Bible. We would never say that
the Bible is the only charter. Calvinists believe in creeds, after
all. We believe in other kinds of written documents: covenants,
contracts, and charters. Calvinists invented constitutionalism.
But all covenants, contracts, and charters, like all creeds, are
subject to the ultimate authority of the Bible. Why does Chris-
tianity Today mock this?

Now, if Mr. Clapp’s essay had been titled, “Secular Human-
ist Democracy as Heresy,” it would have been an accurate
reflection of our publicly stated views. But, then again, such a
title would also have reflected the views of literally millions of
American fundamentalists, and not just some strange and dark
conspiracy of Reconstructionists. All the fun would have gone
out of the game for Mr. Clapp.

And also a good deal of the misrepresentation.
