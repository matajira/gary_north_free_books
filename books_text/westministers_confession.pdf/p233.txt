Sic et Non: Judicial Agnosticism 209

notice that familiar word, simple — “to the effect that Old Testa-
ment law is confirmed in the New Testament and therefore
must be kept now in a literal and straightforward way are not
adequate. Some intrusionists’ simple arguments to the effect
that many laws are not found outside of the Mosaic era and
therefore may safely not be kept are equally inadequate.” No
names are mentioned. No wonder. Now, here is the capper,
the absolute crushing climax:

Both of these routes arc the lazy way out in the sense that they
do not come to grips with the full richness of Old Testament

revelation. We have to work to understand what God is saying.
5

Funny thing: when I sent Dr. Poythress my manuscript for
Tools of Dominion in 1988, he wrote back to tell me how sur-
prised he was to learn that I was working on the case laws of
Exodus, And what, pray tell, did he think would follow my two
volumes on Exodus, with volume two ending with Exodus 20?
(Add to this an appendix to Tools that reached 700 pages of
text: Political Polytheism.)

He admits that “The best representatives of both theonomy
and intrusion are of course not so simplistic. But I think that
even they may be able to learn by some more sensitive listening
to the other side.”** First, I feel compelled to ask Dr. Poyth-
ress: When is it time to stop “listening sensitively” and start
preaching decisively? When do the endless qualifications cease?
Second, I have yet to see a single piece of exegesis of any bibli-
cal law by any intrusionist that is said te be applicable to the
New Covenant era. We have waited patiently for twenty-eight
years.” How long do we have to wait? All I see are defenses
of political pluralism without any exegesis whatsoever. Where

45, Ibid., p. 122.
46, Idem.
47. Kline's Trealy of the Great King appeared in 1963.
