Abusing the Past 245

Therefore, Westminster could not hire Bahnsen and had to
fire Shepherd.

Samuel T. Logan, Jr.

It is time to sail the Atlantic and join the New England
Puritans. This is the area of my own formal specialization, so I
get to play Toto. It’s curtain time!

Dr. Logan offers us “New England Puritans and the State.”
While he never mentions their existence, he is doing his best to
refute the essays in The Journal of Christian Reconstruction (Win-
ter 1978-79): Symposium on Puritanism and Law. In that
volume, Bahnsen wrote an essay, “Introduction to John Cot-
ton’s Abstract of the Laws of New England.” Rev. John Cotton was
asked in 1636 to write a law code for the colony. He did. It
became known as Moses His Judicials. ec later wrote an abstract
of the laws of New England. (It was reprinted in the same issue
of the JCR.) These laws were theonomic. The capital crimes of
the Old Covenant were included. It was never enacted into
law, but it did serve as a model for Rev. Nathaniel Ward’s
proposed civil code, which in turn was used by the Massachu-
setts General Court as a model for the 1641 Body of Liberties.

How important was Cotton’s model? Consider the evaluation
of Charles Lee Haskins, perhaps the major specialist in the
area of early Massachusetts law:

Cotton’s draft was never cnacted into law, and probably for
that reason its importance has been generally ignored. Never-
theless, there are several reasons why it deserves to be remem-
bered, To begin with, it was the first constructive effort to carry
out the mandate of the General Court and to produce a written
body of laws which would serve as a constitution for the colony.
Second, its heavy reliance upon Scripture provides an important
illustration of the strong religious influence which infused Puri-
tan thinking about law and the administration of justice. This
attitude was not confined to the Massachusetts leaders but ap-
