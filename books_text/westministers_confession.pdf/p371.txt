The Paralysis of the Parachurch Ministries 347

churches are therefore unwilling to fund with a portion of their
tithes the specialized activities of parachurch ministries. They
have forgotten the inevitable rule: those who pay the piper call the
tune.

This tight-fisted policy of non-support has encouraged the
parachurch ministries to make a kind of end run around the
churches, which radio, television, and direct-mail techniques
have made possible. Technologically, the parachurch ministries
have had great advantages since the 1920's.

The advantage is not simply technological; it is also person-
al. The parachurch ministries are frequently involved in deal-
ing with Christians in the broader world of culture. The
churches have self-consciously walked away from culture. For
example, churches have not funded scholarships to Christian
day schools, high schools, and colleges. They have ignored
explicitly Christian education, for this would raise questions of
explicidly Christian intellectual standards, i.e., biblical law. They
have not preached biblical law to the exclusion of “neutral”
natural law.

So, the sons and daughters of the faithful are sent by their
parents to distant, tax-funded, humanist collegiate pits. Who is
on campus to help them? Only the collegiate parachurch minis-
tries. Churches located close to the campuses do not cooperate
with churches back home to see to it that out-of-town students
are attending Church regularly. To do so would imply that the
local churches are legal representatives of distant churches — a denial
of independent (Baptist) ecclesiology - and also that the Church
possesses lawful sanctions. Thus, we have lost millions of college-
age former Church members to the humanists. Only the para-
church ministries are there to help.

Conclusion

If I were a donor toa parachurch ministry (or any other
kind of Christian ministry), I would specifically enquire of the
head of the ministry regarding his local Church membership
