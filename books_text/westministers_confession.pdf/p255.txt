Sie et Non: Judicial Agnosticism 231

“Christians have had to rethink what they mean by toler-
ance.”** T am not sure what Christians he has in mind. If we
should not tolerate abortion, then on what judicial basis should
we oppose it? Biblical law or natural law?

He asks. “Is it impossible to harmonize the theonomic vision
of a biblical society and the New Testament picture of a perse-
cuted church? Not necessarily.”" This is the resolution of sic
et non: the Great Maybe.

He says we must exercise creedal humility.’ (Did the or-
iginal Westminster divines exercise creedal humility?) He warns
that there will be ecclesiastical divisions, as there have been in
the past, if any group presses too hard. (Didn’t the Westmins-
ter divines understand. this?) In short, so what?

He raises some major questions: “What actions should evan-
gelical groups today take regarding civil disobedicnce over
abortion (e.g., Operation Rescue)? Should churches discipline
those who encourage disobedience of the state’s trespass laws?
Should they discipline those who refuse to take part in signific-
ant action designed to uphold God’s law? If the answer to one
of those questions is yes, then the evangelical church will be
just as divided as it was by abolition and the Civil War.” If
only this were the case! The evangelical church does not care.
A church that does not care does not raise questions like these.
It defers consideration of questions like these. It does what
President James Buchanan did about slavery and the pressures
for secession, 1857-61: nothing. The Church just wants to be
left alone in its slumber. In this sense, it is the seminary writ
large. But at least Davis asks some good questions. He just
never offers any answers them. This is the dilemma of judicial
agnosticism: it provides no answers.

98, Idem.

99, Ibid., p. 891.

100. 2id., pp. 392-95.
101. Jbid., p. 393.
