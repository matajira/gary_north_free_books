The Question of Millennialism 183

ply to mimic the fundamentalism of an earlier era: no liquor,
no cigarettes, no social dancing, and no movies. It is a scholarly
version of fundamentalism’s old refrain: “We don’t smoke; we
don’t chew; and we don’t go with the boys who do!” We cannot
seriously expect to recruit dedicated, intellectually scrious peo-
ple into “full-time Christian service” with a worldview that says
little more than “we don’t go to R-rated movies.”** So, what
good are these negative intellectual critiques? They serve as
outlets for highly frustrated Christian intellectuals to produce
other highly frustrated Christian intellectuals.

I shall put it as bluntly as I can: Amillennialism is an eschatol-
ogy that ignores the theological, intellectual, and social consequences of
the fact that both Christ’s resurrection and His ascension were events
in history. These were trans-historical events, too, but they were
events in history. Deny this, and you remove the very heart of
Christianity. If Christ did not rise in history, then our faith is
vain. Theological liberals, like the Pharisees before them, fully
understand this. They deny the historicity of Christ's resurrec-
tion in their attempt to destroy the Church. They are following
the rival “Great Commission” of the enemies of Christ, which
is recorded in the text of Matthew’s gospel immediately prior
to Jesus’ issuing of His Great Commission to the Church:

Now when they were going, behold, some of the watch came
into the city, and shewed unto the chief priests all the things
that were done. And when they werc asscmbled with the elders,
and had taken counsel, they gave large money unto the soldiers,
Saying, Say ye, His disciples came by night, and stole him away
while we slept. And if this come to the governor’s ears, we will

24. Tam not exaggerating about the continuing prevalence of such views. Writes
one critic of Christian Reconstruction, one of the leading pastors in England, who
holds the pulpit in Spurgcon’s Metropolitan Tabernacle: “In many cases it [Christian
Reconstruction] leads in a subtle way to worldliness. (After all, if Christians are
commissioned to take dominion over the arts, and so on, they had better start by
participating in them and enjoying them.)” Peter Masters, “Werld Dominion: The
High Ambition of Reconstructionism,” Sword & Trowel (May 24, 1990), p. 19.

 
