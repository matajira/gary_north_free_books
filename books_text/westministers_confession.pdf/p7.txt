TABLE OF CONTENTS

Foreword 6.0... c ee cece tee tee ees ix
Preface 6.60 eens xix
Introduction 2.0.6... cere teen 1

Chapter 1: The Question of Inheritance .......
Chapter 2: Calvin's Divided Judicial Legacy oe
Chapter 3: A Positive Biblical Confession Is Mandatory . . 73
Chapter 4: A Negative Confession Is Insufficient ..... .
Chapter 5: The Question of Law ...........0045
Chapter 6: The Question of God’s Predictable Historical
Sanctions 2.6... cece eee
Chapter 7: The Question of Millennialism . .
Chapter 8: Sic et Non: The Dilemma of Judicial

  

 

 

Agmosticism 666... eee eee eee eee eens 189
Chapter 9: Abusing the Past .............-5-0-005 234
Chapter 10: An Editor’s Task: Just Say No! ....,-.... 259
Conclusion 6.6... ee ee ete 295
Appendix A: H. L. Mencken’s Obituary of Machen .... 312
Appendix B: Honest Reporting as Heresy ........... 317
Appendix C: The Paralysis of the Parachurch Ministries 342
Appendix D: Calvin’s Millennial Confession .......... 349
Appendix E: Julius Shepherd ...........-...0000. 357
Books for Further Reading .............022 05 eee 361
Scripture Index ........ 0.60 e eee eee ee eee 369
Index ............-. . 372

    

About the Author .. 0.0.66 cece eee eee eee 386
