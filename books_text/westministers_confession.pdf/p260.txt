236 WESTMINSTER’S CONFESSION

Jaunched Civil War, the Westminster Confession was self-cons-
ciously stripped by its authors of most of Calvinism’s political
character. Yet the English Civil War was the product of a battle
between Calvinism and a strange alliance between Catholicism
and Arminianism. Without Calvinism, and particularly without
the Scottish Covenanters, it is inconceivable that the war would
have occurred, Marxist historians to the contrary notwithstand-
ing. English Calvinism of the seventeenth century could not be
contained inside the cloister until afier 1660.2>

The third constitution is the 1788 revision of the 1647 Con-
fession, and very few Presbyterians can tell you what was
changed, when, and especially why. They do not know that
these changes were first proposed during the same week and
in the same city that the Constitutional Convention had assem-
bled. With the exception of the brief account concerning the
links between these two events that I wrote in Political Polythe-
ism, no recent Calvinist historian has commented on it.4 When
the eighteenth-century Presbyterians became Whigs Ecclesiasti-
cal, the political and judicial character of Presbyterianism
changed radically. The Presbyterians became the black-robed
anointing army of the social philosophy and politics of the
Scottish Enlightenment. The confessional revision of 1788 is
the judicial foundation of modern Presbyterianism’s political
pluralism, yet this is seldom acknowledged publicly, and never
discussed with the historical background. In this instance, the
past has not merely been abused; it has been self-consciously
buried. The fundamental political fact of the 1788 revision is
also never discussed: it represented the triumph of Servetus. It is
politically unitarian. Not so in the seventeenth century.

The editors of Theonomy: A Reformed Critique did me a favor
when they bunched three essays together in one section, “The-

3. Cloistered Puritanism is the Puritanism of The Banner of Truth ‘Trust.
William Gurnall’s Christian in Complete Armour is the model.

4. Gary North, Political Polytheism: The Myth of Pluralism (Tyler, ‘Texas: Institute
for Christian Economics, 1989), pp. 548-50.
