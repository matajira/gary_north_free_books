Calvin's Divided Judicial Legacy 53

pertain also to morals.”!® The ceremonial laws are abrogated.
So are the judicial laws. “But if this is true, surely every nation
is left free to make such laws as it foresees to be profitable for
itself.”"” But he added this warning: “Yet these must be in
conformity to that perpetual rule of love, so that they indeed
vary in form but have the same purpose, For I do not think
that those barbarous and savage laws such as gave honor to
thieves, permitted promiscuous intercourse, and others both
more filthy and more absurd, are to be regarded as laws. For
they are abhorrent not only to all justice, but to all humanity
and gentleness.” Some civil laws are not binding civil laws,
What are the criteria of morally binding civil laws? Justice,
humanity, and gentleness. He summarized these three in the
term equity. “Equity, because it is natural, cannot but be the
same for all, and therefore, this same purpose ought to apply
to all laws, whatever their object.”* In short, “equity alone
must be the goal and rule and limit of all laws.” All of this was
utterly conventional, and had been since at least the twelfth
century, but especially after Aquinas. This was medieval Schol-
asticism. It did not survive Newton’s worldview, or Kant’s.

Van Til’s Half-a-Legacy

Nevertheless, no post-1788 Reformed Protestant theologian
officially abandoned Calvin’s view of civil law until 1973: R. J.
Rushdoony’s Institutes of Biblical Law. Rushdoony was unfamil-
iar with the Calvin’s sermons on Deuteronomy. He considered
only the Institutes’ defense of natural law theory. The philo-
sophical and ethical foundation of Calvin’s theory of civil law
was his view of equitable natural law. It was this assumption that
had been abandoned over four decades earlier by Van Til,
beginning in the 1920’s. It was Van Til alone who rejected all

16. Institutes (1559), IV:XX:14.
17, Tbid., TWiXXi15.
18, Tbid., WWiXXi16,
