Sic et Non: Judicial Agnosticism 221

munity of the people of God.”* (Then what did economic
restitution point to?) But now this community is defined strictly
as the Church. “The concern of Hebrews is with an offense that
can be committed only by a member of the covenant. Un-
der the new covenant the purity of the covenant community is
maintained not by physical sanctions but by spiritual discipline:
excommunication, not execution. . . .”7¢

Not to put things too graphically, but what if the State wants
to put ritual prostitutes at the foot of our communion tables
every Sunday morning? Preposterous? Maybe, although I seem
to remember something about Antiochus’ sacrificing pigs in the
temple some years back (I Macc. 1:47). What about the purity
of the community then? If we define the community as mem-
bers of the institutional Church, then what protects the purity
of whatever goes on inside its four walls?

If Johnson should suggest that we Christians could then
appeal to “natural law’s” protection of private property — en-
forced under which angels’ mediation? - then I have another
question: What if the civil government allows a nationally fran-
chised, for-profit, ritual prostitution center across the street
from the local church? What then? Silence? Silence out of
respect for the Epistle to the Hebrews?

All right, I am using hyperbole. I will stop (for a moment or
two). Analogous question: What if the government allows a
nationally franchised, for-profit abortion clinic across the street
from the local church? Now I am being realistic. No exaggera-
tion here, except possibly for zoning law considerations. What
then? Silence? Silence out of respect for the Epistle to the
Hebrews?

It is time to end the silence at Westminster. But judicial
agnosticism leads directly to silence on such controversial issues
as these, It leads to a muddled confession.

 

 

p- 191.
76. Tbid., p. 189,
