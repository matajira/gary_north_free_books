An Editor's Task: Just Say No! 285

A traditional gold standard requires the State to define its
official currency in terms of weight and fineness of gold, and
then to buy and sell gold at this defined price. This gets the
State into the money business. There is no warrant for this
practice in the history of Old Testament Israel. The New Testa-
ment example is the Roman Empire - not a morally uplifting
example.

A traditional gold standard is better than a fiat (unbacked)
money standard, but it transfers too much sovereignty to the
State. It also allows the State to “change the rules” at its own
convenience, that is, to redefine the currency unit (usually by
defrauding present holders of the paper currency: less gold per
currency unit), or to cease allowing citizens to make withdrawals.
Better to have the State policing private issuers of gold and
warehouse receipts to gold, and then to collect its taxes in a
specified form of private currency. Under such an arrangement,
the politicians have a greater incentive to police the State’s
source of tax revenues than they do to police the State’s own
monetary practices.

What freedom produces is parallel standards. Various forms of
money compete with each other. The State is to establish no
fixed, bureaucratic price between moneys. The decisions of free
men can then determine which form or forms of money become
most acceptable, There is nothing magic about money. It is
simply the most marketable commodity. The market establishes this,
not the coercive power of the State. Money is the product of volun-
tary human action, not of bureaucratic design. Money is the product
of freedom, and it reinforces freedom.

Yet Mr. Muether equates my thesis in Honest Money with a
biblical defense of the gold standard. In short, he faked the
reference. He simply made it up. It made me look like a fool,
he imagined: building my case for biblically honest money on
the biblical Jaw — not a suggestion — against false weights and

45, Gary North, Honest Money: The Biblical Blueprint for Money and Banking (Ft.
Worth, Texas: Dominion Press, 1986), pp. 107-8.
