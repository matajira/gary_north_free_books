12 WESTMINSTER’S CONFESSION

Verbal Shock Therapy

Sometimes the verbal shock therapy of harsh rhetoric does
persuade an opponent. But the fact is, very few opponents are
ever swayed by anything that the pioneer of a new viewpoint
says. Thus, the use of sharp rhetoric is adopted for reasons
other than persuading one’s opponents. It is adopted to per-
suade one’s followers or those not yet committed. It is used to
tally your troops more than it is to disperse your opponent’s
troops. General George Patton’s famous speech to his troops —
a toned-down version was used to begin the 1970 movie, “Pat-
ton” — was not delivered to persuade the Germans to surren-
der, Neither was Theonomy: A Reformed Critique written to per-
suade Bahnsen to abandon theonomy for the judicial grab-bag
that Westminster Seminary’s faculty teaches these days. It was
written to persuade students that the faculty really does have
legitimate theological reasons for not adopting theonomy, and
more to the point institutionally, for refusing to hire the only
professionally certified, Ph.D.-holding Calvinist philosopher
and follower of Van Til to fill Van Til’s position.

Why am I the theonomists’ main practitioner of confronta-
tional rhetoric? First, Rushdoony does not respond to his critics
in print, politely or otherwise. He never has. I call this the
Dwight Eisenhower strategy. Second, Bahnsen is still governed
by the etiquette of the American university community. He
writes as if rigorous logic and masses of Bible verses might
conceivably persuade his opponents. It never seems to, but he
keeps trying. Devotion! Third, Gentry also prefers classroom
ctiquette. I think it is just a matter of taste with him. Fourth,
DeMar has only recently decided that a decade of lies and
misrepresentations by our opponents, especially dispensational-
ists, is not ethically random. He is not yet fully comfortable
with my approach. While he is beginning: to catch on, he is
hampered by being a nice guy. Fifth, Sutton is also a nice guy.
Chilton, in Productive Christians in an Age of Guilt-Manipulators,
proved himself to be a rhetorical master, but he no longer
