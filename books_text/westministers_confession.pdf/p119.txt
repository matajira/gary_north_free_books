A Positive Biblical Confession Is Mandatory 95

over Althusius and Richard Baxter. In the late nineteenth
century, Darwinism triumphed over Newton’s presumed provi-
dential order.* What the Westminster Assembly had begun,
no other self-consciously Christian organization extended. It
was the last of the great confessions.

In the mid-nineteenth ‘century, Princeton Seminary had a
broader view of Christian civilization, a view reflected in its
scholarly journal, but after the era of the American Civil War,
academia began to walk down the ever multiplying, ever nar-
rowing pathways of specialization. No longer would Presbyteri-
an seminary journals run lengthy reviews of political studies
such as Alexis de Tocqueville’s The Old Regime and the Revolution
or Francis Lieber’s On Civil Liberty and Self-Government.?> The
Ph.D. was imported from Prussia in the late nineteenth centu-
ry; a generation later, so was the kindergarten. The broader
academic vision faded. No institution dared to speak with a
unified voice except the State. There was a universal deferral
of authority to the State. And so the State has inherited, gener-
ation by generation.

Confessions and Confrontation

Theonomy: A Reformed Critique is not a major source of con-
cern for those of us who have been struggling with these larger
questions. It is more of a testimony to what Westminster Semi-
nary has been unwilling to do than a challenge to what we
theonomists have been doing self-consciously for the last eight-
een years. The book is a negative critique, and a negative cri-
tique is next to useless if it is not accompanied with a compre-
hensive alternative to whatever is being criticized. As I never
cease reminding our critics, they cannot beat something with noth-

25. Gary North, The Dominion Covenant: Genesis (2nd ed; Tyler, Texas: Institute
for Christian Economics, 1987), Appendix A: “From Gosmic Purposelessness to
Humanistic Sovereignty.”

26. Princeton Review, XXX (Oct. 1858), pp. 621-45.
