306 WESTMINSTER’S CONFESSION

doubt be ready to contribute another sixteen essays to my pro-
posed symposium, Intrusionism: A Reformed Critique. They will
demonstrate exegetically how they have broken completely
with his intrusionism, yet they will not cite a single Bahnsen-
like argument to justify this break. The Institute for Christian
Economics will pay for all typesetting and printing expenses,
and it will donate one copy each to any 500 libraries in the
United States. 1 just hope this project, once accepted, will not
take seventeen more years.

This project will be accepted when shrimps learn to whistle.

Conclusion

The editors of Theonomy: A Reformed Critique had a responsi-
bility. They were to assemble essays by the Westminster faculty
that would respond forthrightly to the substance of theonomy,
meaning cither (1) the work of the entire Christian Reconstruc-
tion movement, or (2) Greg Bahnsen alone. They owed it to
their authors and their readers to specify which task they had
undertaken. They did neither. The essays fire away at Bahnsen
in an unsystematic fashion, yet Keller and Muether take on
several reconstructionists other than Bahnsen, The book does
not present a series of concentrated, point-by-point cases
against Bahnsen’s thesis, with each contributor using his spec-
ialized knowledge to attack one aspect of Bahnsen’s thesis.
Neither does each of the essays systematically survey a particu-
lar aspect of Christian Reconstruction as a whole. What we find
is a slap-dash collection of unfocused essays that for the most
part have only one message: “We just don’t like theonomy!”

Machen left a legacy to Westminster, a legacy of moral in-
tegrity, personal courage, and impeccable scholarship, in that
order. Muether’s essay is a disgrace: no integrity. It shows
recklessness, not courage. It shows zero scholarship. Keller's
piece is only marginally better. The book as a whole generally
reveals sloppy work, yet it took five years to get it out. Maybe
my book is not great, but it took five months, and I typeset it,
