9

ABUSING THE PAST

The appeal to the ‘ancestral constitution’ satisfies the canon that it
must ‘seem rational and persuasive’, that both its proponents and those
they persuaded could, if pressed, defend themselves ‘by some rules of logic
and evidence that they would themselves accept’. It is therefore a legiti-
mate historical exercise to examine the argument seriously... .

M. I. Finley (1975)!

‘We come now to the topic in which I can claim professional
ceritification: history. The last two decades of my life have been
spent rather like the character described in a Stephen Leacock
story: “He leaped onto his horse and rode off in all directions.”
Multi-directionalism is the Christian Reconstructionists’ version
of Vern Poythress’ multi-perspectivalism. Such furious omnidir-
ectional riding is the burden of Christian Reconstructionists.
Everything needs reconstructing, so everything becomes a
reformer’s snare and temptation.

History, however, is where I know best how the game is
played. There are at least a few legitimate reasons for going to
graduate school, but the best one is that if you pay careful

1, ML. Finley, The Use and Abuse of History (New York: Viking Press, 1975), p.
35.
