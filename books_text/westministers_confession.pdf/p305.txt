An Editor's Task: Just Say No! 281

know that I’m pretty good in the barb field. But I have re-
moved all of them. Mr. Muether’s piece reflects not just spirit-
ed polemical discourse, which I always appreciate, but a nearly
pathological hatred. It is not just that the essay is vitriolic. Who
am I, after all, to complain about vitriol? No, his essay is a
personal attack on my church, my intellectual integrity, and my
commitment to scholarship. I deeply resent it.

The Initial Phase

The initial phase of the essay is not evil - silly, perhaps, but
not evil. He first raises a “sociological question”: “If theonomy
is the consistent teaching of Scripture and the Westminster
Confession of Faith, why does it seem that we have discovered
it only now, in late twentieth-century America? Why not, say, in
seventeenth-century England or in nineteenth-century Hol-
land?”** Notice how he drops a very important modifier: sev-
enteenth-century New England. There he has a problem, as his
colleague Dr. Logan has in Chapter 15. Theonomy was the
operating foundation of the Puritan commonwealth in Massa-
chusctts in the first generation, 1630-60. This is why Roger
Williams fled and invented political pluralism.

The answer to his question can be found in two words: Van
Til. He might have written — indeed, he is implicitly writing on
every page of his essay: “If natural law theory is inconsistent
with the teaching of Scripture, why does it seem that we have
discovered this fact only now, in late twentieth-century Ameri-
ca? Why not in seventcenth-century England or in nineteenth-
century Holland?”

In Mr. Muether’s system, as I have argued in Chapter 6,
there is no place for progressive sanctification of the Church in history.
The redemptive-historical world of the Old Testament, tied as
it was to God’s predictable covenantal sanctions in history, ends

38. Muether, p. 245.
