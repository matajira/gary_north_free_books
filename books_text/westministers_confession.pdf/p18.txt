xviii WESTMINSTER’S CONFESSION

book. It does not have to be a great book. It just has to be
better than Theonomy: A Reformed Critique.

I do not have to cover everything. What I neglect will be
covered by Greg Bahnsen in the book I commissioned him to
write, No Other Standard. What he neglects will be covered by
the contributors to Theonomy: An Informed Response. We will
present in three volumes our case for theonomy and against
Westminster’s critique.

I do not like to write or publish exclusively defensive books.
1 much prefer to take the offensive. (A lot of people have said
that I am offensive, and I have to agree.) It is my deeply felt
belief that you cannot beat something with nothing. It is not
sufficient to show here that Westminster Seminary has self-con-
sciously gone down a pathway leading to a cultural dead end.
I have to point out the correct path and explain why it is cor-
rect. I have attempted to do this in Westminster's Confession.

But Westminster’s Confession is intended to be more than a
monograph on how a particular Calvinist institution sold its
birthright for a pot of message. What Westminster Seminary
has done is a representative example of a much larger process
that has been going on for well over three centuries. It is a case
study of how the intellectual leadership of Calvinism refuses to
adopt the heritage that God has graciously given to Calvinists,
and only to Calvinists. Instead, the leaders return again and
again to the fleshpots of academic Egypt. They also allow their
enemies to set the covenantal war’s agenda. Worse; they submit
to certification by their enemies before they even begin to do
battle. This has been going on from the very beginning of Cal-
vinism. It is time to call a halt to the process. Westminster’s Con-
fession is a warning to Calvinist leaders of the future: “Just say
no.”

Cornelius Van Til taught us how to-say no. Let us follow his
good example.
