A Positive Biblical Confession Is Mandatory 75
Christendom

There is little debate among Christians regarding the legiti-
macy of a confession in the first three cases: personal, ecclesias-
tical, and familial covenants. But with the triumph of unitarian
theology in the U.S. Constitution, followed by the rise of secu-
Jar humanism, accelerating in the twenticth century, American
Christians have begun to doubt the legitimacy of a Christian
civil oath. Such oaths are expressly forbidden by the U.S. Con-
stitution (Article VI, Section III) - the opposite of state oaths
prior to the American Revolution.* This means that Christians
have abandoned any idea of the biblical covenantal require-
ment for a positive Christian confession for civil government.
While they will defend the idea of a biblical blueprint or re-
quired framework for the Church and the family, they assume
that there is no similar blueprint for civil government. The
conservative American affirms the U.S. Constitution as the valid
model — a model bordering on the divine — and the liberal
generally agrees, although there will be a great debate about
the proper interpretation of the Constitution. But, apart from
the theonomists, there is no Trinitarian Christian group still
defending the Puritan ideal of a theocratic republic. The
Church is regarded as theocratic; the family is regarded as
theocratic (laws against polygamy indicate this); but the State is
seen as religiously neutral, and the Bible is said to be devoid of any
model for the State. This is Westminster’s confession,

Then what of society in general? What kinds of positive
confessions are appropriate for a Christian social order? What
kind of society should the four covenants — personal, ecclesiasti-
cal, familial, and civil - produce as history draws closer to the
final judgment? Calvinists prior to 1660 debated this issue.
They spoke to these issues in the name of God. They no longer
do. Worse; they no longer regard it as either possible or reli-

4. Gary North, Political Polytheism: The Myth of Pluralism (Tylex, Texas: Institute
for Christian Economics, 1989), ch. 9.
