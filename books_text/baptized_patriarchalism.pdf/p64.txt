56 BAPTIZED PATRIARCHALISM

could bring visible sanctions, which served as public confirma-
tion of their covenantal authority under God.

Rushdoony protests — against whom, he does not say: “It is
much wiser to see this chapter as a whole in terms of a mandate
to effect restoration, and, failing that, to proceed with separa-
tion.” This is a conventional interpretation of church disci-
pline, as Matthew 18 indicates: at every step of the process, the
goal is restoration. But the final earthly incentive for church
members to restore relations between each other always rests on
the church’s possession of sanctions: the keys of the kingdom.
Christians cannot escape from the church's sanctions.

The fundamental covenantal question is this: Does the insti-
tutional church possess these keys exclusively? Matthew 18:15-18
emphatically says yes. Rushdoony refuses to say one way or the
other in this section: “Fourth, separation is excommunication,
separation from the table of the Lord and from its fellow-
ship.” ‘This is correct, obvious, and partial. Who imposes this
Sanction? At this point, he returns to his carly assertion of the
unity of verses 15-20. He invokes verses 19-20: “Again I say
unto you, That if two of you shall agree on carth as touching
any thing that they shall ask, it shall be done for them of my
Father which is in heaven. For where two or three are gathered
together in my name, there am I in the midst of them.” But if
there is textual unity, it must be a judicial unity.

Who Are We?

Pay very close attention to what has to be the trickiest
sleight-of-word trick in Rushdoony’s long career. He adds the
key word, we. “Fifth, in terms of these ministcrial powers, we
have great authority, of binding and loosing. H two or three
gathered together in Christ’s name, either as a church court or
as simple belicvers, agree on something in faithfulness to Scrip-

98. Ibid., p. 758,
99. Ibid.
