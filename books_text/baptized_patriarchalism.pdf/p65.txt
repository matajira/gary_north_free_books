Baplized Patriarchalism 57

ture, we can bind and loose men.” A hierarchical ecclesias-
tical appeals court system in Matthew 18:15~-18 has now become
two or three people — which could include women — handing
down binding judicial pronouncements.

I ask: ‘To whom? On what judicial basis? Ordained by what
judicial court? With what further appeals court above them?
With the authority to exclude from whose communion table?

Rushdoony’s assertion is so bizarre that he feels compelled to
add: “Now normaily this is a function of church authori-
ties."""! Normally? Do I understand by this that occasionally
a couple of people can get together and hand down binding
judicial pronouncements? This is what he says.

Yo measure the degree of absurdity here, consider his inter-
pretation in terms of a civil court. There must be a trial. In
whose court? In terms of whose law books? By which prece-
dents? Interpreted by what judge? And who will impose the
sanctions? A couple of citizens who get together in society’s
name? Apart from radical anarchism, who would propose such
a judicial system? It would lead to warlordism: the triumph of
the most powerful group. Warlordism is patriarchalism in the civil
vealm. But he affirms patriarchalism in the ecclesiastical realm.

It gets worse: “There is a ministerial binding and loosing
required of all of us. We cannot refuse to excommunicate some-
one because we are attached to them [sic}.”" We? Who are
we? His words are clear: “all of us.” He adds: “If two men have
this power, how much more the church?” But two men — and
on what basis could he here exclude women? - do not have this
declaratory power. Church members are not allowed to go
around handing down excommunications apart from a church
trial. Neither are family members.

100. Tbid., pp. 758-59.
101. Bid., p. 759.
102. Ibid.
