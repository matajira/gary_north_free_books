Baptized Patriarchalism 21

sacraments. The Anabaptist view denies that the sacraments are
more than memorials: authority through naming (nominalism).
Calvin rejected both views. In his theology, the sacraments are
neither an aspect of Greek realism (“secret powers”)* nor
Greek nominalism. They neither infuse grace, as if grace were
a substance, nor do they serve merely as symbols. His theology
was judicial, and so was his view of the sacraments. He said that
they are signs and seals of the covenant, which is a judicial
bond between God and man (vertical) and among all those who
have been placed under these signs and seals (horizontal). The
sacrament is “an outward sign by which the Lord seals on our
consciences the promises of his good will toward us in order to
sustain the weakness of our faith; . . “8 The sacrament of
baptism does not by itself save men, but when combined with
preaching and saving faith, it heals men from sin.”

The element of faith is given to men through God’s sover-
eign grace. But this is God’s work, not the work of the church.
The church baptizes, but it does not impart the grace of saving
faith. This is the heart of Calvinism: saving faith is imparted to a
person exclusively by God's grace. This is what the five points of
Calvinism teach. “But the sacraments properly fulfill their office
only when the Spirit, that inward teacher, comes to them, by
whose power alone hearts are penetrated and affections moved
and our souls opened for the sacraments to enter in.”

Calvin affirmed the necessity of saving faith in the individual
in order for the sacraments to have a positive effect in the
process of salvation, but he made it clear that this saving faith
is God’s work, not the church’s work. Thus, the sacraments are
external and judicial signs insofar as they are marks of mem-
bership in the church. The church does not have the authority

25. Calvin, Institutes, 1V:xivil4, p. 1289.
26. Ibid., FVixix:1, p. 1277.
27, Hid., PV:xix:3, p. 1279.
28. Ibid., IV:xix:9, p. 1284.
