BAPTIZED PATRIARCHALISM

The family is central to the covenant and therefore to every Christian
institution, church, state, school, and all things else.

R. J. Rushdoony (1984, 1994)!

In this book, I show that there has been a major shift in R.
J. Rushdoony’s theology. This shift parallels his adoption of a
doctrine of the church totally at odds with what the Westmins-
ter Confession teaches, and every other historic confession
teaches. When those whom he had recruited in the 1960’s and
1970’s refused to adopt it — all of us refused — it split the Chris-
tian Reconstruction movement into two visible camps: Vallecito
(anti-ecclesiastical} and ‘Tyler (sacramental church). (A third
group, more Presbyterian, is unorganized and underfunded.)

‘The foundations of Christian Reconstruction were laid in the
1960's, prior to Rushdoony’s exegetical work on biblical law. He
wrote almost a dozen books on social theory, history, political
theory, theology, and education before his wrote The institutes of
Biblical Law (1973). In these negative critiques, he made a de-
finitive break with the theology and sociology of humanism.

This Independent Republic had been completed in the summer
of 1962; it was published in 1964. After the publication of The

1. Rousas John Rushdoony, Systematic Theology, 2 vols. (Vallecito, California: Ross
House, 1994), II, p. 678 Te chapter seems to have been. written prior to 1984.
