38 BAPTIZED PATRIARCHALISM

ritual for a man to kiss his wife before leaving for work. . . .”7!

This is “a minor humanistic ritual,” as he says. But God re-
quires rituals, he says. “Baptism and communion are thus clear-
ly rituals. . . . Furthermore, ritual cannot be limited to the
sacraments. All worship involves ritual. Grace said before and
after meals is a ritual.”” Yes, yes, yes, but so what? /udicially
speaking, so what? The theological question is: In what way is a
sacrament not the same as kissing your wife in the morning?
Even more important for someone who argues, as Rushdoony
does, that the sacrament of baptism and the sacrament of the
Lord’s Supper are family rites, how ave these sacraments different
from the ritual of sex? This is the problem of familism and fertil-
ity cult religion. Judicial distinctions between common rituals
and the sacraments are not “Greco-Roman”; they are biblical.
What is missing in Rushdoony’s discussion of ritual is the
crucial covenantal mark: an oath. There are lots of rituals, but
only certain ones possess the unique judicial character of being
oath signs. Baptism is one; the Lord’s Supper is the other. They
are rituals; they are also judicially distinct from all other New
Covenant rituals. They have eternal judicial authority. Other
church rituals do not. But Rushdoony does not so much as hint
at these judicial distinctions, yet Rushdoony is known for his
commitment to biblical law. There is a hidden agenda here.

Hands and Wallets

Under the section on “The Laying on of Hands,” Rushdoony
writes: “The sacraments are unique and exclusive ordinances of
the Lord.” Notice what he does not say: that they are unique
and exclusive ordinances of the institutional church. This calculat-
ed omission makes a huge difference for theology.

TL. Bid, p. 711.
72. Ibid., p. T12.
73. Ibid., p. 716.
