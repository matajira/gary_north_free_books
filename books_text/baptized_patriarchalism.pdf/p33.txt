Baptized Patriarchalism 25

virtual non-existence of the Holy Spirit.”** Those who hold a
hierarchical view of church government are members of a
modern Sanhedrin, he says. “We must separate ourselves from
modern Sanhedrins.”* Yet since 1974 he has been a priest in
a tiny two-congregation Episcopal denomination.

Abraham’s Faith: The Issue of Subordination

Abraham gave his tithe to Melchizedek (Gen. 14:20). This is
crucial for any discussion of the tithe and the church. Rush-
doony never comments on this verse, for obvious reasons: he
denies that the institutional church has a lawful claim on any-
one’s tithe. Melchizedek was the priest of Salem, and Abraham
was under his ecclesiastical authority. It was from Melchizedek
that Abraham received a meal of bread and wine (v. 18). The
author of Hebrews traces the New Covenant priesthood back to
Melchizedek (Heb. 7). Rushdoony traces it back to the Levites,
as we shall sec. This has major implications for his ecclesiology.

Rushdoony interprets Abraham in very different categories:
personal faith, not tithing and communion. He discusses Abra-
ham’s faith under Section 2, “Faith and the Church.”*” The
problem is, his discussion does not exegetically tie Abraham’s
faith to ecclesiology. It could as easily be titled, “Faith and the
Family,” “Faith and Civil Government,” or “Faith and the King-
dom.” What Rushdoony needed to do, but did not do, was to
show how the faith of Abraham (and the others listed in He-
brews 11) was in some way uniquely and judicially tied to Rush-
doony’s definition of the church. Having failed even to attempt
to make this connection, Rushdoony gratuitously concludes: “It
is this faith which must mark the church. Too often the church
identifies faith with itself, and faithfulness with loyalty to the
institutional forms and practices. It then secks conformity rather

35. Ibid., p. 4.
36. Ibid., p. 8.
37. Systematic Theology, pp. 671-75.
