36 BAPTIZED PATRIARCHALISM

Presbyters

He briefly discusses elders, presbyters, and bishops. ‘hen he
adds this comment: “Our concern here, however, is less with
the office and more with the function. Our thinking is much too
colored with the world of Greco-Roman thought and the priori-
ty of emphasis on office.” This is a rhetorical ploy: blaming
pagan Greco-Roman thought for an idea that has its origins in
the traditional, universal interpretation of the orthodox church-
es, i.e., that church office is the primary focus of the words
presbyter and bishop. He is forthright: he is not very concerned
about judicial office; he is extremely concerned with function.
This is the heart of his ecclesiology: the substitution of broad
kingdom functions for specific church office. Why? One reason
is money. Once he switches from judicial office to function, and
persuades the reader thai the sacraments are not a monopoly of the
institutional church, he then lays claim to the tithe.** The under-
lying practical issue is access to the tithe. He conceals this con-
cern by what appears to be a legitimate concern over function.

“In Scripture, both office and function are spoken of as God’s
calling.” So what? I have a calling. I do not have a claim on
any Christian’s tithe. He asks: Who is a bishop? Not a novice,
he says. A man of experience, he says.*” A man of good repu-
tation. But most of all, a teacher. “Such a function is assumed
to be a task of the bishop or presbyter. What Paul here stresses
is the aptitude and the ability to teach.” So, a bishop is primari-
ly a teacher. So is Rushdoony. (Think tithe.) Gone is the Instit-
ules’ discussion of the early church’s bishop as a regional pastor
to local presbyters (p. 745). What is glaringly absent is the
suggestion that a presbyter or bishop is a sanctions-bringer.

63. Ibid., p. 706.

64. For a critique, see North, Tithing and the Church, Part 2.
65. Rushdoony, Systematic Theology, p. 707.

66. Ibid.

67. Ibid., p. 708.
