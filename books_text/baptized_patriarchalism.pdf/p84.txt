76 BAPTIZED PATRIARCHALISM

keys of kingdom, 3, 11-12,
53-56, 58

language, 16n

law (Roman), 2

laying on of hands, 38-40, 58,
65

Levi, 47

Levites, 25, 39, 40

liberals, 10

loyalty, 2

Mafia, 3, 4
marriage, 13
matriarchalism, 4, 66
Melchizedek, 25, 39
Miller, C, John, 20
minister, 33-35, 39
mummy factory, 26
mysticism, 22

New Covenant, 30
New World Order, 4

oath

baptism, 50
church, 4
covenant, 52
covenantal, 38
sanctions, 50
yoking, 16
office, 62-63
ordination, 58
Orthodox Presbyterian Church,
66

paganism, 51
Passover, 46-47

Patriarchalism
blood covenant, 48
matriarchalism, 4, 66
sacraments, 3-4
priesthood
authority, 41
cutting off, 41-42
hierarchical, 29-30
Melchizedek, 25
public schools, 3, 7
Puritanism, 23

quotas, 17n

racialism, 13-17
restoration, 56
revivalism, 23, 24
ritual, 37-38
Rome, 1, 2,6

sacraments

Calvin on, 21-22

church vs. family, 3-4
family, 50-51

ordinances of the Lord, 38
Salvation Army, 26
Sanhedrin, 25

sanctions

minister, 35

oath, 50

priests, 43

Rushdoony, 67-68
Sanctions, 8
self-government, 31-32
social healing, 7

social theory, 5, 12

state (family &), 2-8, 6, 7, 10
