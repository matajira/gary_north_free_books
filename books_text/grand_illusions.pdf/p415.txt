69.

70.
71.

72,
73.
74.
75.

1.
77.

78.

79.
}. See Robin Love Fox, Pagans and Christians (New York: Alfred A. Knopf, 1986).
81.
82.

83.
. Bid.
85.
. Whitehead, Revolution, p. 161.
a7.

88,

a9

90.
91.

End Notes 395.

. Martin Talbot Graves, Data on File: The American Political Scene (Dallas: Christ

Over All, 1985), p. 28.

. Tid.
. Wid., p. 29.
. Did.
. Lbid.

See R. F. McMaster, Jr, No Time for Slaves (Phocnix: Reaper Publishing,
1986).

. Graves, p. 32.

. Tid.

. Bid.

. See Stephen R. Hightower, Committees of Correspondence (Memphis: The Sound

of Liberty, 1967)

See Dennis J. Horan, Edward R. Grant, and Paige C. Cunningham, Abortion
and the Constitution (Washington, DC: Georgetown University Press, 1987).
Ibid., pp. 265-268.

‘Thomas Paine, Common Sense and Other Essays (New York: Signet Classics,
1977), p. 19.

Harold K. Lane, Liberty! Cry Liberty! (Boston: Lamb & Lamb Tractarian
Society, 1939), p. 31.

Abraham Lincoln, Speeches, Letters, and Papers: 1860-1864 (Washington, DC:
Capitol Library, 1951), pp. 341-342

Ronald Reagan, Abortion and the Conscience of a Nation (Nashville, TN: Thomas
Nelson, 1984), p. 15.

Ibid, p. 16.

Ibid, p. 18.

Quoted in, John W. Whitehead, The Separation Ittusiun (Milford, MI: Mott
Media, 1977), p. 21.

See John C. Galhoun, A Disguisition on Government (Indianapolis: Bobbs-Merrill
Educational Publishing, 1953).

McLellan, pp. 127-128.

Schaeffer, Manifesto, p. 92.

Michael Pesce, Legal Documents of the Roman Empire (New York: Black, Albert-
son, and Bloesch, 1959), pp. 46-47.

fbid., p. 47.

Itid., p. 48.

See Randall A. Terry Operation Rescue (Springdale, PA: Whitaker House, 1988):
and Gary North When Justice Is Aborted (Tyler, TX: Institute for Christian Eco
nomics, 1988).

Herbert Schlossberg, Idols for Destruction: Christian Faith and Its Confrontation With
American Society (Nashville: Thomas Nelson, 1983), p. 295.

Ibid.

See “1988-- Pro-Life Momentum Builds,” Action Line 12:1, February 15, 1988.
See Business Week, February 29, 1988.
