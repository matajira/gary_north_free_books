To the Uttermost; The International Legacy 201

and young Peter Romanov became Czar of all the Russias—
until 1848—when the calamitous Marxist rebellions in Paris,
Rome, Venice, Berlin, Parma, Vienna, and Milan were finally
squelched— Europe was racked by one convulsive struggle after
another. During those two centuries, the cause of Christian unity,
veracity, and temerity wore a Khazar face—buffeted by the
Austro-Prussian Wars, the Napoleonic Wars, the American War
of Independence, the Persian-Ottoman Wars, the Sino-Russian
Wars, the French Revolution, the Greek and the South American
Wars of Independence, and the Mogul Invasions. The entire
culture seemed to be driven by an Arimathean impulse to bury
disparaged truth.

At last though, a hush of peace fell upon the continent during
the Victorian Age: Pax Britannia. And within the span of a gen-
eration, the message of Christ and the benefits of a Christian
culture and law code were impressed upon the whole earth.

Three great revolutions—beginning first in England and
then spreading throughout all the European dominions—laid
the foundations for this remarkable turn of events. The first was
the Agricultural Revolution. The replacement of fallowing with
leguminous rotation, the use of chemical fertilizers, and the
introduction of farm machinery enabled Europeans to virtually
break the cycle of famine and triage across the continent for the
first time in mankind’s history. The second was the Industrial
Revolution, Manufactured goods and the division of labor created
a broad-based middle class and freed the unlanded masses—
again, for the ‘first time in human history. The third was the
Transportation Revolution. At the beginning of the nineteenth
century, Napoleon could not cross his domain any more efficiently
than Nebuchadnezzar could have six centuries before Christ. By
the end of the Victorian age, men were racing across the rails
and roads in motorized vehicles of stupendous power, they were
crashing over and under the waves of the sea in iron vessels of
enormous size, and they were cutting through the clouds in in-
genious zeppelins, balloons, and planes.

Suddenly, the earth became a European planet, Whole con-
tinents were carved up between the rival monarchs. With a
thrashing overheated quality —in which charity and good sense
are sometimes sacrificed for the practical end of beating the
