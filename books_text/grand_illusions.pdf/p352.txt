332 Granp ILLusions
F. LaGard Smith, When Choice Becomes God (Eugene, OR: Harvest
House, 1990).

* Joseph Sobran, The Averted Gaze: Liberalism and Fetal Pain (New York:
The Human Life Foundation, 1984).

* Joseph Sobran, Single Issues: Essays on the Crucial Social Questions (New
York: The Human Life Press, 1983).

Ben J. Wattenberg, The Birth Dearth: What Happens When People in Free
Countries Don't Have Enough Babies (New York: Pharos Books, 1987).

* Dr. and Mrs. J. C. Willke, Abortion: Questions & Answers (Cincinnati,
OH: Hayes, 1985).

Dr. and Mrs. J. C. Willke, Handbook on Abortion (Cincinnati, OH:
Hayes, 1979),

J. &. Willke, Abortion and Slavery: History Repeats (Cincinati, OH:
Hayes, 1984).

Ellen Wilson, An Even Dozen (New York: Human Life Press, 1981).
* Curt Young, The Least of These (Chicago, IL: Moody Press, 1984).

Abortion Industry

William Aramony, The United Way: The Next Hundred Years (New York:
Donald I. Fine, 1987).

The Boston Women’s Health Collective The New Our Bodies, Our Selves:
A Book By and For Women (New York: Simon and Schuster, 1984).

William Brennan, Medical Holocausts: Exterminative Medicine in Nazi
Germany and Contemporary America (New York: Nordland, 1980).

Diana Burgwyn, Marriage Without Children (New York: Harper & Row,
1981).

Claire Chambers, The Siecus Circle: A Humanist Revolution (Belmont,
MA: Western Islands, 1977).

Allan Chase, The Legacy of Malthus: The Social Costs of the New Scientific
Racism (New York: Alfred A. Knopf, 1975).

Lindsay R. Curtis, Glade B. Curtis and Mary K. Beard, My Body —
My Decision (New York: Signet, 1987).
