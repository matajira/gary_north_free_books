396 Granp ILLusions

Chapter 14—Altars for Construction: An Agenda for the Future

1, “O come, all ye faithful.”
2. Hilaire Belloc, The Cruise of the Nona (London: Constable & Co., Ltd., 1925),
p. 51.

3. The phrase and the concept, though ancient, was first introduced to me by
Christian economist Dr. Gary North. See, for example, his Unconditional Surrender
(Tyler, TX: Institute for Christian Economics, 1981, 1988).

. Glaude L. Fevre, L'Ecriture de Thyrasymackos (Paris: Parbleu Libraire-Editeur,
1963), p. 61.

. Tbid., p. 62.

. Tbid., p. 61.

. Ibid.

. Ibid., p. 74.

}. Quoted in Hardin Blanchard, The Forerunner of the Reformation: An Analysis of the
Hussite Movement (London: Lollard Publication Board, 1927), p. 137.

10. Zbid., p. 138.

11. Ibid.

12. Ibid.

13, Ibid., p. 139.

14. Ibid.

15. Ibid., p. 172,

16. Quoted in G. Holden Pike, Charles Haddon Spurgeon: Preacher, Author, and Philan-

thropist (New York: Funk and Wagnalls Company, 1892), p. 10.
17. Tid., p. 1.
18. Ibid., p. 9.
19. Ibid., p. 11.
20. Ibid., p. 9.
21. fbid., pp. 8-9.
22, Ibid., p. Mh.
23. Ibid., p. 12.
24. Anatol Kabosili, Faith at Work (Boston: K. L, A. Monastic Orders Publication
Review, 1927), p. 63.

25. Ibid., p. 92.

26. Maria L. H. Blumhardt, Pioneer to Africa: The Life of Otto Blumhardt (St. Louis:
Wittenburg Missionary Press, 1949), p. 6.

27. See George Grant, Bringing in the Sheaves: Transforming Poverty into Productivity
(Atlanta: American Vision Press, 1985).

28. George Grant, Jn the Shadow of Plenty: Biblical Principles of Welfare and Poverty
(Nashville: Thomas Nelson, 1986), p. 4.

29. Bid.

30, ibid.

31. Bid.

32. Did.

33. Ibid.

34. Bid.

35, See George Grant, The Dispossessed: Homelessness in America (Westchester, IL:

Crossway Books, 1986).

36. Grant, In the Shadow of Pleny, p. 5.

37, Ibid.

38, Did.

»

waenau
