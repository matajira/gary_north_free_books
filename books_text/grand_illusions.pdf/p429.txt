ABOUT THE AUTHOR

 

George Grant is the author of more than a dozen books on
politics, social issues, and history, including The Last Crusader:
The Untold Story of Christopher Columbus, Third Time Around: The
History of the Pro-Life Movement from the First Century to the Present,
Hillarious: The Wacky Wit, Wisdom & Wenderment of Hillary Redham-
Clinton, The Blood of the Moon. The Roots of the Middle East Crisis,
and a provocative exposé of the American Civil Liberties Union,
Thal & Error. His undergraduate studies in political science were
conducted at the University of Houston, He has also done post-
graduate studies in politics, literature, and theology. Dr. Grant
currently serves as Executive Director of Legacy Communica-
tions, an educational resource development company based in
middle Tennessee.

409
