Selling Sex: The Educational Scandal 127

There are six things which the Lord hates, yes; seven which
are an abomination to Him: haughty eyes, a lying tongue,
and hands that shed innocent blood, a heart that devises
wicked plans, feet that run rapidly to evil, a false witness who
utters lies, and one who spreads strife among brothers (Prov-

erbs 6:16-19).

Although indiscreet talk is “sweet in the mouths” of fallen
men (Job 20:12), it is terribly destructive (James 3:6): It defiles
the flesh (Jude 1:8). It invites corruption, untowardness, crook-
edness, and perversity (Deuteronomy 32:5). It contributes to the
delinquency of minors (Proverbs 7:6-23). It creates strife and
dissension (Proverbs 16:28). It devises evil in the midst of in-
nocence (Proverbs 16:30). It unleashes mischievous destruction
(Proverbs 17:20). It perverts justice (Deuteronomy 16:19). And it
wreaks havoc, “like a sharp razor, working deceitfully” (Psalm
52:2), even to the point of overthrowing an entire culture (Prov-
erbs 11:11), “A soothing tongue is a tree of life, but perversion in it
crushes the spirit” (Proverbs 15:4).

Foul speech is inescapably fraudulent (Psalm 10:7), just as it is
inescapably violent (Proverbs 10:11; 12:6).

Because such brazenness is an abomination to both God and
man (Psalm 109:2; Proverbs 4:24; 8:7), all men who indulge in
it, of necessity “have their consciences seared as with a hot brand”
(1 Timothy 4:2). Their hearts are hardened (Proverbs 28:14).
Their necks are stiffened (Proverbs 29:1). Their souls are impov-
erished (Matthew 16:26). And, their lives are cheapened (Prov-
erbs 6:26).

Planned Parenthood claims to teach our children the “facts
of life.”“1 But details about unspeakable perversions, concealed
horrors, and obscene titillations are not the “facts of life.”142

The “facts of life” can only be found in the Word of Life. And
that is one source Of inspiration that Planned Parenthood studi-
ously avoids.

With the fruit of a man’s mouth his stomach will be satisfied; he
will be satisfied with the product of his lips. Death and life are
in the power of the tongue, and those who love it will eat its
fruit (Proverbs 18:20-21).
