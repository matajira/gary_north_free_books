if facts can make a difference, this book should put an end to the debate about the so-
cial value of Planned Parenthood. George Grant makes a damning case against this
beneficent-sounding organization which is, indeed, a cancer in American society.

Connie Marshner, Commentator, Focus on The Family, “Weekend”

 

When I read this book, I envisioned Planned Parenthood as a large, luxurious ocean
liner cruising the open seas. 1 saw the leadership of Planned Parenthood on the deck,
smug and defiant, smiling and waving to passing boats, confident that their ship could
never be sunk. They felt an impact, heard an explosion, but continued smiling and
waving, arrogantly self-assured that it was a minor problem. Meanwhile, below the
surface, the lower decks were filling with water, the engine room was on fire, and the
ship was irreparably damaged—doomed to sink, It had been hit by one brilliantly
designed and perfectly aimed torpedo—~ Grand Mlusions. Read it, praise the Lord, and
pass the ammunition,

~Randall A. Terry, Host, Randall Terry Live

 

Planned Parenthood has enjoyed one of the longest free rides in political history. Now,
George Grant, in the tradition of solid investigative reporting, punches their ticket, ex-
posing the organization’s agenda as anything but the benign, responsible movement it
has portrayed itself to be.

—Cal Thomas, Columnist, Los Angeles Times Syndicate

 

George Grant is a prophetic figure whose wisdom and insight shine like a beacon through
the contemporary fog of our culture, calling a generation, drunk on the myth of Planned
Parenthood, back to sobriety in the truth of God’s Word, While many have denied, de-
bated, and defended the truth, George Grant calls us to demonstrate it, May the shocking
reality of Grand Hlusians disturb us out of our complacency to heed Grant’s call.

—Steve Camp, Recording Artist & Author

 

George Grant chronicles the Planned Parenthood agenda of enticing our youth into
“safe sex” in order to feed the cash registers of the largest unregulated legal industry in
the world — the abortion clinics, Harbored under the deception of “non-profit,” Planned
Parenthood continues to profiteer unchecked with our public school system. As a former
abortion provider, I can attest to the accuracy of facts and recommend every truth-seeking
citizen read Grand Hfusions.

—Carol Everett, President, LifeNetwork

 

In every generation, individuals have risen to the challenge of speaking authoritatively
and pro-actively to the critical issue of the day. In our generation, George Grant must
be numbered among them. Over the years George has consistently demonstrated the
ability to research and communicate truth, effectively combating the grand illusions
perpetuated by groups ranging from Planned Parenthood to the promoters of the
abortion pill RU-486. Because his arguments are impeccable and his conclusions
commanding, I regularly use him as primary resource for creative thinking and re-
demptive action.

—David J. Gyertson, Ph.D., President, Regent University
