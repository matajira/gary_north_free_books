190 Granp ILiusions

the world’s full attentions. Every week new images of harsh con-
centration camps, callous racial purging, and ruthless bombing
and strafing haunt the morning papers and evening newscasts.

Once described as “a single nation with two alphabets, three
religions, four languages, five nationalities, six republics, and
seven borders,”5 the erstwhile pluralistic multi-culturalism of
Yugoslavia is now little more than “a canker of hatred, war, and
devastation.” As the predominantly Christian nations of
Slovenia and Croatia take in a flood of refugees from Bosnia,
Herzegovina, Montenegro, and Dalmatia fleeing from Serbian
death squads, the rest of the world watches uneasily and wonders
how something this terrible could actually be happening — again.

“In some ways, it was inevitable that such horrors should
erupt here,” Nada Kovacic told me.

“Anyone who is surprised by the ferocity of this conflict simply
has not been paying attention to the many warning signs— all too
evident to us for several decades now,” agreed Ruza Vejzovic.

A thick morning fog settled into the valley that halved the old
city center of Zagreb—the capital of Croatia, From the broad
vantage of the Tig Jelactc—a vast open square that had been a
hub of commercial activity since the twelfth century—the high
medieval Gornji Grad was barely visible. Actually, the great
cathedral spires of Sveti Stephen’s that pierced the dense shroud
were the only indications that beyond the busy Republique business
district the city continued to rise sharply toward the ancient twin
settlements of Gradic and Kapiol.

Nada and Ruza, both students at the University of Zagreb,
were making their way through the narrow streets of Croatia’s
capital toward those venerable spires for the morning Matins
liturgical service. They turned aside momentarily to purchase a
few crisp pastries at a quaint shop just off the square. Standing
there, across from the oddly modernesque glass crenelations of
the Dudrovnik hotel—favored by many of the foreign journalists
covering the war—they were once again confronted with the
painful plight of their homeland. A film crew was loading equip-
ment into a battered mini-van.

The two women told me that the current obsession of the
Western media with “ethnic cleansing,” “nationalistic rivalry,”
“guerilla terrorism,” and “entrenched aggression” is in a very real
