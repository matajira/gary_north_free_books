The Camera Blinked: The Media Legacy 181

James Hitchcock, professor of history at St, Louis University,
has said:

What is presented in the media, and the way in which it is
presented, are for many people the equivalent of what is real.
By determining what ideas will be discussed in public, the
media determines which ideas are to be considered respectable,
rational, and true. Those excluded from discussion, or treated
only in a negative way, are conversely defined as disreputable,
irrational, and false. The media has the power almost to confer
existence itself. Unless a belief or an institution receives some
recognition, it does not exist. Even those who know that the media
is fundamentally hostile to their values nonetheless court media
recognition as a way of achieving status in the public eye.1%

From the media we discover what is important and what is
not. We learn what is tragic and what is heroic, what is com-
mendable and what is dishonorable, what is sober and what is
humorous.'3? We learn how to dress, what to eat and drink, and
what kind of car to drive. And, of course, we learn how we
should think about public issues, how we should react to per-
sonal crises, and how we should live our lives. '%

In his scathing critique of ethics in journalism, The News at
Any Cost, Tom Goldstein suggests that not only are reporters the
“kingmakers” and “kingbreakers” of our day, they are the “unac-
knowledged legislators” of our none-too-pluralistic society.1%
They shape cultural mores, he says, affect political contests,
create the parameters of public issues, unveil hidden truths—
whether true or not—and dictate the social agenda, all on a two-
hour deadline! They function not only as the judge, jury, and
executioner in the courtroom drama of life, but also as both pub-
lic defender and criminal prosecutor, 5

Such power should not be taken lightly. It colors everything
it touches.

When Karen Denney first began to see the news reports and
the articles denigrating the pro-life cause, she simply shrugged
them off as just one more series of pro-abortion attacks on the
truth. As a volunteer counselor at an alternative center, she had
seen any number of media distortions and moral tirades aimed
at her work. “At the start, it seemed like it was just the same old
propaganda,” she said.
