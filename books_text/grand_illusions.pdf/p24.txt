4 Granp ILLusiIons

martyrs, confessors, ascetics, and every righteous spirit made
pure in Christ— have always looked to the Bible as the blueprint
for living. They have always taken it seriously, studying it,
applying it, and obeying it. That is because they have compre-
hended the reality that from Genesis to Revelation the Bible is
indeed God’s Word. And that God’s Word is hope for the hope-
less, help for the helpless, salve for the sick, balm for the broken,
and strength for the stricken. It is the cure.

The doctors, lawyers, politicians, social scientists, judges,
psychologists, bureaucrats, and various and sundry other experts
who have harnessed their disciplines for the Planned Parenthood
movement, certainly cannot be faulted for their concern over the
plight of women and children — if indeed their concern is genuine.
Where they have gone astray is in taking matters into their own
hands, seeking out their own new and novel cure. Instead of ad-
hering to the wise and inerrant counsel of the Bible— walking
along the well-trod path of the Saints — they have done “what was
right in their own eyes” (Judges 21:25). They have completely
ignored —and as a consequence violated —God’s Wisdom. Their
policies, proposals, and programs have been blatantly man cen-
tered. In other words, they have been Aumanistic.

“Humanism is,” according to the Russian iconodule Aleksandr

Solzhenitsyn, “the proclaimed and practiced autonomy of man
from any higher force above him.”! Or, as theologian Francis
Schaeffer has said, it is “the placing of man at the center of all
things and making him the measure of all things.”!! According
to humanistic dogma, there is no notion of absolute right or
wrong. There are no clear-cut standards. Morality is relative.
And problem solving is entirely subjective.”

The problem is that humanism is entirely out of sync with
the fabric of reality:

To the Law and to the Testimony! If they do not speak accord-
ing to this Word, it is because they have no dawn (Isaiah 8:20).

To attempt to solve the perilous problems of modern society
without hearing and heeding the clear instructions of the Bible
is utter foolishness (Romans 1:18-23). It is an invitation to in-
adequacy, incompetency, irrelevancy, and impotency (Deu-
teronomy 28:15). All such attempts are doomed to frustration
and failure.
