Idols for Destruction: A Strategy for Prophets and Priests 285

Ultimately, the Gospel won and the Roman Empire was con-
verted. But, until that had occurred, the disciples of Christ had
to find other means to affect the ministry of cultural action.

At the present time, the laws of many states and municipali-
ties hinder us from fulfillmg our God-ordained responsibilities:
rescuing the perishing (Proverbs 24:10-12), educating our chil-
dren in the Law of God (Deuteronomy 6:1-9), and caring for the
poor and homeless (Isaiah 58:6-12). What are we to do when
such civil tyranny actually excludes us from the system? Do we
have to resort immediately to civil disobedience?

Not at all.

In the Scriptures, God's people, at various times and in vari-
ous situations, demonstrate a number of different reactions to
civil tyranny that involve working within the system. When the
state oversteps its bounds and begins to violate God’s immutable
Law, believers have several models of tactical action from which
to choose.

For instance, Daniel, when asked to violate Scripture, simply
utilized the tactic of the wise appeal. Instead of instantly indulging
in belligerent rebellion against divinely instituted authority, he
proposed an alternative course of action, which ultimately gained
for him the favor of the court (Daniel 1:8-16).

Similarly, the Apostle Paul, when faced with an ungodly and
unscrupulous jury, exercised the tactic of lawyer delay. Instead
of reviling the authorities, instead of outright rebellion, and in-
stead of sullen submission, Paul upheld the integrity of God's
law through the appellate process (Acts 25:1-27).

Moses, when faced with the awful oppression of God's peo-
ple, began a very forthright lobbying initiative. Rather than ad-
vocating an armed rebellion, he sought a change in Pharaoh’s
tyrannical policy. His whole approach, though from without,
was to force the evil system of Egypt to change from within
(Exodus. 5:1-21).

Obadiah was the chief counsel to King Ahab, perhaps the
most corrupt and tyrannical of all the kings in Israel’s history. As
a devout believer in the Lord, Obadiah was able to instigate a
program of reform and renewal right under the haughty noses of
Jezebel and her dastardly Baalic courtiers. Instead of actively
rebuking the evil monarchs and their consorts, he worked around
