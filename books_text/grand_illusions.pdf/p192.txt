172 Granp ILLusions

X
the research, manufacture, distribution, sale, lease, and servic-
ing of everything from television receivers to electro-optic
devices, from video-cassette equipment to audio records, tapes,
and CDs, and from commercial communications satellites to
military electronic hardware, the company operates the NBC
network, with two hundred affilated television stations and more
than five hundred radio stations.*° In addition to that, the com-
pany operates a system of satellite and submarine cable circuits
linking the continental United States directly with numerous
foreign countries and overseas points.*! It offers telex services,
data transmission programs, telegram transmissals, teleprinter
circuits, and high speed multi-point line data communications
services for terminal communication and point-to-point lines to
host computers. #?

For years, the NBC network has dominated radio and televi-
sion broadcasting.*? It has more often than not controlled the
airwaves and dictated programming fashions.4* And, not sur-
prisingly, it has imposed its leftist perspective on virtually every-
thing that it broadcasts.#* News reporting under the aegesis of
Tom Brokaw has constantly reinforced the Planned Parenthood
party line.*® Its entertainment programming has fallen in line as
well with rabid pro-abortion pieces profiled on numerous prime-
time sitcoms and dramatic series.4”? During the 1992 elections,
when his network’s cheerleading for the pro-abortion Demo-
cratic ticket became obvious to even the most epistemologically
unconscious observer, Brokaw quipped, “Bias, like beauty, is in
the eye of the beholder.”#* But it has gotten so bad that now, we
all behold it.

CapCities. For many years, ABC was the stepchild of national
radio and television broadcasting.#9 A distant third in ratings,
earnings, and affiliates throughout the sixties and seventies, the
network perpetually lagged behind CBS and NBC, exerting litle
influence in either news or entertainment. But, then, a series of
dramatic events in the eighties, including a merger with the vast
Capital Cities Communications Group and a bold and innovative
programming twist, catapulted the network into the number one
position.®° It grew to more than two hundred affiliated television
stations and nearly two thousand affiliated radio stations.5t

With its new-found strength, the company began to build its
own communications empire. It invested in cable and subscription
