134 Granp ILLusions

Nearly forty years ago Congress began pouring millions of
dollars into Planned Parenthood’s draconian programs in a
desperate attempt to hold down the burgeoning costs of welfare
dependency.* Lobbyists for Planned Parenthood argued that
without a comprehensive, nationwide, tax-funded abortion and
birth control network, thousands, if not millions of girls like
Roxanne would be abandoned to an irrevocable spiral of pov-
erty.© They would become a chronic strain and drain on the
system.. “Every dollar invested in family planning,” they argued,
“would save two to three dollars in health and welfare costs.”

That logic—you’ve got to spend money in order to save
money ~ sounded logical enough to Congress. So, anxious to dem-
onstrate a fiscal responsibility heretofore inimicable to its char-
acter, it authorized several well-heeled family planning measures.

In 1964, the Economic Opportunity Act was passed, which
included a number of birth control and maternal health and
hygiene provisions for the very poor. For the first time the fed-
eral government became involved in regulating families and
policing their bedroom behavior.

In 1968, the Center for Population Research was established
in order to coordinate federal activities in “population-related
maiters.”§ A significant appropriations commitment was passed
at that time to provide for contraceptive and abortifacient
research, placement, and service.?

In 1970, President Richard Nixon signed into law the
Tydings Act, consolidating the funding base for the Center, and
granting service contracts and subsidy support for independent
providers.'° The bill created Title X of the Public Health Service
Act and set funding precedents for Title V, Title XIX, Title
XX, and a whole host of other family planning and social welfare
spending programs that came along in later years.!

Interestingly, Section 1008 of the Tydings legislation stipu-
lated that “none of the funds appropriated under this title shall be
used in programs where abortion is a method of family planning.”
The strong wording of this provision indicates that Congress not
only wished to prohibit the use of tax dollars for abortion pro-
cedures, but to exclude the funding of programs and organizations
that counseled or referred for abortion as well.!? Such wording
became essentially moot as successive “pro-life” Republican ad-
ministrations allowed family planning entitlements to grow to
