Bad Seed: The Historical Legacy 53

Margaret Sanger was a Madonna type of woman, with soft
brown hair parted over a quiet brow, and crystal-clear brown
eyes, . . . It was she who introduced us all to the idea of birth
control, and it, along with other related ideas about sex, be-
came her passion. It was as if she had been more or less arbi-
trarily chosen by the powers that be to voice a new gospel of not
only sex-knowledge in regard to conception, but sex-knowledge
about copulation and its intrinsic importance.

She was the first person I ever knew who was openly an ardent
propagandist for the joys of the flesh. This, in those days, was
tadical indeed when the sense of sin was still so indubitably
mixed with the sense of pleasure. . . . Margaret personally set
out to rehabilitate sex... . She was one of its first conscious
promulgators.*?

Everyone seemed to be delighted by Margaret’s explicit and
brazen talks. Everyone except her husband, that is. William
began to see the Socialist revolution as nothing more than “an
excuse for a Saturnalia of sex.”#3 He decided he had best get
Margaret away once again.

‘This time, he took Margaret and the children to Paris. He
could pursue his interests in modern art. Margaret could study
her now keen fascination with the advanced contraceptive
methods widely available in France, And together they could re-
fresh their commitment to each other in the world’s most romantic
city. Again though, he would be disappointed. After two weeks,
Margaret became anxious for her Village friends and lovers. She
begged William to return. He refused, so she simply abandoned
him there, and returned to New York with the children.

Without her husband to support her every whim and fancy,
Margaret was forced to find some means of providing an income
for herself and the children. She had continued to write for The
Call and found some degree of satisfaction in that, so she decided
to try her hand at writing and publishing a paper herself.

She called it The Woman Rebel. It was an eight-sheet pulp with
the slogan “No Gods! No Masters!” emblazoned across the mast-
head. She advertised it as “a paper of militant thought,” and
militant it was indeed, The first issue denounced marriage as a
“degenerate institution,” capitalism as “indecent exploitation,”
and sexual modesty as “obscene prudery.”** In the next issue, an
