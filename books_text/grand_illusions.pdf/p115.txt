A Race of Thoroughbreds: The Racial Legacy 95

beloved disciple of Francis Galton, the brilliant cousin of Charles
Darwin who first systemized and popularized Eugenic thought.3!

Part of the attraction for her was also political: virtually all of
her Socialist friends, lovers, and comradcs were committed
Eugenicists as well—from the followers of Lenin in Revolu-
tionary Socialism, like H. G. Wells, George Bernard Shaw, and
Julius Hammer,®? to the followers of Hitler in National Social-
ism, like Ernest Rudin, Leon Whitney, and Harry. Laughlin.?3

But it wasn’t simply sentiment or politics that drew Margaret
into the Eugenic fold. She was thoroughly convinced that the
“inferior races” were in fact “human weeds” and a “menace to civ-
ilization.”3+ She believed that “social regeneration” would only be
possible as the “sinister forces of the hordes of irresponsibility
and imbecility” were repulsed.3> She had come to regard organ-
ized charity to ethnic minorities and the poor as a “symptom of a
malignant social disease” because it encouraged the prolificacy of
“defectives, delinquents, and dependents.”3¢ She yearned for the
end of the Christian “reign of benevolence” that the Eugenic
Socialists promised, when the “choking human undergrowth” of
“morons and imbeciles” would be “segregated” and “sterilized.”27
Her goal was “to create a race of thoroughbreds” by encouraging
“more children from the fit, and less from the unfit.”38 And the
only way to achieve that goal, she realized, was through Malthu-
sian Eugenics.

Thus, as she began to build the work of the American Birth
Control League, and ultimately, of Planned Parenthood, Margaret
relied heavily on the men, women, ideas, and resources of the
Eugenics movement. Virtually all of the organization's board
members were Eugenicists.5° Financing for the early projects—
from the opening of the birth control clinics to the publishing of
the revolutionary literature~came from Eugenicists.° The
speakers at the conferences, the authors of the literature and the
providers of the services were almost without exception avid
Eugenicists.41 And the international work of Planned Parenthood
were originally housed in the offices of the Eugenics Society —
while the organizations themselves were institutionally inter-
twined for years.*?

The Birth Control Review — Margaret’s magazine and the imme-
diate predecessor to the Planned Parenthood Review — regularly and
openly published the racist articles of Malthusian Eugenicists.*%
In 1920, it published a favorable review of Lothrop Stoddard’s
