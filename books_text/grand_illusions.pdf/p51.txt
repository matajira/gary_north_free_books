All that Glitters 3

Safe and effective? Not by a long shot. Planned Parenthood’s
program of birth control is nothing but foreplay for abortion.
Besides the fact that it is fraught with awful side effects, complica-
tions, and medical risks, it is incapable of preventing unwanted
pregnancies as well. Planned Parenthood’s entire myth is an
empty charade. We simply cannot contend or pretend otherwise.

The STD Illusion

Planned Parenthood claims that it is in the forefront of the battle
against sexually transmitted diseases.'?4 But that is an illusion.

Planned Parenthoed is not only not in the forefront of the
battle, it is not even én the battle. The truth is, Planned Parent-
hood's efforts have been tragically counterproductive. It has be-
come a veritable Typhoid Mary, actually encouraging the spread
of syphilis, gonorrhea, chlamydia, herpes, hepatitis, granuloma,
chancroid, and even AIDS at an alarming rate. Besides the fact
that it constantly exhorts youngsters to flaunt a ribald and irre-
sponsible promiscuity,’?> it continually promotes an alarmingly
“unsafe” exercise of that promiscuity. Instead of affording its
fornicating disciples with the slim security of barrier devices, it
primarily peddles the entirely unguarded prescription birth
control methods. Eighty percent of Planned Parenthood’s
clients receive non-barrier contraceptives,!?§ and eighty-eight
percent of those who previously practiced “safe sex” are dis-
suaded from continuing. 1?”

Admittedly, barrier devices such as condoms offer only limited
protection against venereal infection.1?° Due to in-use mechanical
failure —leaks, breaks, tears, slippage, and spillage — their effec-
tiveness has been estimated to be ai dest eighty-two percent.129
But the Pill offers no protection whatsoever. Neither does the
TUD or the diaphragm or spermicides or contraceptive sponges
or any of the other non-barrier birth control devices that Planned
Parenthood favors. Worse, recent studies indicate that not only
do these methods fail to guard against venereal infection, they
may actually enkance the risks.!3° “Apparently,” says demographic
analyst Robert Ruff, “Planned Parenthood believes that safe sex
is a lot less important than free sex.”13!

Planned Parenthood is not, by any stretch of the imagination,
in the forefront of the battle against venereal disease. It is instead
