Back-Alley Butchers: The Medical Legacy 85

of the gains of the last century stip out of our grasp: incurable
retro-viruses ravaging whole sectors of the population in plague
proportions,°3 mutating strains of cancer cutting wide swathes
through each new generation,®+ and surprising reversals in un-
developed nations of once-conquered foes like smallpox, malaria,
and polio,%

But that’s not the worst of it,

Horror story after horror story — a veritable litany of abuse—
has begun to emerge from the hallowed halls of medicine in our
land: fetal harvesting,9® women serving as breeders in surrogate
motherhood programs,*’ euthanasia,®* genetic manipulation
in test tube baby experiments, infanticide, preprogrammed
implants and brain-to-computer interfacing for intelligence
enhancements,!°! genetic engineering, !°? viral memory manip-
ulation to control psychotic episodes in mental patients,1%
algeny,‘ the development of frighteningly powerful forms of bi-
ological warfare,‘ daeliaforcation,!* the exclusive commercial-
ization of services by health care corporations,!” biocleatics,!
poor patients serving as guinea pigs in bizarre particle bombard-
ment experiments,!° neuroclatology,° handicapped patients
facing compulsory sterilizations,'! artificial natalization,!!? and
racially motivated Eugenic programs.‘ They are stories that
make the Nazi medical atrocities pale in comparison.!!*

How could this come to be? How could modern medicine —
fresh on the heels of the Golden Age—have gone so wrong?

The fact is, medicine has always been a special legacy of
God’s people.

Whenever and wherever Biblical faithfulness has been prac-
ticed, the medical arts have flourished. But whenever and wher-
ever Biblical faithfulness has been shunned, medicine has given
way to superstition, barbarism, and shamanism.

The earliest medical guild appeared on the Aegean island of
Cos, just off the coast of Asia Minor. Around the time that
Nehemiah was organizing the post-exilic Jews in Jerusalem to
rebuild the walls, another refugee from the Babylonian occupa-
tion, Aesculapius, was organizing the post-exilic Jews on Cos
into medical specialists—for the first time in history, moving
medical healing beyond folk remedies and occultic rituals. It was
not long before this elite guild had become the wonder of the
Mediterranean world under the leadership of Hippocrates, the son.
