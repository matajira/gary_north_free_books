SEVEN

ROBBER BARONS: THE
FINANCIAL SCANDAL

argumenium ad crumenam’

 

As always happens to miraculous things, the virtue has all gone out with the lapse
of time.?

Hilaire Belloc

Skimming the streets on a razor-sharp high of adrenaline and
paranoia, Roxanne Robertson circled the clinic’s concrete for-
tress half a dozen times before she finally screwed up enough
courage to pull into the parking lot. An acid rain, the sins of her
fathers, blew down hard and cold, etching obscure messages into
the surface of the graceless asphalt. As she stepped out of the car
and moved toward the building, a scrap of rubbish, plucked up
by the wind, did a careless pirouette before being carried away.

She stepped quickly under the parapet, around the corner,
through glass doors, along a carpet mapped with stains shaped
like dark continents amid a sienna sea, and into a long narrow
lobby. Hunched behind the reception desk sat a gnomish and dis-
heveled woman with a beaked nose and tufts of frowzy brown hair
reminiscent of a lark’s nest. It seemed that upon her forehead the
engraved word finality would not have been at all inappropriate.

The room was close and warm. From within drifted the
smell of bodies pressed together, cigarette smoke, disinfectant,
perfume, and something else—the almost metallic scent of fear.

“I believe I have an appointment,” Roxanne said.

The woman handed her a clipboard. “You'll need to fill this
out.” She smiled a ragged smile. It was a hollow, haunting ges-
ture—a feeble attempt to veil her intentions with an unspoken
cant of compassion.

131
