Introduction: Ad Vitum 5

Humanism cannot work because humanism ignores the
essence of reality (Ephesians 5:6). It is fraught with fantasy (Col-
ossians 2:8). Only the Bible can tell us of things as they really are
(Psalm 19:7-11). Only the Bible faces reality squarely, practically,
completely, and honestly (Deuteronomy 30:11-i4). Thus, only
the Bible can provide genuine solutions to the problems that
plague mankind (Psalm 119:105).

Jesus was forever reminding His disciples of these facts. He
made it clear to them that the Bible was to be their ultimate
standard —for life and godliness, for faith and practice, and for
profession and confession:

It is written, “Man shall not live by bread alone, but on every
Word that proceeds out of the mouth of God” (Matthew 4:4).

But it is easier for heaven and earth to pass away than for one
stroke of a letter of the Law to fail (Luke 16:17).

Whoever then annuls one of the least of these Commandments,
and so teaches others, shall be called least in the Kingdom of
Heaven; but whoever keeps and teaches them, he shall be
called great in the Kingdom of Heaven (Matthew 5:19),

Again and again He affirmed the truth that “all Scripture is
God breathed” (2 Timothy 3:16), that it is useful for “teaching,
rebuking, correcting, and training in righteousness” (2 Timothy
3:17), and that it “cannot be broken” (John 10:35):

All His Precepts are sure. They are upheld forever and ever;
they are performed in truth and uprightness (Psalm 111:7-8).

All men know this. Even the diligent and studied humanists
in Planned Parenthood know this. The work of God's Law is
written on the hearts of all men (Romans 2:14-15). They must
actively restrain or suppress this Truth in order to carry ori with
their novelties (Romans 1:18). Though they know what is right,
they deliberately debase themselves with futile thinking, foolish
passions, and filthy behavior (Romans 1:19-24, 26-27). They pur-
posefully betray reality, exchanging God’s Word for lies (Romans
1:25). Though they know the Ordinances of Life, they consciously
choose the precepts of death (Romans 1:28-31). And then they
