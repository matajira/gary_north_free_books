Slaying Dragons: The Character to Confront 237

Fifth, we need to develop national information networks.
This is the information age. We have been blessed with a pro-
liferation of information technologies. There is no need for us to
depend on the national media any longer. Sadly, Christians
have, by and large, failed to realize that. We have not yet capi-
talized on those technologies. But, of course, one or two brave
pioneers could quickly and easily turn things around.

Knowledge produces endurance (Proverbs 28:2). It multi-
plies resources (Proverbs 24:4). It provides encouragement
(Proverbs 2:10). It releases power (Proverbs 24:5), joy (Ecclesi-
astes 2:26), prudence (Proverbs 13:16), protection (Acts 20:31),
and security (Ecclesiastes 7:12).

Confronting the evil of Planned Parenthood demands the
sturdy advantage of Christian watchfulness. It requires the in-
formed stamina of Christian soberness. It necessitates the right-
eous character of Christian alertness.

Therefore, gird your minds for action, keep sober in spirit, fix
your hope completely on the grace to be brought to you at the
revelation of Jesus Christ, As obedient children, do not be con-
formed to the former lusts which were yours in your ignorance,
but like the Holy One Who called you, be holy yourselves also
in all your behavior (1 Peter 1:13-15).

Steadfastness

If we are to expose the evil of Planned Parenthood, we must
also be faithful, steadfast, and unwavering. Like alertness, this is
a cornerstone of Christian character.

We are all called to stand firm in the faith (2 Thessalonians
2:25). We are to be steadfast in the midst of suffering (1 Peter
5:9), in the midst of strange teaching (Hebrews 13:9), and in
times of trying circumstances (James 1:12).

We are to be steadfast in good works (Galatians 6:9), in
enduring love (Hosea 6:4), in conduct (Philippians 1:27), in
decision-making (1 Kings 18:21), and in absolute loyalty to the
Lord God (Proverbs 24:21).

Therefore, my beloved brethren, be steadfast, immovable,
always abounding in the work of the Lord, knowing that your
toil is not in vain in the Lord (1 Corinthians 15:58).
