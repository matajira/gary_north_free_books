FIVE

A RACE OF
THOROUGHBREDS:
THE RACIAL LEGACY

homo homini lupus!

 

Time after time mankind is driven against the rocks of the horrid reality of a fallen
creation. And time after time mankind must learn the hard lessons of history — the
lessons that for some dangerous and awful reason we can’t seem to keep in our col-
lective memory.?

Hilaire Belloc

It was a very good year. America was boisterously happy.
With Calvin Coolidge in the White House in Washington, Duke
Ellington at the Cotton Club in Harlem, and Babe Ruth at
home plate in New York, things could hardly be better. It was
1927, and Cecil B. DeMille was putting the finishing touches on
his classic film, The King of Kings, Henry Ford was rolling his
fifteen-millionth Model T off the assembly line, Abe Saperstein
was recruiting the razzle-dazzle players that would become the
Harlem Globetrotters, Al Jolson was wowing the public in The
Jazz Singer, and Thornton Wilder was garnering accolades for his
newest book, The Bridge of San Luis Rey. The Great War was an
already distant memory and the Great Depression was still in the
distant future. It was a zany, carefree time of zoot suits and flappers,
speakeasies and dance-a-thons. It was indeed a very good year.

For most folks, anyway.

It wasnt a very good year for Carrie Buck. In a decision
written by Justice Oliver Wendell Holmes, the Supreme Court
upheld a Virginia State Health Department order to have the
nineteen-year-old girl sterilized against her will.

wt
