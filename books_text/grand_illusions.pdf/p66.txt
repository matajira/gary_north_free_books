46 Granp ILLusions

bestow upon them the unfathomable millennial power to alter
the destinies of societies, nations, and epochs. They were a peo-
ple of purpose. They were a people of manifest destiny.

What they did not know was that dark and malignant seeds
were already germinating just beneath the surface of the new
century’s soil. Josef Stalin was a twenty-one-year-old seminary
student in Tiflis, a pious and serene community at the cross-
roads of Georgia and the Ukraine. Benito Mussolini was a
seventeen-year-old student teacher in the quiet suburbs of
Milan. Adolf Hitler was an eleven-year-old aspiring art student
in the quaint upper Austrian village of Brannan. And Margaret
Sanger was a twenty-year-old shy and out-of-sorts nurse-
probationer in White Plains, New York. Who could have ever
guessed on that ebulently auspicious New Year’s Day that those
four youngsters would, over the span of the next century, spill
more innocent blood than all the murderers, warlords, and
tyrants of past history combined? Who could have ever guessed
that those four youngsters would together ensure that the hopes
and dreams and aspirations of the twentieth century would be
smothered under the weight of holocaust, genocide, and triage?

As the champion of the proletariat, Stalin saw to the slaugh-
ter of at least fifteen million Ukrainian kulaks. As the popularly
acclaimed J? Duce, Mussolini massacred as many as four million
Ethiopians, two million Eritreans, and a million Serbs, Croats,
and Albanians. As the wildly lionized Fuhrer, Hitler exterminated
more than six million Jews, two million Slavs, and a million Poles.
As the founder of Planned Parenthood and the impassioned
heroine of feminist causes célébres, Sanger was responsible for the
brutal elimination of more than twenty million children in the
United States and as many as one and a half billion worldwide.

No one in his right mind would want to rehabilitate the rep-
utations of Stalin, Mussolini, or Hitler. Their barbarism, treachery,
and debauchery will make their names live on in infamy forever.
Amazingly though, Sanger has escaped their wretched fate. In
spite of the fact that her crimes against humanity were no less
heinous than theirs, her place in history has somehow been sani-
tized and sanctified. In spite of the fact that she openly identified
herself in one way or another with their aims, intentions, and
movements —with Stalin’s Sebernostic Collectivism,® with Hitler's
Eugenic Racism,’ and with Mussolini's Agathistic Distributism® —
she somehow managed to establish an independent reputation
for the perpetuation of her memory.
