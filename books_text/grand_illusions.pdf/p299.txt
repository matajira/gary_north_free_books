Idols for Destruction: A Strategy for Prophets and Priests 279

these areas, much more needs to be done. Litigation needs to pour
forth from the Christian community in torrents. Parents need to
sue school districts over the debauched curriculum programs.
Women abused by abortion need to sue Planned Parenthood and
other abortuaries for medical malpractice. Pro-life leaders need.
to sue the various media outlets for slander and deliberate mis-
representation of the facts. And churches need to sue zoning
commissions for discriminatory regulation. In the same way that
Planned Parenthood has smothered believers over the last thirty
years with their lawsuits, we need to take to the courts, fighting
fire with fire. We need to stop waiting until we are sued to utilize
the system to save the system. Why not initiate a few lawsuits?
Or, perhaps, sponsor some of our friends in a court fight? Planned
Parenthood has had the courts to themselves for long enough now.

Fourth, we can begin to utilize the defense of necessity. The
defense of necessity is a legal maneuver that argues that “lesser laws
may be broken so that a greater good might be done.” So, for in-
stance, breaking and entering a burning house to save a victim
from the flames is not a crime, Likewise, assault and battery on
a rapist in order to free his victim is a justified act. The defense of
necessity may also be utilized to test laws that protect abortionists.
Sit-ins in the Planned Parenthood clinics where the unborn chil-
dren are butchered in wholesale slaughter are not simple cases of
civil disobedience, but are opportunities to bring the defense of ne-
cessity into the courts, thereby testing the validity of the laws.
Planned Parenthood has been utilizing the various legal tests to
throw out Christian laws for years. Isn’t it about time to turn the
tables? Why not begin to set some legal precedents in a few
minor trials so that the defense of necessity is available for us to use
in the major cases? It is crucial that we plan ahead, gain a foot-
hold, and forge some victories.

One way or another we need to help turn the courts around.
We need to serve them as prophets and priests, guiding and
guarding the land.

The Bureaucracy
The bureaucracy is, perhaps, the most powerful “branch” of
the American governmental system. It is certainly the largest
and the most expensive. Yet it is nowhere mentioned in the Con-
stitution.: It was in no way envisioned by the Founding Fathers.
