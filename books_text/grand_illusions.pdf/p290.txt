270 Granp ILtustons

mittees, put together a few budget proposals, and develop alter-
natives to the present programs? If we would, Planned Parent-
hood would surely be exposed for what it is.

Second, we can participate in precinct caucuses. The precinct
is the lowest common denominator in the American political
process. Basically, it is an election district. It is the place where
votes are cast and counted. But it is also the hub of party organi-
zation. It is there in the precinct that our political parties receive
their direction, support, continuity, platform, and purpose. What
a party is, whom it nominates, and even where it is going are all
determined in those small meetings all across the country, in
schools, American Legion halls, and polling booths, not under
the bright lights of the national convention. And the thing is,
anyone who votes in a party’s primary is eligible to participate in
the precinct caucus. Only about three percent ever actually do.5*
So, why not let that three percent be us? Why not submit resolu-
tions that hit hard on Planned Parenthood’s programs of lust and
greed? Why not elect pro-life activists as precinct chairmen,
election marshals, poll watchers, and convention delegates? We
can actually make a difference if only we will.

Third, we can participate in local campaigns and elections.
Every vote really matters. Since only about sixty percent of the
citizens of this country are registered to vote, and only about
thirty-five percent actually bother to go to the polls, a candidate
only needs the support of a small elite group of people to win.5?
It only takes about fourteen percent of the electorate to gain a
seat in the Senate. It takes about eleven percent to gain a seat
in the House.* Only about nine percent is needed to win a gov-
ernorship.®? And it takes a mere seven percent to take an aver-
age mayoral or city council post.®3 Any race is winnable. Why
not get involved in a worthy campaign? Why not work to get
magistrates in office who will defund Planned Parenthood? Why
not give up a few weekends to stuff some envelopes, post some
yard signs, man a phone bank, host a candidate forum, distrib-
ute bumper stickers, or mount a voter registration drive? We
truly can make every vote count.

One way or another, we need to participate in, and influ-
ence, our local governments. We need to serve them as prophets
and priests, guiding and guarding the land.
