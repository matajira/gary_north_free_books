Robber Barons: The Financial Scandal 143

A coalition of pro-abortion groups began a public smear
campaign, designed as much to stymie Mac’s efforts as to mire
his character in controversy and conflict. In less than three
months he decided to resign rather than to subject his family to
any further harassment. “I hated to back out of the fight,” he ad-
mitted, “but I was out there practically on my own. The thing is,
they’ve got so much money and so many people on the abortion
side that it just doesn’t seem to matter any more what is right,
what is true, what is legal, or what is moral. Money is power. Of
course, the maddening thing is that the money they used to
drive me out of office was my own money, It was tax money.”

Two months after Mac resigned, the other county commis-
sioners voted to increase their funding of the program.

Sadly, Mac Lawton’s experience is all too typical.

That is the financial legacy of Planned Parenthood.

Planned Parenthood and Parents: Expansion

As many as one third of Planned Parenthgod’s clients are
youngsters living at home.,°? Most of them are not the least bit
needy, but because they do not have incomes independent of their
parents, they are classified as needy by clinic personnel.°% Verifi-
cation is verbal only, so the matter is left entirely to the discretion
of Planned Parenthood.®* Not surprisingly then, ninety-seven
percent of them are approved for government subsidies, thus
providing a massive infusion of new income for the clinics.

By funneling federal money through state and local govern-
ments behind the taxpayers’ backs, Planned Parenthood is able
to establish its programs. By applying it to teens behind their par-
ents’ backs, it is able to expand those programs. '

That is why over the years, Planned Parenthood has been
the single most vigorous opponent of parental consent laws for
birth control and abortion services.®> It has fought the right of
parents to know in the courts?* and in the media,%” through
lobbying®® and through legislation,’ with curriculum in the
schools! and with campaigns in the communities.

This despite the fact that again and again, parental consent
laws have been shown to be an effective deterrent to teen prom-
iscuity, pregnancy, abortion, and venereal infection,1
