Preface to the Second Edition x1

doubt that anyone could survey geneticists about amniocentesis
results and publish the findings in a book that same year—or
that any such survey 22 years ago, even if it were done, would
have any bearing on the March of Dimes today. More clearly
absurd is the citation of a March of Dimes statement of our
neutrality on the abortion controversy in a book published in
1953, because in that year we were still purely a polio organiza-
tion that had not even conceived of becoming involved in birth
defects, let alone any as yet nonexistent debates about prenatal
diagnosis or abortion. These anachronisms suggest that Grant,
or someone he's citing as a secondary source, simply made up
these citations, as well as the allegations themselves.*

Sounds convincing. At least it sounds convincing until the
careful reader actually turns to footnotes #66 and #69, in the first
edition of the book (or #69 and #72 in this edition), and dis-
covers that it is Leavitt that is making things up. The dates for
the sources cited are in fact, 1977 and 1985 respectively—not
1967 and 1953 as Leavitt claims.

Besides these deliberate distortions, the rest of the four-page
memo amounts to little more than a vague blanket denial of any
March of Dimes wrong-doing in amniocentesis research, fund-
ing of geneticists through the organization’s granting process,
and institutional involvement with Planned Parenthood — despite
all the irrefutable evidence to the contrary. Shame-faced silence,
blatant falsehood, or sly evasion are ultimately the organization's
only recourses.

Revisions

None of this is to say that the first edition of the book was
flawless. On the contrary, the text’s glitches, omissions, and
awkward passages were more than a little glaring to me from the
start. That is why I am so grateful to have this opportunity to
revise and refine the manuscript for this second edition.

Readers of the original will notice several additions: a new
chapter on International Planned Parenthood, a new appendix,
a new select bibliography, and material on RU-486 and other
technological developments. Besides those changes, I have noted
the dramatic political changes in the United States — which have
radically altered the sociocultural climate worldwide —as well as
updated relevant statistics where necessary.
