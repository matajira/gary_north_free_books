Li2 Granp ILLusions

identities” and to keep them from “fostering the immorality
of morality.”3*

In Values Clarification, “decision-making scenarios” are
placed before the children and they are asked to make a series of
“life and death decisions” where the “only absolute” is that “there
are no. absolutes.”55

Missy Gallagher and Tom Blatten are “high school sweet-
hearts” who recently endured a Planned Parenthood Values
Clarification course in Los Angeles. In one exercise, Missy and
Tom were brought to the front of the class and given a “decision-
making scenario” that they, and the rest of the class, were sup-
posed to respond to,

“The way the teacher set it up,” Missy explained, “was that
Tom was supposed to have gotten me pregnant. On top of that,
we both were supposed to have been kicked out of our homes by
our parents, threatened with the loss of college scholarships, and
facing the possibility of serious physical problems due to
venereal disease. On that basis, we were supposed to decide,
with the help of the rest of the class, whether or not we should
have an abortion.”

“Of course, everyone in the class got a big kick out of all this,”
Tom said. “They knew that Missy and I are both Christians and
that the whole premise of the silly charade was an insult to us.”

“When Tom explained right off that we'd have to accept the
consequences of our sin, if we ever did fall into such rebellion, well
the teacher got really mad at us, made fun of us, and then had the
gall to give us a failing grade for the exercise,” Missy recalled.

“The whole thing-was a real eye-opener for me,” Tom said.
“Tt seems there is room in Planned Parenthood’s pluralism for
anyone and everyone except Christians.”

Peer Facilitation. If word of mouth is the best advertising and
satisfied customers are the best endorsement, then it only stands
to reason that the best propaganda is peer propaganda. That is
the idea behind Planned Parenthood’s Peer Facilitation strategy.
Teens who display “leadership qualities” are recruited to be “sex
educators of their peers” and even of “younger children with
whom they may come into contact.” These leaders are given
“intensive personalized training” so that they will later be able to
“facilitate healthy sexual messages and behaviors” among other
