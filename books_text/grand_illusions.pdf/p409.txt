End Notes 389

. See George Devereux, “A Typological Study of Abortion in 350 Primitive,

Ancient, and Pre-Industrial Societies,” in Harold Rosen, ed., Abortion in America
(Boston: Beacon Press, 1967).

, See David Bakan, The Slaughter of the Innocents (Boston: Beacon Press, 1972).
. See Paul H. Gebhard, Wardell B, Pomeroy, Clyde E, Martin, and Cornelia V.

Christenson, Pregnancy, Birth, and Abortion (New York: Harper Brothers, 1958).

. See Augustus Ambrose, The Women of Gilead: The Abuse of Women and Children in

History (New York: Uptown Bookseller, 1957).

. Plato, The Republic 5.9; Aristotle, Politics 7.1.1, and Aristotle, Historia Animalium

7.3.

. Juvenal, Satire 6.592-601; and Chrysostom, Homily Tiwenty-Four on Romans 3.4.1.
. Soranos, Gynecology 1.19.

. Ambrose, Hexameron 5.18.58; and Hippolytus, Refutation of All Heresies 9.7.

. Justinian, Digeit 48.19.39.

. Calactus, Dogmas 14.9.2.

. A. L. Cameron, “The Exposure of Children and Greed Ethics,” The Classical

Review 46:3 { July 1932), pp. 105-114.

. Robert M. Grant, Augustus to Constantine (New York: Harper and Row, 1970),

pp. 97-99, 272.

, Justinian, Digest 47.11.4; 48.8; 48.19.38-39.
. Albert Karmer, Witness to Truth: The Early Church and Social Reform in the Empire

(New York: Ballister Historical Review, 1951), pp. 92-f11,

. See Gorman, pp. 47-90.
, Didache 2.2.

. Epistle of Barnabas 19.5.

, Athenagoras, A Plea for the Christians 35.6.

. Clement, Paedagogus 2:10.96.1.

. Tertullian, Apology 9.4.

. Basil, Canons 188.2,

, Ambrose, Hexameron 5.18.58.

, Jerome, Letter to Eustochium 22.13.

. Augustine, On Marriage 1.17.15.

. Origen, Against Heresies 9.2.; and Homily Ten 21.22.
. Hippolytus, Refuation of All Heresies 9.7.

. Cyprian, Letters 48.3.

. Methodius, Fragments 6.1.3.

, Chrysostom, Homily Twenty-Four on Romans 3.4.1.

| Minucius Felix, Octavius 30.16,

, Gregory Nazianzus, Oratory 43.66.

. See Karmer, p. 109.

, See Fox, pp. 343, 351, 382.

. See Karmer, p. 142.

. Psalm 68:6
. Ronald L. Numbers and Darrel W. Amundsen, eds., Caring and Curing: Health

 

and Medicine in the Western Religious Traditions (MacMillan Publishing Company,
1986), pp. 40-64.

. Bid., pp. 146-172.
. See George Grant, The Dispossessed: Homelessness in America (Westchester, IL:

Crossway Books, 1986).

. See George Grant, Bringing in the Sheaves: Transforming Poverty into Productivity

(Atlanta: American Vision Press, 1985).

. See David Chilton, Power in the Blood: A Christian Response a AIDS (Brentwood,

TN: Wolgemuth & Hyatt Publishers, 1987),
