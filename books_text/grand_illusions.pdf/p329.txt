Altars for Construction: An Agenda for the Future 309

and function to form (Deuteronomy 5:12; Hebrews 10:24-25).
These disciplines enabled them to wait on the Lord and thus to
“run and not be weary” and to “walk and not be faint” (Isaiah
40:31). Even the years were given special structure and signifi-
cance. In ancient Israel, feasts, festivals, and fasts paced the
believers’ progression through the months (Exodus 13:6-10;
Psalm 31:15). The early church continued this stewardship of
time, punctuating years with the Christian holidays: Advent,
Christmas, Epiphany, Lent, Easter, Ascension, and Pentecost—
each marked by feasts, festivals, and, of course, fasts. Thus,
God’s people were enabled and equipped to run the race (Philip-
pians 2:16), to fight the fight (Ephesians 6:10-18}, to finish the
course (2 Timothy 4:7), and to keep the faith (2 Timothy 3:10).
If we are to attain to even just a little of their potency, power, and
prowess, it is crucial that we return to these patterns of humility
and dependence.

We can’t fight something with nothing. We can’t simply say “no”
to Planned Parenthood; we must say “yes” to the fullness of the
Christian faith and disciplines. Like Chrysostom, Hus, and
Spurgeon, we must say “yes” to utter dependence upon God. We
must say “yes” to fasting.

Where the Rubber Meets the Road

Mark Lincoln is the pastor of a small Presbyterian church in
central California. Active in the pro-life movement for more
than a decade, he recently has gone through a time of tremen-
dous discouragement, frustration, and burnout. “I guess I just
got to the point where I had come to the end of myself,” he said.
“I had worked for years and had very little fruit to show for it. Our
church’s crisis pregnancy center was floundering— understaffed,
underfinanced, and underexposed. Several of the members of
the church had been arrested and then convicted of trespassing
on the property of Planned Parenthood during a prayer protest.
Our workers were exhausted and our resources were depleted.
And we didn’t have a thing to show for any of that. We were all
full of heartache and despair.”

Mark decided to take off several days to spend some time at a
retreat center. “I needed to regain some perspective. I needed a
new focus. It seemed like the enemies of God were racking up all
