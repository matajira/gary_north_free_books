264 Granp ILLUSIONS

Adam was called to be a priest. He was to “cultivate and
guard the garden” (Genesis 2:15). When he failed to do his duty,
the Fall resulted and judgment fell on the land (Genesis 3:1-20).

Aaron was called to be a priest. He was to guard the people
from sin and shame (Exodus 32:25). When he failed to do his
duty, the people began to worship and revel before a golden calf
and judgment fell on the land (Exodus 32:1-6).

The Levites were called to be priests. They were given that
honor and privilege because they guarded the integrity of God when
all the rest of Israel was consumed with idolatry (Exodus 32:26-29).
It was not until they completely failed to do their duty that con-
demnation and judgment befell the land ( Jeremiah 6:13-15).

Jesus commissioned the church to take up the priestly task
(Matthew 28:19-20; 1 Peter 2:5). It was to preserve, protect, and
guard the nations.

You are the salt of the earth; but if the salt has become taste-
less, how will it be made salty again? It is good for nothing any-
more, except to be thrown out and trampled under foot by
men (Matthew 5:13).

If the church refuses the priestly mantle of Adam, Aaron,
and the Levites, the poor and helpless will, indeed, be trampled
under foot by men. They will be crushed by the likes of Planned
Parenthood.

So now, in practical terms, just exactly how is the church to
manifest its prophetic and priestly roles in the struggle for life?
Once believers have been taught, exhorted, equipped, and com-
missioned, what then? Once they have worshiped, prayed, and
proclaimed, what then? What do Christians do once they leave
the four walls of the church?

Very simply, we go out and change the world.*! We serve the
whole of society guiding and guarding the land: in the schools,
in the media, in local government, in the legislature, in the
courts, and in the bureaucracy.

The Schools
“A nation at risk.”42 That is how the United States Depart-
ment of Education describes the state of the Union, as viewed
from the classroom.
