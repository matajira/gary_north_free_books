143,

144,
145,
146,

147.
148.
149,
150,

151.
152.

End Notes 373

Samuel L. Blumenfeld, “The Fraud of Educational Reform,” The Journal of
Christian Reconstruction 11:2, 1987, p. 22.

Ibid., p. 26.

Texas Education Review, May 1992.

R, J. Rushdoony, “Education: Today’s Crisis and Dilemma,” The Journal of
Christian Reconstruction 11:2, 1987, p. 68.

Robert L, Dabney, Discussions (Harrisonburg, VA: Sprinkle Publications,
1879), p. 195.

See Robert Thoburn, The Children Trap: Biblical Principles of Education (Fort
Worth: Dominion Press, 1986); and Gregg Harris, The Christian Home School
(Brentwood, TN: Wolgemuth & Hyatt Publishers, 1988).

Thoburn, p. 79.

Bid., p. 123.

Ibid., p. 23.

Ibid., pp. 86-89.

Chapter 7— Robber Barons: The Financial Scandal

1.
2.
3

i.
12.

4,

“An appeal based on profit.”

Hilaire Belloc, The Path to Rome (New York: Penguin Books, 1958), p. 66.
See Robert Ruif, Aborting Planned Parenthood (Houston: New Vision Books, 1988)
pp. 9-34. This important book is available from Life Advocates, 4848 Guiton,
Suite 204, Houston, TX 77027 or New Vision Books, P.O, Box 920970-A16,
Houston, TX 77018.

. See Erma C. Craven “Abortion, Poverty, and Black Genocide,” in Thomas

Hilger and Dennis J. Horan, Abortion and Social Justice (New York: Althea Books,
1981); Tim Zentler, for Planned Parenthood of Humboldt County, quoted in
The Union, 14 June 1983; Martha Burt, Public Costs for Teenage Childbearing
(Washington, DC: Center for Population Options, 1986); Charles Murray,
Losing Ground; Amerisan Social Policy 1950-1980 (New York: Basic Books, 1984);
George Gilder, Men and Marriage (Gretna, LA: Pelican Publishing Company,
1986); George Grant, The Dispossessed: Homelessness in America (Westchester, HL:
Crossway Books, 1986); and George Grant, In the Shadow of Plenty (Nashville:
Thomas Nelson, 1986).

 

. USA Today, February 3, 1988.
. USA Today, August 10, 1987.
. Ken McKee, “Planned Parenthood and Title X,” All About Issues, March 1984,

pp. 8-9.

. W. Douglas Badger, “Legislative History of Section 1008 of the Public. Health

Service Act,” Action Line, February 1985.

. Lid,
. See Frederick $. Jaffe, Barbara L. Lindheim, and Philip R. Lee, Abortion

Politics: Private Morality and Public Policy (New York: McGraw-Hill Book Com-
pany, 1981); Kristin Luker, Adortion and the Politics of Motherhood (Los Angeles:
University of California Press, 1984); and Linda Gordon, Woman's Body,
Woman's Right: A Social History of Birth Control in America (New York: Penguin
Books, 1977).

Bid.

Howard Phillips, The Newt Four Years: A Vision of Victory (Franklin, TN: Adroit
Press, 1992), pp. 23-24, 55-67.
