APPENDIX B

PRO-LIFE ACTIVISM:
A SOCIAL GOSPEL?

in operibus sit abundantia mea!

 

One should never leave a man without giving him something to show, by way of
token, on the Day of Judgment.?

Hilaire Belloc

Around the turn of the century, a new theology grew to
prominence among Liberal Protestants in Europe and America.
Tt was dubbed the Social Gospel movement.

Essentially, Social Gospel advocates promoted a message of
“salvation by works.” They argued that true spirituality consisted
of good deeds in the realm of politics, economics, and social jus-
tice. Very much like the modern-day proponents of Liberation
Theology, they reduced the Good News of Jesus Christ to a revo-
lutionary ideology.*

By suggesting that authentic Christianity will inevitably con-
cern itself with the plight of the poor, it may seem to some that I
have accepted, at least in part, the tenets of this aberrant theol-
ogy. Nothing could be further from the truth, however.

“Salvation by works” or “salvation by Law” is the most an-
cient of all heresies. It is indeed, “another Gospel” (Galatians
1:6). It is a “different Gospel” (Galatians 1:7). It is a “contrary
Gospel” (Galatians 1:9). It is a Gospel repudiated throughout
both the Old and New ‘Testaments: Abraham condemned it
(Genesis 15:6; Romans 4:3; Galatians 3:6); Moses condemned it
(Deuteronomy 27:26); David condemned it (Psalm 32:1-2;
Psalm 51:1-17); Isaiah condemned it (Isaiah 1:10-18); Jeremiah
condemned it (Jeremiah 4:1-9); Amos condemned it (Amos
5:1-17); Habakkuk condemned it (Habakkuk 2:4); Paul condemned

321
