FOURTEEN

ALTARS FOR
CONSTRUCTION:
AN AGENDA FOR

THE FUTURE

adeste fideles!

 

Aman who knows that the earth is round but lives among men who believe it to be
flat ought to hammer in his doctrine of the earth’s roundness up to the point of arrest,
imprisonment, or even death. Reality will confirm him, and he ts not sa much
testifying to the world as it is— which is worth nothing — as to Him who made the
world, and Who ts worth more than ail things.?

Hilaire Belloc

We can’t fight something with nothing.

That is a basic truth that the church has always understood.?
Its great struggles against darkness, defilement, death, and
deception through the ages have simultaneously been struggles
for light, loveliness, life, and liberty, Wickedness was always met
with righteousness, not just righteous indignation.

Thus, in our efforts to defend the helpless,the hopeless, and
the harassed in our own day, it is important that we not simply
say “no” to Planned Parenthood; we must say “yes” to the fullness
of the Christian faith and its disciplines.

John Chrysostom knew only too well that you can’t fight
something with nothing. The great fourth century saint was
renowned far and wide for his scintillating and prophetic
oratory. His sermons, first in the city of Antioch and then in
Constantinople itself, attracted throngs of rapt listeners. He was
known as the “golden tongued” preacher and glorious revival

291
