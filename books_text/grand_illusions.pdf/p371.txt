85.

86.

87.

St.

92.
93.
94.

95.

96.
97.
98.
. Lia.
100.
101.
102.
103.
104.
105,

End Notes a1

“Celebrating,” p. 32. This is one of the reasons why accumulating accurate data
and statistics on Planned Parenthood is so difficult, and why comprehensive in-
formation about its activities and funding sources is so easy to conceal.

What else can you call the world’s number one dispenser of abortion and birth
control devices than a “family banning” organization? See Franky Schaeffer,
A Time for Anger: The Myth of Neutrality (Westchester, IL: Crossway Books, 1982),

. 99,

Seo Chapter 7. Although most of the Planned Parenthood agencics mect the
legal IRS standards for non-profit status— putting them on a par with, for in-
stance, a local church—they accrue to themselves vast, vast sums of money.
Legal designations aside, by any other standard, Planned Parenthood’s abor-
tion and contraceptive industry is phenomenally profitable. “Non-profit” is thus
very much in question as well.

. Robert Ruff, Aborting Planned Parenthood (Houston: New Vision Books, 1988).

The book is available from Life Advocates, 4848 Guiton, Suite 209, Houston,
TX 77027 or from New Vision Books, P.O. Box 920970-A16, Houston, TX
77018.

. Ebid.
). Let me note as a matter of clarity that Planned Parenthood is only one of nearly

five thousand clinics, hospitals, and other health scrvice providers that receive
Title X funding. But as in the Tide V, Title XX, Title XEX, and all the other
federal appropriations bills mentioned in this chapter, Planned Parenthood is
the single largest recipient in any and all of its sundry incarnations. Additionally,
we should note that in only fourteen states in the United States, and about
seventy-six foreign countries, it is actually legal for Planned Parenthood to
spend its tax bequest on abortions or abortion-related activities. On this point,
Franky Schaeffer's comments from his book A Time for Anger are instructive:
‘Planned Parenthood’s most consistent claim is that it does not use federal
money to fund abortion. This is probably technically true. But the family plan-
ning centers of Planned Parenthood operate in conjunction with medical
clinics, which do perform abortions, Planned Parenthood’s propaganda helps to
convince women to have abortions, so their claim is rather like a pimp saying
he has nothing to do with prostitution” (p. 99),

Howard Phillips, The Next Four Years: A Vision of Victory (Franklin, TN: Adroit
Press, 1992), pp. 24, 55-67.

Ibid.

Bid.

Robert G. Marshall, School Bisth Control: New Promise or Old Problem (Stafford,
VA: American Life League, 1986), p. 1.

Talon Gartrell, Abortion and the States: A Case Against the lechnical Elimination of
Federal Jurisdiction and Roe vs, Wade (Manassas, VA: Life Work Publications,
1987), p. 3.

Alan Guttmacher Institute, Zssees in Brief 4:1 (March 1984).

Ibid.

bid.

Tid.
Ibid.

Gartrell, p. 12

Bid, p. 12,

bid. p. 2.

“Celebrating,” pp. 14-16; Gartrell, pp, 2-4; and William M. O'Reilley, The
Deadly Neo-Coloniatism (Washington, DC: Human Life International, 1986).
