FOUR

BACK-~ALLEY BUTCHERS:
THE MEDICAL LEGACY

aegrescit medeno'

 

There is no fortress of man’s flesh so made, but subtle, treacherous time comes
creeping in. Oh, long before his last assaults begin, the enemy’s on; the strong-
hold is betrayed, and the one lonely watchman, half-dismayed, beyond the cover-
ing of dark, he hears them come: the distant hosts of death that march with
muffled drum.?

Hilaire Belloc

The overcast sky hung in gray strips over the city—pale
where the sun nearly broke through the clouds, darker where
stubborn patches of rain rode the currents of a lolling stormy
breeze. ‘The glaze of the heavens permitted no shadows, only a
darkening of color here and there, and a dulling of perception.

Caroline Ness told me that her life was like that now.
“Dreary,” she said. “Sad and dreary.’

Her thick blonde hair fell in long, loose waves to her
shoulders. Her eyes were as blue as poker chips. Backlit by
bright neon, her slim and elegant frame bore an aristocratic
beauty. But her expression was as dim and distant as a star in
half light.

“I feel like a caged animal—trapped by a terrible and tragic
past.” She turned, her cold gaze piercing me. “And there’s no
way out.”

We had just climbed the stairs from the Columbus Circle sta-
tion into the mid-Manhattan bustle near Lincoln Center. The
long clackety IRT ride from Wall Street through Soho, Green-
wich Village, Chelsea, the Garment District, Times Square,
and the Theater District had afforded us a unique opportunity to

67
