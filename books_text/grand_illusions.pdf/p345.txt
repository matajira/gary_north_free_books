Appendix B: Pro-Life Activism 325

15;54-56). Jesus “tasted death for everyone” (Hebrews 2:9), actu-
ally “abolishing death” for our sakes (2 Timothy 1:10) and offering
us new life (John 5:21),

For God so loved the world, that He sent His only begotten
Son, that whosoever believeth in Him should not perish, but
have everlasting life (John 3:16).

One of the earliest Christian documents — actually predating
much of the New Testament — asserts that “There are two ways:
a way of life and a way of death.”* In Christ, God has afforded us
the opportunity to choose between those two ways—to choose
between fruitful and teeming life on the one hand, and barren
and impoverished death on the other (Deuteronomy 30:19).

Apart from Christ it is not possible to escape the snares of sin
and death (Colossians 2:13). On the other hand:

If any man be in Christ, he is a new creation; old things have
passed away; behold, all things have become new” (2 Corin-
thians 5:17).

All those who hate Christ “love death” (Proverbs 8:36); while
all those who receive Christ are made the sweet savor of life
(2 Corinthians 2:16).

This is the faith, “once and for all delivered to the saints”
(Jude 3). This is not a Soctal Gospel, it is simply The Gospel.

Even so, this Gospel truth is not supposed to be mere
dogma, it is to be worked out in every detail and dimension of
life. Thus, all those who walk the path of life (Psalm 16:11; Prov-
erbs 2:19; 5:6; 10:17; 15:24), and eat from wisdom’s tree of life
(Proverbs 3:18; 3:22; 4:13; 4:22-23), are made the champions of
life (Proverbs 24:10-12).

Ultimately, this is where the whole notion of good works fits
into Biblical theology. The Apostle Paul tells us that though we
are saved “by grace through faith” (Ephesians 2:8), “not as a
result of works” (Ephesians 2:9), we are “created in Christ Jesus
jer good works” (Ephesians 2:10).

In other words, good works are something we do decause we
have been justified, not in order to de justified. Concern and
care for the innocent, helpless, poor, and afflicted is an effect of

  
