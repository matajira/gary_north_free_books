Selling Sex: The Educational Scandal Lis

Roommate 3: You feel whatever anyone does sexually is their
business, but you feel very sad that your friend has closed off
lots of options.*!

Roommate 4: You're a psychology major, and try to Aelp by
giving advice and diagnosing why your roommate might be gay.52

Roommate 5: You feel threatened by the knowledge that your
roommate is gay. You try to reason with him and argue him into
heterosexual good sense.5?

Roommate 6: You are shocked by the announcement and out-
raged that a fag will be on your dorm floor.5+

Roommate 7: You already know about your friend’s gay lifestyle.
The two of you have talked some about it. You have no serious
difficulties with this and stilt feel comfortable with him.55

After playing their roles, the seven students are then asked to
“come to a consensus” about which of the roommates’ attitudes is
“the most constructive.”

Martin Campbell was forced to participate in that very
scenario in a Planned Parenthood-sponsored class at his high
school near Chicago. “After we played our parts,” he told me,
“we had to de-role and then analyze our feelings in a group dis-
cussion. The teacher asked us what stereotypes of homosexuality
had emerged in the skit. And then we were supposed to talk
about why those stereotypes were wrong and based on ignorance
and fear. Well, I was really hacked off by the whole deal. I felt
like I was being set up.”

When the teacher found that Martin was acting a bit recalci-
trant, she began to lecture him about being open, tolerant, accept-
ing, mature, respectful, and honest. “I just kept telling her that I
didn’t agree, and wouldn’t agree, but she wouldn’t let up on me,”
Martin said. “It’s pretty bad when a teacher isolates one kid like
that. I felt like I was getting ganged up.on. It wasr’t at all _fazr.”

Positive Imaging. Very similar to role-playing techniques is
Planned Parenthood’s fantasy, or Positive Imaging, methodol-
ogy. According to one school “mental health” proponent: “The
concept of educational imagery is used to approximate a de-
scribed behavior, decision or outcome through guided imagina-
tion or fantasy in the conscious mind of the individual. In
