Idols for Destruction: A Strategy for Prophets and Priests 277

foolishness” (1 Corinthians 1:23). That is because the rule of law
is a Christian idea, made possible only by the revelation of law
from on high. And all too many men “suppress” that truth in one
way, shape, form or another (Romans 1:18).

Thomas Jefferson acknowledged this saying:

Can the liberties of a nation be sure when we remove their only
firm basis, a conviction in the minds of the people, that these
liberties are the gift of God? That they are not to be violated
but with his wrath? Indeed, I tremble for my country, when I
reflect that God is just; that His justice cannot sleep forever,
that revolution of the wheel of fortune, a change of situation, is
among possible events; that it may become probable by super-
natural influence! The Almighty has no attribute which can
take side with us in that event.”?

In order to protect and preserve any rights we must protect
and preserve ail rights — beginning with the fundamental rights
of life, liberty, and the pursuit of happiness. But in order to protect
those rights, we must return to that distinctly Christian notion
that the God who providentially rules the affairs of men has
already inalienably endowed these rights to each of us.

Again and again the Court has undercut the rule of law prin-
ciple and reinforced the barbarism of child-killing~—thus it has
undercut the very standard of liberty itself.

The lower courts have proven to be equally hostile to life and
liberty. Repeatedly stiff sentences have been handed down to
abortion protesters, alternative service providers, and pro-life
organizers, often on trumped-up charges.

Clearly, if we are going to serve society as prophets and
priests, guiding and guarding the land, we are going to have to
help turn the courts around.

There are several things we can do to do just that.

First, we can utilize our vote. Many of the judges that wreaked
havoc on life and liberty in our land are elected to the bench and
they must stand for re-election. So, why not mount a campaign
to replace pro-abortion, anti-family judges? Why not spend as
much time and energy on those obscure judicial races as we do
on legislative campaigns? In that way we can cut right to the
heart of the matter.
