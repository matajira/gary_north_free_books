294 Granp ILLusions

first in Victorian London, and then all around the globe, drew
unprecedented crowds week after week, year after year, and dec-
ade after decade. He was known as the “prince of preachers” and
enthusiastic revival followed him wherever he went. This despite
the fact that he was uncompromising in his doctrinal fidelity, he
was forthright in his non-conformity, and he was unflinching in
his opposition to vice and corruption.

Why was he so popular? He seemed to break every homiletical
rule in the book. His sermons were too coarse. His theology was
too rigid. His passion was unbridled. And his temperament was
colloquial. How then was he able to attract such a loyal following?

According to Spurgeon’s contemporary W. C. Wilkinson,
the great preacher’s sermons were a “steady unfailing river of
Biblical utterance.”'* During his remarkable ministry, he held to
“an absolute, simple, single fidelity, maintained by him through-
out, maintained unintermittingly, from the juvenile beginning
to the culminating maturity of his work—the serene, unper-
turbed, untempted fidelity of mind, of heart, of conscience, of
will, of all that was in him, and all that was of him, to the mere
and pure and unchanged and unaccommodated Gospel of Jesus
Christ, the same yesterday, today, and forever. That stands up
and out in his sermons, that lifts itself and is eminent, like a peak
of the Himalayas, high regnant over all the subjected high table-
land of this noble church.”!”7 What his preaching did “was to
present to his hearers the one unchanging Gospel in countless
changes of form, each perfectly level to the comprehension of all.
He turned and turned the kaleidoscope of the sermon, and ex-
hibited to his hearers, never weary of beholding, the same
precious Biblical truths, over and over again.”!8 Unlike many
preachers in his day, he was never tempted to entertain “the little
waves of scientific guess, of new theologic shift, or of filial culture
seeking to replace Biblical ethics with pagan aesthetics.” He
never “stood before his hearers like a reed shaken with the
wind.”20 Instead, “he stood solid on the Rock of Scripture, with
the whole balanced weight of his great persona.”*! Wilkinson
concluded saying, “Spurgeon was one of the greatest preachers
of all times and of all climes. Such is the indefeasible heritage of
anointed exposition,”*2

Spurgeon was a Biblical preacher. And there is no doubt
that his pulpit prowess set him apart from most, if not all, of
his contemporaries.
