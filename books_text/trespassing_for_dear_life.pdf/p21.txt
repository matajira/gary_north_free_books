Obedience to God and Not to Men 17

Because of the nature of the public rebellion of the civil rulers
against God, treason against the government was obedience to
God.

The Hebrew midwives and Rahab took grave risks. They
might have been executed. This risk. was inescapable, given the
nature of their deception. To have fled would have been either im-
possible (the midwives in Egypt) or self-defeating (Rahab’s subse-
quent deception of the rulers). This raises a very important point
that must be understood very clearly before anyone chooses to in-
volve himself in similar acts of civil disobedience, These women
placed themselves under the threat of external civil sanctions, This was the
brice of a successful rebellion. To have avoided these risks, they would
have had to flee. Their unwillingness to flee placed them under
the rebellious state’s sanctions. They might have been executed.
But they faced this danger without visible flinching. In fact, their
courage must have been part of the success of their plan of civil
disobedience. Had they shown fear, their lies might have been de-
tected. Only because they did not show fear did the rulers accept
their lies as true.

Another case in the Bible of someone who broke the law of the
state through deception was Jehosheba, who saved the life of the
infant heir to the throne, Joash. “And when Athaliah the mother
of Ahaziah saw that her son was dead, she arose and destroyed all
the seed royal. But Jehosheba, the daughter of king Joram, sister
of Ahaziah, took Joash. the son of Ahaziah, and stole him from
among the king’s sons which were slain; and they hid him, even
him and his nurse, in the bedchamber from Athaliah, so that he
was not slain” (II Kings 11:1-2). By whose.authority did she do
this? By her own, under God. She took the baby to God’s house,
which served as a sanctuary for him until he came of age. “And he
was with her hid in the house of the Lorn six years. And Athaliah
did reign over the land” (II Kings 11:3). There was no lower civil
magistrate involved here. The senior officer of the church took full
tesponsibility for this revolt against civil authority. He surely de-
ceived the civil magistrates.
