Criticisms of Operation Rescue 29

I would also ask this: Is it awful for Christians in Communist
China to resist their civil magistrates today, since abortion is com-
pulsory there after the first child? Would these deacons say that it
is immoral for Western Christians to smuggle Bibles into Red
China, as well as tracts showing the Chinese ways to resist this
evil compulsory abortion law?

Are Christians so.downright blind today that they cannot see
what will come next if Roe v. Wade isn’t overturned? Will the civil
magistrates have to drag our wives and daughters to the compul-
sory abortion mills before these shepherds figure out that Roe u
Wade is in fact only stage one in the humanists’ program of legalized
euthanasia? In Holland, mercy killings have now been legalized;
first abortion was legalized, then the murder of the aged. But
these shepherds still have not caught on.

In 1925, the humanists said that all they wanted to do was to
get Darwinian evolution taught in the public schools alongside the
creation story, “That’s all we're asking. We promise. Trust us!”
Christians did, too. Surprise!

Bait and Switch

‘Armed resistance by Christians is illegitimate except when a lesser mag:
istrate authorizes it, By what authority do these anti-abortion tnterposers
operate?

Two different issues are being raised. The first is armed inter-
position. The second is non-violent interposition. The two are not
the same. It is biblically illegitimate to require members of the sec-
ond group (non-violent resisters) to be bound by the biblical laws
governing the first group (armed revolutionaries). To argue that
they are so bound is deliberately to mix separate legal categories.

If the physical interposers who block the doorway of an abor-
tion clinic remain peaceful, they are not required by God to seek
authorization by any civil magistrate. Did the apostles seek the
authorization of the “lesser magistrate” when they entered the
Temple and synagogues and preached what the Jewish priests and
Roman rulers had forbidden (Acts 4:15-19)? Obviously not. They
