God's Covenant Lawsuit ML

In Biblical law, all life is under God and His law, Under
Roman law, the parent was the source and lord of life. The father
could abort the child, or kill it after birth. The power to abort,
and the power to kill, go hand in hand, whether in parental or in
state hands. When one is claimed, the other is soon claimed also.
"To restore abortion as a legal right is to restore judicial or par-
ental murder (p. 186).

Conclusion

Christians must now make up their minds: Are they going to
assent to legalized murder or oppose it publicly? Are they going to
break the civil law as a means of challenging it as a test case, or
are they going to allow humanists to continue to authorize the
murder of babies? The U.S. Supreme Court has overturned its
own prior rulings at least 150 times. Are Christians ready to give
the Court an opportunity to do it again?
