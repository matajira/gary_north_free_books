14 Trespassing for Dear Life: What About Operation Rescue?

who have the law of Gad itself written on theit hearts (Hebrews
8:9-10; 10:16). Nevertheless, the work of the law. testifies against
all men when they rebel against God’s law. They know better. The
redeemed person in principle knows best, but the unregenerate at
least knows better when he sins.

The human conscience is not perfect in its transmission of
God's warnings. [ts signals can be ignored by a person for so long
that he or she no longer responds. Paul calls this a seared conscience:
“Now the Spirit speaketh expressly, that in the latter times some
shall depart from the faith, giving heed to seducing spirits, and
doctrines of devils; speaking lies in hypocrisy; having their con-
science seared with a hot iron” (I Timothy 4:1-2).

Christians do not take these words literally, of course. We do
not believe that a literal hot iron can sear a person’s conscience.
Paul was using a metaphor. A bleeding wound can be sealed up
by applying a hot iron to’ it, but the nerve endings beneath the
skin may be permanently destroyed. The person later may lose all
feeling on the seared portion of his flesh. So it is with sin. If false
doctrines or evil acts are indulged in, they can sear the con-
science. No longer will the individual hear the warning voice of
God. Again, this is not a literal voice. The conscience is representa-
&vely the voice of God, but it is nonetheless conscience, not liter-
ally a voice.

Self-Government Under Civil Authority

The ultimate lawful authority to inflict physical and all other
sanctions belongs to God, “Vengeance is mine; I will repay, saith
the Lord’ (Romans 12:19b), He delegates this authority to families
over young children and to civil governments. Romans 13 makes
it clear that an individual is always under some form of civil. au-
thority. The civil magistrate is actually called “the minister of
God? (verse 4), The minister of civil justice possesses lawful au-
thority to impose physical punishments on those under the state’s
jorisdiction. Individuals are not to inflict corporal punishment on
others, except in the case of parents punishing their minor chil-
dren, and schoolteachers or other parent-designated authorities
who do the same as lawful representatives of the parents.
