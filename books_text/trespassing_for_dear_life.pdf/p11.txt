2
GOD'S COVENANT LAWSUIT

The prophets of the Old Testament were authorized agents of
God. They were His prosecuting attorneys. They brought a cove-
nant lawsuit against the nation. They reminded the people, the
nobles, and the king of the covenant that God had made with
their forefathers at Sinai. Then they reminded the listeners of the
stipulations (laws) of that original covenant. They pointed to the
obvious violations of these stipulations in their day. Then they
warned everyone of the fact that God, the true king of Israel, would
bring His negative sanctions against the nation: war; pestilence
and famine. All of these negative sanctions had been spelled out
in the original covenant document (Deuteronomy 28:15-68).
Finally, the prophets called the nation to repentance, promising
the blessings of God — positive sanctions (Deuteronomy 28:1-14) —
if the nation did repent. Understand, these sanctions— positive
and negative, blessings and cursings— were applied corporately to
the whole nation. They were not simply sanctions against personal
sins. When the two parts of the nation were sent into captivity,
rightcous people as well as evil people were taken cut of the land.

This office of prophet culminated in the person of Jesus Christ.
His cousin John had brought a preliminary covenant lawsuit _
against Israel. He then baptized Jesus. From that point on, Jesus
brought the main covenant lawsuit against Israel. (John was exe-
cuted when he brought God’s personal covenant lawsuit against
Herod and his wife.) When Israel refused to repent, God raised
up His church, Not only was the church required to bring cove-
nant lawsuit against Israel, it was required to bring the same law-

7
