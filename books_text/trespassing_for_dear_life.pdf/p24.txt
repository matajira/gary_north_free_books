20 Trespassing for Dear Life: What About Operation Rescue?

tive physical sanctions against those in office over them. Ven-
geance was God’s, as it is today, But they were required by God to
act as law-abiding righteous people by lying to the rulers, confus-
ing them, and thwarting their proclamations. Then God brought
the rulers low.

When Paul was brought before the Roman council in Jeru-
salem, the room was filled with Jewish religious leaders, who were
in fact subordinate rulers to Roman civil authority. They had
already admitted this in public at the most judicially critical point
in Israel’s history, the crucifixion of Christ the Messiah: “But they
cried out, Away with him, away with him, crucify him. Pilate
saith unto them, Shall I crucify your King? The chief priests an-
swered, We have no king but Caesar” (John 19:15).

The Jewish leaders were divided between Pharisees, who be-
lieved in the resurrection of the dead at judgment day, as the Old
Testament taught (Daniel 12:1-3), while the Sadducees, who ruled
the temple, rejected this doctrine. So, when Paul testified to the
Roman authorities, he told them the truth, the partial truth, and
everything but the whole truth, He announced that he was on
trial because he was a Pharisee and believed in the resurrection.
Yes, he was a Pharisee—by birth. Yes, he believed in the resurrec-
tion — first of Jesus Christ, then of Christians, and then the unbe-
lievers (I Corinthians 15), This was hardly orthodox Pharisaical
doctrine. But he neglected to mention these “minor” doctrinal
qualifications. Immediately, the two Jewish factions began
screaming against each other, and the meeting broke up (Acts
23:6-10). Thus, he escaped civil judgment that day.

The mora} and legal dilemma arises when therc is a conflict
among these lawful voices of authority. One or more of these God-
authorized voices of lawful authority may issue commands that
are in conflict with God’s Bible-revealed law. What is the Chris-
tian supposed to do?

Conclusion

We have seen that all covenantal government is hierarchical.
Someone or some lawful agency must speak in the name of the
god of that society or group. Biblically, men are required by God
