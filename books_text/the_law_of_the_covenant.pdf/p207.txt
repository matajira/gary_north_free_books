188 The Law of the Covenant

meaning of this stipulation was to point to the fact that Passover
was simply a figure of the salvation to come. It sustained the peo-
ple annually, but would be superseded at the Resurrection of
Christ, when the Sun of Righteousness would arise on Easter
morning. Similarly, the peace sacrifice was not to be consumed
after the third day (Lev. 7:17), pointing to its supersession in the
third-day resurrection of Christ.

Eating signifies application and identification. The death of
the substitute lamb was applied to the people as they ate it. Just so
today, the sacrifice of Jesus Christ is applied to his people as they
eat the sacramental bread and wine. The action of the sacrifice is
long since over and done with, and Christ has been raised and is
seated in heaven; but the food of the sacrifice is still being taken
from the altar and eaten, thus applying His death to His people,

On the day after the sabbath that came during the Feast of
Unleavened Bread, the first sheaf of picked grain was waved be-
fore the Lord (Lev. 23:10-14). This was the first of the first-fruits,
and is probably referred to in Ex, 23:19a. This ties the Feast of
Unleavened Bread to the Feast of the Harvest. Just as soon as the
Lamb has been offered for the people, immediately the land
begins to blossom with new life and fruitfulness, There is no delay
in the progressive enjoyment of the Kingdom of God.

A parallel symbol will help us understand this better. In John
7:37-39, “Jesus stood and cried out, saying, ‘If any man thirst, let
him come unto Me and drink. He that believes on Me, as the
Scripture has said, out of his belly shall flow rivers of living water.’
But this He spoke concerning the Spirit, which they that believe
on Him were to receive.” Now, what is the water? It is clearly, first
of all, the influence of Christ and the Spirit. It also, however,
flows from individual believers out into the world.

So it is with leaven. The leaven inserted into the world at the
first creation was humanity. Adam and his descendents would
leaven the world, and bring it to fulfillment. This leaven (human-
ity) was corrupted. Thus, the Second Adam came to begin the
New Creation. First and foremost, the leaven inserted into the
Church is the Holy Spirit, who came and leavened the bread of
