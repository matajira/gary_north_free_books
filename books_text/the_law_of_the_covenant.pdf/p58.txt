The Law and the Redemption of Israel 39

24:28; Rev. 19:17-18). While Israel is being kept alive (Ex, 11:7)
and celebrating Passover (Ex, 12), the Egyptians are perishing
and being devoured at a feast for dogs (Ex. 11:4-7), This is parallel
to the two feasts in Revelation 19:7-18—~the Marriage Supper of
the Lamb and the Vultures’ Feast.

In view of the importance and centrality of blood-vengeance
to the plan of God, men must not be surprised when God's law re-
quires capital punishment.

God and Pharaoh

The extended interchanges between Moses and Pharaoh are
important to us for two reasons. First, they show the legal founda-
tion for the exodus, and second, they show us one reason why the
laws concerning slavery are placed first in the section we are in-
vestigating.

Daube points out that “the authors of the exodus story
represented Pharaoh as flouting established social regulations,
and God as making him comply with them, maigré /ui, or suifer the
sanctions of his breaches. They construed the exodus as an en-
forcement of legal claims, As one example of many we may quote
God’s demand to Pharaoh: Israel is my son. ... Let my son
go.’” Daube continues, “What we are at the moment concerned
with is the confidence and stability which resulted from this an-
choring in firm legal relations. As God had vindicated those rela-
tions in the exodus, one could be certain that he would vindicate
them again, and again, unto the last. The kind of salvation por-
trayed in the exodus was not, by its nature, an isolated occur-
rence, giving rise to nebulous hopes for similar good luck in the
future: It had its root in, and set the seal on, a permanent
institution — hence it was something on which absolute reliance
might be placed.”!6 God’s vindication of Israel during the exodus
simultaneously vindicated the legal structure which was to govern
social life in the land of Israel. It was reliable and stable because
God is reliable and stable.

16. Daube, pp. 13f. I am greatly indebted to this book for the insights devel-
aped in this section,
