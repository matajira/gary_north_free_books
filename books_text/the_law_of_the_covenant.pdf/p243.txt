224 The Law of the Covenant

will find that God gives them over to a spirit of folly, and they
make bad business decisions, and lose money. (The book of Hag-
gai deals with this.) When men do not tithe willingly to God’s
Church, God takes the tithe and gives it to His enemies, to raise
them up as a scourge to the Church. When Christians return to
the practice of faithful tithing, God will begin to decapitalize the
wicked, and will give dominion back to His people.
