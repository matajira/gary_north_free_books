80 The Law of the Covenant

substitute for the woman, by pointing to the circumcision of the
promised seed, Jesus Christ, the “bridegroom” whose blood pro-
vides the tokens of virginity that an unfaithful wife (Israel, the
Church) needs as legal evidence of her ethical purity. The circum-
cised baby is called a “bloody bridegroom” (Ex. 4:26). The cut-
ling off of the husband’s flesh is extended or applied to the
woman's flesh also, when the circumcised man goes into his wife
on the wedding night.

Now, these three meanings of circumcision also apply to the
ear, and to the Aands and feet.’ Sinners cannot serve God, so their
ears, hands, and feet are, metaphorically, chopped off or stabbed
through. By putting the blood of the sacrifice on the ear, thumb,
and big toe of the priests (Lev. 8:23f.) and of the cleansed (resur-
rected) Israelite (Lev. 14:14), God ceremonially applied death and
resurrection to them. The enemies of God, however, are per-
manently destroyed in hands and feet (Jud. 1:6, 7). The hands and
feet of the cleansed Israelite are not only (i) resurrected, but are
also (2) re-clothed in the oil of the Spirit (Lev. 14:28), and (3) freed
from sin’s hindrances.

The Bible speaks more specifically concerning the ear. The
ears of sinful men are ethically stopped up (Is. 48:8), so they have
to be opened (Is. 42:20), and God then speaks His Word into the
ear of His servants (Dt. 5:1; 31:28, 30; and many other passages).
Opening the ear is literally “ancovering” the ear, using the same
word for uncovering nakedness (“show,” “reveal,” lit., “denude”: i
Sam. 20:2, 12, 13; 22:8, 17; “ancover”: Ruth 3:4). The man whose
car has been opened not only hears God's Word, he also obeys it
(Is. 50:5; Job 33:16; 36:10, 15). The man whose ear is opened, or

9. See Appendix F.

40. The three meanings of circumcision correspond to the three aspects of
salvation: (1) death-resurrection correlates to judgment and justification; (2)
nakedness-clothing correlates to dominion and adoption-glorification; and (3)
removal of hindrance correlates to obedience and sanctification, (Note: 1 am not
correlating these three meanings of circumcision in any way to the circumcisions of
ears, hands, and feet.) On the relationship of resurrection to the area of
Justification, see R. B. Gaffin, Jr., The Centrality of the Resurrection (Grand Rapids:
Baker, 1978).
