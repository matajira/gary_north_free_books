168 The Law of the Covenant

2. Two laws concerning impartial dealings with personal
adversaries. (General impartiality in all of life.)
B. Four laws dealing with magistrates as they rule in court

and as they govern society:

3. Three laws concerning courtroom justice. (Special
judgments before a court.)

4, One law concerning impartial dealings with non-
believers. (General impartiality in all of life.)

Justice and the Witnesses

ja. You shail not carry about a false report.

1b. Do not clasp hands with a wicked man to be a malicious
witness.

2a. You shall not follow a multitude to do evil;

2b. And you shall not bear witness in a suit so as to turn aside
after a multitude, so as to pervert [justice].

3, And to a poor man, you shall not show partiality in his
cause.

The first command forbids rumor mongering. The spread of
rumor and gossip is one of the most serious problems in any com-
munity of people. Rumors subtly prejudice everyone who hears
them, whether they wish to hear them or not. It is next to impossi-
ble to undo the damage done by gossip and the spread of hasty
and premature judgments throughout a community or church.
This command also implies that false reports are not to be given
in court, since the Hebrew for “carry about” literally means “lift
up,” and thus also implies the giving of testimony before an au-
thority.

The second command explicitly forbids conspiring to give
false testimony in court. The “wicked man” referred to here is the
guilty party. Deuteronomy 19:15-21 orders that a conspirator be
given the same punishment as he intended for the party he hoped
to convict: “Then you shall do to him just as he had intended to do
to his brother” (v. 19a). See the discussion of this principle at the
end of this chapter.
