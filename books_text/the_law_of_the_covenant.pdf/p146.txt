Violence (Exodus 21;12-36) 127

This might seem to cause a theological problem with the ex-
odus itself. Did not God threaten to kill Pharaoh’s son if Pharach
did not let God’s son go (Ex. 4:23)? In reply we may point out two
things. First, not all matters of justice have been committed into
the hands of men. God brings punishment upon whole nations
because of the sins of the fathers, and little children suffer. God
has not delegated this right to human government. Human gov-
ernment deals with individual judgment, but God alone takes
care of the judgment of whole cultures. Second, God's judgrnent
against children is based on the fact that they have the sin of
Adam, and deserve to die. God’s judgment of Egypt at the exodus
was not simply designed to illustrate principles of human legal
justice, but to display His wrath against sin (Ex. 9:16). All chil-
dren deserve to die; it is of the mercy of God that He spares most.

V. The Price of a Slave

Ifa notorious ox, or other vicious animal, kills another man’s
slave, the owner of the animal is not liable to death, but simply
pays for the slave, A study of Leviticus 27:1-7 shows that persons
were valued, depending on strength (sex and age) at between
three and fifty shekels of silver. Thirty, then, is a good average;
and in che case of the slave killed by the ox, thirty shekels of silver
was mandatory.

This really puts the slave in the same category as the animal in
v. 36. If a notorious ox kills another ox, the owner simply has to
make it good. But, that is part of the risk of slavery; you might be
gored by an ox. It is best to avoid slavery if you can.

The comparison of the slave io the animal is interesting. It is the re-
verse of the comparison of animals to men, The slave is subject to
other men in a way analogous to the subjection animals are sup-
posed to have. As we have seen (Chapter 5), the Bible wants the
slave to save his money and buy his freedom, or become adopted
into the master’s household as a son-servant.

As we have seen, our Lord Jesus Christ was born into the
world as a homeborn slave-son, for Hig incarnation was His ear’s
circumcision, On the cross, he was made sin for us, and thus
