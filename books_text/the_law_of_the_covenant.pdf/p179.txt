160 The Law of the Covenant

As above, what we do not find in the Deuteronomy law is the
threat of Divine vengeance,

The garment spoken of here is a heavy cloak, which a poor
man would wrap around himéelf to sleep in at night. It is indeed
permissible to take such a garment as a pledge, though not a
handmill or millstone, since these are needed for the maintenance
of life.” Each night, however, the garment must be returned to its
owner again because it is then needed for the maintenance of life
as Exodus 22:27 states. Leibowitz remarks: “We may well ask:
What benefit accrues to the creditor from a pledge which he has to
return to the debtor, whenever he needs it, and which he may only
keep during such times as it is not needed by the owner?” Ibn
Ezra, citing Se‘adia Gaon, gave this reason: “The creditor would
be afraid, otherwise, of the debtor borrowing from someone elsé
against the same pledge. The Torah thus takes account not only of
the interests of one party—~—the debtor, but of both, including the
creditor as well (cf. 23:3: ‘neither shalt thou favour a poor man in
his cause’).3%

Multiple indebtedness is thus restricted, because the poor
wan cannot put the same cloak up as collateral on several
different loans. The cloak of a widow may not be used as collateral
at all (Dt. 24:17), The modern fractional reserve banking system
violates this principle, since the same money can be loaned out
many times, !9

The law here, as we have noted before, particularly has
reference to the protection of the poorer and more helpless
members of the Bride. It is God who has spread His cloak over his
Bride in marriage (Ruth 3:9; Ezk. 16:8). In the symbolism of
Scripture, the veil is removed from the face of the bride, removing
the barrier of clothing between groom and bride (Gen. 24:65f.).

17. To take a modern example, if a man needs his pick-up truck to conduct his
business, then one must not take it as a pledge or use it as collateral.

18. Leibowiiz, Shemot, p. 418.

19. On this see Gary North, An Introduction to Christian Economics (Phillipsburg,
NJ: The Craig Press, 1973), pp. 12ff. See also Gary North, The Dominion Cove-
nant: Exodus (Tyler, TX: Institute for Christian Economics, forthcoming), com-
ments on Exodus 22:26f,

 

 
