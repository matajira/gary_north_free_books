Subject Index

Naming, power of, 132f.

Nathan, 268

Nationalism, 193

Nazirite vow, 231f.

Negligence, punishment for, 93, 99,
102

Negro slavery, 91

Nehemiah, 237

Neighborliness, 142ff.

New Creation, 164

New Moon, 211, 212

New Year's Day, 193

Night, metaphorical meaning,
137

Nile, bloodied, 100n., 253

Nimrod, 41

Noah, 30, 31, 36, 44, 45, 51

Nostrils, God’s, 157f.

Nursing, 191f.

Obed, 258

Occult practices, 152f,

Office, Biblical view of, 212
office bearers, 266f., 271
officers (shoterim), 53f., 150

Onesimus, 135

Open pit, 1298,

Oppression/deliverance pattern, 31ff,

Ordinances, 46, 75, 83
structure of, 61ff.

Original sin, 127, 241

Orphans, 65£., 156ff.

Ox
goring, 90, 116, 1228f., 130
muzzled, 272
symbolic meaning of oxen, 266ff.
yoked, 272

Parachurch organizations, and the
tithe 219

Parents
attacking, 103f,
repudiating, 105i.

Passion, crimes of, 110

307

Passover
event, 38f,, 82f., 82n., 83n., 100n.,
10in., 243ff.
feast, 58, 165, 186iF., 259f.
Paul, 135, 162, 172n., 270
Peace offering, 62, 188
Pentateuch, plactorm for further
revelation, 17, 30
Pentecost
feast, 58, 189
New Testament, 49, 192
Perjury, 179
Persecution, 35f.
Peter, 81, 234
Pharaoh, 33f., 35, 37, 127, 184, 218, 264
fall of, 35n.
God's war with, 39ff.
law of God and, 394., 50, 75
Pharisaism, 8, 173f., 233ff., 276
Philemon, 133
Philistines, 32, 265n.
Phinchas, 86, 158n.
Pillars, and legs, 82n.
Pit, open, 129f.
Pleonasm, 96n., 106n.
Pledges, 159ff.
Poll tax, 225ff.
Pollution, 1374.
Poor persons, 155f., 268ff.
tithe and, 211 ef passim.
Pornography, 23, 23n.
Potiphar, 157
Prayer, house of, 231, 232
Pregnancy, 1134.
Priests
a nation of, 70, 71
of all believers, 8
Princes, 54
Prison, 136
Private property, 70n.
Prizefighting, 111
Property, 131ff.
familistic, 133
