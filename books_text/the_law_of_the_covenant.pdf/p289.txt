270 The Law of the Covenant

boam I of Israel made war against Abijah of Judah. Judah is
stated to have an army of 400,000 men, while Israel is stated to
have an army of 800,000, exactly double that of Judah. If we per-
mit the numbers to take on symbolic significance, then Judah’s is
seen as the relatively poorer army. Abijah makes a speech, in
which he expressly accuses Jeroboam of rebellion against his lord
(v, 6), and in which he expressly states that the Lord God is the
Head of the hosts of Judah (v. 12). Thereafter, Jeroboam attacks
Abijah, and the army of Judah cries to the Lord. God grants them
victory, and we are expressly told that 500,000 chosen men. of
Israel fell slain (v. 17), If we permit the numbers to speak sym-
bolically, then we can see this as a five-fold penalty for rebellion,
and for attempted theft. God had given Jeroboam the ten nor-
thern tribes (Israel), but Jeroboam was invading and attempting
to take over (steal) the land of Judah (v. 4 with Josh. 18:22). Since
this constituted rebellion against a superior, and an attempt to rob
by force, the required restitution was five-fold. The Godly four-
fold dominion of Judah (400,000) overcame the wicked, and ex-
acted five-fold (500,000) restitution.

(Possibly also we have an application of this in 2 Corinthians
11:24, where Paul says, “Five times I received from the Jews the
forty lashes minus one.” The natural way to take this is that on
five separate occasions, Paul was thus whipped. Possibly, how-
ever, Paul was seen as guilty of insurrection and of stealing
members of the synagogues for the Christian faith, and so possi-
bly he is referring to one occasion on which he was punished
five-fold, perhaps whipped on five consecutive days. I offer this as
a bare possibility only.)

Conclusion

If my explanation is correct, then we can readily apply this
law to modern society,!° The best explanation of Exodus 22:1 that

10. [am not totally positive that my interpretation is the correct one. I offer it
to the Church as the best I have heen able to do. There is, ta my knowledge, no
good study of Biblical numerical symbolism, and I have done my best with the
numbers four and five. Studies of Biblical symbolism in general are rare, and
