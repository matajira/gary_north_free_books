Appendix #—Salvation and Statism 244

this Statism.

What is Statism? Satan’s offer to mankind was that man
should be like God. Specifically, man would not have to learn
God’s ways and pass judgments in terms of God’s word, but man
would issue judgments out of himself, reshaping the world to fit
his own desires.? Thus, man’s sinfulness consists essentially of his
desire to exercise sovereignty over God’s universe —a direct sover-
eignty, answerable to no one, rather than a derivative dominion
in terns of God's law. When God rules over a man, He provides
him with his (external) Word, and influences hirn to obedience by
His Gnternal) Spirit. When one man iries to rule another man,
the situation is different. He provides him with an external word
of command, but he cannot reach within his neighbor to influence
him within, Thus, he must influence him externally, by force, by
threat of violence. It is the state which is the repository of force,
the threat of the sword, Sinful man, then, turns to the state to en-
force his attempted sovereignty.

The world is full of problems, which are the consequences of
sin, and sinful man would like to be rid of these problems, so that
he can enjoy the good life. The wicked know that some type of
salvation is needed. People need to be changed (especially those
Christians who refuse to go along with the wicked’s plan of salva-
tion). Again, sinful man can only rely on force, on the siate, to
effect this salvation. The wicked state is thus not only savereign
but also Messianic. Sinful man’s social order is state-centered, or
Statist. The Bible sets its face against Statism, from Babel to the
Beast.

Biblical social order is not state-centered but God-centered.
The solution to human ills is not government spending but Divine
grace. Protection from the enemy is not guaranteed by the state,
though it plays a part here, but by God. Society is not reformed
by state-directed education, but by the Gospel of God. The civil
government is not to serve (rule) as a savior.

This explains why we do not find a set of judicial laws in the

2. See Jordan, Trees and Thoms (forthcoming).
