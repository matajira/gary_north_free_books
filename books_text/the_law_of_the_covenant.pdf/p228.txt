Appendix C— Tithing: Financing Christian Reconstruction 209

Covenant. The Levitical tithe system is, however, preceded, under-
girded, and succeeded by the Melchizedekal tithe system (Heb. 7).

4. The Melchizedekal tithe system is permanently obligatory.
Abraham paid tithes (10%) to Melchizedek, and all the true sons
of Abraham (Rom. 4; Heb. 7) will also pay the tithe to the greater
Melchizedek, Jesus Christ. In return for the tithe, Melchizedek
gave Abraham bread and wine. Anyone who refuses to pay a
tenth to Christ should also be refused the bread and wine of the
Lord’s Supper (Gen. 14:18-20).

5, The Melchizedekal priestly order was connected te sonship
(Heb. 7:3), especially the privileges of the firstborn.? God very
meticulously superseded the Melchizedekal order with the Leviti-
cal order in Numbers 3. Thus, the Melchizedekal order always
underlay the Levitical order throughout the Mosaic period. The
Levitical tithe, then, is an extension and specification of the
Melchizedekal tithe.

6, The Melchizedekal order was typologically reasserted in the
Davidic Covenant (2 Sam. 7), which spoke of the king as a son.
Psalm 110 and the book of Hebrews must be understood in the
light of the Davidic Covenant. Again we see the Melchizedekal
order as the foundation for the Levitical, especially as the Davidic
kings supported and reformed the Levitical system from time to
time. Indeed, the plans for.the Temple and the building of the
Temple were not given to and accomplished by the Levitical
priests, but by the Davidic kings. (See 1 and 2 Chronicles.)

7. The fact that the Levitical tithe is built on the Melchizedekal
means that an examination of how the Levitical tithe functioned
in the Mosaic period can provide useful pointers as to how the
fully established Melchizedekal tithe should be used in the New
Covenant period.

The Levitical Tithe

8. Because of the restrictions on the cultural mandate, and

3. As a type of Christ, Melchizedek is seen as unique. Psalm 10:4 and
Hebrews 5:6 speak of the “order of Melchizedek.” This is our concern at present.
