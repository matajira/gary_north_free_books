Appendix F—~ Proleptic Passover 245

The same unhelpful approach is taken by Matthew Poole,
although Poole does mention in passing the correct approach.
Speaking of the statement “a bloody bridegroom,” he says, “Yet
some take these to be the form or solemn words used in circumci-
sion, Thou art a spouse, or a son of bloods, to me, i.e., made so to me
by the blood of circumcision. But it doth not appear that this was
the usual form. Nor was it likely that she, being a Midianitish,
not a Hebrew woman, and doing this suddenly, and in a rage,
should be so expert to know, and so punctual to use, the right
form of words, when she did not use a fit and decent carriage in
the action, as appears by her casting it as his feet.” Again, the no-
tion that Zipporah “threw” the foreskin is based on a mistransla-
tion, and the idea that she was ignorant of God’s ways, though a
daughter of Godly Jethro and a wife of 35 or so years to Godly
Moses, is not credible. Again, like Calvin, Poole takes no note of
the context.

There is little improvement in the remarks of Matthew Henry.
Like Poole, Henry mentions but dismisses the interpretation that
the phrase “bridegroom of blood” refers to the child’s coming into
covenant with God. He prefers to see Zipporah as acting in fury.
He also wrongly states that Moses was “unequally yoked with a
Midianite,” based on the clearly wrong assumption that Jethro
was 4 pagan.”

 

frequently have much more to say about archaeology and the customs of the An-
cient Near East than they have to say about the text. No conservative denies that
the events recorded really happened; the question is rather where we go to find
the context and meaning of the events as recorded by the Holy Spirit. Calvin here in-
vents an historical and an emotional (psychological) context, and explains the
text in terms of it, mstead of explaining the text in terms of its own literary
Biblical context. The modern “Biblical-theological” approach to exegesis is a
needed corrective to the Antiochene method. On the Nestarianism of Theodare
of Mopsuestia, see Rowan Greer, Theodore of Mopsuestia: Exegete and Theologian
(Westminster, England: The Faith Press, 1961); and Rousas J. Rushdoony, The
Foundations of Social Order; Studies in the Creeds and Councils of the Early Church
(Nutley, NJ: The Presbyterian & Reformed Pub, Co., 1968), pp. 98-111.

6, A Commentary on the Holy Bible (London: Banner of Trath Trast, 1962), eom-
ments ad toc.

7. Commentary on the Whole Bible (countless editions available), comments ad foc.
