Appendix G—Four and Five-Fold Restitution 263

Again, there are several problems with this view, and again
the first problem is that it is wholly without Biblical corrobora-
tion. Second, this canon is valid for any beast, including dog or
donkey: If the thief sells the animal, there will be additional costs
to proving the case, Third, this explanation does not account for
the difference between four-fold and five-fold restitution. Indeed,
it is easier both to steal and to dispose of a sheep than of an ox, so
that the cost of proof would be higher in the case of the sheep than
in the case of the ox; thus, we should expect four-fold for the ox
and five-fold for the sheep. Finally, though the cost of proof is a
valid consideration in a case such as this, it can easily be covered
by bringing an additional suit against the thief to recover any ex-
penses involved. The thief could also be sued for the owner’s loss
of time (Ex. 21:19).

I should like to suggest a different line of approach. We need
to see if the Bible itself gives us any clues to the meaning of these
provisions, and then test our hypothesis to see if it finds cor-
roboration in those passages which show the application of this
law (2 Sam. 12; Luke 19:8).

Four-Fold and Five-Fold

When Israel heard this law, they had a background of Divine
revelation consisting of the book of Genesis. What light, then,
does the book of Genesis shed on the meaning of the numbers four
and five? In Genesis 2:10, we find that the river of Eden split into
four streams, and watered the earth. This signified that the source
of life in the garden was extending itself to the four corners of the
earth (Is. U:12; Jer. 49:36; Ezk, 7:2; etc.). We can make an initial
suggestion that the number four represents comprehensive dominion.
Aman may have dominion over a very limited space, but within
the four corners of that space, he has comprehensive dominion.
Thus, Abraham paid four hundred shekels of silver for a plot of
land in Canaan, the only land he ever owned in the land of prom-
ise (Gen. 23:15f.). Thus, Esau came out to fight Jacob with four
hundred men (Gen, 32:6; 33:1), signifying his continuing claim to
the land of promise.

 

 
