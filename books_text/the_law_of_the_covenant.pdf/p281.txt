262 The Law of the Covenant

ical explanation). Another problem is that it seems economically
naive. In point of fact, the market price for any given sheep at any
given time will include its reproductive potential, and so also for
the ox. In terms of the training of the ox, its value will be propor-
tionately greater depending on how it has been trained. Double
restitution would be made in terms of the actual market value of
the ox, and this value would reflect its training. Thus, the market
price already takes into account both reproductive potential and
training, and double restitution would be adequate if that were all
that is in view. A third problem is that the donkey is also a trained
animal, but apparently double restitution suffices in the event it is
permanently removed.

Another explanation sometimes suggested is that the ox and
sheep signify the tools of a man’s trade. If a man stole a television
set from a painter, for instance, he would owe double restitution;
but if he stole that painter’s pickup truck with all of his equipment
in it, and wrecked the truck, he would owe five-fold restitution
because he had destroyed the tools of his trade. Stealing the
sources of a man’s livelihood is more serious, according to this
argument, than stealing a comparative luxury. One problem with
this interpretation is, again, that the Bible nowhere corroborates
it. Also, when speaking of what is essential to a man’s livelihood,
the Bible speaks of the “handmill or an upper millstone” (Dt.
24:6), not of sheep and oxen, Third, this explanation does not ac-
count for the difference between four-fold and five-fold restitu-
tion. If the pickup truck and tools are like the ox, what is like the
sheep?

A third suggested explanation is that it costs the owner more
effort to prove that his ox or sheep was stolen if the evidence has
been disposed of. If the thief has kept the beast alive, it can be iden-
tified. The cost of recovery is relatively low. On the other hand, if
the beast has been killed, eaten, and the remains buried, or if it has
been sold to foreigners, then it becomes difficult to prove the
suspected thief guilty. The owner may have to hire a detective
agency, and put time and money into proving his case. Thus,
multiple restitution covers the costs of proof in this situation.
