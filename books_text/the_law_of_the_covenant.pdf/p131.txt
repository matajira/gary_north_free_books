2 The Law of the Covenant

has. If the neighborhood bully catches your child on the way home
from school, and your child cannot escape by fleeing, your child
should poke a hole in him with a sharp pencil, or kick him in the
groin. If the bully’s parents will not restrain him, call the police.

If you or your child has been trained in self defense, of course,
you may be able to dispatch your assailant with a minimum of force.
Always realize, though, that the rman who attacks you, or your wife,
has forfeited all his rights to “fair” treatment. Women should be
prepared to gouge out the eyes of any man who attacks them.

In summary, the Bible teaches us to avoid all fighting, and to
suppress it. Only in the case of a direct threat to one’s person or
property, when an appeal to arbitration is not possible, is fighting
permissible (Ex. 22:2). The woman attacked in the city is ex-
pected to fight back and cry out (Dt. 22:24).

Finally, this law implies that flight is an alternative preferable
to fighting. If two men strive, and there are no witnesses, it would
be impossible to prove self defense if one man harmed another, If
aman attacks me, and | harm or kill him in self defense, I may be
charged with murder, for there are no witnesses. My best
recourse, then, is flight, if it is at all possible. This may not square
with modern notions of honor, but Christians are concerned first
with God’s law and honor, not with their own.

This law concerns fighting and its consequences. If a man
dies, the killer is treated as a murderer. What is not in view here is
attempted murder, a situation where it is clear that one party was
trying to kill the other, but was stopped from it. Biblical law treats
an attempted crime the same as if the crime had actually been
committed. This is discussed in my monograph, Sabbath Breaking
and the Death Penalty (Geneva Ministries, forthcoming).

If, Slave Beating

We have discussed this law in Chapter 5, The placement of the
law here is to modify the previous law. Obviously, the master will
have his slave healed, if he lives. Also, the law makes it clear that
murder is murder, even in the case of the slave, for the slave also
is the image of God (Gen. 9:5, 6). The only other situation is one
