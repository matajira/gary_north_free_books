Appendix C~ Tithing: Financing Christian Reconstruction 223

half of the $10,000 was a capital expense, and so $5,000 should be
added to overall capital expenses. This leaves profit at $50,000, so
that a tithe of $5,000 is owed to God before the business makes
any moves to expand.

63, What about taxes? Clearly a man owes God His tithe be-
fore he owes the state a tax. On the other hand, confiscatory taxa-
tion, more than 10% of income, can be viewed as a plague of
locusts or as the damage caused by an invading army. (See 1 Sam-
uel 8, where tyranny is expressed as a government which takes
10% or more in taxes, thus making itself a god.) Increase for the
year can only be calculated in terms of what is left after the locusts
have damaged the crop. In terms of this, it might be proper to
consider taxes as part of basic capital expenses, rent paid to the
invading army, and pay the tithe on what remains after taxes. I
suggest, however, that a man include in his capital expenses only
that amount of tax that goes over 10% of his taxable income. In
that way, his conscience can be clear, for he is ascribing to tyranny
only what tyranny takes in excess of what the state might properly
take.

64. All of this entails a certain amount of juggling. After all,
what the state considers taxable income will not be exactly what a
Christian might consider titheable income, so that 10% of tax is
only a rough way to do service to the principle outlined in para-
graph 63 above. Also, the state permits deductions from tax based
on tithing, up to a certain amount. Thus, precision in tithing is
almost certainly impossible. What God honors, however, is more
the intention to tithe than the actual amount, After all, God has
infinite resources. He can finance Christian reconstruction at any
time He chooses. In terms of that, the widow’s mite, faithfully
given, does more to honor God and bring about Christian recon-
struction than does a large tithe calculated by a niggardly busi-
nessman seeking to tithe as little as possible.

65, Finally, we should note that tithing is inescapable. Gad
will have his 10%, and either we will pay it over voluntarily, or it
will be extracted from us forcibly. Men who do not willingly tithe
a generous 10% will find that God does not prosper them. They
