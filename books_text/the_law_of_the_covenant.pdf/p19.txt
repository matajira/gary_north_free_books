XX The Law of the Covenant

pressly mandated by Scripture, if that is the only way to get the
job done (pp. 138ff. the quotation from Gary North). There is a
kind of common-sense flexibility here that many people do not
associate with iheonomy (although such flexibility has always
characterized the movement at its best: pp. 1384. do, after all,
come from North’s writings). Clearly, Jim is much more in-
terested in what Scripture says than in maintaining some stereo-
typical image of “the theonomist.” His distinctions provide oppor-
tunities for serious discussion between those who accept and those
who reject the theonomy label,

You can learn so much here — from theoretical insights on the
“multiple equity of the law” (pp. 18ff.) to practical advice on what
to do when your child breaks your neighbor’s china (pp. 143f.). I
found especially helpful the discussion of the “uses of the law” (pp.
24f., Appendix E), the correlation between the Book of the Cove-~
nant and the Decalogue (pp. 63ff.), the discussion of tithing (pp.
207f.), and the analysis of Israel’s Exodus from Egypt in terms of
the slavery laws (pp. 39ff.).

At some points I have problems with Jim’s discussion. His
distinction between “Old” and “New” covenants (pp. 55ff., 196ff.)
does not seem to me to do enough justice to Heb. 8. Also, I have
my doubts as to whether the Hebrew plconasm really furnishes us
with “two witnesses” (p. 96, note 5). Sometimes the fertility of
Jim’s mind still exceeds his self-discipline.

On. the whole, however, the book is a tremendous contribu-
tion. There are fresh insights on nearly every page. It raises the
discussion of biblical law to a new level of precision and cogency,
because it deals with the law in such detail. It is the most practical
piece of biblical theology I’ve seen in a long time. It has changed
my thinking on a number of matters. So I encourage you to read
it in gratitude to God and in anticipation of more from this gifted
young author,

Westminster Theological Seminary in California
March 16, 1984
