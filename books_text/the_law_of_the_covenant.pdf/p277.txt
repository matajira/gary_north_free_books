258 The Law of the Covenant

cision of the man does.*# The groom circumcises himself on the
wedding night, painfully, in order to provide legal covering for the
bride he loves, and as a token of their union.

Thus, one of the (many) meanings of circumcision was this:
Since Israel was a harlot, her Husband would give His blood for
her covering and as a sign of their union. The circumcision of
each male child provided a continuing reminder to Israel that she
deserved to be put to death for playing the harlot in her father's
house, and that a substitutionary atonement was the only way she
would find judicial righteousness. Also, circumcision provided
not just a reminder, but an actual covering until the crucifixion of
her Lord would provide the final circumcision, and her definitive
justification.

Zipporah, we are told, “cut off her son's foreskin” (Ex. 4:25).
Why does the text not say, “Moses’ son’s foreskin”? Doubtless it is
because of the bride theology of this passage. It is Israel-Eve as the
Mother of the Seed who must be saved here. It is her son whose
circumcision will deliver her from death. The conflation of hus-
band and son seems to be a mixed metaphor, but while mixed
metaphors are not permitted in college English classes, the Bible
abounds in them. At this point, let me call attention ta the book of
Ruth, in which Boaz is clearly the kinsman-redeemer. Yet, in
Ruth 4:14, 15, Obed is called Naomi’s kinsman-redeemer. The son
is conflated with the husband. **

43, Thus, Adam and Eve felt their shame especially in their private parts, and
made aprons, Illicit sexual activity is called “uncovering nakedness” in Leviticus
18. Discharges from the genitals, life Howing away, cause uncleanness (Lev, 15).
R. J. Rushdoony has called attention to the symbolic parallel between the hnman
body and the garden, with fountain in the center: “To understand this meaning
we must remember that a fountain is a source, a place on earth where living
water comes forth. There is an obvious analogy to the woman’s ovulation,” The
Institutes of Biblical Law (Phillipsburg, NJ: The Craig Press, 1973), p. 429.

44, This is in terms of the Biblical theology of succession. Repeatedly in
Genesis it is seen as important that each bride be succeeded by another true
bride, and each seed by another true seed. Thus, it is immediately upon the
death of Sarah that Abraham moves to pravide a bride for Isaac (Gen. 23, 24),
and when Rebekah arrives, Isaac takes her into Sarah’s tent (24:67). Similarly,
when Jacob is sent to Laban to get a righteous wife (replacement bride), this is
