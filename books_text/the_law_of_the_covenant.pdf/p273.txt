254 The Law of the Covenant

This is exactly parallel to what happened to Moses’ family at
the lodging place. The blood of the Passover smeared on the door-
posts corresponds to the blood of circumcision smeared on Ger-
shom’s legs. It had the same effect. The lodging place became a
place of refuge, just as the Hebrew homes in Egypt did later on.

Kosmala has rightly pointed out, as we noted above, that in
both cases the blood was made visible to God, and that this is the
reason God ceased attacking the household. Possibly the imagery
is more specific. We find in Canticles 5:15 that the legs of a human
figure are compared to pillars, or doorposts. The human body is
compared to a house frequently in Scripture, as in Ecclesiastes 12,
and there is nothing fabulous in seeing a comparison between legs
and doorposts.*5 Indeed, even apart from these other passages the
parallel between the circumcision of Gershom and the Passover
all but explicitly states the analogy.36

We may ask why God attacked Moses’ family at this point,
rather than simply waiting for the Passover. There are two possi-
ble answers. First, Moses had killed a man (Ex. 2:12), and possi-
bly it is his blood in particular which called up the Avenger. Possi-
bly; but Moses was a civil magistrate in Egypt, being a member of

 

and firstborn were the priests, and so the home was the place of refuge.

Also, just as a man under threat of death could leave the city of refuge only
when the high priest had died, so Israel under the threat of death could leave
Egypt only on the basis of che death of the Passover lambs.

33, On the comparison in general, see M. G. Kline, Images of the Spirit (Grand
Rapids: Baker, 1980).

36. We shall note in footnote 37 below that doorposts are a place of birth, and
the comparison between coming out of a doorway and coming out of the womb
between the legs is obvious. Western rationalist man is frequently blind to the
visual images of Scripture, and to visual analogies which Scripture expects men
to see without difficulty. There are architectural parallels among the forms of the
Garden of Eden, the Tabernacle, the Temple, the New Jerusalem, the Glory
Gloud, the Temple of Ezekiel, and the human body, The Bible expects us to note
these, and to draw proper inferences from them. To limit God’s revelation to the
kinds of linear logical structures favored by modern rationalist man is to make
oneself deaf to the Word of God. It is not necessary, in other words, for God to
say explicitly somewhere, “The East Gate of the Garden, guarded by Cherubim,
is parallel to the three Eastern Curtains of the Tabernacle, embroidered with
Cherubim.” Nor is it necessary for God to say, “Now, human legs are parallel to
the doarposts of a house.” But, in fact, see Canticles 5:15.
