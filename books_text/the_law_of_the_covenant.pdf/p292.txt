Appendix H1—On Boiling a Kid in Its Mother's Milk 273

whether or not the contexts in which the law occurs tend to corrobor-
ate our hypothesis. Third, we want to look at redemptive historical ap-
plications of the hypothesis, to see whether or not it fulfills our rea-
sonable expectations. That is, does our inierpretation of this law
enable us better to understand the work of Jesus Christ? Fourth,
we want to look at practical applications of the hypothesis, because the
Bible is designed to be relevant to men in their historical context. If
our interpretation is utterly useless, it is probably also utterly
worthless. In other words: Cari this doctrine be preached?

Now, if everything stacks up, then we can reasonably offer our
hypothesis to the Christian world, as a viable interpretive option,
and ask for criticism and interaction.

First, then, to the particulars of the law. We look to see what
the Bible says about kids. Our hypothesis assumes that they sig-
nify children. There turns out to be relatively little information on
this, and no particular verse which says, “Kids symbolize chil-
dren,” On the other hand, we are not in a position to dictate to
God how He must disclose revelation to us. The Bible does not
have to fit our preconceived (and rationulistic) canons of argu-
ment and proof. The Bible may teach that kids signify children by
a variety of “indirect indications” rather than by “explicit state-
ment.” We have to be open to this. We do find that a number of
passages suggest a connection between kids and children. We also
find that kids were used for food rather often.

Now we turn to mother’s milk. We find that the weaning of
Isaac is an important occasion (Gen. 21:88). At this time, Isaac
leaves the protection of his mother, and thus is exposed to conflict
with Ishmael, Thus, Sarah’s last act is to protect her child. We
also find in a more extensive way that a man is not said to leave
his father and mother until he takes a wife (Gen. 2:24). Thus,
there is a sense in which any unmarried man is “under age.” This
sense is heightened in the case of a child not yet weaned. At any
rate, we find that Jacob is still being protected by his mother,
Rebekah, when Isaac wishes to steal the covenant from him. Ad-
ditionally, we find in the New Testament that the early history of
the Church, before the destruction of Jerusalem, is likened to the
