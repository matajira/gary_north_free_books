300

Asylum (see Cities of refuge)
Atonement
atonement money, 126n., 147n.,
226.
day of, 102
sado-masochism and, 95
substitutionary, 37f.
Authority, 104
Avenger af Bload, 97ff., 252, 2534.
God as Avenger, 94, 146 (see also
Angel of Death)

Baalam, 157f.
Babel, 41, 241
Babylon, 266
Balcony, requirement of, 129
Banking, fractional reserve, 160
Bankruptcy, 92
Baptism, 78n,, 260n.
Barrenness, 194
Bastards, 150
Bathshcba, 268
Beating, as punishment, 73
Benjamin, 78n., 264
Bestiality, 153ff., 154n.
Betrothal, 148, 149f.
Bible
church and, 8f.
devotional reading of, 9
professional use of, 9
purposes of, 1ff.
redemptive-historical approach to,
gf,
Bipolarity
clean/defiled, 14n., 15n. {see also
Clean/unclean)
Israel/nations, 32
special/general, l4n., 48n.
Birds, laws protecting, 2
Birth
doorways (thresholds) and, 82n,
symbolic, 256
Bishop, 54

The Law of the Covenant

Blasphemy, 94, 162
Blood
calls for vengeance, 37f.
issue of, 56
pollutes land, 991,
of wedding night, 257f.
Blood feuding, 100f., 103
Bloodshed, 37
Boaz, 258
Boiling kid in mother’s milk, -190ff.,
2728.
Book of the Covenant, 46, 75, 83
structure of, 61,
Booths, Feast of (see Festivals)
Borrowing, 142ff,
Breaking in (theft), (36f.
Bribes, 172¢f.
Bride (of Christ), 256f., 258n., 259n.,
260n., 274
Bride price, 84f.
Bridegroom of blood, 2434,
Burning of property, 120, 121, 138
Bystanders, laws protecting, 113ff., 117

Cain, 31, 41, 94
Calch, 147
Calendar, 184f., 193
Calf, golden, 59
Canaan (land), 55, 59, 66, 69, 83n.,
99n,, 100n., 186
Canaanites, 265
Cannibalism, 274, 276
Capital
tithed?, 221n.
taxed, 235, 238
Carefulness, doctrine of, 102
Castration, and circumcision, 781.
Census, 2264.
Chedorlaomer, 265
Children
Aghts between, HL.
not punished for parents’ sins, 125ff.
of slaves, 77
