INDEX OF PERSONS CITED OR REFERENCED
(Not including Biblical personages; see Subject Index)

Aalders, G. Ch., vii, 30

Albright, W. F, 116, 18

Alexander, Ralph H., 266

Allis, Oswald T., vii

Ansiay, Martin, 32, 44

Augustine of Hippo, 181

Babnsen, Greg L., 6, 9, 20, 28, 126

Brennan, William, 276

Brichto, Herbert G., 105

Bush, George, 249f,

Calvin, John, 181, 244

Cassuto, Umberto, 63, 70 # passim,
105, 136, 248

Chadwick, G. A., 246

Childs, Brevard, 163, 247, 248f.

Chilton, David, 192

Cole, R. Alan, 248

Courville, Donovan A., 43

Craigie, Peter C., 53

Dabney, Robert L., 91, 11

Danube, David, 33, 34, 39, 44, 113,
117, 120, 125

De Graal, S. G., 246

de Vaux, Roland, 146, 147, 130, 24

Delitzsch, Franz, 230, 246

Dooyeweerd, Herman, 213

Edersheim, Alived, 233, 246

Ezra, Ibn, see Ibn Ezra

Fairfax, Olga, 276

Fensham, F. Charles, 110

Finkelstein, J. J., 123

Frame, John M.., viii, xvit, 5, 67, 102

Gafin, Richard B., Jr., 80, 181

Geikie, Cunningham, 247

Gernser, Berend, 72

Gesenius, 38

Gill, John, 246f.

Gispen, W. H., 152, 170, 246

Greer, Rowan, 245

Gregory of Nyssa, 243f.

Hamilton, Victor P., 181

Harrison, Roland K., vii

Hartley, John B., 240

Henry, Matthew, 245

Hodge, Charles, itt

Hoeksema, Homer C., 246

Hyatt, J. P., 247

Ibn Ezra, 160, 249

Ingram, T. Robert, 132

Jones, Hywel R., 247

Jordan, James B., 10, 20, 219

Kaiser, Walter C., 63 ef passim, 113,
199 et passim

Kaufman, Stephen A., 64, 199

Keil, G. F, 230, 246

Kline, Meredith G., 22, 59, 61, 254

Knight, G. A. F., 247

Knox, John, 181

Kosmala, Hans, 247, 250f.

Krabbendam, Hendrick, 199

Leibowitz, Nehama, 131, 157, 160, 173

Loeb, Meir, ben Yechiel Michael, 157

281
