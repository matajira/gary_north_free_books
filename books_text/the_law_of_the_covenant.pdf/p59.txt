40 The Law of the Covenant

God had prepared Moses to be His intermediary with
Pharaoh through eighty years of education and experience, first in
the court of Pharaoh, and then as son-in-law of Jethro, priest of
Midian (Ex. 2:21). As a Midianite, Jethro was a descendent of
Abraham (Gen. 25:2). Although not all the tribes of Midian re-
tained the true faith (Gen. 25:4; Num. 22:4; 25:6), Jethro clearly
was a worshipper of the God of Abraham (Ex. 18:12). Jethro was a
priest somewhat like that of Melchizedek, that is to say, he was a
priest-king, firstborn of the ruling house (Heb. 7:1; Num. 3:12;
Heb. 1:6). Jethro was able to teach Moses about worship, and also
about how to rule (Ex, 18:13-26). Moses doubtless had many occa-
sions to observe Jethro sitting in judgment, and to learn from it,1”

The issues between God and Pharaoh were these: 1) Who is
God? 2) Are the Israelites properly Pharaoh’s slaves? The Lorp
went to war with the gods of Egypt to settle the first question. The
gods of Egypt were not able to protect Egypt from the nine
plagues, but it was in the death of Egypt's firstborn that the
Lorv’s victory was particularly won (Ex. 12:12; Num. 33:4). The
firstborn were the heirs of birthright, blessing, rule, and
priesthood —their death was the death of Egypt.

As regards the second ‘issue between God and Pharaoh—
Were the Israelites properly Pharaoh’s slaves? —we need to note
that culture is an extension of religion, and Egyptian culture was no
exception to this rule. Its statist organization shows it to have

17, It is unclear how the church and state functions were carried out before
Mt. Sinai among God’s people. It seems that the patriarch of the clan (e.g.,
Abraham, or Jethro) served both as priest and as supreme judge. By the time of
the Exodus, there’ were thirteen tribal republics in Jerael, each with elders and
princes, as we see from the book of Numbers. This matter receives further ex-
ploration in Chapter 3 of this study.

Also, it should be noted here that the priesthood of Jethro was not like that of
Meichizedek, in that it was inherited. The priestly functions of such men as Jethro
and Abraham were typical of the sacrifice of Jesus Christ, but their persons as
priests were not typical. In conzrast to this, both Melchizedek and the Levitical
priesthood were types of the person of Christ. Perhaps another way of geiting at
the distinction would be to say that Jethro was not a type of Christ, though the
sacrifices he offered were typical; while both Melchizedek and his sacrifices
typified Christ.
