246 The Law of the Covenant

There is no need to rehearse in detail the comments of later
expositors who make basically the sarne assumptions in inter-
pretation. These include G. A. Chadwick,® George Rawlinson,
Alfred Edersheim,!° A. W. Pink,!! James G. Murphy,'? Keil &
Delitzsch,"? S. G. De Graaf,!+ W. H. Gispen,'5 and Homer C.
Hoeksema.1® This is an impressive array, but it must be born in
mind that this passage had not been subjected to careful scrutiny
in the way that, say, Romans chapter 5 has been. These ex-
positors basically rehearse the opinions handed to them by their
forefathers, and that interpretation is every bit as flawed at the
end of many repetitions as it was when first dreamed up.

There have been, however, expositors who dealt more carefully
with the text. John Gill, for instance, notes that Jethro and Zip-
porah, descendents of Abraham, would have known about cir-
cumcision, and thus cannot be blamed for Moses’ failure to cir-
cusncise his son. Gill holds that Zipporah touched the foreskin of
her son to Moses, and called him a “bloody bridegroom,” in the
joy of receiving him back from the dead, her husband anew. He
also mentions the possibility that Zipporah was addressing her
son with this phrase, which would be a way of congratulating him

8, In The Expositor’s Bible (Grand Rapids: Eerdmans, 1940; originally pub-
lished in the late 19th century), comm, ad loc.

9, In Ellicott’s Commentary on the Whole Bible, ad lac.

10. Bible History (Grand Rapids: Eerdmans, [1876] 1972) 1:57£.

11. Gleanings in Exodus (Chicago: Moady Press, my edition dated 1971, but
originally published in the carly 20th century), p. 40. Sloppy publication data
plagues the evangelical publishing world.

12, Commentary on the Book of Exodus (Minneapolis: Klock & Klock, [1866]
1979), comm. ad foc.

13, Old Testament Commentaries (several editions available), comments ad loc.
K&D make one improvement, in speculating that Zipporah called Moses (n.b.)
a “bloody bridegroom” because in this act she got him back from the dead as a
new husband, This is still erroneous, however, Zipporah was addressing her son.

14, Promise and Deliverance, trans. by Evan and Elizabeth Runner (St.
Catharines, Ontario: Paideia Press, 1977) 1:262f,

15. Exodus, trans. by Ed van der Maas (Grand Rapids: Zondervan, 1982,
original Dutch version early-mid 20th century), comments ad /oe.

16. Class Syllabus: The Bondage and Exadus (Grandville, MI: Theulogical
School of the Protestant Reformed Churches, 1975), p. 51.
