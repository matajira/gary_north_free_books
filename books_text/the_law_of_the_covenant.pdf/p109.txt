90 The Law of the Covenant

which the rod is to be applied is the buttocks, which is well padded,
Some masters might exceed this common-sense restriction, and if
they do any irreparable damage to the slave, he is to go free on
that account. Exodus 21:26, 27 specify the loss of eye or tooth, but
any permanent damage would be equivalent.

If the slave is beaten to death, the master is to be “punished”
(Ex, 21:20).2? The punishment is spelled out in Leviticus 24:17, 22
as death. Murder is murder. If, however, the slave lingers for a
day or two before dying, the law assumes that the master did not
intend to kill him, and the loss of the financial benefit of the slaye
is regarded as sufficient punishment (Ex. 21:21).

A notorious ox which gores a free person to death brings death
to its owner (Ex. 21:29), though a pecuniary compensation is pos-
sible in this case (vv. 30f.). In the case of the slain slave, however,
the owner of the ox is not executed, but merely gives 30 shekels to
the slave's owner.

To be a slave was to run something of a risk: the risk of being
beaten to death, of losing an eye or a tooth, of being exposed to
goring oxen. Are these laws unjust? Obviously not, being God's
laws. To understand this situation it is only necessary to keep in
mind that slavery ts a remedy for sin, and the goal of slavery ts its own self-
elimination. Lf slavery were a socialistic dream paradise, many peo-
ple would be attracted to it as an escape from responsible living.
The condition of slavery is made sufficiently hard that men will be
discouraged from entering it, and encouraged to seek to earn their
freedom.

Practical Observations

Slavery is still very much with us in the last years of the 20th
century. The most notable example is communism, which has en-
slaved vast portions of the world. In the United States, large
groups of people have approached the national government in re-

22, Shalom Paul points out that this stipulation is absolutely unique in the
Ancient Near East legal material. This is also true of the Exodus 21:26f. protec-
on of the slave against permanent bodily injury. Cf. Shalom Paul. Studies in the
Book of the Covenant (Leiden: Brill, 1970), pp. 69, 78.
