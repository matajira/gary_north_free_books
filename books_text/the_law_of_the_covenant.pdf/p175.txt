156 The Law of the Covenant

26-27. If for any reason you take as a pledge the garment of
your fellow, before the setting of the sun you shail return it to him;
(27,) for that is his only covering; it is his mantle for his bare body.
In what else can he sleep? And it shall come to pass when he cries
to Me, then I will hearken, for I am compassionate.

I. The Foreigner

There is a thematic transition from v. 20 to v, 21, in that
though foreign customs were outlawed, no oppression was to be
visited upon foreign people. Indeed, the kindness shown to the
stranger in the land was designed to win him to the true faith,
Also, such persons, separated from their families and the strength
of their clans, were in a very weak and vulnerable position. God
here announces that anyone living in His land is under His pro-
tection.

At the time this law was given, the sojourners were the mixed
raultitude who came out from Egypt with the Israelites. Later on
such strangers were traders from other lands who settled in Israel
to sell their wares.!3 Also, in highly decentralized times such as
those of the Judges, a fellow Israelite would be in the position of a
stranger if he were laboring outside his own tribe.

This law is repeated in fuller form in 23:9, in the context of the
ninth commandment. We shall have more to say about it in
Chapter 9. Note the alternation of singular and plural here’ The
oppressor may be an individual, but the whole community has an
interest in preventing this individual from continuing in his op-
pression,

Il. The Helpless

Widows and orphans (or better, fatherless children) were fre-
quently in a bad position in ancient cultures. If the woman did
not have family to care for her, and no /evir willing to marry her
(Dt. 25:5-10; Ruth 3, 4), her mohar would not carry her very long

13, They would have had to-setile in the towns, since the land was divided
among the sons of Israel, and reverted to its original owners in the fiftieth year
(Ley, 25). Thus, no outsider could ever settle in the land of Israel.
