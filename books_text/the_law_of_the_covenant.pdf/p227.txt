208 The Law of the Covenant

and Rushdoony’s study. With these things in mind, however, I
can recommend the examination of their study, since it does con-
tain much of value.

The problem in the Church at large is due to the evil influence
of “grace giving.” The notion of grace giving places men in bond-
age, for they never know when they have given enough. It also
dishonors God in that it encourages men to give as they please in-
stead of as He has ordered. Also, in point of fact, men seldom give
anywhere near ten percent of their net business income to God, so
that grace giving usually means robbing God (Mal. 3:8-12). Tith-
ing liberates men because it tells them exactly how much God re-
quires, and leaves them free to use the remainder in dominion
tasks.

T have set this discussion out in a series of numbered proposi-
tions. This is because it makes the various points easier for the
reader to isolate, and because it condenses the essay by elimina-
ting transitional sentences and paragraphs. We shall first of all
consider the nature and rules concerning Biblical tithing, and
then make some practical observations on how these might be irn-
plemented in our day.

The Melchizedekal Tithe

1. The Old Covenant was a provisional administration of
grace and law, while in the New Covenant the kingdom of God
and the law of God are established definitively (Rom. 3:31}, The
Cultural Mandate was restricted under the Old Covenant (Gal.
4:8), but fully republished in the New. The restrictive nature of
the Old Covenant was due to the fact that the Spirit was not yet
given, because Jesus was not yet glorified, and thus power for do-
minion was limited (John 7:38, 39).

2, Part of these restrictions was a system of laws which kept
the people closely tied to an agricultural economy. The Old Cove-
nant laws of tithing are couched in this framework, and they can-
not directly be applied to all New Covenant situations.

3. Moreover, the Levitical tithe system was intimately tied to
the sacrificial system and the centralized sanctuary of the Old
