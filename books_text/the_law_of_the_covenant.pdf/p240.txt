Appendix C—Tithing: Financing Christian Reconstruction 221

America, the churches contributed part of the tithe to support the
American Tract Society, the American Bible Society, and various
other tithe-agencies, such as those dedicated to missions among
immigrants. As the churches became more institutional and less
evangelical, local churches were expected to give only to denom-
inational benevolences. With the splitting of the traditional and
now apostate churches in the early years of the twentieth century,
the fundamentalist groups frequently returned to the practice of
supporting “parachurch” tithe agencies. Thus, God’s general prin-
ciples have been applied in varying: ways due to circumstances.

How to Tithe

57. Since all life and strength comes from God, we owe a tithe
on whatever our hands produce. The tithe is a return to Him for
the strength and capital He has given us. The tithe is paid on the
increase, what we make with what we have been given. Those
who are paid wages and salaries have little problem calculating
10% of their gross income and paying it to God.

58. The laws of tithing are phrased in terms of a man’s
business. Thus, if a man has a herd of sheep, he tithes on all the
newborn of the year, not just on the sheep he takes out of the field
to eat (Leviticus 27:30-33). Thus, a businessman must tithe not
simply on what he removes from his business for his own salary,
but on what the business itself produces.

59. A man is to tithe on his increase. The flock as it exists at
the beginning of the year has already been tithed on,'! The tithe is
on the newborn, They are not bred before they are tithed; just so,
money is not to be used before it is tithed on. I have known men
who sought to increase their money by investing it, before tithing
on it. This is clearly a violation of principle.

60. How about housing and electricity, and other expenses?
God provided these things for Israel in guaranteeing each man a

ii. In other words, God does not tithe on capital. He grants it by creation. He
demands a tithe on the return of capital, on the increase. Application: no prop-
erty taxes in a Biblical law system.

 
