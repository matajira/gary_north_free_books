22 The Law of the Covenant

Second, there is the command to love our fellow man in the
same way as we love ourselves (Lev. 19:18, Matt, 22:39). This
commandment divides the Greatest Commandment into two
parts: our duty to God and our duty to men. We should notice
that these two Great Commandments are not found in any special
place in the Bible, but are placed among the “small” particular
laws of Leviticus and Deuteronomy.

Third, there are the Ten Commandments. The Ten Gom-
mandments break the Greatest Commandment into ten parts.
Each of the Ten Commandments relates to God, and each relates
to our fellow men, but some relate more specifically to God and
others relate more specifically to man. The Ten Commandments
are not to be divided up into two tables.%* The two tables of the
law refer most likely to two separate copies of the entire Ten Com-
mandments. Such treaties were common in the ancient Near
East. One copy was for the ernperor, kept in the house of his god,
and one copy was for the vassal, kept in the house of his god. In
this case, the Lord is the Emperor, and so one copy went in His
house, in the Ark of the covenant in the tabernacle. The vassal was
Israel, and a copy went in the house of their God; thus, the second
copy also went into the Ark of the covenant.*3 This is why it was
called the Ark of the covenant.

Fourth, there are the case Jaws. The case laws of the Old and
New Testaments break the Greatest Commandment into many
parts. As we have seen, any given case law may be related to more

32. “So He declared to you His covenant which He commanded you to per-
form, that is, che ten words; and He wrote thern on two tablets of stone” (Dt.
4:18).

33, This viewpoint is skillfully argued for in Meredith G. Kline, The Structure
of Biblical Auihority (Grand Rapids: William B. Eerdmans Publ. Co,, 1975 [revised
edition), pp. 1134. The two tablets of the ten words also form a testimony of two
witnesses (Dt, 19:15; Rev. 11:3; etc,). Stones were used as symbolic witnesses by
God, as we see in Joshua 24:26, 27 (and see the curse fulfilled in Judges 9:64).
‘There were also two witnesses against Israel’s sin which were outside the Ark:
The completed books of Moses were placed next to the Ark in the Holy of Holies
(Dt, 31:26), and the creation was called as second witness as it heard the Song of
Moses (Dt, 31:28-32:47). On the disposition of the two tablets of the ten words
inside the Ark, see Exodus 25:16 and 1 Kings 8:9.

 
