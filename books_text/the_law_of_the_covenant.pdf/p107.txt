88 The Law of the Covenant

cies of sinful men. Ideally, the wicked are forced by the righteous to
work whether they want to or not; indeed, this is set forth as a
future blessing for God’s people in Isaiah 14:2 and 61:5.

Second, it is a blessing im that i trains men to work, and it does so in
the best passible environment, that of the family or household. In that the
slave is attached to the household, is not paid for his labor, and is
beaten for disobedience, the slave is really an adult child, and is
receiving in his adult years the same kind of formative education
that children receive. The Bible contemplates that there will come
an end of this pedagogy, at least for the faithful converted slave,
after which he takes his place as a late-blooming but now mature
citizen,

Third, it is a blessing in that if places sinners and unbelievers in the
best possible environment for evangelization: the Christian home.

Thus, household slavery is a heading institution. Man’s rela-
tionship to God has been distorted by the rebellion of man.
Household slavery restores order by forcing the unbeliever under
the rule of God, in the persons of the Godly; and places him in the
way of the gospel. Man’s relationship to the cosmos has been
perverted by sin. Household slavery restores sinners to a right
relation to the cosmos, by forcing them to work, and by directing
their labors in a proper, “Jerusalem” direction. Man’s relationship
to his fellowman has been warped by mian’s fall. Household
slavery restores order by breaking down statism, and by placing
natural subjects under their proper rulers. Slavery, then, is a
bypreduct of the rebellion of man, but in the proper form and ad-
ministered by covenantally faithful people, it is a means for
restraining and even rolling back the effects of the Fall and of the
curse, by “common grace” discipline and by “special grace”
evangelization.

In these respects it is like the institutions of divorce and
disinheritance. Had there been no rebellion of man, there would
have been no occasion for divorce or for disinheritance. Because
of the hardness of man’s heart, however, there come times when a
spouse or child is so far gone into sin and rebellion that the only
remedy is that he or she be excised, cut off from the household.
