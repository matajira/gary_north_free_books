Appendix G
FOUR AND FIVE-FOLD RESTITUTION

Exodus 22:1, When a man steals an ox or a sheep and butchers
it or sell: it, five oxen must he make whole for the ox, and four
members of the flock for the sheep.

The difficulty in interpreting this law resides in the fact that
the immediate context gives absolutely no indication of what the
governing principle is, and unless we can came up with a govern-
ing principle, we cannot make any application of this law to any
situation other than the particular one addressed in the very
wording of the law. Why multiple restitution for these clean
animals, but not for unclean (an inference we draw from the fact
that the donkey is added in verse 4, but is not present in this
verse)? Why four-fold for the sheep, and five-fold for the ox?
Since the distinction between clean and unclean is gone in the
New Covenant, into which category do we place swine? The im-
mediate context is silent. Thus, expositors have been forced either
to set aside the question of possible implications this text might
have, or else to speculate (helpfully or unhelpfully) concerning
what its implications might be.

The most common explanation is that sheep reproduce them-
selves, and when a sheep is permanently removed from the flock,
this removes all its potential posterity as well. Similarly, the ox
reproduces itself, and additionally is a trained work animal; thus
the permanent loss of the ox entails the loss of much time and
Jabor invested in training it. One problem with this view is that
there is no corroboration for it anywhere in Scripture, so that it is
pure speculation (as opposed to a Biblically-grounded hypothet-

261
