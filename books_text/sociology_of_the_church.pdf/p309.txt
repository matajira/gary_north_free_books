Appendix A
BIBLICAL TERMINOLOGY FOR THE CHURCH

The most general term in Scripture for those who belong to
God is “the people of God.” The term “people” does not contem-
plate a mere mass of particular individuals, but in terms of the
equal ultimacy of the one and the many, the word may have either
a collective or a distributive use. In Hebrew, it may take a singu-
lar or a plural verb (e.g., Is. 9:13 + 9:2).

The people of God are constituted such by Him (2 Cor. 6:16).
The first actual reference to a people as God’s own peculiar people
is in Exodus 3:7, but this reference assumes that the people were
already in existence. Depending on context, “the people of God” is
not necessarily a synonym for “Israel,” as in Psalm 47:9: “The
princes of the peoples (pl.) have assembled themselves—the peo-
ple of the God of Abraham. , . .” Thus, Israel may be cut out of
the people of God (Hos. 1:9; Rom. 1f:1-32) and other ethnic
groups included (Rom. 9:25f.).

The differentiating mark of God’s people is that they belong to
Him, live in His presence, and obey His laws. The sin of man
brought expulsion from Eden, from the “good life” of the cove-
nant, and brought the curse of death. Salvation restores man to
Eden, to the Presence of God and the outflow therefrom, restores
him to the “good life,” and delivers him from death, Thus the
differentiating mark of the people of God is life, ethical and vital.
It is that range of things denoted by the Biblical concept of bless-
ing. The people of God live in His blessing, while the enemies of
God live under His curse.

295
