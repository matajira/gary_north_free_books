God's Hospitality and Holistic Evangelism 245

through blood. The marriage bond is reestablished through the
blood and pain of the wedding night. The parent-child bond is re-
established through the bloodied birth of the infant. The bond of
adoption is permanently established through the bloody boring of
the servant’s ear at the master’s doorpost (Ex. 21:6). The God-
man bond is reestablished through the blood of the sacrifice and of
circumcision.

These are all threshold experiences, in which a person passes
through a door into a house. Because of sin, the door must be
bloodied, so that the passage through the threshold is a passage
through death to resurrection life. Thus, the door of the human
body is bloodied in marriage and in childbirth, and the door of the
house is bloodied when the slave is adopted into the family (from
then on being known as a “homeborn’ slave).’? Once established
through blood, the covenant is renewed through the evening meal
— those of the same household eating the same food together. This
is simply an extension into common life of what we find in the
church as well: the threshold experience of entering the land was
the passage through the Jordan river, and the daily food was the
milk and honey of the land. The threshold experience of entering
the special priestly covenant with God was circumcision, and the
covenant renewal was the Passover. In the New Covenant, the
threshold experience of entering the house is the cleansing of bap-
tism, and the covenant renewal is the Lord’s Supper.

Thus, covenant bonding is a resurrection phenomenon, and covenant
life is in the sphere of the resurrection. To the extent that the un-
believer experiences covenant bonding in his marriage, family,
business, etc., to that extent he is borrowing capital from the res-
urrection, crumbs that fall from the Lord’s Table. This is common
grace, the goodness of God that leads to repentance. If he will not
improve on these graces, he will lose all covenant life, and be iso-
lated apart from all community by himself in hell forever.

Covenant life, resurrection life, then, entails @ social bond, a
bond between God and man and between man and man, Thus,

13. On this whole matter, see my book The Law of the Covenant, (Tyler, TX:
Institute for Christian Economics, 1984), chapter 5.
