Reconstructing the Church 33

Some language, and this is the point, is primarily performative.
Such speech actually performs an action. Here is an example: “I
now pronounce you man and wife.” Here is another example: “I
baptize you in the Name of the Father, and of the Son, and of the
Holy Spirit.”

Ritual is not “mere ceremony,” though it can become that. Rit-
ual worship is supposed to be performative. We as a congregation
perform the following acts in worship: We confess sin. We accept
forgiveness. We offer ourselves as living sacrifices. We take vows.
We give gifts. We cat. We say “amen,” which is a covenant oath
implying “May I be ripped in half and devoured by the birds and
beasts if I do not confirm these words to do them.” The officiant
also performs certain acts in worship: He baptizes. He declares us
forgiven. He gives us Christ in bread and wine.

The Lord’s Supper

The fourth perspective on ceremony is that of the action of the
Lord’s Supper, As we noted above, the inauguration of the Lord’s
Supper preceded its interpretation. Jesus did not at that point give
an explanation of it. He just said to do it. A truly Christian philos-
ophy must take this into account. Knowing and doing are equally
important. Each is the context for the other, and each is under
submission to the Word.

A faith-commitment to the Word comes before both under-
standing and obedience. It is sometimes naively thought that the
Word is addressed first of all to the understanding, but a moment’s
reflection will show that this is not so. Frequently in Scripture God
tells people to do something without explaining in context what it
means. For instance, in Leviticus 12 there are a number of rules for
the separation of women after childbirth. In context, however, no
explanation is given for these rules. Examples could be multiplied,
and of course, right before us is the example of the Lord’s Supper.

Apart from faith, obedience is nothing but “works of the Law,”
and stands condemned. Apart from faith, knowledge is nothing
but vain imaginings. We must have faithful works, and faithful
understandings. Each leads to and reinforces the other. Obedi-
