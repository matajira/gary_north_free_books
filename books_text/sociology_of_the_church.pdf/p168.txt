154 The Sociology of the Church

ten years ago, I realize. And my understanding of God and of His
ways, of what it means to be a Christian, had better change too.
My faith needs to deepen and broaden. Once again, I necd to give
ail to Him, because my understanding of “all” has expanded.

This means that the kind of Christian experience I may have
had in college is not the norm for my entire life. This is the impor-
tant point, The college-type Christian conversion experience may
be a very important and necessary stage in my Christian develop-
ment, but it would be wrong (even perverse) for me to try contin-
ually to keep up that kind of “lighthearted” Christian experience in
the midst of a mature adult world, with all ils cares, responsibili-
ties, and tribulations.

This is why the kind of testimonies these college students were
making before the Presbyterian congregation seemed off base to
me. They were not really relevant to my stage of life as a 34-year
old family man. I could appreciate and rejoice in what the Lord
was doing with them, but I also saw that He was not doing quite
the same thing with me.

Between my senior year of high school and my freshman year
of college, I too was “converted.” T read Billy Graham’s World
Affame, and I came to understand for the first time that I had to be
justified apart from any of my own works and intentions. I ac-
cepted Christ into my heart, and for a month I was on a kind of
“honeymoon” with the Lord. For years, ] told people that I had
not been a Christian before, only a “good churchgoer.” I now no
longer tell people that.

Was I not a Christian before? Was the young woman whose
testimony I reproduced above not really a Christian before she
went to college? I think I was, and | think she was, too. What
happened was that we came to a new stage of maturity, a stage at
which we needed to understand in a new, more profound way,
what the Christian faith entails. We went through a crisis, and ex-
perienced a conversion.

I believed in Jesus when | was little, and I’m sure she did to.
We were both loyal to Him, We kept His rules, We went to His
church, We sang hymns to Him, We had the kind of faith ap-
propriate for the childish stage of life. When we got to age 17, how-
