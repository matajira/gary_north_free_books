274 The Sociology of the Church

people, but all believers approaching directly to the throne of
Grace with the officiant acting as the ‘orchestra director, leading
the people in their worship, so that worship is done decently and
in order.” Let me make two comments on this.

First, if the minister at any point prays a prayer in which all
the people do not join, he is in some sense “representing” them to
God. Only if all the prayers are choral would this principle of rep-
resentation be avoided. In fact, of course, the principle that the
minister represents the people in prayer, and they join in at the
end by saying “Amen,” is totally “protestant” (as is the principle of
representation in general). If we grant this, though, then we have
granted some kind of representation, and that’s all I need argue for.

Second, however, I think the New Testament indicates the
propriety of an officiant who collects prayers and offers them to
God as the peoples’ representative during worship. The primary
New Testament document designed to teach us about worship is
the Book of Revelation, which takes place on the Lord’s Day, and
which shows us how worship is conducted in heaven. Revelation
starts out by identifying the “angels” or messengers of the churches
as the presiding pastors of seven local churches. The letters to the
seven churches are addressed to the seven angel/bishops, as repre-
sentatives of the churches. Then, in heaven, we repeatedly see
these seven angels (or their heavenly counterparts and arche-
types) performing liturgical acts—specifically, blowing wumpets
(reading the Word to the earth/congregation) and pouring out
chalices (administering the Sacraments, in this case negatively, to
the earth), All of this indicates a continuing New Testament prin-
ciple of liturgical representation. (And see Revelation 8:3, which
seems to refer to Christ, but which by calling Him an “Angel”
links this part of His ministry to the ministry of the seven angels of
the churches.)

Most evangelicals readily grant that there are special as well
as general officers in the church: servant priests who minister to
the royal priesthood. Against ecclesiastical anarchists, we argue
for officers and government in the church. I believe that this same
principle holds true in worship, and that there is good Olid and
New Testament evidence for it. Some people feel that if we have
