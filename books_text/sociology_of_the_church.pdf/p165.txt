5
CONVERSION

My purpose in this essay is not to provide a complete theology
of conversion, but to comment on an experience I had in the sum-
mer of 1984. I was invited to speak at a conservative Presbyterian
church, I spoke in the morning, and in addition to the regular con-
gregation I found I was speaking to a group of bright-eyed college
students, who were in the area for the summer. As part of a basic-
ally Campus Crusade oriented ministry, this group of students was
working at earning money for tuition during the week, attending
Bible classes in the evenings, and doing beach evangelism on the
weekends. This kind of thing is very common, and I was personally
pleased to meet these young people. I was also happy to see that
this conservative Presbyterian church had become their home for
the summer, welcoming them into its fellowship.

As I said above, I spoke in the morning. The evening service
was put on by the students, it being their last Sunday in the area.
They had formed a chorus, and sang some of the modern post-
Jesus Movement songs that are the standard (and sadly superfi-
cial) fare among these groups. They also gave testimonies, and
one of them preached to the congregation.

As I listened to the testimonies, and to the little sermonette, I
realized that there was a time when this kind of thing would have
moved me, but that it no longer seemed very relevant. Was this
because my own faith had grown cold? I hope not. Was this
because their method of presenting the gospel was so grossly
off-base as to be unacceptable? Well, this is sometimes asserted in
“hard-core Reformed” circles, and I once felt this way myself. But

151
