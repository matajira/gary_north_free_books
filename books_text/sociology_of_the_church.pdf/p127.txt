Sociology: A Biblico-Historical Approach 13

Let us look again at three of the primary features of the first
creation, this time in more detail.” There was the duality of one
central sanctuary and downstream lands, with a middle wall of
partition between the two. There was but one Throne of God on
the earth: the Tree of Life, the Tabernacle, the Temple. Second,
there was the flesh of Adam, and the consequent principle of gen-
ealogy by blood. To show that this blood was defiled by sin, the
foreskin of the organ of generation had to be cut off of all male
members of the Seed line. All the same, genealogical records were
of central importance in demonstrating continuity in the old crea-
tion, Third, there was the sabbath day, a token that someday the
first creation would mature and be transfigured into a new creation.

The coming of the New Covenant does not restore the original
world. Rather, we find that the work of Christ brings the first cre-
ation to its fulfillment, and inaugurates a new one. The arrange-
ment of space in a duality of sanctuary and land is set aside, and
one new “land” comes in its place, That “land” is the community
that exists in the sphere of the Spirit, which is nothing other than
heaven itself.2” The first man is of the earth, but the new man is in
the sphere of the Spirit (1 Cor. 15:47). That is, the first man was
made of earth (Gen. 2:7), and there was a tie between man and
the earth, such that when man fell, the world was affected by this
and became cursed. Salvation removes man from this essential tie
to the land, and places him essentially in the eschatological sphere

 

131300, Tyler, TX 75713).

James B. Jordan, “Interaction Tapes on the 4.n, 70 Question,” being replies
and criticisms to the book by Max King (mentioned above), and to J. Stuart
Russell’s book, The Parousia (Grand Rapids: Baker, 1983), These tapes are available
from Geneva Ministries, Box 131300, Tyler, TX 75713 for 814.00 (four lectures).

James B. Jordan, Lectures on Matthew 24, available from Geneva Minis-
tries (address above) for $35.00 (eleven lectures).

26. The careful reader will realize that I am presenting this material in spiral
form, discussing the same basic subjects over and over, but adding more cach
time. It scemed to me to be the best way to present my thesis.

27. On this idea of a community in the Spirit, the reader should read two sem-
inal works. The first is R. B. Gaffin, The Centrality of the Resurrection (Grand Rap-
ids: Baker, 1978). The second is Meredith G. Kline, Images of the Spirit (Baker,
1980). The reader who is unfamiliar with these ideas is advised to read these two
books in this order.
