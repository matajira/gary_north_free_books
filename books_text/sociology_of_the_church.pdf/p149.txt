Thoughis on Modern Protestantism 135

mancer Bishop Pike to be effective for the Kingdom (1 Sam. 28).

The Kingdom starts small, like a mustard seed, The denomi-
nation of which I am a part, the Association of Reformation
Churches, is tiny. So are other faithful denominations. We live in
a time of re-creation, and so we start small, We are not to despise
the day of small things.

The Problem

The problem is that most conservative protestants today do
not act like catholics. That is, they don’t act as true protestants
should act. They tend to act more like sectarians, condemning
those who differ with them, undermining other congregations, re-
fusing to recognize the discipline of other churches, and so forth.

The essays in this section are devoted to certain aspects of this
problem, Chapter 4 deals with the sectarian, statist, and catholic
aspects of the historic Reformation churches. Chapter 5 takes up
a particular sociological problem centered particularly in youth
ministries. Chapter 6 is a satire on how protestants tend to treat
one another. The next two chapters deal with issues currently
widespread in American Christianity, Chapter 7 is designed to
point to a catholic “middle way” among the factions in the dispute
over miracles today, while Chapter 8 deals with the heresy of
Christian Zionism that currently plagues the church in the United
States. Chapter 9 deals with present state-church conflicts.
