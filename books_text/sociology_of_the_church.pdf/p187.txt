Propositions on Pentecostatism 173

irrational counterpart to their primacy-of-the-intellect form of
worship.

4.2.1. Itis not an accident that revivalism sprang
out of the incredibly infrequent communion seasons of the Scot-
tish and Puritan churches.

4.3, The Newtonian world-view, adopted in Reforma-
tion lands, and used in apologetics, is mechanistic and overly ra-
tionalistic.?

4.4. Rationalism can be used for good or for ill.

4.4.1, The development of doctrine in the Re-
formed church has been a good.

4.4.2, The tendency of the Reformed churches his-
torically to slip into Amyraldianism, Arminianism, Unitarianism,
and Liberalism has been an ill,

4.5. Irrationalism can also be used for good or for ill.

4.5,1, The renewed life in the churches after reviv-
als has been a good.

4.5.2. The sexual and other emotional-type sins
produced by revivalism and pentecostalism have been ills.?

5. The modern pentecostal movement has produced much good
and much ill.

5.1, Goad things include breaking down intellectualism
and extreme rationalism among conservative churches, breaking
down rationalistic liberalism in large denominations, renewed
love for and study of Scripture in many circles, renewed concern
for the trans-rational aspects of the faith.

5.2. Bad things include sexual and other forms of moral
license, downgrading of Scriptural authority in favor of en-
es, and a

 

 

thusiasm, increase in demonic activity in many circ
general orientation toward entertainment that goes so far as to see

2, The works of Gornelius Van Til sounded the deathkneil for traditional ra-
tionalistic and evidentialistic apologetics, The intellectual (though not personal)
hostility to Van Til has been so great throughout evangelicalism that he is gener-
ally either ignored or accused of not believing in apologetics at all

3, See Gary North, “Revival: ‘True and False,” in Biblical Economics Today 8:6
(Oct./Nov., 1985).
