268 The Soctology of the Church

collar with Gvo white tabs sticking out from it. Nobody els
dressed that way. Thus, in Scotland, the heartland of super-
presbyterianism, clerical garb and vestments were used.

So, I'd like to know just what Bishop Hooper was fighting
about. Was he opposcd to all cle
Was he opposed to aél liturgical vestments, or just to some of
them? Apparently he opposed those that were associated with the
separate office of bishop, and those associated in the popular mind
with the performance of the “sacrifice of the Mass.”6

It is instructive to continue looking at the history of vestments
in England. James Hastings Nichols points to the problem the
Reformer’s encountered, commenting on how the first Book of
Common Prayer was received: “While Cranmer repudiated any idea
of priestly cons lion or of a propitialory sacrifice, he was scan-
dalized to find that his Romanizing opponents could read these

 

 

I garb, or just to some items?

  

meanings back into his service by means of the ccremonial and
ritual of the Mass, altars, vestments, lights, gestures. In the very
year of its publication he began a revision of the Prayer Book to
make its theology more explicit.”? The problem was that any cere-
monial action or symbol can be understood in a variety of ways,
because in and of themselves ccremonics and symbols are silent.®

To take an example, let us consider the lifting up of the
clements in worship. In the Bible, the gifts given to God were
lifted up to heaven, and then received back from Him. This was
called “heave offering” when the gift was lifted upward and back
down, or “wave offering” when the gift was waved forward toward

 

 

 

the throne and then received back. There was nothing magical
about this; it was simply an external action that accompanied and
displayed the act of giving a gift to God. The priest lifted the gift

6. See the discussion in A. TT. Drysdale, History of the Presbyterians in England.
Their Rise, Decline, and Recival (London; Publication Committee of the Presbyter-
ian Church of England, 1889), pp. 528,

7. James Hastings Nichols, Corporate Wership in the Reformed iadition (Philadel-
phia: Westininster, 1968), p. 63.

8, On how “liturgical piety” changes the way rites are perceived, see my essay
“Christian Piety: Deformed and Reformed,” in The Geneva Papers, Vol. 2, No. 1
(lyler, TX: Geneva Ministries, 1985)

 
