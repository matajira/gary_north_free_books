264 The Sociology of the Church

ciple. Thus, the Bible may indicate that clerical garb and/or vest-
ments are a good thing, without commanding that the church must
always use them, or describing what they must look like.

The main problem we have understanding dress in Biblical
times is that the industrial revolution, coupled with democratic
notions of society, have completely separated us from all human
traditions in this area. Nowadays, men dress in “business suits”
regardless of their profession; earlier, this was not so. In tradi-
tional societies, clothing gave a visible indication of the status of a
person, in two regards. First, it gave an indication either of his oc-
cupation, or of the kind of occupation he held. Particularly on
special occasions, the guild of blacksmiths dressed one way, the
guild of bakers another, and the guild of barber-physicians
another, and so forth. Clothing marked calling. Democracy has
impoverished us to the extent that this no longer is so.

Second, clothing marked clan. Among the Scots, as every
presbyterian knows, the plaid tartan is different for each clan.
This custom is not unique to Scotland. Now, looking back into the
Bible, even if the Bible said nothing about it, we may be virtually
certain that the various tribes had different patterns of clothing.
This means that the Levites dressed differently from everyone
else, and since the Levites became the clergy in Israel, their dis-
tinctive tribal garb became the clerical garb of Israel.

We can go further than this general inference, however. God
marked out the various ranks of clergy with special clothing, dur-
ing the Old Covenant. Because every Israelite was a priest to the
nations, every Israelite was to wear special clerical garb, consist-
ing of a blue tassel at the corners of his outer garment (Num.
15:37-41). Moreover, the house of Aaron were the priests to Israel,
and they were all dressed in special clothing (Ex. 28:4, 40, 41; 1
Sam, 22:18), Finally, the high priest, as priest to all Israel includ-
ing the Aaronic house, had extra-special vestments to wear (Ex.
28:6-39), Now of course, this was in the Old Testament, but there
is no reason to presume any change in principle here. We do not
simply reject something just because we do not find it explicitly
repeated in the New Testament.

ds there a continuity in this area? Certainly. A priest was not
