248 The Sociology of the Church

Baptist theology has grown up. The Baptist doctrine is that bap-
tism symbolizes a person's individual faith and regeneration, so
that only such persons can come to the Table of the Lord. This,
however, is not what baptism means in the Bible. In Scripture,
baptism is God’s claim of ownership and God’s promise of salva-
tion. In the sense that it is a claim, baptism creates an obligation
to obey God’s Word. In the sense that it is a promise, baptism is
the Gospel, and creates an obligation to exercise faith in God.
Thus, the Reformation faith exhorts its children (and slaves, if
there are any) to improve on their baptisms, to mix faith with the
promises, The promise is for you and to your children, we are told
(Acts 2:39), just as it was for Abraham. The promise must be
mixed with faith to be effective, for there is no automatic salva-
tion. Baptism, however, is not man-centered, a sign of faith, but
God-centered, a sign of the promise. Thus, baptism is adminis-
tered first, and then faith is to follow. The Bible does not teach us
to baptize indiscriminately, but to baptize by households, Those
who share table fellowship with the covenant head of the house-
hold (wife, children, and slaves) are included in the household
covenant, and baptized. They also belong at the Lord’s Table.

When Jesus invites us over to His house for a dinner, He does
not tell us to get a babysitter and leave the kids at home. They are
invited, too. They cross the threshold with their parents, and sit
with them at the meals.

Current-day practice, however, often assumes that baptized
children must go through some experience, to the satisfaction of
some spiritual examiner, before they can be admitted to the Lord’s
Table. There is not a shred of evidence in Scripture for this addi-
tional demand. If we are going to treat our children as unregener-
ate until they have gone through some mystical experience, we
had better not teach them to pray, or even permit them to pray.
Away with such hymns as “Jesus loves me, this I know, for the Bible
tells me so. Little ones to Him delong; they are weak, but He is
strong.” That song is a lie, if children are not even allowed to eat
Jesus’ food.

The Biblical perspective is clear. We teach our children that
Jesus is their God and Savior. We teach them to pray, and we
