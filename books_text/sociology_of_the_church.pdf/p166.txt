152 The Sociology of the Church

as I thought about it, I came to a different conclusion, and this
essay is the result.

Let me encapsulate one of the testimonies I heard. A young
woman got up and said something like this: “When I went to col-
lege, I thought I was a good Christian. I didn’t use dope, and I'd
grown up in a good Christian home and had been active in a good
Christian church. But I found out that I wasn’t really a Christian, I
had to break some idols in my heart, and meet Jesus personally.

“There was this boy, you see. We'd been dating seriously, but
he was not a Christian. I didn’t want to give him up. I found my-
self in more and more tension over this, and finally I got down
and prayed that Jesus would just take over. I was finally willing to
give up this boy. And you know what? We broke it off, and I’ve
never missed him since. I’ve found something more wonderful to
live for. I hope you do to.”

Now remember, the people she was addressing with this testi-
mony were mostly well over 30 years of age. Many were over 50. I
could tell that they were delighted that she had found Christ, but I
could also tell that they did not really connect up with her experi-
ence readily.

Now, the testimony I just rehearsed for you is a standard testi-
mony ritual. Impressionable young people take up the forms and
attitudes of influential older people who minister to them, and this
kind of testimony ritual is standard in campus ministries, Point 1:
I thought I was already a Christian. Point 2: I realized I was not,
because I had not given aéf to Him. Point 3: I gave it all to Him,
and found peace, Point 4: You can too.

Now, is there something wrong with all this? Well, clearly not,
in one sense, but in another sense there is something wrong.
What is wrong is that there is an erroneous understanding of con-
version operating here.

What is Conversion?

Conversion is a turning from sin to Christ. Now, tet’s think
about that. Does conversion happen only once in a lifetime, or
does it happen many times? That is the question, I believe, that
needs answering.
