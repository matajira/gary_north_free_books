72 The Sociology of the Church

ment, but the proper procedure was to pray and prophesy, that
the visible church might be restored.

It is precisely at this point, however, that the New Covenant
differs from the Old. Recognizing a liberal Episcopal church as a
true church does not carry with it a requirement that no one may
ever transfer from it to a better church, The New Covenant situa-
tion, being multi-centralized, provides such options. Moreover,
any local church is free at any lime to sever ils ties with the larger
church, because the local church does not receive its existence
from being part of the larger.

The practical question of when to give up on an historical shell
in be answered by taking a “Wave”

 

and move to create a new one
view of the matter. What is important is not the position of the
shell at any given moment, but its settled trend. A shell may be
doing the sacraments in a basically correct manner, and have a
fine constitution on paper, but be in a trend away from God. This
trend can be observed. An observable trend away from God does
indicate that the shell is apostate. The conservati

 

not full
office-bearer must wail for God to bring to pass an issue that re-
quires a carcful, polite, but firm and iniransigent confrontation

   

over the drift, using matters clearly and unmistakably revealed in
the Word'®. This is required by Ezk. 3:17-21. ‘The confrontation
will surely reveal the situation (1 Cor. 11:19), Repeated confronta-
tions will make it clear to all. If what is revealed is a settled deter-

 

mination to defy the clearly-revealed laws of God, departure is
mandated. The Christian should not be enslaved to an evil insti-
tutional shell simply because of a false successional view of the
church. !6

15. For instance, a man commits adultery, forsakes his wife, and renounces
the church. If the elders flatly refuse to excommunicate this man (because he is a
relative of some of them, or a power in the community), then matters are clearly
and unmistakably revealed.

16. It is not the place of non-officers to provoke such confrontations, The lay-
man (or, general officer) should approach a special officer whom he trusts, and
ask him to provoke the confrontation. If there are no special officers who care
enough to fight for orthodoxy, then the general officer should quietly and peace-
ably transler to another church, God never blesses insurrection, even if the
cause is just.
