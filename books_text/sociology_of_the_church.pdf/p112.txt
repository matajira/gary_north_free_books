98 The Sociology of the Church

sets up priestly worship (verse 25), and immediately thereafter the
Philistines come to make covenant with him. The same day,
another well is found (verse 32).

For some reason, modern commentators want to believe that
these covenants between Abraham and the Philistines and be-
tween Isaac and the Philistines do not signify the fulfillment of the
Abrahamic Covenant. The God of such commentators i
parsimonious God, it seems. He just doesn’t save gentiles. Actu-
ally, it is inconceivable that either Abraham or Isaac would form a
covenantal alliance with wicked men, Moreover, in the ancient
world, such covenant alliances were never “merely political,” for
the kind of secularism we have today did not exist then, A cove-
nantal alliance was always religious in character.

The second story is in Genesis 33, When Jacob met Esau, the
priest blessed his brother, at least with gifts (33:11). On the other
hand, when Esau wanted to remain with him, Jacob insisted that
he depart and take dominion over his own land (33:12-16). Esau’s
dominion is set out in Genesis 36, and the integrity of that land is
insisted upon by God in Numbers 20:14-21 and Deutcronomy
23:7. Edom might have been a “Christian nation,” living down-
stream from Israel, though they chose not to be. Jacob did not
permit Esau to join in his priestly work, but he did help establish
him in his kingly estate.

The third story shows the same principles. In Genesis 41, God
destroys the pride and self-confidence of Pharaoh by interrupting
his cozy world with the Word of revelation, When Joseph inter-
prets Pharaoh’s dream, we do not read that Pharaoh had Joseph
driven from his presence as a religious fanatic, Rather, Pharaoh
submits to the Word, and to Joseph’s application of it. This is
nothing other than the conversion of Pharach and of the Egypt-
ians. In line with the Abrahamic promise, they choose to biess
themselves in the seed. Joseph is put in charge of everything.
Later, in Exodus 1, the Egyptians fall from grace, and lose the
benefits of the Edenic influences—but that is later.

Here again, commentators just don’t want to believe this, Par-
tially it is because they utterly fail to do justice to the theology of
Genesis and the initial fulfillments of the Abrahamic promises.

 

a most

   
