178 The Sociology of the Church

stand this religious principle any more than do their British-
Israelite critics. Both conceive of everything in terms of blood and
race.)

So then, it is spurious to criticize Zionism on the grounds that
“Jews really didn’t suffer during World War II,” or “Who knows
who the real Jews are?” It is pretty obvious who the Jews are, and
they are, as always, a force to be reckoned with.

The third line of criticism against Zionism concerns the right-
ness or wrongness of its invasion and conquest of Palestine, We
can listen to arguments to the effect that the Jews stole the land
from its inhabitants, that they have persecuted the Palestinians,
that they committed horrors during their guerilla campaign, and
the like, Then we can listen to arguments that say that the Jews in
Palestine were mistreated under Moslem rule, that the Palestin-
ians are better off today under enlightened Jewish government
than they formerly were, that the Jews have exercised dominion
over the land and the Moslems did not, thereby forfeiting their
right to it, and the like.

Actually, none of this is any of our direct concern as Chris-
tians. As Christians we see both Jews and Moslems as groups that
have rejected Christ as Messiah, and who have opposed the true
faith. If they want to convert, we rejoice. If they want to kill each
other off, then that is too bad, but let them have at it—there’s
nothing we can do about il.

But then, that brings us to the issue: Are Bible-believing
Christians supposed to support a Jewish State, for theological rea-
sons? Such is the assertion of Jerry Falwell, and of the heresy of
Christian Zionism. Let us turn to this doctrine.

Orthodox Dispensationalism versus Christian Zionism

During the nineteenth century, a peculiar doctrinal notion
known as “dispensationalism” arose. Its leading lights were Darby
and Scofield; its Bible was the Scofield Reference Bible; and in re-
cent years its primary headquarters has been Dallas Theological
Seminary. Technically, dispensationalism teaches that God has
two peoples in the history of the world: Israel and the “Church.”
