146 The Sociology of the Church

 

ics,” bat their later heirs were mostly sectaries in their thought.

This is as good a place as any to point out that catholic, na-
tionalistic, and seclarian tendencies can be found in any of the
churches of the Reformation. Except for extreme Anabaptist sec-
taries and extreme Erastian nationalists, all threc faces of protest-
antism can be found within any particular church in any land.
What we can notice, however, is that in protestantism as a whole,
the catholic impulse tends to be lost in a battle between sectarian
and nationalistic tendencies. In both Scotland and in Holland, for
instance, there were numerous church splits, yet each little splin-
ter group maintained that it was the true national church, and
thus entitled to receive a dole from the state!

In protestant lands, it seemed as if Catholicity were impo:
ble. “Catholic” meant bad. The early church was ignored, and the

 

 

 

 

great gains of (he medieval period were all viewed as evils “pro-
duced by antichrist.” Unthinking protestants gave away the gov-
ernmental power of the church to the state, and the result was that
protestants had only two choices: either the church was ruled by
the state, or else the church was a drop-out sect that made no
claim to be an alternative government on the earth. The sects em-
phasized preaching, and the governmental side of the church dis-
appeared, What protestant church today has law courts, or a law

 

school? Where are the protestant texts on church law? Where are
protestant canon lawyers? ‘lo ask such questions is to expose the
sad truth that the protestant churches have given away the great
gains made the by early and medieval churches. The result is
rampant statism everywhere.

The Catholic Reformers in Switzerland did not capture the
day. Had they won out, there would have been a Reformed Cath-
olic Church, ‘There would have been weekly communion, so that
the threat of excommunication would have meant something real.
There would have been Reformed church courts, with elders and
ministers sitting as real judges over matters pertaining to the spir-
itual government of the church. The statism that has led to so
many wars in Europe over the past several centuries would have
been restrained. There would have been no extreme Puritan
