2
TRIUMPHALISTIC INVESTITURE

Since the time of the Vestarian Controversy in Elizabethan
England, and even earlier, the wearing of vestments has been
viewed as a “Romish error” by many evangelicals, particularly
those in the Puritan, Presbyterian, and Baptist traditions, as well
as those influenced by them. The purpose of this essay is to take
another look at the issue. We need to distinguish between two
different matters at the outset. The first is the question of the use
of distinctive clerical garb, and the second is the question of the
use of liturgical vestments (special clothing used in worship by the
officiating elder).

Clerical Garb

Virtually all churches in America expect their ministers to
wear special clothing, to have a special look. Just to illustrate this,
let me briefly describe three common forms of distinctive clerical
garb. First, there is the fundamental baptist’s clerical garb, which
I call the “flashy” look. It often involves, for instance, white or
maroon shoes, a white or maroon belt, a loud necktie, and some
form of relatively loud suit. This flashy (almost “superfly”) look is
found not only among fundamental baptists, but also among pen-
tecostals of all sorts. Clearly not all fundamentalist and pentecos-
tal preachers dress this way, but many do,

Second, there is the young evangelical preacher look. This in-
volves a more conservative business suit, but not black. It also in-
volves a particular styling of the hair, this being perhaps the most
distinctive aspect of the YEP look. One of my colleagues, Elder

259
