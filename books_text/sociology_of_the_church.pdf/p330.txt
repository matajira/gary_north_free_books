316 The Sociology of the Church

Christian Zionism, 180f.
Chureh (see Annotated Table
of Contents)
bureaucratic form, 235
discipline, 22
imperial form, 235
nursery of kingdom, 189
recognition of, 3f.
shell and (see Shell)
splitting, 163#f.
state conflict with, 138.
Sut generis institution, 191, 194
terminology for, 295f.
three-fold aspect of, 5IM., 295#f.
true and false, Sf
unity, 131
visible in Episcopal culture, 2f.
visibility, 53f.
Circumcision, 99ff., 104, 108, 120
and definition of a Jew, 177
Civil War (in United States), 148f.
Clothing, clerical, 2594F
early church, 266,
symbolism of, 264f.
Collar, clerical, 261f., 276
Common grace = crumbs, 245
Communion, Holy (see Lord’s
Supper)
Communion seasons, 228
Community and covenant, 244f,
Compromise, judicious, 197
Confrontation with apostasy, 72
Congregational participation, 230
Conversion, kinds, 152f.
Corpus mysticum, 140F.
Council, 42f,
Covenant theology, 122f.
Covenants with nations, 98
Covenant (see Annotated Table
of Contents, chapter 3)
and community, 244ff
curse, 36
personal and structural, 231
recital, 243

  

Cranfield, C. E. B., 108
Cranmer, Thomas, 143
Gross
architecture, 214 ff.
army, 215.
Israel arranged like, 214
pattern or shape, 212f.
sign of dominion, 217
Crossing, sign or action, 2116, 293
Cuius regio, eius religio, 142
Curse of dust, 84, 97f
Cycles, 240

Dance, 219.
David's Band, 134f.
Deacons, 49, 256f,
Deaconesses, 49
Democracy as heresy, 17
Denominationalism, 74ff., 147
Demons, 287
Dispensationalism, 116, 1221., 1754
negates the Bible, 76
literature ignored, 20
orthodoxy versus pop, 178!
Dix, Gregory, 145
Doing and knowing,
Donatism, 70
Dress (see Clothing)
Duality, UOH., 131.
geographical, 84ff
Dust, curse of, 84, 97f.

3H,

 

East, B8f., 95, 118
‘Edah, 2968.
Eden, 39, B4if.
square, 212fF
Edification, 25
Ekklesia, 298.
Elders
hierarchy (see Hierarchy)
symboli¢ of both Christ and Bride,
2628.
Elim, 101
Elite, 196.
